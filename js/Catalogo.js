var Catalogo = function(){
    
    this.buscador = null;
    this.getBuscador = function(){
        if(!this.buscador) this.buscador = new Buscador();
        return this.buscador;
    }
    
    this.setIdElementoParaResaltar = function(idelemento){
        this.getListaSeleccionable().setIdElementoParaResaltar(idelemento);
    }
    
    this.forma_captura = null;
    this.setFormaCaptura = function(forma){
        this.forma_captura = forma;
    }
    
    this.getFormaCaptura = function(){
        return this.forma_captura;
    }
    
    this.lista_seleccionable = null;
    this.getListaSeleccionable = function() {
        if (!this.lista_seleccionable) {
            this.lista_seleccionable = new ListaSeleccionable();
            this.lista_seleccionable.setNombreInstancia(this.nombre_instancia + ".lista_seleccionable");
        }
        return this.lista_seleccionable;
    }
    
    this.mostrando_forma = 0;
    
    this.mostraOpcionesDeImpresion = function(opcion){
        var contenido = "<br/><div id='"+this.nombre_instancia+"_zona_botones_impresion'>";
        contenido += "<div class='row'>";
        contenido += "<div class='col l6'>";

        contenido += "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".imprimeLista(2)' style='margin:0 auto 0 auto;'>PDF</a>";

        contenido += "</div>";
        contenido += "<div class='col l6'>";

        contenido += "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".imprimeLista(1)' style='margin:0 auto 0 auto;'>CSV</a>";

        contenido += "</div>";
        contenido += "</div>";
        contenido += "</div>";
        
        this.contenedor.innerHTML += contenido;
    }
    
    this.contenedor_forma_flotante = 0;
    this.setContenedorFormaFlotante = function(valor){
        this.contenedor_forma_flotante = valor;
    }
    
    this.tamanio_componente_modal = "m";
    this.setTamanioComponenteModal = function(tamanio){
        this.tamanio_componente_modal = tamanio;
    }
    
    this.componente_padre = null;
    this.setComponentePadre = function(componente){
        this.componente_padre = componente;
    }
    
    this.selector_opcion = null;
    this.getSelectorDeOpcion = function(){
        if(!this.selector_opcion) this.selector_opcion = new SelectorOpcion();
        return this.selector_opcion;
    }
    
    this.funcion_elemento_seleccionado = null;
    this.setFuncionElementoSeleccionado = function(funcion){
        this.funcion_elemento_seleccionado = funcion;
    }
    
    this.setDatosVisualizacionLista = function(encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada){
        this.lista_seleccionable.setEncabezados(encabezados);
        this.lista_seleccionable.setMedidas(medidas);
        this.lista_seleccionable.setAlineaciones(alineaciones);

        this.lista_seleccionable.setFuncionEliminaElemento(funcion_elimina);
        this.lista_seleccionable.setOpcionDetalle(opcion_detalle);
        this.lista_seleccionable.setFuncionElementoSeleccionado(funcion_elemento_seleccionado);

        this.lista_seleccionable.setFuncionTablaMostrada(funcion_tabla_mostrada);
    }
    
    this.titulo_catalogo = "- Catalogo Generico";
    this.setTituloCatalogo = function(titulo){
        this.titulo_catalogo = titulo;
        if(document.getElementById(this.nombre_instancia+"_titulo_catalogo")) document.getElementById(this.nombre_instancia+"_titulo_catalogo").innerHTML = this.titulo_catalogo;
    }
    
    this.texto_agregar_elemento = "Agrega un nuevo elemento";
    this.setTextoAgregarElemento = function(texto){
        this.texto_agregar_elemento = texto;
    }
    
    this.muestra_opciones_laterales = true;
    this.setMuestraOpcionesLaterales = function(valor){
        this.muestra_opciones_laterales = valor;
    }
    
    this.tabla_elementos_busqueda = true;
    this.setTipoTablaElementos = function(tipo){
        this.tabla_elementos_busqueda = tipo;
    }
    
    this.size_contenedor_flotante = "";
    this.setSizeContenedorFlotante = function(size){
        this.size_contenedor_flotante = size;
    }
    
    this.muestraEstructuraCatalogo = function(){
        this.muestraComponenteGenerico();
        
        this.getTituloPrincipal().innerHTML = "<table><tr><td width='60%'><h5>"+this.titulo_catalogo+"</h5></td><td width='10%'></td><td width='30%'><div id='"+this.nombre_instancia+"_zona_buscador'></div></td></tr></table><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>";
        
        this.getBuscador().setComponenteBuscador(document.getElementById(this.nombre_instancia+"_zona_buscador"), this.nombre_instancia+".getBuscador()", this.nombre_instancia+".muestraElementosDelCatalogo()");
        this.getBuscador().muestraCampoBuscadorInstantGrande();
    }
    
    this.muestraEstructuraCatalogoPrincipal = function(){
        this.muestraComponenteGenericoPrincipal();
        
        this.getTituloPrincipal().innerHTML = "<table width='98%'><tr><td width='100%' align='center'><div class='titulo_componente'>"+this.titulo_catalogo+"</div><br/><div id='"+this.nombre_instancia+"_zona_selector'></div><br/><div id='"+this.nombre_instancia+"_zona_buscador'></div></td></tr></table><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>";
        
        this.getBuscador().setComponenteBuscador(document.getElementById(this.nombre_instancia+"_zona_buscador"), this.nombre_instancia+".getBuscador()", this.nombre_instancia+".muestraElementosDelCatalogo()");
        this.getBuscador().muestraCampoBuscadorInstantGrande();
    }
    
    this.muestraEstructuraCatalogoPrincipalConOpcionAgregarNuevo = function(){
        this.mostrando_forma = 0;
        //this.muestraComponenteGenericoPrincipal();
        
        this.contenedor.innerHTML = "<label class='valign-wrapper titulo_forma'>"+this.titulo_catalogo+"</label><hr class='separador'/><div id='"+this.nombre_instancia+"_zona_selector'></div><div id='"+this.nombre_instancia+"_zona_buscador' class='left-align'></div><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>";

        //agregamos el buscador
        this.getBuscador().setComponenteBuscador(document.getElementById(this.nombre_instancia+"_zona_buscador"), this.nombre_instancia+".getBuscador()", this.nombre_instancia+".muestraElementosDelCatalogo()");
        this.getBuscador().muestraCampoBuscadorInstantGrande();
        
        //agregamos el selector
        this.getSelectorDeOpcion().setComponente(document.getElementById(this.nombre_instancia+"_zona_selector"), this.nombre_instancia+".getSelectorDeOpcion()");
        this.getSelectorDeOpcion().setTextoOpcionHover(this.texto_agregar_elemento);
        
        if(this.contenedor_forma_flotante == 0)
            this.getSelectorDeOpcion().setFuncionAgregar(this.nombre_instancia+".nuevoElemento()");
        else if(this.contenedor_forma_flotante == 1)
            this.getSelectorDeOpcion().setFuncionAgregar(this.nombre_instancia+".nuevoElementoFlotante()");
        
        this.getSelectorDeOpcion().muestraComponente();
    }
    
    this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo = function(){
        this.mostrando_forma = 0;
        //this.muestraComponenteGenericoPrincipal();
        
        this.contenedor.innerHTML = "<label class='valign-wrapper titulo_forma'>"+this.titulo_catalogo+"</label><hr class='separador'/><div id='"+this.nombre_instancia+"_zona_buscador'></div><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>";

        //agregamos el buscador
        this.getBuscador().setComponenteBuscador(document.getElementById(this.nombre_instancia+"_zona_buscador"), this.nombre_instancia+".getBuscador()", this.nombre_instancia+".muestraElementosDelCatalogo()");
        this.getBuscador().muestraCampoBuscadorInstantGrande();
        
        if(this.contenedor_forma_flotante == 0)
            this.getSelectorDeOpcion().setFuncionAgregar(this.nombre_instancia+".nuevoElemento()");
        else if(this.contenedor_forma_flotante == 1)
            this.getSelectorDeOpcion().setFuncionAgregar(this.nombre_instancia+".nuevoElementoFlotante()");
    }
    
    this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevoConBotonBuscar = function(){
        this.mostrando_forma = 0;
        
        var contenido = "<label class='valign-wrapper titulo_forma'>"+this.titulo_catalogo+"</label><hr class='separador'/>";
        
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s10 m10 l10 left-align">';
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_buscador'></div>";
        
        contenido += '</div>';
        contenido += '<div class="col s2 m2 l2 left-align">';
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_boton_buscar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".buscar()' style='margin:0 auto 0 auto;'>Buscar</a></div>"
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>";
        
        this.contenedor.innerHTML = contenido;

        //agregamos el buscador
        this.getBuscador().setComponenteBuscador(document.getElementById(this.nombre_instancia+"_zona_buscador"), this.nombre_instancia+".getBuscador()", null);
        this.getBuscador().muestraCampoBuscadorGrande();
        
        if(this.contenedor_forma_flotante == 0)
            this.getSelectorDeOpcion().setFuncionAgregar(this.nombre_instancia+".nuevoElemento()");
        else if(this.contenedor_forma_flotante == 1)
            this.getSelectorDeOpcion().setFuncionAgregar(this.nombre_instancia+".nuevoElementoFlotante()");
    }
    
    this.muestraEstructuraCatalogoPrincipalConOpcionAgregarNuevoSinBuscador = function(){
        /*this.mostrando_forma = 0;
        this.muestraComponenteGenericoPrincipal();
        
        this.getDivTitulo().innerHTML = "<table width='98%'><tr><td width='100%' align='center'><h4>"+this.titulo_catalogo+"</h4><br/><div id='"+this.nombre_instancia+"_zona_selector'></div><br/><div id='"+this.nombre_instancia+"_zona_buscador'></div></td></tr></table><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>";*/
        this.mostrando_forma = 0;
        //this.muestraComponenteGenericoPrincipal();
        
        this.contenedor.innerHTML = "<label class='valign-wrapper titulo_forma'>"+this.titulo_catalogo+"</label><hr class='separador'/><div id='"+this.nombre_instancia+"_zona_selector'></div><div id='"+this.nombre_instancia+"_zona_buscador' class='left-align'></div><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>";

        //agregamos el selector
        this.getSelectorDeOpcion().setComponente(document.getElementById(this.nombre_instancia+"_zona_selector"), this.nombre_instancia+".getSelectorDeOpcion()");
        this.getSelectorDeOpcion().setTextoOpcionHover(this.texto_agregar_elemento);
        
        if(this.contenedor_forma_flotante == 0)
            this.getSelectorDeOpcion().setFuncionAgregar(this.nombre_instancia+".nuevoElemento()");
        else if(this.contenedor_forma_flotante == 1)
            this.getSelectorDeOpcion().setFuncionAgregar(this.nombre_instancia+".nuevoElementoFlotante()");
        
        this.getSelectorDeOpcion().muestraComponente();
    }
    
    this.muestraEstructuraCatalogoPrincipalSinBuscador = function(){
        this.muestraComponenteGenericoPrincipal();
        
        this.getTituloPrincipal().innerHTML = "<table width='98%'><tr><td width='100%' align='center'><div class='titulo_componente'>"+this.titulo_catalogo+"</div><br/><div id='"+this.nombre_instancia+"_zona_selector'></div><br/><div id='"+this.nombre_instancia+"_zona_buscador'></div></td></tr></table><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>"; 
        
        this.setTipoTablaElementos(false);
    }
    
    this.getZonaDeSelector = function(){
        return document.getElementById(this.nombre_instancia+"_zona_selector");
    }
    
    this.muestraEstructuraCatalogoPrincipalConZonaInferior = function(){
        this.muestraComponenteGenericoPrincipal();
        
        this.getTituloPrincipal().innerHTML = "<table width='98%'><tr><td width='100%' align='center'><br/><div class='titulo_componente'>"+this.titulo_catalogo+"</div><br/><div id='"+this.nombre_instancia+"_zona_buscador'></div></td></tr></table><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div><div id='"+this.nombre_instancia+"_zona_botones_catalogo'></div>";
        
        this.getBuscador().setComponenteBuscador(document.getElementById(this.nombre_instancia+"_zona_buscador"), this.nombre_instancia+".getBuscador()", this.nombre_instancia+".muestraElementosDelCatalogo()");
        this.getBuscador().muestraCampoBuscadorInstantGrande();
    }
    
    this.muestraEstructuraCatalogoPrincipalConBarraOpciones = function(){
        this.mostrando_forma = 0;
        //this.muestraComponenteGenericoPrincipal();
        
        this.contenedor.innerHTML = "<label class='valign-wrapper titulo_forma'>"+this.titulo_catalogo+"</label><hr class='separador'/><div id='"+this.nombre_instancia+"_zona_secundaria'></div><div id='"+this.nombre_instancia+"_zona_selector'></div><div id='"+this.nombre_instancia+"_zona_buscador' class='left-align'></div><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>";

        //agregamos el buscador
        this.getBuscador().setComponenteBuscador(document.getElementById(this.nombre_instancia+"_zona_buscador"), this.nombre_instancia+".getBuscador()", this.nombre_instancia+".muestraElementosDelCatalogo()");
        this.getBuscador().muestraCampoBuscadorInstantGrande();
        
        //agregamos el selector
        this.getSelectorDeOpcion().setComponente(document.getElementById(this.nombre_instancia+"_zona_selector"), this.nombre_instancia+".getSelectorDeOpcion()");
        this.getSelectorDeOpcion().setTextoOpcionHover(this.texto_agregar_elemento);
        
        if(this.contenedor_forma_flotante == 0)
            this.getSelectorDeOpcion().setFuncionAgregar(this.nombre_instancia+".nuevoElemento()");
        else if(this.contenedor_forma_flotante == 1)
            this.getSelectorDeOpcion().setFuncionAgregar(this.nombre_instancia+".nuevoElementoFlotante()");
        
        this.getSelectorDeOpcion().muestraComponente();
    }
    
    this.muestraEstructuraCatalogoPrincipalConBarraOpcionesSinOpcionNuevo = function(){
        this.mostrando_forma = 0;
        
        this.contenedor.innerHTML = "<label class='valign-wrapper titulo_forma'>"+this.titulo_catalogo+"</label><hr class='separador'/><div id='"+this.nombre_instancia+"_zona_secundaria'></div><div id='"+this.nombre_instancia+"_zona_buscador' class='left-align'></div><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>";

        //agregamos el buscador
        this.getBuscador().setComponenteBuscador(document.getElementById(this.nombre_instancia+"_zona_buscador"), this.nombre_instancia+".getBuscador()", this.nombre_instancia+".muestraElementosDelCatalogo()");
        this.getBuscador().muestraCampoBuscadorInstantGrande();
    }
    
    this.getZonaBuscador = function(){
        return document.getElementById(this.nombre_instancia+"_zona_buscador");
    }
    
    this.getZonaOpciones = function(){
        return document.getElementById(this.nombre_instancia+"_zona_opciones");
    }
    
    this.getZonaBotonesCatalogo = function(){
        return document.getElementById(this.nombre_instancia+"_zona_botones_catalogo");
    }
    
    this.getZonaElementosDelCatalogo = function(){
        return document.getElementById(this.nombre_instancia+"_zona_elementos_catalogo");
    }
    
    this.getZonaSecundaria = function(){
        return document.getElementById(this.nombre_instancia+"_zona_secundaria");
    }
    
    this.muestraEstructuraCatalogoPrincipalConZonaSecundariaSinTitulo = function(){
        this.muestraComponenteGenericoPrincipal();
        
        this.getTituloPrincipal().innerHTML = "<div id='"+this.nombre_instancia+"_zona_secundaria'></div><br/><div id='"+this.nombre_instancia+"_zona_buscador'></div><br/><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>";
        
        this.getBuscador().setComponenteBuscador(document.getElementById(this.nombre_instancia+"_zona_buscador"), this.nombre_instancia+".getBuscador()", this.nombre_instancia+".muestraElementosDelCatalogo()");
        this.getBuscador().muestraCampoBuscadorInstantGrande();
    }
    
    this.muestraEstructuraCatalogoPrincipalConZonaSecundariaSinTituloConZonaBotones = function(){
        this.muestraComponenteGenericoPrincipal();
        
        this.getTituloPrincipal().innerHTML = "<div id='"+this.nombre_instancia+"_zona_secundaria'></div><br/><div id='"+this.nombre_instancia+"_zona_buscador'></div><br/><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div><br/><div id='"+this.nombre_instancia+"_zona_botones'></div>";
        
        this.getBuscador().setComponenteBuscador(document.getElementById(this.nombre_instancia+"_zona_buscador"), this.nombre_instancia+".getBuscador()", this.nombre_instancia+".muestraElementosDelCatalogo()");
        this.getBuscador().muestraCampoBuscadorInstantGrande();
    }
    
    this.getZonaDeBotones = function(){
        return document.getElementById(this.nombre_instancia+"_zona_botones");
    }
    
    this.muestraEstructuraCatalogoPrincipalConZonaSecundaria = function(){
        this.muestraComponenteGenericoPrincipal();
        
        this.getTituloPrincipal().innerHTML = "<div id='"+this.nombre_instancia+"_zona_secundaria'></div><table><tr><td width='60%'><h5 id='"+this.nombre_instancia+"_titulo_catalogo'>"+this.titulo_catalogo+"</h5></td><td width='10%'></td><td width='30%'><div id='"+this.nombre_instancia+"_zona_buscador'></div></td></tr></table><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>";
        
        this.getBuscador().setComponenteBuscador(document.getElementById(this.nombre_instancia+"_zona_buscador"), this.nombre_instancia+".getBuscador()", this.nombre_instancia+".muestraElementosDelCatalogo()");
        this.getBuscador().muestraCampoBuscadorInstantGrande();
    }
    
    this.muestraEstructuraCatalogoConSelector = function(){
        this.muestraComponenteGenerico();
        
        this.getTituloPrincipal().innerHTML = "<div id='"+this.nombre_instancia+"_zona_selector'></div><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>";
    }
    
    this.muestraElementosDelCatalogo = function(){
       this.getListaSeleccionable().setContenedor(document.getElementById(this.nombre_instancia+"_zona_elementos_catalogo"));
        this.getListaSeleccionable().muestraComponente();
        if(this.tabla_elementos_busqueda) this.getListaSeleccionable().muestraTablaElementosConBusqueda(this.getBuscador().getTerminoBusqueda());
        else this.getListaSeleccionable().muestraTablaElementos();
    }
    
    this.opcionListaSeleccionada = function(){
        this.muestraControladorNavegacion();
        
        this.getFormaCaptura().setComponente(this.getControladorNavegacion().getContenedorDeContenido(), this.nombre_instancia+".getFormaCaptura()");
        this.getFormaCaptura().setInfo(this.getListaSeleccionable().getDatosDeElementoSeleccionado());
        this.getFormaCaptura().editaForma();
    }
    
    this.opcionListaSeleccionadaFlotante = function(){
        this.mostrando_forma = 1;
        this.getContenedorModal().setComponente(this.nombre_instancia+".getContenedorModal()");
        
        if(this.tamanio_componente_modal = "s") this.getContenedorModal().muestraComponenteSmall();
        else this.getContenedorModal().muestraComponente();
        
        this.getFormaCaptura().setComponente(this.getContenedorModal().getZonaContenido(), this.nombre_instancia+".getFormaCaptura()");
        this.getFormaCaptura().muestraComponente();
    }
    
    this.nuevoElemento = function(){
        this.mostrando_forma = 1;
        
        this.showControladorNavegacion("Regresar", this.nombre_instancia+".muestraComponente()");
        this.getFormaCaptura().setComponente(this.getControladorNavegacion().getContenedorDeContenido(), this.nombre_instancia+".getFormaCaptura()");
        this.getFormaCaptura().muestraComponente();
        this.getFormaCaptura().setControladorNavegacion(this.getControladorNavegacion());
        this.getListaSeleccionable().setPosicionElementoSeleccionado(-1);
    }
    
    this.nuevoElementoFlotante = function(){
        this.mostrando_forma = 1;
        this.getContenedorModal().setComponente(this.nombre_instancia+".getContenedorModal()");
        
        if(this.tamanio_componente_modal = "s") this.getContenedorModal().muestraComponenteSmall();
        else this.getContenedorModal().muestraComponente();
        
        this.getFormaCaptura().setComponente(this.getContenedorModal().getContenedorDeContenido(), this.nombre_instancia+".getFormaCaptura()");
        this.getFormaCaptura().muestraComponente();
        this.getListaSeleccionable().setPosicionElementoSeleccionado(-1);
    }
    
    this.deshabilitaOpcionBorrar = function(){
        this.getListaSeleccionable().setOpcionBorrar(0);
        this.muestraElementosDelCatalogo();
    }
    
    this.getElementoSeleccionado = function(){
        return this.getListaSeleccionable().getDatosDeElementoSeleccionado();
    }
    
    this.getTituloCatalogo = function(){
        return document.getElementById(this.nombre_instancia+"_titulo_catalogo");
    }
    
    this.respuestaEliminado = function(respuesta){
        if(respuesta == 1){
            alert("Elemento eliminado");
            this.getListaSeleccionable().quitaElementoDeListaPorId(this.getListaSeleccionable().getIdElementoEliminado());
        }else alert("No se pudo eliminar el elemento, reportalo a informatica");
    }
    
    //esta función recupera los datos del elemento seleccionado
    this.getDatosDeElementoSeleccionadoDeCatalogo = function(){
        try{
            return this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        }catch(error){
            return null;
        }
    }
    
    this.soloSeleccion = 0;
    this.setSoloSeleccion = function(valor){
        this.soloSeleccion = valor;
    }
    
    //se sobreescribe esta función para realizar la búsqueda
    this.buscar = function(){
        var terminoBusqueda = this.getBuscador().getTerminoBusqueda();
        if(!terminoBusqueda) return;
        
        //realizamos la búsqueda sobre los elementos para mostrar unicamente aquellos que cumplen
        this.muestraElementosDelCatalogo();
    }
    
    this.funcionElementoSeleccionado = null;
    this.setFuncionElementoSeleccionado = function(funcion){
        this.funcionElementoSeleccionado = funcion;
    }
    
    //esta función está por defecto para avisar a la función dada cuando se seleccione una opción, se puede sobre escribir para un comportamiento diferente
    this.elementoSeleccionado = function(){
        if(this.funcionElementoSeleccionado) setTimeout(this.funcionElementoSeleccionado, 0);
    }
    
    this.getCadenaParaEnvio = function(vector){
        var cadena = "";
        for(var ii=0; ii<vector.length; ii++){
            if(ii == vector.length-1) cadena += vector[ii];
            else cadena += vector[ii]+"|";
        }
        return cadena;
    }
    
    //esta funcion recupera toda la informacion necesaria para realizar una impresión de los elementos visibles del catálogo, considerando las listas como tal, es decir, sus encabezados, sus medidas, sus alineaciones y los elementos visibles
    this.imprimeLista = function(opcion){
        //opcion 1 PDF y opcion 2 CSV
        
        //primero preparamos la informacion para enviarla, solo la info visible
        var encabezados = this.getListaSeleccionable().encabezados;
        
        //se toma la longitud del encabezado para el envio de la informacion, se truncan los elementos que no son columnas visibles
        var elementosParaEnvio = "";
        var elementosVisibles = this.getListaSeleccionable().getElementosVisiblesEnLista();
        
        var elemento;
        for(var i=0; i<elementosVisibles.length; i++){
            elementosParaEnvio += elementosVisibles[i]+"^";
        }
        
        var encabezadosParaEnvio = this.getCadenaParaEnvio(encabezados);
        var medidasParaEnvio = this.getCadenaParaEnvio(this.getListaSeleccionable().medidas);
        var alineacionParaEnvio = this.getCadenaParaEnvio(this.getListaSeleccionable().alineaciones);
        
        var paraEnvio = opcion+"<*>"+encabezadosParaEnvio+"<*>"+medidasParaEnvio+"<*>"+alineacionParaEnvio+"<*>"+elementosParaEnvio
        console.log("elementos para envio: "+paraEnvio);
        //paraEnvio = 
        
        var parametros = { "datos" : paraEnvio };
        $.ajax({
            data:  parametros, //datos que se envian a traves de ajax
            url:   'ms/generadorDeReportes.php', //archivo que recibe la peticion
            type:  'post', //método de envio
            beforeSend: function () {},
            success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
                console.log("datos en server para ser mostrados");
            
                //var contenido = "<iframe src='ms/generadorDeReportes.php' width='"+($( window ).width()*0.90)+"px;' height='"+($( window ).height()*0.90)+"px;' id='"+this.nombre_instancia+"_iframe_pdf'></iframe>";
                window.open('ms/generadorDeReportes.php', '_blank');

                //$.colorbox({html:contenido, transition:"elastic"});
            }
        });
        
        //opcion, encabezados, medidas, alineacion, elemento
        //getResultadoAjax("opcion=40&datos="+paraEnvio, this.nombre_instancia+".procesaRespuestaParaMostrarReporte", null);
    }
    
    this.procesaRespuestaParaMostrarReporte = function(respuesta){
        console.log("respuesta reporte: "+respuesta);
        if(respuesta == 1){
            console.log("datos en server para ser mostrados");
            
            var contenido = "<iframe src='ms/generadorDeReportes.php' width='"+($( window ).width()*0.90)+"px;' height='"+($( window ).height()*0.90)+"px;' id='"+this.nombre_instancia+"_iframe_pdf'></iframe>";
        
            $.colorbox({html:contenido, transition:"elastic"});
        }
    }
}