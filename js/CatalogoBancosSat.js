var CatalogoBancosSat = function(){
    
    this.bancos = null;
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Catálogo de Bancos SAT");
        this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo();
        
        //realizamos la búsqueda en la base de datos de pacientes
        if(!this.formasPago) getResultadoAjax("opcion=52", this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
        else this.muestraResultados(this.formasPago);
    }
    
    this.muestraResultados = function(respuesta){
        this.bancos = respuesta;
        console.log("bancos: "+respuesta);
        
        var bancos = respuesta.split("^");
        bancos.pop();
        if(bancos.length == 0){
            var contenido = "<div>No se encontró el catálogo de Bancos Sat</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(bancos);
        
        var encabezados = new Array("Clave", "RFC", "Nombre");
        var medidas = new Array("10%", "20%", "60%");
        var alineaciones = new Array("left", "left", "left");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, null, null);
        
        this.getListaSeleccionable().setOpcionBorrar(false);
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
}