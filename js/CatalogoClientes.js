var CatalogoClientes = function(){

    this.muestraComponente = function(){
        this.setTituloCatalogo("Clientes");
        this.setTextoAgregarElemento("Agregar nuevo cliente");
        
		this.setFormaCaptura(new FormaCliente);
        
        if(this.soloSeleccion == 0) this.muestraEstructuraCatalogoPrincipalConOpcionAgregarNuevo();
        else this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo();
        
        //realizamos la búsqueda en la base de datos de pacientes
        getResultadoAjax("opcion=18", this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
    }
    
    this.muestraResultados = function(respuesta){
        console.log("clientes: "+respuesta);
        var clientes = respuesta.split("^");
        clientes.pop();
        if(clientes.length == 0){
            var contenido = "<div>No hay clientes registrados en el sistema aún</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(clientes);
        
        var encabezados = new Array("Nombre", "R.F.C.");
        var medidas = new Array("45%", "45%");
        var alineaciones = new Array("left", "left");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, this.nombre_instancia+".eliminaElemento()", null);
        
        if(this.soloSeleccion == 0) this.getListaSeleccionable().setOpcionBorrar(true);
        else this.getListaSeleccionable().setOpcionBorrar(false);
        
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
        
        //mostramos las opciones de impresion
        if(this.soloSeleccion == 0) this.mostraOpcionesDeImpresion();
    }
    
    this.eliminaElemento = function(){
        //si llega aqui, es por que se confirmó la acción de eliminar el elemento
        var idelemento = this.getListaSeleccionable().getIdElementoEliminado();
        getResultadoAjax("opcion=24&idcliente="+idelemento, this.nombre_instancia+".procesaElementoEliminado", null);
    }
    
    this.procesaElementoEliminado = function(respuesta){
        console.log("respuesta eliminar: "+respuesta);
        if(respuesta == 1){
            alert("Cliente eliminado!");
            this.getListaSeleccionable().quitaElementoDeListaPorId(this.getListaSeleccionable().getIdElementoEliminado());
        }else{
            alert("No se pudo eliminar en este momento, por favor intenta mas tarde!");
        }
    }
    
    this.elementoSeleccionado = function(){
        
        console.log(this.funcionElementoSeleccionado);
        if(this.funcionElementoSeleccionado){
            setTimeout(this.funcionElementoSeleccionado, 0);
            return;
        }
        
        var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        
        var formaCliente = new FormaCliente();
        formaCliente.setIdCliente(datos[0]);
        this.setFormaCaptura(formaCliente);
        this.nuevoElemento();
        
        formaCliente.editaComponente();
    }
}