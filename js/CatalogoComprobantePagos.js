var CatalogoComprobantePagos = function(){
    
    this.rango_fechas = null;
    this.getRangoDeFechas = function(){
        if(!this.rango_fechas) this.rango_fechas = new SelectorRangoDeFecha();
        return this.rango_fechas;
    }
    
    /*VARIABLES Y MÉTODOS PARA EL SELECTOR DE CLIENTE*/
    this.selectorClientes = null;
    this.getSelectorClientes = function(){
        if(!this.selectorClientes){
            this.selectorClientes = new SelectorDeOpcion();
            this.selectorClientes.setVisualizacion(1);
        }
        return this.selectorClientes;
    }
    
    this.clienteSeleccionado = 0;
    this.setClienteSeleccionado = function(idcliente){
        this.clienteSeleccionado = idcliente;
    }
    
    this.catalogoClientes = null;
    this.getCatalogoClientes = function(){
        if(!this.catalogoClientes) {
            this.catalogoClientes = new CatalogoClientes();
            this.catalogoClientes.setSoloSeleccion(1);
        }
        return this.catalogoClientes;
    }
    /*VARIABLES Y MÉTODOS PARA EL SELECTOR DE CLIENTE*/
    
    /*VARIABLES Y MÉTODOS PARA EL SELECTOR DE EMPRESAS*/
    this.idEmpresaSeleccionada = 0;
    this.selectorEmpresa = null;
    this.getSelectorEmpresa = function(){
        if(!this.selectorEmpresa){
            this.selectorEmpresa = new SelectorDeOpcion();
            this.selectorEmpresa.setVisualizacion(1);
        }
        return this.selectorEmpresa;
    }
    
    this.catalogoEmpresas = null;
    this.getCatalogoEmpresas = function(){
        if(!this.catalogoEmpresas){
            this.catalogoEmpresas = new CatalogoEmpresas();
            this.catalogoEmpresas.setSoloSeleccion(1);
        }
        return this.catalogoEmpresas;
    }
    /*VARIABLES Y MÉTODOS PARA EL SELECTOR DE EMPRESAS*/
    
    this.muestraComponente = function(){
        if(orbe.login.getPerfil() != 5){
            this.setTituloCatalogo("Pagos Recibidos");
            this.setTextoAgregarElemento("Agregar un nuevo pago recibido");

            this.setFormaCaptura(new FormaPagoRecibido());
            this.muestraEstructuraCatalogoPrincipalConBarraOpciones();

            var contenido = "";
            
            contenido += '<div class="row center-align">';
            contenido += '<div class="col s6 m6 l6 center-align">';

            contenido += "<div id='"+this.nombre_instancia+"_zona_empresas'></div>";

            contenido += '</div>';
            contenido += '<div class="col s6 m6 l6 center-align">';

            contenido += "<div id='"+this.nombre_instancia+"_zona_clientes'></div>";

            contenido += '</div>';
            contenido += '</div>';
            
            contenido += "<table align='center' width='100%' cellpadding='3' cellspacing='0'><tr>";

            contenido += "<td align='center' width='90%' valign='top'>";

            contenido += "<div id='"+this.nombre_instancia+"_zona_rango_fecha'></div>";

            contenido += "</td>";
            contenido += "<td align='center' width='10%' valign='bottom'>";

            contenido += "<div id='"+this.nombre_instancia+"_zona_boton_todos'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".getTodosLosElementos()' style='margin:0 auto 0 auto;'>Todos</a></div>";

            contenido += "</td>";
            contenido += "</tr></table><br/>";

            this.getZonaSecundaria().innerHTML = contenido;

            this.getRangoDeFechas().setComponente(document.getElementById(this.nombre_instancia+"_zona_rango_fecha"), this.nombre_instancia+".getRangoDeFechas()");
            this.getRangoDeFechas().setFuncionCambiaRango(this.nombre_instancia+".onDateRangeChange()");
            this.getRangoDeFechas().muestraComponente();

            this.getSelectorClientes().setComponente(document.getElementById(this.nombre_instancia+"_zona_clientes"), this.nombre_instancia+".getSelectorClientes()");
            if(this.clienteSeleccionado == 0) this.getSelectorClientes().setTextoDeOpcionSeleccionada("Todos los Clientes");
            this.getSelectorClientes().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoDeClientes()");

            this.getSelectorClientes().muestraComponente();
            
            //selector empresa
            this.getSelectorEmpresa().setComponente(document.getElementById(this.nombre_instancia+"_zona_empresas"), this.nombre_instancia+".getSelectorEmpresa()");

            if(this.idEmpresaSeleccionada == 0) this.getSelectorEmpresa().setTextoDeOpcionSeleccionada("Todas las Empresas");
            this.getSelectorEmpresa().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoEmpresas()");

            this.getSelectorEmpresa().muestraComponente();

            //selector de clientes y catalogo de clientes
            this.getFormaCaptura().setCatalogoClientes(this.getCatalogoClientes());
            this.getFormaCaptura().setSelectorClientes(this.getSelectorClientes());
            
            //selector de empresas y catalogo de empresas
            this.getFormaCaptura().setCatalogoEmpresas(this.getCatalogoEmpresas());
            this.getFormaCaptura().setSelectorEmpresa(this.getSelectorEmpresa());
            
            this.getFormaCaptura().setComponentePadre(this);

            //mostramos las opciones de impresion
            if(this.soloSeleccion == 0) this.mostraOpcionesDeImpresion();
            
            this.onDateRangeChange();
        }else{
            this.setTituloCatalogo("Pagos Recibidos");
            this.muestraEstructuraCatalogoPrincipalConBarraOpcionesSinOpcionNuevo();

            var contenido = "<div id='"+this.nombre_instancia+"_zona_clientes'></div><br/><table align='center' width='100%' cellpadding='3' cellspacing='0'>";
            contenido += "<tr>";

            contenido += "<td align='center' width='90%' valign='top'>";

            contenido += "<div id='"+this.nombre_instancia+"_zona_rango_fecha'></div>";

            contenido += "</td>";
            contenido += "<td align='center' width='10%' valign='bottom'>";

            contenido += "<div id='"+this.nombre_instancia+"_zona_boton_todos'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".getTodosLosElementos()' style='margin:0 auto 0 auto;'>Todos</a></div>";

            contenido += "</td>";
            contenido += "</tr></table><br/>";

            this.getZonaSecundaria().innerHTML = contenido;

            this.getRangoDeFechas().setComponente(document.getElementById(this.nombre_instancia+"_zona_rango_fecha"), this.nombre_instancia+".getRangoDeFechas()");
            this.getRangoDeFechas().setFuncionCambiaRango(this.nombre_instancia+".onDateRangeChange()");
            this.getRangoDeFechas().muestraComponente();
            
            this.onDateRangeChange();

            //mostramos las opciones de impresion
            this.mostraOpcionesDeImpresion();
        }
    }
    
    this.getTodosLosElementos = function(){
        getResultadoAjax("opcion=55&fechaInicio=&fechaFinal=&idCliente=0&idEmpresa=0", this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
    }
    
    this.onDateRangeChange = function(){
        var fecha_inicio = this.getRangoDeFechas().getFechaInicio();
        var fecha_fin = this.getRangoDeFechas().getFechaFin();
        getResultadoAjax("opcion=55&fechaInicio="+fecha_inicio+"&fechaFinal="+fecha_fin+"&idCliente="+this.clienteSeleccionado+"&idEmpresa="+this.idEmpresaSeleccionada, this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
    }
    
    this.muestraResultados = function(respuesta){
        console.log("Pagos recibidos: "+respuesta);
        var pagos = respuesta.split("^");
        pagos.pop();
        if(pagos.length == 0){
            var contenido = "<div>No hay pagos recibidos registrados en el sistema aún</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(pagos);
        
        var encabezados = new Array("Empresa", "Cliente", "Fecha", "Hora", "Cantidad");
        var medidas = new Array("20%", "20%", "15%", "10%", "15%");
        var alineaciones = new Array("left", "left", "center", "center", "right");
        //idPagoRecibido, nombreEmpresa, nombreCliente, fecha, hora, cantidadPagada
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, this.nombre_instancia+".eliminaElemento()", null);
        
        if(this.soloSeleccion == 0) this.getListaSeleccionable().setOpcionBorrar(true);
        else this.getListaSeleccionable().setOpcionBorrar(false);
        
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
    
    this.eliminaElemento = function(){
        //si llega aqui, es por que se confirmó la acción de eliminar el elemento
        var idelemento = this.getListaSeleccionable().getIdElementoEliminado();
        getResultadoAjax("opcion=62&idComprobante="+idelemento, this.nombre_instancia+".procesaElementoEliminado", null);
    }
    
    this.procesaElementoEliminado = function(respuesta){
        console.log("respuesta eliminar: "+respuesta);
        if(respuesta == 1){
            alert("Comprobante de pago eliminado!");
            this.getListaSeleccionable().quitaElementoDeListaPorId(this.getListaSeleccionable().getIdElementoEliminado());
        }else{
            alert("No se pudo eliminar en este momento, por favor intenta mas tarde!");
        }
    }
    
    this.elementoSeleccionado = function(){
        console.log(this.funcionElementoSeleccionado);
        if(this.funcionElementoSeleccionado){
            setTimeout(this.funcionElementoSeleccionado, 0);
            return;
        }
        
        var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        
        var formaPagoRecibido = new FormaPagoRecibido();
        formaPagoRecibido.setIdPago(datos[0]);
        this.setFormaCaptura(formaPagoRecibido);
        this.nuevoElemento();
        
        formaPagoRecibido.editaForma();
    }
    
    /*METODOS PARA INVOCAR EL CATALOGO DE CLIENTES REGISTRADOS*/
    this.muestraCatalogoDeClientes = function(){
        this.getCatalogoClientes().setComponente(null, this.nombre_instancia+".getCatalogoClientes()");
        this.getCatalogoClientes().setContenedorFormaFlotante(1);
        this.getCatalogoClientes().muestraContenedorModal('G');
        this.getCatalogoClientes().controlador_navegacion = null;
        
        this.catalogoClientes.setFuncionElementoSeleccionado(this.nombre_instancia+".clienteSeleccionadoDeCatalogo()");
        this.getCatalogoClientes().setComponente(this.getCatalogoClientes().getContenedorModal().getContenedorDeContenido(), this.getCatalogoClientes().nombre_instancia);
        this.getCatalogoClientes().muestraComponente();
    }
    
    this.clienteSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoClientes().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorClientes().setTextoDeOpcionSeleccionada(datos[1]);
            this.clienteSeleccionado = datos[0];
            this.getCatalogoClientes().getContenedorModal().ocultaComponente();
            
            this.getFormaCaptura().setClienteSeleccionado(this.clienteSeleccionado);
            this.getFormaCaptura().setCatalogoClientes(this.getCatalogoClientes());
            this.getFormaCaptura().setSelectorClientes(this.getSelectorClientes());
            
            this.onDateRangeChange();
        }
    }
    
    /*FUNCIONES PARA CATALOGO DE EMPRESAS*/
    this.muestraCatalogoEmpresas = function(){
        console.log("mostrando catalogo de empresas");
        this.getCatalogoEmpresas().setComponente(null, this.nombre_instancia+".getCatalogoEmpresas()");
        this.getCatalogoEmpresas().setContenedorFormaFlotante(1);
        this.getCatalogoEmpresas().muestraContenedorModal('G');
        this.getCatalogoEmpresas().controlador_navegacion = null;
        
        this.getCatalogoEmpresas().setFuncionElementoSeleccionado(this.nombre_instancia+".empresaSeleccionadoDeCatalogo()");
        this.getCatalogoEmpresas().setComponente(this.getCatalogoEmpresas().getContenedorModal().getContenedorDeContenido(), this.getCatalogoEmpresas().nombre_instancia);
        this.getCatalogoEmpresas().muestraComponente();
    }
    
    this.empresaSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoEmpresas().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorEmpresa().setTextoDeOpcionSeleccionada(datos[1]);
            this.idEmpresaSeleccionada = datos[0];
            this.getCatalogoEmpresas().getContenedorModal().ocultaComponente();
            
            this.getFormaCaptura().setEmpresaSeleccionada(this.idEmpresaSeleccionada);
            this.getFormaCaptura().setCatalogoEmpresas(this.getCatalogoEmpresas());
            this.getFormaCaptura().setSelectorEmpresa(this.getSelectorEmpresa());
            
            this.onDateRangeChange();
        }
    }
}