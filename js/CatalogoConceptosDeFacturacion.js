var CatalogoConceptosDeFacturacion = function(){
    
    this.conceptos = null;
    this.idConceptoSeleccionado = 0;
    
    this.getIdConceptoSeleccionado = function(){
        return this.idConceptoSeleccionado;
    }
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Conceptos previamente usados en facturas");
        
        this.setTextoAgregarElemento("Agregar un nuevo concepto de facturación");
        
        var formaConcepto = new FormaConceptoDeFacturacion();
        formaConcepto.setFuncionAgregarConcepto(this.nombre_instancia+".conceptoSeleccionado()");
		this.setFormaCaptura(formaConcepto);
        
        this.muestraEstructuraCatalogoPrincipalConOpcionAgregarNuevo();
        
        //realizamos la búsqueda en la base de datos de pacientes
        getResultadoAjax("opcion=44", this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
    }
    
    this.muestraResultados = function(respuesta){
        console.log("conceptos facturacion: "+respuesta);
        
        var conceptos = respuesta.split("^");
        conceptos.pop();
        if(conceptos.length == 0){
            var contenido = "<div>No hay conceptos de facturación registrados en el sistema aún</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(conceptos);
        
        var encabezados = new Array("Clave Producto", "Clave Unidad", "Descripción", "Precio Unitario");
        var medidas = new Array("15%", "15%", "30%", "15%");
        var alineaciones = new Array("center","center","left","right");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, this.nombre_instancia+".eliminaElemento()", null);
        
        if(this.soloSeleccion == 0) this.getListaSeleccionable().setOpcionBorrar(true);
        else this.getListaSeleccionable().setOpcionBorrar(false);
        
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
    
    this.eliminaElemento = function(){
        //si llega aqui, es por que se confirmó la acción de eliminar el elemento
        var idelemento = this.getListaSeleccionable().getIdElementoEliminado();
        getResultadoAjax("opcion=61&idConcepto="+idelemento, this.nombre_instancia+".procesaElementoEliminado", null);
    }
    
    this.procesaElementoEliminado = function(respuesta){
        console.log("respuesta eliminar: "+respuesta);
        if(respuesta == 1){
            alert("Concepto de Facturación eliminado!");
            this.getListaSeleccionable().quitaElementoDeListaPorId(this.getListaSeleccionable().getIdElementoEliminado());
        }else{
            alert("No se pudo eliminar en este momento, por favor intenta mas tarde!");
        }
    }
    
    this.elementoSeleccionado = function(){
        var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        
        var formaConcepto = new FormaConceptoDeFacturacion();
        formaConcepto.setIdConceptoFacturacion(datos[0]);
        this.setFormaCaptura(formaConcepto);
        formaConcepto.setFuncionAgregarConcepto(this.nombre_instancia+".conceptoSeleccionado()");
        this.nuevoElemento();
        
        formaConcepto.editaComponente();
        
        this.idConceptoSeleccionado = datos[0];
    }
    
    this.conceptoSeleccionado = function(){
        console.log("concepto seleccionado");
        if(this.funcionElementoSeleccionado) setTimeout(this.funcionElementoSeleccionado, 0);
    }
    
    this.getDatosFormaDeCaptura = function(){
        return this.getFormaCaptura().infoConceptoFacturacion;
    }
}