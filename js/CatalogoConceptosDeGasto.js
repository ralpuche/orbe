var CatalogoConceptosDeGasto = function(){
    
    this.idGasto = 0;
    this.setIdGasto = function(idGasto){
        this.idGasto = idGasto;
    }
    
    this.catalogoConceptosDeFacturacion = null;
    this.getCatalogoConceptosDeFacturacion = function(){
        if(!this.catalogoConceptosDeFacturacion) this.catalogoConceptosDeFacturacion = new CatalogoConceptosDeFacturacion();
        return this.catalogoConceptosDeFacturacion;
    }
    
    //lista con los conceptos de gastos
    this.listaConceptosDeGasto = null;
    
    this.muestraComponente = function(){
        if(this.soloSeleccion == 0) this.setTituloCatalogo("Conceptos a facturar");
        else this.setTituloCatalogo("Conceptos a facturar");
        
        this.setTextoAgregarElemento("Agregar un concepto a la factura");
        
		this.setFormaCaptura(null);
        
        if(this.soloSeleccion == 0) this.muestraEstructuraCatalogoPrincipalConOpcionAgregarNuevo();
        else this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo();
        
        //realizamos la búsqueda en la base de datos de pacientes
        if(this.idGasto > 0) getResultadoAjax("opcion=47&idGasto="+this.idGasto, this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
    }
    
    //sobreescribimos el comportamiento para el botón nuevo
    this.nuevoElemento = function(){
        this.getCatalogoConceptosDeFacturacion().setComponente(null, this.nombre_instancia+".getCatalogoConceptosDeFacturacion()");
        this.getCatalogoConceptosDeFacturacion().muestraContenedorModal('G');
        this.getCatalogoConceptosDeFacturacion().controlador_navegacion = null;
        
        this.getCatalogoConceptosDeFacturacion().setFuncionElementoSeleccionado(this.nombre_instancia+".conceptoDeFacturacionSeleccionado()");
        this.getCatalogoConceptosDeFacturacion().setComponente(this.getCatalogoConceptosDeFacturacion().getContenedorModal().getContenedorDeContenido(), this.getCatalogoConceptosDeFacturacion().nombre_instancia);
        this.getCatalogoConceptosDeFacturacion().muestraComponente();
    }
    
    this.procesaConceptoAgregado = function(respuesta){
        console.log("respuesta concepto agregado: "+respuesta);
        
        this.muestraComponente();
    }
    
    this.conceptoDeFacturacionSeleccionado = function(){
        var idConcepto = this.getCatalogoConceptosDeFacturacion().getIdConceptoSeleccionado();
        
        //verificamos que no exista ya este concepto en la lista, si existe entonces no lo agregamos.
        if(this.listaConceptosDeGasto){
            var conceptos = this.listaConceptosDeGasto.split("^");
            conceptos.pop();
            var concepto;
            for(var i=0; i<conceptos.length; i++){
                concepto = conceptos[i].split("|");
                console.log("idconcepto: "+concepto[8]+" idConcepto: "+idConcepto);
                if(concepto[8] == idConcepto){
                    this.getCatalogoConceptosDeFacturacion().getContenedorModal().ocultaComponente();
                    return;
                }
            }   
        }
        
        //si ya está aquí, entonces quiere decir que no está repetido y entonces si deberá agregarlo a la lista de gastos de este concepto específico. De esta forma, aparecerá en los conceptos específicos de este gasto y quedará tambié en el católogo global de conceptos de facturación
        this.getCatalogoConceptosDeFacturacion().getContenedorModal().ocultaComponente();
        getResultadoAjax("opcion=46&idGasto="+this.idGasto+"&idConcepto="+idConcepto, this.nombre_instancia+".procesaConceptoAgregado", this.contenedor);
        
        /*
        //si ya está aquí, entonces quiere decir que no está repetido y entonces si deberá agregarlo
        console.log("datos: "+this.getCatalogoConceptosDeFacturacion().getDatosFormaDeCaptura());
        
        //idConceptoDeGasto, Unidad, Producto, Descripción, P. Unitario, Cantidad, Total, descuento, idConcepto
        //cuando se agregan, se agregan con 0 como identificador de concepto de gasto
        //2|50121500|H87|Lata de pescado|18.5999|3|0|1|Pieza|Pescado
        var datos = this.getCatalogoConceptosDeFacturacion().getDatosFormaDeCaptura().split("|");
        var nuevoConcepto = "0|"+datos[8]+"|"+datos[9]+"|"+datos[3]+"|"+datos[4]+"|"+datos[5]+"|$"+getCifraLegible((datos[4]*datos[5]) - datos[6])+"|"+datos[6]+"|"+datos[0]+"^";
        
        if(!this.listaConceptosDeGasto) this.listaConceptosDeGasto = "";
        this.listaConceptosDeGasto += nuevoConcepto;
        this.getCatalogoConceptosDeFacturacion().getContenedorModal().ocultaComponente();
        this.muestraResultados(this.listaConceptosDeGasto);*/
    }
    
    //mostramos el catálogo de conceptos de facturación de gastos
    this.muestraResultados = function(respuesta){
        console.log("conceptos facturacion de gasto: "+respuesta);
        ////idConcepto, nombreUnidad, nombreProducto, descripcion, precioUnitario, cantidad, descuento, total, idUnidad, idProducto
        
        var conceptos = respuesta.split("^");
        conceptos.pop();
        if(conceptos.length == 0){
            //if(!this.getZonaElementosDelCatalogo()) return;
            var contenido = "<div>No hay conceptos de facturación registrados para este gasto</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        var nuevosConceptos = "";
        var concepto = null;
        var tablaPrecio = "";
        for(var i=0; i<conceptos.length; i++){
            concepto = conceptos[i].split("|");
            
            tablaPrecio = "<table align='center' cellpadding='3' cellspacing='0'>"
            tablaPrecio += "<tr>";
            
            tablaPrecio += "<td with='50%' align='left'>P. Unitario:</td>";
            tablaPrecio += "<td with='50%' align='right'>$"+getCifraLegible(concepto[4])+"</td>";
            
            tablaPrecio += "</tr>";
            tablaPrecio += "<tr>";
            
            tablaPrecio += "<td with='50%' align='left'>Cantidad:</td>";
            tablaPrecio += "<td with='50%' align='right'>"+concepto[5]+"</td>";
            
            tablaPrecio += "</tr>";
            tablaPrecio += "<tr>";
            
            tablaPrecio += "<td with='50%' align='left'>Sub Total:</td>";
            tablaPrecio += "<td with='50%' align='right'>$"+getCifraLegible(concepto[4] * concepto[5])+"</td>";
            
            tablaPrecio += "</tr>";
            tablaPrecio += "<tr>";
            
            tablaPrecio += "<td with='50%' align='left'>I.V.A:</td>";
            tablaPrecio += "<td with='50%' align='right'>$"+getCifraLegible((concepto[4] * concepto[5])*0.16)+"</td>";
            
            tablaPrecio += "</tr>";
            tablaPrecio += "<tr>";
            
            tablaPrecio += "<td with='50%' align='left'>Descuento:</td>";
            tablaPrecio += "<td with='50%' align='right'>$"+getCifraLegible(concepto[6])+"</td>";
            
            tablaPrecio += "</tr>";
            tablaPrecio += "<tr>";
            
            tablaPrecio += "<td with='50%' align='left'>Total:</td>";
            tablaPrecio += "<td with='50%' align='right'>$"+getCifraLegible(((concepto[4] * concepto[5])*1.16)-concepto[6])+"</td>";
            
            tablaPrecio += "</tr>";
            tablaPrecio += "</table>";
            
            nuevosConceptos += concepto[0]+"|"+concepto[1]+"|"+concepto[2]+"|"+concepto[3]+"|"+tablaPrecio+"|"+concepto[8]+"|"+concepto[9]+"^";
        }
        
        conceptos = nuevosConceptos.split("^");
        conceptos.pop();
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(conceptos);
        
        var encabezados = new Array("Unidad", "Producto", "Descripción", "Precio");
        var medidas = new Array("10%", "20%", "35%", "15%");
        var alineaciones = new Array("left","left","left","right");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, this.nombre_instancia+".eliminaElemento()", null);
        
        this.getListaSeleccionable().setOpcionBorrar(true);
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
    
    this.recargaCatalogoConceptosDeFacturacion = function(){
        console.log("recargando catalogo de conceptos");
        this.muestraComponente();
    }
    
    this.elementoSeleccionado = function(){
        console.log("concepto de facturacion seleccionado");
        var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        
        var formaConcepto = new FormaConceptoDeFacturacion();
        this.setFormaCaptura(formaConcepto);
        
        formaConcepto.setComponente(null, this.nombre_instancia+".getFormaCaptura()");
        formaConcepto.muestraContenedorModal('G');
        formaConcepto.controlador_navegacion = null;
        
        if(datos[0] == 0){
            formaConcepto.setIdConceptoFacturacion(datos[8]);
            formaConcepto.setIdPrestado(true);
        }else formaConcepto.setIdConceptoFacturacion(datos[0]);
        
        formaConcepto.setIdGasto(this.idGasto);
        formaConcepto.setFuncionActualizarConcepto(this.nombre_instancia+".recargaCatalogoConceptosDeFacturacion()");
        formaConcepto.setComponente(formaConcepto.getContenedorModal().getContenedorDeContenido(), formaConcepto.nombre_instancia);
        formaConcepto.editaComponente();
    }
    
    this.eliminaElemento = function(){
        //si llega aqui, es por que se confirmó la acción de eliminar el elemento
        var idelemento = this.getListaSeleccionable().getIdElementoEliminado();
        getResultadoAjax("opcion=48&idConcepto="+idelemento, this.nombre_instancia+".procesaElementoEliminado", null);
    }
    
    this.procesaElementoEliminado = function(respuesta){
        console.log("respuesta eliminar: "+respuesta);
        if(respuesta == 1){
            alert("Concepto de Facturación eliminado!");
            this.getListaSeleccionable().quitaElementoDeListaPorId(this.getListaSeleccionable().getIdElementoEliminado());
        }else{
            alert("No se pudo eliminar en este momento, por favor intenta mas tarde!");
        }
    }
}