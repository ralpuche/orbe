var CatalogoEmpresas = function(){
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Empresas");
        this.setTextoAgregarElemento("Agregar nueva empresa");
        
		this.setFormaCaptura(new FormaEmpresa);
        
        if(this.soloSeleccion == 0) this.muestraEstructuraCatalogoPrincipalConOpcionAgregarNuevo();
        else this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo();
        
        //realizamos la búsqueda en la base de datos de pacientes
        getResultadoAjax("opcion=14", this.nombre_instancia+".muestraResultados", document.getElementById(this.nombre_instancia+"_zona_boton_buscar"));
        
        //mostramos las opciones de impresion
        if(this.soloSeleccion == 0) this.mostraOpcionesDeImpresion();
    }
    
    this.muestraResultados = function(respuesta){
        console.log("empresas: "+respuesta);
        var empresas = respuesta.split("^");
        empresas.pop();
        if(empresas.length == 0){
            var contenido = "<div>No hay empresas registradas en el sistema aún</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(empresas);
        
        var encabezados = new Array("Nombre");
        var medidas = new Array("90%");
        var alineaciones = new Array("left");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, this.nombre_instancia+".eliminaElemento()", null);
        
        if(this.soloSeleccion == 0) this.getListaSeleccionable().setOpcionBorrar(true);
        else this.getListaSeleccionable().setOpcionBorrar(false);
        
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
    
    this.eliminaElemento = function(){
        //si llega aqui, es por que se confirmó la acción de eliminar el elemento
        var idelemento = this.getListaSeleccionable().getIdElementoEliminado();
        getResultadoAjax("opcion=17&idempresa="+idelemento, this.nombre_instancia+".procesaElementoEliminado", null);
    }
    
    this.procesaElementoEliminado = function(respuesta){
        console.log("respuesta eliminar: "+respuesta);
        if(respuesta == 1){
            alert("Empresa eliminada!");
            this.getListaSeleccionable().quitaElementoDeListaPorId(this.getListaSeleccionable().getIdElementoEliminado());
        }else{
            alert("No se pudo eliminar en este momento, por favor intenta mas tarde!");
        }
    }
    
    this.elementoSeleccionado = function(){
        console.log(this.funcionElementoSeleccionado);
        if(this.funcionElementoSeleccionado){
            setTimeout(this.funcionElementoSeleccionado, 0);
            return;
        }
        
        var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        
        var formaEmpresa = new FormaEmpresa();
        formaEmpresa.setIdEmpresa(datos[0]);
        this.setFormaCaptura(formaEmpresa);
        this.nuevoElemento();
        
        formaEmpresa.editaComponente();
    }
}