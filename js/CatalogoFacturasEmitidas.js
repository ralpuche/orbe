var CatalogoFacturasEmitidas = function(){
    
    //1 gasto de cliente
    //2 pago recibido
    this.tipoElemento = 1;
    
    this.setTipoElemento = function(tipo){
        this.tipoElemento = tipo;
    }
    
    this.idElemento = 0;
    
    this.setIdElemento = function(idElemento){
        this.idElemento = idElemento;
    }
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Facturas Emitidas");
        
        this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo();
        
        //realizamos la búsqueda en la base de datos de pacientes
        getResultadoAjax("opcion=56&idElemento="+this.idElemento+"&tipoElemento="+this.tipoElemento, this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
    }
    
    this.muestraResultados = function(respuesta){
        var facturas = respuesta.split("^");
        facturas.pop();
        if(facturas.length == 0){
            var contenido = "<div>No hay facturas emitidas</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        var nuevasFacturas = new Array();
        var factura;
        var divEstatus = "";
        for(var i=0; i<facturas.length; i++){
            factura = facturas[i].split("|");
            
            if(factura[5] == 1) divEstatus = "<div class='factura_activa blue white-text'>Activa</div>";
            else if(factura[5] == -2) divEstatus = "<div class='factura_cancelada red white-text'>Cancelada</div>";
            nuevasFacturas.push(factura[0]+"|"+factura[1]+"|"+factura[2]+"|"+factura[3]+"|"+factura[4]+"|"+divEstatus+"|"+factura[6]+"|"+factura[7]+"|"+factura[5]);
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(nuevasFacturas);
        //idFacturaEmitida, nombreUsuario, cantidadFacturada, fecha, hora, textoEstatus, tipoFactura, idElemento, estatus
        
        var encabezados = new Array("Facturado por", "Fecha", "Hora", "Cantidad", "Estatus");
        var medidas = new Array("40%", "15%", "10%", "15%", "10%");
        var alineaciones = new Array("left", "center", "center", "right", "center");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, null, null);
        
        this.getListaSeleccionable().setOpcionBorrar(false);
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
}