var CatalogoFacturasPendientes = function(){
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Facturas Pendientes");
        this.setTextoAgregarElemento("Agregar nuevo cliente");
        
		//this.setFormaCaptura(new FormaCliente);
        
        //if(this.soloSeleccion == 0) this.muestraEstructuraCatalogoPrincipalConOpcionAgregarNuevo();
        //else this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo();
        
        this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo();
        
        //realizamos la búsqueda en la base de datos de pacientes
        getResultadoAjax("opcion=37", this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
        
        //mostramos las opciones de impresion
        if(this.soloSeleccion == 0) this.mostraOpcionesDeImpresion();
    }
    
    this.procesaRespuestaEstatusDeGastoCambiado = function(respuesta){
        console.log("respuesta servidor: "+respuesta);
        if(respuesta != 1){
            alert("No se actualizó el estatus de este gasto, por favor vuelve a intentar!");
        }
    }
    
    this.cambiaEstatusDeFactura = function(idGasto, checkbox){
        getResultadoAjax("opcion=38&idGasto="+idGasto+"&estatus="+((checkbox.checked)?2:1), this.nombre_instancia+".procesaRespuestaEstatusDeGastoCambiado", null);
    }
    
    this.muestraResultados = function(respuesta){
        //idGastoCliente, nombrePromotor, nombreEmpresa, nombreCliente, nombreBanco, fecha, hora, total, porcentaje, estatus
        console.log("por facturar: "+respuesta);
        var porFacturar = respuesta.split("^");
        porFacturar.pop();
        if(porFacturar.length == 0){
            var contenido = "<div>No hay gastos pendientes por facturar aún</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        //agregamos el selector para marcar si ya fue facturada
        var nuevosGastos = "";
        var gasto;
        var selectorFactura = "";
        
        for(var i=0; i<porFacturar.length; i++){
            gasto = porFacturar[i].split("|");
            
            contenido = '<div class="switch">';
            contenido += '<label>';
            contenido += '<input type="checkbox" onclick="'+this.nombre_instancia+'.cambiaEstatusDeFactura('+gasto[0]+', this)">';
            contenido += '<span class="lever"></span>';
            contenido += '</label></div>';
            
            nuevosGastos += gasto[0]+"|"+gasto[1]+"|"+gasto[2]+"|"+gasto[3]+"|"+gasto[4]+"|"+gasto[5]+"|"+contenido+"^";
        }
        
        var gastos = nuevosGastos.split("^");
        gastos.pop();
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(gastos);
        
        var encabezados = new Array("Empresa", "Cliente", "Banco", "Fecha", "Cantidad", "Facturada");
        var medidas = new Array("15%", "15%", "15%", "15%", "10%", "10%");
        var alineaciones = new Array("left", "left","left", "center","right", "center");
        //"Empresa", "Cliente", "Banco", "Fecha", "Cantidad", "Porcentaje", "Estatus"
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, this.nombre_instancia+".eliminaElemento()", null);
        
        //if(this.soloSeleccion == 0) this.getListaSeleccionable().setOpcionBorrar(true);
        //else this.getListaSeleccionable().setOpcionBorrar(false);
        this.getListaSeleccionable().setOpcionBorrar(false);
        this.getListaSeleccionable().setIndiceDeColumnaSinEventoOnclick(5);
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
    
    this.elementoSeleccionado = function(){
        var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        
        var formaGasto = new FormaGastos();
        formaGasto.setIdGasto(datos[0]);
        this.setFormaCaptura(formaGasto);
        this.nuevoElemento();
        
        formaGasto.editaComponente();
    }
}