var CatalogoGastos = function(){
    
    this.rango_fechas = null;
    this.getRangoDeFechas = function(){
        if(!this.rango_fechas) this.rango_fechas = new SelectorRangoDeFecha();
        return this.rango_fechas;
    }
    
    this.selectorClientes = null;
    this.getSelectorClientes = function(){
        if(!this.selectorClientes){
            this.selectorClientes = new SelectorDeOpcion();
            this.selectorClientes.setVisualizacion(1);
        }
        return this.selectorClientes;
    }
    
    this.clienteSeleccionado = 0;
    this.setClienteSeleccionado = function(idcliente){
        this.clienteSeleccionado = idcliente;
    }
    
    this.catalogoClientes = null;
    this.getCatalogoClientes = function(){
        if(!this.catalogoClientes) {
            this.catalogoClientes = new CatalogoClientes();
            this.catalogoClientes.setSoloSeleccion(1);
        }
        return this.catalogoClientes;
    }
    
    this.muestraComponente = function(){
        if(orbe.login.getPerfil() != 5){
            this.setTituloCatalogo("Gastos De Clientes");
            this.setTextoAgregarElemento("Agregar nuevo gasto de cliente");

            this.setFormaCaptura(new FormaGastos());
            this.muestraEstructuraCatalogoPrincipalConBarraOpciones();

            var contenido = "<div id='"+this.nombre_instancia+"_zona_clientes'></div><br/><table align='center' width='100%' cellpadding='3' cellspacing='0'>";
            contenido += "<tr>";

            contenido += "<td align='center' width='90%' valign='top'>";

            contenido += "<div id='"+this.nombre_instancia+"_zona_rango_fecha'></div>";

            contenido += "</td>";
            contenido += "<td align='center' width='10%' valign='bottom'>";

            contenido += "<div id='"+this.nombre_instancia+"_zona_boton_todos'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".getTodosLosElementos()' style='margin:0 auto 0 auto;'>Todos</a></div>";

            contenido += "</td>";
            contenido += "</tr></table><br/>";

            this.getZonaSecundaria().innerHTML = contenido;

            this.getRangoDeFechas().setComponente(document.getElementById(this.nombre_instancia+"_zona_rango_fecha"), this.nombre_instancia+".getRangoDeFechas()");
            this.getRangoDeFechas().setFuncionCambiaRango(this.nombre_instancia+".onDateRangeChange()");
            this.getRangoDeFechas().muestraComponente();

            this.getSelectorClientes().setComponente(document.getElementById(this.nombre_instancia+"_zona_clientes"), this.nombre_instancia+".getSelectorClientes()");
            if(this.clienteSeleccionado == 0) this.getSelectorClientes().setTextoDeOpcionSeleccionada("Todos los clientes");
            this.getSelectorClientes().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoDeClientes()");

            this.getSelectorClientes().muestraComponente();
            //else this.getSelectorClientes().setIdConceptoSeleccionado(this.clienteSeleccionado);

            this.onDateRangeChange();

            this.getFormaCaptura().setClienteSeleccionado(this.clienteSeleccionado);
            this.getFormaCaptura().setCatalogoClientes(this.getCatalogoClientes());
            this.getFormaCaptura().setSelectorClientes(this.getSelectorClientes());
            this.getFormaCaptura().setComponentePadre(this);

            //mostramos las opciones de impresion
            if(this.soloSeleccion == 0) this.mostraOpcionesDeImpresion();
        }else{
            this.setTituloCatalogo("Mis Gastos Registrados");
            this.muestraEstructuraCatalogoPrincipalConBarraOpcionesSinOpcionNuevo();

            var contenido = "<div id='"+this.nombre_instancia+"_zona_clientes'></div><br/><table align='center' width='100%' cellpadding='3' cellspacing='0'>";
            contenido += "<tr>";

            contenido += "<td align='center' width='90%' valign='top'>";

            contenido += "<div id='"+this.nombre_instancia+"_zona_rango_fecha'></div>";

            contenido += "</td>";
            contenido += "<td align='center' width='10%' valign='bottom'>";

            contenido += "<div id='"+this.nombre_instancia+"_zona_boton_todos'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".getTodosLosElementos()' style='margin:0 auto 0 auto;'>Todos</a></div>";

            contenido += "</td>";
            contenido += "</tr></table><br/>";

            this.getZonaSecundaria().innerHTML = contenido;

            this.getRangoDeFechas().setComponente(document.getElementById(this.nombre_instancia+"_zona_rango_fecha"), this.nombre_instancia+".getRangoDeFechas()");
            this.getRangoDeFechas().setFuncionCambiaRango(this.nombre_instancia+".onDateRangeChange()");
            this.getRangoDeFechas().muestraComponente();

            this.onDateRangeChange();

            //mostramos las opciones de impresion
            this.mostraOpcionesDeImpresion();
        }
    }
    
    this.getTodosLosElementos = function(){
        this.getRangoDeFechas().setFechaInicial("2017-01-01");
        this.getRangoDeFechas().setFechaFinal(getFechaActual());
        this.onDateRangeChange();
    }
    
    this.onDateRangeChange = function(){
        var fecha_inicio = this.getRangoDeFechas().getFechaInicio();
        var fecha_fin = this.getRangoDeFechas().getFechaFin();
        getResultadoAjax("opcion=33&fechaInicio="+fecha_inicio+"&fechaFinal="+fecha_fin+"&idcliente="+this.clienteSeleccionado, this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
    }
    
    this.muestraResultados = function(respuesta){
        //idGastoCliente, nombrePromotor, nombreEmpresa, nombreCliente, nombreBanco, fecha, hora, total, porcentaje, estatus
        console.log("gastos clientes: "+respuesta);
        var gastos = respuesta.split("^");
        gastos.pop();
        if(gastos.length == 0){
            var contenido = "<div>No hay gastos de clientes registrados en el sistema aún</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(gastos);
        
        var encabezados = new Array("Promotor", "Empresa", "Cliente", "Banco", "Fecha", "Cantidad");
        var medidas = new Array("15%", "15%", "15%", "15%", "10%", "10%");
        var alineaciones = new Array("left", "left", "left", "center", "center", "right");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        if(orbe.login.getPerfil() != 5){
            this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, this.nombre_instancia+".eliminaElemento()", null);
            this.getListaSeleccionable().setOpcionBorrar(true);
            this.getListaSeleccionable().setOpcionDetalle(false);
        }else{
            this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, null, null, null, null);
            this.getListaSeleccionable().setOpcionBorrar(false);
            this.getListaSeleccionable().setOpcionDetalle(false);
        }
        
        this.muestraElementosDelCatalogo();
    }
    
    this.elementoSeleccionado = function(){
        var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        
        var formaGasto = new FormaGastos();
        this.setFormaCaptura(formaGasto);
        this.nuevoElemento();
        
        formaGasto.setIdGasto(datos[0]);
        formaGasto.editaComponente();
    }
    
    this.eliminaElemento = function(){
        //si llega aqui, es por que se confirmó la acción de eliminar el elemento
        var idelemento = this.getListaSeleccionable().getIdElementoEliminado();
        getResultadoAjax("opcion=36&idgasto="+idelemento, this.nombre_instancia+".procesaElementoEliminado", null);
    }
    
    this.procesaElementoEliminado = function(respuesta){
        console.log("respuesta eliminar: "+respuesta);
        if(respuesta == 1){
            alert("Gasto de Cliente Eliminado!");
            this.getListaSeleccionable().quitaElementoDeListaPorId(this.getListaSeleccionable().getIdElementoEliminado());
        }else{
            alert("No se pudo eliminar en este momento, por favor intenta mas tarde!");
        }
    }
    
    /*
    METODOS PARA INVOCAR EL CATALOGO DE CLIENTES REGISTRADOS
    */
    this.muestraCatalogoDeClientes = function(){
        this.getCatalogoClientes().setComponente(null, this.nombre_instancia+".getCatalogoClientes()");
        this.getCatalogoClientes().setContenedorFormaFlotante(1);
        this.getCatalogoClientes().muestraContenedorModal('G');
        this.getCatalogoClientes().controlador_navegacion = null;
        
        this.catalogoClientes.setFuncionElementoSeleccionado(this.nombre_instancia+".clienteSeleccionadoDeCatalogo()");
        this.getCatalogoClientes().setComponente(this.getCatalogoClientes().getContenedorModal().getContenedorDeContenido(), this.getCatalogoClientes().nombre_instancia);
        this.getCatalogoClientes().muestraComponente();
    }
    
    this.clienteSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoClientes().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorClientes().setTextoDeOpcionSeleccionada(datos[1]);
            this.clienteSeleccionado = datos[0];
            this.getCatalogoClientes().getContenedorModal().ocultaComponente();
            this.onDateRangeChange();//recargamos el catalogo de gastos de clientes considerando el cliente seleccionado
            
            this.getFormaCaptura().setClienteSeleccionado(this.clienteSeleccionado);
            this.getFormaCaptura().setCatalogoClientes(this.getCatalogoClientes());
            this.getFormaCaptura().setSelectorClientes(this.getSelectorClientes());
        }
    }
}