var CatalogoGastosInternos = function(){
    
    this.rango_fechas = null;
    this.getRangoDeFechas = function(){
        if(!this.rango_fechas) this.rango_fechas = new SelectorRangoDeFecha();
        return this.rango_fechas;
    }
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Gastos Internos");
        this.setTextoAgregarElemento("Agregar nuevo gasto interno");
        
		this.setFormaCaptura(new FormaGastoInterno());
        this.muestraEstructuraCatalogoPrincipalConBarraOpciones();
        
        var contenido = "<table align='center' width='100%' cellpadding='3' cellspacing='0'>";
        contenido += "<tr>";
        
        contenido += "<td align='center' width='90%' valign='top'>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_rango_fecha'></div>";
        
        contenido += "</td>";
        contenido += "<td align='center' width='10%' valign='bottom'>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_boton_todos'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".getTodosLosElementos()' style='margin:0 auto 0 auto;'>Todos</a></div>";
        
        contenido += "</td>";
        contenido += "</tr></table><br/>";
        
        this.getZonaSecundaria().innerHTML = contenido;
        
        this.getRangoDeFechas().setComponente(document.getElementById(this.nombre_instancia+"_zona_rango_fecha"), this.nombre_instancia+".getRangoDeFechas()");
        this.getRangoDeFechas().setFuncionCambiaRango(this.nombre_instancia+".onDateRangeChange()");
        this.getRangoDeFechas().muestraComponente();
        
        this.onDateRangeChange();
        
        //mostramos las opciones de impresion
        if(this.soloSeleccion == 0) this.mostraOpcionesDeImpresion();
    }
    
    this.getTodosLosElementos = function(){
        this.getRangoDeFechas().setFechaInicial("2017-01-01");
        this.getRangoDeFechas().setFechaFinal(getFechaActual());
        this.onDateRangeChange();
    }
    
    this.onDateRangeChange = function(){
        var fecha_inicio = this.getRangoDeFechas().getFechaInicio();
        var fecha_fin = this.getRangoDeFechas().getFechaFin();
        
        getResultadoAjax("opcion=25&fechaInicio="+fecha_inicio+"&fechaFinal="+fecha_fin, this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
    }
    
    this.muestraResultados = function(respuesta){
        //idGastoInterno, concepto, fecha, hora, cantidad, usuario
        console.log("gastos internos: "+respuesta);
        var gastos = respuesta.split("^");
        gastos.pop();
        if(gastos.length == 0){
            var contenido = "<div>No hay gastos internos registrados en el sistema aún</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(gastos);
        
        var encabezados = new Array("Concepto", "Fecha", "Hora", "Usuario", "Cantidad");
        var medidas = new Array("20%", "15%", "15%", "20%", "10%");
        var alineaciones = new Array("left", "center", "center", "left", "right");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, this.nombre_instancia+".eliminaElemento()", null);
        this.getListaSeleccionable().setOpcionBorrar(true);
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
    
    this.elementoSeleccionado = function(){
        var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        
        var formaGastoInterno = new FormaGastoInterno();
        formaGastoInterno.setIdGastoInterno(datos[0]);
        this.setFormaCaptura(formaGastoInterno);
        this.nuevoElemento();
        
        formaGastoInterno.editaComponente();
    }
    
    this.eliminaElemento = function(){
        //si llega aqui, es por que se confirmó la acción de eliminar el elemento
        var idelemento = this.getListaSeleccionable().getIdElementoEliminado();
        getResultadoAjax("opcion=32&idgastointerno="+idelemento, this.nombre_instancia+".procesaElementoEliminado", null);
    }
    
    this.procesaElementoEliminado = function(respuesta){
        console.log("respuesta eliminar: "+respuesta);
        if(respuesta == 1){
            alert("Gasto Interno Eliminado!");
            this.getListaSeleccionable().quitaElementoDeListaPorId(this.getListaSeleccionable().getIdElementoEliminado());
        }else{
            alert("No se pudo eliminar en este momento, por favor intenta mas tarde!");
        }
    }
}