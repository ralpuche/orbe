var CatalogoMonedasSat = function(){
    
    this.monedas = null;
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Catálogo de Monedas SAT");
        this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo();
        
        //realizamos la búsqueda en la base de datos de pacientes
        if(!this.formasPago) getResultadoAjax("opcion=51", this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
        else this.muestraResultados(this.formasPago);
    }
    
    this.muestraResultados = function(respuesta){
        this.monedas = respuesta;
        console.log("monedas: "+respuesta);
        
        var monedas = respuesta.split("^");
        monedas.pop();
        if(monedas.length == 0){
            var contenido = "<div>No se encontró el catálogo de Monedas del Sat</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(monedas);
        
        var encabezados = new Array("Clave", "Nombre");
        var medidas = new Array("30%", "60%");
        var alineaciones = new Array("left", "left");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, null, null);
        
        this.getListaSeleccionable().setOpcionBorrar(false);
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
}