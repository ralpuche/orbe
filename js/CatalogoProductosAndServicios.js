var CatalogoProductosAndServicios = function(){
    
    this.listaProductosAndServicios = null;
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Catálogo de Productos y Servicios SAT");
        this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevoConBotonBuscar();
        
        //realizamos la búsqueda en la base de datos de pacientes
        if(!this.listaProductosAndServicios) getResultadoAjax("opcion=41", this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
        else this.muestraResultados(this.listaProductosAndServicios);
    }
    
    this.muestraResultados = function(respuesta){
        var productos = respuesta.split("^");
        productos.pop();
        if(productos.length == 0){
            var contenido = "<div>No se encontró el catálogo de productos y servicios de sat</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(productos);
        
        var encabezados = new Array("Clave", "Nombre");
        var medidas = new Array("30%", "60%");
        var alineaciones = new Array("left", "left");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, null, null);
        
        this.getListaSeleccionable().setOpcionBorrar(false);
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        //this.muestraElementosDelCatalogo();
        
        this.getZonaElementosDelCatalogo().innerHTML = "<b>"+productos.length+" Productos Cargados</b>";
    }
}