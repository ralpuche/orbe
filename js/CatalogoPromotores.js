var CatalogoPromotores = function(){
    
    this.rango_fechas = null;
    this.getRangoDeFechas = function(){
        if(!this.rango_fechas) this.rango_fechas = new SelectorRangoDeFecha();
        return this.rango_fechas;
    }
    
    //promotores
    this.idPromotorSeleccionado = 0;
    this.selectorPromotor = null;
    this.getSelectorPromotor = function(){
        if(!this.selectorPromotor){
            this.selectorPromotor = new SelectorDeOpcion();
            this.selectorPromotor.setVisualizacion(1);
        }
        return this.selectorPromotor;
    }
    
    this.catalogoPromotores = null;
    this.getCatalogoPromotores = function(){
        if(!this.catalogoPromotores){
            this.catalogoPromotores = new CatalogoUsuarios();
            this.catalogoPromotores.setSoloSeleccion(1);
            this.catalogoPromotores.setFiltroTipoUsuarios(3);
        }
        return this.catalogoPromotores;
    }
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Facturas de Promotores");
        this.muestraEstructuraCatalogoPrincipalConBarraOpcionesSinOpcionNuevo();
        
        var contenido = "<div id='"+this.nombre_instancia+"_zona_promotores'></div><br/><table align='center' width='100%' cellpadding='3' cellspacing='0'>";
        contenido += "<tr>";
        
        contenido += "<td align='center' width='90%' valign='top'>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_rango_fecha'></div>";
        
        contenido += "</td>";
        contenido += "<td align='center' width='10%' valign='bottom'>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_boton_todos'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".getTodosLosElementos()' style='margin:0 auto 0 auto;'>Todos</a></div>";
        
        contenido += "</td>";
        contenido += "</tr></table><br/>";
        
        this.getZonaSecundaria().innerHTML = contenido;        
        this.getRangoDeFechas().setComponente(document.getElementById(this.nombre_instancia+"_zona_rango_fecha"), this.nombre_instancia+".getRangoDeFechas()");
        this.getRangoDeFechas().setFuncionCambiaRango(this.nombre_instancia+".onDateRangeChange()");
        this.getRangoDeFechas().muestraComponente();
        
        //selector promotor
        if(orbe.login.getPerfil() != 3){
            this.getSelectorPromotor().setComponente(document.getElementById(this.nombre_instancia+"_zona_promotores"), this.nombre_instancia+".getSelectorPromotor()");
        
            this.getSelectorPromotor().setTextoDeOpcionSeleccionada("Selecciona Promotor");
            this.getSelectorPromotor().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoPromotores()");

            this.getSelectorPromotor().muestraComponente();   
        }
        
        this.onDateRangeChange();
        
        //mostramos las opciones de impresion
        if(this.soloSeleccion == 0) this.mostraOpcionesDeImpresion();
    }
    
    this.getTodosLosElementos = function(){
        this.getRangoDeFechas().setFechaInicial("2017-01-01");
        this.getRangoDeFechas().setFechaFinal(getFechaActual());
        this.onDateRangeChange();
    }
    
    this.onDateRangeChange = function(){
        var fecha_inicio = this.getRangoDeFechas().getFechaInicio();
        var fecha_fin = this.getRangoDeFechas().getFechaFin();
        getResultadoAjax("opcion=39&fechaInicio="+fecha_inicio+"&fechaFinal="+fecha_fin+"&idpromotor="+this.idPromotorSeleccionado, this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
    }
    
    /*
    FUNCIONES PARA MANEJA DE CATALOGO DE PROMOTORES
    */
    this.muestraCatalogoPromotores = function(){
        console.log("mostrando catalogo de promotores");
        this.getCatalogoPromotores().setComponente(null, this.nombre_instancia+".getCatalogoPromotores()");
        this.getCatalogoPromotores().setContenedorFormaFlotante(1);
        this.getCatalogoPromotores().muestraContenedorModal('G');
        this.getCatalogoPromotores().controlador_navegacion = null;
        
        this.getCatalogoPromotores().setFuncionElementoSeleccionado(this.nombre_instancia+".promotorSeleccionadoDeCatalogo()");
        this.getCatalogoPromotores().setComponente(this.getCatalogoPromotores().getContenedorModal().getContenedorDeContenido(), this.getCatalogoPromotores().nombre_instancia);
        this.getCatalogoPromotores().muestraComponente();
    }
    
    this.promotorSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoPromotores().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorPromotor().setTextoDeOpcionSeleccionada(datos[1]);
            this.idPromotorSeleccionado = datos[0];
            this.getCatalogoPromotores().getContenedorModal().ocultaComponente();
            this.onDateRangeChange();
        }
    }
    
    this.muestraResultados = function(respuesta){
        //idGastoCliente, nombrePromotor, nombreEmpresa, nombreCliente, nombreBanco, fecha, hora, total, porcentaje, estatus
        console.log("gastos promotores: "+respuesta);
        var gastos = respuesta.split("^");
        gastos.pop();
        if(gastos.length == 0){
            var contenido = "<div>No hay gastos para este promotor registrados en el sistema aún</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(gastos);
        
        //idGastoCliente, nombrePromotor, nombreCliente, nombreBanco, fecha, total, porcentaje, beneficio
        var encabezados = new Array("Promotor", "Cliente", "Banco", "Fecha", "Total", "Porcentaje", "Beneficio");
        var medidas = new Array("15%", "15%", "10%", "10%", "10%", "10%", "10%");
        var alineaciones = new Array("left", "left", "center", "right", "right", "right", "right");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, null, null, this.nombre_instancia+".eliminaElemento()", null);
        this.getListaSeleccionable().setOpcionBorrar(false);
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
}