var CatalogoRegimenFiscal = function(){
    
    this.infoRegimen = null;
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Catálogo de Régimen Fiscal");
        this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo();
        
        //realizamos la búsqueda en la base de datos de pacientes
        if(!this.unidades) getResultadoAjax("opcion=49", this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
        else this.muestraResultados(this.infoRegimen);
    }
    
    this.muestraResultados = function(respuesta){
        console.log("regimenes: "+respuesta);
        this.infoRegimen = respuesta;
        
        var regimenes = respuesta.split("^");
        regimenes.pop();
        if(regimenes.length == 0){
            var contenido = "<div>No se encontró el catálogo de régimen fiscal sat</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(regimenes);
        
        var encabezados = new Array("Clave", "Descripción");
        var medidas = new Array("15%", "85%");
        var alineaciones = new Array("left", "left");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, null, null);
        
        this.getListaSeleccionable().setOpcionBorrar(false);
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
        
        //this.getZonaElementosDelCatalogo().innerHTML = "<b>"+unidades.length+" Unidades Cargadas</b>";
    }

}