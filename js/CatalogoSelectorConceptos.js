var CatalogoSelectorConceptos = function(){
    
    this.contenedor_anterior = null;
    
    this.selector_opcion_principal = null;
    this.getSelectorDeOpcionPrincipal = function(){
        if(!this.selector_opcion_principal){
            this.selector_opcion_principal = new SelectorDeOpcion();
            this.selector_opcion_principal.setVisualizacion(2);
        }
        return this.selector_opcion_principal;
    }
    
    this.funcion = null;
    
    this.idConceptoSeleccionado = 0;
    
    this.getIdConceptoSeleccionado = function(){
        return this.idConceptoSeleccionado;
    }
    
    this.setIdConceptoSeleccionado = function(id){
        this.idConceptoSeleccionado = id;
        
        if(!this.listaConceptos){
            getResultadoAjax("opcion=26", this.nombre_instancia+".procesaDatosFacturacionRecuperadosParaMostrarSeleccionado", this.contenedor);
            return;
        }else this.muestraDatosDeFacturacionSeleccionados();
    }
    
    this.procesaDatosFacturacionRecuperadosParaMostrarSeleccionado = function(respuesta){
        this.listaConceptos = respuesta.split("^");
        this.listaConceptos.pop();
        
        this.muestraConceptoSeleccionado();
    }
    
    this.muestraConceptoSeleccionado = function(){
        var datos;
        for(var i=0; i<this.listaConceptos.length; i++){
            datos = this.listaConceptos[i].split("|");
            if(datos[0] == this.idConceptoSeleccionado){
                this.muestraComponenteSelector();
                this.setTextoParaMostrar(datos[1]);
                return;
            }
        }
        this.funcion = null;
        this.muestraComponenteSelector();
    }
    
    this.listaConceptos = null;
    
    this.tipoVisualizacion = 1;
    //el tipo de visualizacion 0 indica que es un componente que muestra su controlador de navegacion para mostrar el catálogo de conceptos. El tipo de visualización 1 indica que el catalogo de datos de facturacion se mostrará en un componente flotante.
    
    this.setTipoVisualizacion = function(tipo){
        this.tipoVisualizacion = tipo;
    }
    
    this.funcion_concepto_seleccionado = null;
    this.setFuncionConceptoSeleccionado = function(funcion){
        this.funcion_concepto_seleccionado = funcion;
    }
    
    this.muestraComponenteSelector = function(){
        if(this.contenedor_anterior) this.contenedor = this.contenedor_anterior;
        
        this.getSelectorDeOpcionPrincipal().setComponente(this.contenedor, this.nombre_instancia+".getSelectorDeOpcionPrincipal()");
        this.getSelectorDeOpcionPrincipal().setTextoDeOpcionSeleccionada("Selecciona un concepto");
        if(this.funcion) this.getSelectorDeOpcionPrincipal().setFuncionSelectorPresionado(this.funcion);
        else this.getSelectorDeOpcionPrincipal().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoDeConceptos()");
       
       if(this.idConceptoSeleccionado != 0){
           this.getSelectorDeOpcionPrincipal().setFuncionOpcionSelectorPresionado(this.nombre_instancia+".opcionSelectorPresionada()");
       }
        
        this.getSelectorDeOpcionPrincipal().muestraComponente();
    }
    
    this.procesaDatosRecuperados = function(respuesta){
        console.log("conceptos: "+respuesta);
        this.listaConceptos = respuesta.split("^");
        this.listaConceptos.pop();
        this.muestraCatalogoDeConceptos();
    }
    
    this.muestraComponente = function(){
        if(this.contenedor_anterior) this.contenedor = this.contenedor_anterior;
        this.muestraCatalogoDeConceptos();
    }
    
    this.muestraCatalogoDeConceptos = function(){
        if(!this.listaConceptos){
            getResultadoAjax("opcion=26", this.nombre_instancia+".procesaDatosRecuperados", null);
            return;
        }
        
        this.contenedor_anterior = this.contenedor;
        
        if(this.tipoVisualizacion == 0){
            /*this.getControladorNavegacion().setControladorNavegacion(this.contenedor, null, null, this.nombre_instancia+".catalogoCancelado();", this.nombre_instancia+".getControladorNavegacion()");
            this.getControladorNavegacion().muestraControladorNavegacionMedio();

            this.setComponente(this.getControladorNavegacion().getContenedorDeContenido(), this.nombre_instancia);   */
        }else{
            this.setContenedorFormaFlotante(1);
            this.muestraContenedorModal('G');
            this.controlador_navegacion = null;
            this.setComponente(this.getContenedorModal().getContenedorDeContenido(), this.nombre_instancia);
        }
        
        this.setTextoAgregarElemento("Agregar nuevo concepto");
        this.setTituloCatalogo("Conceptos de Gastos Internos");
        
        var formaConcepto = new FormaConcepto();
        formaConcepto.setComponentePadre(this);
        
		this.setFormaCaptura(formaConcepto);
        this.setContenedorFormaFlotante(0);
		this.muestraEstructuraCatalogoPrincipalConOpcionAgregarNuevo();
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(this.listaConceptos);
        
        var encabezados = new Array("Descripción");
        var medidas = new Array("90%");
        var alineaciones = new Array("left");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 1, 1, this.nombre_instancia+".setElementoSeleccionado()", this.nombre_instancia+".muestraElementoParaEdicion()", this.nombre_instancia+".eliminaElemento()", null);
        
        //this.getListaSeleccionable().setOpcionImpresora(0);
        this.getListaSeleccionable().setFuncionDetalleElementoSeleccionado(this.nombre_instancia+".muestraElementoParaEdicion()");
        this.muestraElementosDelCatalogo();
        
        //this.muestraComponenteSelector();
    }
    
    this.opcionSelectorPresionada = function(){
        this.muestraCatalogoDeConceptos();
        this.muestraElementoParaEdicion(this.idConceptoSeleccionado);
    }
    
    this.conceptoAgregado = function(info){
        this.listaConceptos = null;
        var datos = info.split("|");
        
        this.idConceptoSeleccionado = datos[0];
        this.muestraComponente();
        this.setTextoParaMostrar(datos[1]);
        
        //si se trata de un componente flotante, debemos cerrarlo
        //como ya se seleccionó un dato de facturación, entonces debemos cerrar la ventana flotante en caso de que se esté mostrando como componente flotante
        if(this.tipoVisualizacion == 1){
            this.getContenedorModal().ocultaComponente();
        }
        
        //invocamos la función dada para notificar cuando se cambió el recepctor de la factura
        if(this.funcion_concepto_seleccionado) setTimeout(this.funcion_concepto_seleccionado, 0);
    }
    
    this.setTextoParaMostrar = function(texto){
        this.getSelectorDeOpcionPrincipal().setTextoDeOpcionSeleccionada(texto);
        this.getSelectorDeOpcionPrincipal().muestraComponente();
    }
    
    this.setElementoSeleccionado = function(){
        //idConcepto, descripcion
        var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        
        this.idConceptoSeleccionado = datos[0];
        this.muestraComponenteSelector();
        this.setTextoParaMostrar(datos[1]);
        
        //como ya se seleccionó un dato de facturación, entonces debemos cerrar la ventana flotante en caso de que se esté mostrando como componente flotante
        if(this.tipoVisualizacion == 1){
            this.getContenedorModal().ocultaComponente();
        }
        
        //invocamos la función dada para notificar cuando se cambió el recepctor de la factura
        if(this.funcion_datos_facturacion_seleccionada) setTimeout(this.funcion_datos_facturacion_seleccionada, 0);
    }
    
    this.muestraElementoParaEdicion = function(iddatos){
        if(!iddatos || iddatos == 0){
            var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
            iddatos = datos[0];
        }
        
        this.getSelectorDeOpcion().getBotonAgregar().click();
        this.getFormaCaptura().setIdDatosCaptura(iddatos);
        this.getFormaCaptura().editaInfo();
    }
    
    this.eliminaElemento = function(){
        var idelemento = this.getListaSeleccionable().getIdElementoEliminado();
        getResultadoAjax("opcion=29&idconcepto="+idelemento, this.nombre_instancia+".procesaElementoEliminado", null);
    }
    
    this.procesaElementoEliminado = function(respuesta){
        console.log("respuesta eliminar: "+respuesta);
        if(respuesta == 1){
            alert("Concepto eliminado!");
            this.getListaSeleccionable().quitaElementoDeListaPorId(this.getListaSeleccionable().getIdElementoEliminado());
        }else{
            alert("No se pudo eliminar en este momento, por favor intenta mas tarde!");
        }
    }
}