var CatalogoSelectorDatosFacturacion = function(){
    
    this.iddatos_facturacion = 0;
    this.getIdDatosFacturacion = function(){
        return this.iddatos_facturacion;
    }
    
    this.tipoVisualizacion = 0;
    //el tipo de visualizacion 0 indica que es un componente que muestra su controlador de navegacion para mostrar el catálogo de datos de facturacion. El tipo de visualización 1 indica que el catalogo de datos de facturacion se mostrará en un componente flotante.
    
    this.setTipoVisualizacion = function(tipo){
        this.tipoVisualizacion = tipo;
    }
    
    this.forma_datos_facturacion = null;
    
    this.setIdDatosFacturacionParaSoloLectura = function(id){
        this.iddatos_facturacion = id;
        
        this.forma_datos_facturacion = new FormaDatosFacturacion();
        this.forma_datos_facturacion.setComponente(this.contenedor, this.nombre_instancia+".forma_datos_facturacion");
        this.forma_datos_facturacion.setIdDatosCaptura(this.iddatos_facturacion);
        this.forma_datos_facturacion.muestraInfoParaSoloLectura();
    }
    
    this.setIdDatosFacturacion = function(id){
        this.iddatos_facturacion = id;
        
        if(!this.lista_datos_facturacion){
            getResultadoAjax("opcion=20", this.nombre_instancia+".procesaDatosFacturacionRecuperadosParaMostrarSeleccionado", this.contenedor);
            return;
        }else this.muestraDatosDeFacturacionSeleccionados();
    }
    
    //esta función deshabilita el selector de datos de facturación, para no permitir cambiar el que ya esta seleccionado
    this.funcion = null;
    this.setFuncionSelectorPresionado = function(funcion){
        this.funcion = funcion;
    }
    
    //función que se invocará cuando los datos de facturación cambien
    this.funcion_datos_facturacion_seleccionada = null;
    this.setFuncionDatosFacturacionSeleccionada = function(funcion){
        this.funcion_datos_facturacion_seleccionada = funcion;
    }
    
    this.procesaDatosFacturacionRecuperadosParaMostrarSeleccionado = function(respuesta){
        this.lista_datos_facturacion = respuesta.split("^");
        this.lista_datos_facturacion.pop();
        
        this.muestraDatosDeFacturacionSeleccionados();
    }
    
    this.muestraDatosDeFacturacionSeleccionados = function(){
        var datos;
        for(var i=0; i<this.lista_datos_facturacion.length; i++){
            datos = this.lista_datos_facturacion[i].split("|");
            if(datos[0] == this.iddatos_facturacion){
                //this.getSelectorDeOpcionPrincipal().setFuncionSelectorPresionado(this.funcion);
                this.muestraComponenteSelector();
                this.setTextoParaMostrar(datos[1]+" - "+datos[2]);
                return;
            }
        }
        this.funcion = null;
        this.muestraComponenteSelector();
    }
    
    this.selector_opcion_principal = null;
    this.getSelectorDeOpcionPrincipal = function(){
        if(!this.selector_opcion_principal){
            this.selector_opcion_principal = new SelectorDeOpcion();
            this.selector_opcion_principal.setVisualizacion(2);
        }
        return this.selector_opcion_principal;
    }
    
    this.setTextoParaMostrar = function(texto){
        this.getSelectorDeOpcionPrincipal().setTextoDeOpcionSeleccionada(texto);
        this.getSelectorDeOpcionPrincipal().muestraComponente();
    }
    
    this.lista_datos_facturacion = null;
    
    this.contenedor_anterior = null;
    
    this.muestraComponenteSelector = function(){
        if(this.contenedor_anterior) this.contenedor = this.contenedor_anterior;
        
        this.getSelectorDeOpcionPrincipal().setComponente(this.contenedor, this.nombre_instancia+".getSelectorDeOpcionPrincipal()");
        this.getSelectorDeOpcionPrincipal().setTextoDeOpcionSeleccionada("Selecciona datos de facturaci&oacute;n");
        if(this.funcion) this.getSelectorDeOpcionPrincipal().setFuncionSelectorPresionado(this.funcion);
        else this.getSelectorDeOpcionPrincipal().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoDeDatosDeFacturacion()");
       
       if(this.iddatos_facturacion != 0) this.getSelectorDeOpcionPrincipal().setFuncionOpcionSelectorPresionado(this.nombre_instancia+".opcionSelectorPresionada("+this.iddatos_facturacion+")");
        this.getSelectorDeOpcionPrincipal().muestraComponente();
    }
    
    this.muestraComponente = function(){
        if(this.contenedor_anterior) this.contenedor = this.contenedor_anterior;
        this.muestraCatalogoDeDatosDeFacturacion();
    }
    
    this.catalogoCancelado = function(){
        if(this.iddatos_facturacion != 0) this.muestraDatosDeFacturacionSeleccionados();
    }
    
    this.procesaDatosFacturacionRecuperados = function(respuesta){
        console.log("datos facturacion: "+respuesta);
        this.lista_datos_facturacion = respuesta.split("^");
        this.lista_datos_facturacion.pop();
        this.muestraCatalogoDeDatosDeFacturacion();
    }
    
    this.muestraCatalogoDeDatosDeFacturacion = function(){
        if(!this.lista_datos_facturacion){
            getResultadoAjax("opcion=20", this.nombre_instancia+".procesaDatosFacturacionRecuperados", null);
            return;
        }
        
        this.contenedor_anterior = this.contenedor;
        
        if(this.tipoVisualizacion == 0){
            this.getControladorNavegacion().setControladorNavegacion(this.contenedor, null, null, this.nombre_instancia+".catalogoCancelado();", this.nombre_instancia+".getControladorNavegacion()");
            this.getControladorNavegacion().muestraControladorNavegacionMedio();

            this.setComponente(this.getControladorNavegacion().getContenedorDeContenido(), this.nombre_instancia);   
        }else{
            this.setContenedorFormaFlotante(1);
            this.muestraContenedorModal('G');
            this.controlador_navegacion = null;
            this.setComponente(this.getContenedorModal().getContenedorDeContenido(), this.nombre_instancia);
        }
        
        this.setTextoAgregarElemento("Agregar nuevo registro de facturación");
        
        //iddatosfacturacion, nombre, rfc, direccion
        this.setTituloCatalogo("Datos de Facturaci&oacute;n");
        
        var datos_facturacion = new FormaDatosFacturacion();
        datos_facturacion.setComponentePadre(this);
        
		this.setFormaCaptura(datos_facturacion);
        this.setContenedorFormaFlotante(0);
        //this.setTamanioComponenteModal("s");
        //this.setTipoTablaElementos(false);
		this.muestraEstructuraCatalogoPrincipalConOpcionAgregarNuevo();
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(this.lista_datos_facturacion);
        
        var encabezados = new Array("Razon social", "R.F.C.");
        var medidas = new Array("45%","40%");
        var alineaciones = new Array("left","left");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 1, 1, this.nombre_instancia+".datosFacturacionSeleccionados()", this.nombre_instancia+".muestraDatosDeFacturacionParaEdicion()", this.nombre_instancia+".eliminaDatosFacturacion()", null);
        
        //this.getListaSeleccionable().setOpcionImpresora(0);
        this.getListaSeleccionable().setFuncionDetalleElementoSeleccionado(this.nombre_instancia+".muestraDatosDeFacturacionParaEdicion()");
        this.muestraElementosDelCatalogo();
    }
    
    this.opcionSelectorPresionada = function(iddatos){
        this.muestraCatalogoDeDatosDeFacturacion();
        this.muestraDatosDeFacturacionParaEdicion(this.iddatos_facturacion);
    }
    
    this.muestraDatosDeFacturacionParaEdicion = function(iddatos){
        if(!iddatos || iddatos == 0){
            var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
            iddatos = datos[0];
        }
        
        this.getSelectorDeOpcion().getBotonAgregar().click();
        this.getFormaCaptura().setIdDatosCaptura(iddatos);
        this.getFormaCaptura().editaInfo();
    }
    
    this.eliminaDatosFacturacion = function(){
        var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        getResultadoAjax("opcion=200&iddatos_facturacion="+datos[0], this.nombre_instancia+".procesaDatosDeFacturacionEliminados", null);
    }
    
    this.procesaDatosDeFacturacionEliminados = function(respuesta){
        //console.log("datos eliminados: "+respuesta);
        if(!isNaN(respuesta) && respuesta == 1){
            this.getListaSeleccionable().quitaElementoDeListaPorPosicion(this.getListaSeleccionable().getPosicionElementoSeleccionado());
            this.lista_datos_facturacion = null;
        }else{
            alert("No se pudo eliminar el registro, intenta mas tarde!");
        }
    }
    
    this.datosFacturacionSeleccionados = function(){
        //iddatosfacturacion, razon social, rfc, direccion
        var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        
        this.iddatos_facturacion = datos[0];
        this.muestraComponenteSelector();
        this.setTextoParaMostrar(datos[1]+" - "+datos[2]);
        
        //como ya se seleccionó un dato de facturación, entonces debemos cerrar la ventana flotante en caso de que se esté mostrando como componente flotante
        if(this.tipoVisualizacion == 1){
            this.getContenedorModal().ocultaComponente();
        }
        
        //invocamos la función dada para notificar cuando se cambió el recepctor de la factura
        if(this.funcion_datos_facturacion_seleccionada) setTimeout(this.funcion_datos_facturacion_seleccionada, 0);
    }
    
    this.datosFacturacionAgregados = function(info){
        this.lista_datos_facturacion = null;
        var datos = info.split("|");
        
        this.iddatos_facturacion = datos[0];
        this.muestraComponente();
        this.setTextoParaMostrar(datos[1]+" - "+datos[2]);
        
        //si se trata de un componente flotante, debemos cerrarlo
        //como ya se seleccionó un dato de facturación, entonces debemos cerrar la ventana flotante en caso de que se esté mostrando como componente flotante
        if(this.tipoVisualizacion == 1){
            this.getContenedorModal().ocultaComponente();
        }
        
        //invocamos la función dada para notificar cuando se cambió el recepctor de la factura
        if(this.funcion_datos_facturacion_seleccionada) setTimeout(this.funcion_datos_facturacion_seleccionada, 0);
    }

}