var CatalogoUnidadesSat = function(){
    
    this.unidades = null;
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Catálogo de Unidades SAT");
        this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevoConBotonBuscar();
        
        //realizamos la búsqueda en la base de datos de pacientes
        if(!this.unidades) getResultadoAjax("opcion=42", this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
        else this.muestraResultados(this.unidades);
    }
    
    this.muestraResultados = function(respuesta){
        console.log("unidades: "+respuesta);
        
        var unidades = respuesta.split("^");
        unidades.pop();
        if(unidades.length == 0){
            var contenido = "<div>No se encontró el catálogo de unidades de sat</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(unidades);
        
        var encabezados = new Array("Clave", "Nombre");
        var medidas = new Array("30%", "60%");
        var alineaciones = new Array("left", "left");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, null, null);
        
        this.getListaSeleccionable().setOpcionBorrar(false);
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        //this.muestraElementosDelCatalogo();
        
        this.getZonaElementosDelCatalogo().innerHTML = "<b>"+unidades.length+" Unidades Cargadas</b>";
    }
}