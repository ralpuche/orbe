var CatalogoUsosCFDISat = function(){
    
    this.usos = null;
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Catálogo de Usos CFDI del SAT");
        this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo();
        
        //realizamos la búsqueda en la base de datos de pacientes
        if(!this.formasPago) getResultadoAjax("opcion=59", this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
        else this.muestraResultados(this.usos);
    }
    
    this.muestraResultados = function(respuesta){
        this.usos = respuesta;
        console.log("usos: "+respuesta);
        
        var usos = respuesta.split("^");
        usos.pop();
        if(usos.length == 0){
            var contenido = "<div>No se encontró el catálogo de Usos CFDI Sat</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(usos);
        
        var encabezados = new Array("Clave", "Uso");
        var medidas = new Array("20%", "60%");
        var alineaciones = new Array("center", "left");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, null, null);
        
        this.getListaSeleccionable().setOpcionBorrar(false);
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
}