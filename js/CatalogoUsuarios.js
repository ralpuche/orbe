var CatalogoUsuarios = function(){
    
    this.filtroTipoUsuarios = 0;
    this.setFiltroTipoUsuarios = function(tipo){
        this.filtroTipoUsuarios = tipo;
    }
    
    this.muestraComponente = function(){
        if(this.soloSeleccion == 0) this.setTituloCatalogo("Usuarios del Sistema");
        else this.setTituloCatalogo("Promotores");
        
        this.setTextoAgregarElemento("Agregar nuevo usuario");
        
		this.setFormaCaptura(new FormaUsuario);
        
        if(this.soloSeleccion == 0) this.muestraEstructuraCatalogoPrincipalConOpcionAgregarNuevo();
        else this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo();
        
        //realizamos la búsqueda en la base de datos de pacientes
        getResultadoAjax("opcion=6", this.nombre_instancia+".muestraResultados", document.getElementById(this.nombre_instancia+"_zona_boton_buscar"));
        
        //mostramos las opciones de impresion
        if(this.soloSeleccion == 0) this.mostraOpcionesDeImpresion();
    }
    
    this.muestraResultados = function(respuesta){
        var usuarios = respuesta.split("^");
        usuarios.pop();
        if(usuarios.length == 0){
            var contenido = "<div>No hay usuarios registrados en el sistema aún</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
        }
        
        if(this.filtroTipoUsuarios != 0){
            //idusuario, nombreCompleto, perfil, idTipoPerfil
            var usuariosFiltrados = new Array();
            var usuario;
            for(var i=0; i<usuarios.length; i++){
                usuario = usuarios[i].split("|");
                if(usuario[3] == this.filtroTipoUsuarios) usuariosFiltrados.push(usuarios[i]);
            }
            
            usuarios = usuariosFiltrados.slice(0);
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(usuarios);
        
        var encabezados = new Array("Nombre", "Perfil");
        var medidas = new Array("45%", "45%");
        var alineaciones = new Array("left","center");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, this.nombre_instancia+".eliminaElemento()", null);
        
        if(this.soloSeleccion == 0) this.getListaSeleccionable().setOpcionBorrar(true);
        else this.getListaSeleccionable().setOpcionBorrar(false);
        
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
    
    this.eliminaElemento = function(){
        console.log("eliminando elemento");
        //si llega aqui, es por que se confirmó la acción de eliminar el elemento
        var idelemento = this.getListaSeleccionable().getIdElementoEliminado();
        getResultadoAjax("opcion=9&idusuario="+idelemento, this.nombre_instancia+".procesaElementoEliminado", null);
    }
    
    this.procesaElementoEliminado = function(respuesta){
        console.log("respuesta eliminar: "+respuesta);
        if(respuesta == 1){
            alert("Usuario eliminado!");
            this.getListaSeleccionable().quitaElementoDeListaPorId(this.getListaSeleccionable().getIdElementoEliminado());
        }else{
            alert("No se pudo eliminar en este momento, por favor intenta mas tarde!");
        }
    }
    
    this.elementoSeleccionado = function(){
        console.log(this.funcionElementoSeleccionado);
        if(this.funcionElementoSeleccionado){
            setTimeout(this.funcionElementoSeleccionado, 0);
            return;
        }
        
        var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        
        var formaUsuario = new FormaUsuario();
        formaUsuario.setIdUsuario(datos[0]);
        this.setFormaCaptura(formaUsuario);
        this.nuevoElemento();
        
        formaUsuario.editaComponente();
    }
}