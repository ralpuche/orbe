// JavaScript Document - Objeto Buscador
var Buscador = function(){
	
	//contenedor del buscador
	this.contenedor_buscador;
	//id html del campo de buscador
	this.id_html_campo;
	//número de búsqueda
	this.numero_busqueda = 0;
	//este texto inicial es lo que se muestra cuando se hace click sobre el cuadro de texto
	this.texto_inicial = "Teclea algo para buscar";
	//esta es la acción que se ejecuta dependiendo del tipo de búsqueda
	this.funcion_busqueda;
	
	//esta variable determina el número de caracteres para iniciar la búsqueda, regresando la expresión en vez de null. Por defecto está en 3
	this.numero_caracteres_minimos = 3
	
	//esta función inicializa las variables del componente buscador
	this.setComponenteBuscador = function(contenedor, id_html, funcion){
		this.contenedor_buscador = contenedor;
		this.id_html_campo =  id_html;
		this.funcion_busqueda = funcion;
	}
	
	//función para setear el número de caracteres mínimos para empezar la búsqueda de información
	this.setNumeroDeCaracteresMinimos = function(numero){
		this.numero_caracteres_minimos = numero;
	}
	
	this.setFuncionBusqueda = function(funcion){
		this.funcion_busqueda = funcion;
	}
	
	//esta función muestra el campo buscador en el contenedor
	this.muestraCampoBuscadorInstant = function(){
		var contenido = "<input type='text' class='buscador' value='"+this.texto_inicial+"' id='"+this.id_html_campo+"' onfocus='limpiaContenido(\""+this.texto_inicial+"\", this)' onkeyup='"+this.funcion_busqueda+"' onblur='llenaContenido(\""+this.texto_inicial+"\", this)'/>";
		this.contenedor_buscador.innerHTML = contenido;
	}
	
	//esta función muestra el campo buscador en el contenedor
	this.muestraCampoBuscadorInstantPequenio = function(){
		var contenido = "<input type='text' class='buscador_pequenio' value='"+this.texto_inicial+"' id='"+this.id_html_campo+"' onfocus='limpiaContenido(\""+this.texto_inicial+"\", this)' onkeyup='"+this.funcion_busqueda+"' onblur='llenaContenido(\""+this.texto_inicial+"\", this)'/>";
		this.contenedor_buscador.innerHTML = contenido;
	}

	//esta función muestra el campo buscador en el contenedor, ocupando la mayor parte de la forma
	this.muestraCampoBuscadorInstantGrande = function(){
		var contenido = "<input type='text' class='buscador_grande' value='"+this.texto_inicial+"' id='"+this.id_html_campo+"' onfocus='limpiaContenido(\""+this.texto_inicial+"\", this)' onkeyup='"+this.funcion_busqueda+"' onblur='llenaContenido(\""+this.texto_inicial+"\", this)'/>";
		this.contenedor_buscador.innerHTML = contenido;
	}
    
    //esta función muestra el campo buscador grande, ocupando la mayoria de la forma. La búsqueda solo inicia cuando se invoca mediante un botón por ejemplo, ya que este no responde a los eventos onkeyup del teclado. Pensado para búsquedas muy tardadas
    this.muestraCampoBuscadorGrande = function(){
        var contenido = "<input type='text' class='buscador_grande' value='"+this.texto_inicial+"' id='"+this.id_html_campo+"' onfocus='limpiaContenido(\""+this.texto_inicial+"\", this)' onblur='llenaContenido(\""+this.texto_inicial+"\", this)'/>";
		this.contenedor_buscador.innerHTML = contenido;
    }
	
	//esta función es usada para limpiar el campo de búsqueda
	this.limpiaBuscador = function(){
		document.getElementById(this.id_html_campo).value = this.texto_inicial;
	}
    
    this.getCampoDeBusqueda = function(){
        return document.getElementById(this.id_html_campo);
    }
	
	//esta función devuelve una expresión para la búsqueda. Si el campo está vacio, devuelve 0
	this.getTerminoBusqueda = function(){
		if(document.getElementById(this.id_html_campo)) var palabras = document.getElementById(this.id_html_campo).value;
		else return 0;
		
		palabras.replace(/ /gi, "");
		if(palabras == this.texto_inicial) return 0;
		if(palabras == "" || palabras == " " || palabras.length < this.numero_caracteres_minimos) return 0;
		return new RegExp(palabras, "i");
	}
}