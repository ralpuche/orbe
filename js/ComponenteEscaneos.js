var ComponenteEscaneos = function(){
    
    this.elementos_escaneados = null;
    this.tipos_escaneos = null;
    this.idpropietario = 0;
    this.idtipopropietario = 0;
    
    /*
    idtipopropietario:
    1.- Gastos de cliente
    2.- Pagos recibidos de cliente
    */
    
    this.opcion_borrar = 1;
    
    this.tipo_escaneo_subido = 0;
    this.getTipoDeEscaneoSubido = function(){
        return this.tipo_escaneo_subido;
    }
    
    //esta variable almacena al controlador de subida del archivo
    this.controlador_subida_archivos = null;
    
    //controlador de navegaci—n
    this.controlador_navegacion = null;
    
    //funcion invocada cuando se agrega un escaneo
    this.funcion_escaneo_agregado = null;
    this.setFuncionEscaneoAgregado = function(funcion){
        this.funcion_escaneo_agregado = funcion;
    }
    
    //función incada cuando se elimina un elemento escaneado
    this.funcion_escaneo_eliminado = null;
    this.setFuncionEscaneoEliminado = function(funcion){
        this.funcion_escaneo_eliminado = funcion;
    }
    
    //función invocada cuando se cargan los escaneos
    this.funcion_escaneos_cargados = null;
    this.setFuncionEscaneosCargados = function(funcion){
        this.funcion_escaneos_cargados = funcion;
    }
    
    this.idtipoescaneo_eliminado = 0;
    this.getTipoDeEscaneoEliminado = function(){
        return this.idtipoescaneo_eliminado;
    }
    
    this.titulo = "- Facturas Emitidas";
    
    //esta funci—n sirve para saber cual es la funci—n que se deber‡ llamar cuando se cierre o se guarde un nuevo escaneo
    this.funcion_componente_escaneo = "setComponenteEscaneos()";
    
    this.setTitulo = function(titulo){
        this.titulo = titulo;
    }
    
    this.lista_tipos_escaneos_permitidos = null;
    this.setListaDeTiposDeEscaneosPermitidos = function(lista){
        this.lista_tipos_escaneos_permitidos = lista;
    }
    
    //NUEVAS FUNCIONES
    this.muestraComponente = function(){
        var contenido = '<div class="row center-align">';
        contenido += '<div class="col s12 m12 l12 center-align">';
        contenido += '<div class="card">';
        
        contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Facturas Emitidas</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_boton_agregar'></div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_contenido_escaneo'></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        contenido += '</div>';
        
        this.contenedor.innerHTML = contenido;
        
        //this.getEscaneosDeUnaPersona();
        //this.funcion_componente_escaneo = this.nombre_instancia+".setComponenteEscaneos()";
    }
    
    
    //NUEVAS FUNCIONES
    
    //esta funci—n muestra la interfaz para agregar un nuevo escaneo
    this.setComponenteEscaneos = function(){
        
    }
    
    this.setComponenteEscaneosParaExpediente = function(){
        
        var contenido = "<div class='titulo_componente' id='"+this.nombre_instancia+"_id_titulo_componente'>"+this.titulo+"</div><br/>";
	contenido += "<div class='blanco_redondo'><br/>";
	contenido += "<div class='azul' id='"+this.nombre_instancia+"_subtitulo_componente'>Lista de Archivos</div><br/><div class='separador_pk'></div><br/>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_escaneos'><table align='center' cellpadding='0' cellspacing='0' border='0' width='96%'><tr><td><div class='additem' onclick='"+this.nombre_instancia+".agregaEscaneo()'></div></td></tr></table><br/>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_contenido_escaneo'></div></div><br/>";
        
        this.contenedor.innerHTML = contenido;
        this.getEscaneosDeUnaPersona();
        
        this.funcion_componente_escaneo = this.nombre_instancia+".setComponenteEscaneosParaExpediente()";
    }
    
    //selector de estudios radiologicos en caso de que se elija ese tipo de estudio
    this.selector = null;
    this.getSelector = function(){
        if(!this.selector) this.selector = new SelectorOpcion();
        return this.selector;
    }
    
    this.setComponenteEscaneosParaHojaCostos = function(){
        var contenido = "<div class='titulo_componente' id='"+this.nombre_instancia+"_id_titulo_componente'>"+this.titulo+"</div><br/>";
        contenido += "<div class='contenedor_gris'><br/><div class='blanco_redondo'><br/>";
        contenido += "<div class='azul' id='"+this.nombre_instancia+"_subtitulo_componente'>Lista de Archivos</div><br/>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_escaneos'></div><br/>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_contenido_escaneo'></div><br/></div><br/></div>";
        
        this.contenedor.innerHTML = contenido;
        this.getEscaneosDeUnaPersona();
        
        this.getSelector().setComponente(document.getElementById(this.nombre_instancia+"_zona_escaneos"), this.nombre_instancia+".getSelector()");
        this.getSelector().setTextoOpcionHover("Agregar factura en pdf o xml");
        this.getSelector().setTextoOpcionSeleccionada("");
        this.getSelector().setFuncionAgregar(this.nombre_instancia+".agregaEscaneo()");
        this.getSelector().muestraComponente();
        
        this.funcion_componente_escaneo = this.nombre_instancia+".setComponenteEscaneosParaHojaCostos()";
    }
    
    this.setComponenteEscaneosParaSoloLectura = function(){
        var contenido = "<div class='titulo_componente' id='"+this.nombre_instancia+"_id_titulo_componente'>"+this.titulo+"</div>";
        contenido += "<div class='contenedor_gris'><br/><div class='blanco_redondo'><br/>";
        //contenido += "<div class='azul' id='"+this.nombre_instancia+"_subtitulo_componente'>Lista de Archivos</div><br/>";
        
        //contenido += "<div id='"+this.nombre_instancia+"_zona_escaneos'></div><br/>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_contenido_escaneo'></div><br/></div><br/></div>";
        
        this.contenedor.innerHTML = contenido;
        this.opcion_borrar = 0;
        this.getEscaneosDeUnaPersona();
        
        //this.funcion_componente_escaneo = this.nombre_instancia+".setComponenteEscaneosParaHojaCostos()";
    }
    
    //esta funci—n agrega un nuevo escaneo
    this.agregaEscaneo = function(){
        if(!this.controlador_navegacion) this.controlador_navegacion = new ControladorNavegacion();
        this.controlador_navegacion.setControladorNavegacion(document.getElementById(this.nombre_instancia+'_zona_escaneos'), null, null, this.funcion_componente_escaneo, this.nombre_instancia+".controlador_navegacion");
        
        var contenido = "<table align='center' width='96%' cellpadding='5' cellspacing='0' border='0'>";
	    contenido += "<tr>";
        contenido += "<td align='center' width='50%' valign='top'><div class='texto_campo'>Nombre <span class='azul'>*</span></div></td>";
        contenido += "<td align='center' width='50%' valign='top'><div class='texto_campo'>Tipo <span class='azul'>*</span></div></td>";
        contenido += "</tr>";
        
        contenido += "<tr>";
        contenido += "<td align='center' width='50%' valign='top'><input type='text' class='campo_forma' onfocus='muestraAyudaCampo(\"u1\", \"Escribe un nombre para el archivo\")' onblur='ocultaAyudaCampo(u1); this.value = this.value.toUpperCase(); "+this.nombre_instancia+".validaEscaneo();' onkeypress='return getComponenteValidacionTexto().esAlfaNumerico(event);' id='"+this.nombre_instancia+"_nombre_escaneo'/></td>";
        contenido += "<td align='center' width='50%' valign='top'><div id='"+this.nombre_instancia+"_zona_tipo_escaneo'></div></td>";
        contenido += "</tr>";
        contenido += "</table>";
        
        contenido += "<div class='ayuda_campos' id='u1'></div><br/>";
        
        contenido += '<div id="container"><div id="filelist"></div><br/><div id="zona_botones_archivos"></div></div><br/>';
        
        this.controlador_navegacion.muestraControladorNavegacionMedio();
        this.controlador_navegacion.getContenedorDeContenido().innerHTML = contenido;
        
        document.getElementById(this.nombre_instancia+'_subtitulo_componente').innerHTML = "Agregando nuevo archivo<br/>";
        
        //agregamos el tipo de documento escaneado
        getTiposEscaneos().inicializaComponente(document.getElementById(this.nombre_instancia+"_zona_tipo_escaneo"), "getTiposEscaneos()");
        getTiposEscaneos().setListaDeTiposDeEscaneosPermitidos(this.lista_tipos_escaneos_permitidos);
        getTiposEscaneos().setComboTiposDeEscaneos(this.nombre_instancia+"_combo_tipos_escaneos", this.nombre_instancia+".validaEscaneo()");
    }
    
    //esta funci—n modifica los par‡metros que se env’an a la forma que guarda la imagen en el servidor
    this.validaEscaneo = function(){
        //primero recuperamos el nombre del escaneo, ya que se deber‡ agregar a la forma de env’o de escaneo al servidor, aunque no se v‡lida que todos tengan un nombre. Este nombre es para el caso de que se requiera un formato especial, entonces si deber‡ proporcionar un nombre de escaneo v‡lido.
        var nombre = document.getElementById(this.nombre_instancia+'_nombre_escaneo').value;
        if(nombre == "" || nombre == " "){
            document.getElementById(this.nombre_instancia+'_nombre_escaneo').focus();
            return;
        }
        
        //recuperamos el tipo de escaneo
        var combo_escaneos = document.getElementById(this.nombre_instancia+"_combo_tipos_escaneos");
        //primero comprobamos que el combo no estŽ en "selecciona..." antes de mostrar la forma
        
        if (combo_escaneos.selectedIndex > 0) {
            
            document.getElementById("zona_botones_archivos").innerHTML = '<table align="center" cellpadding="10" cellspacing="10"><tr align="center"><td><div id="pickfiles" class="boton_normal">Seleccionar</div></td><td><div id="uploadfiles" class="boton_normal">Guardar</div></td></tr></table>';
            
            var forma = this;
            var idpropietario = this.idpropietario;
            var idtipopropietario = this.idtipopropietario;
            this.tipo_escaneo_subido = combo_escaneos.options[combo_escaneos.selectedIndex].value;
            
            //inicializamos el administrador de subida de archivos
            this.controlador_subida_archivos = new plupload.Uploader({
                runtimes : 'html5,html4,flash,silverlight,browserplus',
                browse_button : 'pickfiles',
                container : 'container',
                max_file_size : '200mb',
                url : "../MagicScripts/upload.php?idtipoescaneo="+(combo_escaneos.options[combo_escaneos.selectedIndex].value)+"&nombre="+nombre+"&idpropietario="+idpropietario+"&idtipopropietario="+idtipopropietario,
                flash_swf_url : 'uploadify/js/plupload.flash.swf',
                silverlight_xap_url : 'uploadify/js/plupload.silverlight.xap',
                filters : [],
            });
            
            var uploader = this.controlador_subida_archivos;
            
            this.controlador_subida_archivos.bind('Init', function(up, params) {
            });
            
            $('#uploadfiles').click(function(e) {
                uploader.start();
                e.preventDefault();
            });
            
            this.controlador_subida_archivos.init();
            this.controlador_subida_archivos.bind('FilesAdded', function(up, files) {
                $.each(files, function(i, file) {
                    $('#filelist').append('<div id="' + file.id + '">' +file.name + ' (' + plupload.formatSize(file.size) + ') <b></b>' +'</div>');});
                up.refresh(); // Reposition Flash/Silverlight
            });
            
            this.controlador_subida_archivos.bind('UploadProgress', function(up, file) {
                $('#' + file.id + " b").html(file.percent + "%");
            });
            
            this.controlador_subida_archivos.bind('Error', function(up, err) {
                $('#filelist').append("<div>Error: " + err.code +", Message: " + err.message +(err.file ? ", File: " + err.file.name : "") +"</div>");
                up.refresh(); // Reposition Flash/Silverlight
            });
            
            this.controlador_subida_archivos.bind('FileUploaded', function(up, file) {
                forma.elementos_escaneados = null;
                setTimeout(forma.funcion_componente_escaneo, 0);
                if(forma.funcion_escaneo_agregado) setTimeout(forma.funcion_escaneo_agregado, 0);
            });
        }
        else{
            document.getElementById("zona_botones_archivos").innerHTML = '';
            document.getElementById("filelist").innerHTML = '';
        }
    }
    
    //esta funci—n muestra los escaneos de una persona
    this.getEscaneosDeUnaPersona = function(){
        if (!this.elementos_escaneados) {
            this.getElementosEscaneadosDesdeDb(this.nombre_instancia+".getEscaneosDeUnaPersona()");
            return;
        }
        
        //aqui vamos a mostrar los escaneos que tenga un propietario asociados
        if (this.elementos_escaneados.length == 0) {
            //como no hay elementos escaneados, entonces solo mostramos eso
            document.getElementById(this.nombre_instancia+'_zona_contenido_escaneo').innerHTML = "<div>No hay archivos</div><br/>";
            return;
        }
        
        var contenido = "<table align='center' cellpadding='4' cellspacing='0' border='0' width='96%'>";
        contenido += "<tr class='fila_encabezados'><td>N&uacute;mero</td><td>Nombre</td><td>Tipo</td><td>Opci&oacute;n</td></tr>";
        
        var clase = "";
        var escaneo;
        var link_descarga = "";
        var opcion_elimina = "";
        for (var i=0; i<this.elementos_escaneados.length; i++) {
            
            escaneo = this.elementos_escaneados[i].split("|");
            
            if (i%2 == 0) clase = "fila_tabla_uno";
            else clase = "fila_tabla_dos";
            
            link_descarga = '<a href="javascript:void(0);" onclick="'+this.nombre_instancia+'.descargarEscaneo(\''+escaneo[5]+'\')"><div class="descargar_pk"></div></a>';
            
            if(this.opcion_borrar == 1) opcion_elimina = "<div class='delete_pk' onclick='"+this.nombre_instancia+".eliminaEscaneo("+i+")'></div>";
            else opcion_elimina = "";
            
            contenido += "<tr class='"+clase+"'><td onclick='"+this.nombre_instancia+".muestraImagenEscaneada(\""+escaneo[5]+"\")'>"+(i+1)+"</td><td onclick='"+this.nombre_instancia+".muestraImagenEscaneada(\""+escaneo[5]+"\")'>"+escaneo[4]+"</td><td onclick='"+this.nombre_instancia+".muestraImagenEscaneada(\""+escaneo[5]+"\")'>"+getTiposEscaneos().getNombreTipoEscaneoPorId(escaneo[3])+"</td><td align='center'><table align='center' cellpadding='5' cellspacing='0' border='0'><tr><td align='center'>"+opcion_elimina+"</td><td align='center'><div class='imprimir_pk' onclick='"+this.nombre_instancia+".imprimirEscaneo(\""+escaneo[5]+"\")'></div></td><td align='center'>"+link_descarga+"</td></tr></table></td></tr>";
        }
        
        contenido += "</table>";
        document.getElementById(this.nombre_instancia+'_zona_contenido_escaneo').innerHTML = contenido;
        
        //se invoca a la función a ejecutar cuando se cargan los escaneos
        if(this.funcion_escaneos_cargados) setTimeout(this.funcion_escaneos_cargados, 0);
    }
    
    this.imprimirEscaneo = function(path){
        var nombre_archivo = "../MagicScripts/Escaneos/"+this.idtipopropietario+"/"+path;
        var ventana = window.open(nombre_archivo);
	ventana.print();
    }
    
    this.descargarEscaneo = function(path){
        var nombre_archivo = "../MagicScripts/downloads.php?file=Escaneos/"+this.idtipopropietario+"/"+path;
        //console.log("ARCHIVO: "+nombre_archivo);
        var ventana = window.open(nombre_archivo);
    }
    
    //esta funci—n elimina un escaneo
    this.eliminaEscaneo = function(posicion){
        if(!confirm("Desea eliminar este escaneo?")) return;
        
        var escaneo = this.elementos_escaneados[posicion].split("|");
        //idescaneo, idpropietario, idtipopropietario, idtipoescaneo, nombre, path, estatus
        
        this.idtipoescaneo_eliminado = escaneo[3];
        
        getResultadoAjax("opcion=29&idescaneo="+escaneo[0], this.nombre_instancia+".procesaEscaneoEliminado", null);
    }
    
    this.procesaEscaneoEliminado = function(respuesta){
        //console.log("Respuesta escaneo eliminado: "+respuesta);
        if (respuesta == "1") {
            alert("Escaneo eliminado!");
            this.elementos_escaneados = null;
            this.getEscaneosDeUnaPersona();
            if(this.funcion_escaneo_eliminado) setTimeout(this.funcion_escaneo_eliminado, 0);
            return;
        }else{
            alert("Ocurrio un error al eliminar el escaneo, comprueba tu conectividad con el servidor");
            return;
        }
    }
    
    //funci—n invocada cuando se hace click sobre una imagen para mostrarla
    this.muestraImagenEscaneada = function(path){
        var partes_nombre = path.split(".");
        if(partes_nombre[1].toUpperCase() == "PDF"){
            var contenido = "<iframe src='../MagicScripts/Escaneos/"+this.idtipopropietario+"/"+path+"' width='"+($( window ).width()*0.60)+"' height='"+($( window ).height()*0.90)+"'></iframe>";
            $.colorbox({html:contenido, transition:"elastic"});
        }else{
            //creamos una imagen nueva con la informaci—n src y el path dado
            var nombre_archivo = "../MagicScripts/Escaneos/"+this.idtipopropietario+"/"+path;
            var newImg = new Image();
                newImg.onload = function() {

                getTamanioImagenProporcional(newImg);

                var contenido = "<img src='"+newImg.src+"' width='"+newImg.width+"' height='"+newImg.height+"'/>";
                $.colorbox({html:contenido, transition:"elastic"});
            }
            newImg.src = nombre_archivo;
        }
    }
    
    //esta funci—n recupera los elementos escaneados de una persona
    this.getElementosEscaneadosDesdeDb = function(callback){
        //recuperamos las veriables de idpropietario e idtipopropietario para recuperar los escaneos
        var idpropietario = this.idpropietario;
        var idtipopropietario = this.idtipopropietario;
        
        //ponemos la imagen de espera mientras se recuperan las imagenes escaneadas
        if (document.getElementById(this.nombre_instancia+'_zona_contenido_escaneo')) {
            document.getElementById(this.nombre_instancia+'_zona_contenido_escaneo').innerHTML = "<img src='../Imagenes/ajax-loader-gd.gif'/>";
        }
        
        var escaneos = this;
        var ajax = objetoAjax();
        ajax.open("POST", enlace, true);
        ajax.onreadystatechange=function() {
                if (ajax.readyState == 4){
                    if (ajax.responseText == "0") {
                        escaneos.elementos_escaneados = Array();
                    }else{
                        escaneos.elementos_escaneados = ajax.responseText.split("^");
                        escaneos.elementos_escaneados.pop();
                    }
                    setTimeout(callback, 0);
            }
        }
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("token="+login.getToken()+"&"+"opcion=28&idpropietario="+idpropietario+"&idtipopropietario="+idtipopropietario);
    }
}