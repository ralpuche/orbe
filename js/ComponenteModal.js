//Este componente muestra un elemento emergente y modal multi prop—sito
var ComponenteModal = function(){
    
    this.nombre_instancia = "";
    
    this.permiso_para_cerrar = true;
    this.setPermisoParaCerrar = function(permiso){
        this.permiso_para_cerrar = permiso;
        $.colorbox.setPermisoParaCerrarConOverlay(this.permiso_para_cerrar);
    }
    
    this.funcion_al_cerrar = null;
    this.setFuncionAlCerrar = function(funcion){
        this.funcion_al_cerrar = funcion;
        $.colorbox.setFuncionAlCerrar(this.funcion_al_cerrar);
    }
    
    this.muestra_boton_cerrar = true;
    this.setVisibilidadBotonCerrar = function(valor){
        this.muestra_boton_cerrar = valor;
        $.colorbox.setVisibilidadDeBotonCerrar(this.muestra_boton_cerrar);
    }
    
    this.alto = 0;
    this.ancho = 0;
    this.setDimensiones = function(alto, ancho){
        this.alto = alto;
        this.ancho = ancho;
    }
    
    //este componente no necesita tener un contenedor, ya que se muestra en pantalla completa
    this.setComponente = function(nombre_instancia){
        this.nombre_instancia = nombre_instancia;
    }
    
    this.muestraComponente = function(){
        var contenido = "<table align='center' width='600' cellpadding='0' cellspacing='0' border='0'>";
        contenido += "<tr valign='top'><td align='center'>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_contenido_modal' class='contenido_modal'></div>";
        
        contenido += "</td></tr>";
        contenido += "</table>";
        
        $.colorbox({html:contenido, transition:"elastic"});
    }
    
    this.muestraComponenteSmall = function(){
        var contenido = "<table align='center' width='500' cellpadding='0' cellspacing='0' border='0'>";
        contenido += "<tr valign='top'><td align='center'>";
        
        contenido += "<div style='width:"+($(window).width() * 0.45)+"px; height:"+($(window).height() * 0.90)+"px; overflow-x:hidden;' id='"+this.nombre_instancia+"_zona_contenido_modal'></div>";
        
        contenido += "</td></tr>";
        contenido += "</table>";
        
        $.colorbox({html:contenido, transition:"elastic"});
    }
    
    this.muestraComponenteMuyPequenio = function(){
        console.log("Mostrando este");
        var contenido = "<table align='center' width='300' cellpadding='0' cellspacing='0' border='0'>";
        contenido += "<tr valign='top'><td align='center'>";
        
        contenido += "<div style='height:"+($(window).height() * 0.6)+"px; width:300px; overflow-x:hidden;' id='"+this.nombre_instancia+"_zona_contenido_modal'></div>";
        
        contenido += "</td></tr>";
        contenido += "</table>";
        
        $.colorbox({html:contenido, transition:"elastic"});
    }
    
    this.muestraComponentePersonalizado = function(){
        console.log("Model personalizado");
        var contenido = "<table align='center' width='"+this.ancho+"' cellpadding='0' cellspacing='0' border='0'>";
        contenido += "<tr valign='top'><td align='center'>";
        
        contenido += "<div style='height:"+this.alto+"px; width:"+this.ancho+"px; overflow-x:hidden;' id='"+this.nombre_instancia+"_zona_contenido_modal'></div>";
        
        contenido += "</td></tr>";
        contenido += "</table>";
        
        $.colorbox({html:contenido, transition:"elastic"});
    }
    
    this.muestraComponenteGrande = function(){
        var contenido = "<div style='width:"+($(window).width() * 0.60)+"px; height:"+($(window).height() * 0.90)+"px; overflow-x:hidden; text-align:center; padding:6px;' id='"+this.nombre_instancia+"_zona_contenido_modal'></div>";
        
        $.colorbox({html:contenido, transition:"elastic"});
    }
    
    this.muestraComponenteHuge = function(){
        var contenido = "<div style='width:"+($(window).width() * 0.80)+"px; height:"+($(window).height() * 0.90)+"px; overflow-x:hidden; text-align:center; padding:6px;' id='"+this.nombre_instancia+"_zona_contenido_modal'></div>";
        
        $.colorbox({html:contenido, transition:"elastic"});
    }
    
    this.getContenedorDeContenido = function(){
        return document.getElementById(this.nombre_instancia+"_zona_contenido_modal");
    }
    
    this.ocultaComponente = function(){
        $.colorbox.close();
    }
}