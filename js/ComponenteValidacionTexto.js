var ComponenteValidacionTexto = function(){
	
	//esta variable almacena el la forma de recuperar el keycode dependiendo del navegador, con lo cual una sola llamada bastará para identificar dicho método. Posteriormente esta variable determinará el método a utilizar, con lo cual se evitarán llamadas a la función que recupera el keycode, quedando en su lugar solo una condicional para recuperar el keycode
	this.metodo_keycode = null;
	
	//este arreglo contiene la lista de los caracteres permitidos
	this.caracteres_permitidos = [8,13,32,37,39,42,44,45,46,47,91,93,164,165,209,241];
    
    //arreglo con los caracteres permitidos para cifras
    this.caracteres_permitidos_para_cifras = [46];
	
	//este arreglo contiene la lista de los caracteres prohibídos que se saltan la comprobación de los caracteres permitidos por alguna razón. estos se evalúan al final
	this.caracteres_prohibidos = [];
	
	this.flecha = false;
	
	//esta función identifica y recupera el valor keycode de la tecla presionada
	this.getKeyCode = function(e) {
        var keycode = null;
        if(window.event) {
            keycode = window.event.keyCode;
			this.metodo_keycode = 1;
        }else if(e) {
            keycode = e.which;
			this.metodo_keycode = 2;
        }
        return keycode;
    }
	
	//esta función determina regresa true si el valor keycode dado como parámetro es número, false en otro caso
	this.esNumero = function(keycode){
		if(keycode >= 48 && keycode <= 57) return true;
		return false;
	}
	
	//esta función regresa true si el valor keycode dado como parámetro es letra mayúscula, false en otro caso
	this.esLetraMayuscula = function(keycode){
		if(keycode >= 65 && keycode <= 90) return true;
		return false;
	}
	
	//esta función regresa true si el valor keycode dado como parámetro es uno de los caracteres permitidos, en otro caso false
	this.esCaracterPermitido = function(keycode){
		for(var i=0; i<this.caracteres_permitidos.length; i++) 
			if(keycode == this.caracteres_permitidos[i]) return true;
		return false;
	}
    
    //esta funcion devuelve true si el elemento dado pertenece al vector
    this.esCaracterPermitidoDeCifra = function(keycode){
        for(var i=0; i<this.caracteres_permitidos_para_cifras.length; i++) 
			if(keycode == this.caracteres_permitidos_para_cifras[i]) return true;
		return false;
    }
	
	//esta función regresa true si el valor del keycode dado como parámetro es una minúscula, en otro caso false
	this.esLetraMinuscula = function(keycode){
		if(keycode >= 97 && keycode <= 122) return true;
		return false;
	}
	
	this.esSoloNumero = function(evento){
		//se pregunta si ya se conoce el método de recuperación del keycode dependiendo del navegador. Si ya se conoce solo se recupera la información de la tecla con el método que determina la variable this.metodo_keycode
		if(evento.ctrlKey || evento.metaKey) return false;//para prevenir el copia y pega
		if(this.metodo_keycode == null) var keycode = this.getKeyCode(evento);
		else if(this.metodo_keycode == 1) var keycode = window.event.keyCode;
		else var keycode = evento.which;
		
		//solo número no permite los caracteres permitidos, solo admite del 0 - 9
		if(!this.esNumero(keycode) && !this.esCaracterPermitidoDeCifra(keycode)){
			return false;
		}
		return true;
	}
	
	this.esAlfaNumerico = function(evento){
		//se pregunta si ya se conoce el método de recuperación del keycode dependiendo del navegador. Si ya se conoce solo se recupera la información de la tecla con el método que determina la variable this.metodo_keycode
		if(evento.ctrlKey || evento.metaKey) return false;//para prevenir el copia y pega
		if(this.metodo_keycode == null) var keycode = this.getKeyCode(evento);
		else if(this.metodo_keycode == 1) var keycode = window.event.keyCode;
		else var keycode = evento.which;
		if(!this.esNumero(keycode) && !this.esLetraMayuscula(keycode) && !this.esLetraMinuscula(keycode) && !this.esCaracterPermitido(keycode)){
			return false;
		}
		return true;
	}
    
    this.tieneLongitudMaxima = function(input, maximo){
        if(input.value.length == maximo) return true;
        return false;
    }
}