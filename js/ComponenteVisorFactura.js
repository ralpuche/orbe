var ComponenteVisorFactura = function(){
    
    //1 visor de facturas global
    //2 viros de facturas de pago
    this.visualizacion = 1;
    this.setVisualizacion = function(valor){
        this.visualizacion = valor;
    }
    
    this.idFactura = 0;
    this.setIdFactura = function(idFactura){
        this.idFactura = idFactura;
    }
    
    //1 permite cambiar entre ambiente de prueba y ambiente de produccion
    //2 solo muestra ambiente de producción, ya que en teoría, así debe cargar las facturas que ya se tienen
    //3 solo muestra la opción enviar acuse para facturas canceladas
    this.modo = 1;
    this.setModo = function(valor){
        this.modo = valor;
    }
    
    this.uuid = null;
    this.setUUID = function(uuid){
        this.uuid = uuid;
    }
    
    this.uuid_cancelado = null;
    this.setUUIDCancelado = function(uuid){
        this.uuid_cancelado = uuid;
    }
    
    this.opcion_factura = 0;
    this.setOpcionTipoFactura = function(opcion){
        this.opcion_factura = opcion;
    }
    
    this.idElemento = 0;
    this.setIdElemento = function(idElemento){
        console.log("idelemento: "+idElemento);
        this.idElemento = idElemento;
    }
    
    this.funcion_facturas_actualizadas = null;
    this.funcionFacturasActualizadas = function(funcion){
        this.funcion_facturas_actualizadas = funcion;
    }
    
    this.muestraComponenteParaFacturasCanceladas = function(){
        
        this.getContenedorModal().setComponente(this.nombre_instancia+".getContenedorModal()"); 
        this.getContenedorModal().muestraComponenteHuge();
        
        var contenido = "<table align='center' cellpadding='6' cellspacing='0' border='0' width='100%'>";
        contenido += "<tr>";
        contenido += "<td align='left' valign='top' width='50%'>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_info_factura'></div>";
        
        contenido += "</td>";
        contenido += "<td align='left' valign='top' width='25%'>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_envio'></div>";
        
        contenido += "</td><td align='left' valign='top' width='25%'>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_cancelacion'></div>";
        
        contenido += "</td></tr></table>";
        
        contenido += "</div>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_impresion'></div>";
        
        this.getContenedorModal().getZonaContenido().style = "overflow:hidden;";
        this.getContenedorModal().getZonaContenido().innerHTML = contenido;
        
        var tipo_propietario = 100;
        if(this.visualizacion != 1) tipo_propietario = 101;
        
        var url = "../MagicScripts/Escaneos/"+tipo_propietario+"/"+this.uuid_cancelado+"_cdfi_cancelado.pdf";
        console.log("url: "+url);
        document.getElementById(this.nombre_instancia+"_zona_impresion").innerHTML = "<iframe src='"+url+"' width='99%' height='"+($( window ).height()*0.83)+"' id='"+this.nombre_instancia+"_iframe_pdf'><div style='background-image:url(http://sistema.hospitalmay.com/imagenes/ajax-loader-gd.gif);width:32px;height:32px;background-repeat:no-repeat;'></div></iframe>";
        
        this.getInfoFactura();
    }
    
    this.muestraComponenteParaFacturasEmitidas = function(){
        this.getContenedorModal().setComponente(this.nombre_instancia+".getContenedorModal()"); 
        this.getContenedorModal().muestraComponenteHuge();
        
        var contenido = "<br/><div style='padding-left:30px;'>";
        
        contenido += "<table align='center' cellpadding='6' cellspacing='0' border='0' width='100%'>";
        contenido += "<tr>";
        contenido += "<td align='left' valign='top' width='50%'>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_info_factura'></div>";
        
        contenido += "</td>";
        contenido += "<td align='left' valign='top' width='25%'>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_envio'></div>";
        
        contenido += "</td><td align='left' valign='top' width='25%'>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_cancelacion'></div>";
        
        contenido += "</td></tr></table>";
        
        contenido += "</div>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_impresion'></div>";
        
        this.getContenedorModal().getZonaContenido().style = "overflow:hidden;";
        this.getContenedorModal().getZonaContenido().innerHTML = contenido;
        
        var tipo_propietario = 100;
        if(this.visualizacion != 1) tipo_propietario = 101;
        
        var url = "../MagicScripts/Escaneos/"+tipo_propietario+"/"+this.referencia+".pdf";
        console.log("url: "+url);
        document.getElementById(this.nombre_instancia+"_zona_impresion").innerHTML = "<iframe src='"+url+"' width='99%' height='"+($( window ).height()*0.83)+"' id='"+this.nombre_instancia+"_iframe_pdf'><div style='background-image:url(http://sistema.hospitalmay.com/imagenes/ajax-loader-gd.gif);width:32px;height:32px;background-repeat:no-repeat;'></div></iframe>";
        
        this.getInfoFactura();
    }
    
    this.muestraComponente = function(){
        this.getContenedorModal().setComponente(this.nombre_instancia+".getContenedorModal()"); 
        this.getContenedorModal().muestraComponenteHuge();
        
        var contenido = "<br/><div style='padding-left:3px; text-align:center;'>";
        
        contenido += "<div class='row' id='"+this.nombre_instancia+"_zona_botones'>";
        
        if(this.modo == 1){
            contenido += "<div class='col s4 m4 l4'>";
            
            contenido += "<select id='"+this.nombre_instancia+"_selector_tipo_facturacion' class='browser-default'>";
            contenido += "<option value='1'>Vista Previa (Sin efecto fiscal)</option>";
            contenido += "<option value='2'>Emitir CDFI</option>";
            contenido += "</select>";
            
            contenido += "</div>";
            contenido += "<div class='col s4 m4 l4'>";
            
            if(this.opcion_factura == 1){
                contenido += "<div id='"+this.nombre_instancia+"_zona_boton_facturar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Factura Global</a></div>";   
            }else if(this.opcion_factura == 2){
                contenido += "<div id='"+this.nombre_instancia+"_zona_boton_facturar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Factura Pago</a></div>";   
            }
            
            contenido += "</div>";
            contenido += "<div class='col s4 m4 l4'>";
            contenido += "</div>";   
        }
        
        contenido += "</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_impresion'></div>";
        contenido += "</div>";
        
        this.getContenedorModal().getContenedorDeContenido().style = "overflow:hidden;";
        this.getContenedorModal().getContenedorDeContenido().innerHTML = contenido;
        
        this.generaCDFI(this.opcion_factura);
    }
    
    this.generaCDFI = function(opcion){
        opcion = this.opcion_factura;
        
        var tipo_ambiente = 1;//por defecto siempre el tipo de ambiente es desarrollo
        if(this.modo == 1){
            var selector = document.getElementById(this.nombre_instancia+"_selector_tipo_facturacion");
            tipo_ambiente = selector.options[selector.selectedIndex].value;    
        }
        
        var url = "ms/Facturacion.php?idElemento="+this.idElemento+"&ambiente="+tipo_ambiente+"&opcion="+this.opcion_factura;
        if(this.modo == 2){
            //modo 2 es para solo visualizar la factura ya realizada
            url += "&visualizacion=2";
        }else if(this.modo == 3){
            //modo 3 es para visualizar factura cancelada
            url += "&visualizacion=3";
        }
        
        document.getElementById(this.nombre_instancia+"_zona_impresion").innerHTML = "<iframe src='"+url+"' width='99%' height='"+($( window ).height()*0.79)+"' id='"+this.nombre_instancia+"_iframe_pdf'></iframe>";
        
        if(this.modo == 1){
            document.getElementById(this.nombre_instancia+"_zona_boton_facturar").innerHTML = "<div class='cargando_grande'></div>";
        }
        
        var frame = document.getElementById(this.nombre_instancia+"_iframe_pdf");
        var instancia = this;
        
        var modo = this.modo;
        frame.onload = function(){
            if(modo == 1){
                if(opcion == 1) document.getElementById(instancia.nombre_instancia+"_zona_boton_facturar").innerHTML = "<a class='waves-effect waves-light btn' onclick='"+instancia.nombre_instancia+".generaCDFI("+opcion+")' style='margin:0 auto 0 auto;'>Factura Global</a>";
                else if(opcion == 2) document.getElementById(instancia.nombre_instancia+"_zona_boton_facturar").innerHTML = "<a class='waves-effect waves-light btn' onclick='"+instancia.nombre_instancia+".generaCDFI("+opcion+")' style='margin:0 auto 0 auto;'>Factura Pago</a>";
            }else if(modo == 2){
                var contenido = "<div class='row'>";
                contenido += "<div class='col s6 m6 l6' id='"+instancia.nombre_instancia+"_zona_boton_enviar_factura'>";
                
                contenido += "<a class='waves-effect waves-light btn' onclick='"+instancia.nombre_instancia+".enviarNotificacionFactura()' style='margin:0 auto 0 auto;'>Enviar por Correo</a>";
                
                contenido += "</div>";
                contenido += "<div class='col s6 m6 l6' id='"+instancia.nombre_instancia+"_zona_boton_cancelar_factura'>";
                
                contenido += "<a class='waves-effect waves-light btn' onclick='"+instancia.nombre_instancia+".cancelarCDFI();' style='margin:0 auto 0 auto;'>Cancelar Factura</a>";
                
                contenido += "</div>";
                contenido += "</div>";
                
                document.getElementById(instancia.nombre_instancia+"_zona_botones").innerHTML = contenido;
            }else if(modo == 3){
                var contenido = "<div class='row'>";
                contenido += "<div class='col s6 m6 l6' id='"+instancia.nombre_instancia+"_zona_boton_enviar_factura'>";
                
                contenido += "<a class='waves-effect waves-light btn' onclick='"+instancia.nombre_instancia+".enviarNotificacionFacturaCancelada()' style='margin:0 auto 0 auto;'>Enviar Acuse</a>";
                
                contenido += "</div>";
                contenido += "</div>";
                
                document.getElementById(instancia.nombre_instancia+"_zona_botones").innerHTML = contenido;
            }
            
            
            if(instancia.funcion_facturas_actualizadas && tipo_ambiente == 2) setTimeout(instancia.funcion_facturas_actualizadas, 0);
            setTimeout(instancia.nombre_instancia+".actualizaInfoEmision("+opcion+")", 0);
        }
    }
    
    this.procesaFacturaCancelada = function(respuesta){
        console.log("respuesta cancelacion: "+respuesta);
        
        document.getElementById(this.nombre_instancia+"_zona_boton_cancelar_factura").innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".cancelarCDFI();' style='margin:0 auto 0 auto;'>Cancelar Factura</a>";
        
        if(respuesta == 1){
            alert("Factura Cancelada! Se ha enviado un correo de notificación al emisor y receptor de la cancelación.");
            document.getElementById(this.nombre_instancia+"_zona_boton_cancelar_factura").innerHTML = "";
            console.log(this.funcion_facturas_actualizadas);
            if(this.funcion_facturas_actualizadas) setTimeout(this.funcion_facturas_actualizadas, 0);
        }else{
            alert("No se pudo cancelar la factura, es posible que la factura ya haya sido cancelada previamente.");
        }
    }
    
    this.cancelarCDFI = function(){
        if(!confirm("Estas seguro de querer cancelar este CDFI?")) return;
        
        //continuamos con la cancelacion de la factura
        getResultadoAjax("opcion=58&idFactura="+this.idFactura, this.nombre_instancia+".procesaFacturaCancelada", document.getElementById(this.nombre_instancia+"_zona_boton_cancelar_factura"));
    }
    
    this.procesaInfoDeEmisionDeFacturaRecuperada = function(respuesta){
        console.log("respuesta info emision: "+respuesta);
        
        this.info_factura = respuesta.split("|");
        
        this.getInfoFactura();
    }
    
    this.getInfoFactura = function(){
        console.log("referencia: "+this.referencia);
        if(!this.referencia) return;
        
        /*if(!this.uuid_cancelado && this.modo != 2){
            var selector = document.getElementById(this.nombre_instancia+"_selector_tipo_facturacion");
            var tipo_ambiente = selector.options[selector.selectedIndex].value;
            if(tipo_ambiente == 1) return; //en el ambiente de prueba no se recupera info de la factura   
        }*/
        
        if(!this.info_factura){
            console.log("uuiddos: "+this.uuid);
            getResultadoAjax("opcion=360&uuid="+this.uuid, this.nombre_instancia+".procesaInfoDeEmisionDeFacturaRecuperada", document.getElementById(this.nombre_instancia+"_zona_info_factura"));
            return;
        }
        //idFacturaEmitida, UUID, fecha, hora, usuario, estatus
        getListaDePersonas().setIdPersonaSeleccionada(this.info_factura[4]);
        
        if(this.uuid_cancelado){
            document.getElementById(this.nombre_instancia+"_zona_info_factura").innerHTML = "Cancelada <b>"+getFechaLegibleRecortada(this.info_factura[2])+"</b> a las <b>"+this.info_factura[3]+"</b> por <b>"+getListaDePersonas().getNombreDePersonaSeleccionada()+"</b>";   
            
            document.getElementById(this.nombre_instancia+"_zona_envio").innerHTML = "<div class='boton_normal' onclick='"+this.nombre_instancia+".enviarNotificacionFactura(2)'>Enviar Correo</div>";
            
            return;
        }
        
        document.getElementById(this.nombre_instancia+"_zona_info_factura").innerHTML = "Emitida <b>"+getFechaLegibleRecortada(this.info_factura[2])+"</b> a las <b>"+this.info_factura[3]+"</b> por <b>"+getListaDePersonas().getNombreDePersonaSeleccionada()+"</b>";
        
        document.getElementById(this.nombre_instancia+"_zona_envio").innerHTML = "<div class='boton_normal' onclick='"+this.nombre_instancia+".enviarNotificacionFactura(1)'>Enviar Correo</div>";
        
        //como si se recuperó información de la factura, entonces aparecerá el botón de cancelación de factura, siempre y cuando el estatus sea mayor a 0. Un estatus -1 quiere decir que esta factura ya fue cancelada
        if(this.info_factura[5] > 0) document.getElementById(this.nombre_instancia+"_zona_cancelacion").innerHTML = "<div class='boton_eliminar' onclick='"+this.nombre_instancia+".cancelarCDFI()'>Cancelar CDFI</div>";
        else document.getElementById(this.nombre_instancia+"_zona_cancelacion").innerHTML = "";
    }
    
    this.procesaFacturaEnviada = function(respuesta){
        console.log("respuesta correo enviado: "+respuesta);
        
        if(respuesta == 1){
            alert("Correo de notificación enviado!");
        }else{
            alert("No fue posible enviar el correo de notificación: "+respuesta);
        }
        
        document.getElementById(this.nombre_instancia+"_zona_boton_enviar_factura").innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".enviarNotificacionFactura()' style='margin:0 auto 0 auto;'>Enviar por Correo</a>";
    }
    
    this.enviarNotificacionFactura = function(){
        getResultadoAjax("opcion=57&idFactura="+this.idFactura, this.nombre_instancia+".procesaFacturaEnviada", document.getElementById(this.nombre_instancia+"_zona_boton_enviar_factura"));
    }
    
    this.procesaAcuseEnviado = function(respuesta){
        console.log("respuesta correo enviado: "+respuesta);
        
        if(respuesta == 1){
            alert("Acuse de Cancelación enviado!");
        }else{
            alert("No fue posible enviar el acuse de notificación: "+respuesta);
        }
        
        document.getElementById(this.nombre_instancia+"_zona_boton_enviar_factura").innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".enviarNotificacionFactura()' style='margin:0 auto 0 auto;'>Enviar Acuse</a>";
    }
    
    this.enviarNotificacionFacturaCancelada = function(){
        getResultadoAjax("opcion=60&idFactura="+this.idFactura, this.nombre_instancia+".procesaAcuseEnviado", document.getElementById(this.nombre_instancia+"_zona_boton_enviar_factura"));
    }
    
    this.actualizaInfoEmision = function(opcion){
        return;
    }
}