// JavaScript Controlador de Navegación
var ControladorNavegacion =  function(){
	
	this.contenedor = null;
	this.metodo_atras = null;
	this.nombre_instancia = "";
    
    this.componentes_padre = null;
    this.funciones_atras = null;
    this.textos_ayuda = null;
    
    this.posicion = -1;
	
	this.setControladorNavegacion = function(contenedor, nombre){
		this.contenedor = contenedor;
		this.nombre_instancia = nombre;
	}
    
    this.push = function(padre, funcion, texto){
        this.posicion++;
        
        if(!this.componentes_padre) this.componentes_padre = new Array();
        if(!this.funciones_atras) this.funciones_atras = new Array();
        if(!this.textos_ayuda) this.textos_ayuda = new Array();
        
        this.componentes_padre[this.componentes_padre.length] = padre;
        this.funciones_atras[this.funciones_atras.length] = funcion;
        this.textos_ayuda[this.textos_ayuda.length] = texto;
    }
    
    this.pop = function(){
        //Si la posicion ahora es -1, quiere decir que ya no hay un componente atras, por lo cual el contenedor que devuelve es el contenedor original, es decir, el contenedor principal. En otro caso devuelve el contenedor de contenido del controlador
        console.log("posicion: "+this.posicion);
        var contenedor;
        if(this.posicion == 0) contenedor = this.contenedor;
        else{
            this.posicion--;
            this.muestraControladorNavegacion();
            contenedor = this.getContenedorDeContenido();
            this.posicion++;
        }
        
        this.componentes_padre[this.posicion].setContenedor(contenedor);
        //this.componentes_padre[this.posicion].muestraComponente();
        setTimeout(this.funciones_atras[this.posicion], 0);
        
        //console.log("valores padre: "+this.componentes_padre[this.posicion]+", funcion: "+this.funciones_atras[this.posicion]+" texto:"+this.textos_ayuda[this.posicion]+" posicion: "+this.posicion);
        
        this.posicion--;
        
        if(this.funciones_atras) this.funciones_atras.pop();
        if(this.textos_ayuda) this.textos_ayuda.pop();
        if(this.componentes_padre) this.componentes_padre.pop();
    }    
	
	this.getContenedorDeContenido = function(){
		return document.getElementById(this.nombre_instancia+'_contenido_controlador_navegacion');
	}
    
    this.setTextoAyuda = function(texto){
        this.texto_ayuda = texto;
    }
    
    this.setMetodoAtras = function(metodo){
        this.metodo_atras = metodo;
    }
    
    this.getContenedor = function(){
        return this.contenedor;
    }
	
	//esta función recupera el botón cerrar del controlador de navegacion
	this.getBotonCerrar = function(){
		return document.getElementById(this.nombre_instancia+"_cerrar");
	}
    
    this.botonCerrarPresionado = function(){
        this.pop();
    }
	
	this.muestraControladorNavegacion = function(){
		if(this.textos_ayuda) var texto = this.textos_ayuda[this.posicion];
        else var texto = "";
        
        var contenido = '<div class="card-panel">';
        contenido += "<table class='centered'><tr><td width='5%'>";
        
        contenido += "<div class='left-align'><a class='btn-floating fondo_principal scale-transition scale-in' onclick='"+this.nombre_instancia+".botonCerrarPresionado()' onmouseover='muestraAyudaCampo(\""+this.nombre_instancia+"_controlador_navegacion_close\", \""+texto+"\");' onmouseout='ocultaAyudaCampo(\""+this.nombre_instancia+"_controlador_navegacion_close\");'><i class='material-icons'>close</i></a></div>";
        
        contenido += "</td><td width='95%'>";
        
        contenido += "<div class='ayuda_campos' id='"+this.nombre_instancia+"_controlador_navegacion_close'></div>";
        
        contenido += "</td></tr></table>";
        
        contenido += "<br/><div id='"+this.nombre_instancia+"_contenido_controlador_navegacion' style='height:"+(getAlto()*0.9)+"px; overflow-y:auto;'>";
        contenido += "</div>";
		
		this.contenedor.innerHTML = contenido;
	}
    
    this.muestraControladorNavegacionMedio = function(){
		if(this.textos_ayuda) var texto = this.textos_ayuda[this.posicion];
        else var texto = "";
        
        var contenido = '<div class="card-panel">';
        contenido += "<table class='centered'><tr><td width='5%'>";
        
        contenido += "<div class='left-align'><a class='btn-floating fondo_principal scale-transition scale-in' onclick='"+this.nombre_instancia+".botonCerrarPresionado()' onmouseover='muestraAyudaCampo(\""+this.nombre_instancia+"_controlador_navegacion_close\", \""+texto+"\");' onmouseout='ocultaAyudaCampo(\""+this.nombre_instancia+"_controlador_navegacion_close\");'><i class='material-icons'>close</i></a></div>";
        
        contenido += "</td><td width='95%'>";
        
        contenido += "<div class='ayuda_campos' id='"+this.nombre_instancia+"_controlador_navegacion_close'></div>";
        
        contenido += "</td></tr></table>";
        
        contenido += "<br/><div id='"+this.nombre_instancia+"_contenido_controlador_navegacion' style='overflow-y:auto;'>";
        contenido += "</div>";
		
		this.contenedor.innerHTML = contenido;
	}
}