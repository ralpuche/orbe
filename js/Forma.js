var Forma = function(componente_padre){
    
    //VARIABLES
    this.info = "";
    this.setInfo = function(info){
        this.info = info
    }
    
    //Contenedor de la forma
    this.contenedor = null;
    
    //Nombre de instancia de la forma para referirse a ella desde ámbito global
    this.nombre_instancia = "";
    
    //Componente padre de la forma actual
    this.componente_padre = (componente_padre)? componente_padre:null;
    
    //SETTERS Y GETTERS
    
    //Setea el contenedor para la forma. Debe ser un elemento div
    this.setContenedor = function(contenedor){
        this.contenedor = contenedor;
    }
    
    //Recupera el contenedor para la forma. Debe ser un elemento div o null
    this.getContenedor = function(){
        return this.contenedor;
    }
    
    //Setea el nombre de la instancia para la forma. Para referirse a ella en el ámbito global
    this.setNombreInstancia = function(nombre){
        this.nombre_instancia = nombre;
    }
    
    //Recupera el nombre de la instancia de la forma.
    this.getNombreInstancia = function(){
        this.nombre_instancia;
    }
    
    //Setea el componente padre de la forma actual, debería ser un elemento de clase Forma
    this.setComponentePadre = function(padre){
        this.componente_padre = padre;
    }
    
    this.getComponentePadre = function(){
        return this.componente_padre;
    }
    
    this.setComponente = function(contendor, nombre_instancia){
        this.contenedor = contendor;
        this.nombre_instancia = nombre_instancia;
    }
    
    this.muestraComponenteGenerico = function(){
        var contenido = "<div class='h1' id='"+this.nombre_instancia+"_titulo_principal'></div>";
        contenido += "<div class='h2' id='"+this.nombre_instancia+"_subtitulo_principal'></div>";
        contenido += "<div class='contenido_principal' id='"+this.nombre_instancia+"_contenido_principal'></div>";
        
        this.contenedor.innerHTML = contenido;
    }
    
    this.muestraComponenteGenericoPrincipal = function(){
        var contenido = "<div class='card-panel'><div class='h4' id='"+this.nombre_instancia+"_titulo_principal'></div>";
        
        contenido += "<div class='h5' id='"+this.nombre_instancia+"_subtitulo_principal'></div>";
        contenido += "<div class='contenido_principal' id='"+this.nombre_instancia+"_contenido_principal'></div></div>";
        
        this.contenedor.innerHTML = contenido;
    }
    
    this.muestraComponenteGenericoConSoloContenido = function(){
        var contenido = "<div class='contenido_principal' id='"+this.nombre_instancia+"_contenido_principal'></div>";
        
        this.contenedor.innerHTML = contenido;
    }
    
    this.getDivTitulo = function(){
        return document.getElementById(this.nombre_instancia+"_titulo_principal");   
    }
    
    this.getDivSubtitulo = function(){
        return document.getElementById(this.nombre_instancia+"_subtitulo_principal");   
    }
    
    this.getDivContenido = function(){
        return document.getElementById(this.nombre_instancia+"_contenido_principal");   
    }
    
    //ADMINISTRACION DE LA INFORMACION DE LA FORMA
    this.editaInfo = function(){
        console.log("Debes implementar esta funcion en tu clase superior");
    }
    
    //CONTROLADOR DE NAVEGACION
    this.controlador_navegacion = null;
    
    this.getControladorNavegacion = function(){
        if(!this.controlador_navegacion){
            this.controlador_navegacion = new ControladorNavegacion();
            this.controlador_navegacion.setControladorNavegacion(this.contenedor, this.nombre_instancia+".getControladorNavegacion()");
        }
        return this.controlador_navegacion;
    }
    
    this.setControladorNavegacion = function(controlador){
        this.controlador_navegacion = controlador;
    }
    
    this.muestraControladorNavegacion = function(){
        this.getControladorNavegacion().setControladorNavegacion(this.contenedor, null, null, this.nombre_instancia+".muestraComponente()", this.nombre_instancia+".getControladorNavegacion()");
        this.getControladorNavegacion().muestraControladorNavegacion();
    }
    
    this.showControladorNavegacion = function(texto, funcion){
        this.getControladorNavegacion().push(this, funcion, texto);
        this.getControladorNavegacion().muestraControladorNavegacion();
    }
    
    //COMPONENTE MODAL
    this.contenedor_modal = null;
    this.getContenedorModal = function(){
        if(!this.contenedor_modal) this.contenedor_modal = new ComponenteModal();
        return this.contenedor_modal;
    }
    
    this.muestraContenedorModal = function(tipo, alto, ancho){
        this.getContenedorModal().setComponente(this.nombre_instancia+".getContenedorModal()");
        
        if(tipo == "M") this.getContenedorModal().muestraComponente();
        else if(tipo == "S") this.getContenedorModal().muestraComponenteSmall();
        else if(tipo == "VS") this.getContenedorModal().muestraComponenteMuyPequenio();
        else if(tipo == "G") this.getContenedorModal().muestraComponenteGrande();
        else if(tipo == "P"){
            this.getContenedorModal().setDimensiones(alto, ancho);
            this.getContenedorModal().muestraComponentePersonalizado();
        }
        
        return this.getContenedorModal().getContenedorDeContenido();
    }
}