var FormaBanco = function(){
    
    this.idBanco = 0;
    this.setIdBanco = function(idbanco){
        this.idBanco = idbanco;
    }
    
    this.info = null;
    
    this.muestraComponente = function(){
        var contenido = "<br/>";    
        
        if(this.idBanco == 0) contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Nuevo Banco</div>";
        else contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Editando Banco</div>";
        
        contenido += "<div class='separador'></div>";
        contenido += "<br/>";    
        
        contenido += "<div class='texto_forma'>Nombre completo del banco</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_nombre' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el nombre completo del banco\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        contenido += "<br/>";
        
        contenido += "<div class='ayuda_campos' id='"+this.nombre_instancia+"_a1'></div>";
        contenido += "<br/>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_boton_guardar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a></div>";
        
        contenido += "<br/>";
        
        this.contenedor.innerHTML = contenido;
    }
    
    this.editaComponente = function(){
        if(!this.info){
            getResultadoAjax("opcion=12&idbanco="+this.idBanco, this.nombre_instancia+".procesaInfoRecuperada", this.contenedor);
            return;
        }
        
        this.muestraComponente();
        //idBanco, nombreCompleto, estatus
        
        document.getElementById(this.nombre_instancia+"_campo_nombre").value = this.info[1];
    }
    
    this.procesaInfoRecuperada = function(respuesta){
        console.log("info: "+respuesta);
        
        this.info = respuesta.split("|");
        this.editaComponente();
    }
    
    this.getZonaBotonGuardar = function(){
        return document.getElementById(this.nombre_instancia+"_zona_boton_guardar");
    }
    
    this.validaForma = function(){
        var campoNombre = document.getElementById(this.nombre_instancia+"_campo_nombre");
        if(campoNombre.value.length == 0 || campoNombre.value == "" || campoNombre.value == " "){
            campoNombre.focus();
            return;
        }
        
        var info = this.idBanco+"|"+campoNombre.value;
        //idusuario, nombrecompleto
        
        console.log("info: "+info);
        
        getResultadoAjax("opcion=11&info="+info, this.nombre_instancia+".respuestaBancoGuardado", this.getZonaBotonGuardar());
    }
    
    this.respuestaBancoGuardado = function(respuesta){
        console.log("respuesta guardar: "+respuesta);
        this.getZonaBotonGuardar().innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a>";
        
        if(respuesta > 0 && !isNaN(respuesta)){
            alert("Banco guardado!");
            if(this.idBanco == 0) this.idBanco = respuesta;
        }else{
            alert("No se pudo guardar el banco, por favor intenta más tarde!");
        }
    }
}