var FormaCliente = function(){
    
    this.idCliente = 0;
    this.setIdCliente = function(idcliente){
        this.idCliente = idcliente;
    }
    
    this.iddatos_facturacion = 0;
    
    this.selector_datos_facturacion = null;
    this.getSelectorDatosFacturacion = function(){
        if(!this.selector_datos_facturacion){
            this.selector_datos_facturacion = new CatalogoSelectorDatosFacturacion();
            this.selector_datos_facturacion.setTipoVisualizacion(1);
        }
        return this.selector_datos_facturacion;
    }
    
    this.info = null;
    
    this.muestraComponente = function(){
        var contenido = "<br/>";    
        
        if(this.idBanco == 0) contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Nuevo Cliente</div>";
        else contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Editando Cliente</div>";
        
        contenido += "<div class='separador'></div>";
        contenido += "<br/>";    
        
        contenido += "<div class='texto_forma'>Nombre completo del cliente</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_nombre' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el nombre completo del cliente\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        contenido += "<br/>";
        
        contenido += "<div class='ayuda_campos' id='"+this.nombre_instancia+"_a1'></div>";
        contenido += "<br/>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_datos_facturacion'></div>";
        
        contenido += "<br/>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_boton_guardar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a></div>";
        
        contenido += "<br/>";
        
        this.contenedor.innerHTML = contenido;
        
        //agregamos datos de facturacion
        this.getSelectorDatosFacturacion().setComponente(document.getElementById(this.nombre_instancia+"_zona_datos_facturacion"), this.nombre_instancia+".getSelectorDatosFacturacion()");
        this.getSelectorDatosFacturacion().setFuncionDatosFacturacionSeleccionada(this.nombre_instancia+".actualizaDatosFacturacion()");
        console.log("iddatos facturacion: "+this.iddatos_facturacion);
        if(this.iddatos_facturacion == 0) this.getSelectorDatosFacturacion().muestraComponenteSelector();
        else this.getSelectorDatosFacturacion().setIdDatosFacturacion(this.iddatos_facturacion);
    }
    
    this.actualizaDatosFacturacion = function(){
        
    }
    
    this.editaComponente = function(){
        if(!this.info){
            getResultadoAjax("opcion=23&idcliente="+this.idCliente, this.nombre_instancia+".procesaInfoRecuperada", this.contenedor);
            return;
        }
        this.iddatos_facturacion = this.info[2];
        this.muestraComponente();
        //idBanco, nombreCompleto, estatus
        
        document.getElementById(this.nombre_instancia+"_campo_nombre").value = this.info[1];
        
    }
    
    this.procesaInfoRecuperada = function(respuesta){
        console.log("info: "+respuesta);
        
        this.info = respuesta.split("|");
        this.editaComponente();
    }
    
    this.getZonaBotonGuardar = function(){
        return document.getElementById(this.nombre_instancia+"_zona_boton_guardar");
    }
    
    this.validaForma = function(){
        var campoNombre = document.getElementById(this.nombre_instancia+"_campo_nombre");
        if(campoNombre.value.length == 0 || campoNombre.value == "" || campoNombre.value == " "){
            campoNombre.focus();
            return;
        }
        
        var info = this.idCliente+"|"+campoNombre.value+"|"+this.getSelectorDatosFacturacion().getIdDatosFacturacion();
        //idusuario, nombrecompleto
        
        console.log("info: "+info);
        
        getResultadoAjax("opcion=19&info="+info, this.nombre_instancia+".respuestaElementoGuardado", this.getZonaBotonGuardar());
    }
    
    this.respuestaElementoGuardado = function(respuesta){
        console.log("respuesta guardar: "+respuesta);
        this.getZonaBotonGuardar().innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a>";
        
        if(respuesta > 0 && !isNaN(respuesta)){
            alert("Cliente guardado!");
            if(this.idCliente == 0) this.idCliente = respuesta;
        }else{
            alert("No se pudo guardar el cliente, por favor intenta más tarde!");
        }
    }
}