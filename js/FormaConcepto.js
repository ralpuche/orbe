var FormaConcepto = function(){
    
    this.idConcepto = 0;
    this.setIdConcepto = function(idconcepto){
        this.idConcepto = idconcepto;
    }
    
    this.setIdDatosCaptura = function(id){
        this.setIdConcepto(id);
    }
    
    this.componente_padre = null;
    this.setComponentePadre = function(componente){
        this.componente_padre = componente;
    }
    
    this.info = null;
    
    this.muestraComponente = function(){
        var contenido = "<br/>";    
        
        if(this.idConcepto == 0) contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Nuevo Concepto de Gasto Interno</div>";
        else contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Editando Concepto de Gasto Interno</div>";
        
        contenido += "<div class='separador'></div>";
        contenido += "<br/>";    
        
        contenido += "<div class='texto_forma'>Nombre del Concepto</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_nombre' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el nombre del concepto para gasto interno\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += "<br/>";
        contenido += "<div class='ayuda_campos' id='"+this.nombre_instancia+"_a1'></div>";
        contenido += "<br/>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_boton_guardar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a></div>";
        
        contenido += "<br/>";
        
        this.contenedor.innerHTML = contenido;
    }
    
    this.actualizaDatosFacturacion = function(){
        
    }
    
    this.editaInfo = function(){
        if(!this.info){
            getResultadoAjax("opcion=28&idconcepto="+this.idConcepto, this.nombre_instancia+".procesaInfoRecuperada", this.contenedor);
            return;
        }
        
        this.muestraComponente();
        document.getElementById(this.nombre_instancia+"_campo_nombre").value = this.info[1];
    }
    
    this.procesaInfoRecuperada = function(respuesta){
        console.log("info: "+respuesta);
        
        this.info = respuesta.split("|");
        this.editaInfo();
    }
    
    this.getZonaBotonGuardar = function(){
        return document.getElementById(this.nombre_instancia+"_zona_boton_guardar");
    }
    
    this.validaForma = function(){
        var campoNombre = document.getElementById(this.nombre_instancia+"_campo_nombre");
        if(campoNombre.value.length == 0 || campoNombre.value == "" || campoNombre.value == " "){
            campoNombre.focus();
            return;
        }
        
        var info = this.idConcepto+"|"+campoNombre.value;
        //idConcepto, nombrecompleto
        
        console.log("info: "+info);
        this.info = info;
        
        getResultadoAjax("opcion=27&info="+info, this.nombre_instancia+".respuestaElementoGuardado", this.getZonaBotonGuardar());
    }
    
    this.respuestaElementoGuardado = function(respuesta){
        console.log("respuesta guardar: "+respuesta);
        this.getZonaBotonGuardar().innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a>";
        
        if(respuesta > 0 && !isNaN(respuesta)){
            alert("Concepto guardado!");
            if(this.idConcepto == 0) this.idConcepto = respuesta;
            if(this.componente_padre) this.componente_padre.conceptoAgregado(this.info);
        }else{
            alert("No se pudo guardar el concepto, por favor intenta más tarde!");
        }
    }
}