var FormaConceptoDeFacturacion = function(){
    
    this.idConceptoFacturacion = 0;
    this.setIdConceptoFacturacion = function(idConcepto){
        this.idConceptoFacturacion = idConcepto;
    }
    
    //Catalogo Productos y Servicios
    this.idProductoServicio = 0;
    
    this.selectorProductosAndServicios = null;
    this.getSelectorProductosAndServicios = function(){
        if(!this.selectorProductosAndServicios){
            this.selectorProductosAndServicios = new SelectorDeOpcion();
            this.selectorProductosAndServicios.setVisualizacion(1);
        }
        return this.selectorProductosAndServicios;
    }
    
    this.catalogoProductosAndServicios = null;
    this.getCatalogoProductosAndServicios = function(){
        if(!this.catalogoProductosAndServicios) this.catalogoProductosAndServicios = new CatalogoProductosAndServicios();
        return this.catalogoProductosAndServicios;
    }
    
    //Catalogo de Unidades
    this.idUnidadSat = 0;
    
    this.selectorUnidad = null;
    this.getSelectorUnidad = function(){
        if(!this.selectorUnidad){
            this.selectorUnidad = new SelectorDeOpcion();
            this.selectorUnidad.setVisualizacion(1);
        }
        return this.selectorUnidad;
    }
    
    this.catalogoUnidades = null;
    this.getCatalogoUnidades = function(){
        if(!this.catalogoUnidades) this.catalogoUnidades = new CatalogoUnidadesSat();
        return this.catalogoUnidades;
    }
    
    //información del concepto de facturación
    this.infoConceptoFacturacion = null;
    
    //idGasto si es 0, entonces pertenece al catálogo global de conceptos. Si tiene un idGasto definido, entonces corresponde a un gasto particular
    this.idGasto = 0;
    this.setIdGasto = function(idGasto){
        this.idGasto = idGasto;
    }
    
    //este valor sirve para indicar que el id con el que se está recuperando la info de este gasto es un id de concepto de facturación global, por lo que deberá liberarlo en cuanto se recupere la info, para que no sobreescriba los valores del concepto global y pueda crearse una copia de este concepto como parte del gasto al que se está agregando. Por defecto está en falso
    this.idPrestado = false;
    this.setIdPrestado = function(valor){
        this.idPrestado = valor;
    }
    
    //funcion invocada cuando se actualiza el concepto
    this.funcionActualizarConcepto = null;
    this.setFuncionActualizarConcepto = function(funcion){
        this.funcionActualizarConcepto = funcion;
    }
    
    //función para cuando se presiona el botón agregar del concepto
    this.funcionAgregarConcepto = null;
    this.setFuncionAgregarConcepto = function(funcion){
        this.funcionAgregarConcepto = funcion;
    }
    
    this.muestraComponente = function(){
        var contenido = "<br/>";    
        
        //campos para llenar concepto en cfdi: claveProductosServicios (catalogo), NoIdentificación (interno del cliente o sistema), cantidad, claveUnidad(catalogo), descripcion = Venta, valorUnitario (este valor será el valor sin IVA), Importe (cantidad * valorUnitario), descuento (en caso de haberlo), 
        
        if(this.idConceptoFacturacion == 0) contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Nuevo Concepto de Facturación</div>";
        else contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Editando Concepto de Facturación</div>";
        
        contenido += "<div class='separador'></div>";
        contenido += "<br/>";    
        
        //selector de cliente
        contenido += '<div class="row center-align">';
        contenido += '<div class="col l12 center-align">';
        
        contenido += "<div class='texto_forma'>Clave Productos y Servicios (SAT)</div>";
        contenido += "<br/>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_concepto_global'></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        //claveUnidad y cantidad
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s12 center-align">';
        
        contenido += "<div class='texto_forma'>Clave Unidad</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_clave_unidad'></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        //descripción del producto que se va a agregar
        contenido += '<div class="row center-align">';
        contenido += '<div class="col l12 center-align">';
        
        contenido += "<div class='texto_forma'>Descripción</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_descripcion' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe la descripción del producto o servicio\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Valor Unitario</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_valor_unitario' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el valor unitario (sin I.V.A.) del producto. Solo números (0-9) y punto decimal permitido\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\"); "+this.nombre_instancia+".calculaTotal();'/>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Cantidad</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_cantidad' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el número de unidades de este producto. Solo números (0-9) y punto decimal permitido\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\"); "+this.nombre_instancia+".calculaTotal();'/>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        //descuento
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Descuento</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_descuento' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el descuento del producto si aplica. Solo números (0-9) y punto decimal permitido\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\"); "+this.nombre_instancia+".calculaTotal();'/>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".desglosarIva()' style='margin:0 auto 0 auto;'>Desglosar IVA</a></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += "<br/>";
        contenido += "<div class='ayuda_campos' id='"+this.nombre_instancia+"_a1'></div>";
        contenido += "<br/>";
        
        //total con iva y multiplicado por la cantidad
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s12 center-align">';
        
        contenido += "<div class='texto_forma' id='"+this.nombre_instancia+"_total'><b>Total: $</b></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        //boton guardar y agregar
        contenido += '<div class="row center-align">';
        contenido += '<div class="col l6 center-align">';
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_boton_guardar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma(1)' style='margin:0 auto 0 auto;'>Guardar</a></div>";
        
        contenido += '</div>';
        contenido += '<div class="col l6 center-align">';
        
        //solo se pueden agregar los conceptos que tienen idGasto en 0, en otro caso no se pueden agregar por que ya están agregados, solo se pueden modificar
        if(this.idConceptoFacturacion > 0 && this.idGasto == 0){
            contenido += "<div id='"+this.nombre_instancia+"_zona_boton_agregar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma(2)' style='margin:0 auto 0 auto;'>Agregar</a></div>";   
        }
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += "<br/>";
        
        this.contenedor.innerHTML = contenido;
        
        //selector productos o servicios
        this.getSelectorProductosAndServicios().setComponente(document.getElementById(this.nombre_instancia+"_zona_concepto_global"), this.nombre_instancia+".getSelectorProductosAndServicios()");
        
        if(this.idProductoServicio == 0) this.getSelectorProductosAndServicios().setTextoDeOpcionSeleccionada("Selecciona Clave de Producto o Servicio");
        this.getSelectorProductosAndServicios().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoDeProductosAndServicios()");
        
        this.getSelectorProductosAndServicios().muestraComponente();
        
        //selector de unidad sat
        this.getSelectorUnidad().setComponente(document.getElementById(this.nombre_instancia+"_zona_clave_unidad"), this.nombre_instancia+".getSelectorUnidad()");
        
        if(this.idUnidadSat == 0) this.getSelectorUnidad().setTextoDeOpcionSeleccionada("Selecciona Unidad");
        this.getSelectorUnidad().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoDeUnidadesSat()");
        
        this.getSelectorUnidad().muestraComponente();
    }
    
    //esta función hace el desglose del iva considerando el valor unitario del concepto
    this.desglosarIva = function(){
        //tomamos el valor del campo valor unitario para desglosarlo
        var campoValorUnitario = document.getElementById(this.nombre_instancia+"_campo_valor_unitario");
        var valor = campoValorUnitario.value;
        if(!validateDecimal(valor)){
            alert("El valor unitario debe ser un número 0-9 y . (punto decimal) permitidos");
            return;
        }
        
        campoValorUnitario.value = getSoloDosCifrasDespuesDelPunto(valor / 1.16);
        this.calculaTotal();
    }
    
    //valida los datos de la forma antes de guardar o agregar a los conceptos
    this.validaForma = function(opcion){
        if(this.idProductoServicio == 0){
            muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar la clave de productos y servicios de SAT");
            return;
        }
        
        if(this.idUnidadSat == 0){
            muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar la unidad SAT");
            return;
        }
        
        var descripcion = document.getElementById(this.nombre_instancia+"_campo_descripcion");
        if(descripcion.value == "" || descripcion.value == " "){
            descripcion.focus();
            return;
        }
        
        var campoValorUnitario = document.getElementById(this.nombre_instancia+"_campo_valor_unitario");
        if(!validateDecimal(campoValorUnitario.value)){
            campoValorUnitario.focus();
            return;
        }
        
        var campoCantidad = document.getElementById(this.nombre_instancia+"_campo_cantidad");
        if(!validateDecimal(campoCantidad.value)){
            campoCantidad.focus();
            return;
        }
        
        var campoDescuento = document.getElementById(this.nombre_instancia+"_campo_descuento");
        if(campoDescuento.value.length > 0 && !validateDecimal(campoDescuento.value)){
            campoDescuento.focus();
            return;
        }
        
        var info = this.idConceptoFacturacion+"|"+this.idProductoServicio+"|"+this.idUnidadSat+"|"+descripcion.value+"|"+campoValorUnitario.value+"|"+campoCantidad.value+"|"+((campoDescuento.value == "" || campoDescuento.value == " ")?0:campoDescuento.value)+"|"+this.idGasto;
        
        //idConceptoFacturacion, idProductoServicio, idUnidad, descripcion, campoValorUnitario, campoCantidad, campoDescuento, idGasto
        
        if(opcion == 1){
            //guardamos el concepto en conceptos de facturacion para posterior consulta
            getResultadoAjax("opcion=43&info="+info, this.nombre_instancia+".procesaRespuestaActualizacion", document.getElementById(this.nombre_instancia+"_zona_boton_guardar"));
        }else{
            console.log("funcion a ejecutar: "+this.funcionAgregarConcepto);
            if(this.funcionAgregarConcepto) setTimeout(this.funcionAgregarConcepto, 0);
        }
    }
    
    this.procesaRespuestaActualizacion = function(respuesta){
        console.log("respuesta actualizacion: "+respuesta);
        
        if(respuesta > 0){
            if(this.idConceptoFacturacion == 0) this.idConceptoFacturacion = respuesta;
            alert("Concepto agregado!");
            this.infoConceptoFacturacion = null;
            this.editaComponente();
            
            if(this.funcionActualizarConcepto) setTimeout(this.funcionActualizarConcepto, 1000);
        }else{
            alert("No se pudo agregar el concepto, por favor intenta más tarde!");
            document.getElementById(this.nombre_instancia+"_zona_boton_guardar").innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma(1)' style='margin:0 auto 0 auto;'>Guardar</a>";
        }
    }
    
    //se calcula el total considerando el valor unitario y la cantidad de productos
    this.calculaTotal = function(){
        var campoValorUnitario = document.getElementById(this.nombre_instancia+"_campo_valor_unitario");
        var campoCantidad = document.getElementById(this.nombre_instancia+"_campo_cantidad");
        var campoDescuento = document.getElementById(this.nombre_instancia+"_campo_descuento");
        
        if(isNaN(campoValorUnitario.value)) return;
        if(isNaN(campoCantidad.value)) return;
        if(isNaN(campoDescuento.value)) return;
        
        var subtotal = (campoValorUnitario.value * campoCantidad.value) - campoDescuento.value;
        var contenido = "<b>Sub Total: $"+getCifraLegible(subtotal)+"</b>";
        
        contenido += "<br/><b>I.V.A.: $"+getCifraLegible(subtotal*0.16)+"</b>";
        contenido += "<br/><b>Total: $"+getCifraLegible(subtotal*1.16)+"</b>";
        
        document.getElementById(this.nombre_instancia+"_total").innerHTML = contenido;
    }
    
    //Producto o servicio seleccionado
    this.productoOrServicioSeleccionado = function(){
        var datos = this.getCatalogoProductosAndServicios().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorProductosAndServicios().setTextoDeOpcionSeleccionada(datos[1]+" - "+datos[2]);
            this.idProductoServicio = datos[1];
            this.getSelectorProductosAndServicios().muestraComponente();
        }
    }
    
    this.muestraCatalogoDeProductosAndServicios = function(){
        this.getCatalogoProductosAndServicios().setComponente(document.getElementById(this.nombre_instancia+"_zona_concepto_global"), this.nombre_instancia+".getCatalogoProductosAndServicios()");
        this.getCatalogoProductosAndServicios().setFuncionElementoSeleccionado(this.nombre_instancia+".productoOrServicioSeleccionado()");
        this.getCatalogoProductosAndServicios().muestraComponente();
    }
    
    //Unidad SAT
    this.muestraCatalogoDeUnidadesSat = function(){
        this.getCatalogoUnidades().setComponente(document.getElementById(this.nombre_instancia+"_zona_clave_unidad"), this.nombre_instancia+".getCatalogoUnidades()");
        this.getCatalogoUnidades().setFuncionElementoSeleccionado(this.nombre_instancia+".unidadSeleccionada()");
        this.getCatalogoUnidades().muestraComponente();
    }
    
    this.unidadSeleccionada = function(){
        var datos = this.getCatalogoUnidades().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorUnidad().setTextoDeOpcionSeleccionada(datos[1]+" - "+datos[2]);
            this.idUnidadSat = datos[1];
            this.getSelectorUnidad().muestraComponente();
        }
    }
    
    //recupera y procesa la info del concepto de facturacion
    this.procesaInfoConceptoFacturacion = function(respuesta){
        console.log("concepto recuperado: "+respuesta);
        
        if(respuesta == "" || respuesta == " ") this.infoConceptoFacturacion = "";
        else this.infoConceptoFacturacion = respuesta;
        this.editaComponente();
    }
    
    //editar concepto de facturacion
    this.editaComponente = function(){
        if(!this.infoConceptoFacturacion){
            getResultadoAjax("opcion=45&idConceptoFacturacion="+this.idConceptoFacturacion, this.nombre_instancia+".procesaInfoConceptoFacturacion", this.contenedor);
            return;
        }
        
        this.muestraComponente();
        
        //una vez que la información ha sido recuperada, se tiene que comprobar si el id es prestado, y si es así, dejarlo en 0 para que se pueda crear el nuevo componente como parte del gasto
        if(this.idPrestado){
            console.log("El id es prestado, ahora el id es 0");
            this.idConceptoFacturacion = 0;
        }
        
        var info = this.infoConceptoFacturacion.split("|");
        
        //idConceptoFacturacion, claveProductoServicio, claveUnidad, descripcion, valorUnitario, cantidad, descuento, estatus, descripcionUnidad, descripcionProductos
        this.getSelectorProductosAndServicios().setTextoDeOpcionSeleccionada(info[1]+" - "+info[9]);
        this.getSelectorProductosAndServicios().muestraComponente();
        
        this.getSelectorUnidad().setTextoDeOpcionSeleccionada(info[2]+" - "+info[8]);
        this.getSelectorUnidad().muestraComponente();
        
        document.getElementById(this.nombre_instancia+"_campo_descripcion").value = info[3];
        document.getElementById(this.nombre_instancia+"_campo_valor_unitario").value = info[4];
        document.getElementById(this.nombre_instancia+"_campo_cantidad").value = info[5];
        document.getElementById(this.nombre_instancia+"_campo_descuento").value = info[6];
        
        this.idProductoServicio = info[1];
        this.idUnidadSat = info[2];
        
        this.calculaTotal();
    }
}