var FormaDatosFacturacion = function(){

    this.datos = null;
    this.iddatos_facturacion = 0;
    
    this.selectorRegimenFiscal = null;
    this.getSelectorRegimenFiscal = function(){
        if(!this.selectorRegimenFiscal){
            this.selectorRegimenFiscal = new SelectorDeOpcion();
            this.selectorRegimenFiscal.setVisualizacion(1);
        }
        return this.selectorRegimenFiscal;
    }
    
    this.idRegimenFiscal = 0;
    
    this.catalogoRegimenFiscal = null;
    this.getCatalogoRegimenFiscal = function(){
        if(!this.catalogoRegimenFiscal) this.catalogoRegimenFiscal = new CatalogoRegimenFiscal();
        return this.catalogoRegimenFiscal;
    }
    
    this.setIdDatosCaptura = function(iddatos){
        this.iddatos_facturacion = iddatos;
    }
    
    this.componente_padre = null;
    this.setComponentePadre = function(componente){
        this.componente_padre = componente;
    }
    
    this.procesaDatosFacturacionRecuperados = function(respuesta){
        console.log("respuesta datos recuperados: "+respuesta);
        
        this.datos = respuesta.split("|");
        this.editaInfo();
    }
    
    this.procesaDatosFacturacionRecuperadosParaSoloLectura = function(respuesta){
        //console.log("respuesta datos recuperados: "+respuesta);
        
        this.datos = respuesta.split("|");
        this.muestraInfoParaSoloLectura();
    }
    
    this.editaInfo = function(){
        if(!this.datos){
            getResultadoAjax("opcion=22&idDatosFacturacion="+this.iddatos_facturacion, this.nombre_instancia+".procesaDatosFacturacionRecuperados", null);
            return;
        }
        
        //iddatosfacturacion, nombre, rfc, calle, exterior, interior, referencia, colonia, localidad, municipio, estado, pais, cp, telefono, email, fax, cuenta, estatus, claveRegimen, descripcionClave
        
        this.idRegimenFiscal = this.datos[18];
        this.getSelectorRegimenFiscal().setTextoDeOpcionSeleccionada(this.datos[18]+" - "+this.datos[19]);
        this.getSelectorRegimenFiscal().muestraComponente();
        
        //seteamos los datos recuperados en la forma de facturacion
        document.getElementById(this.nombre_instancia+"_campo_nombre").value = this.datos[1];
        
        document.getElementById(this.nombre_instancia+"_campo_rfc").value = this.datos[2];
        
        document.getElementById(this.nombre_instancia+"_calle").value = this.datos[3];
        
        document.getElementById(this.nombre_instancia+"_exterior").value = this.datos[4];
        
        document.getElementById(this.nombre_instancia+"_interior").value = this.datos[5];
        
        document.getElementById(this.nombre_instancia+"_campo_referencia").value = this.datos[6];
        
        document.getElementById(this.nombre_instancia+"_campo_colonia").value = this.datos[7];
        
        document.getElementById(this.nombre_instancia+"_campo_localidad").value = this.datos[8];
        
        document.getElementById(this.nombre_instancia+"_campo_municipio").value = this.datos[9];
        
        document.getElementById(this.nombre_instancia+"_campo_estado").value = this.datos[10];
        
        document.getElementById(this.nombre_instancia+"_campo_pais").value = this.datos[11];
        
        document.getElementById(this.nombre_instancia+"_campo_cp").value = this.datos[12];
        
        document.getElementById(this.nombre_instancia+"_campo_telefono").value = this.datos[13];
        
        document.getElementById(this.nombre_instancia+"_campo_email").value = this.datos[14];
        
        document.getElementById(this.nombre_instancia+"_campo_fax").value = this.datos[15];
        
        document.getElementById(this.nombre_instancia+"_campo_cuenta").value = this.datos[16];
    }
    
    this.muestraInfoParaSoloLectura = function(){
        if(!this.datos){
            getResultadoAjax("opcion=199&iddatos_facturacion="+this.iddatos_facturacion, this.nombre_instancia+".procesaDatosFacturacionRecuperadosParaSoloLectura", null);
            return;
        }
        
        //iddatosfacturacion, nombre, rfc, calle, exterior, interior, referencia, colonia, localidad, municipio, estado, pais, cp, telefono, email, fax, estatus
        
        //seteamos los datos recuperados en la forma de facturacion
        /*var contenido = "Nombre: <b>"+this.datos[1]+"</b><br/>";
        contenido += "R.F.C.: <b>"+this.datos[2]+"</b><br/>";
        contenido += "Calle <b>"+this.datos[2]+"</b> No. <b>"+this.datos[4]+"</b> Int. <b>"+this.datos[5]+"</b> Colonia: <b>"+this.datos[7]+"</b> Localidad: <b>"+this.datos[8]+"</b> Mpio: <b>"+this.datos[9]+"</b> Estado <b>"+this.datos[10]+"</b> C.P. <b>"+this.datos[12]+"</b><br/>";
        contenido += "Tel&eacute;fono: <b>"+this.datos[13]+"</b><br/>";
        contenido += "e-mail: <b>"+this.datos[14]+"</b>";*/
        
        var contenido = "<b>"+this.datos[1]+"<br/>";
        contenido += ""+this.datos[2]+"<br/>";
        contenido += ""+this.datos[3]+" "+this.datos[4]+" "+this.datos[5]+" "+this.datos[7]+" "+this.datos[8]+" "+this.datos[9]+" "+this.datos[10]+" "+this.datos[12]+"<br/>";
        contenido += ""+this.datos[13]+"<br/>";
        contenido += ""+this.datos[14]+"</b>";
        
        this.contenedor.innerHTML = contenido;
    }
    
    this.getDatos = function(){
        return this.datos;
    }
    
    this.muestraComponente = function(){
        var contenido = "<div><br/>";
        
        if(this.iddatos_facturacion == 0) contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_subtitulo_componente'>Nuevos Datos de Facturacion</div>";
        else contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_subtitulo_componente'>Editando Datos de Facturacion</div>";
        
        contenido += "<table align='center' cellpadding='0' cellspacing='0' width='100%' border='0'>";
        contenido += "<tr>";
        contenido += "<td align='center' valign='top' width='100%'>";
        
        contenido += "<br/>";
        contenido += "<div class='texto_forma'>Régimen Fiscal <span class='azul'>*</span></div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_selector_regimen_fiscal'></div>";
        contenido += "<br/>";
        
        contenido += "</td>";
        contenido += "</tr>";
        contenido += "<tr>";
        contenido += "<td align='center' valign='top' width='100%'>";
        
        contenido += "<div class='texto_forma'>Nombre o raz&oacute;n social <span class='azul'>*</span></div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_nombre' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe el nombre o razon social para facturacion\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        
        contenido += "</td>";
        contenido += "</tr>";
        contenido += "</table>";
        
        
        contenido += "<table align='center' cellpadding='3' cellspacing='3' width='100%' border='0'>";
        contenido += "<tr>";
        contenido += "<td align='center' valign='top' width='50%'>";
        
        contenido += "<div class='texto_forma'>RFC <span class='azul'>*</span></div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_rfc' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"R.F.C. para facturacion\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        
        contenido += "</td>";
        contenido += "<td align='center' valign='top' width='50%'>";
        
        contenido += "<div id='"+this.nombre_instancia+"_label_calle' class='texto_forma'>Calle <span class='azul'>*</span>";
        contenido += "</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_calle' value='' class='campo_forma' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe la calle\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        
        contenido += "</td>";
        contenido += "</tr></table>";
        
        //Calle, no ext, no int
        contenido += "<table id='"+this.nombre_instancia+"_tabla_calle_ext_int' align='center' width='100%' cellpadding='3' cellspacing='3' border='0' class=''>";
        contenido += "<tr>";
        contenido += "<td align='center' width='25%'>";

        contenido += "<div id='"+this.nombre_instancia+"_label_exterior' class='texto_forma'>No. Ext. <span class='azul'>*</span>";
        contenido += "</div>";

        contenido += "<input type='text' id='"+this.nombre_instancia+"_exterior' value='' class='campo_forma' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe el numero exterior\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        contenido += "";

        contenido += "</td>";
        contenido += "<td align='center' width='25%'>";

        contenido += "<div id='"+this.nombre_instancia+"_label_interior' class='texto_forma'>No. Int.";
        contenido += "</div>";

        contenido += "<input type='text' id='"+this.nombre_instancia+"_interior' value='' class='campo_forma' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe el numero interior si hay\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        contenido += "";

        contenido += "</td>";
        contenido += "<td align='center' width='50%'>";

        contenido += "<div class='texto_forma'>Colonia <span class='azul'>*</span></div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_colonia' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe la colonia\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";

        contenido += "</td>";
        contenido += "</tr>";
        contenido += "</table>";
        
        //referencia, colonia
        contenido += "<table align='center' cellpadding='3' cellspacing='3' width='100%' border='0'>";
        contenido += "<tr>";
        contenido += "<td align='center' valign='top' width='50%'>";
        
        contenido += "<div class='texto_forma'>Referencia </div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_referencia' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe una referencia\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        
        contenido += "</td>";
        contenido += "<td align='center' valign='top' width='50%'>";
        
        contenido += "<div class='texto_forma'>Localidad <span class='azul'>*</span></div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_localidad' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe una localidad\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        
        contenido += "</td>";
        contenido += "</tr></table>";
        
        contenido += "<div id='"+this.nombre_instancia+"_u1' class='ayuda_campos'></div>";
        
        //localidad, municipio
        contenido += "<table align='center' cellpadding='3' cellspacing='3' width='100%' border='0'>";
        contenido += "<tr>";
        contenido += "<td align='center' valign='top' width='50%'>";
        
        contenido += "<div class='texto_forma'>Municipio <span class='azul'>*</span></div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_municipio' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe un municipio\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        
        contenido += "</td>";
        contenido += "<td align='center' valign='top' width='50%'>";
        
        contenido += "<div class='texto_forma'>Estado <span class='azul'>*</span></div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_estado' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe un estado\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        
        contenido += "</td>";
        contenido += "</tr></table>";
        
        //estado, pais
        contenido += "<table align='center' cellpadding='3' cellspacing='3' width='100%' border='0'>";
        contenido += "<tr>";
        contenido += "<td align='center' valign='top' width='50%'>";
        
        contenido += "<div class='texto_forma'>Pa&iacute;s <span class='azul'>*</span></div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_pais' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe un pais\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += "</td>";
        contenido += "<td align='center' valign='top' width='50%'>";
        
        contenido += "<div class='texto_forma'>C.P. <span class='azul'>*</span></div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_cp' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe el codigo postal\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        
        contenido += "</td>";
        contenido += "</tr></table>";
        
        //cp, telefono
        contenido += "<table align='center' cellpadding='3' cellspacing='3' width='100%' border='0'>";
        contenido += "<tr>";
        contenido += "<td align='center' valign='top' width='50%'>";
        
        contenido += "<div class='texto_forma'>Tel&eacute;fono </div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_telefono' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe un numero de telefono\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        
        contenido += "</td>";
        contenido += "<td align='center' valign='top' width='50%'>";
        
        contenido += "<div class='texto_forma'>e-mail</div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_email' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe una direccion de correo electronico. Aquí se enviará el cdfi generado\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        
        contenido += "</td>";
        contenido += "</tr></table>";
        
        //email fax
        contenido += "<table align='center' cellpadding='3' cellspacing='3' width='100%' border='0'>";
        contenido += "<tr>";
        contenido += "<td align='center' valign='top' width='50%'>";
        
        contenido += "<div class='texto_forma'>Fax</div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_fax' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe un numero de telefono para envio de fax\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        
        contenido += "</td>";
        contenido += "<td align='center' valign='top' width='50%'>";
        
        contenido += "<div class='texto_forma'>No. de Cuenta</div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_cuenta' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe el número de cuenta de pago\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        
        contenido += "</td>";
        contenido += "</tr></table>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_guardar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a></div>";
        
        contenido += "<br/></div><br/>";
        
        this.contenedor.innerHTML = contenido;
        
        this.getSelectorRegimenFiscal().setComponente(document.getElementById(this.nombre_instancia+"_zona_selector_regimen_fiscal"), this.nombre_instancia+".getSelectorRegimenFiscal()");
        
        if(this.idRegimenFiscal == 0) this.getSelectorRegimenFiscal().setTextoDeOpcionSeleccionada("Selecciona Regimen Fiscal");
        this.getSelectorRegimenFiscal().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoRegimenFiscal()");
        
        this.getSelectorRegimenFiscal().muestraComponente();
    }
    
    this.muestraCatalogoRegimenFiscal = function(){
        this.getCatalogoRegimenFiscal().setComponente(document.getElementById(this.nombre_instancia+"_zona_selector_regimen_fiscal"), this.nombre_instancia+".getCatalogoRegimenFiscal()");
        this.getCatalogoRegimenFiscal().setFuncionElementoSeleccionado(this.nombre_instancia+".regimenSeleccionado()");
        this.getCatalogoRegimenFiscal().muestraComponente();
    }
    
    this.regimenSeleccionado = function(){
        var datos = this.getCatalogoRegimenFiscal().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorRegimenFiscal().setTextoDeOpcionSeleccionada(datos[1]+" - "+datos[2]);
            this.idRegimenFiscal = datos[1];
            this.getSelectorRegimenFiscal().muestraComponente();
        }
    }
    
    this.validaForma = function(){
        if(this.idRegimenFiscal == 0){
            alert("Debes seleccionar el régimen fiscal!");
            return;
        }
        
        var nombre = document.getElementById(this.nombre_instancia+"_campo_nombre");
        if(nombre.value == " " || nombre.value == ""){
            nombre.focus();
            return false;
        }
        
        var rfc = document.getElementById(this.nombre_instancia+"_campo_rfc");
        if(rfc.value == " " || rfc.value == ""){
            rfc.focus();
            return false;
        }
        
        var calle = document.getElementById(this.nombre_instancia+"_calle");
        if(calle.value == " " || calle.value == ""){
            calle.focus();
            return false;
        }
        
        var exterior = document.getElementById(this.nombre_instancia+"_exterior");
        if(exterior.value == " " || exterior.value == ""){
            exterior.focus();
            return false;
        }
        
        var interior = document.getElementById(this.nombre_instancia+"_interior");
        
        var referencia = document.getElementById(this.nombre_instancia+"_campo_referencia");
        
        var colonia = document.getElementById(this.nombre_instancia+"_campo_colonia");
        if(colonia.value == " " || colonia.value == ""){
            colonia.focus();
            return false;
        }
        
        var localidad = document.getElementById(this.nombre_instancia+"_campo_localidad");
        if(localidad.value == " " || localidad.value == ""){
            localidad.focus();
            return false;
        }
        
        var municipio = document.getElementById(this.nombre_instancia+"_campo_municipio");
        if(municipio.value == " " || municipio.value == ""){
            municipio.focus();
            return false;
        }
        
        var estado = document.getElementById(this.nombre_instancia+"_campo_estado");
        if(estado.value == " " || estado.value == ""){
            estado.focus();
            return false;
        }
        
        var pais = document.getElementById(this.nombre_instancia+"_campo_pais");
        if(pais.value == " " || pais.value == ""){
            pais.focus();
            return false;
        }
        
        var cp = document.getElementById(this.nombre_instancia+"_campo_cp");
        if(cp.value == " " || cp.value == ""){
            cp.focus();
            return false;
        }
        
        var telefono = document.getElementById(this.nombre_instancia+"_campo_telefono");
        //calle, exterior, interior, campo_referencia, campo_colonia, campo_localidad, campo_municipio, campo_estado, campo_pais, campo_cp, campo_telefono, campo_email, campo_fax
        var email = document.getElementById(this.nombre_instancia+"_campo_email");
        
        var fax = document.getElementById(this.nombre_instancia+"_campo_fax");
        
        var cuenta = document.getElementById(this.nombre_instancia+"_campo_cuenta");
        
        this.datos = this.iddatos_facturacion+"|"+nombre.value+"|"+rfc.value+"|"+calle.value+"|"+exterior.value+"|"+interior.value+"|"+referencia.value+"|"+colonia.value+"|"+localidad.value+"|"+municipio.value+"|"+estado.value+"|"+pais.value+"|"+cp.value+"|"+telefono.value+"|"+email.value+"|"+fax.value+"|"+cuenta.value+"|"+this.idRegimenFiscal;
        //iddatos_facturacion, nombre, rfc, calle, exterior, interior, referencia, colonia, localidad, municipio, estado, pais, cp, telefono, email, fax, cuenta, claveRegimenFiscal
        //console.log("datos: "+this.datos);
        
        getResultadoAjax("opcion=21&info="+this.datos, this.nombre_instancia+".procesaRespuesta", document.getElementById(this.nombre_instancia+"_zona_guardar"));
    }
    
    this.procesaRespuesta = function(respuesta){
        console.log("respuesta guardar datos facturacion: "+respuesta);
        
        if(!isNaN(respuesta) && respuesta > 0){
            var datos = this.datos.split("|");
            if(this.iddatos_facturacion == 0) this.datos = respuesta+"|"+datos[1]+"|"+datos[2]+"|"+datos[3];
            
            alert("Datos de facturacion guardados!");
            if(this.componente_padre) this.componente_padre.datosFacturacionAgregados(this.datos);
            else{
                this.muestraComponente();
                this.datos = null;
                this.editaInfo();
            }
        }else{
            alert("No se pudo guardar los datos de facturacion, intenta mas tarde!");
            document.getElementById(this.nombre_instancia+"_zona_guardar").innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a>";
        }
    }
}