var FormaEmpresa = function(){
    this.idEmpresa = 0;
    this.setIdEmpresa = function(idempresa){
        this.idEmpresa = idempresa;
    }
    
    this.iddatos_facturacion = 0;
    
    this.selector_datos_facturacion = null;
    this.getSelectorDatosFacturacion = function(){
        if(!this.selector_datos_facturacion){
            this.selector_datos_facturacion = new CatalogoSelectorDatosFacturacion();
            this.selector_datos_facturacion.setTipoVisualizacion(1);
        }
        return this.selector_datos_facturacion;
    }
    
    this.info = null;
    this.selector_fecha = null;
    
    this.muestraComponente = function(){
        var contenido = "<br/>";    
        
        if(this.idEmpresa == 0) contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Nueva Empresa</div>";
        else contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Editando Empresa</div>";
        
        contenido += "<div class='separador'></div>";
        contenido += "<br/>";    
        
        contenido += "<div class='texto_forma'>Nombre completo de la empresa</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_nombre' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el nombre completo de la empresa\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        contenido += "<br/>";
        
        contenido += "<br/>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_datos_facturacion'></div>";
        
        contenido += "<br/>";
        
        contenido += "<div class='ayuda_campos' id='"+this.nombre_instancia+"_a1'></div>";
        contenido += "<br/>";
        
        //Número de certificado y fecha de vencimiento
        contenido += '<div class="row center-align">';
        contenido += '<div class="col l6 center-align">';
        
        contenido += "<div class='texto_forma'>No Certificado</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_numero_certificado' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el número de certificado de tu CSD\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '<div class="col l6 center-align">';
        
        contenido += "<div class='texto_forma'>Fecha de Vencimiento</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_fecha_vencimiento'></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += "<br/>";
        contenido += "<div class='texto_forma'>Certificado en formato texto</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_certificado_text' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Ingresa el certificado en formato texto\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        contenido += "<br/>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_boton_guardar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a></div>";
        
        contenido += "<br/>";
        
        this.contenedor.innerHTML = contenido;
        this.getSelectorDatosFacturacion().setComponente(document.getElementById(this.nombre_instancia+"_zona_datos_facturacion"), this.nombre_instancia+".getSelectorDatosFacturacion()");
        this.getSelectorDatosFacturacion().setFuncionDatosFacturacionSeleccionada(this.nombre_instancia+".actualizaDatosFacturacion()");
        if(this.iddatos_facturacion == 0) this.getSelectorDatosFacturacion().muestraComponenteSelector();
        else this.getSelectorDatosFacturacion().setIdDatosFacturacion(this.iddatos_facturacion);
        
        //selector de fecha y hora, por defecto pone los datos actuales
        this.selector_fecha = new SelectorFechas();
        this.selector_fecha.fecha_actual = new Date();
        this.selector_fecha.setSelectorFecha(document.getElementById(this.nombre_instancia+"_zona_fecha_vencimiento"), this.nombre_instancia+".selector_fecha");
        this.selector_fecha.fecha_modificada = true;
        this.selector_fecha.muestraComboFechas();
    }
    
    this.actualizaDatosFacturacion = function(){
        
    }
    
    this.editaComponente = function(){
        if(!this.info){
            getResultadoAjax("opcion=16&idempresa="+this.idEmpresa, this.nombre_instancia+".procesaInfoRecuperada", this.contenedor);
            return;
        }
        
        this.muestraComponente();
        //idEmpresa, nombre, estatus, idDatosFacturacion, noCertificado, fechaVencimiento, textoCertificado
        
        document.getElementById(this.nombre_instancia+"_campo_nombre").value = this.info[1];
        
        this.iddatos_facturacion = this.info[3];
        this.getSelectorDatosFacturacion().setIdDatosFacturacion(this.iddatos_facturacion);
        document.getElementById(this.nombre_instancia+"_numero_certificado").value = this.info[4];
        campoCertificadoText = document.getElementById(this.nombre_instancia+"_certificado_text").value = this.info[6];
        
        if(this.info[5] != " " && this.info[5] != ""){
            var partesFecha = this.info[5].split(" ");
            this.selector_fecha.setFechaDesdeDB(partesFecha[0]);
            this.selector_fecha.fecha_modificada = true;      
            console.log("fecha modificada!");
        }
    }
    
    this.procesaInfoRecuperada = function(respuesta){
        console.log("info: "+respuesta);
        
        this.info = respuesta.split("|");
        this.editaComponente();
    }
    
    this.getZonaBotonGuardar = function(){
        return document.getElementById(this.nombre_instancia+"_zona_boton_guardar");
    }
    
    this.validaForma = function(){
        var campoNombre = document.getElementById(this.nombre_instancia+"_campo_nombre");
        if(campoNombre.value.length == 0 || campoNombre.value == "" || campoNombre.value == " "){
            campoNombre.focus();
            return;
        }
        
        //si no tiene datos de facturación, entonces no se puede guardar
        if(this.getSelectorDatosFacturacion().getIdDatosFacturacion() == 0){
            muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar datos de facturación para la empresa!");
            return;
        }
        
        var campoNoCertificado = document.getElementById(this.nombre_instancia+"_numero_certificado");
        if(campoNoCertificado.value.length == 0 || campoNoCertificado.value == "" || campoNoCertificado.value == " "){
            campoNoCertificado.focus();
            return;
        }
        
        var fechaBD = this.selector_fecha.getFechaActualParaDB();
        if(!fechaBD){
            muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar la fecha de vencimiento del certificado!");
            return;
        }
        
        var campoCertificadoText = document.getElementById(this.nombre_instancia+"_certificado_text");
        if(campoCertificadoText.value.length == 0 || campoCertificadoText.value == "" || campoCertificadoText.value == " "){
            campoCertificadoText.focus();
            return;
        }
        
        var info = this.idEmpresa+"|"+campoNombre.value+"|"+this.getSelectorDatosFacturacion().getIdDatosFacturacion()+"|"+campoNoCertificado.value+"|"+fechaBD+"|"+campoCertificadoText.value;
        //idempresa, nombrecompleto, datosFacturacion, noCertificado, fechaDB, certificadoText
        
        console.log("info: "+info);
        
        getResultadoAjax("opcion=15&info="+info, this.nombre_instancia+".respuestaElementoGuardado", this.getZonaBotonGuardar());
    }
    
    this.respuestaElementoGuardado = function(respuesta){
        console.log("respuesta guardar: "+respuesta);
        this.getZonaBotonGuardar().innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a>";
        
        if(respuesta > 0 && !isNaN(respuesta)){
            alert("Empresa guardada!");
            if(this.idEmpresa == 0) this.idEmpresa = respuesta;
        }else{
            alert("No se pudo guardar la empresa, por favor intenta más tarde!");
        }
    }
}