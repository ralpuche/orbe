var FormaGastoInterno = function(){
    
    this.idGastoInterno = 0;
    this.setIdGastoInterno = function(idgastointerno){
        this.idGastoInterno = idgastointerno;
    }
    
    this.idConcepto = 0;
    
    this.info = null;
    this.selector_hora = null;
    this.selector_fecha = null;
    
    this.catalogoSelectorConceptos = null;
    this.getCatalogoSelectorConceptos = function(){
        if(!this.catalogoSelectorConceptos){
            this.catalogoSelectorConceptos = new CatalogoSelectorConceptos();
        }
        return this.catalogoSelectorConceptos;
    }
    
    this.muestraComponente = function(){
        var contenido = "<br/>";    
        
        if(this.idGastoInterno == 0) contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Nuevo Gasto Interno</div>";
        else contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Editando Gasto Interno</div>";
        
        contenido += "<div class='separador'></div>";
        contenido += "<br/>";    
        
        //fecha y hora
        contenido += '<div class="row center-align">';
        contenido += '<div class="col l6 center-align">';
        
        contenido += "<div class='texto_forma'>Fecha</div>";
        contenido += "<br/><div id='"+this.nombre_instancia+"_selector_fecha'></div>";
        
        contenido += '</div>';
        contenido += '<div class="col l6 center-align">';
        
        contenido += "<div class='texto_forma'>Hora</div>";
        contenido += "<div id='"+this.nombre_instancia+"_selector_hora'></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += "<br/>";
        contenido += "<div class='ayuda_campos' id='"+this.nombre_instancia+"_a1'></div>";
        contenido += "<br/>";
        
        //concepto y cantidad
        contenido += '<div class="row center-align">';
        contenido += '<div class="col l6 center-align">';
        
        contenido += "<div class='texto_forma'>Concepto</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_concepto' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el concepto para este gasto interno.\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '<div class="col l6 center-align">';
        
        contenido += "<div class='texto_forma'>Cantidad</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_cantidad' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el total de este gasto interno. Solo se permiten números y . decimal\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_boton_guardar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a></div>";
        
        contenido += "<br/>";
        
        this.contenedor.innerHTML = contenido;
        
        
        //selector de fecha y hora, por defecto pone los datos actuales
        this.selector_fecha = new SelectorFechas();
        this.selector_fecha.fecha_actual = new Date();
        this.selector_fecha.setSelectorFecha(document.getElementById(this.nombre_instancia+"_selector_fecha"), this.nombre_instancia+".selector_fecha");
        this.selector_fecha.fecha_modificada = true;
        this.selector_fecha.muestraComboFechas();
        
        this.selector_hora = new SelectorHora();
        this.selector_hora.setSelectorHora(document.getElementById(this.nombre_instancia+"_selector_hora"), -1, null, this.nombre_instancia+".selector_hora");
    }
    
    this.onConceptoSeleccionadoChange = function(){
        this.idConcepto = this.getCatalogoSelectorConceptos().getIdConceptoSeleccionado();
    }
    
    this.actualizaDatosFacturacion = function(){
        
    }
    
    this.editaComponente = function(){
        if(!this.info){
            getResultadoAjax("opcion=31&idgastointerno="+this.idGastoInterno, this.nombre_instancia+".procesaInfoRecuperada", this.contenedor);
            return;
        }
        
        this.muestraComponente();
        //idGastosInterno, concepto, fecha, hora, cantidad, idUsuario, estatus
        
        this.selector_fecha.setFechaDesdeDB(this.info[2]);
        this.selector_hora.setHora(this.info[3]);
        document.getElementById(this.nombre_instancia+"_campo_concepto").value = this.info[1];
        document.getElementById(this.nombre_instancia+"_campo_cantidad").value = this.info[4];
    }
    
    this.procesaInfoRecuperada = function(respuesta){
        console.log("info: "+respuesta);
        
        this.info = respuesta.split("|");
        this.editaComponente();
    }
    
    this.getZonaBotonGuardar = function(){
        return document.getElementById(this.nombre_instancia+"_zona_boton_guardar");
    }
    
    this.validaForma = function(){
        var fecha = this.selector_fecha.getFechaActualParaDB();
        var hora = this.selector_hora.getHora();
        
        var campoConcepto = document.getElementById(this.nombre_instancia+"_campo_concepto");
        console.log(campoConcepto);
        if(campoConcepto.value == "" || campoConcepto.value == " "){
            campoConcepto.focus();
            return;
        }
        
        var campoCantidad = document.getElementById(this.nombre_instancia+"_campo_cantidad");
        if(campoCantidad.value.length == 0 || campoCantidad.value == "" || campoCantidad.value == " " || !parseFloat(campoCantidad.value)){
            campoCantidad.focus();
            return;
        }
        
        var info = this.idGastoInterno+"|"+fecha+"|"+hora+"|"+campoConcepto.value+"|"+campoCantidad.value;
        //idGasto, fecha, hora, concepto, cantidad
        
        getResultadoAjax("opcion=30&info="+info, this.nombre_instancia+".respuestaElementoGuardado", this.getZonaBotonGuardar());
    }
    
    this.respuestaElementoGuardado = function(respuesta){
        console.log("respuesta guardar: "+respuesta);
        this.getZonaBotonGuardar().innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a>";
        
        if(respuesta > 0 && !isNaN(respuesta)){
            alert("Gasto Interno guardado!");
            if(this.idGastoInterno == 0) this.idGastoInterno = respuesta;
            this.getControladorNavegacion().botonCerrarPresionado();
        }else{
            alert("No se pudo guardar el gasto interno, por favor intenta más tarde!");
        }
    }
}