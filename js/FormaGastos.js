var FormaGastos = function(){
    
    this.idGasto = 0;
    this.setIdGasto = function(idgasto){
        this.idGasto = idgasto;
    }
    
    this.info = null;
    
    this.selector_fecha = null;
    
    this.clienteSeleccionado = 0;
    this.setClienteSeleccionado = function(idcliente){
        this.clienteSeleccionado = idcliente;
    }
    
    this.catalogoClientes = null;
    this.getCatalogoClientes = function(){
        if(!this.catalogoClientes) {
            this.catalogoClientes = new CatalogoClientes();
            this.catalogoClientes.setSoloSeleccion(1);
        }
        return this.catalogoClientes;
    }
    
    this.setCatalogoClientes = function(catalogo){
        this.catalogoClientes = catalogo;
    }
    
    this.selectorClientes = null;
    this.getSelectorClientes = function(){
        if(!this.selectorClientes){
            this.selectorClientes = new SelectorDeOpcion();
            this.selectorClientes.setVisualizacion(1);
        }
        return this.selectorClientes;
    }
    
    this.setSelectorClientes = function(selector){
        this.selectorClientes = selector;
    }
    
    //promotores
    this.idPromotorSeleccionado = 0;
    this.selectorPromotor = null;
    this.getSelectorPromotor = function(){
        if(!this.selectorPromotor){
            this.selectorPromotor = new SelectorDeOpcion();
            this.selectorPromotor.setVisualizacion(1);
        }
        return this.selectorPromotor;
    }
    
    this.catalogoPromotores = null;
    this.getCatalogoPromotores = function(){
        if(!this.catalogoPromotores){
            this.catalogoPromotores = new CatalogoUsuarios();
            this.catalogoPromotores.setSoloSeleccion(1);
            this.catalogoPromotores.setFiltroTipoUsuarios(3);
        }
        return this.catalogoPromotores;
    }
    
    //empresa
    this.idEmpresaSeleccionada = 0;
    this.selectorEmpresa = null;
    this.getSelectorEmpresa = function(){
        if(!this.selectorEmpresa){
            this.selectorEmpresa = new SelectorDeOpcion();
            this.selectorEmpresa.setVisualizacion(1);
        }
        return this.selectorEmpresa;
    }
    
    this.catalogoEmpresas = null;
    this.getCatalogoEmpresas = function(){
        if(!this.catalogoEmpresas){
            this.catalogoEmpresas = new CatalogoEmpresas();
            this.catalogoEmpresas.setSoloSeleccion(1);
        }
        return this.catalogoEmpresas;
    }
    
    //Bancos
    this.idBancoSeleccionado = 0;
    this.selectorBancos = null;
    this.getSelectorBancos = function(){
        if(!this.selectorBancos){
            this.selectorBancos = new SelectorDeOpcion();
            this.selectorBancos.setVisualizacion(1);
        }
        return this.selectorBancos;
    }
    
    this.catalogoBancos = null;
    this.getCatalogoBancos = function(){
        if(!this.catalogoBancos){
            this.catalogoBancos = new CatalogoBancos();
            this.catalogoBancos.setSoloSeleccion(1);
        }
        return this.catalogoBancos;
    }
    
    //Catalogo de Conceptos
    this.catalogoConceptos = null;
    this.getCatalogoDeConceptos = function(){
        if(!this.catalogoConceptos){
            this.catalogoConceptos = new CatalogoConceptosDeGasto();
            this.catalogoConceptos.setIdGasto(this.idGasto);
        }
        return this.catalogoConceptos;
    }
    
    //Visor de Facturación
    this.visor_facturas = null;
    this.getComponenteVisorDeFacturas = function(){
        if(!this.visor_facturas){
            this.visor_facturas = new ComponenteVisorFactura();
            this.visor_facturas.setComponente(null, this.nombre_instancia+".getComponenteVisorDeFacturas()");
            this.visor_facturas.funcionFacturasActualizadas(this.nombre_instancia+".recargaFacturasEmitidas()");
        }
        return this.visor_facturas;
    }
    
    //SELECTOR Y CATALOGO DE FORMAS DE PAGO
    this.idFormaPago = 1;
    this.bancarizado = false;
    this.selectorFormasPago = null;
    this.getSelectorFormasPago = function(){
        if(!this.selectorFormasPago){
            this.selectorFormasPago = new SelectorDeOpcion();
            this.selectorFormasPago.setVisualizacion(1);
        }
        return this.selectorFormasPago;
    }
    
    this.catalogoFormasPago = null;
    this.getCatalogoFormasPago = function(){
        if(!this.catalogoFormasPago){
            this.catalogoFormasPago = new CatalogoFormaPagoSat();
            this.catalogoFormasPago.setSoloSeleccion(1);
        }
        return this.catalogoFormasPago;
    }
    //SELECTOR Y CATALOGO DE FORMAS DE PAGO
    
    //SELECTOR Y CATALOGO DE MONEDAS
    this.idMoneda = "MXN";
    this.selectorMoneda = null;
    this.getSelectorMoneda = function(){
        if(!this.selectorMoneda){
            this.selectorMoneda = new SelectorDeOpcion();
            this.selectorMoneda.setVisualizacion(1);
        }
        return this.selectorMoneda;
    }
    
    this.catalogoMonedas = null;
    this.getCatalogoMonedas = function(){
        if(!this.catalogoMonedas){
            this.catalogoMonedas = new CatalogoMonedasSat();
            this.catalogoMonedas.setSoloSeleccion(1);
        }
        return this.catalogoMonedas;
    }
    //SELECTOR Y CATALOGO DE MONEDAS
    
    //SELECTOR Y CATALOGO DE USOS CFDI
    this.idUso = 0;
    this.selectorUsos = null;
    this.getSelectorUsos = function(){
        if(!this.selectorUsos){
            this.selectorUsos = new SelectorDeOpcion();
            this.selectorUsos.setVisualizacion(1);
        }
        return this.selectorUsos;
    }
    
    this.catalogoUsos = null;
    this.getCatalogoUsos = function(){
        if(!this.catalogoUsos){
            this.catalogoUsos = new CatalogoUsosCFDISat();
            this.catalogoUsos.setSoloSeleccion(1);
        }
        return this.catalogoUsos;
    }
    //SELECTOR Y CATALOGO DE USOS CFDI
    
    //Catalogo de facturas emitidas
    this.catalogoFacturas = null;
    this.getCatalogoFacturas = function(){
        if(!this.catalogoFacturas){
            this.catalogoFacturas = new CatalogoFacturasEmitidas();
            this.catalogoFacturas.setIdElemento(this.idGasto);
        }
        
        return this.catalogoFacturas;
    }
    //Catalogo de facturas emitidas
    
    this.muestraComponente = function(){
        var contenido = "<br/>";    
        
        if(this.idGasto == 0) contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Nuevo Gasto de Cliente</div>";
        else contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Editando Gasto de Cliente</div>";
        
        contenido += "<div class='separador'></div>";
        contenido += "<br/>";    
        
        //selector de cliente
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s12 m12 l12 center-align">';
        
        contenido += "<div class='texto_forma'>Cliente</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_clientes'></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        //promotor y empresa
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += "<div class='texto_forma'>Promotor</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_promotores'></div>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += "<div class='texto_forma'>Empresa</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_empresas'></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += "<br/>";
        contenido += "<div class='ayuda_campos' id='"+this.nombre_instancia+"_a1'></div>";
        contenido += "<br/>";
        
        //banco, fecha
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += "<div class='texto_forma'>Banco</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_bancos'></div>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += "<div class='texto_forma'>Fecha</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_fecha'></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        //Cantidad y porcentaje
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += "<div class='texto_forma'>Cantidad</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_cantidad' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el total de este gasto. Solo se permiten números y . decimal\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += "<div class='texto_forma'>Porcentaje</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_porcentaje' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el porcentaje para promotor de este gasto de cliente. Entre 0 y 1, por ejemplo 0.1 para 10%\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        //indicador ya facturada
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += "<table align='center' width='80%' border='0'>";
        contenido += "<tr>";
        contenido += "<td align='right' valign='top' width='5%'>";
        contenido += "</td>";
        contenido += "<td align='right' valign='top' width='30%'>";
        
        contenido += '<div class="texto_forma">¿Facturada?&nbsp;&nbsp;&nbsp;</div>';
        
        contenido += "</td>";
        contenido += "<td align='left' valign='top' width='60%'>";
        
        contenido += '<div class="switch">';
        contenido += '<label> No';
        contenido += '<input type="checkbox" id="'+this.nombre_instancia+'_ya_facturada">';
        contenido += '<span class="lever"></span>';
        contenido += 'Si</label></div>';
        
        contenido += "</td>";
        contenido += "</tr></table>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += '<div class="texto_forma">Método de Pago</div>';
        contenido += '<select id="'+this.nombre_instancia+'_selector_metodo_pago" class="browser-default" onchange="'+this.nombre_instancia+'.cambiaMetodoPago(this)">';
        contenido += '<option value="0">Selecciona método de pago</option>';
        contenido += '<option value="PUE">Pago en una sola Exhibición</option>';
        contenido += '<option value="PPD">Pago en Parcialidades</option>';
        contenido += '</select>';
        
        contenido += '</div>';
        contenido += '</div>';
        
        //campos que se muestran u ocultan dependiendo del método de pago
        contenido += "<div id='"+this.nombre_instancia+"_zona_campos_metodo_pago'>";
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += '<div class="texto_forma">Forma de Pago</div>';
        contenido += '<div id="'+this.nombre_instancia+'_zona_forma_pago"></div>';
        
        contenido += '</div>';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += '<div class="texto_forma">Moneda</div>';
        contenido += '<div id="'+this.nombre_instancia+'_zona_selector_moneda"></div>';
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_tipo_cambio'>";
        contenido += "<div class='texto_forma'>Tipo de Cambio</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_tipo_cambio' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe la cantidad del tipo de cambio de la moneda seleccionada con respecto del peso mexicano\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        contenido += "</div>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += '</div>';
        contenido += '</div>';
        contenido += '</div>';//fin de la zona de campos para pago PUE
        
        //uso de cfdi
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += "<div class='texto_forma'>Uso CFDI</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_uso_cfdi'></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += "<br/>";
        contenido += "<div class='ayuda_campos' id='"+this.nombre_instancia+"_a2'></div>";
        contenido += "<br/>";
        
        //selector para agregar un nuevo concepto a la factura
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s12 m12 l12 center-align">';
        
        contenido += "<br/>";
        contenido += "<div id='"+this.nombre_instancia+"_conceptos_facturacion'></div>";
        contenido += "<br/>";
        
        contenido += "<br/>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_facturas_emitidas'></div>";
        contenido += "<br/>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += "<br/>";
        
        //boton guardar y facturación global
        contenido += '<div class="row center-align">';
        contenido += '<div class="col l6 center-align">';
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_boton_guardar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a></div>";
        
        contenido += '</div>';
        contenido += '<div class="col l6 center-align">';
        
        //solo se pueden agregar los conceptos que tienen idGasto en 0, en otro caso no se pueden agregar por que ya están agregados, solo se pueden modificar
        if(this.info == null || this.info[21] == 0){
            contenido += "<div id='"+this.nombre_instancia+"_zona_boton_agregar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".facturaGlobal()' style='margin:0 auto 0 auto;'>Factura Global</a></div>";   
        }
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += "<br/>";
        
        this.contenedor.innerHTML = contenido;
        
        //selector de fecha, por defecto pone los datos actuales
        this.selector_fecha = new SelectorFechas();
        this.selector_fecha.fecha_actual = new Date();
        this.selector_fecha.setSelectorFecha(document.getElementById(this.nombre_instancia+"_zona_fecha"), this.nombre_instancia+".selector_fecha");
        this.selector_fecha.fecha_modificada = true;
        this.selector_fecha.muestraComboFechas();
        
        this.getSelectorClientes().setComponente(document.getElementById(this.nombre_instancia+"_zona_clientes"), this.nombre_instancia+".getSelectorClientes()");
        
        if(this.clienteSeleccionado == 0) this.getSelectorClientes().setTextoDeOpcionSeleccionada("Todos los clientes");
        this.getSelectorClientes().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoDeClientes()");
        
        this.getSelectorClientes().muestraComponente();
        
        //selector promotor
        this.getSelectorPromotor().setComponente(document.getElementById(this.nombre_instancia+"_zona_promotores"), this.nombre_instancia+".getSelectorPromotor()");
        
        if(this.idPromotorSeleccionado == 0) this.getSelectorPromotor().setTextoDeOpcionSeleccionada("Selecciona Promotor");
        this.getSelectorPromotor().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoPromotores()");
        
        this.getSelectorPromotor().muestraComponente();
        
        //selector empresa
        this.getSelectorEmpresa().setComponente(document.getElementById(this.nombre_instancia+"_zona_empresas"), this.nombre_instancia+".getSelectorEmpresa()");
        
        if(this.idEmpresaSeleccionada == 0) this.getSelectorEmpresa().setTextoDeOpcionSeleccionada("Selecciona Empresa");
        this.getSelectorEmpresa().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoEmpresas()");
        
        this.getSelectorEmpresa().muestraComponente();
        
        //selector bancos
        this.getSelectorBancos().setComponente(document.getElementById(this.nombre_instancia+"_zona_bancos"), this.nombre_instancia+".getSelectorBancos()");
        
        if(this.idBancoSeleccionado == 0) this.getSelectorBancos().setTextoDeOpcionSeleccionada("Selecciona Banco");
        this.getSelectorBancos().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoBancos()");
        
        this.getSelectorBancos().muestraComponente();
        
        //selector formas de pago sat
        this.getSelectorFormasPago().setComponente(document.getElementById(this.nombre_instancia+"_zona_forma_pago"), this.nombre_instancia+".getSelectorFormasPago()");
        
        if(this.idFormaPago == 0) this.getSelectorFormasPago().setTextoDeOpcionSeleccionada("Selecciona Forma de Pago");
        else if(this.idFormaPago == 1) this.getSelectorFormasPago().setTextoDeOpcionSeleccionada("EFECTIVO");
        
        this.getSelectorFormasPago().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoFormasDePago()");
        
        this.getSelectorFormasPago().muestraComponente();
        
        //selector monedas sat
        this.getSelectorMoneda().setComponente(document.getElementById(this.nombre_instancia+"_zona_selector_moneda"), this.nombre_instancia+".getSelectorMoneda()");
        
        if(this.idMoneda == 0) this.getSelectorMoneda().setTextoDeOpcionSeleccionada("Selecciona Moneda");
        else if(this.idMoneda == "MXN") this.getSelectorMoneda().setTextoDeOpcionSeleccionada("PESO MEXICANO (MXN)");
        
        this.getSelectorMoneda().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoMonedas()");
        
        this.getSelectorMoneda().muestraComponente();
        
        //selector usos cfdis
        this.getSelectorUsos().setComponente(document.getElementById(this.nombre_instancia+"_zona_uso_cfdi"), this.nombre_instancia+".getSelectorUsos()");
        if(this.idUso == 0) this.getSelectorUsos().setTextoDeOpcionSeleccionada("Selecciona Uso del CFDI");
        this.getSelectorUsos().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoUsosCFDI()");
        this.getSelectorUsos().muestraComponente();
        
        //catálogo de conceptos a facturar, solo se mostrará cuando se esté editando el gasto
        if(this.idGasto > 0){
            this.getCatalogoDeConceptos().setComponente(document.getElementById(this.nombre_instancia+"_conceptos_facturacion"), this.nombre_instancia+".getCatalogoDeConceptos()");
            this.getCatalogoDeConceptos().muestraComponente();   
            
            this.getCatalogoFacturas().setComponente(document.getElementById(this.nombre_instancia+"_zona_facturas_emitidas"), this.nombre_instancia+".getCatalogoFacturas()");
            this.getCatalogoFacturas().muestraComponente();
            this.getCatalogoFacturas().setFuncionElementoSeleccionado(this.nombre_instancia+".facturaSeleccionada()");
        }
    }
    
    this.facturaGlobal = function(){
        this.getComponenteVisorDeFacturas().setOpcionTipoFactura(1);
        this.getComponenteVisorDeFacturas().setIdElemento(this.idGasto);
        this.getComponenteVisorDeFacturas().setModo(1);
        this.getComponenteVisorDeFacturas().muestraComponente();
    }
    
    this.recargaFacturasEmitidas = function(){
        this.getCatalogoFacturas().muestraComponente();
    }
    
    this.onConceptoSeleccionadoChange = function(){
        this.idConcepto = this.getCatalogoSelectorConceptos().getIdConceptoSeleccionado();
    }
    
    this.actualizaDatosFacturacion = function(){
        
    }
    
    this.editaComponente = function(){
        if(!this.info){
            getResultadoAjax("opcion=35&idgasto="+this.idGasto, this.nombre_instancia+".procesaInfoRecuperada", this.contenedor);
            return;
        }
        
        this.idGasto = this.info[0];
        this.catalogoFacturas = null;
        this.muestraComponente();
        
        //return idGastoCliente, idPromotor, idEmpresa, idCliente, idBanco, fecha, hora, cantidadTotal, porcentaje, idUsuario, estatus, promotor, empresa, cliente, banco, metodoDePago, idFormaDePago, moneda, tipoCambio18, usoCFDI19, formaPago20, tieneXML21, descripcionUsoCFDI22
        
        this.selector_fecha.setFechaDesdeDB(this.info[5]);
        document.getElementById(this.nombre_instancia+"_campo_cantidad").value = this.info[7];
        document.getElementById(this.nombre_instancia+"_campo_porcentaje").value = this.info[8];
        
        this.getSelectorClientes().setTextoDeOpcionSeleccionada(this.info[13]);
        this.getSelectorBancos().setTextoDeOpcionSeleccionada(this.info[14]);
        this.getSelectorEmpresa().setTextoDeOpcionSeleccionada(this.info[12]);
        this.getSelectorPromotor().setTextoDeOpcionSeleccionada(this.info[11]);
        
        this.getSelectorClientes().muestraComponente();
        this.getSelectorBancos().muestraComponente();
        this.getSelectorEmpresa().muestraComponente();
        this.getSelectorPromotor().muestraComponente();
        
        this.clienteSeleccionado = this.info[3];
        this.idBancoSeleccionado = this.info[4];
        this.idEmpresaSeleccionada = this.info[2];
        this.idPromotorSeleccionado = this.info[1];
        
        if(this.info[10] == 2 || this.info[21] == 1) document.getElementById(this.nombre_instancia+"_ya_facturada").click();
        
        var comboMetodoPago = document.getElementById(this.nombre_instancia+"_selector_metodo_pago");
        
        console.log(this.info[15]+","+this.info[16]+","+this.info[17]+","+this.info[18]);
        if(this.info[15] == "PUE") comboMetodoPago.selectedIndex = 1;
        else comboMetodoPago.selectedIndex = 2;
        this.cambiaMetodoPago(comboMetodoPago);
        
        if(this.info[16] != "" && this.info[16] != " "){
            this.idFormaPago = this.info[16];
            this.getSelectorFormasPago().setTextoDeOpcionSeleccionada(this.info[20]);
            this.getSelectorFormasPago().muestraComponente();   
        }
        
        if(this.info[17] != " " && this.info[17] != ""){
            this.idMoneda = this.info[17];
            this.getSelectorMoneda().setTextoDeOpcionSeleccionada(this.idMoneda);
            this.getSelectorMoneda().muestraComponente();   
        }
        
        if(this.info[18] != " " && this.info[18] != ""){
            document.getElementById(this.nombre_instancia+"_tipo_cambio").value = this.info[18];
        }
        
        this.idUso = this.info[19];
        this.getSelectorUsos().setTextoDeOpcionSeleccionada(this.info[19]+" - "+this.info[22]);
    }
    
    this.procesaInfoRecuperada = function(respuesta){
        console.log("info: "+respuesta);
        
        this.info = respuesta.split("|");
        this.editaComponente();
    }
    
    this.getZonaBotonGuardar = function(){
        return document.getElementById(this.nombre_instancia+"_zona_boton_guardar");
    }
    
    this.validaForma = function(){
        var fecha = this.selector_fecha.getFechaActualParaDB();
        
        if(this.clienteSeleccionado == 0){
            muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar un cliente para este gasto");
            return;
        }
        
        if(this.idPromotorSeleccionado == 0){
            muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar un promotor para este gasto");
            return;
        }
        
        if(this.idEmpresaSeleccionada == 0){
            muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar una empresa para este gasto");
            return;
        }
        
        if(this.idBancoSeleccionado == 0){
            muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar un banco para este gasto");
            return;
        }
        
        var campoCantidad = document.getElementById(this.nombre_instancia+"_campo_cantidad");
        if(campoCantidad.value.length == 0 || campoCantidad.value == "" || campoCantidad.value == " " || !parseFloat(campoCantidad.value)){
            campoCantidad.focus();
            return;
        }
        
        var campoPorcentaje = document.getElementById(this.nombre_instancia+"_campo_porcentaje");
        if(campoPorcentaje.value.length <= 0 || campoPorcentaje.value == " " || isNaN(campoPorcentaje.value) || parseFloat(campoPorcentaje.value) < 0.0 || parseFloat(campoPorcentaje.value) > 1.0){
            campoPorcentaje.focus();
            return;
        }
        
        var facturada = document.getElementById(this.nombre_instancia+"_ya_facturada");
        
        var comboMetodoPago = document.getElementById(this.nombre_instancia+"_selector_metodo_pago");
        var formaPago = "99"; //por definir es por defecto
        var moneda = "MXN";
        var tipoCambio = "1";
        var metodoPago = comboMetodoPago.options[comboMetodoPago.selectedIndex].value;
        if(metodoPago == "0"){
            muestraAyudaCampo(this.nombre_instancia+"_a2", "Debes seleccionar un método de pago para este gasto");
            return;
        }
        
        if(metodoPago == "PUE"){
            formaPago = this.idFormaPago;
            moneda = this.idMoneda;
            var campoTipoCambio = document.getElementById(this.nombre_instancia+"_tipo_cambio");
            if(campoTipoCambio.value.length == 0 || campoTipoCambio.value == "" || campoTipoCambio.value == " "){
                campoTipoCambio.focus();
                return;
            }
            tipoCambio = campoTipoCambio.value;
            
            if(formaPago == 99){
                muestraAyudaCampo(this.nombre_instancia+"_a2", "Si el método de pago es 'Pago en una sola exhibición', la forma de pago no puede ser '99 - Por definir'");
                return;
            }
        }
        
        if(this.idUso == 0){
            muestraAyudaCampo(this.nombre_instancia+"_a2", "Debes seleccionar un uso para este CFDI");
            return;
        }
        
        var info = this.idGasto+"|"+this.clienteSeleccionado+"|"+this.idPromotorSeleccionado+"|"+this.idEmpresaSeleccionada+"|"+this.idBancoSeleccionado+"|"+campoCantidad.value+"|"+campoPorcentaje.value+"|"+fecha+"|"+((facturada.checked)?2:1)+"|"+metodoPago+"|"+formaPago+"|"+moneda+"|"+tipoCambio+"|"+this.idUso;
        
        console.log("info: "+info);
        //idGasto, idCliente, promotor, idEmpresa, idbanco, cantidad, porcentaje, fecha, estatus (1 pendiente de facturacion, 2 ya facturada), método de pago, forma de pago, moneda, tipo de cambio, uso cfdi
        
        getResultadoAjax("opcion=34&info="+info, this.nombre_instancia+".respuestaElementoGuardado", this.getZonaBotonGuardar());
    }
    
    this.respuestaElementoGuardado = function(respuesta){
        console.log("respuesta guardar: "+respuesta);
        this.getZonaBotonGuardar().innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a>";
        
        if(respuesta > 0 && !isNaN(respuesta)){
            alert("Gasto de Cliente guardado!");
            if(this.idGasto == 0) this.idGasto = respuesta;
            this.getControladorNavegacion().botonCerrarPresionado();
        }else{
            alert("No se pudo guardar el gasto de cliente, por favor intenta más tarde!");
        }
    }
    
    //FUNCIÓN CUANDO EL SELECTOR DE MÉTODO DE PAGO CAMBIA
    this.cambiaMetodoPago = function(selector){
        var zona = document.getElementById(this.nombre_instancia+"_zona_campos_metodo_pago");
        if(zona){
            if(selector.options[selector.selectedIndex].value == "PUE") $(zona).show();
            else $(zona).hide();
        }
    }
    //FUNCIÓN CUANDO EL SELECTOR DE MÉTODO DE PAGO CAMBIA
    
    /*
    METODOS PARA INVOCAR EL CATALOGO DE CLIENTES REGISTRADOS
    */
    this.muestraCatalogoDeClientes = function(){
        this.getCatalogoClientes().setComponente(null, this.nombre_instancia+".getCatalogoClientes()");
        this.getCatalogoClientes().setContenedorFormaFlotante(1);
        this.getCatalogoClientes().muestraContenedorModal('G');
        this.getCatalogoClientes().controlador_navegacion = null;
        
        this.catalogoClientes.setFuncionElementoSeleccionado(this.nombre_instancia+".clienteSeleccionadoDeCatalogo()");
        this.getCatalogoClientes().setComponente(this.getCatalogoClientes().getContenedorModal().getContenedorDeContenido(), this.getCatalogoClientes().nombre_instancia);
        this.getCatalogoClientes().muestraComponente();
    }
    
    this.clienteSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoClientes().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorClientes().setTextoDeOpcionSeleccionada(datos[1]);
            this.clienteSeleccionado = datos[0];
            this.getCatalogoClientes().getContenedorModal().ocultaComponente();
            if(this.componente_padre) this.componente_padre.setClienteSeleccionado(this.clienteSeleccionado);
        }
    }
    
    /*FUNCIONES PARA CATALOGO PROMOTORES*/
    this.muestraCatalogoPromotores = function(){
        console.log("mostrando catalogo de promotores");
        this.getCatalogoPromotores().setComponente(null, this.nombre_instancia+".getCatalogoPromotores()");
        this.getCatalogoPromotores().setContenedorFormaFlotante(1);
        this.getCatalogoPromotores().muestraContenedorModal('G');
        this.getCatalogoPromotores().controlador_navegacion = null;
        
        this.getCatalogoPromotores().setFuncionElementoSeleccionado(this.nombre_instancia+".promotorSeleccionadoDeCatalogo()");
        this.getCatalogoPromotores().setComponente(this.getCatalogoPromotores().getContenedorModal().getContenedorDeContenido(), this.getCatalogoPromotores().nombre_instancia);
        this.getCatalogoPromotores().muestraComponente();
    }
    
    this.promotorSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoPromotores().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorPromotor().setTextoDeOpcionSeleccionada(datos[1]);
            this.idPromotorSeleccionado = datos[0];
            this.getCatalogoPromotores().getContenedorModal().ocultaComponente();
        }
    }
    
    /*FUNCIONES PARA CATALOGO DE EMPRESAS*/
    this.muestraCatalogoEmpresas = function(){
        console.log("mostrando catalogo de empresas");
        this.getCatalogoEmpresas().setComponente(null, this.nombre_instancia+".getCatalogoEmpresas()");
        this.getCatalogoEmpresas().setContenedorFormaFlotante(1);
        this.getCatalogoEmpresas().muestraContenedorModal('G');
        this.getCatalogoEmpresas().controlador_navegacion = null;
        
        this.getCatalogoEmpresas().setFuncionElementoSeleccionado(this.nombre_instancia+".empresaSeleccionadoDeCatalogo()");
        this.getCatalogoEmpresas().setComponente(this.getCatalogoEmpresas().getContenedorModal().getContenedorDeContenido(), this.getCatalogoEmpresas().nombre_instancia);
        this.getCatalogoEmpresas().muestraComponente();
    }
    
    this.empresaSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoEmpresas().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorEmpresa().setTextoDeOpcionSeleccionada(datos[1]);
            this.idEmpresaSeleccionada = datos[0];
            this.getCatalogoEmpresas().getContenedorModal().ocultaComponente();
        }
    }
    
    /*FUNCIONES PARA BANCOS */
    this.muestraCatalogoBancos = function(){
        console.log("mostrando catalogo de bancos");
        this.getCatalogoBancos().setComponente(null, this.nombre_instancia+".getCatalogoBancos()");
        this.getCatalogoBancos().setContenedorFormaFlotante(1);
        this.getCatalogoBancos().muestraContenedorModal('G');
        this.getCatalogoBancos().controlador_navegacion = null;
        
        this.getCatalogoBancos().setFuncionElementoSeleccionado(this.nombre_instancia+".bancoSeleccionadoDeCatalogo()");
        this.getCatalogoBancos().setComponente(this.getCatalogoBancos().getContenedorModal().getContenedorDeContenido(), this.getCatalogoBancos().nombre_instancia);
        this.getCatalogoBancos().muestraComponente();
    }
    
    this.bancoSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoBancos().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorBancos().setTextoDeOpcionSeleccionada(datos[1]);
            this.idBancoSeleccionado = datos[0];
            this.getCatalogoBancos().getContenedorModal().ocultaComponente();
        }
    }
    
    //FUNCIONES PARA OCULTAR LOS CAMPOS DEPENDIENDO DEL MÉTODO DE PAGO
    this.setCamposParaMetodoPago = function(){
        
    }
    //FUNCIONES PARA OCULTAR LOS CAMPOS DEPENDIENDO DEL MÉTODO DE PAGO
    
    //FUNCIONES PARA SELECTOR DE FORMA DE PAGOS
    this.formaPagoSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoFormasPago().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorFormasPago().setTextoDeOpcionSeleccionada(datos[2]);
            this.idFormaPago = datos[1];
            this.getCatalogoFormasPago().getContenedorModal().ocultaComponente();
        }
    }
    
    //Catalogo Formas de Pago
    this.muestraCatalogoFormasDePago = function(){
        this.getCatalogoFormasPago().setComponente(null, this.nombre_instancia+".getCatalogoFormasPago()");
        this.getCatalogoFormasPago().setContenedorFormaFlotante(1);
        this.getCatalogoFormasPago().muestraContenedorModal('G');
        this.getCatalogoFormasPago().controlador_navegacion = null;
        
        this.getCatalogoFormasPago().setFuncionElementoSeleccionado(this.nombre_instancia+".formaPagoSeleccionadoDeCatalogo()");
        this.getCatalogoFormasPago().setComponente(this.getCatalogoFormasPago().getContenedorModal().getContenedorDeContenido(), this.getCatalogoFormasPago().nombre_instancia);
        this.getCatalogoFormasPago().muestraComponente();
    }
    //FUNCIONES PARA SELECTOR DE FORMA DE PAGOS
    
    //FUNCIONES PARA SELECTOR DE MONEDAS
    this.muestraCatalogoMonedas = function(){
        this.getCatalogoMonedas().setComponente(null, this.nombre_instancia+".getCatalogoMonedas()");
        this.getCatalogoMonedas().setContenedorFormaFlotante(1);
        this.getCatalogoMonedas().muestraContenedorModal('G');
        this.getCatalogoMonedas().controlador_navegacion = null;
        
        this.getCatalogoMonedas().setFuncionElementoSeleccionado(this.nombre_instancia+".monedaSeleccionadoDeCatalogo()");
        this.getCatalogoMonedas().setComponente(this.getCatalogoMonedas().getContenedorModal().getContenedorDeContenido(), this.getCatalogoMonedas().nombre_instancia);
        this.getCatalogoMonedas().muestraComponente();
    }
    
    this.monedaSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoMonedas().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorMoneda().setTextoDeOpcionSeleccionada(datos[2]+" ("+datos[1]+")");
            this.idMoneda = datos[1];
            this.getSelectorMoneda().getContenedorModal().ocultaComponente();
            
            this.setZonaTipoCambio();
        }
    }
    
    this.setZonaTipoCambio = function(){
        var zonaTipoCambio = document.getElementById(this.nombre_instancia+"_zona_tipo_cambio");
        if(zonaTipoCambio) {
            if(this.idMoneda != "MXN"){
                $(zonaTipoCambio).show();
            }else{
                $(zonaTipoCambio).hide();
            }   
        }
    }
    //FUNCIONES PARA SELECTOR DE MONEDAS
    
    //FACTURA SELECCIONADA EN CATALOGO DE FACTURAS EMITIDAS
    this.facturaSeleccionada = function(){
        var datos = this.getCatalogoFacturas().getDatosDeElementoSeleccionadoDeCatalogo();
        console.log("datos factura seleccionada");
        console.log(datos);
        if(datos){
            //idFacturaEmitida, nombreUsuario, cantidadFacturada, fecha, hora, textoEstatus, tipoFactura, idElemento, estatus
            this.getComponenteVisorDeFacturas().setOpcionTipoFactura(1);
            this.getComponenteVisorDeFacturas().setIdFactura(datos[0]);
            this.getComponenteVisorDeFacturas().setIdElemento(this.idGasto);
            this.getComponenteVisorDeFacturas().funcionFacturasActualizadas(this.nombre_instancia+".recargaFacturasEmitidas()");
            
            if(datos[8] == 1){
                this.getComponenteVisorDeFacturas().setModo(2);
                this.getComponenteVisorDeFacturas().muestraComponente();
            }else if(datos[8] == -2){
                this.getComponenteVisorDeFacturas().setModo(3);
                this.getComponenteVisorDeFacturas().muestraComponente();
            }
        }
    }
    //FACTURA SELECCIONADA EN CATALOGO DE FACTURAS EMITIDAS
    
    //FUNCIONES PARA SELECTOR Y CATALOGO DE USOS CFDIS
    this.muestraCatalogoUsosCFDI = function(){
        console.log("mostrando catalogo de usos cfdis");
        this.getCatalogoUsos().setComponente(null, this.nombre_instancia+".getCatalogoUsos()");
        this.getCatalogoUsos().setContenedorFormaFlotante(1);
        this.getCatalogoUsos().muestraContenedorModal('G');
        this.getCatalogoUsos().controlador_navegacion = null;
        
        this.getCatalogoUsos().setFuncionElementoSeleccionado(this.nombre_instancia+".usoCFDISeleccionado()");
        this.getCatalogoUsos().setComponente(this.getCatalogoUsos().getContenedorModal().getContenedorDeContenido(), this.getCatalogoUsos().nombre_instancia);
        this.getCatalogoUsos().muestraComponente();
    }
    
    this.usoCFDISeleccionado = function(){
        var datos = this.getCatalogoUsos().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorUsos().setTextoDeOpcionSeleccionada(datos[1]+" - "+datos[2]);
            this.getCatalogoUsos().getContenedorModal().ocultaComponente();
            this.idUso = datos[1];
        }
    }
    //FUNCIONES PARA SELECTOR Y CATALOGO DE USOS CFDIS
}