var FormaPagoRecibido = function(){
    this.idPago = 0;
    this.infoPago = null;
    
    this.setIdPago = function(idpago){
        this.idPago = idpago;
    }
    
    /*VARIABLES Y MÉTODOS PARA EL SELECTOR DE CLIENTE*/
    this.clienteSeleccionado = 0;
    this.setClienteSeleccionado = function(idcliente){
        this.clienteSeleccionado = idcliente;
    }
    
    this.catalogoClientes = null;
    this.getCatalogoClientes = function(){
        if(!this.catalogoClientes) {
            this.catalogoClientes = new CatalogoClientes();
            this.catalogoClientes.setSoloSeleccion(1);
        }
        return this.catalogoClientes;
    }
    
    this.setCatalogoClientes = function(catalogo){
        this.catalogoClientes = catalogo;
    }
    
    this.selectorClientes = null;
    this.getSelectorClientes = function(){
        if(!this.selectorClientes){
            this.selectorClientes = new SelectorDeOpcion();
            this.selectorClientes.setVisualizacion(1);
        }
        return this.selectorClientes;
    }
    
    this.setSelectorClientes = function(selector){
        this.selectorClientes = selector;
    }
    /*VARIABLES Y MÉTODOS PARA EL SELECTOR DE CLIENTE*/
    
    /*VARIABLES Y MÉTODOS PARA EL SELECTOR DE EMPRESA*/
    this.idEmpresaSeleccionada = 0;
    this.setEmpresaSeleccionada = function(idempresa){
        this.idEmpresaSeleccionada = idempresa;
    }
    
    this.catalogoEmpresas = null;
    this.getCatalogoEmpresas = function(){
        if(!this.catalogoEmpresas){
            this.catalogoEmpresas = new CatalogoEmpresas();
            this.catalogoEmpresas.setSoloSeleccion(1);
        }
        return this.catalogoEmpresas;
    }
    
    this.setCatalogoEmpresas = function(catalogo){
        this.catalogoEmpresas = catalogo;
    }
    
    this.selectorEmpresa = null;
    this.getSelectorEmpresa = function(){
        if(!this.selectorEmpresa){
            this.selectorEmpresa = new SelectorDeOpcion();
            this.selectorEmpresa.setVisualizacion(1);
        }
        return this.selectorEmpresa;
    }
    
    this.setSelectorEmpresa = function(selector){
        this.selectorEmpresa = selector;
    }
    /*VARIABLES Y MÉTODOS PARA EL SELECTOR DE EMPRESA*/
    
    //SELECTOR Y CATALOGO DE FORMAS DE PAGO
    this.idFormaPago = 1;
    this.bancarizado = false;
    this.selectorFormasPago = null;
    this.getSelectorFormasPago = function(){
        if(!this.selectorFormasPago){
            this.selectorFormasPago = new SelectorDeOpcion();
            this.selectorFormasPago.setVisualizacion(1);
        }
        return this.selectorFormasPago;
    }
    
    this.catalogoFormasPago = null;
    this.getCatalogoFormasPago = function(){
        if(!this.catalogoFormasPago){
            this.catalogoFormasPago = new CatalogoFormaPagoSat();
            this.catalogoFormasPago.setSoloSeleccion(1);
        }
        return this.catalogoFormasPago;
    }
    //SELECTOR Y CATALOGO DE FORMAS DE PAGO
    
    //SELECTOR Y CATALOGO DE MONEDAS
    this.idMoneda = "MXN";
    this.selectorMoneda = null;
    this.getSelectorMoneda = function(){
        if(!this.selectorMoneda){
            this.selectorMoneda = new SelectorDeOpcion();
            this.selectorMoneda.setVisualizacion(1);
        }
        return this.selectorMoneda;
    }
    
    this.catalogoMonedas = null;
    this.getCatalogoMonedas = function(){
        if(!this.catalogoMonedas){
            this.catalogoMonedas = new CatalogoMonedasSat();
            this.catalogoMonedas.setSoloSeleccion(1);
        }
        return this.catalogoMonedas;
    }
    //SELECTOR Y CATALOGO DE MONEDAS
    
    //SELECTOR Y CATALOGO DE BANCOS SAT
    this.idBancoOrigen = 0;
    this.selectorBancoOrigen = null;
    this.getSelectorBancoOrigen = function(){
        if(!this.selectorBancoOrigen){
            this.selectorBancoOrigen = new SelectorDeOpcion();
            this.selectorBancoOrigen.setVisualizacion(1);
        }
        return this.selectorBancoOrigen;
    }
    
    this.catalogoBancoOrigen = null;
    this.getCatalogoBancoOrigen = function(){
        if(!this.catalogoBancoOrigen){
            this.catalogoBancoOrigen = new CatalogoBancosSat();
            this.catalogoBancoOrigen.setSoloSeleccion(1);
        }
        return this.catalogoBancoOrigen;
    }
    //SELECTOR Y CATALOGO DE BANCOS SAT
    
    //SELECTOR Y CATALOGO DE BANCOS SAT RECEPTOR
    this.idBancoReceptor = 0;
    this.selectorBancoReceptor = null;
    this.getSelectorBancoReceptor = function(){
        if(!this.selectorBancoReceptor){
            this.selectorBancoReceptor = new SelectorDeOpcion();
            this.selectorBancoReceptor.setVisualizacion(1);
        }
        return this.selectorBancoReceptor;
    }
    //SELECTOR Y CATALOGO DE BANCOS SAT
    
    //selector de fecha del pago
    this.selector_fecha = null;
    
    //selector de hora
    this.selector_hora = null;
    
    //Visor de Facturación
    this.visor_facturas = null;
    this.getComponenteVisorDeFacturas = function(){
        if(!this.visor_facturas){
            this.visor_facturas = new ComponenteVisorFactura();
            this.visor_facturas.setComponente(null, this.nombre_instancia+".getComponenteVisorDeFacturas()");
            this.visor_facturas.funcionFacturasActualizadas(this.nombre_instancia+".recargaEscaneos()");
        }
        return this.visor_facturas;
    }
    
    this.muestraComponente = function(){
        var contenido = "<br/>";    
        
        if(this.idPago == 0) contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Nuevo Pago Recibido</div>";
        else contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Editando Pago Recibido</div>";
        
        contenido += "<div class='separador'></div>";
        contenido += "<br/>";    
        
        //selector de empresa y cliente
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Empresa</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_empresas'></div>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Cliente</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_clientes'></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        //selector de fecha y hora de recepción de pago
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Fecha recepción</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_selector_fecha'></div>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Hora recepción</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_selector_hora'></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        //selector de forma de pago y moneda
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Forma de Pago</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_forma_pago'></div>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Moneda</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_selector_moneda'></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        //Monto y tipo de cambio únicamente si la moneda es distinta de MXN
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Cantidad Pagada</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_cantidad_pagada' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe la cantidad total del pago recibido\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_tipo_cambio'>";
        contenido += "<div class='texto_forma'>Tipo de Cambio</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_tipo_cambio' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe la cantidad del tipo de cambio de la moneda seleccionada con respecto del peso mexicano\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        contenido += "</div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        //DIV DE AYUDA
        contenido += "<br/>";
        contenido += "<div class='ayuda_campos' id='"+this.nombre_instancia+"_a1'></div>";
        contenido += "<br/>";
        
        //ZONA PARA DATOS CUANDO LA FORMA DE PAGO ES BANCARIZADA
        contenido += "<div id='"+this.nombre_instancia+"_zona_datos_bancarizados'>";
        
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Banco Origen</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_selector_banco_origen'></div>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Número de Cuenta Origen</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_numero_cuenta_origen' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Número de cuenta de origen del pago.\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Banco Receptor</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_selector_banco_receptor'></div>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Número de Cuenta Receptor</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_numero_cuenta_receptor' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Número de cuenta donde se recibe el pago.\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Número de Operación</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_numero_operacion' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Permite capturar el número de operación, número de cheque, número de SPEI o cualquier otro número que permita identificar única y eficazmente la transacción.\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 center-align">';
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += '</div>';
        
        //boton guardar y facturación del pago
        contenido += '<div class="row center-align">';
        contenido += '<div class="col l6 center-align">';
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_boton_guardar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a></div>";
        
        contenido += '</div>';
        contenido += '<div class="col l6 center-align">';
        
        //solo se pueden agregar los conceptos que tienen idGasto en 0, en otro caso no se pueden agregar por que ya están agregados, solo se pueden modificar
        if(this.idPago > 0){
            contenido += "<div id='"+this.nombre_instancia+"_zona_boton_facturar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".facturarPago()' style='margin:0 auto 0 auto;'>Facturar Pago</a></div>";   
        }
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += "<br/>";
        
        this.contenedor.innerHTML = contenido;
        
        //selector empresa
        this.getSelectorEmpresa().setComponente(document.getElementById(this.nombre_instancia+"_zona_empresas"), this.nombre_instancia+".getSelectorEmpresa()");
        
        console.log("idempresa: "+this.idEmpresaSeleccionada);
        
        if(this.idEmpresaSeleccionada == 0) this.getSelectorEmpresa().setTextoDeOpcionSeleccionada("Empresa");
        this.getSelectorEmpresa().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoEmpresas()");
        
        this.getSelectorEmpresa().muestraComponente();
        
        //selector de clientes
        this.getSelectorClientes().setComponente(document.getElementById(this.nombre_instancia+"_zona_clientes"), this.nombre_instancia+".getSelectorClientes()");
        
        if(this.clienteSeleccionado == 0) this.getSelectorClientes().setTextoDeOpcionSeleccionada("Cliente");
        this.getSelectorClientes().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoDeClientes()");
        
        this.getSelectorClientes().muestraComponente();
        
        //selector de fecha de recepcion del pago
        //selector de fecha, por defecto pone los datos actuales
        this.selector_fecha = new SelectorFechas();
        this.selector_fecha.fecha_actual = new Date();
        this.selector_fecha.setSelectorFecha(document.getElementById(this.nombre_instancia+"_zona_selector_fecha"), this.nombre_instancia+".selector_fecha");
        this.selector_fecha.fecha_modificada = true;
        this.selector_fecha.muestraComboFechas();
        
        //selector de hora
        this.selector_hora = new SelectorHora();
        this.selector_hora.setSelectorHora(document.getElementById(this.nombre_instancia+"_zona_selector_hora"), -1, null, this.nombre_instancia+".selector_hora");
        
        //selector formas de pago sat
        this.getSelectorFormasPago().setComponente(document.getElementById(this.nombre_instancia+"_zona_forma_pago"), this.nombre_instancia+".getSelectorFormasPago()");
        
        if(this.idFormaPago == 0) this.getSelectorFormasPago().setTextoDeOpcionSeleccionada("Selecciona Forma de Pago");
        else if(this.idFormaPago == 1) this.getSelectorFormasPago().setTextoDeOpcionSeleccionada("EFECTIVO");
        
        this.getSelectorFormasPago().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoFormasDePago()");
        
        this.getSelectorFormasPago().muestraComponente();
        
        //selector monedas sat
        this.getSelectorMoneda().setComponente(document.getElementById(this.nombre_instancia+"_zona_selector_moneda"), this.nombre_instancia+".getSelectorMoneda()");
        
        if(this.idMoneda == 0) this.getSelectorMoneda().setTextoDeOpcionSeleccionada("Selecciona Moneda");
        else if(this.idMoneda == "MXN") this.getSelectorMoneda().setTextoDeOpcionSeleccionada("PESO MEXICANO (MXN)");
        
        this.getSelectorMoneda().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoMonedas()");
        
        this.getSelectorMoneda().muestraComponente();
        
        //selector bancos sat origen
        this.getSelectorBancoOrigen().setComponente(document.getElementById(this.nombre_instancia+"_zona_selector_banco_origen"), this.nombre_instancia+".getSelectorBancoOrigen()");
        
        if(this.idBancoOrigen == 0) this.getSelectorBancoOrigen().setTextoDeOpcionSeleccionada("Selecciona Banco Origen");
        
        this.getSelectorBancoOrigen().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoBancoOrigen()");
        
        this.getSelectorBancoOrigen().muestraComponente();
        
        //selector bancos sat receptor
        this.getSelectorBancoReceptor().setComponente(document.getElementById(this.nombre_instancia+"_zona_selector_banco_receptor"), this.nombre_instancia+".getSelectorBancoReceptor()");
        
        if(this.idBancoReceptor == 0) this.getSelectorBancoReceptor().setTextoDeOpcionSeleccionada("Selecciona Banco Receptor");
        
        this.getSelectorBancoReceptor().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoBancoReceptor()");
        
        this.getSelectorBancoReceptor().muestraComponente();
        
        //seteamos zona de tipo de cambio
        this.setZonaTipoCambio();
        
        //seteamos la zona de elementos bancarizados si aplica
        this.setZonaElementosBancarizados();
    }
    
    this.facturarPago = function(){
        this.getComponenteVisorDeFacturas().setOpcionTipoFactura(2);
        this.getComponenteVisorDeFacturas().setIdElemento(this.idPago);
        this.getComponenteVisorDeFacturas().muestraComponente();
    }
    
    //FUNCION INVOCADA CUANDO SE EMITE UNA FACTURA
    this.recargaEscaneos = function(){
        
    }
    
    //FUNCIONES PARA LA SELECCION DE EMPRESAS
    this.muestraCatalogoEmpresas = function(){
        this.getCatalogoEmpresas().setComponente(null, this.nombre_instancia+".getCatalogoEmpresas()");
        this.getCatalogoEmpresas().setContenedorFormaFlotante(1);
        this.getCatalogoEmpresas().muestraContenedorModal('G');
        this.getCatalogoEmpresas().controlador_navegacion = null;
        
        this.getCatalogoEmpresas().setFuncionElementoSeleccionado(this.nombre_instancia+".empresaSeleccionadaDeCatalogo()");
        this.getCatalogoEmpresas().setComponente(this.getCatalogoEmpresas().getContenedorModal().getContenedorDeContenido(), this.getCatalogoEmpresas().nombre_instancia);
        this.getCatalogoEmpresas().muestraComponente();
    }
    
    this.empresaSeleccionadaDeCatalogo = function(){
        var datos = this.getCatalogoEmpresas().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorEmpresa().setTextoDeOpcionSeleccionada(datos[1]);
            this.idEmpresaSeleccionada = datos[0];
            this.getCatalogoEmpresas().getContenedorModal().ocultaComponente();
        }
    }
    //FUNCIONES PARA LA SELECCION DE EMPRESAS
    
    //FUNCIONES PARA LA SELECCION DE CLIENTE
    this.muestraCatalogoDeClientes = function(){
        this.getCatalogoClientes().setComponente(null, this.nombre_instancia+".getCatalogoClientes()");
        this.getCatalogoClientes().setContenedorFormaFlotante(1);
        this.getCatalogoClientes().muestraContenedorModal('G');
        this.getCatalogoClientes().controlador_navegacion = null;
        
        this.getCatalogoClientes().setFuncionElementoSeleccionado(this.nombre_instancia+".clienteSeleccionadoDeCatalogo()");
        this.getCatalogoClientes().setComponente(this.getCatalogoClientes().getContenedorModal().getContenedorDeContenido(), this.getCatalogoClientes().nombre_instancia);
        this.getCatalogoClientes().muestraComponente();
    }
    
    this.clienteSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoClientes().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorClientes().setTextoDeOpcionSeleccionada(datos[1]);
            this.clienteSeleccionado = datos[0];
            this.getCatalogoClientes().getContenedorModal().ocultaComponente();
        }
    }
    //FUNCIONES PARA LA SELECCION DE CLIENTE
    
    //FUNCIONES PARA SELECTOR DE FORMA DE PAGOS
    this.formaPagoSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoFormasPago().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorFormasPago().setTextoDeOpcionSeleccionada(datos[2]);
            this.idFormaPago = datos[1];
            this.getCatalogoFormasPago().getContenedorModal().ocultaComponente();
            
            if(datos[3] == "Sí") this.bancarizado = true;
            else this.bancarizado = false;
            
            this.setZonaElementosBancarizados();
        }
    }
    
    //Catalogo Formas de Pago
    this.muestraCatalogoFormasDePago = function(){
        this.getCatalogoFormasPago().setComponente(null, this.nombre_instancia+".getCatalogoFormasPago()");
        this.getCatalogoFormasPago().setContenedorFormaFlotante(1);
        this.getCatalogoFormasPago().muestraContenedorModal('G');
        this.getCatalogoFormasPago().controlador_navegacion = null;
        
        this.getCatalogoFormasPago().setFuncionElementoSeleccionado(this.nombre_instancia+".formaPagoSeleccionadoDeCatalogo()");
        this.getCatalogoFormasPago().setComponente(this.getCatalogoFormasPago().getContenedorModal().getContenedorDeContenido(), this.getCatalogoFormasPago().nombre_instancia);
        this.getCatalogoFormasPago().muestraComponente();
    }
    //FUNCIONES PARA SELECTOR DE FORMA DE PAGOS
    
    //FUNCIONES PARA SELECTOR DE MONEDAS
    this.muestraCatalogoMonedas = function(){
        this.getCatalogoMonedas().setComponente(null, this.nombre_instancia+".getCatalogoMonedas()");
        this.getCatalogoMonedas().setContenedorFormaFlotante(1);
        this.getCatalogoMonedas().muestraContenedorModal('G');
        this.getCatalogoMonedas().controlador_navegacion = null;
        
        this.getCatalogoMonedas().setFuncionElementoSeleccionado(this.nombre_instancia+".monedaSeleccionadoDeCatalogo()");
        this.getCatalogoMonedas().setComponente(this.getCatalogoMonedas().getContenedorModal().getContenedorDeContenido(), this.getCatalogoMonedas().nombre_instancia);
        this.getCatalogoMonedas().muestraComponente();
    }
    
    this.monedaSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoMonedas().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorMoneda().setTextoDeOpcionSeleccionada(datos[2]+" ("+datos[1]+")");
            this.idMoneda = datos[1];
            this.getSelectorMoneda().getContenedorModal().ocultaComponente();
            
            this.setZonaTipoCambio();
        }
    }
    
    this.setZonaTipoCambio = function(){
        var zonaTipoCambio = document.getElementById(this.nombre_instancia+"_zona_tipo_cambio");
        if(zonaTipoCambio) {
            if(this.idMoneda != "MXN"){
                $(zonaTipoCambio).show();
            }else{
                $(zonaTipoCambio).hide();
            }   
        }
    }
    //FUNCIONES PARA SELECTOR DE MONEDAS
    
    //funciones para administracion de datos bancarizados
    this.setZonaElementosBancarizados = function(){
        var div = document.getElementById(this.nombre_instancia+"_zona_datos_bancarizados");
        if(div){
            if(this.bancarizado) $(div).show();
            else $(div).hide();
        }
    }
    //funciones para administracion de datos bancarizados
    
    //FUNCIONES PARA BANCO ORIGEN
    this.muestraCatalogoBancoOrigen = function(){
        this.getCatalogoBancoOrigen().setComponente(null, this.nombre_instancia+".getCatalogoBancoOrigen()");
        this.getCatalogoBancoOrigen().setContenedorFormaFlotante(1);
        this.getCatalogoBancoOrigen().muestraContenedorModal('G');
        this.getCatalogoBancoOrigen().controlador_navegacion = null;
        
        this.getCatalogoBancoOrigen().setFuncionElementoSeleccionado(this.nombre_instancia+".bancoOrigenSeleccionadoDeCatalogo()");
        this.getCatalogoBancoOrigen().setComponente(this.getCatalogoBancoOrigen().getContenedorModal().getContenedorDeContenido(), this.getCatalogoBancoOrigen().nombre_instancia);
        this.getCatalogoBancoOrigen().muestraComponente();
    }
    
    this.bancoOrigenSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoBancoOrigen().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorBancoOrigen().setTextoDeOpcionSeleccionada(datos[3]);
            this.idBancoOrigen = datos[1];
            this.getSelectorBancoOrigen().getContenedorModal().ocultaComponente();
        }
    }
    //FUNCIONES PARA BANCO ORIGEN
    
    //FUNCIONES PARA BANCO RECEPTOR
    this.muestraCatalogoBancoReceptor = function(){
        this.getCatalogoBancoOrigen().setComponente(null, this.nombre_instancia+".getCatalogoBancoOrigen()");
        this.getCatalogoBancoOrigen().setContenedorFormaFlotante(1);
        this.getCatalogoBancoOrigen().muestraContenedorModal('G');
        this.getCatalogoBancoOrigen().controlador_navegacion = null;
        
        this.getCatalogoBancoOrigen().setFuncionElementoSeleccionado(this.nombre_instancia+".bancoReceptorSeleccionadoDeCatalogo()");
        this.getCatalogoBancoOrigen().setComponente(this.getCatalogoBancoOrigen().getContenedorModal().getContenedorDeContenido(), this.getCatalogoBancoOrigen().nombre_instancia);
        this.getCatalogoBancoOrigen().muestraComponente();
    }
    
    this.bancoReceptorSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoBancoOrigen().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorBancoReceptor().setTextoDeOpcionSeleccionada(datos[3]);
            this.idBancoReceptor = datos[1];
            this.getSelectorBancoOrigen().getContenedorModal().ocultaComponente();
        }
    }
    //FUNCIONES PARA BANCO RECEPTOR
    
    //VALIDAR Y GUARDAR FORMA DE PAGO
    this.validaForma = function(){
        if(this.idEmpresaSeleccionada == 0){
            muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar una empresa receptora del pago");
            return;
        }
        
        if(this.clienteSeleccionado == 0){
            muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar un cliente emisor del pago");
            return;
        }
        
        var fecha = this.selector_fecha.getFechaActualParaDB();
        var hora = this.selector_hora.getHora();
        
        var cantidadPagada = document.getElementById(this.nombre_instancia+"_campo_cantidad_pagada");
        if(cantidadPagada.value == "" || cantidadPagada.value == " "){
            cantidadPagada.focus();
            return;
        }
        
        var tipoCambio = "1";
        if(this.idMoneda != "MXN"){
            var campoTipoCambio = document.getElementById(this.nombre_instancia+"_tipo_cambio");
            if(campoTipoCambio.value == "" || campoTipoCambio.value == " "){
                campoTipoCambio.focus();
                return;
            }
            
            tipoCambio = campoTipoCambio.value;
        }
        
        var ctaOrigen = "";
        var ctaDestino = "";
        var numeroOperacion = "";
        if(this.bancarizado){
            if(this.idBancoOrigen == 0){
                muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar el banco de donde viene el pago");
                return;
            }
            
            var campoCtaOrigen = document.getElementById(this.nombre_instancia+"_campo_numero_cuenta_origen");
            if(campoCtaOrigen.value == "" || campoCtaOrigen.value == " "){
                campoCtaOrigen.focus();
                return;
            }
            
            if(this.idBancoReceptor == 0){
                muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar el banco en donde se recibe el pago");
                return;
            }
            
            var campoCtaReceptor = document.getElementById(this.nombre_instancia+"_campo_numero_cuenta_receptor");
            if(campoCtaReceptor.value == "" || campoCtaReceptor.value == " "){
                campoCtaReceptor.focus();
                return;
            }
            
            var campoNumeroOperacion = document.getElementById(this.nombre_instancia+"_campo_numero_operacion");
            if(campoNumeroOperacion.value == "" || campoNumeroOperacion.value == " "){
                campoNumeroOperacion.focus();
                return;
            }
            
            ctaOrigen = campoCtaOrigen.value;
            ctaDestino = campoCtaReceptor.value;
            numeroOperacion = campoNumeroOperacion.value;
        }
        
        var info = this.idPago+"|"+this.idEmpresaSeleccionada+"|"+this.clienteSeleccionado+"|"+fecha+"|"+hora+"|"+this.idFormaPago+"|"+this.idMoneda+"|"+cantidadPagada.value+"|"+tipoCambio+"|"+this.idBancoOrigen+"|"+ctaOrigen+"|"+this.idBancoReceptor+"|"+ctaDestino+"|"+numeroOperacion;
        
        console.log("info: "+info);
        
        getResultadoAjax("opcion=53&info="+info, this.nombre_instancia+".procesaGuardado", document.getElementById(this.nombre_instancia+"_zona_boton_guardar"));
    }
    
    this.procesaInfoPago = function(respuesta){
        this.infoPago = respuesta;
        this.editaForma();
    }
    
    this.editaForma = function(){
        if(!this.infoPago){
            getResultadoAjax("opcion=54&idPago="+this.idPago, this.nombre_instancia+".procesaInfoPago", this.contenedor);
            return;
        }
        
        this.muestraComponente();
        
        //idPagoRecibido, idEmpresa, idCliente, fecha, hora, formaPago, moneda, cantidadPagada, tipoCambio, bancoOrigen, cuentaOrigen, bancoReceptor, cuentaReceptor, numeroOperacion, estatus, nombreEmpresa, nombreCliente, nombreFormaPago, nombreMoneda, nombreBancoOrigen, nombreBancoReceptor, esBancarizado
        
        var datos = this.infoPago.split("|");
        this.idEmpresaSeleccionada = datos[1];
        this.clienteSeleccionado = datos[2];
        
        this.selector_fecha.setFechaDesdeDB(datos[3]);
        this.selector_hora.setHora(datos[4]);
        
        this.idFormaPago = datos[5];
        this.idMoneda = datos[6];
        
        document.getElementById(this.nombre_instancia+"_campo_cantidad_pagada").value = datos[7];
        document.getElementById(this.nombre_instancia+"_tipo_cambio").value = datos[8];
        
        this.idBancoOrigen = datos[9];
        document.getElementById(this.nombre_instancia+"_campo_numero_cuenta_origen").value = datos[10];
        
        this.idBancoReceptor = datos[11];
        document.getElementById(this.nombre_instancia+"_campo_numero_cuenta_receptor").value = datos[12];
        
        document.getElementById(this.nombre_instancia+"_campo_numero_operacion").value = datos[13];
        
        this.getSelectorEmpresa().setTextoDeOpcionSeleccionada(datos[15]);
        this.getSelectorEmpresa().muestraComponente();
        
        this.getSelectorClientes().setTextoDeOpcionSeleccionada(datos[16]);
        this.getSelectorClientes().muestraComponente();
        
        this.getSelectorFormasPago().setTextoDeOpcionSeleccionada(datos[17]);
        this.getSelectorFormasPago().muestraComponente();
        
        this.getSelectorMoneda().setTextoDeOpcionSeleccionada(datos[18]);
        this.getSelectorMoneda().muestraComponente();
        
        this.getSelectorBancoOrigen().setTextoDeOpcionSeleccionada(datos[19]);
        this.getSelectorBancoOrigen().muestraComponente();
        
        this.getSelectorBancoReceptor().setTextoDeOpcionSeleccionada(datos[20]);
        this.getSelectorBancoReceptor().muestraComponente();
        
        console.log("bancarizado: "+datos[21]);
        if(datos[21]) this.bancarizado = true;
        else this.bancarizado = false;
        
        //seteamos zona de tipo de cambio
        this.setZonaTipoCambio();
        
        //seteamos la zona de elementos bancarizados si aplica
        this.setZonaElementosBancarizados();
    }
    
    this.procesaGuardado = function(respuesta){
        console.log("respuesta guardado: "+respuesta);
        if(respuesta > 0 && !isNaN(respuesta)){
            alert("Pago recibido guardado!");
            if(this.idPago == 0) this.idPago = respuesta;
            else this.infoPago = null;
            this.editaForma();
        }else{
            alert("No se pudo guardar el pago recibido, por favor intenta nuevamente!");
            document.getElementById(this.nombre_instancia+"_zona_boton_guardar").innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a>";
        }
    }
    //VALIDAR Y GUARDAR FORMA DE PAGO
    
}