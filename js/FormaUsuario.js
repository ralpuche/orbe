var FormaUsuario = function(){
    
    this.idUsuario = 0;
    this.setIdUsuario = function(idusuario){
        this.idUsuario = idusuario;
    }
    
    this.selectorClientes = null;
    this.getSelectorClientes = function(){
        if(!this.selectorClientes){
            this.selectorClientes = new SelectorDeOpcion();
            this.selectorClientes.setVisualizacion(1);
        }
        return this.selectorClientes;
    }
    
    this.clienteSeleccionado = 0;
    this.setClienteSeleccionado = function(idcliente){
        this.clienteSeleccionado = idcliente;
    }
    
    this.catalogoClientes = null;
    this.getCatalogoClientes = function(){
        if(!this.catalogoClientes) {
            this.catalogoClientes = new CatalogoClientes();
            this.catalogoClientes.setSoloSeleccion(1);
        }
        return this.catalogoClientes;
    }
    
    this.info = null;
    
    this.muestraComponente = function(){
        var contenido = "<br/>";    
        
        if(this.idUsuario == 0) contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Nuevo Usuario del Sistema</div>";
        else contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Editando Usuario del Sistema</div>";
        
        contenido += "<div class='separador'></div>";
        contenido += "<br/>";    
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_nombre_usuario'>";
        
        contenido += "<div class='texto_forma'>Nombre completo del usuario</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_nombre' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el nombre completo del usuario\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += "</div>";
        
        contenido += '<div class="row center-align">';
        contenido += "<br/>";
        contenido += '<div class="col l6 center-align">';
        
        //selector de perfil de usuario
        contenido += "<div class='texto_forma'>Perfil del Usuario</div><br/>";
        contenido += '<select id="'+this.nombre_instancia+'_selector_perfil_usuario" onchange="'+this.nombre_instancia+'.tipoPerfilSeleccionado();" class="browser-default">'
        contenido += '<option value="0">Selecciona un perfil</option>';
        contenido += '<option value="1">Administrador</option>';
        contenido += '<option value="2">Facturista</option>';
        contenido += '<option value="3">Promotor</option>';
        contenido += '<option value="4">Operaciones</option>';
        contenido += '<option value="5">Cliente</option>';
        contenido += '</select>';
        
        contenido += '</div>';
        contenido += '<div class="col l6 center-align">';
        
        contenido += "<div class='texto_forma'>Nombre de usuario</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_usuario' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el nombre de usuario para acceso al sistema\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += "<div class='ayuda_campos' id='"+this.nombre_instancia+"_a1'></div>";
        contenido += "<br/>";
        
        contenido += '<div class="row center-align">';
        contenido += '<div class="col l6 center-align">';
        
        contenido += "<div class='texto_forma'>Password de usuario</div>";
        contenido += "<input type='password' id='"+this.nombre_instancia+"_campo_password' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el password de usuario para acceso al sistema. Mínimo 8 caracteres.\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '<div class="col l6 center-align">';
        
        contenido += "<div class='texto_forma'>Confirma el password de usuario</div>";
        contenido += "<input type='password' id='"+this.nombre_instancia+"_campo_confirmacion_password' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Confirma el password de usuario para acceso al sistema. Debe coincidir con el primer password.\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        //contenido += "<br/>";
        contenido += "<div class='texto_forma'>Correo</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_correo' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe una dirección de correo electrónico. Debe ser una dirección válida.\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        contenido += "<br/>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_boton_guardar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a></div>";
        
        contenido += "<br/>";
        
        this.contenedor.innerHTML = contenido;
    }
    
    this.tipoPerfilSeleccionado = function(){
        var selector = document.getElementById(this.nombre_instancia+"_selector_perfil_usuario");
        var contenedor = document.getElementById(this.nombre_instancia+"_zona_nombre_usuario");
        if(selector.options[selector.selectedIndex].value == 5){
            //como es un cliente, entonces aparece el selector de cliente para ligarlo con el usuario que se está creando
            this.getSelectorClientes().setComponente(contenedor, this.nombre_instancia+".getSelectorClientes()");
            this.getSelectorClientes().setTextoDeOpcionSeleccionada("Selecciona el cliente ligado a la cuenta de usuario");
            this.getSelectorClientes().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoDeClientes()");

            this.getSelectorClientes().muestraComponente();
        }else{
            var campoNombreUsuario = document.getElementById(this.nombre_instancia+"_campo_nombre");
            if(!campoNombreUsuario){
                var contenido = "";
                contenido += "<div class='texto_forma'>Nombre completo del usuario</div>";
                contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_nombre' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el nombre completo del usuario\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
                contenido += "<br/>";
                
                contenedor.innerHTML = contenido;
                
                this.clienteSeleccionado = 0;
            }
        }
    }
    
    this.editaComponente = function(){
        if(!this.info){
            getResultadoAjax("opcion=8&idusuario="+this.idUsuario, this.nombre_instancia+".procesaInfoUsuario", this.contenedor);
            return;
        }
        
        this.muestraComponente();
        //idUsuario, nombreCompleto, idPerfil, nombreUsuario, passwordUsuario, correoElectronico, estatus, nombreCliente
        
        var selectorPerfil = document.getElementById(this.nombre_instancia+"_selector_perfil_usuario");
        selectorPerfil.selectedIndex = this.info[2];
        this.tipoPerfilSeleccionado();
        
        if(this.info[2] == 5){
            this.clienteSeleccionado = this.info[1];
            this.getSelectorClientes().setTextoDeOpcionSeleccionada(this.info[7]);
        }else{
            document.getElementById(this.nombre_instancia+"_campo_nombre").value = this.info[1];
        }
        
        document.getElementById(this.nombre_instancia+"_campo_usuario").value = this.info[3];
        
        document.getElementById(this.nombre_instancia+"_campo_password").value = this.info[4];
        
        document.getElementById(this.nombre_instancia+"_campo_confirmacion_password").value = this.info[4];
        
        document.getElementById(this.nombre_instancia+"_campo_correo").value = this.info[5];
    }
    
    this.procesaInfoUsuario = function(respuesta){
        console.log("info usuario: "+respuesta);
        
        this.info = respuesta.split("|");
        this.editaComponente();
    }
    
    this.getZonaBotonGuardar = function(){
        return document.getElementById(this.nombre_instancia+"_zona_boton_guardar");
    }
    
    this.validaForma = function(){
        //debe tener seleccionado un perfil de usuario
        var selectorPerfil = document.getElementById(this.nombre_instancia+"_selector_perfil_usuario");
        if(selectorPerfil.selectedIndex == 0){
            muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar un perfil para el usuario.");
            return;
        }
        
        var campoNombre = "";
        if(selectorPerfil.options[selectorPerfil.selectedIndex].value == 5){
            campoNombre = this.clienteSeleccionado;
        }else{
            campoNombre = document.getElementById(this.nombre_instancia+"_campo_nombre").value;
            if(campoNombre.length == 0 || campoNombre == "" || campoNombre == " "){
                campoNombre.focus();
                return;
            }
        }
        
        var campoUsuario = document.getElementById(this.nombre_instancia+"_campo_usuario");
        if(campoUsuario.value.length == 0 || campoUsuario.value == "" || campoUsuario.value == " "){
            campoUsuario.focus();
            return;
        }
        
        var campoPassword = document.getElementById(this.nombre_instancia+"_campo_password");
        if(campoPassword.value.length == 0 || campoPassword.value == "" || campoPassword.value == " " || campoPassword.value.length < 8){
            campoPassword.focus();
            return;
        }
        
        var campoConfirmaPassword = document.getElementById(this.nombre_instancia+"_campo_confirmacion_password");
        if(campoConfirmaPassword.value.length == 0 || campoConfirmaPassword.value == "" || campoConfirmaPassword.value == " " || campoConfirmaPassword.value != campoPassword.value){
            campoConfirmaPassword.focus();
            return;
        }
        
        var campoCorreo = document.getElementById(this.nombre_instancia+"_campo_correo");
        if(campoCorreo.value.length > 0 && !esUnEmailValido(campoCorreo.value)){
            campoCorreo.focus();
            return;
        }
        
        var info = this.idUsuario+"|"+campoNombre+"|"+selectorPerfil.selectedIndex+"|"+campoUsuario.value+"|"+campoPassword.value+"|"+campoCorreo.value;
        //idusuario, nombrecompleto | idUsuarioLigado, idperfil, campousuario, campopassword, campocorreo
        
        console.log("info: "+info);
        
        getResultadoAjax("opcion=7&info="+info, this.nombre_instancia+".respuestaUsuarioGuardado", this.getZonaBotonGuardar());
    }
    
    this.respuestaUsuarioGuardado = function(respuesta){
        console.log("respuesta guardar: "+respuesta);
        this.getZonaBotonGuardar().innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a>";
        
        if(respuesta == -1){
            alert("El nombre de usuario ya está en uso, por favor elige otro");
            var campoUsuario = document.getElementById(this.nombre_instancia+"_campo_usuario");
            campoUsuario.focus();
            return;
        }
        
        if(respuesta > 0 && !isNaN(respuesta)){
            alert("Usuario guardado!");
            if(this.idUsuario == 0) this.idUsuario = respuesta;
        }else{
            alert("No se pudo guardar el usuario, por favor intenta más tarde!");
        }
    }
    
    /*
    METODOS PARA INVOCAR EL CATALOGO DE CLIENTES REGISTRADOS
    */
    this.muestraCatalogoDeClientes = function(){
        this.getCatalogoClientes().setComponente(null, this.nombre_instancia+".getCatalogoClientes()");
        this.getCatalogoClientes().setContenedorFormaFlotante(1);
        this.getCatalogoClientes().muestraContenedorModal('G');
        this.getCatalogoClientes().controlador_navegacion = null;
        
        this.catalogoClientes.setFuncionElementoSeleccionado(this.nombre_instancia+".clienteSeleccionadoDeCatalogo()");
        this.getCatalogoClientes().setComponente(this.getCatalogoClientes().getContenedorModal().getContenedorDeContenido(), this.getCatalogoClientes().nombre_instancia);
        this.getCatalogoClientes().muestraComponente();
    }
    
    this.clienteSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoClientes().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorClientes().setTextoDeOpcionSeleccionada(datos[1]);
            this.clienteSeleccionado = datos[0];
            this.getCatalogoClientes().getContenedorModal().ocultaComponente();
        }
    }
}