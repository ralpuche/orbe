var ListaSeleccionable = function(){
    
    this.idelemento_seleccionado = 0;
    this.posicion_elemento_seleccionado = -1;
    this.elementos_recuperados = null;
    this.encabezados = null;
    this.medidas = null;
    this.c = null;
    
    this.idelemento_eliminado = 0;
    
    this.tipo_elementos = 1;
    this.setTipoElementos = function(tipo_elementos){
        this.tipo_elementos = tipo_elementos;
    }
    
    //titulo de la lista cuando no haya elementos que mostrar
    this.titulo_lista = "No hay elementos para mostrar";
    
    //posición del elemento seleccionado anteriormente
    this.posicion_elemento_seleccionado_anterior = -1;
    
    //vector que almacena las funciones que se ejecutan cuando se hace click sobre una celda editable
    this.funciones_celdas_editables = null;
    
    //opciones
    this.opcion_borrar = 1;
    this.opcion_detalles = 1;
    this.opcion_impresora = 1;
    
    //elementos que se están visualizando actualmente, y que considera los filtros de búsquedas
    this.elementosVisiblesEnLista = null;
    this.getElementosVisiblesEnLista = function(){
        return this.elementosVisiblesEnLista;
    }
    
    //opciones enviadas para recuperar la lista de elementos
    this.opciones_recuperar_elementos = "";
    
    this.funcion_elimina_elemento = "";
    
    this.funcion_elemento_seleccionado = "";
    
    this.funcion_detalle_elemento_seleccionado = "";
    
    this.funcion_imprime_elemento_seleccionado = "";
    
    this.numero_celda_seleccionada = -1;
    this.celda_seleccionada = null;
    this.campo_seleccionado = null;
    
    this.columnasOrdenadas = null;
    
    this.getColumnasOrdenadas = function(){
        if(!this.columnasOrdenadas){
            this.columnasOrdenadas = new Array();
            
            //init all positions with 0. 0 means no sorted using this index column. 1 already sorted using this index column ascending. 2 already sorted using this index column descending
            for(var i=0; i<this.encabezados.length; i++) this.columnasOrdenadas[i] = 0;
        }
        return this.columnasOrdenadas;
    }
    
    this.elementosRecuperadosEnVector = null;
    
    this.getElementosRecuperadosEnVector = function(){
        this.elementosRecuperadosEnVector = new Array();
            
        for(var i=0; i<this.elementos_recuperados.length; i++) this.elementosRecuperadosEnVector.push(this.elementos_recuperados[i].split("|"));
        
        return this.elementosRecuperadosEnVector;
    }
    
    this.es_contenedor_flotante = false;
    this.setEsContenedorFlotante = function(valor){
        this.es_contenedor_flotante = valor;
    }
    
    //este vector almacena los indices de las columnas que van a ocultarse
    this.vector_columnas_ocultas = null;
    
    this.setIndiceDeColumnaOculta = function(indice){
        if(!this.vector_columnas_ocultas) this.vector_columnas_ocultas = new Array();
        this.vector_columnas_ocultas[this.vector_columnas_ocultas.length] = indice;
    }
    
    //elemento que habrá de resaltarse cuando se muestra la tabla de elementos
    this.idelemento_resaltar = null;
    this.setIdElementoParaResaltar = function(idelemento){
        this.idelemento_resaltar = idelemento;
    }
    
    //esta función oculta las columnas con base en el vector de columnas
    this.ocultaColumnas = function(filas){
        if(!this.vector_columnas_ocultas) return filas.slice(0);
        var fila;
        var nueva_fila;
        for(var i=0; i<filas.length; i++){
            fila = filas[i].split("|");
            nueva_fila = "";
            for(var k=0; k<fila.length; k++){
                if(!existeElementoEnVector(k, this.vector_columnas_ocultas)){
                    if((k+1) == fila.length) nueva_fila += fila[k];
                    else nueva_fila += fila[k]+"|";
                }
            }
            filas[i] = nueva_fila;
        }
        return filas.slice(0);
    }
    
    //este vector indica el indice de las columnas que no deberán responder al evento onClick
    this.vector_columnas_sinevento_onclick = null;
    
    this.setIndiceDeColumnaSinEventoOnclick = function(indice){
        if(!this.vector_columnas_sinevento_onclick) this.vector_columnas_sinevento_onclick = new Array();
        this.vector_columnas_sinevento_onclick[this.vector_columnas_sinevento_onclick.length] = indice;
    }
    
    this.esColumnaConEventoOnclick = function(indice_columna){
        if(!this.vector_columnas_sinevento_onclick) return true;
        for(var i=0; i<this.vector_columnas_sinevento_onclick.length; i++)
            if(this.vector_columnas_sinevento_onclick[i] == indice_columna) return false;
        return true;
    }
    
    this.implementa_busqueda = true;
    
    //esta función se invoca justo después de mostrar la tabla de elementos
    this.funcion_tabla_mostrada = null;
    
    this.setElementosRecuperados = function(elementos){
        this.elementos_recuperados = elementos;
    }
    
    this.getElementosRecuperados = function(){
        return this.elementos_recuperados;
    }
    
    this.getElementosEnListaParaDb = function(){
        if(!this.elementos_recuperados) return "";
        var cadena = "";
        for(var i=0; i<this.elementos_recuperados.length; i++){
            cadena += this.elementos_recuperados[i]+"^";
        }
        return cadena;
    }
    
    this.setFuncionTablaMostrada = function(funcion){
        this.funcion_tabla_mostrada = funcion;
    }
    
    this.getCeldaSeleccionada = function(){
        return this.celda_seleccionada;
    }
    
    this.setNumeroDeCeldaEditableConFuncion = function(numero, funcion){
        if(!this.funciones_celdas_editables) this.funciones_celdas_editables = new Array();
        this.funciones_celdas_editables[numero] = funcion;
    }
    
    this.setIdElementoSeleccionado = function(idelemento){
        this.idelemento_seleccionado = idelemento;
    }
    
    this.setOpcionDetalle = function(opcion){
        this.opcion_detalles = opcion;
    }
    
    this.setOpcionBorrar = function(opcion){
        this.opcion_borrar = opcion;
    }
    
    this.setOpcionImpresora = function(opcion){
        this.opcion_impresora = opcion;
    }
    
    this.setEncabezados = function(encabezados){
        this.encabezados = encabezados;
    }
    
    this.setMedidas = function(medidas){
        this.medidas = medidas;
    }
    
    this.setAlineaciones = function(alineaciones){
        this.alineaciones = alineaciones;
    }
    
    this.setFuncionEliminaElemento = function(funcion){
        this.funcion_elimina_elemento = funcion;
    }
    
    this.setOpcionesRecuperarElementos = function(opciones){
        this.opciones_recuperar_elementos = opciones;
    }
    
    this.getTituloLista = function(){
        return document.getElementById(this.nombre_instancia+"_titulo_lista_seleccionable");
    }
    
    this.setTituloLista = function(titulo){
        this.titulo_lista = titulo;
        document.getElementById(this.nombre_instancia+"_titulo_lista_seleccionable").innerHTML = titulo;
    }
    
    this.getBotonAgregarElemento = function(){
        return document.getElementById(this.nombre_instancia+"_boton_nuevo_elemento");
    }
    
    this.getZonaElementos = function(){
        return document.getElementById(this.nombre_instancia+"_zona_elementos_lista");
    }
    
    this.getPosicionElementoSeleccionado = function(){
        return this.posicion_elemento_seleccionado;
    }
    
    this.getDatosDeElementoSeleccionado = function(){
        //console.log("posicion elemento seleccionado: "+this.posicion_elemento_seleccionado);
        return this.elementos_recuperados[this.posicion_elemento_seleccionado];
    }
    
    this.getIdElementoSeleccionado = function(){
        var info = this.elementos_recuperados[this.posicion_elemento_seleccionado].split("|");
        return info[0];
    }
    
    //esta función recupera la fila en donde se hizo click que equivale a la posición del elemento seleccionado + 1
    this.getFilaDeElementoSeleccionado = function(){
        return document.getElementById(this.nombre_instancia+"_tabla_elementos").rows[this.posicion_elemento_seleccionado+1];
    }
    
    this.setFuncionElementoSeleccionado = function(funcion){
        this.funcion_elemento_seleccionado = funcion;
    }
    
    this.setFuncionDetalleElementoSeleccionado = function(funcion){
        this.funcion_detalle_elemento_seleccionado = funcion;
    }
    
    this.setFuncionImprimirElementoSeleccionado = function(funcion){
        this.funcion_imprime_elemento_seleccionado = funcion;
    }
    
    //esta funci—n agrega el elemento nuevo al final de los elementos_recuperados
    this.agregaElemento = function(datos){
        if (!this.elementos_recuperados) this.elementos_recuperados = new Array();
        this.elementos_recuperados[this.elementos_recuperados.length] = datos;
        
        //se pone como el último elemento seleccionado
        this.posicion_elemento_seleccionado = this.elementos_recuperados.length-1;
    }
    
    //esta función agrega un elemento o sobreescribe en la posición dada
    this.agregaElementoEnPosicion = function(datos, posicion){
        if (!this.elementos_recuperados) this.elementos_recuperados = new Array();
        this.elementos_recuperados[posicion] = datos;
        
        //se pone como el último elemento seleccionado
        this.posicion_elemento_seleccionado = posicion;
    }
    
    //esta función recupera el número de elementos de la lista
    this.getNumeroElementosEnLaLista = function(){
        if(!this.elementos_recuperados) return -1;
        return this.elementos_recuperados.length;
    }
    
    //esta funci—n modifica el elemento en la posici—n del elemento seleccionado
    this.modificaElementoEnPosicionSeleccionada = function(datos){
        if (!this.elementos_recuperados) this.elementos_recuperados = new Array();
        this.elementos_recuperados[this.posicion_elemento_seleccionado] = datos;
        this.posicion_elemento_seleccionado = -1;
    }
    
    //esta función compara el id dado con los id de la lista actual. Si existe devuelve true, en otro caso false
    this.existeElementoEnLaListaPorId = function(idelemento){
        if(!this.elementos_recuperados) return false;
        var elemento;
        for(var i=0; i<this.elementos_recuperados.length; i++){
            elemento = this.elementos_recuperados[i].split("|");
            if(elemento[0] == idelemento) return true;
        }
        return false;
    }
    
    //si existe el elemento por id mostrado, entonces se remarcará
    this.remarcaElementoPorId = function(idelemento){
        var fila = document.getElementById(idelemento);
        if(fila){
            $("#"+idelemento).show("highlight ", 1000);
        }
    }
    
    //esta funci—n muestra el componente en su vista para seleccionar
    this.muestraComponente = function(){
        var contenido = "<div id='"+this.nombre_instancia+"_div_lista'>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_elementos_lista'></div><br/>";
        contenido += "</div>";
        
        this.contenedor.innerHTML = contenido;
    }
    
    //esta funci—n muestra el componente en su vista sin el boton de agregar nuevo elemento
    this.muestraComponenteSinOpcionAgregar = function(){
        var contenido = "<div id='"+this.nombre_instancia+"_div_lista'>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_elementos_lista'></div><br/>";
        contenido += "</div>";
        
        this.contenedor.innerHTML = contenido;
    }
    
    //esta funci—n muestra la tabla de elementos que se tienen en elementos_recuperados. Si es null, entonces no muestra la tabla. Se muestra de forma predeterminada, pero si se desea se podr’a sobreescribir el mŽtodo para mostrar la tabla de elementos de la forma deseada
    this.muestraTablaElementos = function(){
        this.implementa_busqueda = false;
        if (!this.elementos_recuperados || this.elementos_recuperados.length == 0){
            this.getZonaElementos().innerHTML = "No hay elementos que mostrar";
            if(this.funcion_tabla_mostrada) setTimeout(this.funcion_tabla_mostrada, 0);
            return;
        }
        
        var aux = this.ocultaColumnas(this.elementos_recuperados.slice(0));
        
        var alto_div = (($(window).height())-($(this.contenedor).offset().top))-120;
        
        var contenido = "<br/><table id='"+this.nombre_instancia+"_tabla_titulos_elementos' align='center' width='98%' cellpadding='5' cellspacing='0' border='0'>";
        
        //agregamos la informaci—n de los encabezados
        contenido += "<tr class='fila_encabezados'><td align='center' width='10%' valign='bottom'>No.</td>";
        for(var i=0; i<this.encabezados.length; i++) contenido += "<td align='center' width='"+this.medidas[i]+"' valign='middle'>"+this.encabezados[i]+"</td>";
        
        if(this.getOpcionesDeElementos() != null){
            contenido += "<td align='center' width='10%' valign='bottom'>Opci&oacute;n</td></tr></table>";
        }else contenido += "</tr></table>";
        
        //agregamos la informaci—n de los elementos recuperados
        var clase = "";
        var elemento;
        
        //style='height:"+alto_div+"px; overflow-y:auto;'
        if(this.es_contenedor_flotante) contenido += "<div><div class='fin_lista'></div><table  id='"+this.nombre_instancia+"_tabla_elementos' align='center' width='98%' cellpadding='5' cellspacing='0' border='0'>";
        else  contenido += "<div><div class='fin_lista'></div><table id='"+this.nombre_instancia+"_tabla_elementos' align='center' width='98%' cellpadding='5' cellspacing='0' border='0'>";
            
        for(var i=0; i<aux.length; i++){
            
            elemento = aux[i].split("|");
            
            if (i%2 == 0) clase = "fila_lista_uno";
            else clase = "fila_lista_dos";
            
            contenido += "<tr class='"+clase+"' id='"+this.nombre_instancia+"_"+elemento[0]+"'><td align='center' onclick='"+this.nombre_instancia+".elementoSeleccionadoPorPosicion("+i+")' width='10%'>"+(i+1)+"</td>";
            
            for(var j=1; j<=this.encabezados.length; j++){
                if(this.funciones_celdas_editables && this.funciones_celdas_editables[j]) contenido += "<td align='"+this.alineaciones[j-1]+"' width='"+this.medidas[j-1]+"'><input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_celda_"+(i+1)+"_"+j+"' value='"+elemento[j]+"' onkeypress='"+this.nombre_instancia+".elementoSeleccionadoPorPosicion("+i+"); "+this.nombre_instancia+".campo_seleccionado = this; isEnterLaTeclaEnCampo(event, \""+this.funciones_celdas_editables[j]+"\");'/></td>";
                else {
                    if(this.esColumnaConEventoOnclick(j-1))
                        contenido += "<td align='"+this.alineaciones[j-1]+"' width='"+this.medidas[j-1]+"' onclick='"+this.nombre_instancia+".elementoSeleccionadoPorPosicion("+i+")'>"+elemento[j]+"</td>";
                    else
                        contenido += "<td align='"+this.alineaciones[j-1]+"' width='"+this.medidas[j-1]+"' >"+elemento[j]+"</td>";
                }
            }
            if(this.getOpcionesElementoPorPosicion() != null) contenido += "<td align='center' width='10%'>"+this.getOpcionesElementoPorPosicion(i)+"</td></tr>";
            else contenido += "</tr>";
        }
        contenido += "</table><div class='fin_lista'></div></div>";
        
        this.getZonaElementos().innerHTML = contenido;
        if(this.getTituloLista()) this.getTituloLista().innerHTML = "";
        if(this.funcion_tabla_mostrada) setTimeout(this.funcion_tabla_mostrada, 0);
        
        this.muestraElementoResaltado();
    }
    
    this.getTablaElementos = function(){
        return document.getElementById(this.nombre_instancia+"_tabla_elementos");
    }
    
    this.ordenaPorColumna = function(numeroColumna){
        if(!this.columnasOrdenadas){
            this.columnasOrdenadas = new Array();
            for(var i=0; i<this.encabezados.length+1; i++) this.columnasOrdenadas[i] = 1;
        }
        
        var sorted = null;
        if(this.getColumnasOrdenadas()[numeroColumna] == 1){
            //sorted ascending
            sorted = sortMatrix(this.getElementosRecuperadosEnVector(), numeroColumna);
            this.columnasOrdenadas[numeroColumna] = 2;
        }else if(this.getColumnasOrdenadas()[numeroColumna] == 2){
            //sorted descending
            sorted = sortMatrix(this.getElementosRecuperadosEnVector(), numeroColumna);
            sorted.reverse();
            this.columnasOrdenadas[numeroColumna] = 1;
        }
        
        var cadena = ""; var vector;
        for(var i=0; i<sorted.length; i++){
            vector = sorted[i];
            for(var j=0; j<vector.length; j++){
                if(j == vector.length-1) cadena += vector[j];
                else cadena += vector[j]+"|";
            }
            cadena += "^";
        }
        var elementos = cadena.split("^");
        elementos.pop();
        
        this.setElementosRecuperados(elementos);
        this.muestraTablaElementosConBusqueda();
    }
    
    //esta funcion copia la implementacion de muestraTablaElementos, pero agrega funcionalidad para busqueda
    this.muestraTablaElementosConBusqueda = function(expresion_regular){
        if (!this.elementos_recuperados || this.elementos_recuperados.length == 0){
            this.getZonaElementos().innerHTML = "No hay elementos que mostrar";
            if(this.funcion_tabla_mostrada) setTimeout(this.funcion_tabla_mostrada, 0);
            return;
        }
        
        //realizamos el filtrado por aquellos elementos que contengan la expresion dada como parametros
        var aux = new Array();
        if(expresion_regular){
            for(var i=0; i<this.elementos_recuperados.length; i++){
                if(expresion_regular.test(this.elementos_recuperados[i])) aux[aux.length] = this.elementos_recuperados[i];
            }
            aux = this.ocultaColumnas(aux);
        }else aux = this.ocultaColumnas(this.elementos_recuperados.slice(0));
        
        var alto_div = (($(window).height())-($(this.contenedor).offset().top))-120;
        
        var contenido = "<br/><table id='"+this.nombre_instancia+"_tabla_titulos_elementos' align='center' width='98%' cellpadding='3' cellspacing='0' border='0'>";
        
        //agregamos la informaci—n de los encabezados
        contenido += "<tr class='fila_encabezados'><td align='center' width='10%' valign='top'>No.</td>";
        for(var i=0; i<this.encabezados.length; i++) contenido += "<td align='center' width='"+this.medidas[i]+"' valign='middle'><div class='headerSelectable' onclick='"+this.nombre_instancia+".ordenaPorColumna("+(i+1)+")'>"+this.encabezados[i]+"</div></td>";
        
        if(this.getOpcionesDeElementos() != null){
            contenido += "<td align='center' width='10%' valign='top'>Opci&oacute;n</td></tr></table>";
        }else contenido += "</tr></table>";
        
        //agregamos la informaci—n de los elementos recuperados
        var clase = "";
        var elemento;
        
        //style='height:"+alto_div+"px; overflow-y:auto;'
        if(this.es_contenedor_flotante) contenido += "<div><div class='fin_lista'></div><table id='"+this.nombre_instancia+"_tabla_elementos' align='center' align='center' width='98%' cellpadding='3' cellspacing='0' border='0'>";
        else  contenido += "<div><div class='fin_lista'></div><table id='"+this.nombre_instancia+"_tabla_elementos' align='center' width='98%' cellpadding='3'  cellspacing='0' border='0'>";
            
        for(var i=0; i<aux.length; i++){
            
            elemento = aux[i].split("|");
            
            if(this.tipo_elementos == 1){
                if (i%2 == 0) clase = "fila_lista_uno";
                else clase = "fila_lista_dos";   
            }else{
                if (i%2 == 0) clase = "fila_lista_uno_small";
                else clase = "fila_lista_dos_small";   
            }
            
            contenido += "<tr class='"+clase+"' id='"+this.nombre_instancia+"_"+elemento[0]+"'><td align='center' onclick='"+this.nombre_instancia+".elementoSeleccionado("+elemento[0]+")' width='10%' valign='top'>"+(i+1)+"</td>";
            
            for(var j=1; j<=this.encabezados.length; j++){
                if(this.funciones_celdas_editables && this.funciones_celdas_editables[j]) contenido += "<td align='"+this.alineaciones[j-1]+"' width='"+this.medidas[j-1]+"' valign='top'><input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_celda_"+(i+1)+"_"+j+"' value='"+elemento[j]+"' onkeypress='"+this.nombre_instancia+".elementoSeleccionado("+i+", "+j+", this); "+this.nombre_instancia+".campo_seleccionado = this; isEnterLaTeclaEnCampo(event, \""+this.funciones_celdas_editables[j]+"\");'/></td>";
                
                else{
                    if(this.esColumnaConEventoOnclick(j-1))
                        contenido += "<td align='"+this.alineaciones[j-1]+"' width='"+this.medidas[j-1]+"' onclick='"+this.nombre_instancia+".elementoSeleccionado("+elemento[0]+");' valign='top'>"+elemento[j]+"</td>";
                    else 
                        contenido += "<td align='"+this.alineaciones[j-1]+"' width='"+this.medidas[j-1]+"' valign='top'>"+elemento[j]+"</td>";
                }
            }
            
            if(this.getOpcionesDeElementos() != null){
                contenido += "<td align='center' width='10%'>"+this.getOpcionesDeElementos(elemento[0])+"</td></tr>";
            }else contenido += "</tr>";
        }
        contenido += "</table><div class='fin_lista'></div></div>";
        
        this.getZonaElementos().innerHTML = contenido;
        
        this.elementosVisiblesEnLista = aux.slice(0);
        
        if(this.getTituloLista()) this.getTituloLista().innerHTML = "";
        if(this.funcion_tabla_mostrada) setTimeout(this.funcion_tabla_mostrada, 0);
        this.muestraElementoResaltado();
    }
    
    this.muestraElementoResaltado = function(){
        var fila_resaltar = document.getElementById(this.nombre_instancia+"_"+this.idelemento_resaltar);
        if(!fila_resaltar) return;
        
        $('html,body').animate({scrollTop:($(fila_resaltar).position().top-200)+"px"}, 500);
        $(fila_resaltar).show("highlight", {color: '#00AAE8'}, 15000, null);
    }
    
    this.getCampoSeleccionado = function(){
        return this.campo_seleccionado;
    }
    
    this.getOpcionesDeElementos = function(idelemento){
        if(!this.opcion_borrar && !this.opcion_detalles) return null;
        var contenido = "<table align='center' cellpadding='5' cellspacing='0' border='0'>";
        contenido += "<tr align='center' valign='top'>";
        
        if (this.opcion_borrar == 1) contenido += "<td align='center'><div class='delete_pk' onclick='"+this.nombre_instancia+".eliminaElemento("+idelemento+")'></div></td>";
        if (this.opcion_detalles == 1) contenido += "<td align='center'><div class='descargar_pk' onclick='"+this.nombre_instancia+".muestraDetalleDeElementoSeleccionado("+idelemento+")'></div></td>";
        
        contenido += "</tr></table>";
        return contenido;
    }
    
    this.getOpcionesElementoPorPosicion = function(posicion){
        //console.log(this.opcion_borrar+" - "+this.opcion_impresora);
        if(!this.opcion_borrar && !this.opcion_detalles && !this.opcion_impresora) return null;
        var contenido = "<table align='center' cellpadding='5' cellspacing='0' border='0'>";
        contenido += "<tr align='center' valign='top'>";
        
        if (this.opcion_borrar == 1) contenido += "<td align='center'><div class='delete_pk' onclick='"+this.nombre_instancia+".eliminaElementoPorPosicion("+posicion+")'></div></td>";
        if (this.opcion_detalles == 1) contenido += "<td align='center'><div class='descargar_pk' onclick='"+this.nombre_instancia+".muestraDetalleDeElementoSeleccionado("+posicion+")'></div></td>";
        if (this.opcion_impresora == 1) contenido += "<td align='center'><div class='imprimir_pk' onclick='"+this.nombre_instancia+".imprimirElementoSeleccionado("+posicion+")'></div></td>";
        
        contenido += "</tr></table>";
        return contenido;
    }
    
    //función para imprimir el elemento seleccionado
    this.imprimirElementoSeleccionado = function(posicion){
        this.posicion_elemento_seleccionado = posicion;
        
        if(this.funcion_imprime_elemento_seleccionado != "") setTimeout(this.funcion_imprime_elemento_seleccionado, 0);
    }
    
    //esta funci—n elimina el elemento identificado por posicion
    this.eliminaElemento = function(idelemento){
        if (confirm("Deseas eliminar este elemento?")){
            if(this.funcion_elimina_elemento) setTimeout(this.funcion_elimina_elemento, 0);
            this.posicion_elemento_seleccionado = this.getPosicionElementoSeleccionadoPorId(idelemento);
            this.idelemento_eliminado = idelemento;
        }
    }
    
    this.eliminaElementoPorPosicion = function(posicion){
        if (confirm("Deseas eliminar este elemento?")){
            //console.log("eliminando elemento de posicion: "+posicion);
            this.posicion_elemento_seleccionado = posicion;
            var partes = this.elementos_recuperados[posicion].split("|");
            //console.log("idelemento: "+partes[0]);
            if(partes[0] != 0) this.idelemento_eliminado = partes[0];
            if(this.idelemento_eliminado > 0){
                if(this.funcion_elimina_elemento){
                    setTimeout(this.funcion_elimina_elemento, 0);
                    return;
                }
            }
            this.quitaElementoDeListaPorPosicion(posicion);
        }
    }
    
    this.quitaElementoDeListaPorPosicion = function(posicion){
        var arreglo_auxiliar = new Array();
            
        //console.log("eliminar de posicion: "+posicion);
        for(var i=0; i<this.elementos_recuperados.length; i++){
            if (i != posicion) arreglo_auxiliar[arreglo_auxiliar.length] = this.elementos_recuperados[i];
        }
            
        this.elementos_recuperados = arreglo_auxiliar.slice(0);
        if(this.implementa_busqueda) this.muestraTablaElementosConBusqueda(null);
        else this.muestraTablaElementos();
        this.posicion_elemento_seleccionado = -1;
    }
    
    this.quitaElementoDeListaPorId = function(idelemento){
        var posicion = this.getPosicionElementoSeleccionado();
        var arreglo_auxiliar = new Array();
            
        for(var i=0; i<this.elementos_recuperados.length; i++){
            if (i != posicion) arreglo_auxiliar[arreglo_auxiliar.length] = this.elementos_recuperados[i];
        }
            
        this.elementos_recuperados = arreglo_auxiliar.slice(0);
        this.muestraTablaElementosConBusqueda(null);
    }
    
    this.getIdElementoEliminado = function(){
        return this.idelemento_eliminado;
    }
    
    this.getPosicionElementoSeleccionadoPorId = function(idelemento){
        var partes;
        for(var i=0; i<this.elementos_recuperados.length; i++){
            partes = this.elementos_recuperados[i].split("|");
            if(partes[0] == idelemento) return i;
        }
        return -1;
    }
    
    this.setPosicionElementoSeleccionado = function(posicion){
        this.posicion_elemento_seleccionado = posicion;
    }
    
    this.elementoSeleccionadoPorPosicion = function(posicion){
        this.posicion_elemento_seleccionado_anterior = this.posicion_elemento_seleccionado;
        this.posicion_elemento_seleccionado = posicion;
        
        /*this.numero_celda_seleccionada = numero_columna;
        this.celda_seleccionada = elemento_celda;*/
        
        if(this.funciones_celdas_editables && this.funciones_celdas_editables[numero_columna]) setTimeout(this.funciones_celdas_editables[numero_columna], 0);
        else setTimeout(this.funcion_elemento_seleccionado, 0);
        //console.log("posicion actual presionada sin busqueda: "+this.posicion_elemento_seleccionado);
    }
    
    //funci—n invocada cuando se hace click sobre un elemento
    this.elementoSeleccionado = function(idelemento){
        var posicion = this.getPosicionElementoSeleccionadoPorId(idelemento);
        this.posicion_elemento_seleccionado_anterior = this.posicion_elemento_seleccionado;
        this.posicion_elemento_seleccionado = posicion;
        
        /*this.numero_celda_seleccionada = numero_columna;
        this.celda_seleccionada = elemento_celda;*/
        
        console.log("funcion elemento seleccionado: "+this.funcion_elemento_seleccionado);
        
        if(this.funciones_celdas_editables && this.funciones_celdas_editables[numero_columna]) setTimeout(this.funciones_celdas_editables[numero_columna], 0);
        else setTimeout(this.funcion_elemento_seleccionado, 0);
    }
    
    //funciones que deber‡n ser implementadas cuando se use una lista seleccionable
    this.agregaElementoEnLaLista = function(nuevo_elemento){
        if(!this.elementos_recuperados) this.elementos_recuperados = new Array();
        if(this.posicion_elemento_seleccionado != -1) this.elementos_recuperados[this.posicion_elemento_seleccionado] = nuevo_elemento;
        else this.elementos_recuperados[this.elementos_recuperados.length] = nuevo_elemento;
    }
    
    //esta funci—n proporciona el comportamiento a detalle de cuando se selecciona un elemento de la lista
    this.muestraDetalleDeElementoSeleccionado = function(idelemento){
        var posicion = this.getPosicionElementoSeleccionadoPorId(idelemento);
        
        this.posicion_elemento_seleccionado_anterior = this.posicion_elemento_seleccionado;
        this.posicion_elemento_seleccionado = posicion;
        
        setTimeout(this.funcion_detalle_elemento_seleccionado, 0);
    }
    
    //Esta funci—n recupera los elementos de la lista, dependiendo de la configuraci—n que se especifique para la lista
    this.getElementosDeLaLista = function(){
        if (this.getZonaElementos()) this.getZonaElementos().innerHTML = "<div class='cargando_grande'></div>";
        var componente = this;
        var ajax = objetoAjax();
        ajax.open("POST", enlace, true);
        ajax.onreadystatechange=function() {
                if (ajax.readyState == 4){
                    //console.log("respuesta al recuperar elementos: "+ajax.responseText);
                    if(ajax.responseText == "") componente.elementos_recuperados = Array();
                    else{
                        componente.elementos_recuperados = ajax.responseText.split("^");
                        componente.elementos_recuperados.pop();
                    }
                    componente.muestraTablaElementos();
                }
        }
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("token="+login.getToken()+"&"+this.opciones_recuperar_elementos);
    }
    
    //esta función recupera un elemento específico de la lista
    this.getElementoEnPosicion = function(posicion){
        if(!this.elementos_recuperados || posicion >= this.elementos_recuperados.length) return null;
        return this.elementos_recuperados[posicion];
    }
    
    //esta función recupera la celda del elemento seleccionado, dada la posición
    this.getCeldaDeElementoSeleccionadaPorPosicion = function(posicion){
        var datos = this.elementos_recuperados[this.posicion_elemento_seleccionado].split("|");
        var fila = document.getElementById(this.nombre_instancia+"_"+datos[0]);
        return fila.cells[posicion];
    }
    
    //esta función modifica el dato en el elemento seleccionado y lo guarda nuevamente en los elementos recuperados
    this.modificaDatoDeElementoSeleccionado = function(dato, posicion){
        var datos = this.elementos_recuperados[this.posicion_elemento_seleccionado].split("|");
        datos[posicion] = dato;
        var nueva_info = "";
        for(var i=0; i<datos.length; i++){
            nueva_info += datos[i]+"|";
        }
        this.elementos_recuperados[this.posicion_elemento_seleccionado] = nueva_info;
    }
    
    //esta función pone como seleccionada la fila anterior de la fila seleccionada actual, solo si la fila seleccionada actual es mayor que 1. En otro caso regresa sin más
    this.setFilaAnteriorSeleccionada = function(){
        if(this.posicion_elemento_seleccionado > 1){
            this.posicion_elemento_seleccionado--;
        }
    }
}