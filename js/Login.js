var Login = function(){
    
    this.funcion_si_hay_sesion_activa = null;
    this.setFuncionSiHaySesionActiva = function(funcion){
        this.funcion_si_hay_sesion_activa = funcion;
    }
    
    this.sesion_activa = null;
    this.haySesionActiva = function(){
        if(!this.sesion_activa){
            //checamos la sesión en el servidor
            getResultadoAjax("opcion=2", this.nombre_instancia+".procesaSesionRecuperada", null);
        }else setTimeout(this.funcion_si_hay_sesion_activa, 0);
    }
    
    this.procesaSesionRecuperada = function(respuesta){
        console.log("sesion activa: "+respuesta);
        this.sesion_activa = respuesta;
        if(respuesta == 0) this.muestraComponente();
        else{
            this.getContenedorModal().setPermisoParaCerrar(true);
            this.getContenedorModal().setVisibilidadBotonCerrar(true);
            this.getContenedorModal().ocultaComponente();
            setTimeout(this.funcion_si_hay_sesion_activa, 0);
        }
    }
    
    this.muestraComponente = function(){
        this.muestraContenedorModal("P", 600, 300);
        this.getContenedorModal().setPermisoParaCerrar(false);
        this.getContenedorModal().setVisibilidadBotonCerrar(false);
        
        var contenido = "<br/>";
        
        contenido += "<img src='images/logo2.jpg' width='200'/><br/><br/><br/>";
        contenido += "<div class='texto_forma'>Usuario</div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_usuario' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe tu nombre de usuario\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\");'/>";
        
        contenido += "<br/>";
        contenido += "<div id='"+this.nombre_instancia+"_a1' class='ayuda_campos'></div>";
        contenido += "<br/>";
        
        contenido += "<div class='texto_forma'>Password</div>";
        contenido += "<input type='password' class='campo_forma' id='"+this.nombre_instancia+"_password' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe tu password\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\");' onkeypress='"+this.nombre_instancia+".isEnterLaTeclaEnCampoPassword(event)'/>";
        
        contenido += "<br/><br/>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_guardar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()'>Entrar</a></div>";
        
        this.getContenedorModal().getContenedorDeContenido().innerHTML = contenido;
    }
    
    this.isEnterLaTeclaEnCampoPassword = function(evento){
        //recupera el código de la tecla presionada
		var tecla_presionada = (document.all)?evento.keyCode:evento.which;
		//si la tecla presionada es igual a 13, entonces se valida la forma de logueo
		if(tecla_presionada == 13) this.validaForma();
    }
    
    this.validaForma = function(){
        var usuario = document.getElementById(this.nombre_instancia+"_usuario");
        if(usuario.value == "" || usuario.value == " "){
            usuario.focus();
            return;
        }
        
        var password = document.getElementById(this.nombre_instancia+"_password");
        if(password.value == "" || password.value == " "){
            password.focus();
            return;
        }
        
        getResultadoAjax("opcion=3&usuario="+usuario.value+"&password="+password.value, this.nombre_instancia+".procesaSesionRecuperada", document.getElementById(this.nombre_instancia+"_zona_guardar"));
    }
    
    this.getNombreCompletoDeUsuarioLogueado = function(){
        if(!this.sesion_activa) return "";
        var datos = this.sesion_activa.split("|");
        return datos[2];
    }
    
    this.getToken = function(){
        if(!this.sesion_activa) return "";
        var datos = this.sesion_activa.split("|");
        return datos[0];
    }
    
    this.getPerfil = function(){
        if(!this.sesion_activa) return "";
        var datos = this.sesion_activa.split("|");
        return datos[4];
    }
}