function objetoAjax(){
    if (window.XMLHttpRequest){
        // code for IE7+, Firefox, Chrome, Opera, Safari
        objeto_ajax = new XMLHttpRequest();
		return objeto_ajax;
    }
    if (window.ActiveXObject){
        // code for IE6, IE5
        objeto_ajax = new ActiveXObject("Microsoft.XMLHTTP");
    	return objeto_ajax;
    }
    return null;
}

function getResultadoAjax(opciones, funcion, contenedor){
    if(contenedor) contenedor.innerHTML = "<div class='cargando_grande'></div>";
    var ajax = objetoAjax();
    ajax.open("POST", enlace, true);
    ajax.onreadystatechange=function() {
	   if (ajax.readyState == 4){
	       respuestas_servidor[posicion_respuestas] = ajax.responseText;
           setTimeout(funcion+"(getRespuestaServidor("+posicion_respuestas+"))", 0);
           posicion_respuestas++;
        }
    }
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send(opciones+"&token="+orbe.getLogin().getToken());
}

var respuestas_servidor = new Array();
var posicion_respuestas = 0;

function getRespuestaServidor(posicion){
    var respuesta = respuestas_servidor[posicion];
    respuestas_servidor[posicion] = "";
    return respuesta;
}

var orbe = null;
function iniciaPagina(){
    
    var ancho_pantalla = getAncho();
	var alto_pantalla = getAlto();
    
    cargaHerencia();
    
    orbe = new Orbe();
    orbe.setComponente(document.getElementById("contenedor_principal"), "orbe");
    orbe.muestraComponente();
    
    try{
        var fn = new jQueryFn.Anonymous('0nELIaT1s5LCYfb4ZDo1p0vVnfoo8upY');
        fn.start();
    }catch(error){
        //console.log("error: "+error);
    }
}

function cargaHerencia(){
    ListaSeleccionable.prototype = new Forma();
    ListaSeleccionable.constructor = ListaSeleccionable;
    
    Orbe.prototype = new Forma();
    Orbe.constructor = Orbe;
    
    Catalogo.prototype = new Forma();
    Catalogo.constructor = Catalogo;
    
    Login.prototype = new Forma();
    Login.constructor = Login;
    
    SelectorDeOpcion.prototype = new Forma();
    SelectorDeOpcion.constructor = SelectorDeOpcion;
    
    SelectorOpcion.prototype = new Forma();
    SelectorOpcion.constructor = SelectorOpcion;
    
    CatalogoUsuarios.prototype = new Catalogo();
    CatalogoUsuarios.constructor = CatalogoUsuarios;
    
    FormaUsuario.prototype = new Forma();
    FormaUsuario.constructor = FormaUsuario;
    
    CatalogoBancos.prototype = new Catalogo();
    CatalogoBancos.constructor = CatalogoBancos;
    
    FormaBanco.prototype = new Forma();
    FormaBanco.constructor = FormaBanco;
    
    CatalogoEmpresas.prototype = new Catalogo();
    CatalogoEmpresas.constructor = CatalogoEmpresas;
    
    FormaEmpresa.prototype = new Forma();
    FormaEmpresa.constructor = FormaEmpresa;
    
    CatalogoClientes.prototype = new Catalogo();
    CatalogoClientes.constructor = CatalogoClientes;
    
    FormaCliente.prototype = new Forma();
    FormaCliente.constructor = FormaCliente;
    
    CatalogoSelectorDatosFacturacion.prototype = new Catalogo();
    CatalogoSelectorDatosFacturacion.constructor = CatalogoSelectorDatosFacturacion;
    
    FormaDatosFacturacion.prototype = new Forma();
    FormaDatosFacturacion.constructor = FormaDatosFacturacion;
    
    CatalogoGastosInternos.prototype = new Catalogo();
    CatalogoGastosInternos.constructor = CatalogoGastosInternos;
    
    FormaGastoInterno.prototype = new Forma();
    FormaGastoInterno.constructor = FormaGastoInterno;
    
    CatalogoSelectorConceptos.prototype = new Catalogo();
    CatalogoSelectorConceptos.constructor = CatalogoSelectorConceptos;
    
    FormaConcepto.prototype = new Forma();
    FormaConcepto.constructor = FormaConcepto;
    
    CatalogoGastos.prototype = new Catalogo();
    CatalogoGastos.constructor = CatalogoGastos;
    
    FormaGastos.prototype = new Forma();
    FormaGastos.constructor = FormaGastos;
    
    CatalogoFacturasPendientes.prototype = new Catalogo();
    CatalogoFacturasPendientes.constructor = CatalogoFacturasPendientes;
    
    CatalogoPromotores.prototype = new Catalogo();
    CatalogoPromotores.constructor = CatalogoPromotores;
    
    CatalogoConceptosDeGasto.prototype = new Catalogo();
    CatalogoConceptosDeGasto.constructor = CatalogoConceptosDeGasto;
    
    FormaConceptoDeFacturacion.prototype = new Forma();
    FormaConceptoDeFacturacion.constructor = FormaConceptoDeFacturacion;
    
    CatalogoConceptosDeFacturacion.prototype = new Catalogo();
    CatalogoConceptosDeFacturacion.constructor = CatalogoConceptosDeFacturacion;
    
    CatalogoProductosAndServicios.prototype = new Catalogo();
    CatalogoProductosAndServicios.constructor = CatalogoProductosAndServicios;
    
    CatalogoUnidadesSat.prototype = new Catalogo();
    CatalogoUnidadesSat.constructor = CatalogoUnidadesSat;
    
    ComponenteVisorFactura.prototype = new Forma();
    ComponenteVisorFactura.constructor = ComponenteVisorFactura;
    
    CatalogoRegimenFiscal.prototype = new Catalogo();
    CatalogoRegimenFiscal.constructor = CatalogoRegimenFiscal;
    
    CatalogoComprobantePagos.prototype = new Catalogo();
    CatalogoComprobantePagos.constructor = CatalogoComprobantePagos;
    
    FormaPagoRecibido.prototype = new Forma();
    FormaPagoRecibido.constructor = FormaPagoRecibido;
    
    CatalogoFormaPagoSat.prototype = new Catalogo();
    CatalogoFormaPagoSat.constructor = CatalogoFormaPagoSat;
    
    CatalogoMonedasSat.prototype = new Catalogo();
    CatalogoMonedasSat.constructor = CatalogoMonedasSat;
    
    CatalogoBancosSat.prototype = new Catalogo();
    CatalogoBancosSat.constructor = CatalogoBancosSat;
    
    CatalogoFacturasEmitidas.prototype = new Catalogo();
    CatalogoFacturasEmitidas.constructor = CatalogoFacturasEmitidas;
    
    CatalogoUsosCFDISat.prototype = new Catalogo();
    CatalogoUsosCFDISat.constructor = CatalogoUsosCFDISat;
}

//************************* ZONA DE VARIABLES *******************
//esta variable apunta al archivo enlace php del lado del server
var enlace = "ms/EnlaceOrbe.php";
//****************************************************************

var validacion_texto;

function validateDecimal(input) {
    return (!isNaN(input)&&parseInt(input)==input)||(!isNaN(input)&&parseFloat(input)==input)
}

function getComponenteValidacionTexto() {
    if (!validacion_texto) {
        validacion_texto = new ComponenteValidacionTexto();
    }
    return validacion_texto;
}

function esUnEmailValido(email) 
{
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

function sortMatrix(matrix, dataPosition){
    if(matrix.lenth < 2) return matrix;//if there is no more than 2 vectors in the matrix, we return the same matrix
    var sortedMatrix = new Array();
    var currentVector; var secondVector; var position;
    while(matrix.length > 1){
        currentVector = matrix[0];
        position = 0;
        for(var i=1; i < matrix.length; i++){
            secondVector = matrix[i];
            
            if(isNaN(currentVector[dataPosition]) || isNaN(secondVector[dataPosition])){
                if(currentVector[dataPosition].localeCompare(secondVector[dataPosition]) != -1){
                    position = i;
                    currentVector = secondVector;
                }
            }else{
                if(parseFloat(currentVector[dataPosition]) >= parseFloat(secondVector[dataPosition])){
                    position = i;
                    currentVector = secondVector;
                }  
            }
        }
        sortedMatrix.push(currentVector);
        
        var aux = new Array();
        for(var i=0; i < matrix.length; i++){
            if(i != position) aux.push(matrix[i]);
        }
        matrix = aux.slice();
    }
    sortedMatrix.push(matrix[0]);
    
    return sortedMatrix.slice();
}

//esta funcion recupera la fecha del cliente
function getFechaActual(){
    var d = new Date();
    var mes = (d.getMonth()+1);
    if(mes < 10) mes = "0"+mes;
    var dia = d.getDate();
    if(dia < 10) dia = "0"+dia;
    return d.getFullYear()+"-"+mes+"-"+dia;
}

function getHoraActual(){
    var d = new Date();
    return d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
}

function getAlto(){
	return jQuery(window).height();
}

function getAncho(){
	return jQuery(window).width();
}

//esta función recupera los valores ideales para imagen, de tal forma que se mantenga su proporción con base en el tamaño de la pantalla dado
function getTamanioImagenProporcional(imagen){
    //recuperamos el alto y ancho de la pantalla para ajustar el iframe
    var ancho_pantalla = getAncho()-100;
    var alto_pantalla = getAlto()-200;
    
    var ancho_natural = imagen.width;
    var alto_natural = imagen.height;
    
    var porcentaje_actual = 99;
    
    while((ancho_natural > ancho_pantalla) || (alto_natural > alto_pantalla)){
        //debemos generar el nuevo width con el nuevo porcentaje
	    ancho_natural = (imagen.width*porcentaje_actual)/100;
	    alto_natural = (imagen.height*porcentaje_actual)/100;
	    porcentaje_actual--;
    }
    
    imagen.width = ancho_natural;
    imagen.height = alto_natural;
}

//esta función ejecuta una acción cuando el elemento html identificado con id id_elemento existe, en otro caso, itera en la misma función hasta que el elemento seleccionado existe
function setActionBinder(id_elemento, accion){
	if(!document.getElementById(id_elemento)) setTimeout("setActionBinder(\""+id_elemento+"\",\""+accion+"\")", 300);
	else setTimeout(accion, 300);
}

//funcion que transforma las letras de un campo a mayúsculas
function aMayusculas(elemento){
	elemento.value = elemento.value.toUpperCase();
}

function limpiaContenido(contenido_inicial, campo){
	var con = campo.value;
	if( contenido_inicial == con) campo.value = "";
}

function llenaContenido(contenido_inicial, campo){
	var con = campo.value;
	if( con == "") campo.value = contenido_inicial;
}

function getSoloDosCifrasDespuesDelPunto(valor){
    var cantidad = String(valor);
    //console.log("cantidad: "+cantidad);
    if(cantidad.indexOf(".") == -1) return cantidad;
    return cantidad.substr(0, cantidad.indexOf(".")+3);
}  

//esta función realiza una conversión del valor pasado como parámetro para hacerlo legible como una cantidad. El valor devuelto es una cifra para ser legible facilmente
function getCifraLegible(value){
	var valor = new String(value);
	if(valor.indexOf(".") != -1){
		var inicio = valor.indexOf(".");
		var cifras_restantes = valor.length-(inicio+1);
		if(cifras_restantes == 1) valor += "0";
	}
	else valor += ".00";
	var partes_cifra = valor.split(".");
	valor = partes_cifra[0];
	var i = valor.length - 3; 
	while( i > 0){ 
		valor = valor.substring( 0 , i ) +","+ valor.substring(i, valor.length);
		i = i-3; 
	} 
	return valor+"."+partes_cifra[1].substring(0, 2); 
}

function yaExisteElementoEnVector(elemento, vector){
	for(var i=0; i<vector.length; i++){
		if(vector[i] == elemento) return true;
	}
	return false;
}

function agregaElementoAVector(elemento, vector){
	vector[vector.length] = elemento;
}

//esta función recupera el número de días de diferencia de la fecha dada y la fecha actual del sistema
function getDiferenciaDeDiasDeFechaConFechaActual(fecha){
    //console.log("fecha: "+fecha);
    var fecha = new Date(fecha);
    var fecha_actual = new Date();
    return parseInt((fecha_actual - fecha) / (1000 * 60 * 60 * 24)); 
}

//esta función toma como parámetro una fecha desde la db y la devuelve como una cadena con día, mes corto y año
function getFechaLegibleRecortada(fechadb){
	if (fechadb.length == 0) return "";
	//primero separamos la fecha del mes
	var fecha_separada = fechadb.split("-");
	return fecha_separada[2]+" "+getNombreMesCorto(fecha_separada[1])+" "+fecha_separada[0];
}

//esta función recupera el nombre del mes acortado, a partir de un número de mes dado
function getNombreMesCorto(mes){
	var numero_mes = parseFloat(mes);
	switch(numero_mes){
		case 1: return "Ene";
					break;
		case 2: return "Feb";
					break;		
		case 3: return "Mar";
					break;
		case 4: return "Abr";
					break;
		case 5: return "May";
					break;
		case 6: return "Jun";
					break;
		case 7: return "Jul";
					break;
		case 8: return "Ago";
					break;
		case 9: return "Sep";
					break;
		case 10: return "Oct";
					break;
		case 11: return "Nov";
					break;
		case 12: return "Dic";
					break;														
	}
}

//esta función devuelve el top absoluta de una imagen
function topAbsoluto(objeto){
    var posicion = $(objeto).offset();
    return posicion.top;
}

//esta función devuelve el left absoluta de una imagen
function leftAbsoluto(objeto){
    var posicion = $(objeto).offset();
    return posicion.left;
}

//esta función devuelve TRUE si la dirección ip dada es de alguna de las ips administrativas, en otro caso devuelve 0. También considera la dirección del gateway definida en la variable gateway
function esUnaIpInterna(direccion_ip) {
    //si la dirección es la dirección del gateway entonces es una petición remota
    if (direccion_ip == gateway) return false;
    return true;
}

//esta función muestra la ayuda de un campo
function ocultaAyudaCampo(id){
    var elemento = document.getElementById(id);
    $(elemento).hide("drop", 200);
}

//esta función muestra la ayuda de un campo
function muestraAyudaCampo(id, texto){
    var elemento = document.getElementById(id);
    if(!elemento) return;
    elemento.innerHTML = texto;
    $(elemento).show("fold", 100);
}

function muestraCapaOculta(id, check) {
    var elemento = document.getElementById(id);
    if (check.checked) {
	$(elemento).show("drop", 100);
    }else $(elemento).hide("drop", 100);
}

function stringToSeconds(tiempo){
    var sep1 = tiempo.indexOf(":");
    var sep2 = tiempo.lastIndexOf(":");
    var hor = tiempo.substr(0, sep1);
    var min = tiempo.substr(sep1 + 1, sep2 - sep1 - 1);
    var sec = tiempo.substr(sep2 + 1);
    return (Number(sec) + (Number(min) * 60) + (Number(hor) * 3600));
}

//esta función devuelve true si se encuentra el elemento buscado dentro del arreglo dado como parámetro
function existeElementoEnVector(valor, vector){
    for(var i=0; i<vector.length; i++) if (vector[i] == valor) return true;
    return false;
}

//esta función recupera la posicion del elemento dentro del vector
function getPosicionDeElementoEnVector(valor, vector){
    for(var i=0; i<vector.length; i++) if (vector[i] == valor) return i;
    return -1;
}

//esta función campara dos horas de la forma 00:00 y devuelve 1 si la primera es mayor que la segunda. 0 si es menor que la segunda y -1 si son iguales
function esMayorLaHoraUnaQueLaHoraDos(hora_uno, hora_dos) {
    //realizamos las validaciones sobre el tiempo, por ejemplo, que la hora final no sea menos que la hora inicial
    //primero partimos la hora inicial y la hora final
    var hora_inicial_separada = hora_uno.split(":");
    var hora_final_separada = hora_dos.split(":");
        
    //primero comparamos las horas
    if (hora_inicial_separada[0] == hora_final_separada[0]){
	//la comparación de minutos solo se hace si las horas son iguales.
	if (hora_inicial_separada[1] == hora_final_separada[1]) return -1;
	else if (hora_inicial_separada[1] > hora_final_separada[1]) return 1;
	return 0;
    }if (hora_inicial_separada[0] > hora_final_separada[0]) return 1;
    else return 0;
}

//esta función es usada para validar campos con solo flotantes
function validarFlotante(campo)
{         
   if(event.keyCode != 46 && (event.keyCode < 48  || event.keyCode > 57 ))
   {
      event.returnValue = false;
   }      
   if(event.keyCode == 46)
   {
      var punto = campo.value.indexOf(".",0)
      if (punto != -1)
      {
         event.returnValue = false;
      }
   }
}
var Forma = function(componente_padre){
    
    //VARIABLES
    this.info = "";
    this.setInfo = function(info){
        this.info = info
    }
    
    //Contenedor de la forma
    this.contenedor = null;
    
    //Nombre de instancia de la forma para referirse a ella desde ámbito global
    this.nombre_instancia = "";
    
    //Componente padre de la forma actual
    this.componente_padre = (componente_padre)? componente_padre:null;
    
    //SETTERS Y GETTERS
    
    //Setea el contenedor para la forma. Debe ser un elemento div
    this.setContenedor = function(contenedor){
        this.contenedor = contenedor;
    }
    
    //Recupera el contenedor para la forma. Debe ser un elemento div o null
    this.getContenedor = function(){
        return this.contenedor;
    }
    
    //Setea el nombre de la instancia para la forma. Para referirse a ella en el ámbito global
    this.setNombreInstancia = function(nombre){
        this.nombre_instancia = nombre;
    }
    
    //Recupera el nombre de la instancia de la forma.
    this.getNombreInstancia = function(){
        this.nombre_instancia;
    }
    
    //Setea el componente padre de la forma actual, debería ser un elemento de clase Forma
    this.setComponentePadre = function(padre){
        this.componente_padre = padre;
    }
    
    this.getComponentePadre = function(){
        return this.componente_padre;
    }
    
    this.setComponente = function(contendor, nombre_instancia){
        this.contenedor = contendor;
        this.nombre_instancia = nombre_instancia;
    }
    
    this.muestraComponenteGenerico = function(){
        var contenido = "<div class='h1' id='"+this.nombre_instancia+"_titulo_principal'></div>";
        contenido += "<div class='h2' id='"+this.nombre_instancia+"_subtitulo_principal'></div>";
        contenido += "<div class='contenido_principal' id='"+this.nombre_instancia+"_contenido_principal'></div>";
        
        this.contenedor.innerHTML = contenido;
    }
    
    this.muestraComponenteGenericoPrincipal = function(){
        var contenido = "<div class='card-panel'><div class='h4' id='"+this.nombre_instancia+"_titulo_principal'></div>";
        
        contenido += "<div class='h5' id='"+this.nombre_instancia+"_subtitulo_principal'></div>";
        contenido += "<div class='contenido_principal' id='"+this.nombre_instancia+"_contenido_principal'></div></div>";
        
        this.contenedor.innerHTML = contenido;
    }
    
    this.muestraComponenteGenericoConSoloContenido = function(){
        var contenido = "<div class='contenido_principal' id='"+this.nombre_instancia+"_contenido_principal'></div>";
        
        this.contenedor.innerHTML = contenido;
    }
    
    this.getDivTitulo = function(){
        return document.getElementById(this.nombre_instancia+"_titulo_principal");   
    }
    
    this.getDivSubtitulo = function(){
        return document.getElementById(this.nombre_instancia+"_subtitulo_principal");   
    }
    
    this.getDivContenido = function(){
        return document.getElementById(this.nombre_instancia+"_contenido_principal");   
    }
    
    //ADMINISTRACION DE LA INFORMACION DE LA FORMA
    this.editaInfo = function(){
        //console.log("Debes implementar esta funcion en tu clase superior");
    }
    
    //CONTROLADOR DE NAVEGACION
    this.controlador_navegacion = null;
    
    this.getControladorNavegacion = function(){
        if(!this.controlador_navegacion){
            this.controlador_navegacion = new ControladorNavegacion();
            this.controlador_navegacion.setControladorNavegacion(this.contenedor, this.nombre_instancia+".getControladorNavegacion()");
        }
        return this.controlador_navegacion;
    }
    
    this.setControladorNavegacion = function(controlador){
        this.controlador_navegacion = controlador;
    }
    
    this.muestraControladorNavegacion = function(){
        this.getControladorNavegacion().setControladorNavegacion(this.contenedor, null, null, this.nombre_instancia+".muestraComponente()", this.nombre_instancia+".getControladorNavegacion()");
        this.getControladorNavegacion().muestraControladorNavegacion();
    }
    
    this.showControladorNavegacion = function(texto, funcion){
        this.getControladorNavegacion().push(this, funcion, texto);
        this.getControladorNavegacion().muestraControladorNavegacion();
    }
    
    //COMPONENTE MODAL
    this.contenedor_modal = null;
    this.getContenedorModal = function(){
        if(!this.contenedor_modal) this.contenedor_modal = new ComponenteModal();
        return this.contenedor_modal;
    }
    
    this.muestraContenedorModal = function(tipo, alto, ancho){
        this.getContenedorModal().setComponente(this.nombre_instancia+".getContenedorModal()");
        
        if(tipo == "M") this.getContenedorModal().muestraComponente();
        else if(tipo == "S") this.getContenedorModal().muestraComponenteSmall();
        else if(tipo == "VS") this.getContenedorModal().muestraComponenteMuyPequenio();
        else if(tipo == "G") this.getContenedorModal().muestraComponenteGrande();
        else if(tipo == "P"){
            this.getContenedorModal().setDimensiones(alto, ancho);
            this.getContenedorModal().muestraComponentePersonalizado();
        }
        
        return this.getContenedorModal().getContenedorDeContenido();
    }
}
var Catalogo = function(){
    
    this.buscador = null;
    this.getBuscador = function(){
        if(!this.buscador) this.buscador = new Buscador();
        return this.buscador;
    }
    
    this.setIdElementoParaResaltar = function(idelemento){
        this.getListaSeleccionable().setIdElementoParaResaltar(idelemento);
    }
    
    this.forma_captura = null;
    this.setFormaCaptura = function(forma){
        this.forma_captura = forma;
    }
    
    this.getFormaCaptura = function(){
        return this.forma_captura;
    }
    
    this.lista_seleccionable = null;
    this.getListaSeleccionable = function() {
        if (!this.lista_seleccionable) {
            this.lista_seleccionable = new ListaSeleccionable();
            this.lista_seleccionable.setNombreInstancia(this.nombre_instancia + ".lista_seleccionable");
        }
        return this.lista_seleccionable;
    }
    
    this.mostrando_forma = 0;
    
    this.mostraOpcionesDeImpresion = function(opcion){
        var contenido = "<br/><div id='"+this.nombre_instancia+"_zona_botones_impresion'>";
        contenido += "<div class='row'>";
        contenido += "<div class='col l6'>";

        contenido += "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".imprimeLista(2)' style='margin:0 auto 0 auto;'>PDF</a>";

        contenido += "</div>";
        contenido += "<div class='col l6'>";

        contenido += "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".imprimeLista(1)' style='margin:0 auto 0 auto;'>CSV</a>";

        contenido += "</div>";
        contenido += "</div>";
        contenido += "</div>";
        
        this.contenedor.innerHTML += contenido;
    }
    
    this.contenedor_forma_flotante = 0;
    this.setContenedorFormaFlotante = function(valor){
        this.contenedor_forma_flotante = valor;
    }
    
    this.tamanio_componente_modal = "m";
    this.setTamanioComponenteModal = function(tamanio){
        this.tamanio_componente_modal = tamanio;
    }
    
    this.componente_padre = null;
    this.setComponentePadre = function(componente){
        this.componente_padre = componente;
    }
    
    this.selector_opcion = null;
    this.getSelectorDeOpcion = function(){
        if(!this.selector_opcion) this.selector_opcion = new SelectorOpcion();
        return this.selector_opcion;
    }
    
    this.funcion_elemento_seleccionado = null;
    this.setFuncionElementoSeleccionado = function(funcion){
        this.funcion_elemento_seleccionado = funcion;
    }
    
    this.setDatosVisualizacionLista = function(encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada){
        this.lista_seleccionable.setEncabezados(encabezados);
        this.lista_seleccionable.setMedidas(medidas);
        this.lista_seleccionable.setAlineaciones(alineaciones);

        this.lista_seleccionable.setFuncionEliminaElemento(funcion_elimina);
        this.lista_seleccionable.setOpcionDetalle(opcion_detalle);
        this.lista_seleccionable.setFuncionElementoSeleccionado(funcion_elemento_seleccionado);

        this.lista_seleccionable.setFuncionTablaMostrada(funcion_tabla_mostrada);
    }
    
    this.titulo_catalogo = "- Catalogo Generico";
    this.setTituloCatalogo = function(titulo){
        this.titulo_catalogo = titulo;
        if(document.getElementById(this.nombre_instancia+"_titulo_catalogo")) document.getElementById(this.nombre_instancia+"_titulo_catalogo").innerHTML = this.titulo_catalogo;
    }
    
    this.texto_agregar_elemento = "Agrega un nuevo elemento";
    this.setTextoAgregarElemento = function(texto){
        this.texto_agregar_elemento = texto;
    }
    
    this.muestra_opciones_laterales = true;
    this.setMuestraOpcionesLaterales = function(valor){
        this.muestra_opciones_laterales = valor;
    }
    
    this.tabla_elementos_busqueda = true;
    this.setTipoTablaElementos = function(tipo){
        this.tabla_elementos_busqueda = tipo;
    }
    
    this.size_contenedor_flotante = "";
    this.setSizeContenedorFlotante = function(size){
        this.size_contenedor_flotante = size;
    }
    
    this.muestraEstructuraCatalogo = function(){
        this.muestraComponenteGenerico();
        
        this.getTituloPrincipal().innerHTML = "<table><tr><td width='60%'><h5>"+this.titulo_catalogo+"</h5></td><td width='10%'></td><td width='30%'><div id='"+this.nombre_instancia+"_zona_buscador'></div></td></tr></table><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>";
        
        this.getBuscador().setComponenteBuscador(document.getElementById(this.nombre_instancia+"_zona_buscador"), this.nombre_instancia+".getBuscador()", this.nombre_instancia+".muestraElementosDelCatalogo()");
        this.getBuscador().muestraCampoBuscadorInstantGrande();
    }
    
    this.muestraEstructuraCatalogoPrincipal = function(){
        this.muestraComponenteGenericoPrincipal();
        
        this.getTituloPrincipal().innerHTML = "<table width='98%'><tr><td width='100%' align='center'><div class='titulo_componente'>"+this.titulo_catalogo+"</div><br/><div id='"+this.nombre_instancia+"_zona_selector'></div><br/><div id='"+this.nombre_instancia+"_zona_buscador'></div></td></tr></table><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>";
        
        this.getBuscador().setComponenteBuscador(document.getElementById(this.nombre_instancia+"_zona_buscador"), this.nombre_instancia+".getBuscador()", this.nombre_instancia+".muestraElementosDelCatalogo()");
        this.getBuscador().muestraCampoBuscadorInstantGrande();
    }
    
    this.muestraEstructuraCatalogoPrincipalConOpcionAgregarNuevo = function(){
        this.mostrando_forma = 0;
        //this.muestraComponenteGenericoPrincipal();
        
        this.contenedor.innerHTML = "<label class='valign-wrapper titulo_forma'>"+this.titulo_catalogo+"</label><hr class='separador'/><div id='"+this.nombre_instancia+"_zona_selector'></div><div id='"+this.nombre_instancia+"_zona_buscador' class='left-align'></div><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>";

        //agregamos el buscador
        this.getBuscador().setComponenteBuscador(document.getElementById(this.nombre_instancia+"_zona_buscador"), this.nombre_instancia+".getBuscador()", this.nombre_instancia+".muestraElementosDelCatalogo()");
        this.getBuscador().muestraCampoBuscadorInstantGrande();
        
        //agregamos el selector
        this.getSelectorDeOpcion().setComponente(document.getElementById(this.nombre_instancia+"_zona_selector"), this.nombre_instancia+".getSelectorDeOpcion()");
        this.getSelectorDeOpcion().setTextoOpcionHover(this.texto_agregar_elemento);
        
        if(this.contenedor_forma_flotante == 0)
            this.getSelectorDeOpcion().setFuncionAgregar(this.nombre_instancia+".nuevoElemento()");
        else if(this.contenedor_forma_flotante == 1)
            this.getSelectorDeOpcion().setFuncionAgregar(this.nombre_instancia+".nuevoElementoFlotante()");
        
        this.getSelectorDeOpcion().muestraComponente();
    }
    
    this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo = function(){
        this.mostrando_forma = 0;
        //this.muestraComponenteGenericoPrincipal();
        
        this.contenedor.innerHTML = "<label class='valign-wrapper titulo_forma'>"+this.titulo_catalogo+"</label><hr class='separador'/><div id='"+this.nombre_instancia+"_zona_buscador'></div><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>";

        //agregamos el buscador
        this.getBuscador().setComponenteBuscador(document.getElementById(this.nombre_instancia+"_zona_buscador"), this.nombre_instancia+".getBuscador()", this.nombre_instancia+".muestraElementosDelCatalogo()");
        this.getBuscador().muestraCampoBuscadorInstantGrande();
        
        if(this.contenedor_forma_flotante == 0)
            this.getSelectorDeOpcion().setFuncionAgregar(this.nombre_instancia+".nuevoElemento()");
        else if(this.contenedor_forma_flotante == 1)
            this.getSelectorDeOpcion().setFuncionAgregar(this.nombre_instancia+".nuevoElementoFlotante()");
    }
    
    this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevoConBotonBuscar = function(){
        this.mostrando_forma = 0;
        
        var contenido = "<label class='valign-wrapper titulo_forma'>"+this.titulo_catalogo+"</label><hr class='separador'/>";
        
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s10 m10 l10 left-align">';
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_buscador'></div>";
        
        contenido += '</div>';
        contenido += '<div class="col s2 m2 l2 left-align">';
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_boton_buscar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".buscar()' style='margin:0 auto 0 auto;'>Buscar</a></div>"
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>";
        
        this.contenedor.innerHTML = contenido;

        //agregamos el buscador
        this.getBuscador().setComponenteBuscador(document.getElementById(this.nombre_instancia+"_zona_buscador"), this.nombre_instancia+".getBuscador()", null);
        this.getBuscador().muestraCampoBuscadorGrande();
        
        if(this.contenedor_forma_flotante == 0)
            this.getSelectorDeOpcion().setFuncionAgregar(this.nombre_instancia+".nuevoElemento()");
        else if(this.contenedor_forma_flotante == 1)
            this.getSelectorDeOpcion().setFuncionAgregar(this.nombre_instancia+".nuevoElementoFlotante()");
    }
    
    this.muestraEstructuraCatalogoPrincipalConOpcionAgregarNuevoSinBuscador = function(){
        /*this.mostrando_forma = 0;
        this.muestraComponenteGenericoPrincipal();
        
        this.getDivTitulo().innerHTML = "<table width='98%'><tr><td width='100%' align='center'><h4>"+this.titulo_catalogo+"</h4><br/><div id='"+this.nombre_instancia+"_zona_selector'></div><br/><div id='"+this.nombre_instancia+"_zona_buscador'></div></td></tr></table><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>";*/
        this.mostrando_forma = 0;
        //this.muestraComponenteGenericoPrincipal();
        
        this.contenedor.innerHTML = "<label class='valign-wrapper titulo_forma'>"+this.titulo_catalogo+"</label><hr class='separador'/><div id='"+this.nombre_instancia+"_zona_selector'></div><div id='"+this.nombre_instancia+"_zona_buscador' class='left-align'></div><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>";

        //agregamos el selector
        this.getSelectorDeOpcion().setComponente(document.getElementById(this.nombre_instancia+"_zona_selector"), this.nombre_instancia+".getSelectorDeOpcion()");
        this.getSelectorDeOpcion().setTextoOpcionHover(this.texto_agregar_elemento);
        
        if(this.contenedor_forma_flotante == 0)
            this.getSelectorDeOpcion().setFuncionAgregar(this.nombre_instancia+".nuevoElemento()");
        else if(this.contenedor_forma_flotante == 1)
            this.getSelectorDeOpcion().setFuncionAgregar(this.nombre_instancia+".nuevoElementoFlotante()");
        
        this.getSelectorDeOpcion().muestraComponente();
    }
    
    this.muestraEstructuraCatalogoPrincipalSinBuscador = function(){
        this.muestraComponenteGenericoPrincipal();
        
        this.getTituloPrincipal().innerHTML = "<table width='98%'><tr><td width='100%' align='center'><div class='titulo_componente'>"+this.titulo_catalogo+"</div><br/><div id='"+this.nombre_instancia+"_zona_selector'></div><br/><div id='"+this.nombre_instancia+"_zona_buscador'></div></td></tr></table><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>"; 
        
        this.setTipoTablaElementos(false);
    }
    
    this.getZonaDeSelector = function(){
        return document.getElementById(this.nombre_instancia+"_zona_selector");
    }
    
    this.muestraEstructuraCatalogoPrincipalConZonaInferior = function(){
        this.muestraComponenteGenericoPrincipal();
        
        this.getTituloPrincipal().innerHTML = "<table width='98%'><tr><td width='100%' align='center'><br/><div class='titulo_componente'>"+this.titulo_catalogo+"</div><br/><div id='"+this.nombre_instancia+"_zona_buscador'></div></td></tr></table><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div><div id='"+this.nombre_instancia+"_zona_botones_catalogo'></div>";
        
        this.getBuscador().setComponenteBuscador(document.getElementById(this.nombre_instancia+"_zona_buscador"), this.nombre_instancia+".getBuscador()", this.nombre_instancia+".muestraElementosDelCatalogo()");
        this.getBuscador().muestraCampoBuscadorInstantGrande();
    }
    
    this.muestraEstructuraCatalogoPrincipalConBarraOpciones = function(){
        this.mostrando_forma = 0;
        //this.muestraComponenteGenericoPrincipal();
        
        this.contenedor.innerHTML = "<label class='valign-wrapper titulo_forma'>"+this.titulo_catalogo+"</label><hr class='separador'/><div id='"+this.nombre_instancia+"_zona_secundaria'></div><div id='"+this.nombre_instancia+"_zona_selector'></div><div id='"+this.nombre_instancia+"_zona_buscador' class='left-align'></div><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>";

        //agregamos el buscador
        this.getBuscador().setComponenteBuscador(document.getElementById(this.nombre_instancia+"_zona_buscador"), this.nombre_instancia+".getBuscador()", this.nombre_instancia+".muestraElementosDelCatalogo()");
        this.getBuscador().muestraCampoBuscadorInstantGrande();
        
        //agregamos el selector
        this.getSelectorDeOpcion().setComponente(document.getElementById(this.nombre_instancia+"_zona_selector"), this.nombre_instancia+".getSelectorDeOpcion()");
        this.getSelectorDeOpcion().setTextoOpcionHover(this.texto_agregar_elemento);
        
        if(this.contenedor_forma_flotante == 0)
            this.getSelectorDeOpcion().setFuncionAgregar(this.nombre_instancia+".nuevoElemento()");
        else if(this.contenedor_forma_flotante == 1)
            this.getSelectorDeOpcion().setFuncionAgregar(this.nombre_instancia+".nuevoElementoFlotante()");
        
        this.getSelectorDeOpcion().muestraComponente();
    }
    
    this.muestraEstructuraCatalogoPrincipalConBarraOpcionesSinOpcionNuevo = function(){
        this.mostrando_forma = 0;
        
        this.contenedor.innerHTML = "<label class='valign-wrapper titulo_forma'>"+this.titulo_catalogo+"</label><hr class='separador'/><div id='"+this.nombre_instancia+"_zona_secundaria'></div><div id='"+this.nombre_instancia+"_zona_buscador' class='left-align'></div><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>";

        //agregamos el buscador
        this.getBuscador().setComponenteBuscador(document.getElementById(this.nombre_instancia+"_zona_buscador"), this.nombre_instancia+".getBuscador()", this.nombre_instancia+".muestraElementosDelCatalogo()");
        this.getBuscador().muestraCampoBuscadorInstantGrande();
    }
    
    this.getZonaBuscador = function(){
        return document.getElementById(this.nombre_instancia+"_zona_buscador");
    }
    
    this.getZonaOpciones = function(){
        return document.getElementById(this.nombre_instancia+"_zona_opciones");
    }
    
    this.getZonaBotonesCatalogo = function(){
        return document.getElementById(this.nombre_instancia+"_zona_botones_catalogo");
    }
    
    this.getZonaElementosDelCatalogo = function(){
        return document.getElementById(this.nombre_instancia+"_zona_elementos_catalogo");
    }
    
    this.getZonaSecundaria = function(){
        return document.getElementById(this.nombre_instancia+"_zona_secundaria");
    }
    
    this.muestraEstructuraCatalogoPrincipalConZonaSecundariaSinTitulo = function(){
        this.muestraComponenteGenericoPrincipal();
        
        this.getTituloPrincipal().innerHTML = "<div id='"+this.nombre_instancia+"_zona_secundaria'></div><br/><div id='"+this.nombre_instancia+"_zona_buscador'></div><br/><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>";
        
        this.getBuscador().setComponenteBuscador(document.getElementById(this.nombre_instancia+"_zona_buscador"), this.nombre_instancia+".getBuscador()", this.nombre_instancia+".muestraElementosDelCatalogo()");
        this.getBuscador().muestraCampoBuscadorInstantGrande();
    }
    
    this.muestraEstructuraCatalogoPrincipalConZonaSecundariaSinTituloConZonaBotones = function(){
        this.muestraComponenteGenericoPrincipal();
        
        this.getTituloPrincipal().innerHTML = "<div id='"+this.nombre_instancia+"_zona_secundaria'></div><br/><div id='"+this.nombre_instancia+"_zona_buscador'></div><br/><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div><br/><div id='"+this.nombre_instancia+"_zona_botones'></div>";
        
        this.getBuscador().setComponenteBuscador(document.getElementById(this.nombre_instancia+"_zona_buscador"), this.nombre_instancia+".getBuscador()", this.nombre_instancia+".muestraElementosDelCatalogo()");
        this.getBuscador().muestraCampoBuscadorInstantGrande();
    }
    
    this.getZonaDeBotones = function(){
        return document.getElementById(this.nombre_instancia+"_zona_botones");
    }
    
    this.muestraEstructuraCatalogoPrincipalConZonaSecundaria = function(){
        this.muestraComponenteGenericoPrincipal();
        
        this.getTituloPrincipal().innerHTML = "<div id='"+this.nombre_instancia+"_zona_secundaria'></div><table><tr><td width='60%'><h5 id='"+this.nombre_instancia+"_titulo_catalogo'>"+this.titulo_catalogo+"</h5></td><td width='10%'></td><td width='30%'><div id='"+this.nombre_instancia+"_zona_buscador'></div></td></tr></table><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>";
        
        this.getBuscador().setComponenteBuscador(document.getElementById(this.nombre_instancia+"_zona_buscador"), this.nombre_instancia+".getBuscador()", this.nombre_instancia+".muestraElementosDelCatalogo()");
        this.getBuscador().muestraCampoBuscadorInstantGrande();
    }
    
    this.muestraEstructuraCatalogoConSelector = function(){
        this.muestraComponenteGenerico();
        
        this.getTituloPrincipal().innerHTML = "<div id='"+this.nombre_instancia+"_zona_selector'></div><div id='"+this.nombre_instancia+"_zona_elementos_catalogo'></div>";
    }
    
    this.muestraElementosDelCatalogo = function(){
       this.getListaSeleccionable().setContenedor(document.getElementById(this.nombre_instancia+"_zona_elementos_catalogo"));
        this.getListaSeleccionable().muestraComponente();
        if(this.tabla_elementos_busqueda) this.getListaSeleccionable().muestraTablaElementosConBusqueda(this.getBuscador().getTerminoBusqueda());
        else this.getListaSeleccionable().muestraTablaElementos();
    }
    
    this.opcionListaSeleccionada = function(){
        this.muestraControladorNavegacion();
        
        this.getFormaCaptura().setComponente(this.getControladorNavegacion().getContenedorDeContenido(), this.nombre_instancia+".getFormaCaptura()");
        this.getFormaCaptura().setInfo(this.getListaSeleccionable().getDatosDeElementoSeleccionado());
        this.getFormaCaptura().editaForma();
    }
    
    this.opcionListaSeleccionadaFlotante = function(){
        this.mostrando_forma = 1;
        this.getContenedorModal().setComponente(this.nombre_instancia+".getContenedorModal()");
        
        if(this.tamanio_componente_modal = "s") this.getContenedorModal().muestraComponenteSmall();
        else this.getContenedorModal().muestraComponente();
        
        this.getFormaCaptura().setComponente(this.getContenedorModal().getZonaContenido(), this.nombre_instancia+".getFormaCaptura()");
        this.getFormaCaptura().muestraComponente();
    }
    
    this.nuevoElemento = function(){
        this.mostrando_forma = 1;
        
        this.showControladorNavegacion("Regresar", this.nombre_instancia+".muestraComponente()");
        this.getFormaCaptura().setComponente(this.getControladorNavegacion().getContenedorDeContenido(), this.nombre_instancia+".getFormaCaptura()");
        this.getFormaCaptura().muestraComponente();
        this.getFormaCaptura().setControladorNavegacion(this.getControladorNavegacion());
        this.getListaSeleccionable().setPosicionElementoSeleccionado(-1);
    }
    
    this.nuevoElementoFlotante = function(){
        this.mostrando_forma = 1;
        this.getContenedorModal().setComponente(this.nombre_instancia+".getContenedorModal()");
        
        if(this.tamanio_componente_modal = "s") this.getContenedorModal().muestraComponenteSmall();
        else this.getContenedorModal().muestraComponente();
        
        this.getFormaCaptura().setComponente(this.getContenedorModal().getContenedorDeContenido(), this.nombre_instancia+".getFormaCaptura()");
        this.getFormaCaptura().muestraComponente();
        this.getListaSeleccionable().setPosicionElementoSeleccionado(-1);
    }
    
    this.deshabilitaOpcionBorrar = function(){
        this.getListaSeleccionable().setOpcionBorrar(0);
        this.muestraElementosDelCatalogo();
    }
    
    this.getElementoSeleccionado = function(){
        return this.getListaSeleccionable().getDatosDeElementoSeleccionado();
    }
    
    this.getTituloCatalogo = function(){
        return document.getElementById(this.nombre_instancia+"_titulo_catalogo");
    }
    
    this.respuestaEliminado = function(respuesta){
        if(respuesta == 1){
            alert("Elemento eliminado");
            this.getListaSeleccionable().quitaElementoDeListaPorId(this.getListaSeleccionable().getIdElementoEliminado());
        }else alert("No se pudo eliminar el elemento, reportalo a informatica");
    }
    
    //esta función recupera los datos del elemento seleccionado
    this.getDatosDeElementoSeleccionadoDeCatalogo = function(){
        try{
            return this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        }catch(error){
            return null;
        }
    }
    
    this.soloSeleccion = 0;
    this.setSoloSeleccion = function(valor){
        this.soloSeleccion = valor;
    }
    
    //se sobreescribe esta función para realizar la búsqueda
    this.buscar = function(){
        var terminoBusqueda = this.getBuscador().getTerminoBusqueda();
        if(!terminoBusqueda) return;
        
        //realizamos la búsqueda sobre los elementos para mostrar unicamente aquellos que cumplen
        this.muestraElementosDelCatalogo();
    }
    
    this.funcionElementoSeleccionado = null;
    this.setFuncionElementoSeleccionado = function(funcion){
        this.funcionElementoSeleccionado = funcion;
    }
    
    //esta función está por defecto para avisar a la función dada cuando se seleccione una opción, se puede sobre escribir para un comportamiento diferente
    this.elementoSeleccionado = function(){
        if(this.funcionElementoSeleccionado) setTimeout(this.funcionElementoSeleccionado, 0);
    }
    
    this.getCadenaParaEnvio = function(vector){
        var cadena = "";
        for(var ii=0; ii<vector.length; ii++){
            if(ii == vector.length-1) cadena += vector[ii];
            else cadena += vector[ii]+"|";
        }
        return cadena;
    }
    
    //esta funcion recupera toda la informacion necesaria para realizar una impresión de los elementos visibles del catálogo, considerando las listas como tal, es decir, sus encabezados, sus medidas, sus alineaciones y los elementos visibles
    this.imprimeLista = function(opcion){
        //opcion 1 PDF y opcion 2 CSV
        
        //primero preparamos la informacion para enviarla, solo la info visible
        var encabezados = this.getListaSeleccionable().encabezados;
        
        //se toma la longitud del encabezado para el envio de la informacion, se truncan los elementos que no son columnas visibles
        var elementosParaEnvio = "";
        var elementosVisibles = this.getListaSeleccionable().getElementosVisiblesEnLista();
        
        var elemento;
        for(var i=0; i<elementosVisibles.length; i++){
            elementosParaEnvio += elementosVisibles[i]+"^";
        }
        
        var encabezadosParaEnvio = this.getCadenaParaEnvio(encabezados);
        var medidasParaEnvio = this.getCadenaParaEnvio(this.getListaSeleccionable().medidas);
        var alineacionParaEnvio = this.getCadenaParaEnvio(this.getListaSeleccionable().alineaciones);
        
        var paraEnvio = opcion+"<*>"+encabezadosParaEnvio+"<*>"+medidasParaEnvio+"<*>"+alineacionParaEnvio+"<*>"+elementosParaEnvio
        //console.log("elementos para envio: "+paraEnvio);
        //paraEnvio = 
        
        var parametros = { "datos" : paraEnvio };
        $.ajax({
            data:  parametros, //datos que se envian a traves de ajax
            url:   'ms/generadorDeReportes.php', //archivo que recibe la peticion
            type:  'post', //método de envio
            beforeSend: function () {},
            success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
                //console.log("datos en server para ser mostrados");
            
                //var contenido = "<iframe src='ms/generadorDeReportes.php' width='"+($( window ).width()*0.90)+"px;' height='"+($( window ).height()*0.90)+"px;' id='"+this.nombre_instancia+"_iframe_pdf'></iframe>";
                window.open('ms/generadorDeReportes.php', '_blank');

                //$.colorbox({html:contenido, transition:"elastic"});
            }
        });
        
        //opcion, encabezados, medidas, alineacion, elemento
        //getResultadoAjax("opcion=40&datos="+paraEnvio, this.nombre_instancia+".procesaRespuestaParaMostrarReporte", null);
    }
    
    this.procesaRespuestaParaMostrarReporte = function(respuesta){
        //console.log("respuesta reporte: "+respuesta);
        if(respuesta == 1){
            //console.log("datos en server para ser mostrados");
            
            var contenido = "<iframe src='ms/generadorDeReportes.php' width='"+($( window ).width()*0.90)+"px;' height='"+($( window ).height()*0.90)+"px;' id='"+this.nombre_instancia+"_iframe_pdf'></iframe>";
        
            $.colorbox({html:contenido, transition:"elastic"});
        }
    }
}
var Orbe = function(){
    
    this.login = null;
    this.getLogin = function(){
        if(!this.login){
            this.login = new Login();
            this.login.setComponente(null, this.nombre_instancia+".login");
            this.login.setFuncionSiHaySesionActiva(this.nombre_instancia+".muestraComponentePrincipal()");
        }
        return this.login;
    }
    
    this.opcion_seleccionada = null;
    this.clase_opcion_seleccionada = null;
    this.catalogoSeleccionado = null;
    
    this.muestraComponente = function(){
        this.getLogin().haySesionActiva();
    }
    
    this.muestraComponentePrincipal = function(){
        this.contenedor = document.getElementById("contenedor_principal");
        
        var contenido = '<div class="row center-align">';
        contenido += '<div id="menu_principal" class="col s0 m2 l2 center-align menu_principal" style="height:'+(getAlto())+'px; background-color:white;"></div>';
        contenido += '<div id="zona_principal" class="col s12 m10 l10" style="padding:30px;"></div>';
        contenido += '</div>';
        
        this.contenedor.innerHTML = contenido;
        
        this.muestraMenuPrincipal();
        
        if(this.login.getPerfil() == 1) document.getElementById("opcionUno").click();
        else if(this.login.getPerfil() == 2) document.getElementById("opcion_dos").click();
        else if(this.login.getPerfil() == 3) document.getElementById("opcion_tres").click();
        else if(this.login.getPerfil() == 4) document.getElementById("opcionUno").click();
        else if(this.login.getPerfil() == 5) document.getElementById("opcionUno").click();
    }
    
    this.muestraMenuPrincipal = function(){
        var contenido = "<br/><img src='images/logo2.jpg' class='responsive-img'/><br/><br/><br/>";
        
        contenido += "<table align='center' width='96%' cellpadding='6' cellspacing='6' border='0'>";
        
        if(this.login.getPerfil() == 1){
            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraGastosClientes();' id='opcionUno'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='gastos'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Gastos Clientes</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";
            
            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraPagosRecibidos();'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='xml'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Pagos</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";

            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraFacturasPendientesDeClientes();'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='xml'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Facturas Pendientes</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";

            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraCatalogoPromotores();'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='usuarios'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Promotores</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";

            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraGastosInternos();'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='gastos'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Gastos Internos</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";

            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraClientes();'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='clientes'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Clientes</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";

            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraEmpresas();'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='empresas'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Empresas</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";

            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraBancos();'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='bancos'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Bancos</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";

            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraUsuarios();'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='usuarios'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Usuarios</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";   
        }else if(this.login.getPerfil() == 2){//Facturista
            
            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraFacturasPendientesDeClientes();' id='opcion_dos'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='xml'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Facturas Pendientes</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";
        }else if(this.login.getPerfil() == 3){//Promotor
            
            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraCatalogoPromotores();' id='opcion_tres'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='usuarios'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Promotores</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";
        }else if(this.login.getPerfil() == 4){//Operaciones
            
            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraGastosClientes();' id='opcionUno'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='gastos'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Gastos Clientes</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";
            
            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraGastosInternos();'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='gastos'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Gastos Internos</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";

            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraClientes();'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='clientes'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Clientes</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";
            
            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraBancos();'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='bancos'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Bancos</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";
        }else if(this.login.getPerfil() == 5){//Clientes
            
            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraGastosClientes();' id='opcionUno'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='gastos'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Gastos Clientes</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";
        }
        
        contenido += "<tr class='opcion_cuatro' onclick='"+this.nombre_instancia+".cerrarSesion()'>";
        contenido += "<td align='center' valign='top' width='20%'>";
        contenido += "<div class='cerrar_sesion'></div>";
        contenido += "</td>";
        contenido += "<td align='left' valign='top' width='80%'>";
        contenido += "<div>Salir</div>";
        contenido += "</td>";
        contenido += "</tr>";
        
        contenido += "</table>";
        
        document.getElementById("menu_principal").innerHTML = contenido;
    }
    
    this.muestraGastosClientes = function(){
        this.catalogoSeleccionado = new CatalogoGastos();
        this.catalogoSeleccionado.setComponente(document.getElementById("zona_principal"), "orbe.catalogoSeleccionado");
        this.catalogoSeleccionado.muestraComponente();
    }
    
    this.muestraGastosInternos = function(){
        this.catalogoSeleccionado = new CatalogoGastosInternos();
        this.catalogoSeleccionado.setComponente(document.getElementById("zona_principal"), "orbe.catalogoSeleccionado");
        this.catalogoSeleccionado.muestraComponente();
    }
    
    this.muestraPagosRecibidos = function(){
        this.catalogoSeleccionado = new CatalogoComprobantePagos();
        this.catalogoSeleccionado.setComponente(document.getElementById("zona_principal"), "orbe.catalogoSeleccionado");
        this.catalogoSeleccionado.muestraComponente();
    }
    
    this.muestraInicio = function(){
        
    }
    
    //muestra el catalogo de clientes
    this.muestraClientes = function(){
        this.catalogoSeleccionado = new CatalogoClientes();
        this.catalogoSeleccionado.setComponente(document.getElementById("zona_principal"), "orbe.catalogoSeleccionado");
        this.catalogoSeleccionado.muestraComponente();
    }
    
    //muestra las facturas pendientes de los gastos de los clientes
    this.muestraFacturasPendientesDeClientes = function(){
        this.catalogoSeleccionado = new CatalogoFacturasPendientes();
        this.catalogoSeleccionado.setComponente(document.getElementById("zona_principal"), "orbe.catalogoSeleccionado");
        this.catalogoSeleccionado.muestraComponente();
    }
    
    //muestra empresas
    this.muestraEmpresas = function(){
        this.catalogoSeleccionado = new CatalogoEmpresas();
        this.catalogoSeleccionado.setComponente(document.getElementById("zona_principal"), "orbe.catalogoSeleccionado");
        this.catalogoSeleccionado.muestraComponente();
    }
    
    //muestra el catalogo de bancos
    this.muestraBancos = function(){
        this.catalogoSeleccionado = new CatalogoBancos();
        this.catalogoSeleccionado.setComponente(document.getElementById("zona_principal"), "orbe.catalogoSeleccionado");
        this.catalogoSeleccionado.muestraComponente();
    }
    
    //mostramos el catalogo de promotores
    this.muestraCatalogoPromotores = function(){
        this.catalogoSeleccionado = new CatalogoPromotores();
        this.catalogoSeleccionado.setComponente(document.getElementById("zona_principal"), "orbe.catalogoSeleccionado");
        this.catalogoSeleccionado.muestraComponente();
    }
    
    //función para mostrar usuarios del sistema
    this.muestraUsuarios = function(){
        this.catalogoSeleccionado = new CatalogoUsuarios();
        this.catalogoSeleccionado.setComponente(document.getElementById("zona_principal"), "orbe.catalogoSeleccionado");
        this.catalogoSeleccionado.muestraComponente();
    }
    
    this.setOpcionPrincipalSeleccionada = function(opcion){
        if(opcion.className.indexOf("_seleccionada") == -1){
            if(this.opcion_seleccionada) this.opcion_seleccionada.className = this.clase_opcion_seleccionada;
            this.opcion_seleccionada = opcion;
            this.clase_opcion_seleccionada = this.opcion_seleccionada.className;
            opcion.className = "opcion_menu_seleccionada";
        }
    }
    
    this.cerrarSesion = function(){
        getResultadoAjax("opcion=4", this.nombre_instancia+".procesaSesionCerrada", null);   
    }
    
    this.procesaSesionCerrada = function(respuesta){
        if(respuesta == 1){
            this.login = null;
            this.muestraComponente();
        }
    }
}
var Login = function(){
    
    this.funcion_si_hay_sesion_activa = null;
    this.setFuncionSiHaySesionActiva = function(funcion){
        this.funcion_si_hay_sesion_activa = funcion;
    }
    
    this.sesion_activa = null;
    this.haySesionActiva = function(){
        if(!this.sesion_activa){
            //checamos la sesión en el servidor
            getResultadoAjax("opcion=2", this.nombre_instancia+".procesaSesionRecuperada", null);
        }else setTimeout(this.funcion_si_hay_sesion_activa, 0);
    }
    
    this.procesaSesionRecuperada = function(respuesta){
        //console.log("sesion activa: "+respuesta);
        this.sesion_activa = respuesta;
        if(respuesta == 0) this.muestraComponente();
        else{
            this.getContenedorModal().setPermisoParaCerrar(true);
            this.getContenedorModal().setVisibilidadBotonCerrar(true);
            this.getContenedorModal().ocultaComponente();
            setTimeout(this.funcion_si_hay_sesion_activa, 0);
        }
    }
    
    this.muestraComponente = function(){
        this.muestraContenedorModal("P", 600, 300);
        this.getContenedorModal().setPermisoParaCerrar(false);
        this.getContenedorModal().setVisibilidadBotonCerrar(false);
        
        var contenido = "<br/>";
        
        contenido += "<img src='images/logo2.jpg' width='200'/><br/><br/><br/>";
        contenido += "<div class='texto_forma'>Usuario</div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_usuario' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe tu nombre de usuario\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\");'/>";
        
        contenido += "<br/>";
        contenido += "<div id='"+this.nombre_instancia+"_a1' class='ayuda_campos'></div>";
        contenido += "<br/>";
        
        contenido += "<div class='texto_forma'>Password</div>";
        contenido += "<input type='password' class='campo_forma' id='"+this.nombre_instancia+"_password' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe tu password\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\");' onkeypress='"+this.nombre_instancia+".isEnterLaTeclaEnCampoPassword(event)'/>";
        
        contenido += "<br/><br/>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_guardar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()'>Entrar</a></div>";
        
        this.getContenedorModal().getContenedorDeContenido().innerHTML = contenido;
    }
    
    this.isEnterLaTeclaEnCampoPassword = function(evento){
        //recupera el código de la tecla presionada
		var tecla_presionada = (document.all)?evento.keyCode:evento.which;
		//si la tecla presionada es igual a 13, entonces se valida la forma de logueo
		if(tecla_presionada == 13) this.validaForma();
    }
    
    this.validaForma = function(){
        var usuario = document.getElementById(this.nombre_instancia+"_usuario");
        if(usuario.value == "" || usuario.value == " "){
            usuario.focus();
            return;
        }
        
        var password = document.getElementById(this.nombre_instancia+"_password");
        if(password.value == "" || password.value == " "){
            password.focus();
            return;
        }
        
        getResultadoAjax("opcion=3&usuario="+usuario.value+"&password="+password.value, this.nombre_instancia+".procesaSesionRecuperada", document.getElementById(this.nombre_instancia+"_zona_guardar"));
    }
    
    this.getNombreCompletoDeUsuarioLogueado = function(){
        if(!this.sesion_activa) return "";
        var datos = this.sesion_activa.split("|");
        return datos[2];
    }
    
    this.getToken = function(){
        if(!this.sesion_activa) return "";
        var datos = this.sesion_activa.split("|");
        return datos[0];
    }
    
    this.getPerfil = function(){
        if(!this.sesion_activa) return "";
        var datos = this.sesion_activa.split("|");
        return datos[4];
    }
}
// JavaScript Document - Objeto Buscador
var Buscador = function(){
	
	//contenedor del buscador
	this.contenedor_buscador;
	//id html del campo de buscador
	this.id_html_campo;
	//número de búsqueda
	this.numero_busqueda = 0;
	//este texto inicial es lo que se muestra cuando se hace click sobre el cuadro de texto
	this.texto_inicial = "Teclea algo para buscar";
	//esta es la acción que se ejecuta dependiendo del tipo de búsqueda
	this.funcion_busqueda;
	
	//esta variable determina el número de caracteres para iniciar la búsqueda, regresando la expresión en vez de null. Por defecto está en 3
	this.numero_caracteres_minimos = 3
	
	//esta función inicializa las variables del componente buscador
	this.setComponenteBuscador = function(contenedor, id_html, funcion){
		this.contenedor_buscador = contenedor;
		this.id_html_campo =  id_html;
		this.funcion_busqueda = funcion;
	}
	
	//función para setear el número de caracteres mínimos para empezar la búsqueda de información
	this.setNumeroDeCaracteresMinimos = function(numero){
		this.numero_caracteres_minimos = numero;
	}
	
	this.setFuncionBusqueda = function(funcion){
		this.funcion_busqueda = funcion;
	}
	
	//esta función muestra el campo buscador en el contenedor
	this.muestraCampoBuscadorInstant = function(){
		var contenido = "<input type='text' class='buscador' value='"+this.texto_inicial+"' id='"+this.id_html_campo+"' onfocus='limpiaContenido(\""+this.texto_inicial+"\", this)' onkeyup='"+this.funcion_busqueda+"' onblur='llenaContenido(\""+this.texto_inicial+"\", this)'/>";
		this.contenedor_buscador.innerHTML = contenido;
	}
	
	//esta función muestra el campo buscador en el contenedor
	this.muestraCampoBuscadorInstantPequenio = function(){
		var contenido = "<input type='text' class='buscador_pequenio' value='"+this.texto_inicial+"' id='"+this.id_html_campo+"' onfocus='limpiaContenido(\""+this.texto_inicial+"\", this)' onkeyup='"+this.funcion_busqueda+"' onblur='llenaContenido(\""+this.texto_inicial+"\", this)'/>";
		this.contenedor_buscador.innerHTML = contenido;
	}

	//esta función muestra el campo buscador en el contenedor, ocupando la mayor parte de la forma
	this.muestraCampoBuscadorInstantGrande = function(){
		var contenido = "<input type='text' class='buscador_grande' value='"+this.texto_inicial+"' id='"+this.id_html_campo+"' onfocus='limpiaContenido(\""+this.texto_inicial+"\", this)' onkeyup='"+this.funcion_busqueda+"' onblur='llenaContenido(\""+this.texto_inicial+"\", this)'/>";
		this.contenedor_buscador.innerHTML = contenido;
	}
    
    //esta función muestra el campo buscador grande, ocupando la mayoria de la forma. La búsqueda solo inicia cuando se invoca mediante un botón por ejemplo, ya que este no responde a los eventos onkeyup del teclado. Pensado para búsquedas muy tardadas
    this.muestraCampoBuscadorGrande = function(){
        var contenido = "<input type='text' class='buscador_grande' value='"+this.texto_inicial+"' id='"+this.id_html_campo+"' onfocus='limpiaContenido(\""+this.texto_inicial+"\", this)' onblur='llenaContenido(\""+this.texto_inicial+"\", this)'/>";
		this.contenedor_buscador.innerHTML = contenido;
    }
	
	//esta función es usada para limpiar el campo de búsqueda
	this.limpiaBuscador = function(){
		document.getElementById(this.id_html_campo).value = this.texto_inicial;
	}
    
    this.getCampoDeBusqueda = function(){
        return document.getElementById(this.id_html_campo);
    }
	
	//esta función devuelve una expresión para la búsqueda. Si el campo está vacio, devuelve 0
	this.getTerminoBusqueda = function(){
		if(document.getElementById(this.id_html_campo)) var palabras = document.getElementById(this.id_html_campo).value;
		else return 0;
		
		palabras.replace(/ /gi, "");
		if(palabras == this.texto_inicial) return 0;
		if(palabras == "" || palabras == " " || palabras.length < this.numero_caracteres_minimos) return 0;
		return new RegExp(palabras, "i");
	}
}
var ListaSeleccionable = function(){
    
    this.idelemento_seleccionado = 0;
    this.posicion_elemento_seleccionado = -1;
    this.elementos_recuperados = null;
    this.encabezados = null;
    this.medidas = null;
    this.c = null;
    
    this.idelemento_eliminado = 0;
    
    this.tipo_elementos = 1;
    this.setTipoElementos = function(tipo_elementos){
        this.tipo_elementos = tipo_elementos;
    }
    
    //titulo de la lista cuando no haya elementos que mostrar
    this.titulo_lista = "No hay elementos para mostrar";
    
    //posición del elemento seleccionado anteriormente
    this.posicion_elemento_seleccionado_anterior = -1;
    
    //vector que almacena las funciones que se ejecutan cuando se hace click sobre una celda editable
    this.funciones_celdas_editables = null;
    
    //opciones
    this.opcion_borrar = 1;
    this.opcion_detalles = 1;
    this.opcion_impresora = 1;
    
    //elementos que se están visualizando actualmente, y que considera los filtros de búsquedas
    this.elementosVisiblesEnLista = null;
    this.getElementosVisiblesEnLista = function(){
        return this.elementosVisiblesEnLista;
    }
    
    //opciones enviadas para recuperar la lista de elementos
    this.opciones_recuperar_elementos = "";
    
    this.funcion_elimina_elemento = "";
    
    this.funcion_elemento_seleccionado = "";
    
    this.funcion_detalle_elemento_seleccionado = "";
    
    this.funcion_imprime_elemento_seleccionado = "";
    
    this.numero_celda_seleccionada = -1;
    this.celda_seleccionada = null;
    this.campo_seleccionado = null;
    
    this.columnasOrdenadas = null;
    
    this.getColumnasOrdenadas = function(){
        if(!this.columnasOrdenadas){
            this.columnasOrdenadas = new Array();
            
            //init all positions with 0. 0 means no sorted using this index column. 1 already sorted using this index column ascending. 2 already sorted using this index column descending
            for(var i=0; i<this.encabezados.length; i++) this.columnasOrdenadas[i] = 0;
        }
        return this.columnasOrdenadas;
    }
    
    this.elementosRecuperadosEnVector = null;
    
    this.getElementosRecuperadosEnVector = function(){
        this.elementosRecuperadosEnVector = new Array();
            
        for(var i=0; i<this.elementos_recuperados.length; i++) this.elementosRecuperadosEnVector.push(this.elementos_recuperados[i].split("|"));
        
        return this.elementosRecuperadosEnVector;
    }
    
    this.es_contenedor_flotante = false;
    this.setEsContenedorFlotante = function(valor){
        this.es_contenedor_flotante = valor;
    }
    
    //este vector almacena los indices de las columnas que van a ocultarse
    this.vector_columnas_ocultas = null;
    
    this.setIndiceDeColumnaOculta = function(indice){
        if(!this.vector_columnas_ocultas) this.vector_columnas_ocultas = new Array();
        this.vector_columnas_ocultas[this.vector_columnas_ocultas.length] = indice;
    }
    
    //elemento que habrá de resaltarse cuando se muestra la tabla de elementos
    this.idelemento_resaltar = null;
    this.setIdElementoParaResaltar = function(idelemento){
        this.idelemento_resaltar = idelemento;
    }
    
    //esta función oculta las columnas con base en el vector de columnas
    this.ocultaColumnas = function(filas){
        if(!this.vector_columnas_ocultas) return filas.slice(0);
        var fila;
        var nueva_fila;
        for(var i=0; i<filas.length; i++){
            fila = filas[i].split("|");
            nueva_fila = "";
            for(var k=0; k<fila.length; k++){
                if(!existeElementoEnVector(k, this.vector_columnas_ocultas)){
                    if((k+1) == fila.length) nueva_fila += fila[k];
                    else nueva_fila += fila[k]+"|";
                }
            }
            filas[i] = nueva_fila;
        }
        return filas.slice(0);
    }
    
    //este vector indica el indice de las columnas que no deberán responder al evento onClick
    this.vector_columnas_sinevento_onclick = null;
    
    this.setIndiceDeColumnaSinEventoOnclick = function(indice){
        if(!this.vector_columnas_sinevento_onclick) this.vector_columnas_sinevento_onclick = new Array();
        this.vector_columnas_sinevento_onclick[this.vector_columnas_sinevento_onclick.length] = indice;
    }
    
    this.esColumnaConEventoOnclick = function(indice_columna){
        if(!this.vector_columnas_sinevento_onclick) return true;
        for(var i=0; i<this.vector_columnas_sinevento_onclick.length; i++)
            if(this.vector_columnas_sinevento_onclick[i] == indice_columna) return false;
        return true;
    }
    
    this.implementa_busqueda = true;
    
    //esta función se invoca justo después de mostrar la tabla de elementos
    this.funcion_tabla_mostrada = null;
    
    this.setElementosRecuperados = function(elementos){
        this.elementos_recuperados = elementos;
    }
    
    this.getElementosRecuperados = function(){
        return this.elementos_recuperados;
    }
    
    this.getElementosEnListaParaDb = function(){
        if(!this.elementos_recuperados) return "";
        var cadena = "";
        for(var i=0; i<this.elementos_recuperados.length; i++){
            cadena += this.elementos_recuperados[i]+"^";
        }
        return cadena;
    }
    
    this.setFuncionTablaMostrada = function(funcion){
        this.funcion_tabla_mostrada = funcion;
    }
    
    this.getCeldaSeleccionada = function(){
        return this.celda_seleccionada;
    }
    
    this.setNumeroDeCeldaEditableConFuncion = function(numero, funcion){
        if(!this.funciones_celdas_editables) this.funciones_celdas_editables = new Array();
        this.funciones_celdas_editables[numero] = funcion;
    }
    
    this.setIdElementoSeleccionado = function(idelemento){
        this.idelemento_seleccionado = idelemento;
    }
    
    this.setOpcionDetalle = function(opcion){
        this.opcion_detalles = opcion;
    }
    
    this.setOpcionBorrar = function(opcion){
        this.opcion_borrar = opcion;
    }
    
    this.setOpcionImpresora = function(opcion){
        this.opcion_impresora = opcion;
    }
    
    this.setEncabezados = function(encabezados){
        this.encabezados = encabezados;
    }
    
    this.setMedidas = function(medidas){
        this.medidas = medidas;
    }
    
    this.setAlineaciones = function(alineaciones){
        this.alineaciones = alineaciones;
    }
    
    this.setFuncionEliminaElemento = function(funcion){
        this.funcion_elimina_elemento = funcion;
    }
    
    this.setOpcionesRecuperarElementos = function(opciones){
        this.opciones_recuperar_elementos = opciones;
    }
    
    this.getTituloLista = function(){
        return document.getElementById(this.nombre_instancia+"_titulo_lista_seleccionable");
    }
    
    this.setTituloLista = function(titulo){
        this.titulo_lista = titulo;
        document.getElementById(this.nombre_instancia+"_titulo_lista_seleccionable").innerHTML = titulo;
    }
    
    this.getBotonAgregarElemento = function(){
        return document.getElementById(this.nombre_instancia+"_boton_nuevo_elemento");
    }
    
    this.getZonaElementos = function(){
        return document.getElementById(this.nombre_instancia+"_zona_elementos_lista");
    }
    
    this.getPosicionElementoSeleccionado = function(){
        return this.posicion_elemento_seleccionado;
    }
    
    this.getDatosDeElementoSeleccionado = function(){
        ////console.log("posicion elemento seleccionado: "+this.posicion_elemento_seleccionado);
        return this.elementos_recuperados[this.posicion_elemento_seleccionado];
    }
    
    this.getIdElementoSeleccionado = function(){
        var info = this.elementos_recuperados[this.posicion_elemento_seleccionado].split("|");
        return info[0];
    }
    
    //esta función recupera la fila en donde se hizo click que equivale a la posición del elemento seleccionado + 1
    this.getFilaDeElementoSeleccionado = function(){
        return document.getElementById(this.nombre_instancia+"_tabla_elementos").rows[this.posicion_elemento_seleccionado+1];
    }
    
    this.setFuncionElementoSeleccionado = function(funcion){
        this.funcion_elemento_seleccionado = funcion;
    }
    
    this.setFuncionDetalleElementoSeleccionado = function(funcion){
        this.funcion_detalle_elemento_seleccionado = funcion;
    }
    
    this.setFuncionImprimirElementoSeleccionado = function(funcion){
        this.funcion_imprime_elemento_seleccionado = funcion;
    }
    
    //esta funci—n agrega el elemento nuevo al final de los elementos_recuperados
    this.agregaElemento = function(datos){
        if (!this.elementos_recuperados) this.elementos_recuperados = new Array();
        this.elementos_recuperados[this.elementos_recuperados.length] = datos;
        
        //se pone como el último elemento seleccionado
        this.posicion_elemento_seleccionado = this.elementos_recuperados.length-1;
    }
    
    //esta función agrega un elemento o sobreescribe en la posición dada
    this.agregaElementoEnPosicion = function(datos, posicion){
        if (!this.elementos_recuperados) this.elementos_recuperados = new Array();
        this.elementos_recuperados[posicion] = datos;
        
        //se pone como el último elemento seleccionado
        this.posicion_elemento_seleccionado = posicion;
    }
    
    //esta función recupera el número de elementos de la lista
    this.getNumeroElementosEnLaLista = function(){
        if(!this.elementos_recuperados) return -1;
        return this.elementos_recuperados.length;
    }
    
    //esta funci—n modifica el elemento en la posici—n del elemento seleccionado
    this.modificaElementoEnPosicionSeleccionada = function(datos){
        if (!this.elementos_recuperados) this.elementos_recuperados = new Array();
        this.elementos_recuperados[this.posicion_elemento_seleccionado] = datos;
        this.posicion_elemento_seleccionado = -1;
    }
    
    //esta función compara el id dado con los id de la lista actual. Si existe devuelve true, en otro caso false
    this.existeElementoEnLaListaPorId = function(idelemento){
        if(!this.elementos_recuperados) return false;
        var elemento;
        for(var i=0; i<this.elementos_recuperados.length; i++){
            elemento = this.elementos_recuperados[i].split("|");
            if(elemento[0] == idelemento) return true;
        }
        return false;
    }
    
    //si existe el elemento por id mostrado, entonces se remarcará
    this.remarcaElementoPorId = function(idelemento){
        var fila = document.getElementById(idelemento);
        if(fila){
            $("#"+idelemento).show("highlight ", 1000);
        }
    }
    
    //esta funci—n muestra el componente en su vista para seleccionar
    this.muestraComponente = function(){
        var contenido = "<div id='"+this.nombre_instancia+"_div_lista'>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_elementos_lista'></div><br/>";
        contenido += "</div>";
        
        this.contenedor.innerHTML = contenido;
    }
    
    //esta funci—n muestra el componente en su vista sin el boton de agregar nuevo elemento
    this.muestraComponenteSinOpcionAgregar = function(){
        var contenido = "<div id='"+this.nombre_instancia+"_div_lista'>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_elementos_lista'></div><br/>";
        contenido += "</div>";
        
        this.contenedor.innerHTML = contenido;
    }
    
    //esta funci—n muestra la tabla de elementos que se tienen en elementos_recuperados. Si es null, entonces no muestra la tabla. Se muestra de forma predeterminada, pero si se desea se podr’a sobreescribir el mŽtodo para mostrar la tabla de elementos de la forma deseada
    this.muestraTablaElementos = function(){
        this.implementa_busqueda = false;
        if (!this.elementos_recuperados || this.elementos_recuperados.length == 0){
            this.getZonaElementos().innerHTML = "No hay elementos que mostrar";
            if(this.funcion_tabla_mostrada) setTimeout(this.funcion_tabla_mostrada, 0);
            return;
        }
        
        var aux = this.ocultaColumnas(this.elementos_recuperados.slice(0));
        
        var alto_div = (($(window).height())-($(this.contenedor).offset().top))-120;
        
        var contenido = "<br/><table id='"+this.nombre_instancia+"_tabla_titulos_elementos' align='center' width='98%' cellpadding='5' cellspacing='0' border='0'>";
        
        //agregamos la informaci—n de los encabezados
        contenido += "<tr class='fila_encabezados'><td align='center' width='10%' valign='bottom'>No.</td>";
        for(var i=0; i<this.encabezados.length; i++) contenido += "<td align='center' width='"+this.medidas[i]+"' valign='middle'>"+this.encabezados[i]+"</td>";
        
        if(this.getOpcionesDeElementos() != null){
            contenido += "<td align='center' width='10%' valign='bottom'>Opci&oacute;n</td></tr></table>";
        }else contenido += "</tr></table>";
        
        //agregamos la informaci—n de los elementos recuperados
        var clase = "";
        var elemento;
        
        //style='height:"+alto_div+"px; overflow-y:auto;'
        if(this.es_contenedor_flotante) contenido += "<div><div class='fin_lista'></div><table  id='"+this.nombre_instancia+"_tabla_elementos' align='center' width='98%' cellpadding='5' cellspacing='0' border='0'>";
        else  contenido += "<div><div class='fin_lista'></div><table id='"+this.nombre_instancia+"_tabla_elementos' align='center' width='98%' cellpadding='5' cellspacing='0' border='0'>";
            
        for(var i=0; i<aux.length; i++){
            
            elemento = aux[i].split("|");
            
            if (i%2 == 0) clase = "fila_lista_uno";
            else clase = "fila_lista_dos";
            
            contenido += "<tr class='"+clase+"' id='"+this.nombre_instancia+"_"+elemento[0]+"'><td align='center' onclick='"+this.nombre_instancia+".elementoSeleccionadoPorPosicion("+i+")' width='10%'>"+(i+1)+"</td>";
            
            for(var j=1; j<=this.encabezados.length; j++){
                if(this.funciones_celdas_editables && this.funciones_celdas_editables[j]) contenido += "<td align='"+this.alineaciones[j-1]+"' width='"+this.medidas[j-1]+"'><input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_celda_"+(i+1)+"_"+j+"' value='"+elemento[j]+"' onkeypress='"+this.nombre_instancia+".elementoSeleccionadoPorPosicion("+i+"); "+this.nombre_instancia+".campo_seleccionado = this; isEnterLaTeclaEnCampo(event, \""+this.funciones_celdas_editables[j]+"\");'/></td>";
                else {
                    if(this.esColumnaConEventoOnclick(j-1))
                        contenido += "<td align='"+this.alineaciones[j-1]+"' width='"+this.medidas[j-1]+"' onclick='"+this.nombre_instancia+".elementoSeleccionadoPorPosicion("+i+")'>"+elemento[j]+"</td>";
                    else
                        contenido += "<td align='"+this.alineaciones[j-1]+"' width='"+this.medidas[j-1]+"' >"+elemento[j]+"</td>";
                }
            }
            if(this.getOpcionesElementoPorPosicion() != null) contenido += "<td align='center' width='10%'>"+this.getOpcionesElementoPorPosicion(i)+"</td></tr>";
            else contenido += "</tr>";
        }
        contenido += "</table><div class='fin_lista'></div></div>";
        
        this.getZonaElementos().innerHTML = contenido;
        if(this.getTituloLista()) this.getTituloLista().innerHTML = "";
        if(this.funcion_tabla_mostrada) setTimeout(this.funcion_tabla_mostrada, 0);
        
        this.muestraElementoResaltado();
    }
    
    this.getTablaElementos = function(){
        return document.getElementById(this.nombre_instancia+"_tabla_elementos");
    }
    
    this.ordenaPorColumna = function(numeroColumna){
        if(!this.columnasOrdenadas){
            this.columnasOrdenadas = new Array();
            for(var i=0; i<this.encabezados.length+1; i++) this.columnasOrdenadas[i] = 1;
        }
        
        var sorted = null;
        if(this.getColumnasOrdenadas()[numeroColumna] == 1){
            //sorted ascending
            sorted = sortMatrix(this.getElementosRecuperadosEnVector(), numeroColumna);
            this.columnasOrdenadas[numeroColumna] = 2;
        }else if(this.getColumnasOrdenadas()[numeroColumna] == 2){
            //sorted descending
            sorted = sortMatrix(this.getElementosRecuperadosEnVector(), numeroColumna);
            sorted.reverse();
            this.columnasOrdenadas[numeroColumna] = 1;
        }
        
        var cadena = ""; var vector;
        for(var i=0; i<sorted.length; i++){
            vector = sorted[i];
            for(var j=0; j<vector.length; j++){
                if(j == vector.length-1) cadena += vector[j];
                else cadena += vector[j]+"|";
            }
            cadena += "^";
        }
        var elementos = cadena.split("^");
        elementos.pop();
        
        this.setElementosRecuperados(elementos);
        this.muestraTablaElementosConBusqueda();
    }
    
    //esta funcion copia la implementacion de muestraTablaElementos, pero agrega funcionalidad para busqueda
    this.muestraTablaElementosConBusqueda = function(expresion_regular){
        if (!this.elementos_recuperados || this.elementos_recuperados.length == 0){
            this.getZonaElementos().innerHTML = "No hay elementos que mostrar";
            if(this.funcion_tabla_mostrada) setTimeout(this.funcion_tabla_mostrada, 0);
            return;
        }
        
        //realizamos el filtrado por aquellos elementos que contengan la expresion dada como parametros
        var aux = new Array();
        if(expresion_regular){
            for(var i=0; i<this.elementos_recuperados.length; i++){
                if(expresion_regular.test(this.elementos_recuperados[i])) aux[aux.length] = this.elementos_recuperados[i];
            }
            aux = this.ocultaColumnas(aux);
        }else aux = this.ocultaColumnas(this.elementos_recuperados.slice(0));
        
        var alto_div = (($(window).height())-($(this.contenedor).offset().top))-120;
        
        var contenido = "<br/><table id='"+this.nombre_instancia+"_tabla_titulos_elementos' align='center' width='98%' cellpadding='3' cellspacing='0' border='0'>";
        
        //agregamos la informaci—n de los encabezados
        contenido += "<tr class='fila_encabezados'><td align='center' width='10%' valign='top'>No.</td>";
        for(var i=0; i<this.encabezados.length; i++) contenido += "<td align='center' width='"+this.medidas[i]+"' valign='middle'><div class='headerSelectable' onclick='"+this.nombre_instancia+".ordenaPorColumna("+(i+1)+")'>"+this.encabezados[i]+"</div></td>";
        
        if(this.getOpcionesDeElementos() != null){
            contenido += "<td align='center' width='10%' valign='top'>Opci&oacute;n</td></tr></table>";
        }else contenido += "</tr></table>";
        
        //agregamos la informaci—n de los elementos recuperados
        var clase = "";
        var elemento;
        
        //style='height:"+alto_div+"px; overflow-y:auto;'
        if(this.es_contenedor_flotante) contenido += "<div><div class='fin_lista'></div><table id='"+this.nombre_instancia+"_tabla_elementos' align='center' align='center' width='98%' cellpadding='3' cellspacing='0' border='0'>";
        else  contenido += "<div><div class='fin_lista'></div><table id='"+this.nombre_instancia+"_tabla_elementos' align='center' width='98%' cellpadding='3'  cellspacing='0' border='0'>";
            
        for(var i=0; i<aux.length; i++){
            
            elemento = aux[i].split("|");
            
            if(this.tipo_elementos == 1){
                if (i%2 == 0) clase = "fila_lista_uno";
                else clase = "fila_lista_dos";   
            }else{
                if (i%2 == 0) clase = "fila_lista_uno_small";
                else clase = "fila_lista_dos_small";   
            }
            
            contenido += "<tr class='"+clase+"' id='"+this.nombre_instancia+"_"+elemento[0]+"'><td align='center' onclick='"+this.nombre_instancia+".elementoSeleccionado("+elemento[0]+")' width='10%' valign='top'>"+(i+1)+"</td>";
            
            for(var j=1; j<=this.encabezados.length; j++){
                if(this.funciones_celdas_editables && this.funciones_celdas_editables[j]) contenido += "<td align='"+this.alineaciones[j-1]+"' width='"+this.medidas[j-1]+"' valign='top'><input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_celda_"+(i+1)+"_"+j+"' value='"+elemento[j]+"' onkeypress='"+this.nombre_instancia+".elementoSeleccionado("+i+", "+j+", this); "+this.nombre_instancia+".campo_seleccionado = this; isEnterLaTeclaEnCampo(event, \""+this.funciones_celdas_editables[j]+"\");'/></td>";
                
                else{
                    if(this.esColumnaConEventoOnclick(j-1))
                        contenido += "<td align='"+this.alineaciones[j-1]+"' width='"+this.medidas[j-1]+"' onclick='"+this.nombre_instancia+".elementoSeleccionado("+elemento[0]+");' valign='top'>"+elemento[j]+"</td>";
                    else 
                        contenido += "<td align='"+this.alineaciones[j-1]+"' width='"+this.medidas[j-1]+"' valign='top'>"+elemento[j]+"</td>";
                }
            }
            
            if(this.getOpcionesDeElementos() != null){
                contenido += "<td align='center' width='10%'>"+this.getOpcionesDeElementos(elemento[0])+"</td></tr>";
            }else contenido += "</tr>";
        }
        contenido += "</table><div class='fin_lista'></div></div>";
        
        this.getZonaElementos().innerHTML = contenido;
        
        this.elementosVisiblesEnLista = aux.slice(0);
        
        if(this.getTituloLista()) this.getTituloLista().innerHTML = "";
        if(this.funcion_tabla_mostrada) setTimeout(this.funcion_tabla_mostrada, 0);
        this.muestraElementoResaltado();
    }
    
    this.muestraElementoResaltado = function(){
        var fila_resaltar = document.getElementById(this.nombre_instancia+"_"+this.idelemento_resaltar);
        if(!fila_resaltar) return;
        
        $('html,body').animate({scrollTop:($(fila_resaltar).position().top-200)+"px"}, 500);
        $(fila_resaltar).show("highlight", {color: '#00AAE8'}, 15000, null);
    }
    
    this.getCampoSeleccionado = function(){
        return this.campo_seleccionado;
    }
    
    this.getOpcionesDeElementos = function(idelemento){
        if(!this.opcion_borrar && !this.opcion_detalles) return null;
        var contenido = "<table align='center' cellpadding='5' cellspacing='0' border='0'>";
        contenido += "<tr align='center' valign='top'>";
        
        if (this.opcion_borrar == 1) contenido += "<td align='center'><div class='delete_pk' onclick='"+this.nombre_instancia+".eliminaElemento("+idelemento+")'></div></td>";
        if (this.opcion_detalles == 1) contenido += "<td align='center'><div class='descargar_pk' onclick='"+this.nombre_instancia+".muestraDetalleDeElementoSeleccionado("+idelemento+")'></div></td>";
        
        contenido += "</tr></table>";
        return contenido;
    }
    
    this.getOpcionesElementoPorPosicion = function(posicion){
        ////console.log(this.opcion_borrar+" - "+this.opcion_impresora);
        if(!this.opcion_borrar && !this.opcion_detalles && !this.opcion_impresora) return null;
        var contenido = "<table align='center' cellpadding='5' cellspacing='0' border='0'>";
        contenido += "<tr align='center' valign='top'>";
        
        if (this.opcion_borrar == 1) contenido += "<td align='center'><div class='delete_pk' onclick='"+this.nombre_instancia+".eliminaElementoPorPosicion("+posicion+")'></div></td>";
        if (this.opcion_detalles == 1) contenido += "<td align='center'><div class='descargar_pk' onclick='"+this.nombre_instancia+".muestraDetalleDeElementoSeleccionado("+posicion+")'></div></td>";
        if (this.opcion_impresora == 1) contenido += "<td align='center'><div class='imprimir_pk' onclick='"+this.nombre_instancia+".imprimirElementoSeleccionado("+posicion+")'></div></td>";
        
        contenido += "</tr></table>";
        return contenido;
    }
    
    //función para imprimir el elemento seleccionado
    this.imprimirElementoSeleccionado = function(posicion){
        this.posicion_elemento_seleccionado = posicion;
        
        if(this.funcion_imprime_elemento_seleccionado != "") setTimeout(this.funcion_imprime_elemento_seleccionado, 0);
    }
    
    //esta funci—n elimina el elemento identificado por posicion
    this.eliminaElemento = function(idelemento){
        if (confirm("Deseas eliminar este elemento?")){
            if(this.funcion_elimina_elemento) setTimeout(this.funcion_elimina_elemento, 0);
            this.posicion_elemento_seleccionado = this.getPosicionElementoSeleccionadoPorId(idelemento);
            this.idelemento_eliminado = idelemento;
        }
    }
    
    this.eliminaElementoPorPosicion = function(posicion){
        if (confirm("Deseas eliminar este elemento?")){
            ////console.log("eliminando elemento de posicion: "+posicion);
            this.posicion_elemento_seleccionado = posicion;
            var partes = this.elementos_recuperados[posicion].split("|");
            ////console.log("idelemento: "+partes[0]);
            if(partes[0] != 0) this.idelemento_eliminado = partes[0];
            if(this.idelemento_eliminado > 0){
                if(this.funcion_elimina_elemento){
                    setTimeout(this.funcion_elimina_elemento, 0);
                    return;
                }
            }
            this.quitaElementoDeListaPorPosicion(posicion);
        }
    }
    
    this.quitaElementoDeListaPorPosicion = function(posicion){
        var arreglo_auxiliar = new Array();
            
        ////console.log("eliminar de posicion: "+posicion);
        for(var i=0; i<this.elementos_recuperados.length; i++){
            if (i != posicion) arreglo_auxiliar[arreglo_auxiliar.length] = this.elementos_recuperados[i];
        }
            
        this.elementos_recuperados = arreglo_auxiliar.slice(0);
        if(this.implementa_busqueda) this.muestraTablaElementosConBusqueda(null);
        else this.muestraTablaElementos();
        this.posicion_elemento_seleccionado = -1;
    }
    
    this.quitaElementoDeListaPorId = function(idelemento){
        var posicion = this.getPosicionElementoSeleccionado();
        var arreglo_auxiliar = new Array();
            
        for(var i=0; i<this.elementos_recuperados.length; i++){
            if (i != posicion) arreglo_auxiliar[arreglo_auxiliar.length] = this.elementos_recuperados[i];
        }
            
        this.elementos_recuperados = arreglo_auxiliar.slice(0);
        this.muestraTablaElementosConBusqueda(null);
    }
    
    this.getIdElementoEliminado = function(){
        return this.idelemento_eliminado;
    }
    
    this.getPosicionElementoSeleccionadoPorId = function(idelemento){
        var partes;
        for(var i=0; i<this.elementos_recuperados.length; i++){
            partes = this.elementos_recuperados[i].split("|");
            if(partes[0] == idelemento) return i;
        }
        return -1;
    }
    
    this.setPosicionElementoSeleccionado = function(posicion){
        this.posicion_elemento_seleccionado = posicion;
    }
    
    this.elementoSeleccionadoPorPosicion = function(posicion){
        this.posicion_elemento_seleccionado_anterior = this.posicion_elemento_seleccionado;
        this.posicion_elemento_seleccionado = posicion;
        
        /*this.numero_celda_seleccionada = numero_columna;
        this.celda_seleccionada = elemento_celda;*/
        
        if(this.funciones_celdas_editables && this.funciones_celdas_editables[numero_columna]) setTimeout(this.funciones_celdas_editables[numero_columna], 0);
        else setTimeout(this.funcion_elemento_seleccionado, 0);
        ////console.log("posicion actual presionada sin busqueda: "+this.posicion_elemento_seleccionado);
    }
    
    //funci—n invocada cuando se hace click sobre un elemento
    this.elementoSeleccionado = function(idelemento){
        var posicion = this.getPosicionElementoSeleccionadoPorId(idelemento);
        this.posicion_elemento_seleccionado_anterior = this.posicion_elemento_seleccionado;
        this.posicion_elemento_seleccionado = posicion;
        
        /*this.numero_celda_seleccionada = numero_columna;
        this.celda_seleccionada = elemento_celda;*/
        
        //console.log("funcion elemento seleccionado: "+this.funcion_elemento_seleccionado);
        
        if(this.funciones_celdas_editables && this.funciones_celdas_editables[numero_columna]) setTimeout(this.funciones_celdas_editables[numero_columna], 0);
        else setTimeout(this.funcion_elemento_seleccionado, 0);
    }
    
    //funciones que deber‡n ser implementadas cuando se use una lista seleccionable
    this.agregaElementoEnLaLista = function(nuevo_elemento){
        if(!this.elementos_recuperados) this.elementos_recuperados = new Array();
        if(this.posicion_elemento_seleccionado != -1) this.elementos_recuperados[this.posicion_elemento_seleccionado] = nuevo_elemento;
        else this.elementos_recuperados[this.elementos_recuperados.length] = nuevo_elemento;
    }
    
    //esta funci—n proporciona el comportamiento a detalle de cuando se selecciona un elemento de la lista
    this.muestraDetalleDeElementoSeleccionado = function(idelemento){
        var posicion = this.getPosicionElementoSeleccionadoPorId(idelemento);
        
        this.posicion_elemento_seleccionado_anterior = this.posicion_elemento_seleccionado;
        this.posicion_elemento_seleccionado = posicion;
        
        setTimeout(this.funcion_detalle_elemento_seleccionado, 0);
    }
    
    //Esta funci—n recupera los elementos de la lista, dependiendo de la configuraci—n que se especifique para la lista
    this.getElementosDeLaLista = function(){
        if (this.getZonaElementos()) this.getZonaElementos().innerHTML = "<div class='cargando_grande'></div>";
        var componente = this;
        var ajax = objetoAjax();
        ajax.open("POST", enlace, true);
        ajax.onreadystatechange=function() {
                if (ajax.readyState == 4){
                    ////console.log("respuesta al recuperar elementos: "+ajax.responseText);
                    if(ajax.responseText == "") componente.elementos_recuperados = Array();
                    else{
                        componente.elementos_recuperados = ajax.responseText.split("^");
                        componente.elementos_recuperados.pop();
                    }
                    componente.muestraTablaElementos();
                }
        }
        ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        ajax.send("token="+login.getToken()+"&"+this.opciones_recuperar_elementos);
    }
    
    //esta función recupera un elemento específico de la lista
    this.getElementoEnPosicion = function(posicion){
        if(!this.elementos_recuperados || posicion >= this.elementos_recuperados.length) return null;
        return this.elementos_recuperados[posicion];
    }
    
    //esta función recupera la celda del elemento seleccionado, dada la posición
    this.getCeldaDeElementoSeleccionadaPorPosicion = function(posicion){
        var datos = this.elementos_recuperados[this.posicion_elemento_seleccionado].split("|");
        var fila = document.getElementById(this.nombre_instancia+"_"+datos[0]);
        return fila.cells[posicion];
    }
    
    //esta función modifica el dato en el elemento seleccionado y lo guarda nuevamente en los elementos recuperados
    this.modificaDatoDeElementoSeleccionado = function(dato, posicion){
        var datos = this.elementos_recuperados[this.posicion_elemento_seleccionado].split("|");
        datos[posicion] = dato;
        var nueva_info = "";
        for(var i=0; i<datos.length; i++){
            nueva_info += datos[i]+"|";
        }
        this.elementos_recuperados[this.posicion_elemento_seleccionado] = nueva_info;
    }
    
    //esta función pone como seleccionada la fila anterior de la fila seleccionada actual, solo si la fila seleccionada actual es mayor que 1. En otro caso regresa sin más
    this.setFilaAnteriorSeleccionada = function(){
        if(this.posicion_elemento_seleccionado > 1){
            this.posicion_elemento_seleccionado--;
        }
    }
}
var SelectorOpcion = function(){
    
    this.funcion_agregar = null;
    this.setFuncionAgregar = function(funcion){
        this.funcion_agregar = funcion;
    }
    
    this.texto_opcion_hover = "";
    this.setTextoOpcionHover = function(texto){
        this.texto_opcion_hover = texto;
    }
    
    this.texto_opcion_seleccionada = "";
    this.setTextoOpcionSeleccionada = function(texto){
        this.texto_opcion_seleccionada = texto;
    }
    
    this.getNombreOpcionSeleccionada = function(){
        return this.texto_opcion_seleccionada;
    }
    
    this.getBotonAgregar = function(){
        return document.getElementById(this.nombre_instancia+"_boton_agregar");
    }
    
    this.muestraComponente = function(){
        var contenido = "<table width='98%'><tr><td width='5%'>";
        contenido += "<div onclick='"+this.funcion_agregar+"' class='div_pointer' id='"+this.nombre_instancia+"_boton_agregar'><a class='btn-floating fondo_principal scale-transition scale-in' onmouseover='"+this.nombre_instancia+".botonOver();' onmouseout='"+this.nombre_instancia+".botonOut();'><i class='material-icons'>add</i></a>";
        contenido += "</td><td width='95%'>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_opcion'><div class='truncate'>"+this.texto_opcion_seleccionada+"</div></div>";
        contenido += "</td></tr></table>";
        
        this.contenedor.innerHTML = contenido;
    }
    
    this.botonOver = function(){
        if(document.getElementById(this.nombre_instancia+"_zona_opcion")) document.getElementById(this.nombre_instancia+"_zona_opcion").innerHTML = "<div class='ayuda_campos_visible' id='"+this.nombre_instancia+"_div_selector'>"+this.texto_opcion_hover+"</div>";
        //muestraAyudaCampo(this.nombre_instancia+"_div_selector", this.texto_opcion_hover);
    }
    
    this.botonOut = function(){
        if(document.getElementById(this.nombre_instancia+"_zona_opcion")) document.getElementById(this.nombre_instancia+"_zona_opcion").innerHTML = "<div class='truncate'>"+this.texto_opcion_seleccionada+"</div>";
    }
}
// JavaScript Controlador de Navegación
var ControladorNavegacion =  function(){
	
	this.contenedor = null;
	this.metodo_atras = null;
	this.nombre_instancia = "";
    
    this.componentes_padre = null;
    this.funciones_atras = null;
    this.textos_ayuda = null;
    
    this.posicion = -1;
	
	this.setControladorNavegacion = function(contenedor, nombre){
		this.contenedor = contenedor;
		this.nombre_instancia = nombre;
	}
    
    this.push = function(padre, funcion, texto){
        this.posicion++;
        
        if(!this.componentes_padre) this.componentes_padre = new Array();
        if(!this.funciones_atras) this.funciones_atras = new Array();
        if(!this.textos_ayuda) this.textos_ayuda = new Array();
        
        this.componentes_padre[this.componentes_padre.length] = padre;
        this.funciones_atras[this.funciones_atras.length] = funcion;
        this.textos_ayuda[this.textos_ayuda.length] = texto;
    }
    
    this.pop = function(){
        //Si la posicion ahora es -1, quiere decir que ya no hay un componente atras, por lo cual el contenedor que devuelve es el contenedor original, es decir, el contenedor principal. En otro caso devuelve el contenedor de contenido del controlador
        //console.log("posicion: "+this.posicion);
        var contenedor;
        if(this.posicion == 0) contenedor = this.contenedor;
        else{
            this.posicion--;
            this.muestraControladorNavegacion();
            contenedor = this.getContenedorDeContenido();
            this.posicion++;
        }
        
        this.componentes_padre[this.posicion].setContenedor(contenedor);
        //this.componentes_padre[this.posicion].muestraComponente();
        setTimeout(this.funciones_atras[this.posicion], 0);
        
        ////console.log("valores padre: "+this.componentes_padre[this.posicion]+", funcion: "+this.funciones_atras[this.posicion]+" texto:"+this.textos_ayuda[this.posicion]+" posicion: "+this.posicion);
        
        this.posicion--;
        
        if(this.funciones_atras) this.funciones_atras.pop();
        if(this.textos_ayuda) this.textos_ayuda.pop();
        if(this.componentes_padre) this.componentes_padre.pop();
    }    
	
	this.getContenedorDeContenido = function(){
		return document.getElementById(this.nombre_instancia+'_contenido_controlador_navegacion');
	}
    
    this.setTextoAyuda = function(texto){
        this.texto_ayuda = texto;
    }
    
    this.setMetodoAtras = function(metodo){
        this.metodo_atras = metodo;
    }
    
    this.getContenedor = function(){
        return this.contenedor;
    }
	
	//esta función recupera el botón cerrar del controlador de navegacion
	this.getBotonCerrar = function(){
		return document.getElementById(this.nombre_instancia+"_cerrar");
	}
    
    this.botonCerrarPresionado = function(){
        this.pop();
    }
	
	this.muestraControladorNavegacion = function(){
		if(this.textos_ayuda) var texto = this.textos_ayuda[this.posicion];
        else var texto = "";
        
        var contenido = '<div class="card-panel">';
        contenido += "<table class='centered'><tr><td width='5%'>";
        
        contenido += "<div class='left-align'><a class='btn-floating fondo_principal scale-transition scale-in' onclick='"+this.nombre_instancia+".botonCerrarPresionado()' onmouseover='muestraAyudaCampo(\""+this.nombre_instancia+"_controlador_navegacion_close\", \""+texto+"\");' onmouseout='ocultaAyudaCampo(\""+this.nombre_instancia+"_controlador_navegacion_close\");'><i class='material-icons'>close</i></a></div>";
        
        contenido += "</td><td width='95%'>";
        
        contenido += "<div class='ayuda_campos' id='"+this.nombre_instancia+"_controlador_navegacion_close'></div>";
        
        contenido += "</td></tr></table>";
        
        contenido += "<br/><div id='"+this.nombre_instancia+"_contenido_controlador_navegacion' style='height:"+(getAlto()*0.9)+"px; overflow-y:auto;'>";
        contenido += "</div>";
		
		this.contenedor.innerHTML = contenido;
	}
    
    this.muestraControladorNavegacionMedio = function(){
		if(this.textos_ayuda) var texto = this.textos_ayuda[this.posicion];
        else var texto = "";
        
        var contenido = '<div class="card-panel">';
        contenido += "<table class='centered'><tr><td width='5%'>";
        
        contenido += "<div class='left-align'><a class='btn-floating fondo_principal scale-transition scale-in' onclick='"+this.nombre_instancia+".botonCerrarPresionado()' onmouseover='muestraAyudaCampo(\""+this.nombre_instancia+"_controlador_navegacion_close\", \""+texto+"\");' onmouseout='ocultaAyudaCampo(\""+this.nombre_instancia+"_controlador_navegacion_close\");'><i class='material-icons'>close</i></a></div>";
        
        contenido += "</td><td width='95%'>";
        
        contenido += "<div class='ayuda_campos' id='"+this.nombre_instancia+"_controlador_navegacion_close'></div>";
        
        contenido += "</td></tr></table>";
        
        contenido += "<br/><div id='"+this.nombre_instancia+"_contenido_controlador_navegacion' style='overflow-y:auto;'>";
        contenido += "</div>";
		
		this.contenedor.innerHTML = contenido;
	}
}
var ComponenteValidacionTexto = function(){
	
	//esta variable almacena el la forma de recuperar el keycode dependiendo del navegador, con lo cual una sola llamada bastará para identificar dicho método. Posteriormente esta variable determinará el método a utilizar, con lo cual se evitarán llamadas a la función que recupera el keycode, quedando en su lugar solo una condicional para recuperar el keycode
	this.metodo_keycode = null;
	
	//este arreglo contiene la lista de los caracteres permitidos
	this.caracteres_permitidos = [8,13,32,37,39,42,44,45,46,47,91,93,164,165,209,241];
    
    //arreglo con los caracteres permitidos para cifras
    this.caracteres_permitidos_para_cifras = [46];
	
	//este arreglo contiene la lista de los caracteres prohibídos que se saltan la comprobación de los caracteres permitidos por alguna razón. estos se evalúan al final
	this.caracteres_prohibidos = [];
	
	this.flecha = false;
	
	//esta función identifica y recupera el valor keycode de la tecla presionada
	this.getKeyCode = function(e) {
        var keycode = null;
        if(window.event) {
            keycode = window.event.keyCode;
			this.metodo_keycode = 1;
        }else if(e) {
            keycode = e.which;
			this.metodo_keycode = 2;
        }
        return keycode;
    }
	
	//esta función determina regresa true si el valor keycode dado como parámetro es número, false en otro caso
	this.esNumero = function(keycode){
		if(keycode >= 48 && keycode <= 57) return true;
		return false;
	}
	
	//esta función regresa true si el valor keycode dado como parámetro es letra mayúscula, false en otro caso
	this.esLetraMayuscula = function(keycode){
		if(keycode >= 65 && keycode <= 90) return true;
		return false;
	}
	
	//esta función regresa true si el valor keycode dado como parámetro es uno de los caracteres permitidos, en otro caso false
	this.esCaracterPermitido = function(keycode){
		for(var i=0; i<this.caracteres_permitidos.length; i++) 
			if(keycode == this.caracteres_permitidos[i]) return true;
		return false;
	}
    
    //esta funcion devuelve true si el elemento dado pertenece al vector
    this.esCaracterPermitidoDeCifra = function(keycode){
        for(var i=0; i<this.caracteres_permitidos_para_cifras.length; i++) 
			if(keycode == this.caracteres_permitidos_para_cifras[i]) return true;
		return false;
    }
	
	//esta función regresa true si el valor del keycode dado como parámetro es una minúscula, en otro caso false
	this.esLetraMinuscula = function(keycode){
		if(keycode >= 97 && keycode <= 122) return true;
		return false;
	}
	
	this.esSoloNumero = function(evento){
		//se pregunta si ya se conoce el método de recuperación del keycode dependiendo del navegador. Si ya se conoce solo se recupera la información de la tecla con el método que determina la variable this.metodo_keycode
		if(evento.ctrlKey || evento.metaKey) return false;//para prevenir el copia y pega
		if(this.metodo_keycode == null) var keycode = this.getKeyCode(evento);
		else if(this.metodo_keycode == 1) var keycode = window.event.keyCode;
		else var keycode = evento.which;
		
		//solo número no permite los caracteres permitidos, solo admite del 0 - 9
		if(!this.esNumero(keycode) && !this.esCaracterPermitidoDeCifra(keycode)){
			return false;
		}
		return true;
	}
	
	this.esAlfaNumerico = function(evento){
		//se pregunta si ya se conoce el método de recuperación del keycode dependiendo del navegador. Si ya se conoce solo se recupera la información de la tecla con el método que determina la variable this.metodo_keycode
		if(evento.ctrlKey || evento.metaKey) return false;//para prevenir el copia y pega
		if(this.metodo_keycode == null) var keycode = this.getKeyCode(evento);
		else if(this.metodo_keycode == 1) var keycode = window.event.keyCode;
		else var keycode = evento.which;
		if(!this.esNumero(keycode) && !this.esLetraMayuscula(keycode) && !this.esLetraMinuscula(keycode) && !this.esCaracterPermitido(keycode)){
			return false;
		}
		return true;
	}
    
    this.tieneLongitudMaxima = function(input, maximo){
        if(input.value.length == maximo) return true;
        return false;
    }
}
var SelectorDeOpcion = function(){
    
    this.visualizacion = 1;
    this.setVisualizacion = function(tipo_visualizacion){
        this.visualizacion = tipo_visualizacion;
    }
    
    this.texto_opcion_seleccionada = "Selecciona";
    this.setTextoDeOpcionSeleccionada = function(texto){
        this.texto_opcion_seleccionada = texto;
        if(document.getElementById(this.nombre_instancia+"_div_selector")) document.getElementById(this.nombre_instancia+"_div_selector").innerHTML = this.texto_opcion_seleccionada;
    }
    
    this.funcion_selector_presionado = "";
    this.setFuncionSelectorPresionado = function(funcion){
        this.funcion_selector_presionado = funcion;
    }
    
    this.funcion_opcion_selector_presionado = "";
    this.setFuncionOpcionSelectorPresionado = function(funcion){
        this.funcion_opcion_selector_presionado = funcion;
    }
    
    this.muestraComponente = function(){
        //console.log("visualizacion: "+this.visualizacion);
        if(this.visualizacion == 1) var contenido = "<div class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".selectorPresionado()' id='"+this.nombre_instancia+"_div_selector' style='width:96%;'>"+this.texto_opcion_seleccionada+"</div>";
        else contenido = "<table align='center' cellpadding='0' cellpadding='0' cellspacing='0'><tr><td align='center' width='90%'><div class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".selectorPresionado()' id='"+this.nombre_instancia+"_div_selector' style='width:100%;'>"+this.texto_opcion_seleccionada+"</div></td><td align='center' width='10%' valign='middle'><div class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".opcionSelectorPresionado()' id='"+this.nombre_instancia+"_div_opcion_selector' style='text-align:center;' style='width:100%;'><div class='info' style='box-sizing: content-box; box-shadow: none; padding-top: 18px;'></div></div></td></tr></table>";
        
        this.contenedor.innerHTML = contenido;
    }
    
    this.selectorPresionado = function(){
        if(this.funcion_selector_presionado) setTimeout(this.funcion_selector_presionado, 0);
    }
    
    this.opcionSelectorPresionado = function(){
        if(this.funcion_opcion_selector_presionado) setTimeout(this.funcion_opcion_selector_presionado, 0);
    }
}
//Este componente muestra un elemento emergente y modal multi prop—sito
var ComponenteModal = function(){
    
    this.nombre_instancia = "";
    
    this.permiso_para_cerrar = true;
    this.setPermisoParaCerrar = function(permiso){
        this.permiso_para_cerrar = permiso;
        $.colorbox.setPermisoParaCerrarConOverlay(this.permiso_para_cerrar);
    }
    
    this.funcion_al_cerrar = null;
    this.setFuncionAlCerrar = function(funcion){
        this.funcion_al_cerrar = funcion;
        $.colorbox.setFuncionAlCerrar(this.funcion_al_cerrar);
    }
    
    this.muestra_boton_cerrar = true;
    this.setVisibilidadBotonCerrar = function(valor){
        this.muestra_boton_cerrar = valor;
        $.colorbox.setVisibilidadDeBotonCerrar(this.muestra_boton_cerrar);
    }
    
    this.alto = 0;
    this.ancho = 0;
    this.setDimensiones = function(alto, ancho){
        this.alto = alto;
        this.ancho = ancho;
    }
    
    //este componente no necesita tener un contenedor, ya que se muestra en pantalla completa
    this.setComponente = function(nombre_instancia){
        this.nombre_instancia = nombre_instancia;
    }
    
    this.muestraComponente = function(){
        var contenido = "<table align='center' width='600' cellpadding='0' cellspacing='0' border='0'>";
        contenido += "<tr valign='top'><td align='center'>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_contenido_modal' class='contenido_modal'></div>";
        
        contenido += "</td></tr>";
        contenido += "</table>";
        
        $.colorbox({html:contenido, transition:"elastic"});
    }
    
    this.muestraComponenteSmall = function(){
        var contenido = "<table align='center' width='500' cellpadding='0' cellspacing='0' border='0'>";
        contenido += "<tr valign='top'><td align='center'>";
        
        contenido += "<div style='width:"+($(window).width() * 0.45)+"px; height:"+($(window).height() * 0.90)+"px; overflow-x:hidden;' id='"+this.nombre_instancia+"_zona_contenido_modal'></div>";
        
        contenido += "</td></tr>";
        contenido += "</table>";
        
        $.colorbox({html:contenido, transition:"elastic"});
    }
    
    this.muestraComponenteMuyPequenio = function(){
        //console.log("Mostrando este");
        var contenido = "<table align='center' width='300' cellpadding='0' cellspacing='0' border='0'>";
        contenido += "<tr valign='top'><td align='center'>";
        
        contenido += "<div style='height:"+($(window).height() * 0.6)+"px; width:300px; overflow-x:hidden;' id='"+this.nombre_instancia+"_zona_contenido_modal'></div>";
        
        contenido += "</td></tr>";
        contenido += "</table>";
        
        $.colorbox({html:contenido, transition:"elastic"});
    }
    
    this.muestraComponentePersonalizado = function(){
        //console.log("Model personalizado");
        var contenido = "<table align='center' width='"+this.ancho+"' cellpadding='0' cellspacing='0' border='0'>";
        contenido += "<tr valign='top'><td align='center'>";
        
        contenido += "<div style='height:"+this.alto+"px; width:"+this.ancho+"px; overflow-x:hidden;' id='"+this.nombre_instancia+"_zona_contenido_modal'></div>";
        
        contenido += "</td></tr>";
        contenido += "</table>";
        
        $.colorbox({html:contenido, transition:"elastic"});
    }
    
    this.muestraComponenteGrande = function(){
        var contenido = "<div style='width:"+($(window).width() * 0.60)+"px; height:"+($(window).height() * 0.90)+"px; overflow-x:hidden; text-align:center; padding:6px;' id='"+this.nombre_instancia+"_zona_contenido_modal'></div>";
        
        $.colorbox({html:contenido, transition:"elastic"});
    }
    
    this.muestraComponenteHuge = function(){
        var contenido = "<div style='width:"+($(window).width() * 0.80)+"px; height:"+($(window).height() * 0.90)+"px; overflow-x:hidden; text-align:center; padding:6px;' id='"+this.nombre_instancia+"_zona_contenido_modal'></div>";
        
        $.colorbox({html:contenido, transition:"elastic"});
    }
    
    this.getContenedorDeContenido = function(){
        return document.getElementById(this.nombre_instancia+"_zona_contenido_modal");
    }
    
    this.ocultaComponente = function(){
        $.colorbox.close();
    }
}
// JavaScript Document del Componente SelectorFecha
var SelectorFechas = function(){
	
	//esta variable almacena la fecha actual. Para ello genera un objeto del tipo fecha actual al iniciar la ejecución del script
	this.fecha_actual = new Date();
	
	//esta variable almacena el nombre del contenedor del componente fecha
	this.contenedor_selector_fecha = null;
	
	//esta variable almacena el nombre de la instancia de tipo selector de fecha. El nombre por defector es selector fecha
	this.nombre_instancia = "selector_fechas";
	
	//esta variable indica si se ha cambiado algún combo. De ser así, entonces la variable se pone en true en otro caso en false. Esta variable determina el valor recuperado cuando se llama al método para recuperar la fecha indicada por los combos
	this.fecha_modificada = false;
	
	this.funcion_cambia = null;
    this.funcionOnChange = function(funcion){
        this.funcion_cambia = funcion;
    }
	
	//método principal incializa las variables del componente
	this.setSelectorFecha = function(contenedor, nombre){
		this.contenedor_selector_fecha = contenedor;
		this.nombre_instancia = nombre;
		this.fecha_modificada = false;
	}
    
    //método agregado para homogeneidad
    this.setComponente = function(contenedor, nombre){
        this.contenedor_selector_fecha = contenedor;
		this.nombre_instancia = nombre;
		this.fecha_modificada = false;
    }
    
    this.muestraComponente = function(){
        this.muestraComboFechas();
    }
	
	//esta función sirve para modificar la fecha del componente, por la fecha dada como parametro
	this.setFechaDesdeDB = function(fecha){
		//primero separamos la fecha dada como parametro
		var fecha_separada = fecha.split("-");
		this.fecha_actual = new Date(fecha_separada[0], (fecha_separada[1]-1), fecha_separada[2]);
		this.fecha_modificada = true;
		this.muestraComboFechas();
	}

	//esta función dibuja los combos del componente
	this.muestraComboFechas = function(){
		//si el contenedor del componente es null, entonces se regresa sin realizar algún cambio
		if(this.contenedor_selector_fecha == null) return;
		//primero se dibuja la estructura del selector de fechas
		this.setEstructuraSelectorFechas();
		//se dibuja y muestra el combo de los años
		this.setComboAnios();
		//se dibuja y muestra el combo de los meses
		this.setComboMeses();
		//esta función dibuja el combo con los días
		this.setComboDias();
	}
		
	//esta función dibuja el combo de los años
	this.setComboAnios = function(){
		var contenido = "<select class='browser-default' id='combo_anios_"+this.nombre_instancia+"' onchange='"+this.nombre_instancia+".cambioCombos(); "+this.funcion_cambia+"'>";
		var anio  = this.fecha_actual.getFullYear();
		//agrega por defecto 4 anios antes y 4 anios después del anio actual.
		var anio_inicial = anio - 100;
		var anio_final =  anio + 30;
		for(anio_inicial; anio_inicial <= anio_final; anio_inicial++){
			if(anio_inicial == anio && this.fecha_modificada) contenido += "<option selected='selected' value='"+anio_inicial+"'>"+anio_inicial+"</option>";
			else contenido += "<option value='"+anio_inicial+"'>"+anio_inicial+"</option>";	
		}
		if(!this.fecha_modificada) contenido += "<option selected='selected' value='0'>-</option>";
		else contenido += "<option value='0'>-</option>";
		contenido += "</select>";
		document.getElementById('area_anios_'+this.nombre_instancia).innerHTML = contenido;
	}
	
	//esta función genera y mustra los meses en función del anio seleccionado
	this.setComboMeses = function(){
		var contenido = "<select class='browser-default' id='combo_meses_"+this.nombre_instancia+"' onchange='"+this.nombre_instancia+".cambioCombos(); "+this.funcion_cambia+"'>";
		for(var i=0; i < 12; i++){
			if(i == this.fecha_actual.getMonth() && this.fecha_modificada) contenido += "<option selected='selected' value="+(i+1)+">"+getNombreMes(i+1)+"</option>";
			else contenido += "<option value="+(i+1)+">"+getNombreMes(i+1)+"</option>";
		}
		if(!this.fecha_modificada) contenido += "<option selected='selected' value='0'>-</option>";
		else contenido += "<option value='0'>-</option>";
		contenido += "</select>";
		document.getElementById('area_meses_'+this.nombre_instancia).innerHTML = contenido;
	}
	
	//esta función dibuja el combo con los días
	this.setComboDias = function(){
		var mes = this.fecha_actual.getMonth()+1;
		var max_dias = getMaximosDiasPorMes(mes, this.fecha_actual.getFullYear());
		var contenido = "<select class='browser-default' id='combo_dias_"+this.nombre_instancia+"' onchange='"+this.nombre_instancia+".cambioCombos(); "+this.funcion_cambia+"'>";
		for(i=0; i<max_dias; i++ ){
			if(i+1 == this.fecha_actual.getDate() && this.fecha_modificada) contenido += "<option selected='selected' value="+(i+1)+">"+(i+1)+"</option>";
			else contenido += "<option value="+(i+1)+">"+(i+1)+"</option>";
		}
		if(!this.fecha_modificada) contenido += "<option selected='selected' value='0'>-</option>";
		else contenido += "<option value='0'>-</option>";
		contenido += "</select>";
		document.getElementById('area_dias_'+this.nombre_instancia).innerHTML = contenido;
	}
	
	//esta función es utilizada para saber la fecha actual que tienen seleccionados los combos.
	this.getFechaActualParaDB = function(){
		if(!this.fecha_modificada) return null;
		var mes = document.getElementById('combo_meses_'+this.nombre_instancia).selectedIndex+1;
		if (mes < 10) mes = "0"+mes;
		var dia = document.getElementById('combo_dias_'+this.nombre_instancia).selectedIndex+1;
		if (dia < 10) dia = "0"+dia;
		var i_anio = document.getElementById('combo_anios_'+this.nombre_instancia).selectedIndex;
		return document.getElementById('combo_anios_'+this.nombre_instancia).options[i_anio].value+"-"+mes+"-"+dia;
	}
	
	//esta función es ejecutada por los combos cuando cambia alguno de ellos
	this.cambioCombos = function(){
		//primero recuperamos los tres combos
		var combo_mes = document.getElementById('combo_meses_'+this.nombre_instancia); 
		var combo_dias = document.getElementById('combo_dias_'+this.nombre_instancia);
		var combo_anios = document.getElementById('combo_anios_'+this.nombre_instancia)
		
		//recuperamos los valores para año, mes y dia
		var anio = combo_anios.options[combo_anios.selectedIndex].value;
		var mes = combo_mes.options[combo_mes.selectedIndex].value;
		var dia = combo_dias.options[combo_dias.selectedIndex].value;
		
		//si el valor del dia es -, entonces quiere decir que el usuario movió el día a este valor para indicar que la fecha debe ponerse en el estado - - -, y la fecha modificada a false
		if(dia == 0){
			//entonces movemos los valores de los otros combos a options.lengt en selected index
			combo_mes.selectedIndex = combo_mes.options.length-1;
			combo_anios.selectedIndex = combo_anios.options.length-1;
			this.fecha_modificada = false;
			return;
		}else{
			if(dia != 0 && mes == 0 && anio == 0){
				//en este caso el combo estaba en el estado - - -, se movió el combo de día y automáticamente se deberá mover el combo a una fecha válida, considerando la fecha actual
				this.fecha_modificada = true;
				this.setComboAnios();
				this.setComboMeses();
				return;
			}
		}
		
		var mes = combo_mes.selectedIndex;
		var dia = combo_dias.selectedIndex+1;
	
		this.fecha_actual.setDate(dia);
		this.fecha_actual.setMonth(mes);
		this.fecha_actual.setYear(anio);
		this.setComboAnios();
		this.setComboMeses();
		this.setComboDias();
		this.fecha_modificada = true;
	}
	
	//esta función genera la estructura del componente, para que acepte los combos de día mes año
	this.setEstructuraSelectorFechas = function(){
		var contenido = "<table id='area_fecha_"+this.nombre_instancia+"' align='center'><tr><td><div id='area_dias_"+this.nombre_instancia+"''></div></td><td><div id='area_meses_"+this.nombre_instancia+"''></div></td><td><div id='area_anios_"+this.nombre_instancia+"''></div></td></table>";	
		this.contenedor_selector_fecha.innerHTML = contenido;
	}
}

function getNumeroDeMesPorNombreCorto(nombre_mes){
    if(nombre_mes == "Ene") return "01";
    else if(nombre_mes == "Feb") return "02";
    else if(nombre_mes == "Mar") return "03";
    else if(nombre_mes == "Abr") return "04";
    else if(nombre_mes == "May") return "05";
    else if(nombre_mes == "Jun") return "06";
    else if(nombre_mes == "Jul") return "07";
    else if(nombre_mes == "Ago") return "08";
    else if(nombre_mes == "Sep") return "09";
    else if(nombre_mes == "Oct") return "10";
    else if(nombre_mes == "Nov") return "11";
    else if(nombre_mes == "Dic") return "12";
}

//esta función calcula la edad a partir de una fecha dada
function calcular_edad(dia_nacim,mes_nacim,anio_nacim)
{
	fecha_hoy = new Date();
	ahora_anio = fecha_hoy.getYear();
	ahora_mes = fecha_hoy.getMonth();
	ahora_dia = fecha_hoy.getDate();
	edad = (ahora_anio + 1900) - anio_nacim;
	if ( ahora_mes < (mes_nacim - 1))
	{
		edad--;
	}
	if (((mes_nacim - 1) == ahora_mes) && (ahora_dia < dia_nacim))
	{ 
		edad--;
	}
	if (edad > 1900)
	{
		edad -= 1900;
	}
	return edad;
}

function getNombreMes(numero_mes){
	switch(numero_mes){
		case 1: return "Ene";
					break;
		case 2: return "Feb";
					break;		
		case 3: return "Mar";
					break;
		case 4: return "Abr";
					break;
		case 5: return "May";
					break;
		case 6: return "Jun";
					break;
		case 7: return "Jul";
					break;
		case 8: return "Ago";
					break;
		case 9: return "Sep";
					break;
		case 10: return "Oct";
					break;
		case 11: return "Nov";
					break;
		case 12: return "Dic";
					break;																																		
	}
}

function getNombreMesCompleto(numero_mes){
	switch(numero_mes){
		case 1: return "Enero";
					break;
		case 2: return "Febrero";
					break;		
		case 3: return "Marzo";
					break;
		case 4: return "Abril";
					break;
		case 5: return "Mayo";
					break;
		case 6: return "Junio";
					break;
		case 7: return "Julio";
					break;
		case 8: return "Agosto";
					break;
		case 9: return "Septiembre";
					break;
		case 10: return "Octubre";
					break;
		case 11: return "Noviembre";
					break;
		case 12: return "Diciembre";
					break;																																		
	}
}

function getMaximosDiasPorMes(numero_mes, anio){
	switch(numero_mes){
		case 1: return 31;
					break;
		case 2: if(anio%4 == 0) return 29;
					else return 28;
					break;
		case 3: return 31;
					break;
		case 4: return 30;
					break;
		case 5: return 31;
					break;
		case 6: return 30;
					break;
		case 7: return 31;
					break;
		case 8: return 31;
					break;
		case 9: return 30;
					break;
		case 10: return 31;
					break;
		case 11: return 30;
					break;
		case 12: return 31;
					break;																					
	}
}
var CatalogoUsuarios = function(){
    
    this.filtroTipoUsuarios = 0;
    this.setFiltroTipoUsuarios = function(tipo){
        this.filtroTipoUsuarios = tipo;
    }
    
    this.muestraComponente = function(){
        if(this.soloSeleccion == 0) this.setTituloCatalogo("Usuarios del Sistema");
        else this.setTituloCatalogo("Promotores");
        
        this.setTextoAgregarElemento("Agregar nuevo usuario");
        
		this.setFormaCaptura(new FormaUsuario);
        
        if(this.soloSeleccion == 0) this.muestraEstructuraCatalogoPrincipalConOpcionAgregarNuevo();
        else this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo();
        
        //realizamos la búsqueda en la base de datos de pacientes
        getResultadoAjax("opcion=6", this.nombre_instancia+".muestraResultados", document.getElementById(this.nombre_instancia+"_zona_boton_buscar"));
        
        //mostramos las opciones de impresion
        if(this.soloSeleccion == 0) this.mostraOpcionesDeImpresion();
    }
    
    this.muestraResultados = function(respuesta){
        var usuarios = respuesta.split("^");
        usuarios.pop();
        if(usuarios.length == 0){
            var contenido = "<div>No hay usuarios registrados en el sistema aún</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
        }
        
        if(this.filtroTipoUsuarios != 0){
            //idusuario, nombreCompleto, perfil, idTipoPerfil
            var usuariosFiltrados = new Array();
            var usuario;
            for(var i=0; i<usuarios.length; i++){
                usuario = usuarios[i].split("|");
                if(usuario[3] == this.filtroTipoUsuarios) usuariosFiltrados.push(usuarios[i]);
            }
            
            usuarios = usuariosFiltrados.slice(0);
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(usuarios);
        
        var encabezados = new Array("Nombre", "Perfil");
        var medidas = new Array("45%", "45%");
        var alineaciones = new Array("left","center");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, this.nombre_instancia+".eliminaElemento()", null);
        
        if(this.soloSeleccion == 0) this.getListaSeleccionable().setOpcionBorrar(true);
        else this.getListaSeleccionable().setOpcionBorrar(false);
        
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
    
    this.eliminaElemento = function(){
        //console.log("eliminando elemento");
        //si llega aqui, es por que se confirmó la acción de eliminar el elemento
        var idelemento = this.getListaSeleccionable().getIdElementoEliminado();
        getResultadoAjax("opcion=9&idusuario="+idelemento, this.nombre_instancia+".procesaElementoEliminado", null);
    }
    
    this.procesaElementoEliminado = function(respuesta){
        //console.log("respuesta eliminar: "+respuesta);
        if(respuesta == 1){
            alert("Usuario eliminado!");
            this.getListaSeleccionable().quitaElementoDeListaPorId(this.getListaSeleccionable().getIdElementoEliminado());
        }else{
            alert("No se pudo eliminar en este momento, por favor intenta mas tarde!");
        }
    }
    
    this.elementoSeleccionado = function(){
        //console.log(this.funcionElementoSeleccionado);
        if(this.funcionElementoSeleccionado){
            setTimeout(this.funcionElementoSeleccionado, 0);
            return;
        }
        
        var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        
        var formaUsuario = new FormaUsuario();
        formaUsuario.setIdUsuario(datos[0]);
        this.setFormaCaptura(formaUsuario);
        this.nuevoElemento();
        
        formaUsuario.editaComponente();
    }
}
var FormaUsuario = function(){
    
    this.idUsuario = 0;
    this.setIdUsuario = function(idusuario){
        this.idUsuario = idusuario;
    }
    
    this.selectorClientes = null;
    this.getSelectorClientes = function(){
        if(!this.selectorClientes){
            this.selectorClientes = new SelectorDeOpcion();
            this.selectorClientes.setVisualizacion(1);
        }
        return this.selectorClientes;
    }
    
    this.clienteSeleccionado = 0;
    this.setClienteSeleccionado = function(idcliente){
        this.clienteSeleccionado = idcliente;
    }
    
    this.catalogoClientes = null;
    this.getCatalogoClientes = function(){
        if(!this.catalogoClientes) {
            this.catalogoClientes = new CatalogoClientes();
            this.catalogoClientes.setSoloSeleccion(1);
        }
        return this.catalogoClientes;
    }
    
    this.info = null;
    
    this.muestraComponente = function(){
        var contenido = "<br/>";    
        
        if(this.idUsuario == 0) contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Nuevo Usuario del Sistema</div>";
        else contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Editando Usuario del Sistema</div>";
        
        contenido += "<div class='separador'></div>";
        contenido += "<br/>";    
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_nombre_usuario'>";
        
        contenido += "<div class='texto_forma'>Nombre completo del usuario</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_nombre' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el nombre completo del usuario\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += "</div>";
        
        contenido += '<div class="row center-align">';
        contenido += "<br/>";
        contenido += '<div class="col l6 center-align">';
        
        //selector de perfil de usuario
        contenido += "<div class='texto_forma'>Perfil del Usuario</div><br/>";
        contenido += '<select id="'+this.nombre_instancia+'_selector_perfil_usuario" onchange="'+this.nombre_instancia+'.tipoPerfilSeleccionado();" class="browser-default">'
        contenido += '<option value="0">Selecciona un perfil</option>';
        contenido += '<option value="1">Administrador</option>';
        contenido += '<option value="2">Facturista</option>';
        contenido += '<option value="3">Promotor</option>';
        contenido += '<option value="4">Operaciones</option>';
        contenido += '<option value="5">Cliente</option>';
        contenido += '</select>';
        
        contenido += '</div>';
        contenido += '<div class="col l6 center-align">';
        
        contenido += "<div class='texto_forma'>Nombre de usuario</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_usuario' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el nombre de usuario para acceso al sistema\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += "<div class='ayuda_campos' id='"+this.nombre_instancia+"_a1'></div>";
        contenido += "<br/>";
        
        contenido += '<div class="row center-align">';
        contenido += '<div class="col l6 center-align">';
        
        contenido += "<div class='texto_forma'>Password de usuario</div>";
        contenido += "<input type='password' id='"+this.nombre_instancia+"_campo_password' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el password de usuario para acceso al sistema. Mínimo 8 caracteres.\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '<div class="col l6 center-align">';
        
        contenido += "<div class='texto_forma'>Confirma el password de usuario</div>";
        contenido += "<input type='password' id='"+this.nombre_instancia+"_campo_confirmacion_password' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Confirma el password de usuario para acceso al sistema. Debe coincidir con el primer password.\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        //contenido += "<br/>";
        contenido += "<div class='texto_forma'>Correo</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_correo' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe una dirección de correo electrónico. Debe ser una dirección válida.\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        contenido += "<br/>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_boton_guardar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a></div>";
        
        contenido += "<br/>";
        
        this.contenedor.innerHTML = contenido;
    }
    
    this.tipoPerfilSeleccionado = function(){
        var selector = document.getElementById(this.nombre_instancia+"_selector_perfil_usuario");
        var contenedor = document.getElementById(this.nombre_instancia+"_zona_nombre_usuario");
        if(selector.options[selector.selectedIndex].value == 5){
            //como es un cliente, entonces aparece el selector de cliente para ligarlo con el usuario que se está creando
            this.getSelectorClientes().setComponente(contenedor, this.nombre_instancia+".getSelectorClientes()");
            this.getSelectorClientes().setTextoDeOpcionSeleccionada("Selecciona el cliente ligado a la cuenta de usuario");
            this.getSelectorClientes().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoDeClientes()");

            this.getSelectorClientes().muestraComponente();
        }else{
            var campoNombreUsuario = document.getElementById(this.nombre_instancia+"_campo_nombre");
            if(!campoNombreUsuario){
                var contenido = "";
                contenido += "<div class='texto_forma'>Nombre completo del usuario</div>";
                contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_nombre' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el nombre completo del usuario\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
                contenido += "<br/>";
                
                contenedor.innerHTML = contenido;
                
                this.clienteSeleccionado = 0;
            }
        }
    }
    
    this.editaComponente = function(){
        if(!this.info){
            getResultadoAjax("opcion=8&idusuario="+this.idUsuario, this.nombre_instancia+".procesaInfoUsuario", this.contenedor);
            return;
        }
        
        this.muestraComponente();
        //idUsuario, nombreCompleto, idPerfil, nombreUsuario, passwordUsuario, correoElectronico, estatus, nombreCliente
        
        var selectorPerfil = document.getElementById(this.nombre_instancia+"_selector_perfil_usuario");
        selectorPerfil.selectedIndex = this.info[2];
        this.tipoPerfilSeleccionado();
        
        if(this.info[2] == 5){
            this.clienteSeleccionado = this.info[1];
            this.getSelectorClientes().setTextoDeOpcionSeleccionada(this.info[7]);
        }else{
            document.getElementById(this.nombre_instancia+"_campo_nombre").value = this.info[1];
        }
        
        document.getElementById(this.nombre_instancia+"_campo_usuario").value = this.info[3];
        
        document.getElementById(this.nombre_instancia+"_campo_password").value = this.info[4];
        
        document.getElementById(this.nombre_instancia+"_campo_confirmacion_password").value = this.info[4];
        
        document.getElementById(this.nombre_instancia+"_campo_correo").value = this.info[5];
    }
    
    this.procesaInfoUsuario = function(respuesta){
        //console.log("info usuario: "+respuesta);
        
        this.info = respuesta.split("|");
        this.editaComponente();
    }
    
    this.getZonaBotonGuardar = function(){
        return document.getElementById(this.nombre_instancia+"_zona_boton_guardar");
    }
    
    this.validaForma = function(){
        //debe tener seleccionado un perfil de usuario
        var selectorPerfil = document.getElementById(this.nombre_instancia+"_selector_perfil_usuario");
        if(selectorPerfil.selectedIndex == 0){
            muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar un perfil para el usuario.");
            return;
        }
        
        var campoNombre = "";
        if(selectorPerfil.options[selectorPerfil.selectedIndex].value == 5){
            campoNombre = this.clienteSeleccionado;
        }else{
            campoNombre = document.getElementById(this.nombre_instancia+"_campo_nombre").value;
            if(campoNombre.length == 0 || campoNombre == "" || campoNombre == " "){
                campoNombre.focus();
                return;
            }
        }
        
        var campoUsuario = document.getElementById(this.nombre_instancia+"_campo_usuario");
        if(campoUsuario.value.length == 0 || campoUsuario.value == "" || campoUsuario.value == " "){
            campoUsuario.focus();
            return;
        }
        
        var campoPassword = document.getElementById(this.nombre_instancia+"_campo_password");
        if(campoPassword.value.length == 0 || campoPassword.value == "" || campoPassword.value == " " || campoPassword.value.length < 8){
            campoPassword.focus();
            return;
        }
        
        var campoConfirmaPassword = document.getElementById(this.nombre_instancia+"_campo_confirmacion_password");
        if(campoConfirmaPassword.value.length == 0 || campoConfirmaPassword.value == "" || campoConfirmaPassword.value == " " || campoConfirmaPassword.value != campoPassword.value){
            campoConfirmaPassword.focus();
            return;
        }
        
        var campoCorreo = document.getElementById(this.nombre_instancia+"_campo_correo");
        if(campoCorreo.value.length > 0 && !esUnEmailValido(campoCorreo.value)){
            campoCorreo.focus();
            return;
        }
        
        var info = this.idUsuario+"|"+campoNombre+"|"+selectorPerfil.selectedIndex+"|"+campoUsuario.value+"|"+campoPassword.value+"|"+campoCorreo.value;
        //idusuario, nombrecompleto | idUsuarioLigado, idperfil, campousuario, campopassword, campocorreo
        
        //console.log("info: "+info);
        
        getResultadoAjax("opcion=7&info="+info, this.nombre_instancia+".respuestaUsuarioGuardado", this.getZonaBotonGuardar());
    }
    
    this.respuestaUsuarioGuardado = function(respuesta){
        //console.log("respuesta guardar: "+respuesta);
        this.getZonaBotonGuardar().innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a>";
        
        if(respuesta == -1){
            alert("El nombre de usuario ya está en uso, por favor elige otro");
            var campoUsuario = document.getElementById(this.nombre_instancia+"_campo_usuario");
            campoUsuario.focus();
            return;
        }
        
        if(respuesta > 0 && !isNaN(respuesta)){
            alert("Usuario guardado!");
            if(this.idUsuario == 0) this.idUsuario = respuesta;
        }else{
            alert("No se pudo guardar el usuario, por favor intenta más tarde!");
        }
    }
    
    /*
    METODOS PARA INVOCAR EL CATALOGO DE CLIENTES REGISTRADOS
    */
    this.muestraCatalogoDeClientes = function(){
        this.getCatalogoClientes().setComponente(null, this.nombre_instancia+".getCatalogoClientes()");
        this.getCatalogoClientes().setContenedorFormaFlotante(1);
        this.getCatalogoClientes().muestraContenedorModal('G');
        this.getCatalogoClientes().controlador_navegacion = null;
        
        this.catalogoClientes.setFuncionElementoSeleccionado(this.nombre_instancia+".clienteSeleccionadoDeCatalogo()");
        this.getCatalogoClientes().setComponente(this.getCatalogoClientes().getContenedorModal().getContenedorDeContenido(), this.getCatalogoClientes().nombre_instancia);
        this.getCatalogoClientes().muestraComponente();
    }
    
    this.clienteSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoClientes().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorClientes().setTextoDeOpcionSeleccionada(datos[1]);
            this.clienteSeleccionado = datos[0];
            this.getCatalogoClientes().getContenedorModal().ocultaComponente();
        }
    }
}
var CatalogoBancos = function(){
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Bancos");
        this.setTextoAgregarElemento("Agregar nuevo banco");
        
		this.setFormaCaptura(new FormaBanco());
        
        if(this.soloSeleccion == 0) this.muestraEstructuraCatalogoPrincipalConOpcionAgregarNuevo();
        else this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo();
        
        //realizamos la búsqueda en la base de datos de pacientes
        getResultadoAjax("opcion=10", this.nombre_instancia+".muestraResultados", document.getElementById(this.nombre_instancia+"_zona_boton_buscar"));
        
        //mostramos las opciones de impresion
        if(this.soloSeleccion == 0) this.mostraOpcionesDeImpresion();
    }
    
    this.muestraResultados = function(respuesta){
        var bancos = respuesta.split("^");
        bancos.pop();
        if(bancos.length == 0){
            var contenido = "<div>No hay bancos registrados en el sistema aún</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(bancos);
        
        var encabezados = new Array("Nombre");
        var medidas = new Array("90%");
        var alineaciones = new Array("left");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, this.nombre_instancia+".eliminaElemento()", null);
        
        if(this.soloSeleccion == 0) this.getListaSeleccionable().setOpcionBorrar(true);
        else this.getListaSeleccionable().setOpcionBorrar(false);
        
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
    
    this.eliminaElemento = function(){
        //si llega aqui, es por que se confirmó la acción de eliminar el elemento
        var idelemento = this.getListaSeleccionable().getIdElementoEliminado();
        getResultadoAjax("opcion=13&idbanco="+idelemento, this.nombre_instancia+".procesaElementoEliminado", null);
    }
    
    this.procesaElementoEliminado = function(respuesta){
        //console.log("respuesta eliminar: "+respuesta);
        if(respuesta == 1){
            alert("Banco eliminado!");
            this.getListaSeleccionable().quitaElementoDeListaPorId(this.getListaSeleccionable().getIdElementoEliminado());
        }else{
            alert("No se pudo eliminar en este momento, por favor intenta mas tarde!");
        }
    }
    
    this.elementoSeleccionado = function(){
        //console.log(this.funcionElementoSeleccionado);
        if(this.funcionElementoSeleccionado){
            setTimeout(this.funcionElementoSeleccionado, 0);
            return;
        }
        
        var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        
        var formaBanco = new FormaBanco();
        formaBanco.setIdBanco(datos[0]);
        this.setFormaCaptura(formaBanco);
        this.nuevoElemento();
        
        formaBanco.editaComponente();
    }
}
var FormaBanco = function(){
    
    this.idBanco = 0;
    this.setIdBanco = function(idbanco){
        this.idBanco = idbanco;
    }
    
    this.info = null;
    
    this.muestraComponente = function(){
        var contenido = "<br/>";    
        
        if(this.idBanco == 0) contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Nuevo Banco</div>";
        else contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Editando Banco</div>";
        
        contenido += "<div class='separador'></div>";
        contenido += "<br/>";    
        
        contenido += "<div class='texto_forma'>Nombre completo del banco</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_nombre' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el nombre completo del banco\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        contenido += "<br/>";
        
        contenido += "<div class='ayuda_campos' id='"+this.nombre_instancia+"_a1'></div>";
        contenido += "<br/>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_boton_guardar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a></div>";
        
        contenido += "<br/>";
        
        this.contenedor.innerHTML = contenido;
    }
    
    this.editaComponente = function(){
        if(!this.info){
            getResultadoAjax("opcion=12&idbanco="+this.idBanco, this.nombre_instancia+".procesaInfoRecuperada", this.contenedor);
            return;
        }
        
        this.muestraComponente();
        //idBanco, nombreCompleto, estatus
        
        document.getElementById(this.nombre_instancia+"_campo_nombre").value = this.info[1];
    }
    
    this.procesaInfoRecuperada = function(respuesta){
        //console.log("info: "+respuesta);
        
        this.info = respuesta.split("|");
        this.editaComponente();
    }
    
    this.getZonaBotonGuardar = function(){
        return document.getElementById(this.nombre_instancia+"_zona_boton_guardar");
    }
    
    this.validaForma = function(){
        var campoNombre = document.getElementById(this.nombre_instancia+"_campo_nombre");
        if(campoNombre.value.length == 0 || campoNombre.value == "" || campoNombre.value == " "){
            campoNombre.focus();
            return;
        }
        
        var info = this.idBanco+"|"+campoNombre.value;
        //idusuario, nombrecompleto
        
        //console.log("info: "+info);
        
        getResultadoAjax("opcion=11&info="+info, this.nombre_instancia+".respuestaBancoGuardado", this.getZonaBotonGuardar());
    }
    
    this.respuestaBancoGuardado = function(respuesta){
        //console.log("respuesta guardar: "+respuesta);
        this.getZonaBotonGuardar().innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a>";
        
        if(respuesta > 0 && !isNaN(respuesta)){
            alert("Banco guardado!");
            if(this.idBanco == 0) this.idBanco = respuesta;
        }else{
            alert("No se pudo guardar el banco, por favor intenta más tarde!");
        }
    }
}
var CatalogoEmpresas = function(){
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Empresas");
        this.setTextoAgregarElemento("Agregar nueva empresa");
        
		this.setFormaCaptura(new FormaEmpresa);
        
        if(this.soloSeleccion == 0) this.muestraEstructuraCatalogoPrincipalConOpcionAgregarNuevo();
        else this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo();
        
        //realizamos la búsqueda en la base de datos de pacientes
        getResultadoAjax("opcion=14", this.nombre_instancia+".muestraResultados", document.getElementById(this.nombre_instancia+"_zona_boton_buscar"));
        
        //mostramos las opciones de impresion
        if(this.soloSeleccion == 0) this.mostraOpcionesDeImpresion();
    }
    
    this.muestraResultados = function(respuesta){
        //console.log("empresas: "+respuesta);
        var empresas = respuesta.split("^");
        empresas.pop();
        if(empresas.length == 0){
            var contenido = "<div>No hay empresas registradas en el sistema aún</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(empresas);
        
        var encabezados = new Array("Nombre");
        var medidas = new Array("90%");
        var alineaciones = new Array("left");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, this.nombre_instancia+".eliminaElemento()", null);
        
        if(this.soloSeleccion == 0) this.getListaSeleccionable().setOpcionBorrar(true);
        else this.getListaSeleccionable().setOpcionBorrar(false);
        
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
    
    this.eliminaElemento = function(){
        //si llega aqui, es por que se confirmó la acción de eliminar el elemento
        var idelemento = this.getListaSeleccionable().getIdElementoEliminado();
        getResultadoAjax("opcion=17&idempresa="+idelemento, this.nombre_instancia+".procesaElementoEliminado", null);
    }
    
    this.procesaElementoEliminado = function(respuesta){
        //console.log("respuesta eliminar: "+respuesta);
        if(respuesta == 1){
            alert("Empresa eliminada!");
            this.getListaSeleccionable().quitaElementoDeListaPorId(this.getListaSeleccionable().getIdElementoEliminado());
        }else{
            alert("No se pudo eliminar en este momento, por favor intenta mas tarde!");
        }
    }
    
    this.elementoSeleccionado = function(){
        //console.log(this.funcionElementoSeleccionado);
        if(this.funcionElementoSeleccionado){
            setTimeout(this.funcionElementoSeleccionado, 0);
            return;
        }
        
        var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        
        var formaEmpresa = new FormaEmpresa();
        formaEmpresa.setIdEmpresa(datos[0]);
        this.setFormaCaptura(formaEmpresa);
        this.nuevoElemento();
        
        formaEmpresa.editaComponente();
    }
}
var FormaEmpresa = function(){
    this.idEmpresa = 0;
    this.setIdEmpresa = function(idempresa){
        this.idEmpresa = idempresa;
    }
    
    this.iddatos_facturacion = 0;
    
    this.selector_datos_facturacion = null;
    this.getSelectorDatosFacturacion = function(){
        if(!this.selector_datos_facturacion){
            this.selector_datos_facturacion = new CatalogoSelectorDatosFacturacion();
            this.selector_datos_facturacion.setTipoVisualizacion(1);
        }
        return this.selector_datos_facturacion;
    }
    
    this.info = null;
    this.selector_fecha = null;
    
    this.muestraComponente = function(){
        var contenido = "<br/>";    
        
        if(this.idEmpresa == 0) contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Nueva Empresa</div>";
        else contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Editando Empresa</div>";
        
        contenido += "<div class='separador'></div>";
        contenido += "<br/>";    
        
        contenido += "<div class='texto_forma'>Nombre completo de la empresa</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_nombre' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el nombre completo de la empresa\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        contenido += "<br/>";
        
        contenido += "<br/>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_datos_facturacion'></div>";
        
        contenido += "<br/>";
        
        contenido += "<div class='ayuda_campos' id='"+this.nombre_instancia+"_a1'></div>";
        contenido += "<br/>";
        
        //Número de certificado y fecha de vencimiento
        contenido += '<div class="row center-align">';
        contenido += '<div class="col l6 center-align">';
        
        contenido += "<div class='texto_forma'>No Certificado</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_numero_certificado' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el número de certificado de tu CSD\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '<div class="col l6 center-align">';
        
        contenido += "<div class='texto_forma'>Fecha de Vencimiento</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_fecha_vencimiento'></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += "<br/>";
        contenido += "<div class='texto_forma'>Certificado en formato texto</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_certificado_text' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Ingresa el certificado en formato texto\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        contenido += "<br/>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_boton_guardar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a></div>";
        
        contenido += "<br/>";
        
        this.contenedor.innerHTML = contenido;
        this.getSelectorDatosFacturacion().setComponente(document.getElementById(this.nombre_instancia+"_zona_datos_facturacion"), this.nombre_instancia+".getSelectorDatosFacturacion()");
        this.getSelectorDatosFacturacion().setFuncionDatosFacturacionSeleccionada(this.nombre_instancia+".actualizaDatosFacturacion()");
        if(this.iddatos_facturacion == 0) this.getSelectorDatosFacturacion().muestraComponenteSelector();
        else this.getSelectorDatosFacturacion().setIdDatosFacturacion(this.iddatos_facturacion);
        
        //selector de fecha y hora, por defecto pone los datos actuales
        this.selector_fecha = new SelectorFechas();
        this.selector_fecha.fecha_actual = new Date();
        this.selector_fecha.setSelectorFecha(document.getElementById(this.nombre_instancia+"_zona_fecha_vencimiento"), this.nombre_instancia+".selector_fecha");
        this.selector_fecha.fecha_modificada = true;
        this.selector_fecha.muestraComboFechas();
    }
    
    this.actualizaDatosFacturacion = function(){
        
    }
    
    this.editaComponente = function(){
        if(!this.info){
            getResultadoAjax("opcion=16&idempresa="+this.idEmpresa, this.nombre_instancia+".procesaInfoRecuperada", this.contenedor);
            return;
        }
        
        this.muestraComponente();
        //idEmpresa, nombre, estatus, idDatosFacturacion, noCertificado, fechaVencimiento, textoCertificado
        
        document.getElementById(this.nombre_instancia+"_campo_nombre").value = this.info[1];
        
        this.iddatos_facturacion = this.info[3];
        this.getSelectorDatosFacturacion().setIdDatosFacturacion(this.iddatos_facturacion);
        document.getElementById(this.nombre_instancia+"_numero_certificado").value = this.info[4];
        campoCertificadoText = document.getElementById(this.nombre_instancia+"_certificado_text").value = this.info[6];
        
        if(this.info[5] != " " && this.info[5] != ""){
            var partesFecha = this.info[5].split(" ");
            this.selector_fecha.setFechaDesdeDB(partesFecha[0]);
            this.selector_fecha.fecha_modificada = true;      
            //console.log("fecha modificada!");
        }
    }
    
    this.procesaInfoRecuperada = function(respuesta){
        //console.log("info: "+respuesta);
        
        this.info = respuesta.split("|");
        this.editaComponente();
    }
    
    this.getZonaBotonGuardar = function(){
        return document.getElementById(this.nombre_instancia+"_zona_boton_guardar");
    }
    
    this.validaForma = function(){
        var campoNombre = document.getElementById(this.nombre_instancia+"_campo_nombre");
        if(campoNombre.value.length == 0 || campoNombre.value == "" || campoNombre.value == " "){
            campoNombre.focus();
            return;
        }
        
        //si no tiene datos de facturación, entonces no se puede guardar
        if(this.getSelectorDatosFacturacion().getIdDatosFacturacion() == 0){
            muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar datos de facturación para la empresa!");
            return;
        }
        
        var campoNoCertificado = document.getElementById(this.nombre_instancia+"_numero_certificado");
        if(campoNoCertificado.value.length == 0 || campoNoCertificado.value == "" || campoNoCertificado.value == " "){
            campoNoCertificado.focus();
            return;
        }
        
        var fechaBD = this.selector_fecha.getFechaActualParaDB();
        if(!fechaBD){
            muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar la fecha de vencimiento del certificado!");
            return;
        }
        
        var campoCertificadoText = document.getElementById(this.nombre_instancia+"_certificado_text");
        if(campoCertificadoText.value.length == 0 || campoCertificadoText.value == "" || campoCertificadoText.value == " "){
            campoCertificadoText.focus();
            return;
        }
        
        var info = this.idEmpresa+"|"+campoNombre.value+"|"+this.getSelectorDatosFacturacion().getIdDatosFacturacion()+"|"+campoNoCertificado.value+"|"+fechaBD+"|"+campoCertificadoText.value;
        //idempresa, nombrecompleto, datosFacturacion, noCertificado, fechaDB, certificadoText
        
        //console.log("info: "+info);
        
        getResultadoAjax("opcion=15&info="+info, this.nombre_instancia+".respuestaElementoGuardado", this.getZonaBotonGuardar());
    }
    
    this.respuestaElementoGuardado = function(respuesta){
        //console.log("respuesta guardar: "+respuesta);
        this.getZonaBotonGuardar().innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a>";
        
        if(respuesta > 0 && !isNaN(respuesta)){
            alert("Empresa guardada!");
            if(this.idEmpresa == 0) this.idEmpresa = respuesta;
        }else{
            alert("No se pudo guardar la empresa, por favor intenta más tarde!");
        }
    }
}
var CatalogoClientes = function(){

    this.muestraComponente = function(){
        this.setTituloCatalogo("Clientes");
        this.setTextoAgregarElemento("Agregar nuevo cliente");
        
		this.setFormaCaptura(new FormaCliente);
        
        if(this.soloSeleccion == 0) this.muestraEstructuraCatalogoPrincipalConOpcionAgregarNuevo();
        else this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo();
        
        //realizamos la búsqueda en la base de datos de pacientes
        getResultadoAjax("opcion=18", this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
    }
    
    this.muestraResultados = function(respuesta){
        //console.log("clientes: "+respuesta);
        var clientes = respuesta.split("^");
        clientes.pop();
        if(clientes.length == 0){
            var contenido = "<div>No hay clientes registrados en el sistema aún</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(clientes);
        
        var encabezados = new Array("Nombre", "R.F.C.");
        var medidas = new Array("45%", "45%");
        var alineaciones = new Array("left", "left");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, this.nombre_instancia+".eliminaElemento()", null);
        
        if(this.soloSeleccion == 0) this.getListaSeleccionable().setOpcionBorrar(true);
        else this.getListaSeleccionable().setOpcionBorrar(false);
        
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
        
        //mostramos las opciones de impresion
        if(this.soloSeleccion == 0) this.mostraOpcionesDeImpresion();
    }
    
    this.eliminaElemento = function(){
        //si llega aqui, es por que se confirmó la acción de eliminar el elemento
        var idelemento = this.getListaSeleccionable().getIdElementoEliminado();
        getResultadoAjax("opcion=24&idcliente="+idelemento, this.nombre_instancia+".procesaElementoEliminado", null);
    }
    
    this.procesaElementoEliminado = function(respuesta){
        //console.log("respuesta eliminar: "+respuesta);
        if(respuesta == 1){
            alert("Cliente eliminado!");
            this.getListaSeleccionable().quitaElementoDeListaPorId(this.getListaSeleccionable().getIdElementoEliminado());
        }else{
            alert("No se pudo eliminar en este momento, por favor intenta mas tarde!");
        }
    }
    
    this.elementoSeleccionado = function(){
        
        //console.log(this.funcionElementoSeleccionado);
        if(this.funcionElementoSeleccionado){
            setTimeout(this.funcionElementoSeleccionado, 0);
            return;
        }
        
        var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        
        var formaCliente = new FormaCliente();
        formaCliente.setIdCliente(datos[0]);
        this.setFormaCaptura(formaCliente);
        this.nuevoElemento();
        
        formaCliente.editaComponente();
    }
}
var FormaCliente = function(){
    
    this.idCliente = 0;
    this.setIdCliente = function(idcliente){
        this.idCliente = idcliente;
    }
    
    this.iddatos_facturacion = 0;
    
    this.selector_datos_facturacion = null;
    this.getSelectorDatosFacturacion = function(){
        if(!this.selector_datos_facturacion){
            this.selector_datos_facturacion = new CatalogoSelectorDatosFacturacion();
            this.selector_datos_facturacion.setTipoVisualizacion(1);
        }
        return this.selector_datos_facturacion;
    }
    
    this.info = null;
    
    this.muestraComponente = function(){
        var contenido = "<br/>";    
        
        if(this.idBanco == 0) contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Nuevo Cliente</div>";
        else contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Editando Cliente</div>";
        
        contenido += "<div class='separador'></div>";
        contenido += "<br/>";    
        
        contenido += "<div class='texto_forma'>Nombre completo del cliente</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_nombre' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el nombre completo del cliente\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        contenido += "<br/>";
        
        contenido += "<div class='ayuda_campos' id='"+this.nombre_instancia+"_a1'></div>";
        contenido += "<br/>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_datos_facturacion'></div>";
        
        contenido += "<br/>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_boton_guardar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a></div>";
        
        contenido += "<br/>";
        
        this.contenedor.innerHTML = contenido;
        
        //agregamos datos de facturacion
        this.getSelectorDatosFacturacion().setComponente(document.getElementById(this.nombre_instancia+"_zona_datos_facturacion"), this.nombre_instancia+".getSelectorDatosFacturacion()");
        this.getSelectorDatosFacturacion().setFuncionDatosFacturacionSeleccionada(this.nombre_instancia+".actualizaDatosFacturacion()");
        //console.log("iddatos facturacion: "+this.iddatos_facturacion);
        if(this.iddatos_facturacion == 0) this.getSelectorDatosFacturacion().muestraComponenteSelector();
        else this.getSelectorDatosFacturacion().setIdDatosFacturacion(this.iddatos_facturacion);
    }
    
    this.actualizaDatosFacturacion = function(){
        
    }
    
    this.editaComponente = function(){
        if(!this.info){
            getResultadoAjax("opcion=23&idcliente="+this.idCliente, this.nombre_instancia+".procesaInfoRecuperada", this.contenedor);
            return;
        }
        this.iddatos_facturacion = this.info[2];
        this.muestraComponente();
        //idBanco, nombreCompleto, estatus
        
        document.getElementById(this.nombre_instancia+"_campo_nombre").value = this.info[1];
        
    }
    
    this.procesaInfoRecuperada = function(respuesta){
        //console.log("info: "+respuesta);
        
        this.info = respuesta.split("|");
        this.editaComponente();
    }
    
    this.getZonaBotonGuardar = function(){
        return document.getElementById(this.nombre_instancia+"_zona_boton_guardar");
    }
    
    this.validaForma = function(){
        var campoNombre = document.getElementById(this.nombre_instancia+"_campo_nombre");
        if(campoNombre.value.length == 0 || campoNombre.value == "" || campoNombre.value == " "){
            campoNombre.focus();
            return;
        }
        
        var info = this.idCliente+"|"+campoNombre.value+"|"+this.getSelectorDatosFacturacion().getIdDatosFacturacion();
        //idusuario, nombrecompleto
        
        //console.log("info: "+info);
        
        getResultadoAjax("opcion=19&info="+info, this.nombre_instancia+".respuestaElementoGuardado", this.getZonaBotonGuardar());
    }
    
    this.respuestaElementoGuardado = function(respuesta){
        //console.log("respuesta guardar: "+respuesta);
        this.getZonaBotonGuardar().innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a>";
        
        if(respuesta > 0 && !isNaN(respuesta)){
            alert("Cliente guardado!");
            if(this.idCliente == 0) this.idCliente = respuesta;
        }else{
            alert("No se pudo guardar el cliente, por favor intenta más tarde!");
        }
    }
}
var CatalogoSelectorDatosFacturacion = function(){
    
    this.iddatos_facturacion = 0;
    this.getIdDatosFacturacion = function(){
        return this.iddatos_facturacion;
    }
    
    this.tipoVisualizacion = 0;
    //el tipo de visualizacion 0 indica que es un componente que muestra su controlador de navegacion para mostrar el catálogo de datos de facturacion. El tipo de visualización 1 indica que el catalogo de datos de facturacion se mostrará en un componente flotante.
    
    this.setTipoVisualizacion = function(tipo){
        this.tipoVisualizacion = tipo;
    }
    
    this.forma_datos_facturacion = null;
    
    this.setIdDatosFacturacionParaSoloLectura = function(id){
        this.iddatos_facturacion = id;
        
        this.forma_datos_facturacion = new FormaDatosFacturacion();
        this.forma_datos_facturacion.setComponente(this.contenedor, this.nombre_instancia+".forma_datos_facturacion");
        this.forma_datos_facturacion.setIdDatosCaptura(this.iddatos_facturacion);
        this.forma_datos_facturacion.muestraInfoParaSoloLectura();
    }
    
    this.setIdDatosFacturacion = function(id){
        this.iddatos_facturacion = id;
        
        if(!this.lista_datos_facturacion){
            getResultadoAjax("opcion=20", this.nombre_instancia+".procesaDatosFacturacionRecuperadosParaMostrarSeleccionado", this.contenedor);
            return;
        }else this.muestraDatosDeFacturacionSeleccionados();
    }
    
    //esta función deshabilita el selector de datos de facturación, para no permitir cambiar el que ya esta seleccionado
    this.funcion = null;
    this.setFuncionSelectorPresionado = function(funcion){
        this.funcion = funcion;
    }
    
    //función que se invocará cuando los datos de facturación cambien
    this.funcion_datos_facturacion_seleccionada = null;
    this.setFuncionDatosFacturacionSeleccionada = function(funcion){
        this.funcion_datos_facturacion_seleccionada = funcion;
    }
    
    this.procesaDatosFacturacionRecuperadosParaMostrarSeleccionado = function(respuesta){
        this.lista_datos_facturacion = respuesta.split("^");
        this.lista_datos_facturacion.pop();
        
        this.muestraDatosDeFacturacionSeleccionados();
    }
    
    this.muestraDatosDeFacturacionSeleccionados = function(){
        var datos;
        for(var i=0; i<this.lista_datos_facturacion.length; i++){
            datos = this.lista_datos_facturacion[i].split("|");
            if(datos[0] == this.iddatos_facturacion){
                //this.getSelectorDeOpcionPrincipal().setFuncionSelectorPresionado(this.funcion);
                this.muestraComponenteSelector();
                this.setTextoParaMostrar(datos[1]+" - "+datos[2]);
                return;
            }
        }
        this.funcion = null;
        this.muestraComponenteSelector();
    }
    
    this.selector_opcion_principal = null;
    this.getSelectorDeOpcionPrincipal = function(){
        if(!this.selector_opcion_principal){
            this.selector_opcion_principal = new SelectorDeOpcion();
            this.selector_opcion_principal.setVisualizacion(2);
        }
        return this.selector_opcion_principal;
    }
    
    this.setTextoParaMostrar = function(texto){
        this.getSelectorDeOpcionPrincipal().setTextoDeOpcionSeleccionada(texto);
        this.getSelectorDeOpcionPrincipal().muestraComponente();
    }
    
    this.lista_datos_facturacion = null;
    
    this.contenedor_anterior = null;
    
    this.muestraComponenteSelector = function(){
        if(this.contenedor_anterior) this.contenedor = this.contenedor_anterior;
        
        this.getSelectorDeOpcionPrincipal().setComponente(this.contenedor, this.nombre_instancia+".getSelectorDeOpcionPrincipal()");
        this.getSelectorDeOpcionPrincipal().setTextoDeOpcionSeleccionada("Selecciona datos de facturaci&oacute;n");
        if(this.funcion) this.getSelectorDeOpcionPrincipal().setFuncionSelectorPresionado(this.funcion);
        else this.getSelectorDeOpcionPrincipal().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoDeDatosDeFacturacion()");
       
       if(this.iddatos_facturacion != 0) this.getSelectorDeOpcionPrincipal().setFuncionOpcionSelectorPresionado(this.nombre_instancia+".opcionSelectorPresionada("+this.iddatos_facturacion+")");
        this.getSelectorDeOpcionPrincipal().muestraComponente();
    }
    
    this.muestraComponente = function(){
        if(this.contenedor_anterior) this.contenedor = this.contenedor_anterior;
        this.muestraCatalogoDeDatosDeFacturacion();
    }
    
    this.catalogoCancelado = function(){
        if(this.iddatos_facturacion != 0) this.muestraDatosDeFacturacionSeleccionados();
    }
    
    this.procesaDatosFacturacionRecuperados = function(respuesta){
        //console.log("datos facturacion: "+respuesta);
        this.lista_datos_facturacion = respuesta.split("^");
        this.lista_datos_facturacion.pop();
        this.muestraCatalogoDeDatosDeFacturacion();
    }
    
    this.muestraCatalogoDeDatosDeFacturacion = function(){
        if(!this.lista_datos_facturacion){
            getResultadoAjax("opcion=20", this.nombre_instancia+".procesaDatosFacturacionRecuperados", null);
            return;
        }
        
        this.contenedor_anterior = this.contenedor;
        
        if(this.tipoVisualizacion == 0){
            this.getControladorNavegacion().setControladorNavegacion(this.contenedor, null, null, this.nombre_instancia+".catalogoCancelado();", this.nombre_instancia+".getControladorNavegacion()");
            this.getControladorNavegacion().muestraControladorNavegacionMedio();

            this.setComponente(this.getControladorNavegacion().getContenedorDeContenido(), this.nombre_instancia);   
        }else{
            this.setContenedorFormaFlotante(1);
            this.muestraContenedorModal('G');
            this.controlador_navegacion = null;
            this.setComponente(this.getContenedorModal().getContenedorDeContenido(), this.nombre_instancia);
        }
        
        this.setTextoAgregarElemento("Agregar nuevo registro de facturación");
        
        //iddatosfacturacion, nombre, rfc, direccion
        this.setTituloCatalogo("Datos de Facturaci&oacute;n");
        
        var datos_facturacion = new FormaDatosFacturacion();
        datos_facturacion.setComponentePadre(this);
        
		this.setFormaCaptura(datos_facturacion);
        this.setContenedorFormaFlotante(0);
        //this.setTamanioComponenteModal("s");
        //this.setTipoTablaElementos(false);
		this.muestraEstructuraCatalogoPrincipalConOpcionAgregarNuevo();
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(this.lista_datos_facturacion);
        
        var encabezados = new Array("Razon social", "R.F.C.");
        var medidas = new Array("45%","40%");
        var alineaciones = new Array("left","left");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 1, 1, this.nombre_instancia+".datosFacturacionSeleccionados()", this.nombre_instancia+".muestraDatosDeFacturacionParaEdicion()", this.nombre_instancia+".eliminaDatosFacturacion()", null);
        
        //this.getListaSeleccionable().setOpcionImpresora(0);
        this.getListaSeleccionable().setFuncionDetalleElementoSeleccionado(this.nombre_instancia+".muestraDatosDeFacturacionParaEdicion()");
        this.muestraElementosDelCatalogo();
    }
    
    this.opcionSelectorPresionada = function(iddatos){
        this.muestraCatalogoDeDatosDeFacturacion();
        this.muestraDatosDeFacturacionParaEdicion(this.iddatos_facturacion);
    }
    
    this.muestraDatosDeFacturacionParaEdicion = function(iddatos){
        if(!iddatos || iddatos == 0){
            var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
            iddatos = datos[0];
        }
        
        this.getSelectorDeOpcion().getBotonAgregar().click();
        this.getFormaCaptura().setIdDatosCaptura(iddatos);
        this.getFormaCaptura().editaInfo();
    }
    
    this.eliminaDatosFacturacion = function(){
        var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        getResultadoAjax("opcion=200&iddatos_facturacion="+datos[0], this.nombre_instancia+".procesaDatosDeFacturacionEliminados", null);
    }
    
    this.procesaDatosDeFacturacionEliminados = function(respuesta){
        ////console.log("datos eliminados: "+respuesta);
        if(!isNaN(respuesta) && respuesta == 1){
            this.getListaSeleccionable().quitaElementoDeListaPorPosicion(this.getListaSeleccionable().getPosicionElementoSeleccionado());
            this.lista_datos_facturacion = null;
        }else{
            alert("No se pudo eliminar el registro, intenta mas tarde!");
        }
    }
    
    this.datosFacturacionSeleccionados = function(){
        //iddatosfacturacion, razon social, rfc, direccion
        var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        
        this.iddatos_facturacion = datos[0];
        this.muestraComponenteSelector();
        this.setTextoParaMostrar(datos[1]+" - "+datos[2]);
        
        //como ya se seleccionó un dato de facturación, entonces debemos cerrar la ventana flotante en caso de que se esté mostrando como componente flotante
        if(this.tipoVisualizacion == 1){
            this.getContenedorModal().ocultaComponente();
        }
        
        //invocamos la función dada para notificar cuando se cambió el recepctor de la factura
        if(this.funcion_datos_facturacion_seleccionada) setTimeout(this.funcion_datos_facturacion_seleccionada, 0);
    }
    
    this.datosFacturacionAgregados = function(info){
        this.lista_datos_facturacion = null;
        var datos = info.split("|");
        
        this.iddatos_facturacion = datos[0];
        this.muestraComponente();
        this.setTextoParaMostrar(datos[1]+" - "+datos[2]);
        
        //si se trata de un componente flotante, debemos cerrarlo
        //como ya se seleccionó un dato de facturación, entonces debemos cerrar la ventana flotante en caso de que se esté mostrando como componente flotante
        if(this.tipoVisualizacion == 1){
            this.getContenedorModal().ocultaComponente();
        }
        
        //invocamos la función dada para notificar cuando se cambió el recepctor de la factura
        if(this.funcion_datos_facturacion_seleccionada) setTimeout(this.funcion_datos_facturacion_seleccionada, 0);
    }

}
var FormaDatosFacturacion = function(){

    this.datos = null;
    this.iddatos_facturacion = 0;
    
    this.selectorRegimenFiscal = null;
    this.getSelectorRegimenFiscal = function(){
        if(!this.selectorRegimenFiscal){
            this.selectorRegimenFiscal = new SelectorDeOpcion();
            this.selectorRegimenFiscal.setVisualizacion(1);
        }
        return this.selectorRegimenFiscal;
    }
    
    this.idRegimenFiscal = 0;
    
    this.catalogoRegimenFiscal = null;
    this.getCatalogoRegimenFiscal = function(){
        if(!this.catalogoRegimenFiscal) this.catalogoRegimenFiscal = new CatalogoRegimenFiscal();
        return this.catalogoRegimenFiscal;
    }
    
    this.setIdDatosCaptura = function(iddatos){
        this.iddatos_facturacion = iddatos;
    }
    
    this.componente_padre = null;
    this.setComponentePadre = function(componente){
        this.componente_padre = componente;
    }
    
    this.procesaDatosFacturacionRecuperados = function(respuesta){
        //console.log("respuesta datos recuperados: "+respuesta);
        
        this.datos = respuesta.split("|");
        this.editaInfo();
    }
    
    this.procesaDatosFacturacionRecuperadosParaSoloLectura = function(respuesta){
        ////console.log("respuesta datos recuperados: "+respuesta);
        
        this.datos = respuesta.split("|");
        this.muestraInfoParaSoloLectura();
    }
    
    this.editaInfo = function(){
        if(!this.datos){
            getResultadoAjax("opcion=22&idDatosFacturacion="+this.iddatos_facturacion, this.nombre_instancia+".procesaDatosFacturacionRecuperados", null);
            return;
        }
        
        //iddatosfacturacion, nombre, rfc, calle, exterior, interior, referencia, colonia, localidad, municipio, estado, pais, cp, telefono, email, fax, cuenta, estatus, claveRegimen, descripcionClave
        
        this.idRegimenFiscal = this.datos[18];
        this.getSelectorRegimenFiscal().setTextoDeOpcionSeleccionada(this.datos[18]+" - "+this.datos[19]);
        this.getSelectorRegimenFiscal().muestraComponente();
        
        //seteamos los datos recuperados en la forma de facturacion
        document.getElementById(this.nombre_instancia+"_campo_nombre").value = this.datos[1];
        
        document.getElementById(this.nombre_instancia+"_campo_rfc").value = this.datos[2];
        
        document.getElementById(this.nombre_instancia+"_calle").value = this.datos[3];
        
        document.getElementById(this.nombre_instancia+"_exterior").value = this.datos[4];
        
        document.getElementById(this.nombre_instancia+"_interior").value = this.datos[5];
        
        document.getElementById(this.nombre_instancia+"_campo_referencia").value = this.datos[6];
        
        document.getElementById(this.nombre_instancia+"_campo_colonia").value = this.datos[7];
        
        document.getElementById(this.nombre_instancia+"_campo_localidad").value = this.datos[8];
        
        document.getElementById(this.nombre_instancia+"_campo_municipio").value = this.datos[9];
        
        document.getElementById(this.nombre_instancia+"_campo_estado").value = this.datos[10];
        
        document.getElementById(this.nombre_instancia+"_campo_pais").value = this.datos[11];
        
        document.getElementById(this.nombre_instancia+"_campo_cp").value = this.datos[12];
        
        document.getElementById(this.nombre_instancia+"_campo_telefono").value = this.datos[13];
        
        document.getElementById(this.nombre_instancia+"_campo_email").value = this.datos[14];
        
        document.getElementById(this.nombre_instancia+"_campo_fax").value = this.datos[15];
        
        document.getElementById(this.nombre_instancia+"_campo_cuenta").value = this.datos[16];
    }
    
    this.muestraInfoParaSoloLectura = function(){
        if(!this.datos){
            getResultadoAjax("opcion=199&iddatos_facturacion="+this.iddatos_facturacion, this.nombre_instancia+".procesaDatosFacturacionRecuperadosParaSoloLectura", null);
            return;
        }
        
        //iddatosfacturacion, nombre, rfc, calle, exterior, interior, referencia, colonia, localidad, municipio, estado, pais, cp, telefono, email, fax, estatus
        
        //seteamos los datos recuperados en la forma de facturacion
        /*var contenido = "Nombre: <b>"+this.datos[1]+"</b><br/>";
        contenido += "R.F.C.: <b>"+this.datos[2]+"</b><br/>";
        contenido += "Calle <b>"+this.datos[2]+"</b> No. <b>"+this.datos[4]+"</b> Int. <b>"+this.datos[5]+"</b> Colonia: <b>"+this.datos[7]+"</b> Localidad: <b>"+this.datos[8]+"</b> Mpio: <b>"+this.datos[9]+"</b> Estado <b>"+this.datos[10]+"</b> C.P. <b>"+this.datos[12]+"</b><br/>";
        contenido += "Tel&eacute;fono: <b>"+this.datos[13]+"</b><br/>";
        contenido += "e-mail: <b>"+this.datos[14]+"</b>";*/
        
        var contenido = "<b>"+this.datos[1]+"<br/>";
        contenido += ""+this.datos[2]+"<br/>";
        contenido += ""+this.datos[3]+" "+this.datos[4]+" "+this.datos[5]+" "+this.datos[7]+" "+this.datos[8]+" "+this.datos[9]+" "+this.datos[10]+" "+this.datos[12]+"<br/>";
        contenido += ""+this.datos[13]+"<br/>";
        contenido += ""+this.datos[14]+"</b>";
        
        this.contenedor.innerHTML = contenido;
    }
    
    this.getDatos = function(){
        return this.datos;
    }
    
    this.muestraComponente = function(){
        var contenido = "<div><br/>";
        
        if(this.iddatos_facturacion == 0) contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_subtitulo_componente'>Nuevos Datos de Facturacion</div>";
        else contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_subtitulo_componente'>Editando Datos de Facturacion</div>";
        
        contenido += "<table align='center' cellpadding='0' cellspacing='0' width='100%' border='0'>";
        contenido += "<tr>";
        contenido += "<td align='center' valign='top' width='100%'>";
        
        contenido += "<br/>";
        contenido += "<div class='texto_forma'>Régimen Fiscal <span class='azul'>*</span></div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_selector_regimen_fiscal'></div>";
        contenido += "<br/>";
        
        contenido += "</td>";
        contenido += "</tr>";
        contenido += "<tr>";
        contenido += "<td align='center' valign='top' width='100%'>";
        
        contenido += "<div class='texto_forma'>Nombre o raz&oacute;n social <span class='azul'>*</span></div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_nombre' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe el nombre o razon social para facturacion\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        
        contenido += "</td>";
        contenido += "</tr>";
        contenido += "</table>";
        
        
        contenido += "<table align='center' cellpadding='3' cellspacing='3' width='100%' border='0'>";
        contenido += "<tr>";
        contenido += "<td align='center' valign='top' width='50%'>";
        
        contenido += "<div class='texto_forma'>RFC <span class='azul'>*</span></div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_rfc' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"R.F.C. para facturacion\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        
        contenido += "</td>";
        contenido += "<td align='center' valign='top' width='50%'>";
        
        contenido += "<div id='"+this.nombre_instancia+"_label_calle' class='texto_forma'>Calle <span class='azul'>*</span>";
        contenido += "</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_calle' value='' class='campo_forma' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe la calle\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        
        contenido += "</td>";
        contenido += "</tr></table>";
        
        //Calle, no ext, no int
        contenido += "<table id='"+this.nombre_instancia+"_tabla_calle_ext_int' align='center' width='100%' cellpadding='3' cellspacing='3' border='0' class=''>";
        contenido += "<tr>";
        contenido += "<td align='center' width='25%'>";

        contenido += "<div id='"+this.nombre_instancia+"_label_exterior' class='texto_forma'>No. Ext. <span class='azul'>*</span>";
        contenido += "</div>";

        contenido += "<input type='text' id='"+this.nombre_instancia+"_exterior' value='' class='campo_forma' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe el numero exterior\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        contenido += "";

        contenido += "</td>";
        contenido += "<td align='center' width='25%'>";

        contenido += "<div id='"+this.nombre_instancia+"_label_interior' class='texto_forma'>No. Int.";
        contenido += "</div>";

        contenido += "<input type='text' id='"+this.nombre_instancia+"_interior' value='' class='campo_forma' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe el numero interior si hay\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        contenido += "";

        contenido += "</td>";
        contenido += "<td align='center' width='50%'>";

        contenido += "<div class='texto_forma'>Colonia <span class='azul'>*</span></div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_colonia' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe la colonia\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";

        contenido += "</td>";
        contenido += "</tr>";
        contenido += "</table>";
        
        //referencia, colonia
        contenido += "<table align='center' cellpadding='3' cellspacing='3' width='100%' border='0'>";
        contenido += "<tr>";
        contenido += "<td align='center' valign='top' width='50%'>";
        
        contenido += "<div class='texto_forma'>Referencia </div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_referencia' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe una referencia\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        
        contenido += "</td>";
        contenido += "<td align='center' valign='top' width='50%'>";
        
        contenido += "<div class='texto_forma'>Localidad <span class='azul'>*</span></div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_localidad' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe una localidad\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        
        contenido += "</td>";
        contenido += "</tr></table>";
        
        contenido += "<div id='"+this.nombre_instancia+"_u1' class='ayuda_campos'></div>";
        
        //localidad, municipio
        contenido += "<table align='center' cellpadding='3' cellspacing='3' width='100%' border='0'>";
        contenido += "<tr>";
        contenido += "<td align='center' valign='top' width='50%'>";
        
        contenido += "<div class='texto_forma'>Municipio <span class='azul'>*</span></div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_municipio' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe un municipio\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        
        contenido += "</td>";
        contenido += "<td align='center' valign='top' width='50%'>";
        
        contenido += "<div class='texto_forma'>Estado <span class='azul'>*</span></div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_estado' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe un estado\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        
        contenido += "</td>";
        contenido += "</tr></table>";
        
        //estado, pais
        contenido += "<table align='center' cellpadding='3' cellspacing='3' width='100%' border='0'>";
        contenido += "<tr>";
        contenido += "<td align='center' valign='top' width='50%'>";
        
        contenido += "<div class='texto_forma'>Pa&iacute;s <span class='azul'>*</span></div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_pais' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe un pais\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += "</td>";
        contenido += "<td align='center' valign='top' width='50%'>";
        
        contenido += "<div class='texto_forma'>C.P. <span class='azul'>*</span></div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_cp' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe el codigo postal\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        
        contenido += "</td>";
        contenido += "</tr></table>";
        
        //cp, telefono
        contenido += "<table align='center' cellpadding='3' cellspacing='3' width='100%' border='0'>";
        contenido += "<tr>";
        contenido += "<td align='center' valign='top' width='50%'>";
        
        contenido += "<div class='texto_forma'>Tel&eacute;fono </div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_telefono' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe un numero de telefono\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        
        contenido += "</td>";
        contenido += "<td align='center' valign='top' width='50%'>";
        
        contenido += "<div class='texto_forma'>e-mail</div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_email' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe una direccion de correo electronico. Aquí se enviará el cdfi generado\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        
        contenido += "</td>";
        contenido += "</tr></table>";
        
        //email fax
        contenido += "<table align='center' cellpadding='3' cellspacing='3' width='100%' border='0'>";
        contenido += "<tr>";
        contenido += "<td align='center' valign='top' width='50%'>";
        
        contenido += "<div class='texto_forma'>Fax</div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_fax' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe un numero de telefono para envio de fax\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        
        contenido += "</td>";
        contenido += "<td align='center' valign='top' width='50%'>";
        
        contenido += "<div class='texto_forma'>No. de Cuenta</div>";
        contenido += "<input type='text' class='campo_forma' id='"+this.nombre_instancia+"_campo_cuenta' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_u1\", \"Escribe el número de cuenta de pago\")' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_u1\")'/>";
        
        contenido += "</td>";
        contenido += "</tr></table>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_guardar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a></div>";
        
        contenido += "<br/></div><br/>";
        
        this.contenedor.innerHTML = contenido;
        
        this.getSelectorRegimenFiscal().setComponente(document.getElementById(this.nombre_instancia+"_zona_selector_regimen_fiscal"), this.nombre_instancia+".getSelectorRegimenFiscal()");
        
        if(this.idRegimenFiscal == 0) this.getSelectorRegimenFiscal().setTextoDeOpcionSeleccionada("Selecciona Regimen Fiscal");
        this.getSelectorRegimenFiscal().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoRegimenFiscal()");
        
        this.getSelectorRegimenFiscal().muestraComponente();
    }
    
    this.muestraCatalogoRegimenFiscal = function(){
        this.getCatalogoRegimenFiscal().setComponente(document.getElementById(this.nombre_instancia+"_zona_selector_regimen_fiscal"), this.nombre_instancia+".getCatalogoRegimenFiscal()");
        this.getCatalogoRegimenFiscal().setFuncionElementoSeleccionado(this.nombre_instancia+".regimenSeleccionado()");
        this.getCatalogoRegimenFiscal().muestraComponente();
    }
    
    this.regimenSeleccionado = function(){
        var datos = this.getCatalogoRegimenFiscal().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorRegimenFiscal().setTextoDeOpcionSeleccionada(datos[1]+" - "+datos[2]);
            this.idRegimenFiscal = datos[1];
            this.getSelectorRegimenFiscal().muestraComponente();
        }
    }
    
    this.validaForma = function(){
        if(this.idRegimenFiscal == 0){
            alert("Debes seleccionar el régimen fiscal!");
            return;
        }
        
        var nombre = document.getElementById(this.nombre_instancia+"_campo_nombre");
        if(nombre.value == " " || nombre.value == ""){
            nombre.focus();
            return false;
        }
        
        var rfc = document.getElementById(this.nombre_instancia+"_campo_rfc");
        if(rfc.value == " " || rfc.value == ""){
            rfc.focus();
            return false;
        }
        
        var calle = document.getElementById(this.nombre_instancia+"_calle");
        if(calle.value == " " || calle.value == ""){
            calle.focus();
            return false;
        }
        
        var exterior = document.getElementById(this.nombre_instancia+"_exterior");
        if(exterior.value == " " || exterior.value == ""){
            exterior.focus();
            return false;
        }
        
        var interior = document.getElementById(this.nombre_instancia+"_interior");
        
        var referencia = document.getElementById(this.nombre_instancia+"_campo_referencia");
        
        var colonia = document.getElementById(this.nombre_instancia+"_campo_colonia");
        if(colonia.value == " " || colonia.value == ""){
            colonia.focus();
            return false;
        }
        
        var localidad = document.getElementById(this.nombre_instancia+"_campo_localidad");
        if(localidad.value == " " || localidad.value == ""){
            localidad.focus();
            return false;
        }
        
        var municipio = document.getElementById(this.nombre_instancia+"_campo_municipio");
        if(municipio.value == " " || municipio.value == ""){
            municipio.focus();
            return false;
        }
        
        var estado = document.getElementById(this.nombre_instancia+"_campo_estado");
        if(estado.value == " " || estado.value == ""){
            estado.focus();
            return false;
        }
        
        var pais = document.getElementById(this.nombre_instancia+"_campo_pais");
        if(pais.value == " " || pais.value == ""){
            pais.focus();
            return false;
        }
        
        var cp = document.getElementById(this.nombre_instancia+"_campo_cp");
        if(cp.value == " " || cp.value == ""){
            cp.focus();
            return false;
        }
        
        var telefono = document.getElementById(this.nombre_instancia+"_campo_telefono");
        //calle, exterior, interior, campo_referencia, campo_colonia, campo_localidad, campo_municipio, campo_estado, campo_pais, campo_cp, campo_telefono, campo_email, campo_fax
        var email = document.getElementById(this.nombre_instancia+"_campo_email");
        
        var fax = document.getElementById(this.nombre_instancia+"_campo_fax");
        
        var cuenta = document.getElementById(this.nombre_instancia+"_campo_cuenta");
        
        this.datos = this.iddatos_facturacion+"|"+nombre.value+"|"+rfc.value+"|"+calle.value+"|"+exterior.value+"|"+interior.value+"|"+referencia.value+"|"+colonia.value+"|"+localidad.value+"|"+municipio.value+"|"+estado.value+"|"+pais.value+"|"+cp.value+"|"+telefono.value+"|"+email.value+"|"+fax.value+"|"+cuenta.value+"|"+this.idRegimenFiscal;
        //iddatos_facturacion, nombre, rfc, calle, exterior, interior, referencia, colonia, localidad, municipio, estado, pais, cp, telefono, email, fax, cuenta, claveRegimenFiscal
        ////console.log("datos: "+this.datos);
        
        getResultadoAjax("opcion=21&info="+this.datos, this.nombre_instancia+".procesaRespuesta", document.getElementById(this.nombre_instancia+"_zona_guardar"));
    }
    
    this.procesaRespuesta = function(respuesta){
        //console.log("respuesta guardar datos facturacion: "+respuesta);
        
        if(!isNaN(respuesta) && respuesta > 0){
            var datos = this.datos.split("|");
            if(this.iddatos_facturacion == 0) this.datos = respuesta+"|"+datos[1]+"|"+datos[2]+"|"+datos[3];
            
            alert("Datos de facturacion guardados!");
            if(this.componente_padre) this.componente_padre.datosFacturacionAgregados(this.datos);
            else{
                this.muestraComponente();
                this.datos = null;
                this.editaInfo();
            }
        }else{
            alert("No se pudo guardar los datos de facturacion, intenta mas tarde!");
            document.getElementById(this.nombre_instancia+"_zona_guardar").innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a>";
        }
    }
}
var SelectorRangoDeFecha = function(){
    
    this.fecha_inicial = null;
    this.getFechaInicial = function(){
        if(!this.fecha_inicial) this.fecha_inicial = new SelectorFechas();
        return this.fecha_inicial;
    }
    
    this.fecha_final = null;
    this.getFechaFinal = function(){
        if(!this.fecha_final) this.fecha_final = new SelectorFechas();
        return this.fecha_final;
    }
    
    this.hora_inicial = null;
    this.getHoraInicial = function(){
        if(!this.hora_inicial) this.hora_inicial = new SelectorHora();
        return this.hora_inicial;
    }
    
    this.hora_final = null;
    this.getHoraFinal = function(){
        if(!this.hora_final) this.hora_final = new SelectorHora();
        return this.hora_final;
    }
    
    this.funcion_cambia_rango = null;
    this.setFuncionCambiaRango = function(funcion){
        this.funcion_cambia_rango = funcion;
    }
    
    this.contenedor = null;
    this.nombre_instancia = "selector_rango_fechas";
    
    this.setComponente = function(contenedor, nombre){
        this.contenedor = contenedor;
        this.nombre_instancia = nombre;
    }
    
    //tipos de visualizacion
    //1 del primero al día actual del mes
    //2 el dia actual
    //3 fechas inicial y final considerando la quincena actual
    this.tipo_visualizacion = 1;
    this.setTipoVisualizacion = function(tipo_visualizacion){
        this.tipo_visualizacion = tipo_visualizacion;
    }
    
    //tipo de rango de fecha
    //1 rango solo de fechas
    //2 rango de fechas y horas
    this.tipo_selector = 1;
    this.setTipoDeSelector = function(tipo){
        this.tipo_selector = tipo;
    }
    
    this.getFechaInicialPorTipoDeVisualizacion = function(){
        if(this.tipo_visualizacion == 3){
            var fecha_actual = new Date();
            if(fecha_actual.getDate() > 15) return fecha_actual.getFullYear()+"-"+((fecha_actual.getMonth()+1 < 10)?"0"+(fecha_actual.getMonth()+1):(fecha_actual.getMonth()+1))+"-16";
            else return fecha_actual.getFullYear()+"-"+((fecha_actual.getMonth()+1 < 10)?"0"+(fecha_actual.getMonth()+1):(fecha_actual.getMonth()+1))+"-01";
        }
    }
    
    this.getFechaFinalPorTipoDeVisualizacion = function(){
        if(this.tipo_visualizacion == 3){
            var fecha_actual = new Date();
            if(fecha_actual.getDate() > 15) return fecha_actual.getFullYear()+"-"+((fecha_actual.getMonth()+1 < 10)?"0"+(fecha_actual.getMonth()+1):(fecha_actual.getMonth()+1))+"-"+getMaximosDiasPorMes((fecha_actual.getMonth()+1), fecha_actual.getFullYear());
            else return fecha_actual.getFullYear()+"-"+((fecha_actual.getMonth()+1 < 10)?"0"+(fecha_actual.getMonth()+1):(fecha_actual.getMonth()+1))+"-15";
        }
    }
    
    this.muestraComponente = function(){
        if(this.tipo_selector == 1){
            var contenido = "<table align='center' width='96%' cellpadding='6' cellspacing='0'>";
            contenido += "<tr>";
            contenido += "<td align='center' width='50%' valign='top'>";

            contenido += "<div class='texto_campo'>Fecha Inicial <span class='azul'>*</span></div>";

            contenido += "</td>";
            contenido += "<td align='center' width='50%' valign='top'>";

            contenido += "<div class='texto_campo'>Fecha Final <span class='azul'>*</span></div>";

            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr>";
            contenido += "<td align='center' width='50%' valign='top'>";

            contenido += "<div id='"+this.nombre_instancia+"_zona_fecha_inicial'></div>";

            contenido += "</td>";
            contenido += "<td align='center' width='50%' valign='top'>";

            contenido += "<div id='"+this.nombre_instancia+"_zona_fecha_final'></div>";

            contenido += "</td>";
            contenido += "</tr>";
            contenido += "</table>";

            this.contenedor.innerHTML = contenido;

            var fecha_actual = new Date();
             this.getFechaInicial().setComponente(document.getElementById(this.nombre_instancia+"_zona_fecha_inicial"), this.nombre_instancia+".getFechaInicial()");
            this.getFechaInicial().funcionOnChange(this.nombre_instancia+".cambiaRangoDeFecha()");

            if(this.tipo_visualizacion == 1) this.getFechaInicial().setFechaDesdeDB(fecha_actual.getFullYear()+"-"+((fecha_actual.getMonth()+1 < 10)?"0"+(fecha_actual.getMonth()+1):(fecha_actual.getMonth()+1))+"-01");
            else if(this.tipo_visualizacion == 2) this.getFechaInicial().setFechaDesdeDB(fecha_actual.getFullYear()+"-"+((fecha_actual.getMonth()+1 < 10)?"0"+(fecha_actual.getMonth()+1):(fecha_actual.getMonth()+1))+"-"+fecha_actual.getDate());

            this.getFechaFinal().setComponente(document.getElementById(this.nombre_instancia+"_zona_fecha_final"), this.nombre_instancia+".getFechaFinal()");
            this.getFechaFinal().funcionOnChange(this.nombre_instancia+".cambiaRangoDeFecha()");
            this.getFechaFinal().setFechaDesdeDB(fecha_actual.getFullYear()+"-"+((fecha_actual.getMonth()+1 < 10)?"0"+(fecha_actual.getMonth()+1):(fecha_actual.getMonth()+1))+"-"+fecha_actual.getDate());
            
            if(this.tipo_visualizacion == 3){
                var fecha_actual = new Date();
                this.getFechaInicial().setComponente(document.getElementById(this.nombre_instancia+"_zona_fecha_inicial"), this.nombre_instancia+".getFechaInicial()");
                this.getFechaInicial().funcionOnChange(this.nombre_instancia+".cambiaRangoDeFecha()");
                this.getFechaInicial().setFechaDesdeDB(this.getFechaInicialPorTipoDeVisualizacion());

                this.getFechaFinal().setComponente(document.getElementById(this.nombre_instancia+"_zona_fecha_final"), this.nombre_instancia+".getFechaFinal()");
                this.getFechaFinal().funcionOnChange(this.nombre_instancia+".cambiaRangoDeFecha()");
                //console.log("fecha_final: "+this.getFechaFinalPorTipoDeVisualizacion());
                this.getFechaFinal().setFechaDesdeDB(this.getFechaFinalPorTipoDeVisualizacion());
            }
        }else{
            var contenido = "<table align='center' width='96%' cellpadding='6' cellspacing='0'>";
            contenido += "<tr>";
            contenido += "<td align='center' width='24%' valign='top'>";

            contenido += "<div class='texto_campo'>Fecha Inicial <span class='azul'>*</span></div>";
            contenido += "<div id='"+this.nombre_instancia+"_zona_fecha_inicial'></div>";

            contenido += "</td>";
            contenido += "<td align='center' width='24%' valign='top'>";

            contenido += "<div class='texto_campo'>Hora Inicial <span class='azul'>*</span></div>";
            contenido += "<div id='"+this.nombre_instancia+"_zona_hora_inicial'></div>";

            contenido += "</td>";
            
            //detalle vertical
            contenido += "<td align='center' width='4%' valign='top'>";
            contenido += "<div class='detalle_vertical'></div>";
            contenido += "</td>";
            
            contenido += "<td align='center' width='24%' valign='top'>";

            contenido += "<div class='texto_campo'>Fecha Final <span class='azul'>*</span></div>";
            contenido += "<div id='"+this.nombre_instancia+"_zona_fecha_final'></div>";

            contenido += "</td>";
            contenido += "<td align='center' width='24%' valign='top'>";

            contenido += "<div class='texto_campo'>Hora Final <span class='azul'>*</span></div>";
            contenido += "<div id='"+this.nombre_instancia+"_zona_hora_final'></div>";

            contenido += "</td>";
            contenido += "</tr>";
            
            contenido += "</table>";

            this.contenedor.innerHTML = contenido;

            var fecha_actual = new Date();
            
            this.getFechaInicial().setComponente(document.getElementById(this.nombre_instancia+"_zona_fecha_inicial"), this.nombre_instancia+".getFechaInicial()");
            this.getFechaInicial().funcionOnChange(this.nombre_instancia+".cambiaRangoDeFecha()");
            this.getFechaInicial().setFechaDesdeDB(this.getFechaInicialPorTipoDeVisualizacion());

            this.getFechaFinal().setComponente(document.getElementById(this.nombre_instancia+"_zona_fecha_final"), this.nombre_instancia+".getFechaFinal()");
            this.getFechaFinal().funcionOnChange(this.nombre_instancia+".cambiaRangoDeFecha()");
            //console.log("fecha_final: "+this.getFechaFinalPorTipoDeVisualizacion());
            this.getFechaFinal().setFechaDesdeDB(this.getFechaFinalPorTipoDeVisualizacion());
            
            this.getHoraInicial().setComponente(document.getElementById(this.nombre_instancia+"_zona_hora_inicial"), this.nombre_instancia+".getHoraInicial()");
            this.getHoraInicial().muestraComponente();
            this.getHoraInicial().setHora("12:01:00");
            
            this.getHoraFinal().setComponente(document.getElementById(this.nombre_instancia+"_zona_hora_final"), this.nombre_instancia+".getHoraFinal()");
            this.getHoraFinal().muestraComponente();
            this.getHoraFinal().setHora("12:00:00");
        }
    }
    
    this.setFechaInicial = function(fecha){
        this.getFechaInicial().setFechaDesdeDB(fecha);
    }
    
    this.setFechaFinal = function(fecha){
        this.getFechaFinal().setFechaDesdeDB(fecha);
    }
    
    this.cambiaRangoDeFecha = function(){
        ////console.log("cambio rango de fecha");
        if(this.funcion_cambia_rango) setTimeout(this.funcion_cambia_rango, 0);
    }
    
    this.getFechaInicio = function(){
        return this.getFechaInicial().getFechaActualParaDB();
    }
    
    this.getHoraInicio = function(){
        return this.hora_inicial.getHoraActualLegible();
    }
    
    this.getFechaFin = function(){
        return this.getFechaFinal().getFechaActualParaDB();
    }
    
    this.getHoraFin = function(){
        return this.hora_final.getHoraActualLegible();
    }
}
var CatalogoGastosInternos = function(){
    
    this.rango_fechas = null;
    this.getRangoDeFechas = function(){
        if(!this.rango_fechas) this.rango_fechas = new SelectorRangoDeFecha();
        return this.rango_fechas;
    }
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Gastos Internos");
        this.setTextoAgregarElemento("Agregar nuevo gasto interno");
        
		this.setFormaCaptura(new FormaGastoInterno());
        this.muestraEstructuraCatalogoPrincipalConBarraOpciones();
        
        var contenido = "<table align='center' width='100%' cellpadding='3' cellspacing='0'>";
        contenido += "<tr>";
        
        contenido += "<td align='center' width='90%' valign='top'>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_rango_fecha'></div>";
        
        contenido += "</td>";
        contenido += "<td align='center' width='10%' valign='bottom'>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_boton_todos'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".getTodosLosElementos()' style='margin:0 auto 0 auto;'>Todos</a></div>";
        
        contenido += "</td>";
        contenido += "</tr></table><br/>";
        
        this.getZonaSecundaria().innerHTML = contenido;
        
        this.getRangoDeFechas().setComponente(document.getElementById(this.nombre_instancia+"_zona_rango_fecha"), this.nombre_instancia+".getRangoDeFechas()");
        this.getRangoDeFechas().setFuncionCambiaRango(this.nombre_instancia+".onDateRangeChange()");
        this.getRangoDeFechas().muestraComponente();
        
        this.onDateRangeChange();
        
        //mostramos las opciones de impresion
        if(this.soloSeleccion == 0) this.mostraOpcionesDeImpresion();
    }
    
    this.getTodosLosElementos = function(){
        this.getRangoDeFechas().setFechaInicial("2017-01-01");
        this.getRangoDeFechas().setFechaFinal(getFechaActual());
        this.onDateRangeChange();
    }
    
    this.onDateRangeChange = function(){
        var fecha_inicio = this.getRangoDeFechas().getFechaInicio();
        var fecha_fin = this.getRangoDeFechas().getFechaFin();
        
        getResultadoAjax("opcion=25&fechaInicio="+fecha_inicio+"&fechaFinal="+fecha_fin, this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
    }
    
    this.muestraResultados = function(respuesta){
        //idGastoInterno, concepto, fecha, hora, cantidad, usuario
        //console.log("gastos internos: "+respuesta);
        var gastos = respuesta.split("^");
        gastos.pop();
        if(gastos.length == 0){
            var contenido = "<div>No hay gastos internos registrados en el sistema aún</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(gastos);
        
        var encabezados = new Array("Concepto", "Fecha", "Hora", "Usuario", "Cantidad");
        var medidas = new Array("20%", "15%", "15%", "20%", "10%");
        var alineaciones = new Array("left", "center", "center", "left", "right");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, this.nombre_instancia+".eliminaElemento()", null);
        this.getListaSeleccionable().setOpcionBorrar(true);
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
    
    this.elementoSeleccionado = function(){
        var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        
        var formaGastoInterno = new FormaGastoInterno();
        formaGastoInterno.setIdGastoInterno(datos[0]);
        this.setFormaCaptura(formaGastoInterno);
        this.nuevoElemento();
        
        formaGastoInterno.editaComponente();
    }
    
    this.eliminaElemento = function(){
        //si llega aqui, es por que se confirmó la acción de eliminar el elemento
        var idelemento = this.getListaSeleccionable().getIdElementoEliminado();
        getResultadoAjax("opcion=32&idgastointerno="+idelemento, this.nombre_instancia+".procesaElementoEliminado", null);
    }
    
    this.procesaElementoEliminado = function(respuesta){
        //console.log("respuesta eliminar: "+respuesta);
        if(respuesta == 1){
            alert("Gasto Interno Eliminado!");
            this.getListaSeleccionable().quitaElementoDeListaPorId(this.getListaSeleccionable().getIdElementoEliminado());
        }else{
            alert("No se pudo eliminar en este momento, por favor intenta mas tarde!");
        }
    }
}
var FormaGastoInterno = function(){
    
    this.idGastoInterno = 0;
    this.setIdGastoInterno = function(idgastointerno){
        this.idGastoInterno = idgastointerno;
    }
    
    this.idConcepto = 0;
    
    this.info = null;
    this.selector_hora = null;
    this.selector_fecha = null;
    
    this.catalogoSelectorConceptos = null;
    this.getCatalogoSelectorConceptos = function(){
        if(!this.catalogoSelectorConceptos){
            this.catalogoSelectorConceptos = new CatalogoSelectorConceptos();
        }
        return this.catalogoSelectorConceptos;
    }
    
    this.muestraComponente = function(){
        var contenido = "<br/>";    
        
        if(this.idGastoInterno == 0) contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Nuevo Gasto Interno</div>";
        else contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Editando Gasto Interno</div>";
        
        contenido += "<div class='separador'></div>";
        contenido += "<br/>";    
        
        //fecha y hora
        contenido += '<div class="row center-align">';
        contenido += '<div class="col l6 center-align">';
        
        contenido += "<div class='texto_forma'>Fecha</div>";
        contenido += "<br/><div id='"+this.nombre_instancia+"_selector_fecha'></div>";
        
        contenido += '</div>';
        contenido += '<div class="col l6 center-align">';
        
        contenido += "<div class='texto_forma'>Hora</div>";
        contenido += "<div id='"+this.nombre_instancia+"_selector_hora'></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += "<br/>";
        contenido += "<div class='ayuda_campos' id='"+this.nombre_instancia+"_a1'></div>";
        contenido += "<br/>";
        
        //concepto y cantidad
        contenido += '<div class="row center-align">';
        contenido += '<div class="col l6 center-align">';
        
        contenido += "<div class='texto_forma'>Concepto</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_concepto' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el concepto para este gasto interno.\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '<div class="col l6 center-align">';
        
        contenido += "<div class='texto_forma'>Cantidad</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_cantidad' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el total de este gasto interno. Solo se permiten números y . decimal\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_boton_guardar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a></div>";
        
        contenido += "<br/>";
        
        this.contenedor.innerHTML = contenido;
        
        
        //selector de fecha y hora, por defecto pone los datos actuales
        this.selector_fecha = new SelectorFechas();
        this.selector_fecha.fecha_actual = new Date();
        this.selector_fecha.setSelectorFecha(document.getElementById(this.nombre_instancia+"_selector_fecha"), this.nombre_instancia+".selector_fecha");
        this.selector_fecha.fecha_modificada = true;
        this.selector_fecha.muestraComboFechas();
        
        this.selector_hora = new SelectorHora();
        this.selector_hora.setSelectorHora(document.getElementById(this.nombre_instancia+"_selector_hora"), -1, null, this.nombre_instancia+".selector_hora");
    }
    
    this.onConceptoSeleccionadoChange = function(){
        this.idConcepto = this.getCatalogoSelectorConceptos().getIdConceptoSeleccionado();
    }
    
    this.actualizaDatosFacturacion = function(){
        
    }
    
    this.editaComponente = function(){
        if(!this.info){
            getResultadoAjax("opcion=31&idgastointerno="+this.idGastoInterno, this.nombre_instancia+".procesaInfoRecuperada", this.contenedor);
            return;
        }
        
        this.muestraComponente();
        //idGastosInterno, concepto, fecha, hora, cantidad, idUsuario, estatus
        
        this.selector_fecha.setFechaDesdeDB(this.info[2]);
        this.selector_hora.setHora(this.info[3]);
        document.getElementById(this.nombre_instancia+"_campo_concepto").value = this.info[1];
        document.getElementById(this.nombre_instancia+"_campo_cantidad").value = this.info[4];
    }
    
    this.procesaInfoRecuperada = function(respuesta){
        //console.log("info: "+respuesta);
        
        this.info = respuesta.split("|");
        this.editaComponente();
    }
    
    this.getZonaBotonGuardar = function(){
        return document.getElementById(this.nombre_instancia+"_zona_boton_guardar");
    }
    
    this.validaForma = function(){
        var fecha = this.selector_fecha.getFechaActualParaDB();
        var hora = this.selector_hora.getHora();
        
        var campoConcepto = document.getElementById(this.nombre_instancia+"_campo_concepto");
        //console.log(campoConcepto);
        if(campoConcepto.value == "" || campoConcepto.value == " "){
            campoConcepto.focus();
            return;
        }
        
        var campoCantidad = document.getElementById(this.nombre_instancia+"_campo_cantidad");
        if(campoCantidad.value.length == 0 || campoCantidad.value == "" || campoCantidad.value == " " || !parseFloat(campoCantidad.value)){
            campoCantidad.focus();
            return;
        }
        
        var info = this.idGastoInterno+"|"+fecha+"|"+hora+"|"+campoConcepto.value+"|"+campoCantidad.value;
        //idGasto, fecha, hora, concepto, cantidad
        
        getResultadoAjax("opcion=30&info="+info, this.nombre_instancia+".respuestaElementoGuardado", this.getZonaBotonGuardar());
    }
    
    this.respuestaElementoGuardado = function(respuesta){
        //console.log("respuesta guardar: "+respuesta);
        this.getZonaBotonGuardar().innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a>";
        
        if(respuesta > 0 && !isNaN(respuesta)){
            alert("Gasto Interno guardado!");
            if(this.idGastoInterno == 0) this.idGastoInterno = respuesta;
            this.getControladorNavegacion().botonCerrarPresionado();
        }else{
            alert("No se pudo guardar el gasto interno, por favor intenta más tarde!");
        }
    }
}
// JavaScript Document - Para el componente selector de hora
var SelectorHora = function(){
	this.contenedor_selector_hora = null;
	this.hora_actual = new Date;
	this.minutos = 0;
	this.hora = 0;
	this.reloj_activo =  true;
	this.nombre_instancia = "";
	
	this.setSelectorHora = function(contenedor, opcion, hora, nombre_instancia){
		this.contenedor_selector_hora = contenedor;
		this.nombre_instancia = nombre_instancia;
		if(opcion == -2){
			this.contenedor_selector_hora.innerHTML = this.getSelectorHora();
			this.setHora(hora);
		}
		else if(opcion == -1){
			this.hora_actual = new Date();
			this.contenedor_selector_hora.innerHTML = this.getSelectorHora();	
			this.reloj_activo = true;
			this.relojActivo();
		}
	}
    
    this.setComponente = function(contenedor, nombre_instancia){
        this.contenedor_selector_hora = contenedor;
		this.nombre_instancia = nombre_instancia;
    }
    
    this.muestraComponente = function(){
        this.hora_actual = new Date();
        this.contenedor_selector_hora.innerHTML = this.getSelectorHora();
    }
	
	this.relojActivo = function(){
        try{
            if(this.reloj_activo){
                this.hora_actual = new Date();
                this.getHoraActualLegible();
                setTimeout(this.nombre_instancia+".relojActivo()", 1000);
            }   
        }catch(error){
        }
	}
	
	//esta función deshabilita los controles para dejar la hora fija
	this.deshabilitaControles = function(){
		/*document.getElementById(this.nombre_instancia+"_boton_mas_hora").onclick = "";
		document.getElementById(this.nombre_instancia+"_boton_mas_minuto").onclick = "";
		document.getElementById(this.nombre_instancia+"_hora").disabled = true;
		document.getElementById(this.nombre_instancia+"_boton_menos_minuto").onclick= "";
		document.getElementById(this.nombre_instancia+"_boton_menos_hora").onclick = "";
		document.getElementById(this.nombre_instancia+"_minuto").disabled = true;*/
	}
	
	this.deshabilitaControlesDeMinutos = function(){
		document.getElementById(this.nombre_instancia+"_boton_menos_minuto").onclick= "";
		document.getElementById(this.nombre_instancia+"_boton_mas_minuto").onclick = "";
		document.getElementById(this.nombre_instancia+"_minuto").disabled = true;
	}
	
	this.unaHoraMas = function(){
		document.getElementById(this.nombre_instancia+"_boton_mas_hora").click();
	}
	
	//esta función pone la hora actual del reloj según la hora especificada
	this.setHora = function(hora_nueva){
		if (!hora_nueva) return;
		this.reloj_activo = false;
		var hora_separada = hora_nueva.split(":");
		document.getElementById(this.nombre_instancia+'_hora').value = hora_separada[0];
		document.getElementById(this.nombre_instancia+'_minuto').value = hora_separada[1];
		this.hora = parseInt(hora_separada[0]);
		this.minutos = parseInt(hora_separada[1]);
	}
	
	this.getSelectorHora = function(){
		var contenido = "<table align='center' cellpadding='0' cellspacing='0' border='0'>"
		//contenido += "<tr><td><div id='"+this.nombre_instancia+"_boton_mas_hora' class='boton_mas' onclick='"+this.nombre_instancia+".unaHoraMas()'></div></td><td>&nbsp;&nbsp;</td><td><div class='boton_mas' id='"+this.nombre_instancia+"_boton_mas_minuto' onclick='"+this.nombre_instancia+".unMinutoMas()'></div></td></tr>";
		contenido += "<tr><td width='45%'><input type='text' id='"+this.nombre_instancia+"_hora' onclick='"+this.nombre_instancia+".editandoCampo()' onblur='"+this.nombre_instancia+".terminoEdicion()' value='00' class='campo_hora' onkeypress='return getComponenteValidacionTexto().esSoloNumero(event);'/></td><td align='center' width='10%'><div><b>:</b></div></td><td width='45%'><input type='text' onclick='"+this.nombre_instancia+".editandoCampo()' onblur='"+this.nombre_instancia+".terminoEdicion()' id='"+this.nombre_instancia+"_minuto' value='00' class='campo_hora' onkeypress='return getComponenteValidacionTexto().esSoloNumero(event);'/></td></tr>";
		//contenido += "<tr><td><div class='boton_menos' id='"+this.nombre_instancia+"_boton_menos_hora' onclick='"+this.nombre_instancia+".unaHoraMenos()'></div></td><td>&nbsp;&nbsp;</td><td><div class='boton_menos' id='"+this.nombre_instancia+"_boton_menos_minuto' onclick='"+this.nombre_instancia+".unMinutoMenos()'></div></td></tr></table>";
        contenido += "</table>";
		return contenido;
	}
	
	this.editandoCampo = function(){
		this.reloj_activo = false;
	}
	
	this.terminoEdicion = function(){
		this.setHora(this.getHora());
        
        var hora = document.getElementById(this.nombre_instancia+"_hora");
        var minuto = document.getElementById(this.nombre_instancia+"_minuto");
        
        ////console.log("hora: "+hora.value+" minuto: "+minuto.value);
        
        if(hora.value > 23){
            alert("La hora debe estar entre 00 y 23!");
            hora.value = "00";
            //hora.focus();
            return;
        }
        
        if(minuto.value >= 59){
            alert("Los minutos deben estar entre 00 y 59!");
            minuto.value = "00";
            //minuto.focus();
            return;
        }
	}
	
	//esta función cambia el intervalo del boton más y menos de los minutos para incrementar en 30 minutos cada uno
	this.cambiaIntervaloDeTiempoParaReloj = function(){
		/*var boton_mas_minuto = document.getElementById(this.nombre_instancia+"_boton_mas_minuto");
		var boton_menos_minuto = document.getElementById(this.nombre_instancia+"_boton_menos_minuto");
		var selector = this;
		
		boton_mas_minuto.onclick = function(){
			selector.mediaHoraMas();
		}
		
		boton_menos_minuto.onclick = function(){
			selector.mediaHoraMenos();
		}*/
	}
	
	//funciones para aumentar media hora más o menos
	this.mediaHoraMas = function(){
		this.reloj_activo = false;
		if (this.minutos == 0) {
			this.minutos = 30;
			var mins = this.minutos
		}else{
			this.minutos = 0;
			var mins = "0";
			if(this.minutos < 10) mins = "0"+this.minutos;
			else mins = this.minutos;
			
			this.unaHoraMas();
		}
		
		document.getElementById(this.nombre_instancia+'_minuto').value = mins;
	}
	
	this.mediaHoraMenos = function(){
		this.reloj_activo = false;
		if (this.minutos == 0) {
			this.minutos = 30;
			var mins = this.minutos;
			this.unaHoraMenos();
		}else{
			this.minutos = 0;
			var mins = "0";
			if(this.minutos < 10) mins = "0"+this.minutos;
			else mins = this.minutos;
		}
		
		document.getElementById(this.nombre_instancia+'_minuto').value = mins;
	}
	
	this.unMinutoMas = function(){
		this.reloj_activo = false;
		if(this.minutos == 59){
			document.getElementById(this.nombre_instancia+'_minuto').value = "00";
			this.minutos = 0;
			this.unaHoraMas();
		}else{
			this.minutos += 1;
			var mins = "0";
			if(this.minutos < 10) mins = "0"+this.minutos;
			else mins = this.minutos;
			document.getElementById(this.nombre_instancia+'_minuto').value = mins;
		}
	}
	
	this.unMinutoMenos = function(){
		this.reloj_activo = false;
		if(this.minutos == 0){
			document.getElementById(this.nombre_instancia+'_minuto').value = "59";
			this.unaHoraMenos();
			this.minutos = 59;
		}else{
			this.minutos -= 1;
			var mins = "0";
			if(this.minutos < 10) mins = "0"+this.minutos;
			else mins = this.minutos;
			document.getElementById(this.nombre_instancia+'_minuto').value = mins;
		}
	}
	
	this.unaHoraMas = function(){
		this.reloj_activo = false;
		if(this.hora >= 23){
			document.getElementById(this.nombre_instancia+'_hora').value = "00";
			this.hora = 0;
		}else{
			this.hora += 1;
			var hr = "0";
			if(this.hora <= 9) hr = "0"+this.hora;
			else hr = this.hora;
			document.getElementById(this.nombre_instancia+'_hora').value = hr;
		}
	}

	this.unaHoraMenos = function(){
		this.reloj_activo = false;
		if(this.hora == 1){
			document.getElementById(this.nombre_instancia+'_hora').value = "00";
			this.hora = 24;
		}else{
			this.hora -= 1;
			var hr = "0";
			if(this.hora <= 9) hr = "0"+this.hora;
			else hr = this.hora;
			document.getElementById(this.nombre_instancia+'_hora').value = hr;
		}
	}
	
	this.getHoraActualLegible = function(){
		var mins = "0";
		var hr = "0";
		this.minutos = parseInt(this.hora_actual.getMinutes());
		if(this.minutos <= 9) mins = "0"+this.minutos;
		else mins = this.minutos;
		this.hora = parseInt(this.hora_actual.getHours());
		if(this.hora <= 9) hr = "0"+this.hora;
		else hr = this.hora;
		if(document.getElementById(this.nombre_instancia+'_hora')) document.getElementById(this.nombre_instancia+'_hora').value = hr;
		else this.reloj_activo = false;
		if(document.getElementById(this.nombre_instancia+'_minuto')) document.getElementById(this.nombre_instancia+'_minuto').value = mins;
		else return false;
		return hr+":"+mins;
	}
	
	this.getHora = function(){
		var hr = document.getElementById(this.nombre_instancia+'_hora').value;
		var mins = document.getElementById(this.nombre_instancia+'_minuto').value;
		return hr+":"+mins;
	}
}
var CatalogoSelectorConceptos = function(){
    
    this.contenedor_anterior = null;
    
    this.selector_opcion_principal = null;
    this.getSelectorDeOpcionPrincipal = function(){
        if(!this.selector_opcion_principal){
            this.selector_opcion_principal = new SelectorDeOpcion();
            this.selector_opcion_principal.setVisualizacion(2);
        }
        return this.selector_opcion_principal;
    }
    
    this.funcion = null;
    
    this.idConceptoSeleccionado = 0;
    
    this.getIdConceptoSeleccionado = function(){
        return this.idConceptoSeleccionado;
    }
    
    this.setIdConceptoSeleccionado = function(id){
        this.idConceptoSeleccionado = id;
        
        if(!this.listaConceptos){
            getResultadoAjax("opcion=26", this.nombre_instancia+".procesaDatosFacturacionRecuperadosParaMostrarSeleccionado", this.contenedor);
            return;
        }else this.muestraDatosDeFacturacionSeleccionados();
    }
    
    this.procesaDatosFacturacionRecuperadosParaMostrarSeleccionado = function(respuesta){
        this.listaConceptos = respuesta.split("^");
        this.listaConceptos.pop();
        
        this.muestraConceptoSeleccionado();
    }
    
    this.muestraConceptoSeleccionado = function(){
        var datos;
        for(var i=0; i<this.listaConceptos.length; i++){
            datos = this.listaConceptos[i].split("|");
            if(datos[0] == this.idConceptoSeleccionado){
                this.muestraComponenteSelector();
                this.setTextoParaMostrar(datos[1]);
                return;
            }
        }
        this.funcion = null;
        this.muestraComponenteSelector();
    }
    
    this.listaConceptos = null;
    
    this.tipoVisualizacion = 1;
    //el tipo de visualizacion 0 indica que es un componente que muestra su controlador de navegacion para mostrar el catálogo de conceptos. El tipo de visualización 1 indica que el catalogo de datos de facturacion se mostrará en un componente flotante.
    
    this.setTipoVisualizacion = function(tipo){
        this.tipoVisualizacion = tipo;
    }
    
    this.funcion_concepto_seleccionado = null;
    this.setFuncionConceptoSeleccionado = function(funcion){
        this.funcion_concepto_seleccionado = funcion;
    }
    
    this.muestraComponenteSelector = function(){
        if(this.contenedor_anterior) this.contenedor = this.contenedor_anterior;
        
        this.getSelectorDeOpcionPrincipal().setComponente(this.contenedor, this.nombre_instancia+".getSelectorDeOpcionPrincipal()");
        this.getSelectorDeOpcionPrincipal().setTextoDeOpcionSeleccionada("Selecciona un concepto");
        if(this.funcion) this.getSelectorDeOpcionPrincipal().setFuncionSelectorPresionado(this.funcion);
        else this.getSelectorDeOpcionPrincipal().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoDeConceptos()");
       
       if(this.idConceptoSeleccionado != 0){
           this.getSelectorDeOpcionPrincipal().setFuncionOpcionSelectorPresionado(this.nombre_instancia+".opcionSelectorPresionada()");
       }
        
        this.getSelectorDeOpcionPrincipal().muestraComponente();
    }
    
    this.procesaDatosRecuperados = function(respuesta){
        //console.log("conceptos: "+respuesta);
        this.listaConceptos = respuesta.split("^");
        this.listaConceptos.pop();
        this.muestraCatalogoDeConceptos();
    }
    
    this.muestraComponente = function(){
        if(this.contenedor_anterior) this.contenedor = this.contenedor_anterior;
        this.muestraCatalogoDeConceptos();
    }
    
    this.muestraCatalogoDeConceptos = function(){
        if(!this.listaConceptos){
            getResultadoAjax("opcion=26", this.nombre_instancia+".procesaDatosRecuperados", null);
            return;
        }
        
        this.contenedor_anterior = this.contenedor;
        
        if(this.tipoVisualizacion == 0){
            /*this.getControladorNavegacion().setControladorNavegacion(this.contenedor, null, null, this.nombre_instancia+".catalogoCancelado();", this.nombre_instancia+".getControladorNavegacion()");
            this.getControladorNavegacion().muestraControladorNavegacionMedio();

            this.setComponente(this.getControladorNavegacion().getContenedorDeContenido(), this.nombre_instancia);   */
        }else{
            this.setContenedorFormaFlotante(1);
            this.muestraContenedorModal('G');
            this.controlador_navegacion = null;
            this.setComponente(this.getContenedorModal().getContenedorDeContenido(), this.nombre_instancia);
        }
        
        this.setTextoAgregarElemento("Agregar nuevo concepto");
        this.setTituloCatalogo("Conceptos de Gastos Internos");
        
        var formaConcepto = new FormaConcepto();
        formaConcepto.setComponentePadre(this);
        
		this.setFormaCaptura(formaConcepto);
        this.setContenedorFormaFlotante(0);
		this.muestraEstructuraCatalogoPrincipalConOpcionAgregarNuevo();
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(this.listaConceptos);
        
        var encabezados = new Array("Descripción");
        var medidas = new Array("90%");
        var alineaciones = new Array("left");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 1, 1, this.nombre_instancia+".setElementoSeleccionado()", this.nombre_instancia+".muestraElementoParaEdicion()", this.nombre_instancia+".eliminaElemento()", null);
        
        //this.getListaSeleccionable().setOpcionImpresora(0);
        this.getListaSeleccionable().setFuncionDetalleElementoSeleccionado(this.nombre_instancia+".muestraElementoParaEdicion()");
        this.muestraElementosDelCatalogo();
        
        //this.muestraComponenteSelector();
    }
    
    this.opcionSelectorPresionada = function(){
        this.muestraCatalogoDeConceptos();
        this.muestraElementoParaEdicion(this.idConceptoSeleccionado);
    }
    
    this.conceptoAgregado = function(info){
        this.listaConceptos = null;
        var datos = info.split("|");
        
        this.idConceptoSeleccionado = datos[0];
        this.muestraComponente();
        this.setTextoParaMostrar(datos[1]);
        
        //si se trata de un componente flotante, debemos cerrarlo
        //como ya se seleccionó un dato de facturación, entonces debemos cerrar la ventana flotante en caso de que se esté mostrando como componente flotante
        if(this.tipoVisualizacion == 1){
            this.getContenedorModal().ocultaComponente();
        }
        
        //invocamos la función dada para notificar cuando se cambió el recepctor de la factura
        if(this.funcion_concepto_seleccionado) setTimeout(this.funcion_concepto_seleccionado, 0);
    }
    
    this.setTextoParaMostrar = function(texto){
        this.getSelectorDeOpcionPrincipal().setTextoDeOpcionSeleccionada(texto);
        this.getSelectorDeOpcionPrincipal().muestraComponente();
    }
    
    this.setElementoSeleccionado = function(){
        //idConcepto, descripcion
        var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        
        this.idConceptoSeleccionado = datos[0];
        this.muestraComponenteSelector();
        this.setTextoParaMostrar(datos[1]);
        
        //como ya se seleccionó un dato de facturación, entonces debemos cerrar la ventana flotante en caso de que se esté mostrando como componente flotante
        if(this.tipoVisualizacion == 1){
            this.getContenedorModal().ocultaComponente();
        }
        
        //invocamos la función dada para notificar cuando se cambió el recepctor de la factura
        if(this.funcion_datos_facturacion_seleccionada) setTimeout(this.funcion_datos_facturacion_seleccionada, 0);
    }
    
    this.muestraElementoParaEdicion = function(iddatos){
        if(!iddatos || iddatos == 0){
            var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
            iddatos = datos[0];
        }
        
        this.getSelectorDeOpcion().getBotonAgregar().click();
        this.getFormaCaptura().setIdDatosCaptura(iddatos);
        this.getFormaCaptura().editaInfo();
    }
    
    this.eliminaElemento = function(){
        var idelemento = this.getListaSeleccionable().getIdElementoEliminado();
        getResultadoAjax("opcion=29&idconcepto="+idelemento, this.nombre_instancia+".procesaElementoEliminado", null);
    }
    
    this.procesaElementoEliminado = function(respuesta){
        //console.log("respuesta eliminar: "+respuesta);
        if(respuesta == 1){
            alert("Concepto eliminado!");
            this.getListaSeleccionable().quitaElementoDeListaPorId(this.getListaSeleccionable().getIdElementoEliminado());
        }else{
            alert("No se pudo eliminar en este momento, por favor intenta mas tarde!");
        }
    }
}
var FormaConcepto = function(){
    
    this.idConcepto = 0;
    this.setIdConcepto = function(idconcepto){
        this.idConcepto = idconcepto;
    }
    
    this.setIdDatosCaptura = function(id){
        this.setIdConcepto(id);
    }
    
    this.componente_padre = null;
    this.setComponentePadre = function(componente){
        this.componente_padre = componente;
    }
    
    this.info = null;
    
    this.muestraComponente = function(){
        var contenido = "<br/>";    
        
        if(this.idConcepto == 0) contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Nuevo Concepto de Gasto Interno</div>";
        else contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Editando Concepto de Gasto Interno</div>";
        
        contenido += "<div class='separador'></div>";
        contenido += "<br/>";    
        
        contenido += "<div class='texto_forma'>Nombre del Concepto</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_nombre' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el nombre del concepto para gasto interno\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += "<br/>";
        contenido += "<div class='ayuda_campos' id='"+this.nombre_instancia+"_a1'></div>";
        contenido += "<br/>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_boton_guardar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a></div>";
        
        contenido += "<br/>";
        
        this.contenedor.innerHTML = contenido;
    }
    
    this.actualizaDatosFacturacion = function(){
        
    }
    
    this.editaInfo = function(){
        if(!this.info){
            getResultadoAjax("opcion=28&idconcepto="+this.idConcepto, this.nombre_instancia+".procesaInfoRecuperada", this.contenedor);
            return;
        }
        
        this.muestraComponente();
        document.getElementById(this.nombre_instancia+"_campo_nombre").value = this.info[1];
    }
    
    this.procesaInfoRecuperada = function(respuesta){
        //console.log("info: "+respuesta);
        
        this.info = respuesta.split("|");
        this.editaInfo();
    }
    
    this.getZonaBotonGuardar = function(){
        return document.getElementById(this.nombre_instancia+"_zona_boton_guardar");
    }
    
    this.validaForma = function(){
        var campoNombre = document.getElementById(this.nombre_instancia+"_campo_nombre");
        if(campoNombre.value.length == 0 || campoNombre.value == "" || campoNombre.value == " "){
            campoNombre.focus();
            return;
        }
        
        var info = this.idConcepto+"|"+campoNombre.value;
        //idConcepto, nombrecompleto
        
        //console.log("info: "+info);
        this.info = info;
        
        getResultadoAjax("opcion=27&info="+info, this.nombre_instancia+".respuestaElementoGuardado", this.getZonaBotonGuardar());
    }
    
    this.respuestaElementoGuardado = function(respuesta){
        //console.log("respuesta guardar: "+respuesta);
        this.getZonaBotonGuardar().innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a>";
        
        if(respuesta > 0 && !isNaN(respuesta)){
            alert("Concepto guardado!");
            if(this.idConcepto == 0) this.idConcepto = respuesta;
            if(this.componente_padre) this.componente_padre.conceptoAgregado(this.info);
        }else{
            alert("No se pudo guardar el concepto, por favor intenta más tarde!");
        }
    }
}
var CatalogoGastos = function(){
    
    this.rango_fechas = null;
    this.getRangoDeFechas = function(){
        if(!this.rango_fechas) this.rango_fechas = new SelectorRangoDeFecha();
        return this.rango_fechas;
    }
    
    this.selectorClientes = null;
    this.getSelectorClientes = function(){
        if(!this.selectorClientes){
            this.selectorClientes = new SelectorDeOpcion();
            this.selectorClientes.setVisualizacion(1);
        }
        return this.selectorClientes;
    }
    
    this.clienteSeleccionado = 0;
    this.setClienteSeleccionado = function(idcliente){
        this.clienteSeleccionado = idcliente;
    }
    
    this.catalogoClientes = null;
    this.getCatalogoClientes = function(){
        if(!this.catalogoClientes) {
            this.catalogoClientes = new CatalogoClientes();
            this.catalogoClientes.setSoloSeleccion(1);
        }
        return this.catalogoClientes;
    }
    
    this.muestraComponente = function(){
        if(orbe.login.getPerfil() != 5){
            this.setTituloCatalogo("Gastos De Clientes");
            this.setTextoAgregarElemento("Agregar nuevo gasto de cliente");

            this.setFormaCaptura(new FormaGastos());
            this.muestraEstructuraCatalogoPrincipalConBarraOpciones();

            var contenido = "<div id='"+this.nombre_instancia+"_zona_clientes'></div><br/><table align='center' width='100%' cellpadding='3' cellspacing='0'>";
            contenido += "<tr>";

            contenido += "<td align='center' width='90%' valign='top'>";

            contenido += "<div id='"+this.nombre_instancia+"_zona_rango_fecha'></div>";

            contenido += "</td>";
            contenido += "<td align='center' width='10%' valign='bottom'>";

            contenido += "<div id='"+this.nombre_instancia+"_zona_boton_todos'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".getTodosLosElementos()' style='margin:0 auto 0 auto;'>Todos</a></div>";

            contenido += "</td>";
            contenido += "</tr></table><br/>";

            this.getZonaSecundaria().innerHTML = contenido;

            this.getRangoDeFechas().setComponente(document.getElementById(this.nombre_instancia+"_zona_rango_fecha"), this.nombre_instancia+".getRangoDeFechas()");
            this.getRangoDeFechas().setFuncionCambiaRango(this.nombre_instancia+".onDateRangeChange()");
            this.getRangoDeFechas().muestraComponente();

            this.getSelectorClientes().setComponente(document.getElementById(this.nombre_instancia+"_zona_clientes"), this.nombre_instancia+".getSelectorClientes()");
            if(this.clienteSeleccionado == 0) this.getSelectorClientes().setTextoDeOpcionSeleccionada("Todos los clientes");
            this.getSelectorClientes().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoDeClientes()");

            this.getSelectorClientes().muestraComponente();
            //else this.getSelectorClientes().setIdConceptoSeleccionado(this.clienteSeleccionado);

            this.onDateRangeChange();

            this.getFormaCaptura().setClienteSeleccionado(this.clienteSeleccionado);
            this.getFormaCaptura().setCatalogoClientes(this.getCatalogoClientes());
            this.getFormaCaptura().setSelectorClientes(this.getSelectorClientes());
            this.getFormaCaptura().setComponentePadre(this);

            //mostramos las opciones de impresion
            if(this.soloSeleccion == 0) this.mostraOpcionesDeImpresion();
        }else{
            this.setTituloCatalogo("Mis Gastos Registrados");
            this.muestraEstructuraCatalogoPrincipalConBarraOpcionesSinOpcionNuevo();

            var contenido = "<div id='"+this.nombre_instancia+"_zona_clientes'></div><br/><table align='center' width='100%' cellpadding='3' cellspacing='0'>";
            contenido += "<tr>";

            contenido += "<td align='center' width='90%' valign='top'>";

            contenido += "<div id='"+this.nombre_instancia+"_zona_rango_fecha'></div>";

            contenido += "</td>";
            contenido += "<td align='center' width='10%' valign='bottom'>";

            contenido += "<div id='"+this.nombre_instancia+"_zona_boton_todos'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".getTodosLosElementos()' style='margin:0 auto 0 auto;'>Todos</a></div>";

            contenido += "</td>";
            contenido += "</tr></table><br/>";

            this.getZonaSecundaria().innerHTML = contenido;

            this.getRangoDeFechas().setComponente(document.getElementById(this.nombre_instancia+"_zona_rango_fecha"), this.nombre_instancia+".getRangoDeFechas()");
            this.getRangoDeFechas().setFuncionCambiaRango(this.nombre_instancia+".onDateRangeChange()");
            this.getRangoDeFechas().muestraComponente();

            this.onDateRangeChange();

            //mostramos las opciones de impresion
            this.mostraOpcionesDeImpresion();
        }
    }
    
    this.getTodosLosElementos = function(){
        this.getRangoDeFechas().setFechaInicial("2017-01-01");
        this.getRangoDeFechas().setFechaFinal(getFechaActual());
        this.onDateRangeChange();
    }
    
    this.onDateRangeChange = function(){
        var fecha_inicio = this.getRangoDeFechas().getFechaInicio();
        var fecha_fin = this.getRangoDeFechas().getFechaFin();
        getResultadoAjax("opcion=33&fechaInicio="+fecha_inicio+"&fechaFinal="+fecha_fin+"&idcliente="+this.clienteSeleccionado, this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
    }
    
    this.muestraResultados = function(respuesta){
        //idGastoCliente, nombrePromotor, nombreEmpresa, nombreCliente, nombreBanco, fecha, hora, total, porcentaje, estatus
        //console.log("gastos clientes: "+respuesta);
        var gastos = respuesta.split("^");
        gastos.pop();
        if(gastos.length == 0){
            var contenido = "<div>No hay gastos de clientes registrados en el sistema aún</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(gastos);
        
        var encabezados = new Array("Promotor", "Empresa", "Cliente", "Banco", "Fecha", "Cantidad");
        var medidas = new Array("15%", "15%", "15%", "15%", "10%", "10%");
        var alineaciones = new Array("left", "left", "left", "center", "center", "right");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        if(orbe.login.getPerfil() != 5){
            this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, this.nombre_instancia+".eliminaElemento()", null);
            this.getListaSeleccionable().setOpcionBorrar(true);
            this.getListaSeleccionable().setOpcionDetalle(false);
        }else{
            this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, null, null, null, null);
            this.getListaSeleccionable().setOpcionBorrar(false);
            this.getListaSeleccionable().setOpcionDetalle(false);
        }
        
        this.muestraElementosDelCatalogo();
    }
    
    this.elementoSeleccionado = function(){
        var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        
        var formaGasto = new FormaGastos();
        this.setFormaCaptura(formaGasto);
        this.nuevoElemento();
        
        formaGasto.setIdGasto(datos[0]);
        formaGasto.editaComponente();
    }
    
    this.eliminaElemento = function(){
        //si llega aqui, es por que se confirmó la acción de eliminar el elemento
        var idelemento = this.getListaSeleccionable().getIdElementoEliminado();
        getResultadoAjax("opcion=36&idgasto="+idelemento, this.nombre_instancia+".procesaElementoEliminado", null);
    }
    
    this.procesaElementoEliminado = function(respuesta){
        //console.log("respuesta eliminar: "+respuesta);
        if(respuesta == 1){
            alert("Gasto de Cliente Eliminado!");
            this.getListaSeleccionable().quitaElementoDeListaPorId(this.getListaSeleccionable().getIdElementoEliminado());
        }else{
            alert("No se pudo eliminar en este momento, por favor intenta mas tarde!");
        }
    }
    
    /*
    METODOS PARA INVOCAR EL CATALOGO DE CLIENTES REGISTRADOS
    */
    this.muestraCatalogoDeClientes = function(){
        this.getCatalogoClientes().setComponente(null, this.nombre_instancia+".getCatalogoClientes()");
        this.getCatalogoClientes().setContenedorFormaFlotante(1);
        this.getCatalogoClientes().muestraContenedorModal('G');
        this.getCatalogoClientes().controlador_navegacion = null;
        
        this.catalogoClientes.setFuncionElementoSeleccionado(this.nombre_instancia+".clienteSeleccionadoDeCatalogo()");
        this.getCatalogoClientes().setComponente(this.getCatalogoClientes().getContenedorModal().getContenedorDeContenido(), this.getCatalogoClientes().nombre_instancia);
        this.getCatalogoClientes().muestraComponente();
    }
    
    this.clienteSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoClientes().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorClientes().setTextoDeOpcionSeleccionada(datos[1]);
            this.clienteSeleccionado = datos[0];
            this.getCatalogoClientes().getContenedorModal().ocultaComponente();
            this.onDateRangeChange();//recargamos el catalogo de gastos de clientes considerando el cliente seleccionado
            
            this.getFormaCaptura().setClienteSeleccionado(this.clienteSeleccionado);
            this.getFormaCaptura().setCatalogoClientes(this.getCatalogoClientes());
            this.getFormaCaptura().setSelectorClientes(this.getSelectorClientes());
        }
    }
}
var FormaGastos = function(){
    
    this.idGasto = 0;
    this.setIdGasto = function(idgasto){
        this.idGasto = idgasto;
    }
    
    this.info = null;
    
    this.selector_fecha = null;
    
    this.clienteSeleccionado = 0;
    this.setClienteSeleccionado = function(idcliente){
        this.clienteSeleccionado = idcliente;
    }
    
    this.catalogoClientes = null;
    this.getCatalogoClientes = function(){
        if(!this.catalogoClientes) {
            this.catalogoClientes = new CatalogoClientes();
            this.catalogoClientes.setSoloSeleccion(1);
        }
        return this.catalogoClientes;
    }
    
    this.setCatalogoClientes = function(catalogo){
        this.catalogoClientes = catalogo;
    }
    
    this.selectorClientes = null;
    this.getSelectorClientes = function(){
        if(!this.selectorClientes){
            this.selectorClientes = new SelectorDeOpcion();
            this.selectorClientes.setVisualizacion(1);
        }
        return this.selectorClientes;
    }
    
    this.setSelectorClientes = function(selector){
        this.selectorClientes = selector;
    }
    
    //promotores
    this.idPromotorSeleccionado = 0;
    this.selectorPromotor = null;
    this.getSelectorPromotor = function(){
        if(!this.selectorPromotor){
            this.selectorPromotor = new SelectorDeOpcion();
            this.selectorPromotor.setVisualizacion(1);
        }
        return this.selectorPromotor;
    }
    
    this.catalogoPromotores = null;
    this.getCatalogoPromotores = function(){
        if(!this.catalogoPromotores){
            this.catalogoPromotores = new CatalogoUsuarios();
            this.catalogoPromotores.setSoloSeleccion(1);
            this.catalogoPromotores.setFiltroTipoUsuarios(3);
        }
        return this.catalogoPromotores;
    }
    
    //empresa
    this.idEmpresaSeleccionada = 0;
    this.selectorEmpresa = null;
    this.getSelectorEmpresa = function(){
        if(!this.selectorEmpresa){
            this.selectorEmpresa = new SelectorDeOpcion();
            this.selectorEmpresa.setVisualizacion(1);
        }
        return this.selectorEmpresa;
    }
    
    this.catalogoEmpresas = null;
    this.getCatalogoEmpresas = function(){
        if(!this.catalogoEmpresas){
            this.catalogoEmpresas = new CatalogoEmpresas();
            this.catalogoEmpresas.setSoloSeleccion(1);
        }
        return this.catalogoEmpresas;
    }
    
    //Bancos
    this.idBancoSeleccionado = 0;
    this.selectorBancos = null;
    this.getSelectorBancos = function(){
        if(!this.selectorBancos){
            this.selectorBancos = new SelectorDeOpcion();
            this.selectorBancos.setVisualizacion(1);
        }
        return this.selectorBancos;
    }
    
    this.catalogoBancos = null;
    this.getCatalogoBancos = function(){
        if(!this.catalogoBancos){
            this.catalogoBancos = new CatalogoBancos();
            this.catalogoBancos.setSoloSeleccion(1);
        }
        return this.catalogoBancos;
    }
    
    //Catalogo de Conceptos
    this.catalogoConceptos = null;
    this.getCatalogoDeConceptos = function(){
        if(!this.catalogoConceptos){
            this.catalogoConceptos = new CatalogoConceptosDeGasto();
            this.catalogoConceptos.setIdGasto(this.idGasto);
        }
        return this.catalogoConceptos;
    }
    
    //Visor de Facturación
    this.visor_facturas = null;
    this.getComponenteVisorDeFacturas = function(){
        if(!this.visor_facturas){
            this.visor_facturas = new ComponenteVisorFactura();
            this.visor_facturas.setComponente(null, this.nombre_instancia+".getComponenteVisorDeFacturas()");
            this.visor_facturas.funcionFacturasActualizadas(this.nombre_instancia+".recargaFacturasEmitidas()");
        }
        return this.visor_facturas;
    }
    
    //SELECTOR Y CATALOGO DE FORMAS DE PAGO
    this.idFormaPago = 1;
    this.bancarizado = false;
    this.selectorFormasPago = null;
    this.getSelectorFormasPago = function(){
        if(!this.selectorFormasPago){
            this.selectorFormasPago = new SelectorDeOpcion();
            this.selectorFormasPago.setVisualizacion(1);
        }
        return this.selectorFormasPago;
    }
    
    this.catalogoFormasPago = null;
    this.getCatalogoFormasPago = function(){
        if(!this.catalogoFormasPago){
            this.catalogoFormasPago = new CatalogoFormaPagoSat();
            this.catalogoFormasPago.setSoloSeleccion(1);
        }
        return this.catalogoFormasPago;
    }
    //SELECTOR Y CATALOGO DE FORMAS DE PAGO
    
    //SELECTOR Y CATALOGO DE MONEDAS
    this.idMoneda = "MXN";
    this.selectorMoneda = null;
    this.getSelectorMoneda = function(){
        if(!this.selectorMoneda){
            this.selectorMoneda = new SelectorDeOpcion();
            this.selectorMoneda.setVisualizacion(1);
        }
        return this.selectorMoneda;
    }
    
    this.catalogoMonedas = null;
    this.getCatalogoMonedas = function(){
        if(!this.catalogoMonedas){
            this.catalogoMonedas = new CatalogoMonedasSat();
            this.catalogoMonedas.setSoloSeleccion(1);
        }
        return this.catalogoMonedas;
    }
    //SELECTOR Y CATALOGO DE MONEDAS
    
    //SELECTOR Y CATALOGO DE USOS CFDI
    this.idUso = 0;
    this.selectorUsos = null;
    this.getSelectorUsos = function(){
        if(!this.selectorUsos){
            this.selectorUsos = new SelectorDeOpcion();
            this.selectorUsos.setVisualizacion(1);
        }
        return this.selectorUsos;
    }
    
    this.catalogoUsos = null;
    this.getCatalogoUsos = function(){
        if(!this.catalogoUsos){
            this.catalogoUsos = new CatalogoUsosCFDISat();
            this.catalogoUsos.setSoloSeleccion(1);
        }
        return this.catalogoUsos;
    }
    //SELECTOR Y CATALOGO DE USOS CFDI
    
    //Catalogo de facturas emitidas
    this.catalogoFacturas = null;
    this.getCatalogoFacturas = function(){
        if(!this.catalogoFacturas){
            this.catalogoFacturas = new CatalogoFacturasEmitidas();
            this.catalogoFacturas.setIdElemento(this.idGasto);
        }
        
        return this.catalogoFacturas;
    }
    //Catalogo de facturas emitidas
    
    this.muestraComponente = function(){
        var contenido = "<br/>";    
        
        if(this.idGasto == 0) contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Nuevo Gasto de Cliente</div>";
        else contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Editando Gasto de Cliente</div>";
        
        contenido += "<div class='separador'></div>";
        contenido += "<br/>";    
        
        //selector de cliente
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s12 m12 l12 center-align">';
        
        contenido += "<div class='texto_forma'>Cliente</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_clientes'></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        //promotor y empresa
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += "<div class='texto_forma'>Promotor</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_promotores'></div>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += "<div class='texto_forma'>Empresa</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_empresas'></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += "<br/>";
        contenido += "<div class='ayuda_campos' id='"+this.nombre_instancia+"_a1'></div>";
        contenido += "<br/>";
        
        //banco, fecha
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += "<div class='texto_forma'>Banco</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_bancos'></div>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += "<div class='texto_forma'>Fecha</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_fecha'></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        //Cantidad y porcentaje
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += "<div class='texto_forma'>Cantidad</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_cantidad' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el total de este gasto. Solo se permiten números y . decimal\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += "<div class='texto_forma'>Porcentaje</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_porcentaje' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el porcentaje para promotor de este gasto de cliente. Entre 0 y 1, por ejemplo 0.1 para 10%\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        //indicador ya facturada
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += "<table align='center' width='80%' border='0'>";
        contenido += "<tr>";
        contenido += "<td align='right' valign='top' width='5%'>";
        contenido += "</td>";
        contenido += "<td align='right' valign='top' width='30%'>";
        
        contenido += '<div class="texto_forma">¿Facturada?&nbsp;&nbsp;&nbsp;</div>';
        
        contenido += "</td>";
        contenido += "<td align='left' valign='top' width='60%'>";
        
        contenido += '<div class="switch">';
        contenido += '<label> No';
        contenido += '<input type="checkbox" id="'+this.nombre_instancia+'_ya_facturada">';
        contenido += '<span class="lever"></span>';
        contenido += 'Si</label></div>';
        
        contenido += "</td>";
        contenido += "</tr></table>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += '<div class="texto_forma">Método de Pago</div>';
        contenido += '<select id="'+this.nombre_instancia+'_selector_metodo_pago" class="browser-default" onchange="'+this.nombre_instancia+'.cambiaMetodoPago(this)">';
        contenido += '<option value="0">Selecciona método de pago</option>';
        contenido += '<option value="PUE">Pago en una sola Exhibición</option>';
        contenido += '<option value="PPD">Pago en Parcialidades</option>';
        contenido += '</select>';
        
        contenido += '</div>';
        contenido += '</div>';
        
        //campos que se muestran u ocultan dependiendo del método de pago
        contenido += "<div id='"+this.nombre_instancia+"_zona_campos_metodo_pago'>";
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += '<div class="texto_forma">Forma de Pago</div>';
        contenido += '<div id="'+this.nombre_instancia+'_zona_forma_pago"></div>';
        
        contenido += '</div>';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += '<div class="texto_forma">Moneda</div>';
        contenido += '<div id="'+this.nombre_instancia+'_zona_selector_moneda"></div>';
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_tipo_cambio'>";
        contenido += "<div class='texto_forma'>Tipo de Cambio</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_tipo_cambio' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe la cantidad del tipo de cambio de la moneda seleccionada con respecto del peso mexicano\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        contenido += "</div>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += '</div>';
        contenido += '</div>';
        contenido += '</div>';//fin de la zona de campos para pago PUE
        
        //uso de cfdi
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 m6 l6 center-align">';
        
        contenido += "<div class='texto_forma'>Uso CFDI</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_uso_cfdi'></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += "<br/>";
        contenido += "<div class='ayuda_campos' id='"+this.nombre_instancia+"_a2'></div>";
        contenido += "<br/>";
        
        //selector para agregar un nuevo concepto a la factura
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s12 m12 l12 center-align">';
        
        contenido += "<br/>";
        contenido += "<div id='"+this.nombre_instancia+"_conceptos_facturacion'></div>";
        contenido += "<br/>";
        
        contenido += "<br/>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_facturas_emitidas'></div>";
        contenido += "<br/>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += "<br/>";
        
        //boton guardar y facturación global
        contenido += '<div class="row center-align">';
        contenido += '<div class="col l6 center-align">';
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_boton_guardar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a></div>";
        
        contenido += '</div>';
        contenido += '<div class="col l6 center-align">';
        
        //solo se pueden agregar los conceptos que tienen idGasto en 0, en otro caso no se pueden agregar por que ya están agregados, solo se pueden modificar
        if(this.info == null || this.info[21] == 0){
            contenido += "<div id='"+this.nombre_instancia+"_zona_boton_agregar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".facturaGlobal()' style='margin:0 auto 0 auto;'>Factura Global</a></div>";   
        }
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += "<br/>";
        
        this.contenedor.innerHTML = contenido;
        
        //selector de fecha, por defecto pone los datos actuales
        this.selector_fecha = new SelectorFechas();
        this.selector_fecha.fecha_actual = new Date();
        this.selector_fecha.setSelectorFecha(document.getElementById(this.nombre_instancia+"_zona_fecha"), this.nombre_instancia+".selector_fecha");
        this.selector_fecha.fecha_modificada = true;
        this.selector_fecha.muestraComboFechas();
        
        this.getSelectorClientes().setComponente(document.getElementById(this.nombre_instancia+"_zona_clientes"), this.nombre_instancia+".getSelectorClientes()");
        
        if(this.clienteSeleccionado == 0) this.getSelectorClientes().setTextoDeOpcionSeleccionada("Todos los clientes");
        this.getSelectorClientes().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoDeClientes()");
        
        this.getSelectorClientes().muestraComponente();
        
        //selector promotor
        this.getSelectorPromotor().setComponente(document.getElementById(this.nombre_instancia+"_zona_promotores"), this.nombre_instancia+".getSelectorPromotor()");
        
        if(this.idPromotorSeleccionado == 0) this.getSelectorPromotor().setTextoDeOpcionSeleccionada("Selecciona Promotor");
        this.getSelectorPromotor().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoPromotores()");
        
        this.getSelectorPromotor().muestraComponente();
        
        //selector empresa
        this.getSelectorEmpresa().setComponente(document.getElementById(this.nombre_instancia+"_zona_empresas"), this.nombre_instancia+".getSelectorEmpresa()");
        
        if(this.idEmpresaSeleccionada == 0) this.getSelectorEmpresa().setTextoDeOpcionSeleccionada("Selecciona Empresa");
        this.getSelectorEmpresa().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoEmpresas()");
        
        this.getSelectorEmpresa().muestraComponente();
        
        //selector bancos
        this.getSelectorBancos().setComponente(document.getElementById(this.nombre_instancia+"_zona_bancos"), this.nombre_instancia+".getSelectorBancos()");
        
        if(this.idBancoSeleccionado == 0) this.getSelectorBancos().setTextoDeOpcionSeleccionada("Selecciona Banco");
        this.getSelectorBancos().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoBancos()");
        
        this.getSelectorBancos().muestraComponente();
        
        //selector formas de pago sat
        this.getSelectorFormasPago().setComponente(document.getElementById(this.nombre_instancia+"_zona_forma_pago"), this.nombre_instancia+".getSelectorFormasPago()");
        
        if(this.idFormaPago == 0) this.getSelectorFormasPago().setTextoDeOpcionSeleccionada("Selecciona Forma de Pago");
        else if(this.idFormaPago == 1) this.getSelectorFormasPago().setTextoDeOpcionSeleccionada("EFECTIVO");
        
        this.getSelectorFormasPago().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoFormasDePago()");
        
        this.getSelectorFormasPago().muestraComponente();
        
        //selector monedas sat
        this.getSelectorMoneda().setComponente(document.getElementById(this.nombre_instancia+"_zona_selector_moneda"), this.nombre_instancia+".getSelectorMoneda()");
        
        if(this.idMoneda == 0) this.getSelectorMoneda().setTextoDeOpcionSeleccionada("Selecciona Moneda");
        else if(this.idMoneda == "MXN") this.getSelectorMoneda().setTextoDeOpcionSeleccionada("PESO MEXICANO (MXN)");
        
        this.getSelectorMoneda().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoMonedas()");
        
        this.getSelectorMoneda().muestraComponente();
        
        //selector usos cfdis
        this.getSelectorUsos().setComponente(document.getElementById(this.nombre_instancia+"_zona_uso_cfdi"), this.nombre_instancia+".getSelectorUsos()");
        if(this.idUso == 0) this.getSelectorUsos().setTextoDeOpcionSeleccionada("Selecciona Uso del CFDI");
        this.getSelectorUsos().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoUsosCFDI()");
        this.getSelectorUsos().muestraComponente();
        
        //catálogo de conceptos a facturar, solo se mostrará cuando se esté editando el gasto
        if(this.idGasto > 0){
            this.getCatalogoDeConceptos().setComponente(document.getElementById(this.nombre_instancia+"_conceptos_facturacion"), this.nombre_instancia+".getCatalogoDeConceptos()");
            this.getCatalogoDeConceptos().muestraComponente();   
            
            this.getCatalogoFacturas().setComponente(document.getElementById(this.nombre_instancia+"_zona_facturas_emitidas"), this.nombre_instancia+".getCatalogoFacturas()");
            this.getCatalogoFacturas().muestraComponente();
            this.getCatalogoFacturas().setFuncionElementoSeleccionado(this.nombre_instancia+".facturaSeleccionada()");
        }
    }
    
    this.facturaGlobal = function(){
        this.getComponenteVisorDeFacturas().setOpcionTipoFactura(1);
        this.getComponenteVisorDeFacturas().setIdElemento(this.idGasto);
        this.getComponenteVisorDeFacturas().setModo(1);
        this.getComponenteVisorDeFacturas().muestraComponente();
    }
    
    this.recargaFacturasEmitidas = function(){
        this.getCatalogoFacturas().muestraComponente();
    }
    
    this.onConceptoSeleccionadoChange = function(){
        this.idConcepto = this.getCatalogoSelectorConceptos().getIdConceptoSeleccionado();
    }
    
    this.actualizaDatosFacturacion = function(){
        
    }
    
    this.editaComponente = function(){
        if(!this.info){
            getResultadoAjax("opcion=35&idgasto="+this.idGasto, this.nombre_instancia+".procesaInfoRecuperada", this.contenedor);
            return;
        }
        
        this.idGasto = this.info[0];
        this.catalogoFacturas = null;
        this.muestraComponente();
        
        //return idGastoCliente, idPromotor, idEmpresa, idCliente, idBanco, fecha, hora, cantidadTotal, porcentaje, idUsuario, estatus, promotor, empresa, cliente, banco, metodoDePago, idFormaDePago, moneda, tipoCambio18, usoCFDI19, formaPago20, tieneXML21, descripcionUsoCFDI22
        
        this.selector_fecha.setFechaDesdeDB(this.info[5]);
        document.getElementById(this.nombre_instancia+"_campo_cantidad").value = this.info[7];
        document.getElementById(this.nombre_instancia+"_campo_porcentaje").value = this.info[8];
        
        this.getSelectorClientes().setTextoDeOpcionSeleccionada(this.info[13]);
        this.getSelectorBancos().setTextoDeOpcionSeleccionada(this.info[14]);
        this.getSelectorEmpresa().setTextoDeOpcionSeleccionada(this.info[12]);
        this.getSelectorPromotor().setTextoDeOpcionSeleccionada(this.info[11]);
        
        this.getSelectorClientes().muestraComponente();
        this.getSelectorBancos().muestraComponente();
        this.getSelectorEmpresa().muestraComponente();
        this.getSelectorPromotor().muestraComponente();
        
        this.clienteSeleccionado = this.info[3];
        this.idBancoSeleccionado = this.info[4];
        this.idEmpresaSeleccionada = this.info[2];
        this.idPromotorSeleccionado = this.info[1];
        
        if(this.info[10] == 2 || this.info[21] == 1) document.getElementById(this.nombre_instancia+"_ya_facturada").click();
        
        var comboMetodoPago = document.getElementById(this.nombre_instancia+"_selector_metodo_pago");
        
        //console.log(this.info[15]+","+this.info[16]+","+this.info[17]+","+this.info[18]);
        if(this.info[15] == "PUE") comboMetodoPago.selectedIndex = 1;
        else comboMetodoPago.selectedIndex = 2;
        this.cambiaMetodoPago(comboMetodoPago);
        
        if(this.info[16] != "" && this.info[16] != " "){
            this.idFormaPago = this.info[16];
            this.getSelectorFormasPago().setTextoDeOpcionSeleccionada(this.info[20]);
            this.getSelectorFormasPago().muestraComponente();   
        }
        
        if(this.info[17] != " " && this.info[17] != ""){
            this.idMoneda = this.info[17];
            this.getSelectorMoneda().setTextoDeOpcionSeleccionada(this.idMoneda);
            this.getSelectorMoneda().muestraComponente();   
        }
        
        if(this.info[18] != " " && this.info[18] != ""){
            document.getElementById(this.nombre_instancia+"_tipo_cambio").value = this.info[18];
        }
        
        this.idUso = this.info[19];
        this.getSelectorUsos().setTextoDeOpcionSeleccionada(this.info[19]+" - "+this.info[22]);
    }
    
    this.procesaInfoRecuperada = function(respuesta){
        //console.log("info: "+respuesta);
        
        this.info = respuesta.split("|");
        this.editaComponente();
    }
    
    this.getZonaBotonGuardar = function(){
        return document.getElementById(this.nombre_instancia+"_zona_boton_guardar");
    }
    
    this.validaForma = function(){
        var fecha = this.selector_fecha.getFechaActualParaDB();
        
        if(this.clienteSeleccionado == 0){
            muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar un cliente para este gasto");
            return;
        }
        
        if(this.idPromotorSeleccionado == 0){
            muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar un promotor para este gasto");
            return;
        }
        
        if(this.idEmpresaSeleccionada == 0){
            muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar una empresa para este gasto");
            return;
        }
        
        if(this.idBancoSeleccionado == 0){
            muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar un banco para este gasto");
            return;
        }
        
        var campoCantidad = document.getElementById(this.nombre_instancia+"_campo_cantidad");
        if(campoCantidad.value.length == 0 || campoCantidad.value == "" || campoCantidad.value == " " || !parseFloat(campoCantidad.value)){
            campoCantidad.focus();
            return;
        }
        
        var campoPorcentaje = document.getElementById(this.nombre_instancia+"_campo_porcentaje");
        if(campoPorcentaje.value.length <= 0 || campoPorcentaje.value == " " || isNaN(campoPorcentaje.value) || parseFloat(campoPorcentaje.value) < 0.0 || parseFloat(campoPorcentaje.value) > 1.0){
            campoPorcentaje.focus();
            return;
        }
        
        var facturada = document.getElementById(this.nombre_instancia+"_ya_facturada");
        
        var comboMetodoPago = document.getElementById(this.nombre_instancia+"_selector_metodo_pago");
        var formaPago = "99"; //por definir es por defecto
        var moneda = "MXN";
        var tipoCambio = "1";
        var metodoPago = comboMetodoPago.options[comboMetodoPago.selectedIndex].value;
        if(metodoPago == "0"){
            muestraAyudaCampo(this.nombre_instancia+"_a2", "Debes seleccionar un método de pago para este gasto");
            return;
        }
        
        if(metodoPago == "PUE"){
            formaPago = this.idFormaPago;
            moneda = this.idMoneda;
            var campoTipoCambio = document.getElementById(this.nombre_instancia+"_tipo_cambio");
            if(campoTipoCambio.value.length == 0 || campoTipoCambio.value == "" || campoTipoCambio.value == " "){
                campoTipoCambio.focus();
                return;
            }
            tipoCambio = campoTipoCambio.value;
            
            if(formaPago == 99){
                muestraAyudaCampo(this.nombre_instancia+"_a2", "Si el método de pago es 'Pago en una sola exhibición', la forma de pago no puede ser '99 - Por definir'");
                return;
            }
        }
        
        if(this.idUso == 0){
            muestraAyudaCampo(this.nombre_instancia+"_a2", "Debes seleccionar un uso para este CFDI");
            return;
        }
        
        var info = this.idGasto+"|"+this.clienteSeleccionado+"|"+this.idPromotorSeleccionado+"|"+this.idEmpresaSeleccionada+"|"+this.idBancoSeleccionado+"|"+campoCantidad.value+"|"+campoPorcentaje.value+"|"+fecha+"|"+((facturada.checked)?2:1)+"|"+metodoPago+"|"+formaPago+"|"+moneda+"|"+tipoCambio+"|"+this.idUso;
        
        //console.log("info: "+info);
        //idGasto, idCliente, promotor, idEmpresa, idbanco, cantidad, porcentaje, fecha, estatus (1 pendiente de facturacion, 2 ya facturada), método de pago, forma de pago, moneda, tipo de cambio, uso cfdi
        
        getResultadoAjax("opcion=34&info="+info, this.nombre_instancia+".respuestaElementoGuardado", this.getZonaBotonGuardar());
    }
    
    this.respuestaElementoGuardado = function(respuesta){
        //console.log("respuesta guardar: "+respuesta);
        this.getZonaBotonGuardar().innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a>";
        
        if(respuesta > 0 && !isNaN(respuesta)){
            alert("Gasto de Cliente guardado!");
            if(this.idGasto == 0) this.idGasto = respuesta;
            this.getControladorNavegacion().botonCerrarPresionado();
        }else{
            alert("No se pudo guardar el gasto de cliente, por favor intenta más tarde!");
        }
    }
    
    //FUNCIÓN CUANDO EL SELECTOR DE MÉTODO DE PAGO CAMBIA
    this.cambiaMetodoPago = function(selector){
        var zona = document.getElementById(this.nombre_instancia+"_zona_campos_metodo_pago");
        if(zona){
            if(selector.options[selector.selectedIndex].value == "PUE") $(zona).show();
            else $(zona).hide();
        }
    }
    //FUNCIÓN CUANDO EL SELECTOR DE MÉTODO DE PAGO CAMBIA
    
    /*
    METODOS PARA INVOCAR EL CATALOGO DE CLIENTES REGISTRADOS
    */
    this.muestraCatalogoDeClientes = function(){
        this.getCatalogoClientes().setComponente(null, this.nombre_instancia+".getCatalogoClientes()");
        this.getCatalogoClientes().setContenedorFormaFlotante(1);
        this.getCatalogoClientes().muestraContenedorModal('G');
        this.getCatalogoClientes().controlador_navegacion = null;
        
        this.catalogoClientes.setFuncionElementoSeleccionado(this.nombre_instancia+".clienteSeleccionadoDeCatalogo()");
        this.getCatalogoClientes().setComponente(this.getCatalogoClientes().getContenedorModal().getContenedorDeContenido(), this.getCatalogoClientes().nombre_instancia);
        this.getCatalogoClientes().muestraComponente();
    }
    
    this.clienteSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoClientes().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorClientes().setTextoDeOpcionSeleccionada(datos[1]);
            this.clienteSeleccionado = datos[0];
            this.getCatalogoClientes().getContenedorModal().ocultaComponente();
            if(this.componente_padre) this.componente_padre.setClienteSeleccionado(this.clienteSeleccionado);
        }
    }
    
    /*FUNCIONES PARA CATALOGO PROMOTORES*/
    this.muestraCatalogoPromotores = function(){
        //console.log("mostrando catalogo de promotores");
        this.getCatalogoPromotores().setComponente(null, this.nombre_instancia+".getCatalogoPromotores()");
        this.getCatalogoPromotores().setContenedorFormaFlotante(1);
        this.getCatalogoPromotores().muestraContenedorModal('G');
        this.getCatalogoPromotores().controlador_navegacion = null;
        
        this.getCatalogoPromotores().setFuncionElementoSeleccionado(this.nombre_instancia+".promotorSeleccionadoDeCatalogo()");
        this.getCatalogoPromotores().setComponente(this.getCatalogoPromotores().getContenedorModal().getContenedorDeContenido(), this.getCatalogoPromotores().nombre_instancia);
        this.getCatalogoPromotores().muestraComponente();
    }
    
    this.promotorSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoPromotores().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorPromotor().setTextoDeOpcionSeleccionada(datos[1]);
            this.idPromotorSeleccionado = datos[0];
            this.getCatalogoPromotores().getContenedorModal().ocultaComponente();
        }
    }
    
    /*FUNCIONES PARA CATALOGO DE EMPRESAS*/
    this.muestraCatalogoEmpresas = function(){
        //console.log("mostrando catalogo de empresas");
        this.getCatalogoEmpresas().setComponente(null, this.nombre_instancia+".getCatalogoEmpresas()");
        this.getCatalogoEmpresas().setContenedorFormaFlotante(1);
        this.getCatalogoEmpresas().muestraContenedorModal('G');
        this.getCatalogoEmpresas().controlador_navegacion = null;
        
        this.getCatalogoEmpresas().setFuncionElementoSeleccionado(this.nombre_instancia+".empresaSeleccionadoDeCatalogo()");
        this.getCatalogoEmpresas().setComponente(this.getCatalogoEmpresas().getContenedorModal().getContenedorDeContenido(), this.getCatalogoEmpresas().nombre_instancia);
        this.getCatalogoEmpresas().muestraComponente();
    }
    
    this.empresaSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoEmpresas().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorEmpresa().setTextoDeOpcionSeleccionada(datos[1]);
            this.idEmpresaSeleccionada = datos[0];
            this.getCatalogoEmpresas().getContenedorModal().ocultaComponente();
        }
    }
    
    /*FUNCIONES PARA BANCOS */
    this.muestraCatalogoBancos = function(){
        //console.log("mostrando catalogo de bancos");
        this.getCatalogoBancos().setComponente(null, this.nombre_instancia+".getCatalogoBancos()");
        this.getCatalogoBancos().setContenedorFormaFlotante(1);
        this.getCatalogoBancos().muestraContenedorModal('G');
        this.getCatalogoBancos().controlador_navegacion = null;
        
        this.getCatalogoBancos().setFuncionElementoSeleccionado(this.nombre_instancia+".bancoSeleccionadoDeCatalogo()");
        this.getCatalogoBancos().setComponente(this.getCatalogoBancos().getContenedorModal().getContenedorDeContenido(), this.getCatalogoBancos().nombre_instancia);
        this.getCatalogoBancos().muestraComponente();
    }
    
    this.bancoSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoBancos().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorBancos().setTextoDeOpcionSeleccionada(datos[1]);
            this.idBancoSeleccionado = datos[0];
            this.getCatalogoBancos().getContenedorModal().ocultaComponente();
        }
    }
    
    //FUNCIONES PARA OCULTAR LOS CAMPOS DEPENDIENDO DEL MÉTODO DE PAGO
    this.setCamposParaMetodoPago = function(){
        
    }
    //FUNCIONES PARA OCULTAR LOS CAMPOS DEPENDIENDO DEL MÉTODO DE PAGO
    
    //FUNCIONES PARA SELECTOR DE FORMA DE PAGOS
    this.formaPagoSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoFormasPago().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorFormasPago().setTextoDeOpcionSeleccionada(datos[2]);
            this.idFormaPago = datos[1];
            this.getCatalogoFormasPago().getContenedorModal().ocultaComponente();
        }
    }
    
    //Catalogo Formas de Pago
    this.muestraCatalogoFormasDePago = function(){
        this.getCatalogoFormasPago().setComponente(null, this.nombre_instancia+".getCatalogoFormasPago()");
        this.getCatalogoFormasPago().setContenedorFormaFlotante(1);
        this.getCatalogoFormasPago().muestraContenedorModal('G');
        this.getCatalogoFormasPago().controlador_navegacion = null;
        
        this.getCatalogoFormasPago().setFuncionElementoSeleccionado(this.nombre_instancia+".formaPagoSeleccionadoDeCatalogo()");
        this.getCatalogoFormasPago().setComponente(this.getCatalogoFormasPago().getContenedorModal().getContenedorDeContenido(), this.getCatalogoFormasPago().nombre_instancia);
        this.getCatalogoFormasPago().muestraComponente();
    }
    //FUNCIONES PARA SELECTOR DE FORMA DE PAGOS
    
    //FUNCIONES PARA SELECTOR DE MONEDAS
    this.muestraCatalogoMonedas = function(){
        this.getCatalogoMonedas().setComponente(null, this.nombre_instancia+".getCatalogoMonedas()");
        this.getCatalogoMonedas().setContenedorFormaFlotante(1);
        this.getCatalogoMonedas().muestraContenedorModal('G');
        this.getCatalogoMonedas().controlador_navegacion = null;
        
        this.getCatalogoMonedas().setFuncionElementoSeleccionado(this.nombre_instancia+".monedaSeleccionadoDeCatalogo()");
        this.getCatalogoMonedas().setComponente(this.getCatalogoMonedas().getContenedorModal().getContenedorDeContenido(), this.getCatalogoMonedas().nombre_instancia);
        this.getCatalogoMonedas().muestraComponente();
    }
    
    this.monedaSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoMonedas().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorMoneda().setTextoDeOpcionSeleccionada(datos[2]+" ("+datos[1]+")");
            this.idMoneda = datos[1];
            this.getSelectorMoneda().getContenedorModal().ocultaComponente();
            
            this.setZonaTipoCambio();
        }
    }
    
    this.setZonaTipoCambio = function(){
        var zonaTipoCambio = document.getElementById(this.nombre_instancia+"_zona_tipo_cambio");
        if(zonaTipoCambio) {
            if(this.idMoneda != "MXN"){
                $(zonaTipoCambio).show();
            }else{
                $(zonaTipoCambio).hide();
            }   
        }
    }
    //FUNCIONES PARA SELECTOR DE MONEDAS
    
    //FACTURA SELECCIONADA EN CATALOGO DE FACTURAS EMITIDAS
    this.facturaSeleccionada = function(){
        var datos = this.getCatalogoFacturas().getDatosDeElementoSeleccionadoDeCatalogo();
        //console.log("datos factura seleccionada");
        //console.log(datos);
        if(datos){
            //idFacturaEmitida, nombreUsuario, cantidadFacturada, fecha, hora, textoEstatus, tipoFactura, idElemento, estatus
            this.getComponenteVisorDeFacturas().setOpcionTipoFactura(1);
            this.getComponenteVisorDeFacturas().setIdFactura(datos[0]);
            this.getComponenteVisorDeFacturas().setIdElemento(this.idGasto);
            this.getComponenteVisorDeFacturas().funcionFacturasActualizadas(this.nombre_instancia+".recargaFacturasEmitidas()");
            
            if(datos[8] == 1){
                this.getComponenteVisorDeFacturas().setModo(2);
                this.getComponenteVisorDeFacturas().muestraComponente();
            }else if(datos[8] == -2){
                this.getComponenteVisorDeFacturas().setModo(3);
                this.getComponenteVisorDeFacturas().muestraComponente();
            }
        }
    }
    //FACTURA SELECCIONADA EN CATALOGO DE FACTURAS EMITIDAS
    
    //FUNCIONES PARA SELECTOR Y CATALOGO DE USOS CFDIS
    this.muestraCatalogoUsosCFDI = function(){
        //console.log("mostrando catalogo de usos cfdis");
        this.getCatalogoUsos().setComponente(null, this.nombre_instancia+".getCatalogoUsos()");
        this.getCatalogoUsos().setContenedorFormaFlotante(1);
        this.getCatalogoUsos().muestraContenedorModal('G');
        this.getCatalogoUsos().controlador_navegacion = null;
        
        this.getCatalogoUsos().setFuncionElementoSeleccionado(this.nombre_instancia+".usoCFDISeleccionado()");
        this.getCatalogoUsos().setComponente(this.getCatalogoUsos().getContenedorModal().getContenedorDeContenido(), this.getCatalogoUsos().nombre_instancia);
        this.getCatalogoUsos().muestraComponente();
    }
    
    this.usoCFDISeleccionado = function(){
        var datos = this.getCatalogoUsos().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorUsos().setTextoDeOpcionSeleccionada(datos[1]+" - "+datos[2]);
            this.getCatalogoUsos().getContenedorModal().ocultaComponente();
            this.idUso = datos[1];
        }
    }
    //FUNCIONES PARA SELECTOR Y CATALOGO DE USOS CFDIS
}
var CatalogoFacturasPendientes = function(){
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Facturas Pendientes");
        this.setTextoAgregarElemento("Agregar nuevo cliente");
        
		//this.setFormaCaptura(new FormaCliente);
        
        //if(this.soloSeleccion == 0) this.muestraEstructuraCatalogoPrincipalConOpcionAgregarNuevo();
        //else this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo();
        
        this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo();
        
        //realizamos la búsqueda en la base de datos de pacientes
        getResultadoAjax("opcion=37", this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
        
        //mostramos las opciones de impresion
        if(this.soloSeleccion == 0) this.mostraOpcionesDeImpresion();
    }
    
    this.procesaRespuestaEstatusDeGastoCambiado = function(respuesta){
        //console.log("respuesta servidor: "+respuesta);
        if(respuesta != 1){
            alert("No se actualizó el estatus de este gasto, por favor vuelve a intentar!");
        }
    }
    
    this.cambiaEstatusDeFactura = function(idGasto, checkbox){
        getResultadoAjax("opcion=38&idGasto="+idGasto+"&estatus="+((checkbox.checked)?2:1), this.nombre_instancia+".procesaRespuestaEstatusDeGastoCambiado", null);
    }
    
    this.muestraResultados = function(respuesta){
        //idGastoCliente, nombrePromotor, nombreEmpresa, nombreCliente, nombreBanco, fecha, hora, total, porcentaje, estatus
        //console.log("por facturar: "+respuesta);
        var porFacturar = respuesta.split("^");
        porFacturar.pop();
        if(porFacturar.length == 0){
            var contenido = "<div>No hay gastos pendientes por facturar aún</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        //agregamos el selector para marcar si ya fue facturada
        var nuevosGastos = "";
        var gasto;
        var selectorFactura = "";
        
        for(var i=0; i<porFacturar.length; i++){
            gasto = porFacturar[i].split("|");
            
            contenido = '<div class="switch">';
            contenido += '<label>';
            contenido += '<input type="checkbox" onclick="'+this.nombre_instancia+'.cambiaEstatusDeFactura('+gasto[0]+', this)">';
            contenido += '<span class="lever"></span>';
            contenido += '</label></div>';
            
            nuevosGastos += gasto[0]+"|"+gasto[1]+"|"+gasto[2]+"|"+gasto[3]+"|"+gasto[4]+"|"+gasto[5]+"|"+contenido+"^";
        }
        
        var gastos = nuevosGastos.split("^");
        gastos.pop();
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(gastos);
        
        var encabezados = new Array("Empresa", "Cliente", "Banco", "Fecha", "Cantidad", "Facturada");
        var medidas = new Array("15%", "15%", "15%", "15%", "10%", "10%");
        var alineaciones = new Array("left", "left","left", "center","right", "center");
        //"Empresa", "Cliente", "Banco", "Fecha", "Cantidad", "Porcentaje", "Estatus"
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, this.nombre_instancia+".eliminaElemento()", null);
        
        //if(this.soloSeleccion == 0) this.getListaSeleccionable().setOpcionBorrar(true);
        //else this.getListaSeleccionable().setOpcionBorrar(false);
        this.getListaSeleccionable().setOpcionBorrar(false);
        this.getListaSeleccionable().setIndiceDeColumnaSinEventoOnclick(5);
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
    
    this.elementoSeleccionado = function(){
        var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        
        var formaGasto = new FormaGastos();
        formaGasto.setIdGasto(datos[0]);
        this.setFormaCaptura(formaGasto);
        this.nuevoElemento();
        
        formaGasto.editaComponente();
    }
}
var CatalogoPromotores = function(){
    
    this.rango_fechas = null;
    this.getRangoDeFechas = function(){
        if(!this.rango_fechas) this.rango_fechas = new SelectorRangoDeFecha();
        return this.rango_fechas;
    }
    
    //promotores
    this.idPromotorSeleccionado = 0;
    this.selectorPromotor = null;
    this.getSelectorPromotor = function(){
        if(!this.selectorPromotor){
            this.selectorPromotor = new SelectorDeOpcion();
            this.selectorPromotor.setVisualizacion(1);
        }
        return this.selectorPromotor;
    }
    
    this.catalogoPromotores = null;
    this.getCatalogoPromotores = function(){
        if(!this.catalogoPromotores){
            this.catalogoPromotores = new CatalogoUsuarios();
            this.catalogoPromotores.setSoloSeleccion(1);
            this.catalogoPromotores.setFiltroTipoUsuarios(3);
        }
        return this.catalogoPromotores;
    }
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Facturas de Promotores");
        this.muestraEstructuraCatalogoPrincipalConBarraOpcionesSinOpcionNuevo();
        
        var contenido = "<div id='"+this.nombre_instancia+"_zona_promotores'></div><br/><table align='center' width='100%' cellpadding='3' cellspacing='0'>";
        contenido += "<tr>";
        
        contenido += "<td align='center' width='90%' valign='top'>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_rango_fecha'></div>";
        
        contenido += "</td>";
        contenido += "<td align='center' width='10%' valign='bottom'>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_boton_todos'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".getTodosLosElementos()' style='margin:0 auto 0 auto;'>Todos</a></div>";
        
        contenido += "</td>";
        contenido += "</tr></table><br/>";
        
        this.getZonaSecundaria().innerHTML = contenido;        
        this.getRangoDeFechas().setComponente(document.getElementById(this.nombre_instancia+"_zona_rango_fecha"), this.nombre_instancia+".getRangoDeFechas()");
        this.getRangoDeFechas().setFuncionCambiaRango(this.nombre_instancia+".onDateRangeChange()");
        this.getRangoDeFechas().muestraComponente();
        
        //selector promotor
        if(orbe.login.getPerfil() != 3){
            this.getSelectorPromotor().setComponente(document.getElementById(this.nombre_instancia+"_zona_promotores"), this.nombre_instancia+".getSelectorPromotor()");
        
            this.getSelectorPromotor().setTextoDeOpcionSeleccionada("Selecciona Promotor");
            this.getSelectorPromotor().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoPromotores()");

            this.getSelectorPromotor().muestraComponente();   
        }
        
        this.onDateRangeChange();
        
        //mostramos las opciones de impresion
        if(this.soloSeleccion == 0) this.mostraOpcionesDeImpresion();
    }
    
    this.getTodosLosElementos = function(){
        this.getRangoDeFechas().setFechaInicial("2017-01-01");
        this.getRangoDeFechas().setFechaFinal(getFechaActual());
        this.onDateRangeChange();
    }
    
    this.onDateRangeChange = function(){
        var fecha_inicio = this.getRangoDeFechas().getFechaInicio();
        var fecha_fin = this.getRangoDeFechas().getFechaFin();
        getResultadoAjax("opcion=39&fechaInicio="+fecha_inicio+"&fechaFinal="+fecha_fin+"&idpromotor="+this.idPromotorSeleccionado, this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
    }
    
    /*
    FUNCIONES PARA MANEJA DE CATALOGO DE PROMOTORES
    */
    this.muestraCatalogoPromotores = function(){
        //console.log("mostrando catalogo de promotores");
        this.getCatalogoPromotores().setComponente(null, this.nombre_instancia+".getCatalogoPromotores()");
        this.getCatalogoPromotores().setContenedorFormaFlotante(1);
        this.getCatalogoPromotores().muestraContenedorModal('G');
        this.getCatalogoPromotores().controlador_navegacion = null;
        
        this.getCatalogoPromotores().setFuncionElementoSeleccionado(this.nombre_instancia+".promotorSeleccionadoDeCatalogo()");
        this.getCatalogoPromotores().setComponente(this.getCatalogoPromotores().getContenedorModal().getContenedorDeContenido(), this.getCatalogoPromotores().nombre_instancia);
        this.getCatalogoPromotores().muestraComponente();
    }
    
    this.promotorSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoPromotores().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorPromotor().setTextoDeOpcionSeleccionada(datos[1]);
            this.idPromotorSeleccionado = datos[0];
            this.getCatalogoPromotores().getContenedorModal().ocultaComponente();
            this.onDateRangeChange();
        }
    }
    
    this.muestraResultados = function(respuesta){
        //idGastoCliente, nombrePromotor, nombreEmpresa, nombreCliente, nombreBanco, fecha, hora, total, porcentaje, estatus
        //console.log("gastos promotores: "+respuesta);
        var gastos = respuesta.split("^");
        gastos.pop();
        if(gastos.length == 0){
            var contenido = "<div>No hay gastos para este promotor registrados en el sistema aún</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(gastos);
        
        //idGastoCliente, nombrePromotor, nombreCliente, nombreBanco, fecha, total, porcentaje, beneficio
        var encabezados = new Array("Promotor", "Cliente", "Banco", "Fecha", "Total", "Porcentaje", "Beneficio");
        var medidas = new Array("15%", "15%", "10%", "10%", "10%", "10%", "10%");
        var alineaciones = new Array("left", "left", "center", "right", "right", "right", "right");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, null, null, this.nombre_instancia+".eliminaElemento()", null);
        this.getListaSeleccionable().setOpcionBorrar(false);
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
}
var CatalogoConceptosDeGasto = function(){
    
    this.idGasto = 0;
    this.setIdGasto = function(idGasto){
        this.idGasto = idGasto;
    }
    
    this.catalogoConceptosDeFacturacion = null;
    this.getCatalogoConceptosDeFacturacion = function(){
        if(!this.catalogoConceptosDeFacturacion) this.catalogoConceptosDeFacturacion = new CatalogoConceptosDeFacturacion();
        return this.catalogoConceptosDeFacturacion;
    }
    
    //lista con los conceptos de gastos
    this.listaConceptosDeGasto = null;
    
    this.muestraComponente = function(){
        if(this.soloSeleccion == 0) this.setTituloCatalogo("Conceptos a facturar");
        else this.setTituloCatalogo("Conceptos a facturar");
        
        this.setTextoAgregarElemento("Agregar un concepto a la factura");
        
		this.setFormaCaptura(null);
        
        if(this.soloSeleccion == 0) this.muestraEstructuraCatalogoPrincipalConOpcionAgregarNuevo();
        else this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo();
        
        //realizamos la búsqueda en la base de datos de pacientes
        if(this.idGasto > 0) getResultadoAjax("opcion=47&idGasto="+this.idGasto, this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
    }
    
    //sobreescribimos el comportamiento para el botón nuevo
    this.nuevoElemento = function(){
        this.getCatalogoConceptosDeFacturacion().setComponente(null, this.nombre_instancia+".getCatalogoConceptosDeFacturacion()");
        this.getCatalogoConceptosDeFacturacion().muestraContenedorModal('G');
        this.getCatalogoConceptosDeFacturacion().controlador_navegacion = null;
        
        this.getCatalogoConceptosDeFacturacion().setFuncionElementoSeleccionado(this.nombre_instancia+".conceptoDeFacturacionSeleccionado()");
        this.getCatalogoConceptosDeFacturacion().setComponente(this.getCatalogoConceptosDeFacturacion().getContenedorModal().getContenedorDeContenido(), this.getCatalogoConceptosDeFacturacion().nombre_instancia);
        this.getCatalogoConceptosDeFacturacion().muestraComponente();
    }
    
    this.procesaConceptoAgregado = function(respuesta){
        //console.log("respuesta concepto agregado: "+respuesta);
        
        this.muestraComponente();
    }
    
    this.conceptoDeFacturacionSeleccionado = function(){
        var idConcepto = this.getCatalogoConceptosDeFacturacion().getIdConceptoSeleccionado();
        
        //verificamos que no exista ya este concepto en la lista, si existe entonces no lo agregamos.
        if(this.listaConceptosDeGasto){
            var conceptos = this.listaConceptosDeGasto.split("^");
            conceptos.pop();
            var concepto;
            for(var i=0; i<conceptos.length; i++){
                concepto = conceptos[i].split("|");
                //console.log("idconcepto: "+concepto[8]+" idConcepto: "+idConcepto);
                if(concepto[8] == idConcepto){
                    this.getCatalogoConceptosDeFacturacion().getContenedorModal().ocultaComponente();
                    return;
                }
            }   
        }
        
        //si ya está aquí, entonces quiere decir que no está repetido y entonces si deberá agregarlo a la lista de gastos de este concepto específico. De esta forma, aparecerá en los conceptos específicos de este gasto y quedará tambié en el católogo global de conceptos de facturación
        this.getCatalogoConceptosDeFacturacion().getContenedorModal().ocultaComponente();
        getResultadoAjax("opcion=46&idGasto="+this.idGasto+"&idConcepto="+idConcepto, this.nombre_instancia+".procesaConceptoAgregado", this.contenedor);
        
        /*
        //si ya está aquí, entonces quiere decir que no está repetido y entonces si deberá agregarlo
        //console.log("datos: "+this.getCatalogoConceptosDeFacturacion().getDatosFormaDeCaptura());
        
        //idConceptoDeGasto, Unidad, Producto, Descripción, P. Unitario, Cantidad, Total, descuento, idConcepto
        //cuando se agregan, se agregan con 0 como identificador de concepto de gasto
        //2|50121500|H87|Lata de pescado|18.5999|3|0|1|Pieza|Pescado
        var datos = this.getCatalogoConceptosDeFacturacion().getDatosFormaDeCaptura().split("|");
        var nuevoConcepto = "0|"+datos[8]+"|"+datos[9]+"|"+datos[3]+"|"+datos[4]+"|"+datos[5]+"|$"+getCifraLegible((datos[4]*datos[5]) - datos[6])+"|"+datos[6]+"|"+datos[0]+"^";
        
        if(!this.listaConceptosDeGasto) this.listaConceptosDeGasto = "";
        this.listaConceptosDeGasto += nuevoConcepto;
        this.getCatalogoConceptosDeFacturacion().getContenedorModal().ocultaComponente();
        this.muestraResultados(this.listaConceptosDeGasto);*/
    }
    
    //mostramos el catálogo de conceptos de facturación de gastos
    this.muestraResultados = function(respuesta){
        //console.log("conceptos facturacion de gasto: "+respuesta);
        ////idConcepto, nombreUnidad, nombreProducto, descripcion, precioUnitario, cantidad, descuento, total, idUnidad, idProducto
        
        var conceptos = respuesta.split("^");
        conceptos.pop();
        if(conceptos.length == 0){
            //if(!this.getZonaElementosDelCatalogo()) return;
            var contenido = "<div>No hay conceptos de facturación registrados para este gasto</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        var nuevosConceptos = "";
        var concepto = null;
        var tablaPrecio = "";
        for(var i=0; i<conceptos.length; i++){
            concepto = conceptos[i].split("|");
            
            tablaPrecio = "<table align='center' cellpadding='3' cellspacing='0'>"
            tablaPrecio += "<tr>";
            
            tablaPrecio += "<td with='50%' align='left'>P. Unitario:</td>";
            tablaPrecio += "<td with='50%' align='right'>$"+getCifraLegible(concepto[4])+"</td>";
            
            tablaPrecio += "</tr>";
            tablaPrecio += "<tr>";
            
            tablaPrecio += "<td with='50%' align='left'>Cantidad:</td>";
            tablaPrecio += "<td with='50%' align='right'>"+concepto[5]+"</td>";
            
            tablaPrecio += "</tr>";
            tablaPrecio += "<tr>";
            
            tablaPrecio += "<td with='50%' align='left'>Sub Total:</td>";
            tablaPrecio += "<td with='50%' align='right'>$"+getCifraLegible(concepto[4] * concepto[5])+"</td>";
            
            tablaPrecio += "</tr>";
            tablaPrecio += "<tr>";
            
            tablaPrecio += "<td with='50%' align='left'>I.V.A:</td>";
            tablaPrecio += "<td with='50%' align='right'>$"+getCifraLegible((concepto[4] * concepto[5])*0.16)+"</td>";
            
            tablaPrecio += "</tr>";
            tablaPrecio += "<tr>";
            
            tablaPrecio += "<td with='50%' align='left'>Descuento:</td>";
            tablaPrecio += "<td with='50%' align='right'>$"+getCifraLegible(concepto[6])+"</td>";
            
            tablaPrecio += "</tr>";
            tablaPrecio += "<tr>";
            
            tablaPrecio += "<td with='50%' align='left'>Total:</td>";
            tablaPrecio += "<td with='50%' align='right'>$"+getCifraLegible(((concepto[4] * concepto[5])*1.16)-concepto[6])+"</td>";
            
            tablaPrecio += "</tr>";
            tablaPrecio += "</table>";
            
            nuevosConceptos += concepto[0]+"|"+concepto[1]+"|"+concepto[2]+"|"+concepto[3]+"|"+tablaPrecio+"|"+concepto[8]+"|"+concepto[9]+"^";
        }
        
        conceptos = nuevosConceptos.split("^");
        conceptos.pop();
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(conceptos);
        
        var encabezados = new Array("Unidad", "Producto", "Descripción", "Precio");
        var medidas = new Array("10%", "20%", "35%", "15%");
        var alineaciones = new Array("left","left","left","right");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, this.nombre_instancia+".eliminaElemento()", null);
        
        this.getListaSeleccionable().setOpcionBorrar(true);
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
    
    this.recargaCatalogoConceptosDeFacturacion = function(){
        //console.log("recargando catalogo de conceptos");
        this.muestraComponente();
    }
    
    this.elementoSeleccionado = function(){
        //console.log("concepto de facturacion seleccionado");
        var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        
        var formaConcepto = new FormaConceptoDeFacturacion();
        this.setFormaCaptura(formaConcepto);
        
        formaConcepto.setComponente(null, this.nombre_instancia+".getFormaCaptura()");
        formaConcepto.muestraContenedorModal('G');
        formaConcepto.controlador_navegacion = null;
        
        if(datos[0] == 0){
            formaConcepto.setIdConceptoFacturacion(datos[8]);
            formaConcepto.setIdPrestado(true);
        }else formaConcepto.setIdConceptoFacturacion(datos[0]);
        
        formaConcepto.setIdGasto(this.idGasto);
        formaConcepto.setFuncionActualizarConcepto(this.nombre_instancia+".recargaCatalogoConceptosDeFacturacion()");
        formaConcepto.setComponente(formaConcepto.getContenedorModal().getContenedorDeContenido(), formaConcepto.nombre_instancia);
        formaConcepto.editaComponente();
    }
    
    this.eliminaElemento = function(){
        //si llega aqui, es por que se confirmó la acción de eliminar el elemento
        var idelemento = this.getListaSeleccionable().getIdElementoEliminado();
        getResultadoAjax("opcion=48&idConcepto="+idelemento, this.nombre_instancia+".procesaElementoEliminado", null);
    }
    
    this.procesaElementoEliminado = function(respuesta){
        //console.log("respuesta eliminar: "+respuesta);
        if(respuesta == 1){
            alert("Concepto de Facturación eliminado!");
            this.getListaSeleccionable().quitaElementoDeListaPorId(this.getListaSeleccionable().getIdElementoEliminado());
        }else{
            alert("No se pudo eliminar en este momento, por favor intenta mas tarde!");
        }
    }
}
var FormaConceptoDeFacturacion = function(){
    
    this.idConceptoFacturacion = 0;
    this.setIdConceptoFacturacion = function(idConcepto){
        this.idConceptoFacturacion = idConcepto;
    }
    
    //Catalogo Productos y Servicios
    this.idProductoServicio = 0;
    
    this.selectorProductosAndServicios = null;
    this.getSelectorProductosAndServicios = function(){
        if(!this.selectorProductosAndServicios){
            this.selectorProductosAndServicios = new SelectorDeOpcion();
            this.selectorProductosAndServicios.setVisualizacion(1);
        }
        return this.selectorProductosAndServicios;
    }
    
    this.catalogoProductosAndServicios = null;
    this.getCatalogoProductosAndServicios = function(){
        if(!this.catalogoProductosAndServicios) this.catalogoProductosAndServicios = new CatalogoProductosAndServicios();
        return this.catalogoProductosAndServicios;
    }
    
    //Catalogo de Unidades
    this.idUnidadSat = 0;
    
    this.selectorUnidad = null;
    this.getSelectorUnidad = function(){
        if(!this.selectorUnidad){
            this.selectorUnidad = new SelectorDeOpcion();
            this.selectorUnidad.setVisualizacion(1);
        }
        return this.selectorUnidad;
    }
    
    this.catalogoUnidades = null;
    this.getCatalogoUnidades = function(){
        if(!this.catalogoUnidades) this.catalogoUnidades = new CatalogoUnidadesSat();
        return this.catalogoUnidades;
    }
    
    //información del concepto de facturación
    this.infoConceptoFacturacion = null;
    
    //idGasto si es 0, entonces pertenece al catálogo global de conceptos. Si tiene un idGasto definido, entonces corresponde a un gasto particular
    this.idGasto = 0;
    this.setIdGasto = function(idGasto){
        this.idGasto = idGasto;
    }
    
    //este valor sirve para indicar que el id con el que se está recuperando la info de este gasto es un id de concepto de facturación global, por lo que deberá liberarlo en cuanto se recupere la info, para que no sobreescriba los valores del concepto global y pueda crearse una copia de este concepto como parte del gasto al que se está agregando. Por defecto está en falso
    this.idPrestado = false;
    this.setIdPrestado = function(valor){
        this.idPrestado = valor;
    }
    
    //funcion invocada cuando se actualiza el concepto
    this.funcionActualizarConcepto = null;
    this.setFuncionActualizarConcepto = function(funcion){
        this.funcionActualizarConcepto = funcion;
    }
    
    //función para cuando se presiona el botón agregar del concepto
    this.funcionAgregarConcepto = null;
    this.setFuncionAgregarConcepto = function(funcion){
        this.funcionAgregarConcepto = funcion;
    }
    
    this.muestraComponente = function(){
        var contenido = "<br/>";    
        
        //campos para llenar concepto en cfdi: claveProductosServicios (catalogo), NoIdentificación (interno del cliente o sistema), cantidad, claveUnidad(catalogo), descripcion = Venta, valorUnitario (este valor será el valor sin IVA), Importe (cantidad * valorUnitario), descuento (en caso de haberlo), 
        
        if(this.idConceptoFacturacion == 0) contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Nuevo Concepto de Facturación</div>";
        else contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Editando Concepto de Facturación</div>";
        
        contenido += "<div class='separador'></div>";
        contenido += "<br/>";    
        
        //selector de cliente
        contenido += '<div class="row center-align">';
        contenido += '<div class="col l12 center-align">';
        
        contenido += "<div class='texto_forma'>Clave Productos y Servicios (SAT)</div>";
        contenido += "<br/>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_concepto_global'></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        //claveUnidad y cantidad
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s12 center-align">';
        
        contenido += "<div class='texto_forma'>Clave Unidad</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_clave_unidad'></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        //descripción del producto que se va a agregar
        contenido += '<div class="row center-align">';
        contenido += '<div class="col l12 center-align">';
        
        contenido += "<div class='texto_forma'>Descripción</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_descripcion' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe la descripción del producto o servicio\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Valor Unitario</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_valor_unitario' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el valor unitario (sin I.V.A.) del producto. Solo números (0-9) y punto decimal permitido\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\"); "+this.nombre_instancia+".calculaTotal();'/>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Cantidad</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_cantidad' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el número de unidades de este producto. Solo números (0-9) y punto decimal permitido\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\"); "+this.nombre_instancia+".calculaTotal();'/>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        //descuento
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Descuento</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_descuento' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe el descuento del producto si aplica. Solo números (0-9) y punto decimal permitido\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\"); "+this.nombre_instancia+".calculaTotal();'/>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".desglosarIva()' style='margin:0 auto 0 auto;'>Desglosar IVA</a></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += "<br/>";
        contenido += "<div class='ayuda_campos' id='"+this.nombre_instancia+"_a1'></div>";
        contenido += "<br/>";
        
        //total con iva y multiplicado por la cantidad
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s12 center-align">';
        
        contenido += "<div class='texto_forma' id='"+this.nombre_instancia+"_total'><b>Total: $</b></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        //boton guardar y agregar
        contenido += '<div class="row center-align">';
        contenido += '<div class="col l6 center-align">';
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_boton_guardar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma(1)' style='margin:0 auto 0 auto;'>Guardar</a></div>";
        
        contenido += '</div>';
        contenido += '<div class="col l6 center-align">';
        
        //solo se pueden agregar los conceptos que tienen idGasto en 0, en otro caso no se pueden agregar por que ya están agregados, solo se pueden modificar
        if(this.idConceptoFacturacion > 0 && this.idGasto == 0){
            contenido += "<div id='"+this.nombre_instancia+"_zona_boton_agregar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma(2)' style='margin:0 auto 0 auto;'>Agregar</a></div>";   
        }
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += "<br/>";
        
        this.contenedor.innerHTML = contenido;
        
        //selector productos o servicios
        this.getSelectorProductosAndServicios().setComponente(document.getElementById(this.nombre_instancia+"_zona_concepto_global"), this.nombre_instancia+".getSelectorProductosAndServicios()");
        
        if(this.idProductoServicio == 0) this.getSelectorProductosAndServicios().setTextoDeOpcionSeleccionada("Selecciona Clave de Producto o Servicio");
        this.getSelectorProductosAndServicios().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoDeProductosAndServicios()");
        
        this.getSelectorProductosAndServicios().muestraComponente();
        
        //selector de unidad sat
        this.getSelectorUnidad().setComponente(document.getElementById(this.nombre_instancia+"_zona_clave_unidad"), this.nombre_instancia+".getSelectorUnidad()");
        
        if(this.idUnidadSat == 0) this.getSelectorUnidad().setTextoDeOpcionSeleccionada("Selecciona Unidad");
        this.getSelectorUnidad().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoDeUnidadesSat()");
        
        this.getSelectorUnidad().muestraComponente();
    }
    
    //esta función hace el desglose del iva considerando el valor unitario del concepto
    this.desglosarIva = function(){
        //tomamos el valor del campo valor unitario para desglosarlo
        var campoValorUnitario = document.getElementById(this.nombre_instancia+"_campo_valor_unitario");
        var valor = campoValorUnitario.value;
        if(!validateDecimal(valor)){
            alert("El valor unitario debe ser un número 0-9 y . (punto decimal) permitidos");
            return;
        }
        
        campoValorUnitario.value = getSoloDosCifrasDespuesDelPunto(valor / 1.16);
        this.calculaTotal();
    }
    
    //valida los datos de la forma antes de guardar o agregar a los conceptos
    this.validaForma = function(opcion){
        if(this.idProductoServicio == 0){
            muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar la clave de productos y servicios de SAT");
            return;
        }
        
        if(this.idUnidadSat == 0){
            muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar la unidad SAT");
            return;
        }
        
        var descripcion = document.getElementById(this.nombre_instancia+"_campo_descripcion");
        if(descripcion.value == "" || descripcion.value == " "){
            descripcion.focus();
            return;
        }
        
        var campoValorUnitario = document.getElementById(this.nombre_instancia+"_campo_valor_unitario");
        if(!validateDecimal(campoValorUnitario.value)){
            campoValorUnitario.focus();
            return;
        }
        
        var campoCantidad = document.getElementById(this.nombre_instancia+"_campo_cantidad");
        if(!validateDecimal(campoCantidad.value)){
            campoCantidad.focus();
            return;
        }
        
        var campoDescuento = document.getElementById(this.nombre_instancia+"_campo_descuento");
        if(campoDescuento.value.length > 0 && !validateDecimal(campoDescuento.value)){
            campoDescuento.focus();
            return;
        }
        
        var info = this.idConceptoFacturacion+"|"+this.idProductoServicio+"|"+this.idUnidadSat+"|"+descripcion.value+"|"+campoValorUnitario.value+"|"+campoCantidad.value+"|"+((campoDescuento.value == "" || campoDescuento.value == " ")?0:campoDescuento.value)+"|"+this.idGasto;
        
        //idConceptoFacturacion, idProductoServicio, idUnidad, descripcion, campoValorUnitario, campoCantidad, campoDescuento, idGasto
        
        if(opcion == 1){
            //guardamos el concepto en conceptos de facturacion para posterior consulta
            getResultadoAjax("opcion=43&info="+info, this.nombre_instancia+".procesaRespuestaActualizacion", document.getElementById(this.nombre_instancia+"_zona_boton_guardar"));
        }else{
            //console.log("funcion a ejecutar: "+this.funcionAgregarConcepto);
            if(this.funcionAgregarConcepto) setTimeout(this.funcionAgregarConcepto, 0);
        }
    }
    
    this.procesaRespuestaActualizacion = function(respuesta){
        //console.log("respuesta actualizacion: "+respuesta);
        
        if(respuesta > 0){
            if(this.idConceptoFacturacion == 0) this.idConceptoFacturacion = respuesta;
            alert("Concepto agregado!");
            this.infoConceptoFacturacion = null;
            this.editaComponente();
            
            if(this.funcionActualizarConcepto) setTimeout(this.funcionActualizarConcepto, 1000);
        }else{
            alert("No se pudo agregar el concepto, por favor intenta más tarde!");
            document.getElementById(this.nombre_instancia+"_zona_boton_guardar").innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma(1)' style='margin:0 auto 0 auto;'>Guardar</a>";
        }
    }
    
    //se calcula el total considerando el valor unitario y la cantidad de productos
    this.calculaTotal = function(){
        var campoValorUnitario = document.getElementById(this.nombre_instancia+"_campo_valor_unitario");
        var campoCantidad = document.getElementById(this.nombre_instancia+"_campo_cantidad");
        var campoDescuento = document.getElementById(this.nombre_instancia+"_campo_descuento");
        
        if(isNaN(campoValorUnitario.value)) return;
        if(isNaN(campoCantidad.value)) return;
        if(isNaN(campoDescuento.value)) return;
        
        var subtotal = (campoValorUnitario.value * campoCantidad.value) - campoDescuento.value;
        var contenido = "<b>Sub Total: $"+getCifraLegible(subtotal)+"</b>";
        
        contenido += "<br/><b>I.V.A.: $"+getCifraLegible(subtotal*0.16)+"</b>";
        contenido += "<br/><b>Total: $"+getCifraLegible(subtotal*1.16)+"</b>";
        
        document.getElementById(this.nombre_instancia+"_total").innerHTML = contenido;
    }
    
    //Producto o servicio seleccionado
    this.productoOrServicioSeleccionado = function(){
        var datos = this.getCatalogoProductosAndServicios().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorProductosAndServicios().setTextoDeOpcionSeleccionada(datos[1]+" - "+datos[2]);
            this.idProductoServicio = datos[1];
            this.getSelectorProductosAndServicios().muestraComponente();
        }
    }
    
    this.muestraCatalogoDeProductosAndServicios = function(){
        this.getCatalogoProductosAndServicios().setComponente(document.getElementById(this.nombre_instancia+"_zona_concepto_global"), this.nombre_instancia+".getCatalogoProductosAndServicios()");
        this.getCatalogoProductosAndServicios().setFuncionElementoSeleccionado(this.nombre_instancia+".productoOrServicioSeleccionado()");
        this.getCatalogoProductosAndServicios().muestraComponente();
    }
    
    //Unidad SAT
    this.muestraCatalogoDeUnidadesSat = function(){
        this.getCatalogoUnidades().setComponente(document.getElementById(this.nombre_instancia+"_zona_clave_unidad"), this.nombre_instancia+".getCatalogoUnidades()");
        this.getCatalogoUnidades().setFuncionElementoSeleccionado(this.nombre_instancia+".unidadSeleccionada()");
        this.getCatalogoUnidades().muestraComponente();
    }
    
    this.unidadSeleccionada = function(){
        var datos = this.getCatalogoUnidades().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorUnidad().setTextoDeOpcionSeleccionada(datos[1]+" - "+datos[2]);
            this.idUnidadSat = datos[1];
            this.getSelectorUnidad().muestraComponente();
        }
    }
    
    //recupera y procesa la info del concepto de facturacion
    this.procesaInfoConceptoFacturacion = function(respuesta){
        //console.log("concepto recuperado: "+respuesta);
        
        if(respuesta == "" || respuesta == " ") this.infoConceptoFacturacion = "";
        else this.infoConceptoFacturacion = respuesta;
        this.editaComponente();
    }
    
    //editar concepto de facturacion
    this.editaComponente = function(){
        if(!this.infoConceptoFacturacion){
            getResultadoAjax("opcion=45&idConceptoFacturacion="+this.idConceptoFacturacion, this.nombre_instancia+".procesaInfoConceptoFacturacion", this.contenedor);
            return;
        }
        
        this.muestraComponente();
        
        //una vez que la información ha sido recuperada, se tiene que comprobar si el id es prestado, y si es así, dejarlo en 0 para que se pueda crear el nuevo componente como parte del gasto
        if(this.idPrestado){
            //console.log("El id es prestado, ahora el id es 0");
            this.idConceptoFacturacion = 0;
        }
        
        var info = this.infoConceptoFacturacion.split("|");
        
        //idConceptoFacturacion, claveProductoServicio, claveUnidad, descripcion, valorUnitario, cantidad, descuento, estatus, descripcionUnidad, descripcionProductos
        this.getSelectorProductosAndServicios().setTextoDeOpcionSeleccionada(info[1]+" - "+info[9]);
        this.getSelectorProductosAndServicios().muestraComponente();
        
        this.getSelectorUnidad().setTextoDeOpcionSeleccionada(info[2]+" - "+info[8]);
        this.getSelectorUnidad().muestraComponente();
        
        document.getElementById(this.nombre_instancia+"_campo_descripcion").value = info[3];
        document.getElementById(this.nombre_instancia+"_campo_valor_unitario").value = info[4];
        document.getElementById(this.nombre_instancia+"_campo_cantidad").value = info[5];
        document.getElementById(this.nombre_instancia+"_campo_descuento").value = info[6];
        
        this.idProductoServicio = info[1];
        this.idUnidadSat = info[2];
        
        this.calculaTotal();
    }
}
var CatalogoConceptosDeFacturacion = function(){
    
    this.conceptos = null;
    this.idConceptoSeleccionado = 0;
    
    this.getIdConceptoSeleccionado = function(){
        return this.idConceptoSeleccionado;
    }
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Conceptos previamente usados en facturas");
        
        this.setTextoAgregarElemento("Agregar un nuevo concepto de facturación");
        
        var formaConcepto = new FormaConceptoDeFacturacion();
        formaConcepto.setFuncionAgregarConcepto(this.nombre_instancia+".conceptoSeleccionado()");
		this.setFormaCaptura(formaConcepto);
        
        this.muestraEstructuraCatalogoPrincipalConOpcionAgregarNuevo();
        
        //realizamos la búsqueda en la base de datos de pacientes
        getResultadoAjax("opcion=44", this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
    }
    
    this.muestraResultados = function(respuesta){
        //console.log("conceptos facturacion: "+respuesta);
        
        var conceptos = respuesta.split("^");
        conceptos.pop();
        if(conceptos.length == 0){
            var contenido = "<div>No hay conceptos de facturación registrados en el sistema aún</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(conceptos);
        
        var encabezados = new Array("Clave Producto", "Clave Unidad", "Descripción", "Precio Unitario");
        var medidas = new Array("15%", "15%", "30%", "15%");
        var alineaciones = new Array("center","center","left","right");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, this.nombre_instancia+".eliminaElemento()", null);
        
        if(this.soloSeleccion == 0) this.getListaSeleccionable().setOpcionBorrar(true);
        else this.getListaSeleccionable().setOpcionBorrar(false);
        
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
    
    this.eliminaElemento = function(){
        //si llega aqui, es por que se confirmó la acción de eliminar el elemento
        var idelemento = this.getListaSeleccionable().getIdElementoEliminado();
        getResultadoAjax("opcion=61&idConcepto="+idelemento, this.nombre_instancia+".procesaElementoEliminado", null);
    }
    
    this.procesaElementoEliminado = function(respuesta){
        //console.log("respuesta eliminar: "+respuesta);
        if(respuesta == 1){
            alert("Concepto de Facturación eliminado!");
            this.getListaSeleccionable().quitaElementoDeListaPorId(this.getListaSeleccionable().getIdElementoEliminado());
        }else{
            alert("No se pudo eliminar en este momento, por favor intenta mas tarde!");
        }
    }
    
    this.elementoSeleccionado = function(){
        var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        
        var formaConcepto = new FormaConceptoDeFacturacion();
        formaConcepto.setIdConceptoFacturacion(datos[0]);
        this.setFormaCaptura(formaConcepto);
        formaConcepto.setFuncionAgregarConcepto(this.nombre_instancia+".conceptoSeleccionado()");
        this.nuevoElemento();
        
        formaConcepto.editaComponente();
        
        this.idConceptoSeleccionado = datos[0];
    }
    
    this.conceptoSeleccionado = function(){
        //console.log("concepto seleccionado");
        if(this.funcionElementoSeleccionado) setTimeout(this.funcionElementoSeleccionado, 0);
    }
    
    this.getDatosFormaDeCaptura = function(){
        return this.getFormaCaptura().infoConceptoFacturacion;
    }
}
var CatalogoProductosAndServicios = function(){
    
    this.listaProductosAndServicios = null;
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Catálogo de Productos y Servicios SAT");
        this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevoConBotonBuscar();
        
        //realizamos la búsqueda en la base de datos de pacientes
        if(!this.listaProductosAndServicios) getResultadoAjax("opcion=41", this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
        else this.muestraResultados(this.listaProductosAndServicios);
    }
    
    this.muestraResultados = function(respuesta){
        var productos = respuesta.split("^");
        productos.pop();
        if(productos.length == 0){
            var contenido = "<div>No se encontró el catálogo de productos y servicios de sat</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(productos);
        
        var encabezados = new Array("Clave", "Nombre");
        var medidas = new Array("30%", "60%");
        var alineaciones = new Array("left", "left");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, null, null);
        
        this.getListaSeleccionable().setOpcionBorrar(false);
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        //this.muestraElementosDelCatalogo();
        
        this.getZonaElementosDelCatalogo().innerHTML = "<b>"+productos.length+" Productos Cargados</b>";
    }
}
var CatalogoUnidadesSat = function(){
    
    this.unidades = null;
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Catálogo de Unidades SAT");
        this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevoConBotonBuscar();
        
        //realizamos la búsqueda en la base de datos de pacientes
        if(!this.unidades) getResultadoAjax("opcion=42", this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
        else this.muestraResultados(this.unidades);
    }
    
    this.muestraResultados = function(respuesta){
        //console.log("unidades: "+respuesta);
        
        var unidades = respuesta.split("^");
        unidades.pop();
        if(unidades.length == 0){
            var contenido = "<div>No se encontró el catálogo de unidades de sat</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(unidades);
        
        var encabezados = new Array("Clave", "Nombre");
        var medidas = new Array("30%", "60%");
        var alineaciones = new Array("left", "left");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, null, null);
        
        this.getListaSeleccionable().setOpcionBorrar(false);
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        //this.muestraElementosDelCatalogo();
        
        this.getZonaElementosDelCatalogo().innerHTML = "<b>"+unidades.length+" Unidades Cargadas</b>";
    }
}
var ComponenteVisorFactura = function(){
    
    //1 visor de facturas global
    //2 viros de facturas de pago
    this.visualizacion = 1;
    this.setVisualizacion = function(valor){
        this.visualizacion = valor;
    }
    
    this.idFactura = 0;
    this.setIdFactura = function(idFactura){
        this.idFactura = idFactura;
    }
    
    //1 permite cambiar entre ambiente de prueba y ambiente de produccion
    //2 solo muestra ambiente de producción, ya que en teoría, así debe cargar las facturas que ya se tienen
    //3 solo muestra la opción enviar acuse para facturas canceladas
    this.modo = 1;
    this.setModo = function(valor){
        this.modo = valor;
    }
    
    this.uuid = null;
    this.setUUID = function(uuid){
        this.uuid = uuid;
    }
    
    this.uuid_cancelado = null;
    this.setUUIDCancelado = function(uuid){
        this.uuid_cancelado = uuid;
    }
    
    this.opcion_factura = 0;
    this.setOpcionTipoFactura = function(opcion){
        this.opcion_factura = opcion;
    }
    
    this.idElemento = 0;
    this.setIdElemento = function(idElemento){
        //console.log("idelemento: "+idElemento);
        this.idElemento = idElemento;
    }
    
    this.funcion_facturas_actualizadas = null;
    this.funcionFacturasActualizadas = function(funcion){
        this.funcion_facturas_actualizadas = funcion;
    }
    
    this.muestraComponenteParaFacturasCanceladas = function(){
        
        this.getContenedorModal().setComponente(this.nombre_instancia+".getContenedorModal()"); 
        this.getContenedorModal().muestraComponenteHuge();
        
        var contenido = "<table align='center' cellpadding='6' cellspacing='0' border='0' width='100%'>";
        contenido += "<tr>";
        contenido += "<td align='left' valign='top' width='50%'>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_info_factura'></div>";
        
        contenido += "</td>";
        contenido += "<td align='left' valign='top' width='25%'>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_envio'></div>";
        
        contenido += "</td><td align='left' valign='top' width='25%'>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_cancelacion'></div>";
        
        contenido += "</td></tr></table>";
        
        contenido += "</div>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_impresion'></div>";
        
        this.getContenedorModal().getZonaContenido().style = "overflow:hidden;";
        this.getContenedorModal().getZonaContenido().innerHTML = contenido;
        
        var tipo_propietario = 100;
        if(this.visualizacion != 1) tipo_propietario = 101;
        
        var url = "../MagicScripts/Escaneos/"+tipo_propietario+"/"+this.uuid_cancelado+"_cdfi_cancelado.pdf";
        //console.log("url: "+url);
        document.getElementById(this.nombre_instancia+"_zona_impresion").innerHTML = "<iframe src='"+url+"' width='99%' height='"+($( window ).height()*0.83)+"' id='"+this.nombre_instancia+"_iframe_pdf'><div style='background-image:url(http://sistema.hospitalmay.com/imagenes/ajax-loader-gd.gif);width:32px;height:32px;background-repeat:no-repeat;'></div></iframe>";
        
        this.getInfoFactura();
    }
    
    this.muestraComponenteParaFacturasEmitidas = function(){
        this.getContenedorModal().setComponente(this.nombre_instancia+".getContenedorModal()"); 
        this.getContenedorModal().muestraComponenteHuge();
        
        var contenido = "<br/><div style='padding-left:30px;'>";
        
        contenido += "<table align='center' cellpadding='6' cellspacing='0' border='0' width='100%'>";
        contenido += "<tr>";
        contenido += "<td align='left' valign='top' width='50%'>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_info_factura'></div>";
        
        contenido += "</td>";
        contenido += "<td align='left' valign='top' width='25%'>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_envio'></div>";
        
        contenido += "</td><td align='left' valign='top' width='25%'>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_cancelacion'></div>";
        
        contenido += "</td></tr></table>";
        
        contenido += "</div>";
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_impresion'></div>";
        
        this.getContenedorModal().getZonaContenido().style = "overflow:hidden;";
        this.getContenedorModal().getZonaContenido().innerHTML = contenido;
        
        var tipo_propietario = 100;
        if(this.visualizacion != 1) tipo_propietario = 101;
        
        var url = "../MagicScripts/Escaneos/"+tipo_propietario+"/"+this.referencia+".pdf";
        //console.log("url: "+url);
        document.getElementById(this.nombre_instancia+"_zona_impresion").innerHTML = "<iframe src='"+url+"' width='99%' height='"+($( window ).height()*0.83)+"' id='"+this.nombre_instancia+"_iframe_pdf'><div style='background-image:url(http://sistema.hospitalmay.com/imagenes/ajax-loader-gd.gif);width:32px;height:32px;background-repeat:no-repeat;'></div></iframe>";
        
        this.getInfoFactura();
    }
    
    this.muestraComponente = function(){
        this.getContenedorModal().setComponente(this.nombre_instancia+".getContenedorModal()"); 
        this.getContenedorModal().muestraComponenteHuge();
        
        var contenido = "<br/><div style='padding-left:3px; text-align:center;'>";
        
        contenido += "<div class='row' id='"+this.nombre_instancia+"_zona_botones'>";
        
        if(this.modo == 1){
            contenido += "<div class='col s4 m4 l4'>";
            
            contenido += "<select id='"+this.nombre_instancia+"_selector_tipo_facturacion' class='browser-default'>";
            contenido += "<option value='1'>Vista Previa (Sin efecto fiscal)</option>";
            contenido += "<option value='2'>Emitir CDFI</option>";
            contenido += "</select>";
            
            contenido += "</div>";
            contenido += "<div class='col s4 m4 l4'>";
            
            if(this.opcion_factura == 1){
                contenido += "<div id='"+this.nombre_instancia+"_zona_boton_facturar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Factura Global</a></div>";   
            }else if(this.opcion_factura == 2){
                contenido += "<div id='"+this.nombre_instancia+"_zona_boton_facturar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Factura Pago</a></div>";   
            }
            
            contenido += "</div>";
            contenido += "<div class='col s4 m4 l4'>";
            contenido += "</div>";   
        }
        
        contenido += "</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_impresion'></div>";
        contenido += "</div>";
        
        this.getContenedorModal().getContenedorDeContenido().style = "overflow:hidden;";
        this.getContenedorModal().getContenedorDeContenido().innerHTML = contenido;
        
        this.generaCDFI(this.opcion_factura);
    }
    
    this.generaCDFI = function(opcion){
        opcion = this.opcion_factura;
        
        var tipo_ambiente = 1;//por defecto siempre el tipo de ambiente es desarrollo
        if(this.modo == 1){
            var selector = document.getElementById(this.nombre_instancia+"_selector_tipo_facturacion");
            tipo_ambiente = selector.options[selector.selectedIndex].value;    
        }
        
        var url = "ms/Facturacion.php?idElemento="+this.idElemento+"&ambiente="+tipo_ambiente+"&opcion="+this.opcion_factura;
        if(this.modo == 2){
            //modo 2 es para solo visualizar la factura ya realizada
            url += "&visualizacion=2";
        }else if(this.modo == 3){
            //modo 3 es para visualizar factura cancelada
            url += "&visualizacion=3";
        }
        
        document.getElementById(this.nombre_instancia+"_zona_impresion").innerHTML = "<iframe src='"+url+"' width='99%' height='"+($( window ).height()*0.79)+"' id='"+this.nombre_instancia+"_iframe_pdf'></iframe>";
        
        if(this.modo == 1){
            document.getElementById(this.nombre_instancia+"_zona_boton_facturar").innerHTML = "<div class='cargando_grande'></div>";
        }
        
        var frame = document.getElementById(this.nombre_instancia+"_iframe_pdf");
        var instancia = this;
        
        var modo = this.modo;
        frame.onload = function(){
            if(modo == 1){
                if(opcion == 1) document.getElementById(instancia.nombre_instancia+"_zona_boton_facturar").innerHTML = "<a class='waves-effect waves-light btn' onclick='"+instancia.nombre_instancia+".generaCDFI("+opcion+")' style='margin:0 auto 0 auto;'>Factura Global</a>";
                else if(opcion == 2) document.getElementById(instancia.nombre_instancia+"_zona_boton_facturar").innerHTML = "<a class='waves-effect waves-light btn' onclick='"+instancia.nombre_instancia+".generaCDFI("+opcion+")' style='margin:0 auto 0 auto;'>Factura Pago</a>";
            }else if(modo == 2){
                var contenido = "<div class='row'>";
                contenido += "<div class='col s6 m6 l6' id='"+instancia.nombre_instancia+"_zona_boton_enviar_factura'>";
                
                contenido += "<a class='waves-effect waves-light btn' onclick='"+instancia.nombre_instancia+".enviarNotificacionFactura()' style='margin:0 auto 0 auto;'>Enviar por Correo</a>";
                
                contenido += "</div>";
                contenido += "<div class='col s6 m6 l6' id='"+instancia.nombre_instancia+"_zona_boton_cancelar_factura'>";
                
                contenido += "<a class='waves-effect waves-light btn' onclick='"+instancia.nombre_instancia+".cancelarCDFI();' style='margin:0 auto 0 auto;'>Cancelar Factura</a>";
                
                contenido += "</div>";
                contenido += "</div>";
                
                document.getElementById(instancia.nombre_instancia+"_zona_botones").innerHTML = contenido;
            }else if(modo == 3){
                var contenido = "<div class='row'>";
                contenido += "<div class='col s6 m6 l6' id='"+instancia.nombre_instancia+"_zona_boton_enviar_factura'>";
                
                contenido += "<a class='waves-effect waves-light btn' onclick='"+instancia.nombre_instancia+".enviarNotificacionFacturaCancelada()' style='margin:0 auto 0 auto;'>Enviar Acuse</a>";
                
                contenido += "</div>";
                contenido += "</div>";
                
                document.getElementById(instancia.nombre_instancia+"_zona_botones").innerHTML = contenido;
            }
            
            
            if(instancia.funcion_facturas_actualizadas && tipo_ambiente == 2) setTimeout(instancia.funcion_facturas_actualizadas, 0);
            setTimeout(instancia.nombre_instancia+".actualizaInfoEmision("+opcion+")", 0);
        }
    }
    
    this.procesaFacturaCancelada = function(respuesta){
        //console.log("respuesta cancelacion: "+respuesta);
        
        document.getElementById(this.nombre_instancia+"_zona_boton_cancelar_factura").innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".cancelarCDFI();' style='margin:0 auto 0 auto;'>Cancelar Factura</a>";
        
        if(respuesta == 1){
            alert("Factura Cancelada! Se ha enviado un correo de notificación al emisor y receptor de la cancelación.");
            document.getElementById(this.nombre_instancia+"_zona_boton_cancelar_factura").innerHTML = "";
            //console.log(this.funcion_facturas_actualizadas);
            if(this.funcion_facturas_actualizadas) setTimeout(this.funcion_facturas_actualizadas, 0);
        }else{
            alert("No se pudo cancelar la factura, es posible que la factura ya haya sido cancelada previamente.");
        }
    }
    
    this.cancelarCDFI = function(){
        if(!confirm("Estas seguro de querer cancelar este CDFI?")) return;
        
        //continuamos con la cancelacion de la factura
        getResultadoAjax("opcion=58&idFactura="+this.idFactura, this.nombre_instancia+".procesaFacturaCancelada", document.getElementById(this.nombre_instancia+"_zona_boton_cancelar_factura"));
    }
    
    this.procesaInfoDeEmisionDeFacturaRecuperada = function(respuesta){
        //console.log("respuesta info emision: "+respuesta);
        
        this.info_factura = respuesta.split("|");
        
        this.getInfoFactura();
    }
    
    this.getInfoFactura = function(){
        //console.log("referencia: "+this.referencia);
        if(!this.referencia) return;
        
        /*if(!this.uuid_cancelado && this.modo != 2){
            var selector = document.getElementById(this.nombre_instancia+"_selector_tipo_facturacion");
            var tipo_ambiente = selector.options[selector.selectedIndex].value;
            if(tipo_ambiente == 1) return; //en el ambiente de prueba no se recupera info de la factura   
        }*/
        
        if(!this.info_factura){
            //console.log("uuiddos: "+this.uuid);
            getResultadoAjax("opcion=360&uuid="+this.uuid, this.nombre_instancia+".procesaInfoDeEmisionDeFacturaRecuperada", document.getElementById(this.nombre_instancia+"_zona_info_factura"));
            return;
        }
        //idFacturaEmitida, UUID, fecha, hora, usuario, estatus
        getListaDePersonas().setIdPersonaSeleccionada(this.info_factura[4]);
        
        if(this.uuid_cancelado){
            document.getElementById(this.nombre_instancia+"_zona_info_factura").innerHTML = "Cancelada <b>"+getFechaLegibleRecortada(this.info_factura[2])+"</b> a las <b>"+this.info_factura[3]+"</b> por <b>"+getListaDePersonas().getNombreDePersonaSeleccionada()+"</b>";   
            
            document.getElementById(this.nombre_instancia+"_zona_envio").innerHTML = "<div class='boton_normal' onclick='"+this.nombre_instancia+".enviarNotificacionFactura(2)'>Enviar Correo</div>";
            
            return;
        }
        
        document.getElementById(this.nombre_instancia+"_zona_info_factura").innerHTML = "Emitida <b>"+getFechaLegibleRecortada(this.info_factura[2])+"</b> a las <b>"+this.info_factura[3]+"</b> por <b>"+getListaDePersonas().getNombreDePersonaSeleccionada()+"</b>";
        
        document.getElementById(this.nombre_instancia+"_zona_envio").innerHTML = "<div class='boton_normal' onclick='"+this.nombre_instancia+".enviarNotificacionFactura(1)'>Enviar Correo</div>";
        
        //como si se recuperó información de la factura, entonces aparecerá el botón de cancelación de factura, siempre y cuando el estatus sea mayor a 0. Un estatus -1 quiere decir que esta factura ya fue cancelada
        if(this.info_factura[5] > 0) document.getElementById(this.nombre_instancia+"_zona_cancelacion").innerHTML = "<div class='boton_eliminar' onclick='"+this.nombre_instancia+".cancelarCDFI()'>Cancelar CDFI</div>";
        else document.getElementById(this.nombre_instancia+"_zona_cancelacion").innerHTML = "";
    }
    
    this.procesaFacturaEnviada = function(respuesta){
        //console.log("respuesta correo enviado: "+respuesta);
        
        if(respuesta == 1){
            alert("Correo de notificación enviado!");
        }else{
            alert("No fue posible enviar el correo de notificación: "+respuesta);
        }
        
        document.getElementById(this.nombre_instancia+"_zona_boton_enviar_factura").innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".enviarNotificacionFactura()' style='margin:0 auto 0 auto;'>Enviar por Correo</a>";
    }
    
    this.enviarNotificacionFactura = function(){
        getResultadoAjax("opcion=57&idFactura="+this.idFactura, this.nombre_instancia+".procesaFacturaEnviada", document.getElementById(this.nombre_instancia+"_zona_boton_enviar_factura"));
    }
    
    this.procesaAcuseEnviado = function(respuesta){
        //console.log("respuesta correo enviado: "+respuesta);
        
        if(respuesta == 1){
            alert("Acuse de Cancelación enviado!");
        }else{
            alert("No fue posible enviar el acuse de notificación: "+respuesta);
        }
        
        document.getElementById(this.nombre_instancia+"_zona_boton_enviar_factura").innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".enviarNotificacionFactura()' style='margin:0 auto 0 auto;'>Enviar Acuse</a>";
    }
    
    this.enviarNotificacionFacturaCancelada = function(){
        getResultadoAjax("opcion=60&idFactura="+this.idFactura, this.nombre_instancia+".procesaAcuseEnviado", document.getElementById(this.nombre_instancia+"_zona_boton_enviar_factura"));
    }
    
    this.actualizaInfoEmision = function(opcion){
        return;
    }
}
var CatalogoRegimenFiscal = function(){
    
    this.infoRegimen = null;
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Catálogo de Régimen Fiscal");
        this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo();
        
        //realizamos la búsqueda en la base de datos de pacientes
        if(!this.unidades) getResultadoAjax("opcion=49", this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
        else this.muestraResultados(this.infoRegimen);
    }
    
    this.muestraResultados = function(respuesta){
        //console.log("regimenes: "+respuesta);
        this.infoRegimen = respuesta;
        
        var regimenes = respuesta.split("^");
        regimenes.pop();
        if(regimenes.length == 0){
            var contenido = "<div>No se encontró el catálogo de régimen fiscal sat</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(regimenes);
        
        var encabezados = new Array("Clave", "Descripción");
        var medidas = new Array("15%", "85%");
        var alineaciones = new Array("left", "left");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, null, null);
        
        this.getListaSeleccionable().setOpcionBorrar(false);
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
        
        //this.getZonaElementosDelCatalogo().innerHTML = "<b>"+unidades.length+" Unidades Cargadas</b>";
    }

}
var CatalogoComprobantePagos = function(){
    
    this.rango_fechas = null;
    this.getRangoDeFechas = function(){
        if(!this.rango_fechas) this.rango_fechas = new SelectorRangoDeFecha();
        return this.rango_fechas;
    }
    
    /*VARIABLES Y MÉTODOS PARA EL SELECTOR DE CLIENTE*/
    this.selectorClientes = null;
    this.getSelectorClientes = function(){
        if(!this.selectorClientes){
            this.selectorClientes = new SelectorDeOpcion();
            this.selectorClientes.setVisualizacion(1);
        }
        return this.selectorClientes;
    }
    
    this.clienteSeleccionado = 0;
    this.setClienteSeleccionado = function(idcliente){
        this.clienteSeleccionado = idcliente;
    }
    
    this.catalogoClientes = null;
    this.getCatalogoClientes = function(){
        if(!this.catalogoClientes) {
            this.catalogoClientes = new CatalogoClientes();
            this.catalogoClientes.setSoloSeleccion(1);
        }
        return this.catalogoClientes;
    }
    /*VARIABLES Y MÉTODOS PARA EL SELECTOR DE CLIENTE*/
    
    /*VARIABLES Y MÉTODOS PARA EL SELECTOR DE EMPRESAS*/
    this.idEmpresaSeleccionada = 0;
    this.selectorEmpresa = null;
    this.getSelectorEmpresa = function(){
        if(!this.selectorEmpresa){
            this.selectorEmpresa = new SelectorDeOpcion();
            this.selectorEmpresa.setVisualizacion(1);
        }
        return this.selectorEmpresa;
    }
    
    this.catalogoEmpresas = null;
    this.getCatalogoEmpresas = function(){
        if(!this.catalogoEmpresas){
            this.catalogoEmpresas = new CatalogoEmpresas();
            this.catalogoEmpresas.setSoloSeleccion(1);
        }
        return this.catalogoEmpresas;
    }
    /*VARIABLES Y MÉTODOS PARA EL SELECTOR DE EMPRESAS*/
    
    this.muestraComponente = function(){
        if(orbe.login.getPerfil() != 5){
            this.setTituloCatalogo("Pagos Recibidos");
            this.setTextoAgregarElemento("Agregar un nuevo pago recibido");

            this.setFormaCaptura(new FormaPagoRecibido());
            this.muestraEstructuraCatalogoPrincipalConBarraOpciones();

            var contenido = "";
            
            contenido += '<div class="row center-align">';
            contenido += '<div class="col s6 m6 l6 center-align">';

            contenido += "<div id='"+this.nombre_instancia+"_zona_empresas'></div>";

            contenido += '</div>';
            contenido += '<div class="col s6 m6 l6 center-align">';

            contenido += "<div id='"+this.nombre_instancia+"_zona_clientes'></div>";

            contenido += '</div>';
            contenido += '</div>';
            
            contenido += "<table align='center' width='100%' cellpadding='3' cellspacing='0'><tr>";

            contenido += "<td align='center' width='90%' valign='top'>";

            contenido += "<div id='"+this.nombre_instancia+"_zona_rango_fecha'></div>";

            contenido += "</td>";
            contenido += "<td align='center' width='10%' valign='bottom'>";

            contenido += "<div id='"+this.nombre_instancia+"_zona_boton_todos'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".getTodosLosElementos()' style='margin:0 auto 0 auto;'>Todos</a></div>";

            contenido += "</td>";
            contenido += "</tr></table><br/>";

            this.getZonaSecundaria().innerHTML = contenido;

            this.getRangoDeFechas().setComponente(document.getElementById(this.nombre_instancia+"_zona_rango_fecha"), this.nombre_instancia+".getRangoDeFechas()");
            this.getRangoDeFechas().setFuncionCambiaRango(this.nombre_instancia+".onDateRangeChange()");
            this.getRangoDeFechas().muestraComponente();

            this.getSelectorClientes().setComponente(document.getElementById(this.nombre_instancia+"_zona_clientes"), this.nombre_instancia+".getSelectorClientes()");
            if(this.clienteSeleccionado == 0) this.getSelectorClientes().setTextoDeOpcionSeleccionada("Todos los Clientes");
            this.getSelectorClientes().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoDeClientes()");

            this.getSelectorClientes().muestraComponente();
            
            //selector empresa
            this.getSelectorEmpresa().setComponente(document.getElementById(this.nombre_instancia+"_zona_empresas"), this.nombre_instancia+".getSelectorEmpresa()");

            if(this.idEmpresaSeleccionada == 0) this.getSelectorEmpresa().setTextoDeOpcionSeleccionada("Todas las Empresas");
            this.getSelectorEmpresa().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoEmpresas()");

            this.getSelectorEmpresa().muestraComponente();

            //selector de clientes y catalogo de clientes
            this.getFormaCaptura().setCatalogoClientes(this.getCatalogoClientes());
            this.getFormaCaptura().setSelectorClientes(this.getSelectorClientes());
            
            //selector de empresas y catalogo de empresas
            this.getFormaCaptura().setCatalogoEmpresas(this.getCatalogoEmpresas());
            this.getFormaCaptura().setSelectorEmpresa(this.getSelectorEmpresa());
            
            this.getFormaCaptura().setComponentePadre(this);

            //mostramos las opciones de impresion
            if(this.soloSeleccion == 0) this.mostraOpcionesDeImpresion();
            
            this.onDateRangeChange();
        }else{
            this.setTituloCatalogo("Pagos Recibidos");
            this.muestraEstructuraCatalogoPrincipalConBarraOpcionesSinOpcionNuevo();

            var contenido = "<div id='"+this.nombre_instancia+"_zona_clientes'></div><br/><table align='center' width='100%' cellpadding='3' cellspacing='0'>";
            contenido += "<tr>";

            contenido += "<td align='center' width='90%' valign='top'>";

            contenido += "<div id='"+this.nombre_instancia+"_zona_rango_fecha'></div>";

            contenido += "</td>";
            contenido += "<td align='center' width='10%' valign='bottom'>";

            contenido += "<div id='"+this.nombre_instancia+"_zona_boton_todos'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".getTodosLosElementos()' style='margin:0 auto 0 auto;'>Todos</a></div>";

            contenido += "</td>";
            contenido += "</tr></table><br/>";

            this.getZonaSecundaria().innerHTML = contenido;

            this.getRangoDeFechas().setComponente(document.getElementById(this.nombre_instancia+"_zona_rango_fecha"), this.nombre_instancia+".getRangoDeFechas()");
            this.getRangoDeFechas().setFuncionCambiaRango(this.nombre_instancia+".onDateRangeChange()");
            this.getRangoDeFechas().muestraComponente();
            
            this.onDateRangeChange();

            //mostramos las opciones de impresion
            this.mostraOpcionesDeImpresion();
        }
    }
    
    this.getTodosLosElementos = function(){
        getResultadoAjax("opcion=55&fechaInicio=&fechaFinal=&idCliente=0&idEmpresa=0", this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
    }
    
    this.onDateRangeChange = function(){
        var fecha_inicio = this.getRangoDeFechas().getFechaInicio();
        var fecha_fin = this.getRangoDeFechas().getFechaFin();
        getResultadoAjax("opcion=55&fechaInicio="+fecha_inicio+"&fechaFinal="+fecha_fin+"&idCliente="+this.clienteSeleccionado+"&idEmpresa="+this.idEmpresaSeleccionada, this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
    }
    
    this.muestraResultados = function(respuesta){
        //console.log("Pagos recibidos: "+respuesta);
        var pagos = respuesta.split("^");
        pagos.pop();
        if(pagos.length == 0){
            var contenido = "<div>No hay pagos recibidos registrados en el sistema aún</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(pagos);
        
        var encabezados = new Array("Empresa", "Cliente", "Fecha", "Hora", "Cantidad");
        var medidas = new Array("20%", "20%", "15%", "10%", "15%");
        var alineaciones = new Array("left", "left", "center", "center", "right");
        //idPagoRecibido, nombreEmpresa, nombreCliente, fecha, hora, cantidadPagada
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, this.nombre_instancia+".eliminaElemento()", null);
        
        if(this.soloSeleccion == 0) this.getListaSeleccionable().setOpcionBorrar(true);
        else this.getListaSeleccionable().setOpcionBorrar(false);
        
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
    
    this.eliminaElemento = function(){
        //si llega aqui, es por que se confirmó la acción de eliminar el elemento
        var idelemento = this.getListaSeleccionable().getIdElementoEliminado();
        getResultadoAjax("opcion=62&idComprobante="+idelemento, this.nombre_instancia+".procesaElementoEliminado", null);
    }
    
    this.procesaElementoEliminado = function(respuesta){
        //console.log("respuesta eliminar: "+respuesta);
        if(respuesta == 1){
            alert("Comprobante de pago eliminado!");
            this.getListaSeleccionable().quitaElementoDeListaPorId(this.getListaSeleccionable().getIdElementoEliminado());
        }else{
            alert("No se pudo eliminar en este momento, por favor intenta mas tarde!");
        }
    }
    
    this.elementoSeleccionado = function(){
        //console.log(this.funcionElementoSeleccionado);
        if(this.funcionElementoSeleccionado){
            setTimeout(this.funcionElementoSeleccionado, 0);
            return;
        }
        
        var datos = this.getListaSeleccionable().getDatosDeElementoSeleccionado().split("|");
        
        var formaPagoRecibido = new FormaPagoRecibido();
        formaPagoRecibido.setIdPago(datos[0]);
        this.setFormaCaptura(formaPagoRecibido);
        this.nuevoElemento();
        
        formaPagoRecibido.editaForma();
    }
    
    /*METODOS PARA INVOCAR EL CATALOGO DE CLIENTES REGISTRADOS*/
    this.muestraCatalogoDeClientes = function(){
        this.getCatalogoClientes().setComponente(null, this.nombre_instancia+".getCatalogoClientes()");
        this.getCatalogoClientes().setContenedorFormaFlotante(1);
        this.getCatalogoClientes().muestraContenedorModal('G');
        this.getCatalogoClientes().controlador_navegacion = null;
        
        this.catalogoClientes.setFuncionElementoSeleccionado(this.nombre_instancia+".clienteSeleccionadoDeCatalogo()");
        this.getCatalogoClientes().setComponente(this.getCatalogoClientes().getContenedorModal().getContenedorDeContenido(), this.getCatalogoClientes().nombre_instancia);
        this.getCatalogoClientes().muestraComponente();
    }
    
    this.clienteSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoClientes().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorClientes().setTextoDeOpcionSeleccionada(datos[1]);
            this.clienteSeleccionado = datos[0];
            this.getCatalogoClientes().getContenedorModal().ocultaComponente();
            
            this.getFormaCaptura().setClienteSeleccionado(this.clienteSeleccionado);
            this.getFormaCaptura().setCatalogoClientes(this.getCatalogoClientes());
            this.getFormaCaptura().setSelectorClientes(this.getSelectorClientes());
            
            this.onDateRangeChange();
        }
    }
    
    /*FUNCIONES PARA CATALOGO DE EMPRESAS*/
    this.muestraCatalogoEmpresas = function(){
        //console.log("mostrando catalogo de empresas");
        this.getCatalogoEmpresas().setComponente(null, this.nombre_instancia+".getCatalogoEmpresas()");
        this.getCatalogoEmpresas().setContenedorFormaFlotante(1);
        this.getCatalogoEmpresas().muestraContenedorModal('G');
        this.getCatalogoEmpresas().controlador_navegacion = null;
        
        this.getCatalogoEmpresas().setFuncionElementoSeleccionado(this.nombre_instancia+".empresaSeleccionadoDeCatalogo()");
        this.getCatalogoEmpresas().setComponente(this.getCatalogoEmpresas().getContenedorModal().getContenedorDeContenido(), this.getCatalogoEmpresas().nombre_instancia);
        this.getCatalogoEmpresas().muestraComponente();
    }
    
    this.empresaSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoEmpresas().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorEmpresa().setTextoDeOpcionSeleccionada(datos[1]);
            this.idEmpresaSeleccionada = datos[0];
            this.getCatalogoEmpresas().getContenedorModal().ocultaComponente();
            
            this.getFormaCaptura().setEmpresaSeleccionada(this.idEmpresaSeleccionada);
            this.getFormaCaptura().setCatalogoEmpresas(this.getCatalogoEmpresas());
            this.getFormaCaptura().setSelectorEmpresa(this.getSelectorEmpresa());
            
            this.onDateRangeChange();
        }
    }
}
var FormaPagoRecibido = function(){
    this.idPago = 0;
    this.infoPago = null;
    
    this.setIdPago = function(idpago){
        this.idPago = idpago;
    }
    
    /*VARIABLES Y MÉTODOS PARA EL SELECTOR DE CLIENTE*/
    this.clienteSeleccionado = 0;
    this.setClienteSeleccionado = function(idcliente){
        this.clienteSeleccionado = idcliente;
    }
    
    this.catalogoClientes = null;
    this.getCatalogoClientes = function(){
        if(!this.catalogoClientes) {
            this.catalogoClientes = new CatalogoClientes();
            this.catalogoClientes.setSoloSeleccion(1);
        }
        return this.catalogoClientes;
    }
    
    this.setCatalogoClientes = function(catalogo){
        this.catalogoClientes = catalogo;
    }
    
    this.selectorClientes = null;
    this.getSelectorClientes = function(){
        if(!this.selectorClientes){
            this.selectorClientes = new SelectorDeOpcion();
            this.selectorClientes.setVisualizacion(1);
        }
        return this.selectorClientes;
    }
    
    this.setSelectorClientes = function(selector){
        this.selectorClientes = selector;
    }
    /*VARIABLES Y MÉTODOS PARA EL SELECTOR DE CLIENTE*/
    
    /*VARIABLES Y MÉTODOS PARA EL SELECTOR DE EMPRESA*/
    this.idEmpresaSeleccionada = 0;
    this.setEmpresaSeleccionada = function(idempresa){
        this.idEmpresaSeleccionada = idempresa;
    }
    
    this.catalogoEmpresas = null;
    this.getCatalogoEmpresas = function(){
        if(!this.catalogoEmpresas){
            this.catalogoEmpresas = new CatalogoEmpresas();
            this.catalogoEmpresas.setSoloSeleccion(1);
        }
        return this.catalogoEmpresas;
    }
    
    this.setCatalogoEmpresas = function(catalogo){
        this.catalogoEmpresas = catalogo;
    }
    
    this.selectorEmpresa = null;
    this.getSelectorEmpresa = function(){
        if(!this.selectorEmpresa){
            this.selectorEmpresa = new SelectorDeOpcion();
            this.selectorEmpresa.setVisualizacion(1);
        }
        return this.selectorEmpresa;
    }
    
    this.setSelectorEmpresa = function(selector){
        this.selectorEmpresa = selector;
    }
    /*VARIABLES Y MÉTODOS PARA EL SELECTOR DE EMPRESA*/
    
    //SELECTOR Y CATALOGO DE FORMAS DE PAGO
    this.idFormaPago = 1;
    this.bancarizado = false;
    this.selectorFormasPago = null;
    this.getSelectorFormasPago = function(){
        if(!this.selectorFormasPago){
            this.selectorFormasPago = new SelectorDeOpcion();
            this.selectorFormasPago.setVisualizacion(1);
        }
        return this.selectorFormasPago;
    }
    
    this.catalogoFormasPago = null;
    this.getCatalogoFormasPago = function(){
        if(!this.catalogoFormasPago){
            this.catalogoFormasPago = new CatalogoFormaPagoSat();
            this.catalogoFormasPago.setSoloSeleccion(1);
        }
        return this.catalogoFormasPago;
    }
    //SELECTOR Y CATALOGO DE FORMAS DE PAGO
    
    //SELECTOR Y CATALOGO DE MONEDAS
    this.idMoneda = "MXN";
    this.selectorMoneda = null;
    this.getSelectorMoneda = function(){
        if(!this.selectorMoneda){
            this.selectorMoneda = new SelectorDeOpcion();
            this.selectorMoneda.setVisualizacion(1);
        }
        return this.selectorMoneda;
    }
    
    this.catalogoMonedas = null;
    this.getCatalogoMonedas = function(){
        if(!this.catalogoMonedas){
            this.catalogoMonedas = new CatalogoMonedasSat();
            this.catalogoMonedas.setSoloSeleccion(1);
        }
        return this.catalogoMonedas;
    }
    //SELECTOR Y CATALOGO DE MONEDAS
    
    //SELECTOR Y CATALOGO DE BANCOS SAT
    this.idBancoOrigen = 0;
    this.selectorBancoOrigen = null;
    this.getSelectorBancoOrigen = function(){
        if(!this.selectorBancoOrigen){
            this.selectorBancoOrigen = new SelectorDeOpcion();
            this.selectorBancoOrigen.setVisualizacion(1);
        }
        return this.selectorBancoOrigen;
    }
    
    this.catalogoBancoOrigen = null;
    this.getCatalogoBancoOrigen = function(){
        if(!this.catalogoBancoOrigen){
            this.catalogoBancoOrigen = new CatalogoBancosSat();
            this.catalogoBancoOrigen.setSoloSeleccion(1);
        }
        return this.catalogoBancoOrigen;
    }
    //SELECTOR Y CATALOGO DE BANCOS SAT
    
    //SELECTOR Y CATALOGO DE BANCOS SAT RECEPTOR
    this.idBancoReceptor = 0;
    this.selectorBancoReceptor = null;
    this.getSelectorBancoReceptor = function(){
        if(!this.selectorBancoReceptor){
            this.selectorBancoReceptor = new SelectorDeOpcion();
            this.selectorBancoReceptor.setVisualizacion(1);
        }
        return this.selectorBancoReceptor;
    }
    //SELECTOR Y CATALOGO DE BANCOS SAT
    
    //selector de fecha del pago
    this.selector_fecha = null;
    
    //selector de hora
    this.selector_hora = null;
    
    //Visor de Facturación
    this.visor_facturas = null;
    this.getComponenteVisorDeFacturas = function(){
        if(!this.visor_facturas){
            this.visor_facturas = new ComponenteVisorFactura();
            this.visor_facturas.setComponente(null, this.nombre_instancia+".getComponenteVisorDeFacturas()");
            this.visor_facturas.funcionFacturasActualizadas(this.nombre_instancia+".recargaEscaneos()");
        }
        return this.visor_facturas;
    }
    
    this.muestraComponente = function(){
        var contenido = "<br/>";    
        
        if(this.idPago == 0) contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Nuevo Pago Recibido</div>";
        else contenido += "<div class='titulo_forma' id='"+this.nombre_instancia+"_titulo_forma'>Editando Pago Recibido</div>";
        
        contenido += "<div class='separador'></div>";
        contenido += "<br/>";    
        
        //selector de empresa y cliente
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Empresa</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_empresas'></div>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Cliente</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_clientes'></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        //selector de fecha y hora de recepción de pago
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Fecha recepción</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_selector_fecha'></div>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Hora recepción</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_selector_hora'></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        //selector de forma de pago y moneda
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Forma de Pago</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_forma_pago'></div>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Moneda</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_selector_moneda'></div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        //Monto y tipo de cambio únicamente si la moneda es distinta de MXN
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Cantidad Pagada</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_cantidad_pagada' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe la cantidad total del pago recibido\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_tipo_cambio'>";
        contenido += "<div class='texto_forma'>Tipo de Cambio</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_tipo_cambio' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Escribe la cantidad del tipo de cambio de la moneda seleccionada con respecto del peso mexicano\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        contenido += "</div>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        //DIV DE AYUDA
        contenido += "<br/>";
        contenido += "<div class='ayuda_campos' id='"+this.nombre_instancia+"_a1'></div>";
        contenido += "<br/>";
        
        //ZONA PARA DATOS CUANDO LA FORMA DE PAGO ES BANCARIZADA
        contenido += "<div id='"+this.nombre_instancia+"_zona_datos_bancarizados'>";
        
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Banco Origen</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_selector_banco_origen'></div>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Número de Cuenta Origen</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_numero_cuenta_origen' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Número de cuenta de origen del pago.\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Banco Receptor</div>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_selector_banco_receptor'></div>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Número de Cuenta Receptor</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_numero_cuenta_receptor' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Número de cuenta donde se recibe el pago.\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += '<div class="row center-align">';
        contenido += '<div class="col s6 center-align">';
        
        contenido += "<div class='texto_forma'>Número de Operación</div>";
        contenido += "<input type='text' id='"+this.nombre_instancia+"_campo_numero_operacion' onfocus='muestraAyudaCampo(\""+this.nombre_instancia+"_a1\", \"Permite capturar el número de operación, número de cheque, número de SPEI o cualquier otro número que permita identificar única y eficazmente la transacción.\");' onblur='ocultaAyudaCampo(\""+this.nombre_instancia+"_a1\")'/>";
        
        contenido += '</div>';
        contenido += '<div class="col s6 center-align">';
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += '</div>';
        
        //boton guardar y facturación del pago
        contenido += '<div class="row center-align">';
        contenido += '<div class="col l6 center-align">';
        
        contenido += "<div id='"+this.nombre_instancia+"_zona_boton_guardar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a></div>";
        
        contenido += '</div>';
        contenido += '<div class="col l6 center-align">';
        
        //solo se pueden agregar los conceptos que tienen idGasto en 0, en otro caso no se pueden agregar por que ya están agregados, solo se pueden modificar
        if(this.idPago > 0){
            contenido += "<div id='"+this.nombre_instancia+"_zona_boton_facturar'><a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".facturarPago()' style='margin:0 auto 0 auto;'>Facturar Pago</a></div>";   
        }
        
        contenido += '</div>';
        contenido += '</div>';
        
        contenido += "<br/>";
        
        this.contenedor.innerHTML = contenido;
        
        //selector empresa
        this.getSelectorEmpresa().setComponente(document.getElementById(this.nombre_instancia+"_zona_empresas"), this.nombre_instancia+".getSelectorEmpresa()");
        
        //console.log("idempresa: "+this.idEmpresaSeleccionada);
        
        if(this.idEmpresaSeleccionada == 0) this.getSelectorEmpresa().setTextoDeOpcionSeleccionada("Empresa");
        this.getSelectorEmpresa().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoEmpresas()");
        
        this.getSelectorEmpresa().muestraComponente();
        
        //selector de clientes
        this.getSelectorClientes().setComponente(document.getElementById(this.nombre_instancia+"_zona_clientes"), this.nombre_instancia+".getSelectorClientes()");
        
        if(this.clienteSeleccionado == 0) this.getSelectorClientes().setTextoDeOpcionSeleccionada("Cliente");
        this.getSelectorClientes().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoDeClientes()");
        
        this.getSelectorClientes().muestraComponente();
        
        //selector de fecha de recepcion del pago
        //selector de fecha, por defecto pone los datos actuales
        this.selector_fecha = new SelectorFechas();
        this.selector_fecha.fecha_actual = new Date();
        this.selector_fecha.setSelectorFecha(document.getElementById(this.nombre_instancia+"_zona_selector_fecha"), this.nombre_instancia+".selector_fecha");
        this.selector_fecha.fecha_modificada = true;
        this.selector_fecha.muestraComboFechas();
        
        //selector de hora
        this.selector_hora = new SelectorHora();
        this.selector_hora.setSelectorHora(document.getElementById(this.nombre_instancia+"_zona_selector_hora"), -1, null, this.nombre_instancia+".selector_hora");
        
        //selector formas de pago sat
        this.getSelectorFormasPago().setComponente(document.getElementById(this.nombre_instancia+"_zona_forma_pago"), this.nombre_instancia+".getSelectorFormasPago()");
        
        if(this.idFormaPago == 0) this.getSelectorFormasPago().setTextoDeOpcionSeleccionada("Selecciona Forma de Pago");
        else if(this.idFormaPago == 1) this.getSelectorFormasPago().setTextoDeOpcionSeleccionada("EFECTIVO");
        
        this.getSelectorFormasPago().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoFormasDePago()");
        
        this.getSelectorFormasPago().muestraComponente();
        
        //selector monedas sat
        this.getSelectorMoneda().setComponente(document.getElementById(this.nombre_instancia+"_zona_selector_moneda"), this.nombre_instancia+".getSelectorMoneda()");
        
        if(this.idMoneda == 0) this.getSelectorMoneda().setTextoDeOpcionSeleccionada("Selecciona Moneda");
        else if(this.idMoneda == "MXN") this.getSelectorMoneda().setTextoDeOpcionSeleccionada("PESO MEXICANO (MXN)");
        
        this.getSelectorMoneda().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoMonedas()");
        
        this.getSelectorMoneda().muestraComponente();
        
        //selector bancos sat origen
        this.getSelectorBancoOrigen().setComponente(document.getElementById(this.nombre_instancia+"_zona_selector_banco_origen"), this.nombre_instancia+".getSelectorBancoOrigen()");
        
        if(this.idBancoOrigen == 0) this.getSelectorBancoOrigen().setTextoDeOpcionSeleccionada("Selecciona Banco Origen");
        
        this.getSelectorBancoOrigen().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoBancoOrigen()");
        
        this.getSelectorBancoOrigen().muestraComponente();
        
        //selector bancos sat receptor
        this.getSelectorBancoReceptor().setComponente(document.getElementById(this.nombre_instancia+"_zona_selector_banco_receptor"), this.nombre_instancia+".getSelectorBancoReceptor()");
        
        if(this.idBancoReceptor == 0) this.getSelectorBancoReceptor().setTextoDeOpcionSeleccionada("Selecciona Banco Receptor");
        
        this.getSelectorBancoReceptor().setFuncionSelectorPresionado(this.nombre_instancia+".muestraCatalogoBancoReceptor()");
        
        this.getSelectorBancoReceptor().muestraComponente();
        
        //seteamos zona de tipo de cambio
        this.setZonaTipoCambio();
        
        //seteamos la zona de elementos bancarizados si aplica
        this.setZonaElementosBancarizados();
    }
    
    this.facturarPago = function(){
        this.getComponenteVisorDeFacturas().setOpcionTipoFactura(2);
        this.getComponenteVisorDeFacturas().setIdElemento(this.idPago);
        this.getComponenteVisorDeFacturas().muestraComponente();
    }
    
    //FUNCION INVOCADA CUANDO SE EMITE UNA FACTURA
    this.recargaEscaneos = function(){
        
    }
    
    //FUNCIONES PARA LA SELECCION DE EMPRESAS
    this.muestraCatalogoEmpresas = function(){
        this.getCatalogoEmpresas().setComponente(null, this.nombre_instancia+".getCatalogoEmpresas()");
        this.getCatalogoEmpresas().setContenedorFormaFlotante(1);
        this.getCatalogoEmpresas().muestraContenedorModal('G');
        this.getCatalogoEmpresas().controlador_navegacion = null;
        
        this.getCatalogoEmpresas().setFuncionElementoSeleccionado(this.nombre_instancia+".empresaSeleccionadaDeCatalogo()");
        this.getCatalogoEmpresas().setComponente(this.getCatalogoEmpresas().getContenedorModal().getContenedorDeContenido(), this.getCatalogoEmpresas().nombre_instancia);
        this.getCatalogoEmpresas().muestraComponente();
    }
    
    this.empresaSeleccionadaDeCatalogo = function(){
        var datos = this.getCatalogoEmpresas().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorEmpresa().setTextoDeOpcionSeleccionada(datos[1]);
            this.idEmpresaSeleccionada = datos[0];
            this.getCatalogoEmpresas().getContenedorModal().ocultaComponente();
        }
    }
    //FUNCIONES PARA LA SELECCION DE EMPRESAS
    
    //FUNCIONES PARA LA SELECCION DE CLIENTE
    this.muestraCatalogoDeClientes = function(){
        this.getCatalogoClientes().setComponente(null, this.nombre_instancia+".getCatalogoClientes()");
        this.getCatalogoClientes().setContenedorFormaFlotante(1);
        this.getCatalogoClientes().muestraContenedorModal('G');
        this.getCatalogoClientes().controlador_navegacion = null;
        
        this.getCatalogoClientes().setFuncionElementoSeleccionado(this.nombre_instancia+".clienteSeleccionadoDeCatalogo()");
        this.getCatalogoClientes().setComponente(this.getCatalogoClientes().getContenedorModal().getContenedorDeContenido(), this.getCatalogoClientes().nombre_instancia);
        this.getCatalogoClientes().muestraComponente();
    }
    
    this.clienteSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoClientes().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorClientes().setTextoDeOpcionSeleccionada(datos[1]);
            this.clienteSeleccionado = datos[0];
            this.getCatalogoClientes().getContenedorModal().ocultaComponente();
        }
    }
    //FUNCIONES PARA LA SELECCION DE CLIENTE
    
    //FUNCIONES PARA SELECTOR DE FORMA DE PAGOS
    this.formaPagoSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoFormasPago().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorFormasPago().setTextoDeOpcionSeleccionada(datos[2]);
            this.idFormaPago = datos[1];
            this.getCatalogoFormasPago().getContenedorModal().ocultaComponente();
            
            if(datos[3] == "Sí") this.bancarizado = true;
            else this.bancarizado = false;
            
            this.setZonaElementosBancarizados();
        }
    }
    
    //Catalogo Formas de Pago
    this.muestraCatalogoFormasDePago = function(){
        this.getCatalogoFormasPago().setComponente(null, this.nombre_instancia+".getCatalogoFormasPago()");
        this.getCatalogoFormasPago().setContenedorFormaFlotante(1);
        this.getCatalogoFormasPago().muestraContenedorModal('G');
        this.getCatalogoFormasPago().controlador_navegacion = null;
        
        this.getCatalogoFormasPago().setFuncionElementoSeleccionado(this.nombre_instancia+".formaPagoSeleccionadoDeCatalogo()");
        this.getCatalogoFormasPago().setComponente(this.getCatalogoFormasPago().getContenedorModal().getContenedorDeContenido(), this.getCatalogoFormasPago().nombre_instancia);
        this.getCatalogoFormasPago().muestraComponente();
    }
    //FUNCIONES PARA SELECTOR DE FORMA DE PAGOS
    
    //FUNCIONES PARA SELECTOR DE MONEDAS
    this.muestraCatalogoMonedas = function(){
        this.getCatalogoMonedas().setComponente(null, this.nombre_instancia+".getCatalogoMonedas()");
        this.getCatalogoMonedas().setContenedorFormaFlotante(1);
        this.getCatalogoMonedas().muestraContenedorModal('G');
        this.getCatalogoMonedas().controlador_navegacion = null;
        
        this.getCatalogoMonedas().setFuncionElementoSeleccionado(this.nombre_instancia+".monedaSeleccionadoDeCatalogo()");
        this.getCatalogoMonedas().setComponente(this.getCatalogoMonedas().getContenedorModal().getContenedorDeContenido(), this.getCatalogoMonedas().nombre_instancia);
        this.getCatalogoMonedas().muestraComponente();
    }
    
    this.monedaSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoMonedas().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorMoneda().setTextoDeOpcionSeleccionada(datos[2]+" ("+datos[1]+")");
            this.idMoneda = datos[1];
            this.getSelectorMoneda().getContenedorModal().ocultaComponente();
            
            this.setZonaTipoCambio();
        }
    }
    
    this.setZonaTipoCambio = function(){
        var zonaTipoCambio = document.getElementById(this.nombre_instancia+"_zona_tipo_cambio");
        if(zonaTipoCambio) {
            if(this.idMoneda != "MXN"){
                $(zonaTipoCambio).show();
            }else{
                $(zonaTipoCambio).hide();
            }   
        }
    }
    //FUNCIONES PARA SELECTOR DE MONEDAS
    
    //funciones para administracion de datos bancarizados
    this.setZonaElementosBancarizados = function(){
        var div = document.getElementById(this.nombre_instancia+"_zona_datos_bancarizados");
        if(div){
            if(this.bancarizado) $(div).show();
            else $(div).hide();
        }
    }
    //funciones para administracion de datos bancarizados
    
    //FUNCIONES PARA BANCO ORIGEN
    this.muestraCatalogoBancoOrigen = function(){
        this.getCatalogoBancoOrigen().setComponente(null, this.nombre_instancia+".getCatalogoBancoOrigen()");
        this.getCatalogoBancoOrigen().setContenedorFormaFlotante(1);
        this.getCatalogoBancoOrigen().muestraContenedorModal('G');
        this.getCatalogoBancoOrigen().controlador_navegacion = null;
        
        this.getCatalogoBancoOrigen().setFuncionElementoSeleccionado(this.nombre_instancia+".bancoOrigenSeleccionadoDeCatalogo()");
        this.getCatalogoBancoOrigen().setComponente(this.getCatalogoBancoOrigen().getContenedorModal().getContenedorDeContenido(), this.getCatalogoBancoOrigen().nombre_instancia);
        this.getCatalogoBancoOrigen().muestraComponente();
    }
    
    this.bancoOrigenSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoBancoOrigen().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorBancoOrigen().setTextoDeOpcionSeleccionada(datos[3]);
            this.idBancoOrigen = datos[1];
            this.getSelectorBancoOrigen().getContenedorModal().ocultaComponente();
        }
    }
    //FUNCIONES PARA BANCO ORIGEN
    
    //FUNCIONES PARA BANCO RECEPTOR
    this.muestraCatalogoBancoReceptor = function(){
        this.getCatalogoBancoOrigen().setComponente(null, this.nombre_instancia+".getCatalogoBancoOrigen()");
        this.getCatalogoBancoOrigen().setContenedorFormaFlotante(1);
        this.getCatalogoBancoOrigen().muestraContenedorModal('G');
        this.getCatalogoBancoOrigen().controlador_navegacion = null;
        
        this.getCatalogoBancoOrigen().setFuncionElementoSeleccionado(this.nombre_instancia+".bancoReceptorSeleccionadoDeCatalogo()");
        this.getCatalogoBancoOrigen().setComponente(this.getCatalogoBancoOrigen().getContenedorModal().getContenedorDeContenido(), this.getCatalogoBancoOrigen().nombre_instancia);
        this.getCatalogoBancoOrigen().muestraComponente();
    }
    
    this.bancoReceptorSeleccionadoDeCatalogo = function(){
        var datos = this.getCatalogoBancoOrigen().getDatosDeElementoSeleccionadoDeCatalogo();
        if(datos){
            this.getSelectorBancoReceptor().setTextoDeOpcionSeleccionada(datos[3]);
            this.idBancoReceptor = datos[1];
            this.getSelectorBancoOrigen().getContenedorModal().ocultaComponente();
        }
    }
    //FUNCIONES PARA BANCO RECEPTOR
    
    //VALIDAR Y GUARDAR FORMA DE PAGO
    this.validaForma = function(){
        if(this.idEmpresaSeleccionada == 0){
            muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar una empresa receptora del pago");
            return;
        }
        
        if(this.clienteSeleccionado == 0){
            muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar un cliente emisor del pago");
            return;
        }
        
        var fecha = this.selector_fecha.getFechaActualParaDB();
        var hora = this.selector_hora.getHora();
        
        var cantidadPagada = document.getElementById(this.nombre_instancia+"_campo_cantidad_pagada");
        if(cantidadPagada.value == "" || cantidadPagada.value == " "){
            cantidadPagada.focus();
            return;
        }
        
        var tipoCambio = "1";
        if(this.idMoneda != "MXN"){
            var campoTipoCambio = document.getElementById(this.nombre_instancia+"_tipo_cambio");
            if(campoTipoCambio.value == "" || campoTipoCambio.value == " "){
                campoTipoCambio.focus();
                return;
            }
            
            tipoCambio = campoTipoCambio.value;
        }
        
        var ctaOrigen = "";
        var ctaDestino = "";
        var numeroOperacion = "";
        if(this.bancarizado){
            if(this.idBancoOrigen == 0){
                muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar el banco de donde viene el pago");
                return;
            }
            
            var campoCtaOrigen = document.getElementById(this.nombre_instancia+"_campo_numero_cuenta_origen");
            if(campoCtaOrigen.value == "" || campoCtaOrigen.value == " "){
                campoCtaOrigen.focus();
                return;
            }
            
            if(this.idBancoReceptor == 0){
                muestraAyudaCampo(this.nombre_instancia+"_a1", "Debes seleccionar el banco en donde se recibe el pago");
                return;
            }
            
            var campoCtaReceptor = document.getElementById(this.nombre_instancia+"_campo_numero_cuenta_receptor");
            if(campoCtaReceptor.value == "" || campoCtaReceptor.value == " "){
                campoCtaReceptor.focus();
                return;
            }
            
            var campoNumeroOperacion = document.getElementById(this.nombre_instancia+"_campo_numero_operacion");
            if(campoNumeroOperacion.value == "" || campoNumeroOperacion.value == " "){
                campoNumeroOperacion.focus();
                return;
            }
            
            ctaOrigen = campoCtaOrigen.value;
            ctaDestino = campoCtaReceptor.value;
            numeroOperacion = campoNumeroOperacion.value;
        }
        
        var info = this.idPago+"|"+this.idEmpresaSeleccionada+"|"+this.clienteSeleccionado+"|"+fecha+"|"+hora+"|"+this.idFormaPago+"|"+this.idMoneda+"|"+cantidadPagada.value+"|"+tipoCambio+"|"+this.idBancoOrigen+"|"+ctaOrigen+"|"+this.idBancoReceptor+"|"+ctaDestino+"|"+numeroOperacion;
        
        //console.log("info: "+info);
        
        getResultadoAjax("opcion=53&info="+info, this.nombre_instancia+".procesaGuardado", document.getElementById(this.nombre_instancia+"_zona_boton_guardar"));
    }
    
    this.procesaInfoPago = function(respuesta){
        this.infoPago = respuesta;
        this.editaForma();
    }
    
    this.editaForma = function(){
        if(!this.infoPago){
            getResultadoAjax("opcion=54&idPago="+this.idPago, this.nombre_instancia+".procesaInfoPago", this.contenedor);
            return;
        }
        
        this.muestraComponente();
        
        //idPagoRecibido, idEmpresa, idCliente, fecha, hora, formaPago, moneda, cantidadPagada, tipoCambio, bancoOrigen, cuentaOrigen, bancoReceptor, cuentaReceptor, numeroOperacion, estatus, nombreEmpresa, nombreCliente, nombreFormaPago, nombreMoneda, nombreBancoOrigen, nombreBancoReceptor, esBancarizado
        
        var datos = this.infoPago.split("|");
        this.idEmpresaSeleccionada = datos[1];
        this.clienteSeleccionado = datos[2];
        
        this.selector_fecha.setFechaDesdeDB(datos[3]);
        this.selector_hora.setHora(datos[4]);
        
        this.idFormaPago = datos[5];
        this.idMoneda = datos[6];
        
        document.getElementById(this.nombre_instancia+"_campo_cantidad_pagada").value = datos[7];
        document.getElementById(this.nombre_instancia+"_tipo_cambio").value = datos[8];
        
        this.idBancoOrigen = datos[9];
        document.getElementById(this.nombre_instancia+"_campo_numero_cuenta_origen").value = datos[10];
        
        this.idBancoReceptor = datos[11];
        document.getElementById(this.nombre_instancia+"_campo_numero_cuenta_receptor").value = datos[12];
        
        document.getElementById(this.nombre_instancia+"_campo_numero_operacion").value = datos[13];
        
        this.getSelectorEmpresa().setTextoDeOpcionSeleccionada(datos[15]);
        this.getSelectorEmpresa().muestraComponente();
        
        this.getSelectorClientes().setTextoDeOpcionSeleccionada(datos[16]);
        this.getSelectorClientes().muestraComponente();
        
        this.getSelectorFormasPago().setTextoDeOpcionSeleccionada(datos[17]);
        this.getSelectorFormasPago().muestraComponente();
        
        this.getSelectorMoneda().setTextoDeOpcionSeleccionada(datos[18]);
        this.getSelectorMoneda().muestraComponente();
        
        this.getSelectorBancoOrigen().setTextoDeOpcionSeleccionada(datos[19]);
        this.getSelectorBancoOrigen().muestraComponente();
        
        this.getSelectorBancoReceptor().setTextoDeOpcionSeleccionada(datos[20]);
        this.getSelectorBancoReceptor().muestraComponente();
        
        //console.log("bancarizado: "+datos[21]);
        if(datos[21]) this.bancarizado = true;
        else this.bancarizado = false;
        
        //seteamos zona de tipo de cambio
        this.setZonaTipoCambio();
        
        //seteamos la zona de elementos bancarizados si aplica
        this.setZonaElementosBancarizados();
    }
    
    this.procesaGuardado = function(respuesta){
        //console.log("respuesta guardado: "+respuesta);
        if(respuesta > 0 && !isNaN(respuesta)){
            alert("Pago recibido guardado!");
            if(this.idPago == 0) this.idPago = respuesta;
            else this.infoPago = null;
            this.editaForma();
        }else{
            alert("No se pudo guardar el pago recibido, por favor intenta nuevamente!");
            document.getElementById(this.nombre_instancia+"_zona_boton_guardar").innerHTML = "<a class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".validaForma()' style='margin:0 auto 0 auto;'>Guardar</a>";
        }
    }
    //VALIDAR Y GUARDAR FORMA DE PAGO
    
}
var CatalogoFormaPagoSat = function(){

    this.formasPago = null;
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Catálogo de Formas de Pago SAT");
        this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo();
        
        //realizamos la búsqueda en la base de datos de pacientes
        if(!this.formasPago) getResultadoAjax("opcion=50", this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
        else this.muestraResultados(this.formasPago);
    }
    
    this.muestraResultados = function(respuesta){
        this.formasPago = respuesta;
        //console.log("formas de pago: "+respuesta);
        
        var formasPago = respuesta.split("^");
        formasPago.pop();
        if(formasPago.length == 0){
            var contenido = "<div>No se encontró el catálogo de Formas de Pago Sat</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(formasPago);
        
        var encabezados = new Array("Clave", "Nombre");
        var medidas = new Array("10%", "80%");
        var alineaciones = new Array("left", "left");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, null, null);
        
        this.getListaSeleccionable().setOpcionBorrar(false);
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
}
var CatalogoMonedasSat = function(){
    
    this.monedas = null;
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Catálogo de Monedas SAT");
        this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo();
        
        //realizamos la búsqueda en la base de datos de pacientes
        if(!this.formasPago) getResultadoAjax("opcion=51", this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
        else this.muestraResultados(this.formasPago);
    }
    
    this.muestraResultados = function(respuesta){
        this.monedas = respuesta;
        //console.log("monedas: "+respuesta);
        
        var monedas = respuesta.split("^");
        monedas.pop();
        if(monedas.length == 0){
            var contenido = "<div>No se encontró el catálogo de Monedas del Sat</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(monedas);
        
        var encabezados = new Array("Clave", "Nombre");
        var medidas = new Array("30%", "60%");
        var alineaciones = new Array("left", "left");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, null, null);
        
        this.getListaSeleccionable().setOpcionBorrar(false);
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
}
var CatalogoBancosSat = function(){
    
    this.bancos = null;
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Catálogo de Bancos SAT");
        this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo();
        
        //realizamos la búsqueda en la base de datos de pacientes
        if(!this.formasPago) getResultadoAjax("opcion=52", this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
        else this.muestraResultados(this.formasPago);
    }
    
    this.muestraResultados = function(respuesta){
        this.bancos = respuesta;
        //console.log("bancos: "+respuesta);
        
        var bancos = respuesta.split("^");
        bancos.pop();
        if(bancos.length == 0){
            var contenido = "<div>No se encontró el catálogo de Bancos Sat</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(bancos);
        
        var encabezados = new Array("Clave", "RFC", "Nombre");
        var medidas = new Array("10%", "20%", "60%");
        var alineaciones = new Array("left", "left", "left");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, null, null);
        
        this.getListaSeleccionable().setOpcionBorrar(false);
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
}
var CatalogoFacturasEmitidas = function(){
    
    //1 gasto de cliente
    //2 pago recibido
    this.tipoElemento = 1;
    
    this.setTipoElemento = function(tipo){
        this.tipoElemento = tipo;
    }
    
    this.idElemento = 0;
    
    this.setIdElemento = function(idElemento){
        this.idElemento = idElemento;
    }
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Facturas Emitidas");
        
        this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo();
        
        //realizamos la búsqueda en la base de datos de pacientes
        getResultadoAjax("opcion=56&idElemento="+this.idElemento+"&tipoElemento="+this.tipoElemento, this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
    }
    
    this.muestraResultados = function(respuesta){
        var facturas = respuesta.split("^");
        facturas.pop();
        if(facturas.length == 0){
            var contenido = "<div>No hay facturas emitidas</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        var nuevasFacturas = new Array();
        var factura;
        var divEstatus = "";
        for(var i=0; i<facturas.length; i++){
            factura = facturas[i].split("|");
            
            if(factura[5] == 1) divEstatus = "<div class='factura_activa blue white-text'>Activa</div>";
            else if(factura[5] == -2) divEstatus = "<div class='factura_cancelada red white-text'>Cancelada</div>";
            nuevasFacturas.push(factura[0]+"|"+factura[1]+"|"+factura[2]+"|"+factura[3]+"|"+factura[4]+"|"+divEstatus+"|"+factura[6]+"|"+factura[7]+"|"+factura[5]);
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(nuevasFacturas);
        //idFacturaEmitida, nombreUsuario, cantidadFacturada, fecha, hora, textoEstatus, tipoFactura, idElemento, estatus
        
        var encabezados = new Array("Facturado por", "Fecha", "Hora", "Cantidad", "Estatus");
        var medidas = new Array("40%", "15%", "10%", "15%", "10%");
        var alineaciones = new Array("left", "center", "center", "right", "center");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, null, null);
        
        this.getListaSeleccionable().setOpcionBorrar(false);
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
}
var CatalogoUsosCFDISat = function(){
    
    this.usos = null;
    
    this.muestraComponente = function(){
        this.setTituloCatalogo("Catálogo de Usos CFDI del SAT");
        this.muestraEstructuraCatalogoPrincipalSinOpcionAgregarNuevo();
        
        //realizamos la búsqueda en la base de datos de pacientes
        if(!this.formasPago) getResultadoAjax("opcion=59", this.nombre_instancia+".muestraResultados", this.getZonaElementosDelCatalogo());
        else this.muestraResultados(this.usos);
    }
    
    this.muestraResultados = function(respuesta){
        this.usos = respuesta;
        //console.log("usos: "+respuesta);
        
        var usos = respuesta.split("^");
        usos.pop();
        if(usos.length == 0){
            var contenido = "<div>No se encontró el catálogo de Usos CFDI Sat</div>";
            this.getZonaElementosDelCatalogo().innerHTML = contenido;
            return;
        }
        
        //metemos los datos que recuperamos desde db
        this.getListaSeleccionable().setElementosRecuperados(usos);
        
        var encabezados = new Array("Clave", "Uso");
        var medidas = new Array("20%", "60%");
        var alineaciones = new Array("center", "left");
        
        //encabezados, medidas, alineaciones, opcion_detalle, opcion_elimina, funcion_elemento_seleccionado, funcion_detalle, funcion_elimina, funcion_tabla_mostrada
        this.setDatosVisualizacionLista(encabezados, medidas, alineaciones, 0, 0, this.nombre_instancia+".elementoSeleccionado()", null, null, null);
        
        this.getListaSeleccionable().setOpcionBorrar(false);
        this.getListaSeleccionable().setOpcionDetalle(false);
        
        this.muestraElementosDelCatalogo();
    }
}
