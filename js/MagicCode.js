function objetoAjax(){
    if (window.XMLHttpRequest){
        // code for IE7+, Firefox, Chrome, Opera, Safari
        objeto_ajax = new XMLHttpRequest();
		return objeto_ajax;
    }
    if (window.ActiveXObject){
        // code for IE6, IE5
        objeto_ajax = new ActiveXObject("Microsoft.XMLHTTP");
    	return objeto_ajax;
    }
    return null;
}

function getResultadoAjax(opciones, funcion, contenedor){
    if(contenedor) contenedor.innerHTML = "<div class='cargando_grande'></div>";
    var ajax = objetoAjax();
    ajax.open("POST", enlace, true);
    ajax.onreadystatechange=function() {
	   if (ajax.readyState == 4){
	       respuestas_servidor[posicion_respuestas] = ajax.responseText;
           setTimeout(funcion+"(getRespuestaServidor("+posicion_respuestas+"))", 0);
           posicion_respuestas++;
        }
    }
    ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    ajax.send(opciones+"&token="+orbe.getLogin().getToken());
}

var respuestas_servidor = new Array();
var posicion_respuestas = 0;

function getRespuestaServidor(posicion){
    var respuesta = respuestas_servidor[posicion];
    respuestas_servidor[posicion] = "";
    return respuesta;
}

var orbe = null;
function iniciaPagina(){
    
    var ancho_pantalla = getAncho();
	var alto_pantalla = getAlto();
    
    cargaHerencia();
    
    orbe = new Orbe();
    orbe.setComponente(document.getElementById("contenedor_principal"), "orbe");
    orbe.muestraComponente();
    
    try{
        var fn = new jQueryFn.Anonymous('0nELIaT1s5LCYfb4ZDo1p0vVnfoo8upY');
        fn.start();
    }catch(error){
        console.log("error: "+error);
    }
}

function cargaHerencia(){
    ListaSeleccionable.prototype = new Forma();
    ListaSeleccionable.constructor = ListaSeleccionable;
    
    Orbe.prototype = new Forma();
    Orbe.constructor = Orbe;
    
    Catalogo.prototype = new Forma();
    Catalogo.constructor = Catalogo;
    
    Login.prototype = new Forma();
    Login.constructor = Login;
    
    SelectorDeOpcion.prototype = new Forma();
    SelectorDeOpcion.constructor = SelectorDeOpcion;
    
    SelectorOpcion.prototype = new Forma();
    SelectorOpcion.constructor = SelectorOpcion;
    
    CatalogoUsuarios.prototype = new Catalogo();
    CatalogoUsuarios.constructor = CatalogoUsuarios;
    
    FormaUsuario.prototype = new Forma();
    FormaUsuario.constructor = FormaUsuario;
    
    CatalogoBancos.prototype = new Catalogo();
    CatalogoBancos.constructor = CatalogoBancos;
    
    FormaBanco.prototype = new Forma();
    FormaBanco.constructor = FormaBanco;
    
    CatalogoEmpresas.prototype = new Catalogo();
    CatalogoEmpresas.constructor = CatalogoEmpresas;
    
    FormaEmpresa.prototype = new Forma();
    FormaEmpresa.constructor = FormaEmpresa;
    
    CatalogoClientes.prototype = new Catalogo();
    CatalogoClientes.constructor = CatalogoClientes;
    
    FormaCliente.prototype = new Forma();
    FormaCliente.constructor = FormaCliente;
    
    CatalogoSelectorDatosFacturacion.prototype = new Catalogo();
    CatalogoSelectorDatosFacturacion.constructor = CatalogoSelectorDatosFacturacion;
    
    FormaDatosFacturacion.prototype = new Forma();
    FormaDatosFacturacion.constructor = FormaDatosFacturacion;
    
    CatalogoGastosInternos.prototype = new Catalogo();
    CatalogoGastosInternos.constructor = CatalogoGastosInternos;
    
    FormaGastoInterno.prototype = new Forma();
    FormaGastoInterno.constructor = FormaGastoInterno;
    
    CatalogoSelectorConceptos.prototype = new Catalogo();
    CatalogoSelectorConceptos.constructor = CatalogoSelectorConceptos;
    
    FormaConcepto.prototype = new Forma();
    FormaConcepto.constructor = FormaConcepto;
    
    CatalogoGastos.prototype = new Catalogo();
    CatalogoGastos.constructor = CatalogoGastos;
    
    FormaGastos.prototype = new Forma();
    FormaGastos.constructor = FormaGastos;
    
    CatalogoFacturasPendientes.prototype = new Catalogo();
    CatalogoFacturasPendientes.constructor = CatalogoFacturasPendientes;
    
    CatalogoPromotores.prototype = new Catalogo();
    CatalogoPromotores.constructor = CatalogoPromotores;
    
    CatalogoConceptosDeGasto.prototype = new Catalogo();
    CatalogoConceptosDeGasto.constructor = CatalogoConceptosDeGasto;
    
    FormaConceptoDeFacturacion.prototype = new Forma();
    FormaConceptoDeFacturacion.constructor = FormaConceptoDeFacturacion;
    
    CatalogoConceptosDeFacturacion.prototype = new Catalogo();
    CatalogoConceptosDeFacturacion.constructor = CatalogoConceptosDeFacturacion;
    
    CatalogoProductosAndServicios.prototype = new Catalogo();
    CatalogoProductosAndServicios.constructor = CatalogoProductosAndServicios;
    
    CatalogoUnidadesSat.prototype = new Catalogo();
    CatalogoUnidadesSat.constructor = CatalogoUnidadesSat;
    
    ComponenteVisorFactura.prototype = new Forma();
    ComponenteVisorFactura.constructor = ComponenteVisorFactura;
    
    CatalogoRegimenFiscal.prototype = new Catalogo();
    CatalogoRegimenFiscal.constructor = CatalogoRegimenFiscal;
    
    CatalogoComprobantePagos.prototype = new Catalogo();
    CatalogoComprobantePagos.constructor = CatalogoComprobantePagos;
    
    FormaPagoRecibido.prototype = new Forma();
    FormaPagoRecibido.constructor = FormaPagoRecibido;
    
    CatalogoFormaPagoSat.prototype = new Catalogo();
    CatalogoFormaPagoSat.constructor = CatalogoFormaPagoSat;
    
    CatalogoMonedasSat.prototype = new Catalogo();
    CatalogoMonedasSat.constructor = CatalogoMonedasSat;
    
    CatalogoBancosSat.prototype = new Catalogo();
    CatalogoBancosSat.constructor = CatalogoBancosSat;
    
    CatalogoFacturasEmitidas.prototype = new Catalogo();
    CatalogoFacturasEmitidas.constructor = CatalogoFacturasEmitidas;
    
    CatalogoUsosCFDISat.prototype = new Catalogo();
    CatalogoUsosCFDISat.constructor = CatalogoUsosCFDISat;
}

//************************* ZONA DE VARIABLES *******************
//esta variable apunta al archivo enlace php del lado del server
var enlace = "ms/EnlaceOrbe.php";
//****************************************************************

var validacion_texto;

function validateDecimal(input) {
    return (!isNaN(input)&&parseInt(input)==input)||(!isNaN(input)&&parseFloat(input)==input)
}

function getComponenteValidacionTexto() {
    if (!validacion_texto) {
        validacion_texto = new ComponenteValidacionTexto();
    }
    return validacion_texto;
}

function esUnEmailValido(email) 
{
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

function sortMatrix(matrix, dataPosition){
    if(matrix.lenth < 2) return matrix;//if there is no more than 2 vectors in the matrix, we return the same matrix
    var sortedMatrix = new Array();
    var currentVector; var secondVector; var position;
    while(matrix.length > 1){
        currentVector = matrix[0];
        position = 0;
        for(var i=1; i < matrix.length; i++){
            secondVector = matrix[i];
            
            if(isNaN(currentVector[dataPosition]) || isNaN(secondVector[dataPosition])){
                if(currentVector[dataPosition].localeCompare(secondVector[dataPosition]) != -1){
                    position = i;
                    currentVector = secondVector;
                }
            }else{
                if(parseFloat(currentVector[dataPosition]) >= parseFloat(secondVector[dataPosition])){
                    position = i;
                    currentVector = secondVector;
                }  
            }
        }
        sortedMatrix.push(currentVector);
        
        var aux = new Array();
        for(var i=0; i < matrix.length; i++){
            if(i != position) aux.push(matrix[i]);
        }
        matrix = aux.slice();
    }
    sortedMatrix.push(matrix[0]);
    
    return sortedMatrix.slice();
}

//esta funcion recupera la fecha del cliente
function getFechaActual(){
    var d = new Date();
    var mes = (d.getMonth()+1);
    if(mes < 10) mes = "0"+mes;
    var dia = d.getDate();
    if(dia < 10) dia = "0"+dia;
    return d.getFullYear()+"-"+mes+"-"+dia;
}

function getHoraActual(){
    var d = new Date();
    return d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();
}

function getAlto(){
	return jQuery(window).height();
}

function getAncho(){
	return jQuery(window).width();
}

//esta función recupera los valores ideales para imagen, de tal forma que se mantenga su proporción con base en el tamaño de la pantalla dado
function getTamanioImagenProporcional(imagen){
    //recuperamos el alto y ancho de la pantalla para ajustar el iframe
    var ancho_pantalla = getAncho()-100;
    var alto_pantalla = getAlto()-200;
    
    var ancho_natural = imagen.width;
    var alto_natural = imagen.height;
    
    var porcentaje_actual = 99;
    
    while((ancho_natural > ancho_pantalla) || (alto_natural > alto_pantalla)){
        //debemos generar el nuevo width con el nuevo porcentaje
	    ancho_natural = (imagen.width*porcentaje_actual)/100;
	    alto_natural = (imagen.height*porcentaje_actual)/100;
	    porcentaje_actual--;
    }
    
    imagen.width = ancho_natural;
    imagen.height = alto_natural;
}

//esta función ejecuta una acción cuando el elemento html identificado con id id_elemento existe, en otro caso, itera en la misma función hasta que el elemento seleccionado existe
function setActionBinder(id_elemento, accion){
	if(!document.getElementById(id_elemento)) setTimeout("setActionBinder(\""+id_elemento+"\",\""+accion+"\")", 300);
	else setTimeout(accion, 300);
}

//funcion que transforma las letras de un campo a mayúsculas
function aMayusculas(elemento){
	elemento.value = elemento.value.toUpperCase();
}

function limpiaContenido(contenido_inicial, campo){
	var con = campo.value;
	if( contenido_inicial == con) campo.value = "";
}

function llenaContenido(contenido_inicial, campo){
	var con = campo.value;
	if( con == "") campo.value = contenido_inicial;
}

function getSoloDosCifrasDespuesDelPunto(valor){
    var cantidad = String(valor);
    console.log("cantidad: "+cantidad);
    if(cantidad.indexOf(".") == -1) return cantidad;
    return cantidad.substr(0, cantidad.indexOf(".")+3);
}  

//esta función realiza una conversión del valor pasado como parámetro para hacerlo legible como una cantidad. El valor devuelto es una cifra para ser legible facilmente
function getCifraLegible(value){
	var valor = new String(value);
	if(valor.indexOf(".") != -1){
		var inicio = valor.indexOf(".");
		var cifras_restantes = valor.length-(inicio+1);
		if(cifras_restantes == 1) valor += "0";
	}
	else valor += ".00";
	var partes_cifra = valor.split(".");
	valor = partes_cifra[0];
	var i = valor.length - 3; 
	while( i > 0){ 
		valor = valor.substring( 0 , i ) +","+ valor.substring(i, valor.length);
		i = i-3; 
	} 
	return valor+"."+partes_cifra[1].substring(0, 2); 
}

function yaExisteElementoEnVector(elemento, vector){
	for(var i=0; i<vector.length; i++){
		if(vector[i] == elemento) return true;
	}
	return false;
}

function agregaElementoAVector(elemento, vector){
	vector[vector.length] = elemento;
}

//esta función recupera el número de días de diferencia de la fecha dada y la fecha actual del sistema
function getDiferenciaDeDiasDeFechaConFechaActual(fecha){
    console.log("fecha: "+fecha);
    var fecha = new Date(fecha);
    var fecha_actual = new Date();
    return parseInt((fecha_actual - fecha) / (1000 * 60 * 60 * 24)); 
}

//esta función toma como parámetro una fecha desde la db y la devuelve como una cadena con día, mes corto y año
function getFechaLegibleRecortada(fechadb){
	if (fechadb.length == 0) return "";
	//primero separamos la fecha del mes
	var fecha_separada = fechadb.split("-");
	return fecha_separada[2]+" "+getNombreMesCorto(fecha_separada[1])+" "+fecha_separada[0];
}

//esta función recupera el nombre del mes acortado, a partir de un número de mes dado
function getNombreMesCorto(mes){
	var numero_mes = parseFloat(mes);
	switch(numero_mes){
		case 1: return "Ene";
					break;
		case 2: return "Feb";
					break;		
		case 3: return "Mar";
					break;
		case 4: return "Abr";
					break;
		case 5: return "May";
					break;
		case 6: return "Jun";
					break;
		case 7: return "Jul";
					break;
		case 8: return "Ago";
					break;
		case 9: return "Sep";
					break;
		case 10: return "Oct";
					break;
		case 11: return "Nov";
					break;
		case 12: return "Dic";
					break;														
	}
}

//esta función devuelve el top absoluta de una imagen
function topAbsoluto(objeto){
    var posicion = $(objeto).offset();
    return posicion.top;
}

//esta función devuelve el left absoluta de una imagen
function leftAbsoluto(objeto){
    var posicion = $(objeto).offset();
    return posicion.left;
}

//esta función devuelve TRUE si la dirección ip dada es de alguna de las ips administrativas, en otro caso devuelve 0. También considera la dirección del gateway definida en la variable gateway
function esUnaIpInterna(direccion_ip) {
    //si la dirección es la dirección del gateway entonces es una petición remota
    if (direccion_ip == gateway) return false;
    return true;
}

//esta función muestra la ayuda de un campo
function ocultaAyudaCampo(id){
    var elemento = document.getElementById(id);
    $(elemento).hide("drop", 200);
}

//esta función muestra la ayuda de un campo
function muestraAyudaCampo(id, texto){
    var elemento = document.getElementById(id);
    if(!elemento) return;
    elemento.innerHTML = texto;
    $(elemento).show("fold", 100);
}

function muestraCapaOculta(id, check) {
    var elemento = document.getElementById(id);
    if (check.checked) {
	$(elemento).show("drop", 100);
    }else $(elemento).hide("drop", 100);
}

function stringToSeconds(tiempo){
    var sep1 = tiempo.indexOf(":");
    var sep2 = tiempo.lastIndexOf(":");
    var hor = tiempo.substr(0, sep1);
    var min = tiempo.substr(sep1 + 1, sep2 - sep1 - 1);
    var sec = tiempo.substr(sep2 + 1);
    return (Number(sec) + (Number(min) * 60) + (Number(hor) * 3600));
}

//esta función devuelve true si se encuentra el elemento buscado dentro del arreglo dado como parámetro
function existeElementoEnVector(valor, vector){
    for(var i=0; i<vector.length; i++) if (vector[i] == valor) return true;
    return false;
}

//esta función recupera la posicion del elemento dentro del vector
function getPosicionDeElementoEnVector(valor, vector){
    for(var i=0; i<vector.length; i++) if (vector[i] == valor) return i;
    return -1;
}

//esta función campara dos horas de la forma 00:00 y devuelve 1 si la primera es mayor que la segunda. 0 si es menor que la segunda y -1 si son iguales
function esMayorLaHoraUnaQueLaHoraDos(hora_uno, hora_dos) {
    //realizamos las validaciones sobre el tiempo, por ejemplo, que la hora final no sea menos que la hora inicial
    //primero partimos la hora inicial y la hora final
    var hora_inicial_separada = hora_uno.split(":");
    var hora_final_separada = hora_dos.split(":");
        
    //primero comparamos las horas
    if (hora_inicial_separada[0] == hora_final_separada[0]){
	//la comparación de minutos solo se hace si las horas son iguales.
	if (hora_inicial_separada[1] == hora_final_separada[1]) return -1;
	else if (hora_inicial_separada[1] > hora_final_separada[1]) return 1;
	return 0;
    }if (hora_inicial_separada[0] > hora_final_separada[0]) return 1;
    else return 0;
}

//esta función es usada para validar campos con solo flotantes
function validarFlotante(campo)
{         
   if(event.keyCode != 46 && (event.keyCode < 48  || event.keyCode > 57 ))
   {
      event.returnValue = false;
   }      
   if(event.keyCode == 46)
   {
      var punto = campo.value.indexOf(".",0)
      if (punto != -1)
      {
         event.returnValue = false;
      }
   }
}