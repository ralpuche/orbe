var Orbe = function(){
    
    this.login = null;
    this.getLogin = function(){
        if(!this.login){
            this.login = new Login();
            this.login.setComponente(null, this.nombre_instancia+".login");
            this.login.setFuncionSiHaySesionActiva(this.nombre_instancia+".muestraComponentePrincipal()");
        }
        return this.login;
    }
    
    this.opcion_seleccionada = null;
    this.clase_opcion_seleccionada = null;
    this.catalogoSeleccionado = null;
    
    this.muestraComponente = function(){
        this.getLogin().haySesionActiva();
    }
    
    this.muestraComponentePrincipal = function(){
        this.contenedor = document.getElementById("contenedor_principal");
        
        var contenido = '<div class="row center-align">';
        contenido += '<div id="menu_principal" class="col s0 m2 l2 center-align menu_principal" style="height:'+(getAlto())+'px; background-color:white;"></div>';
        contenido += '<div id="zona_principal" class="col s12 m10 l10" style="padding:30px;"></div>';
        contenido += '</div>';
        
        this.contenedor.innerHTML = contenido;
        
        this.muestraMenuPrincipal();
        
        if(this.login.getPerfil() == 1) document.getElementById("opcionUno").click();
        else if(this.login.getPerfil() == 2) document.getElementById("opcion_dos").click();
        else if(this.login.getPerfil() == 3) document.getElementById("opcion_tres").click();
        else if(this.login.getPerfil() == 4) document.getElementById("opcionUno").click();
        else if(this.login.getPerfil() == 5) document.getElementById("opcionUno").click();
    }
    
    this.muestraMenuPrincipal = function(){
        var contenido = "<br/><img src='images/logo2.jpg' class='responsive-img'/><br/><br/><br/>";
        
        contenido += "<table align='center' width='96%' cellpadding='6' cellspacing='6' border='0'>";
        
        if(this.login.getPerfil() == 1){
            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraGastosClientes();' id='opcionUno'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='gastos'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Gastos Clientes</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";
            
            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraPagosRecibidos();'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='xml'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Pagos</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";

            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraFacturasPendientesDeClientes();'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='xml'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Facturas Pendientes</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";

            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraCatalogoPromotores();'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='usuarios'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Promotores</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";

            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraGastosInternos();'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='gastos'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Gastos Internos</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";

            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraClientes();'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='clientes'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Clientes</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";

            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraEmpresas();'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='empresas'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Empresas</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";

            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraBancos();'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='bancos'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Bancos</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";

            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraUsuarios();'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='usuarios'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Usuarios</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";   
        }else if(this.login.getPerfil() == 2){//Facturista
            
            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraFacturasPendientesDeClientes();' id='opcion_dos'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='xml'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Facturas Pendientes</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";
        }else if(this.login.getPerfil() == 3){//Promotor
            
            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraCatalogoPromotores();' id='opcion_tres'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='usuarios'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Promotores</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";
        }else if(this.login.getPerfil() == 4){//Operaciones
            
            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraGastosClientes();' id='opcionUno'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='gastos'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Gastos Clientes</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";
            
            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraGastosInternos();'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='gastos'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Gastos Internos</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";

            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraClientes();'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='clientes'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Clientes</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";
            
            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraBancos();'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='bancos'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Bancos</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";
        }else if(this.login.getPerfil() == 5){//Clientes
            
            contenido += "<tr class='opcion_menu' onclick='"+this.nombre_instancia+".setOpcionPrincipalSeleccionada(this); "+this.nombre_instancia+".muestraGastosClientes();' id='opcionUno'>";
            contenido += "<td align='center' valign='top' width='20%'>";
            contenido += "<div class='gastos'></div>";
            contenido += "</td>";
            contenido += "<td align='left' valign='top' width='80%'>";
            contenido += "<div>Gastos Clientes</div>";
            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr><td></td><td></td></tr>";
        }
        
        contenido += "<tr class='opcion_cuatro' onclick='"+this.nombre_instancia+".cerrarSesion()'>";
        contenido += "<td align='center' valign='top' width='20%'>";
        contenido += "<div class='cerrar_sesion'></div>";
        contenido += "</td>";
        contenido += "<td align='left' valign='top' width='80%'>";
        contenido += "<div>Salir</div>";
        contenido += "</td>";
        contenido += "</tr>";
        
        contenido += "</table>";
        
        document.getElementById("menu_principal").innerHTML = contenido;
    }
    
    this.muestraGastosClientes = function(){
        this.catalogoSeleccionado = new CatalogoGastos();
        this.catalogoSeleccionado.setComponente(document.getElementById("zona_principal"), "orbe.catalogoSeleccionado");
        this.catalogoSeleccionado.muestraComponente();
    }
    
    this.muestraGastosInternos = function(){
        this.catalogoSeleccionado = new CatalogoGastosInternos();
        this.catalogoSeleccionado.setComponente(document.getElementById("zona_principal"), "orbe.catalogoSeleccionado");
        this.catalogoSeleccionado.muestraComponente();
    }
    
    this.muestraPagosRecibidos = function(){
        this.catalogoSeleccionado = new CatalogoComprobantePagos();
        this.catalogoSeleccionado.setComponente(document.getElementById("zona_principal"), "orbe.catalogoSeleccionado");
        this.catalogoSeleccionado.muestraComponente();
    }
    
    this.muestraInicio = function(){
        
    }
    
    //muestra el catalogo de clientes
    this.muestraClientes = function(){
        this.catalogoSeleccionado = new CatalogoClientes();
        this.catalogoSeleccionado.setComponente(document.getElementById("zona_principal"), "orbe.catalogoSeleccionado");
        this.catalogoSeleccionado.muestraComponente();
    }
    
    //muestra las facturas pendientes de los gastos de los clientes
    this.muestraFacturasPendientesDeClientes = function(){
        this.catalogoSeleccionado = new CatalogoFacturasPendientes();
        this.catalogoSeleccionado.setComponente(document.getElementById("zona_principal"), "orbe.catalogoSeleccionado");
        this.catalogoSeleccionado.muestraComponente();
    }
    
    //muestra empresas
    this.muestraEmpresas = function(){
        this.catalogoSeleccionado = new CatalogoEmpresas();
        this.catalogoSeleccionado.setComponente(document.getElementById("zona_principal"), "orbe.catalogoSeleccionado");
        this.catalogoSeleccionado.muestraComponente();
    }
    
    //muestra el catalogo de bancos
    this.muestraBancos = function(){
        this.catalogoSeleccionado = new CatalogoBancos();
        this.catalogoSeleccionado.setComponente(document.getElementById("zona_principal"), "orbe.catalogoSeleccionado");
        this.catalogoSeleccionado.muestraComponente();
    }
    
    //mostramos el catalogo de promotores
    this.muestraCatalogoPromotores = function(){
        this.catalogoSeleccionado = new CatalogoPromotores();
        this.catalogoSeleccionado.setComponente(document.getElementById("zona_principal"), "orbe.catalogoSeleccionado");
        this.catalogoSeleccionado.muestraComponente();
    }
    
    //función para mostrar usuarios del sistema
    this.muestraUsuarios = function(){
        this.catalogoSeleccionado = new CatalogoUsuarios();
        this.catalogoSeleccionado.setComponente(document.getElementById("zona_principal"), "orbe.catalogoSeleccionado");
        this.catalogoSeleccionado.muestraComponente();
    }
    
    this.setOpcionPrincipalSeleccionada = function(opcion){
        if(opcion.className.indexOf("_seleccionada") == -1){
            if(this.opcion_seleccionada) this.opcion_seleccionada.className = this.clase_opcion_seleccionada;
            this.opcion_seleccionada = opcion;
            this.clase_opcion_seleccionada = this.opcion_seleccionada.className;
            opcion.className = "opcion_menu_seleccionada";
        }
    }
    
    this.cerrarSesion = function(){
        getResultadoAjax("opcion=4", this.nombre_instancia+".procesaSesionCerrada", null);   
    }
    
    this.procesaSesionCerrada = function(respuesta){
        if(respuesta == 1){
            this.login = null;
            this.muestraComponente();
        }
    }
}