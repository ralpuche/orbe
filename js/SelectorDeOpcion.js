var SelectorDeOpcion = function(){
    
    this.visualizacion = 1;
    this.setVisualizacion = function(tipo_visualizacion){
        this.visualizacion = tipo_visualizacion;
    }
    
    this.texto_opcion_seleccionada = "Selecciona";
    this.setTextoDeOpcionSeleccionada = function(texto){
        this.texto_opcion_seleccionada = texto;
        if(document.getElementById(this.nombre_instancia+"_div_selector")) document.getElementById(this.nombre_instancia+"_div_selector").innerHTML = this.texto_opcion_seleccionada;
    }
    
    this.funcion_selector_presionado = "";
    this.setFuncionSelectorPresionado = function(funcion){
        this.funcion_selector_presionado = funcion;
    }
    
    this.funcion_opcion_selector_presionado = "";
    this.setFuncionOpcionSelectorPresionado = function(funcion){
        this.funcion_opcion_selector_presionado = funcion;
    }
    
    this.muestraComponente = function(){
        console.log("visualizacion: "+this.visualizacion);
        if(this.visualizacion == 1) var contenido = "<div class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".selectorPresionado()' id='"+this.nombre_instancia+"_div_selector' style='width:96%;'>"+this.texto_opcion_seleccionada+"</div>";
        else contenido = "<table align='center' cellpadding='0' cellpadding='0' cellspacing='0'><tr><td align='center' width='90%'><div class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".selectorPresionado()' id='"+this.nombre_instancia+"_div_selector' style='width:100%;'>"+this.texto_opcion_seleccionada+"</div></td><td align='center' width='10%' valign='middle'><div class='waves-effect waves-light btn' onclick='"+this.nombre_instancia+".opcionSelectorPresionado()' id='"+this.nombre_instancia+"_div_opcion_selector' style='text-align:center;' style='width:100%;'><div class='info' style='box-sizing: content-box; box-shadow: none; padding-top: 18px;'></div></div></td></tr></table>";
        
        this.contenedor.innerHTML = contenido;
    }
    
    this.selectorPresionado = function(){
        if(this.funcion_selector_presionado) setTimeout(this.funcion_selector_presionado, 0);
    }
    
    this.opcionSelectorPresionado = function(){
        if(this.funcion_opcion_selector_presionado) setTimeout(this.funcion_opcion_selector_presionado, 0);
    }
}