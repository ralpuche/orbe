// JavaScript Document del Componente SelectorFecha
var SelectorFechas = function(){
	
	//esta variable almacena la fecha actual. Para ello genera un objeto del tipo fecha actual al iniciar la ejecución del script
	this.fecha_actual = new Date();
	
	//esta variable almacena el nombre del contenedor del componente fecha
	this.contenedor_selector_fecha = null;
	
	//esta variable almacena el nombre de la instancia de tipo selector de fecha. El nombre por defector es selector fecha
	this.nombre_instancia = "selector_fechas";
	
	//esta variable indica si se ha cambiado algún combo. De ser así, entonces la variable se pone en true en otro caso en false. Esta variable determina el valor recuperado cuando se llama al método para recuperar la fecha indicada por los combos
	this.fecha_modificada = false;
	
	this.funcion_cambia = null;
    this.funcionOnChange = function(funcion){
        this.funcion_cambia = funcion;
    }
	
	//método principal incializa las variables del componente
	this.setSelectorFecha = function(contenedor, nombre){
		this.contenedor_selector_fecha = contenedor;
		this.nombre_instancia = nombre;
		this.fecha_modificada = false;
	}
    
    //método agregado para homogeneidad
    this.setComponente = function(contenedor, nombre){
        this.contenedor_selector_fecha = contenedor;
		this.nombre_instancia = nombre;
		this.fecha_modificada = false;
    }
    
    this.muestraComponente = function(){
        this.muestraComboFechas();
    }
	
	//esta función sirve para modificar la fecha del componente, por la fecha dada como parametro
	this.setFechaDesdeDB = function(fecha){
		//primero separamos la fecha dada como parametro
		var fecha_separada = fecha.split("-");
		this.fecha_actual = new Date(fecha_separada[0], (fecha_separada[1]-1), fecha_separada[2]);
		this.fecha_modificada = true;
		this.muestraComboFechas();
	}

	//esta función dibuja los combos del componente
	this.muestraComboFechas = function(){
		//si el contenedor del componente es null, entonces se regresa sin realizar algún cambio
		if(this.contenedor_selector_fecha == null) return;
		//primero se dibuja la estructura del selector de fechas
		this.setEstructuraSelectorFechas();
		//se dibuja y muestra el combo de los años
		this.setComboAnios();
		//se dibuja y muestra el combo de los meses
		this.setComboMeses();
		//esta función dibuja el combo con los días
		this.setComboDias();
	}
		
	//esta función dibuja el combo de los años
	this.setComboAnios = function(){
		var contenido = "<select class='browser-default' id='combo_anios_"+this.nombre_instancia+"' onchange='"+this.nombre_instancia+".cambioCombos(); "+this.funcion_cambia+"'>";
		var anio  = this.fecha_actual.getFullYear();
		//agrega por defecto 4 anios antes y 4 anios después del anio actual.
		var anio_inicial = anio - 100;
		var anio_final =  anio + 30;
		for(anio_inicial; anio_inicial <= anio_final; anio_inicial++){
			if(anio_inicial == anio && this.fecha_modificada) contenido += "<option selected='selected' value='"+anio_inicial+"'>"+anio_inicial+"</option>";
			else contenido += "<option value='"+anio_inicial+"'>"+anio_inicial+"</option>";	
		}
		if(!this.fecha_modificada) contenido += "<option selected='selected' value='0'>-</option>";
		else contenido += "<option value='0'>-</option>";
		contenido += "</select>";
		document.getElementById('area_anios_'+this.nombre_instancia).innerHTML = contenido;
	}
	
	//esta función genera y mustra los meses en función del anio seleccionado
	this.setComboMeses = function(){
		var contenido = "<select class='browser-default' id='combo_meses_"+this.nombre_instancia+"' onchange='"+this.nombre_instancia+".cambioCombos(); "+this.funcion_cambia+"'>";
		for(var i=0; i < 12; i++){
			if(i == this.fecha_actual.getMonth() && this.fecha_modificada) contenido += "<option selected='selected' value="+(i+1)+">"+getNombreMes(i+1)+"</option>";
			else contenido += "<option value="+(i+1)+">"+getNombreMes(i+1)+"</option>";
		}
		if(!this.fecha_modificada) contenido += "<option selected='selected' value='0'>-</option>";
		else contenido += "<option value='0'>-</option>";
		contenido += "</select>";
		document.getElementById('area_meses_'+this.nombre_instancia).innerHTML = contenido;
	}
	
	//esta función dibuja el combo con los días
	this.setComboDias = function(){
		var mes = this.fecha_actual.getMonth()+1;
		var max_dias = getMaximosDiasPorMes(mes, this.fecha_actual.getFullYear());
		var contenido = "<select class='browser-default' id='combo_dias_"+this.nombre_instancia+"' onchange='"+this.nombre_instancia+".cambioCombos(); "+this.funcion_cambia+"'>";
		for(i=0; i<max_dias; i++ ){
			if(i+1 == this.fecha_actual.getDate() && this.fecha_modificada) contenido += "<option selected='selected' value="+(i+1)+">"+(i+1)+"</option>";
			else contenido += "<option value="+(i+1)+">"+(i+1)+"</option>";
		}
		if(!this.fecha_modificada) contenido += "<option selected='selected' value='0'>-</option>";
		else contenido += "<option value='0'>-</option>";
		contenido += "</select>";
		document.getElementById('area_dias_'+this.nombre_instancia).innerHTML = contenido;
	}
	
	//esta función es utilizada para saber la fecha actual que tienen seleccionados los combos.
	this.getFechaActualParaDB = function(){
		if(!this.fecha_modificada) return null;
		var mes = document.getElementById('combo_meses_'+this.nombre_instancia).selectedIndex+1;
		if (mes < 10) mes = "0"+mes;
		var dia = document.getElementById('combo_dias_'+this.nombre_instancia).selectedIndex+1;
		if (dia < 10) dia = "0"+dia;
		var i_anio = document.getElementById('combo_anios_'+this.nombre_instancia).selectedIndex;
		return document.getElementById('combo_anios_'+this.nombre_instancia).options[i_anio].value+"-"+mes+"-"+dia;
	}
	
	//esta función es ejecutada por los combos cuando cambia alguno de ellos
	this.cambioCombos = function(){
		//primero recuperamos los tres combos
		var combo_mes = document.getElementById('combo_meses_'+this.nombre_instancia); 
		var combo_dias = document.getElementById('combo_dias_'+this.nombre_instancia);
		var combo_anios = document.getElementById('combo_anios_'+this.nombre_instancia)
		
		//recuperamos los valores para año, mes y dia
		var anio = combo_anios.options[combo_anios.selectedIndex].value;
		var mes = combo_mes.options[combo_mes.selectedIndex].value;
		var dia = combo_dias.options[combo_dias.selectedIndex].value;
		
		//si el valor del dia es -, entonces quiere decir que el usuario movió el día a este valor para indicar que la fecha debe ponerse en el estado - - -, y la fecha modificada a false
		if(dia == 0){
			//entonces movemos los valores de los otros combos a options.lengt en selected index
			combo_mes.selectedIndex = combo_mes.options.length-1;
			combo_anios.selectedIndex = combo_anios.options.length-1;
			this.fecha_modificada = false;
			return;
		}else{
			if(dia != 0 && mes == 0 && anio == 0){
				//en este caso el combo estaba en el estado - - -, se movió el combo de día y automáticamente se deberá mover el combo a una fecha válida, considerando la fecha actual
				this.fecha_modificada = true;
				this.setComboAnios();
				this.setComboMeses();
				return;
			}
		}
		
		var mes = combo_mes.selectedIndex;
		var dia = combo_dias.selectedIndex+1;
	
		this.fecha_actual.setDate(dia);
		this.fecha_actual.setMonth(mes);
		this.fecha_actual.setYear(anio);
		this.setComboAnios();
		this.setComboMeses();
		this.setComboDias();
		this.fecha_modificada = true;
	}
	
	//esta función genera la estructura del componente, para que acepte los combos de día mes año
	this.setEstructuraSelectorFechas = function(){
		var contenido = "<table id='area_fecha_"+this.nombre_instancia+"' align='center'><tr><td><div id='area_dias_"+this.nombre_instancia+"''></div></td><td><div id='area_meses_"+this.nombre_instancia+"''></div></td><td><div id='area_anios_"+this.nombre_instancia+"''></div></td></table>";	
		this.contenedor_selector_fecha.innerHTML = contenido;
	}
}

function getNumeroDeMesPorNombreCorto(nombre_mes){
    if(nombre_mes == "Ene") return "01";
    else if(nombre_mes == "Feb") return "02";
    else if(nombre_mes == "Mar") return "03";
    else if(nombre_mes == "Abr") return "04";
    else if(nombre_mes == "May") return "05";
    else if(nombre_mes == "Jun") return "06";
    else if(nombre_mes == "Jul") return "07";
    else if(nombre_mes == "Ago") return "08";
    else if(nombre_mes == "Sep") return "09";
    else if(nombre_mes == "Oct") return "10";
    else if(nombre_mes == "Nov") return "11";
    else if(nombre_mes == "Dic") return "12";
}

//esta función calcula la edad a partir de una fecha dada
function calcular_edad(dia_nacim,mes_nacim,anio_nacim)
{
	fecha_hoy = new Date();
	ahora_anio = fecha_hoy.getYear();
	ahora_mes = fecha_hoy.getMonth();
	ahora_dia = fecha_hoy.getDate();
	edad = (ahora_anio + 1900) - anio_nacim;
	if ( ahora_mes < (mes_nacim - 1))
	{
		edad--;
	}
	if (((mes_nacim - 1) == ahora_mes) && (ahora_dia < dia_nacim))
	{ 
		edad--;
	}
	if (edad > 1900)
	{
		edad -= 1900;
	}
	return edad;
}

function getNombreMes(numero_mes){
	switch(numero_mes){
		case 1: return "Ene";
					break;
		case 2: return "Feb";
					break;		
		case 3: return "Mar";
					break;
		case 4: return "Abr";
					break;
		case 5: return "May";
					break;
		case 6: return "Jun";
					break;
		case 7: return "Jul";
					break;
		case 8: return "Ago";
					break;
		case 9: return "Sep";
					break;
		case 10: return "Oct";
					break;
		case 11: return "Nov";
					break;
		case 12: return "Dic";
					break;																																		
	}
}

function getNombreMesCompleto(numero_mes){
	switch(numero_mes){
		case 1: return "Enero";
					break;
		case 2: return "Febrero";
					break;		
		case 3: return "Marzo";
					break;
		case 4: return "Abril";
					break;
		case 5: return "Mayo";
					break;
		case 6: return "Junio";
					break;
		case 7: return "Julio";
					break;
		case 8: return "Agosto";
					break;
		case 9: return "Septiembre";
					break;
		case 10: return "Octubre";
					break;
		case 11: return "Noviembre";
					break;
		case 12: return "Diciembre";
					break;																																		
	}
}

function getMaximosDiasPorMes(numero_mes, anio){
	switch(numero_mes){
		case 1: return 31;
					break;
		case 2: if(anio%4 == 0) return 29;
					else return 28;
					break;
		case 3: return 31;
					break;
		case 4: return 30;
					break;
		case 5: return 31;
					break;
		case 6: return 30;
					break;
		case 7: return 31;
					break;
		case 8: return 31;
					break;
		case 9: return 30;
					break;
		case 10: return 31;
					break;
		case 11: return 30;
					break;
		case 12: return 31;
					break;																					
	}
}