// JavaScript Document - Para el componente selector de hora
var SelectorHora = function(){
	this.contenedor_selector_hora = null;
	this.hora_actual = new Date;
	this.minutos = 0;
	this.hora = 0;
	this.reloj_activo =  true;
	this.nombre_instancia = "";
	
	this.setSelectorHora = function(contenedor, opcion, hora, nombre_instancia){
		this.contenedor_selector_hora = contenedor;
		this.nombre_instancia = nombre_instancia;
		if(opcion == -2){
			this.contenedor_selector_hora.innerHTML = this.getSelectorHora();
			this.setHora(hora);
		}
		else if(opcion == -1){
			this.hora_actual = new Date();
			this.contenedor_selector_hora.innerHTML = this.getSelectorHora();	
			this.reloj_activo = true;
			this.relojActivo();
		}
	}
    
    this.setComponente = function(contenedor, nombre_instancia){
        this.contenedor_selector_hora = contenedor;
		this.nombre_instancia = nombre_instancia;
    }
    
    this.muestraComponente = function(){
        this.hora_actual = new Date();
        this.contenedor_selector_hora.innerHTML = this.getSelectorHora();
    }
	
	this.relojActivo = function(){
        try{
            if(this.reloj_activo){
                this.hora_actual = new Date();
                this.getHoraActualLegible();
                setTimeout(this.nombre_instancia+".relojActivo()", 1000);
            }   
        }catch(error){
        }
	}
	
	//esta función deshabilita los controles para dejar la hora fija
	this.deshabilitaControles = function(){
		/*document.getElementById(this.nombre_instancia+"_boton_mas_hora").onclick = "";
		document.getElementById(this.nombre_instancia+"_boton_mas_minuto").onclick = "";
		document.getElementById(this.nombre_instancia+"_hora").disabled = true;
		document.getElementById(this.nombre_instancia+"_boton_menos_minuto").onclick= "";
		document.getElementById(this.nombre_instancia+"_boton_menos_hora").onclick = "";
		document.getElementById(this.nombre_instancia+"_minuto").disabled = true;*/
	}
	
	this.deshabilitaControlesDeMinutos = function(){
		document.getElementById(this.nombre_instancia+"_boton_menos_minuto").onclick= "";
		document.getElementById(this.nombre_instancia+"_boton_mas_minuto").onclick = "";
		document.getElementById(this.nombre_instancia+"_minuto").disabled = true;
	}
	
	this.unaHoraMas = function(){
		document.getElementById(this.nombre_instancia+"_boton_mas_hora").click();
	}
	
	//esta función pone la hora actual del reloj según la hora especificada
	this.setHora = function(hora_nueva){
		if (!hora_nueva) return;
		this.reloj_activo = false;
		var hora_separada = hora_nueva.split(":");
		document.getElementById(this.nombre_instancia+'_hora').value = hora_separada[0];
		document.getElementById(this.nombre_instancia+'_minuto').value = hora_separada[1];
		this.hora = parseInt(hora_separada[0]);
		this.minutos = parseInt(hora_separada[1]);
	}
	
	this.getSelectorHora = function(){
		var contenido = "<table align='center' cellpadding='0' cellspacing='0' border='0'>"
		//contenido += "<tr><td><div id='"+this.nombre_instancia+"_boton_mas_hora' class='boton_mas' onclick='"+this.nombre_instancia+".unaHoraMas()'></div></td><td>&nbsp;&nbsp;</td><td><div class='boton_mas' id='"+this.nombre_instancia+"_boton_mas_minuto' onclick='"+this.nombre_instancia+".unMinutoMas()'></div></td></tr>";
		contenido += "<tr><td width='45%'><input type='text' id='"+this.nombre_instancia+"_hora' onclick='"+this.nombre_instancia+".editandoCampo()' onblur='"+this.nombre_instancia+".terminoEdicion()' value='00' class='campo_hora' onkeypress='return getComponenteValidacionTexto().esSoloNumero(event);'/></td><td align='center' width='10%'><div><b>:</b></div></td><td width='45%'><input type='text' onclick='"+this.nombre_instancia+".editandoCampo()' onblur='"+this.nombre_instancia+".terminoEdicion()' id='"+this.nombre_instancia+"_minuto' value='00' class='campo_hora' onkeypress='return getComponenteValidacionTexto().esSoloNumero(event);'/></td></tr>";
		//contenido += "<tr><td><div class='boton_menos' id='"+this.nombre_instancia+"_boton_menos_hora' onclick='"+this.nombre_instancia+".unaHoraMenos()'></div></td><td>&nbsp;&nbsp;</td><td><div class='boton_menos' id='"+this.nombre_instancia+"_boton_menos_minuto' onclick='"+this.nombre_instancia+".unMinutoMenos()'></div></td></tr></table>";
        contenido += "</table>";
		return contenido;
	}
	
	this.editandoCampo = function(){
		this.reloj_activo = false;
	}
	
	this.terminoEdicion = function(){
		this.setHora(this.getHora());
        
        var hora = document.getElementById(this.nombre_instancia+"_hora");
        var minuto = document.getElementById(this.nombre_instancia+"_minuto");
        
        //console.log("hora: "+hora.value+" minuto: "+minuto.value);
        
        if(hora.value > 23){
            alert("La hora debe estar entre 00 y 23!");
            hora.value = "00";
            //hora.focus();
            return;
        }
        
        if(minuto.value >= 59){
            alert("Los minutos deben estar entre 00 y 59!");
            minuto.value = "00";
            //minuto.focus();
            return;
        }
	}
	
	//esta función cambia el intervalo del boton más y menos de los minutos para incrementar en 30 minutos cada uno
	this.cambiaIntervaloDeTiempoParaReloj = function(){
		/*var boton_mas_minuto = document.getElementById(this.nombre_instancia+"_boton_mas_minuto");
		var boton_menos_minuto = document.getElementById(this.nombre_instancia+"_boton_menos_minuto");
		var selector = this;
		
		boton_mas_minuto.onclick = function(){
			selector.mediaHoraMas();
		}
		
		boton_menos_minuto.onclick = function(){
			selector.mediaHoraMenos();
		}*/
	}
	
	//funciones para aumentar media hora más o menos
	this.mediaHoraMas = function(){
		this.reloj_activo = false;
		if (this.minutos == 0) {
			this.minutos = 30;
			var mins = this.minutos
		}else{
			this.minutos = 0;
			var mins = "0";
			if(this.minutos < 10) mins = "0"+this.minutos;
			else mins = this.minutos;
			
			this.unaHoraMas();
		}
		
		document.getElementById(this.nombre_instancia+'_minuto').value = mins;
	}
	
	this.mediaHoraMenos = function(){
		this.reloj_activo = false;
		if (this.minutos == 0) {
			this.minutos = 30;
			var mins = this.minutos;
			this.unaHoraMenos();
		}else{
			this.minutos = 0;
			var mins = "0";
			if(this.minutos < 10) mins = "0"+this.minutos;
			else mins = this.minutos;
		}
		
		document.getElementById(this.nombre_instancia+'_minuto').value = mins;
	}
	
	this.unMinutoMas = function(){
		this.reloj_activo = false;
		if(this.minutos == 59){
			document.getElementById(this.nombre_instancia+'_minuto').value = "00";
			this.minutos = 0;
			this.unaHoraMas();
		}else{
			this.minutos += 1;
			var mins = "0";
			if(this.minutos < 10) mins = "0"+this.minutos;
			else mins = this.minutos;
			document.getElementById(this.nombre_instancia+'_minuto').value = mins;
		}
	}
	
	this.unMinutoMenos = function(){
		this.reloj_activo = false;
		if(this.minutos == 0){
			document.getElementById(this.nombre_instancia+'_minuto').value = "59";
			this.unaHoraMenos();
			this.minutos = 59;
		}else{
			this.minutos -= 1;
			var mins = "0";
			if(this.minutos < 10) mins = "0"+this.minutos;
			else mins = this.minutos;
			document.getElementById(this.nombre_instancia+'_minuto').value = mins;
		}
	}
	
	this.unaHoraMas = function(){
		this.reloj_activo = false;
		if(this.hora >= 23){
			document.getElementById(this.nombre_instancia+'_hora').value = "00";
			this.hora = 0;
		}else{
			this.hora += 1;
			var hr = "0";
			if(this.hora <= 9) hr = "0"+this.hora;
			else hr = this.hora;
			document.getElementById(this.nombre_instancia+'_hora').value = hr;
		}
	}

	this.unaHoraMenos = function(){
		this.reloj_activo = false;
		if(this.hora == 1){
			document.getElementById(this.nombre_instancia+'_hora').value = "00";
			this.hora = 24;
		}else{
			this.hora -= 1;
			var hr = "0";
			if(this.hora <= 9) hr = "0"+this.hora;
			else hr = this.hora;
			document.getElementById(this.nombre_instancia+'_hora').value = hr;
		}
	}
	
	this.getHoraActualLegible = function(){
		var mins = "0";
		var hr = "0";
		this.minutos = parseInt(this.hora_actual.getMinutes());
		if(this.minutos <= 9) mins = "0"+this.minutos;
		else mins = this.minutos;
		this.hora = parseInt(this.hora_actual.getHours());
		if(this.hora <= 9) hr = "0"+this.hora;
		else hr = this.hora;
		if(document.getElementById(this.nombre_instancia+'_hora')) document.getElementById(this.nombre_instancia+'_hora').value = hr;
		else this.reloj_activo = false;
		if(document.getElementById(this.nombre_instancia+'_minuto')) document.getElementById(this.nombre_instancia+'_minuto').value = mins;
		else return false;
		return hr+":"+mins;
	}
	
	this.getHora = function(){
		var hr = document.getElementById(this.nombre_instancia+'_hora').value;
		var mins = document.getElementById(this.nombre_instancia+'_minuto').value;
		return hr+":"+mins;
	}
}