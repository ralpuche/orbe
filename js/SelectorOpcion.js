var SelectorOpcion = function(){
    
    this.funcion_agregar = null;
    this.setFuncionAgregar = function(funcion){
        this.funcion_agregar = funcion;
    }
    
    this.texto_opcion_hover = "";
    this.setTextoOpcionHover = function(texto){
        this.texto_opcion_hover = texto;
    }
    
    this.texto_opcion_seleccionada = "";
    this.setTextoOpcionSeleccionada = function(texto){
        this.texto_opcion_seleccionada = texto;
    }
    
    this.getNombreOpcionSeleccionada = function(){
        return this.texto_opcion_seleccionada;
    }
    
    this.getBotonAgregar = function(){
        return document.getElementById(this.nombre_instancia+"_boton_agregar");
    }
    
    this.muestraComponente = function(){
        var contenido = "<table width='98%'><tr><td width='5%'>";
        contenido += "<div onclick='"+this.funcion_agregar+"' class='div_pointer' id='"+this.nombre_instancia+"_boton_agregar'><a class='btn-floating fondo_principal scale-transition scale-in' onmouseover='"+this.nombre_instancia+".botonOver();' onmouseout='"+this.nombre_instancia+".botonOut();'><i class='material-icons'>add</i></a>";
        contenido += "</td><td width='95%'>";
        contenido += "<div id='"+this.nombre_instancia+"_zona_opcion'><div class='truncate'>"+this.texto_opcion_seleccionada+"</div></div>";
        contenido += "</td></tr></table>";
        
        this.contenedor.innerHTML = contenido;
    }
    
    this.botonOver = function(){
        if(document.getElementById(this.nombre_instancia+"_zona_opcion")) document.getElementById(this.nombre_instancia+"_zona_opcion").innerHTML = "<div class='ayuda_campos_visible' id='"+this.nombre_instancia+"_div_selector'>"+this.texto_opcion_hover+"</div>";
        //muestraAyudaCampo(this.nombre_instancia+"_div_selector", this.texto_opcion_hover);
    }
    
    this.botonOut = function(){
        if(document.getElementById(this.nombre_instancia+"_zona_opcion")) document.getElementById(this.nombre_instancia+"_zona_opcion").innerHTML = "<div class='truncate'>"+this.texto_opcion_seleccionada+"</div>";
    }
}