var SelectorRangoDeFecha = function(){
    
    this.fecha_inicial = null;
    this.getFechaInicial = function(){
        if(!this.fecha_inicial) this.fecha_inicial = new SelectorFechas();
        return this.fecha_inicial;
    }
    
    this.fecha_final = null;
    this.getFechaFinal = function(){
        if(!this.fecha_final) this.fecha_final = new SelectorFechas();
        return this.fecha_final;
    }
    
    this.hora_inicial = null;
    this.getHoraInicial = function(){
        if(!this.hora_inicial) this.hora_inicial = new SelectorHora();
        return this.hora_inicial;
    }
    
    this.hora_final = null;
    this.getHoraFinal = function(){
        if(!this.hora_final) this.hora_final = new SelectorHora();
        return this.hora_final;
    }
    
    this.funcion_cambia_rango = null;
    this.setFuncionCambiaRango = function(funcion){
        this.funcion_cambia_rango = funcion;
    }
    
    this.contenedor = null;
    this.nombre_instancia = "selector_rango_fechas";
    
    this.setComponente = function(contenedor, nombre){
        this.contenedor = contenedor;
        this.nombre_instancia = nombre;
    }
    
    //tipos de visualizacion
    //1 del primero al día actual del mes
    //2 el dia actual
    //3 fechas inicial y final considerando la quincena actual
    this.tipo_visualizacion = 1;
    this.setTipoVisualizacion = function(tipo_visualizacion){
        this.tipo_visualizacion = tipo_visualizacion;
    }
    
    //tipo de rango de fecha
    //1 rango solo de fechas
    //2 rango de fechas y horas
    this.tipo_selector = 1;
    this.setTipoDeSelector = function(tipo){
        this.tipo_selector = tipo;
    }
    
    this.getFechaInicialPorTipoDeVisualizacion = function(){
        if(this.tipo_visualizacion == 3){
            var fecha_actual = new Date();
            if(fecha_actual.getDate() > 15) return fecha_actual.getFullYear()+"-"+((fecha_actual.getMonth()+1 < 10)?"0"+(fecha_actual.getMonth()+1):(fecha_actual.getMonth()+1))+"-16";
            else return fecha_actual.getFullYear()+"-"+((fecha_actual.getMonth()+1 < 10)?"0"+(fecha_actual.getMonth()+1):(fecha_actual.getMonth()+1))+"-01";
        }
    }
    
    this.getFechaFinalPorTipoDeVisualizacion = function(){
        if(this.tipo_visualizacion == 3){
            var fecha_actual = new Date();
            if(fecha_actual.getDate() > 15) return fecha_actual.getFullYear()+"-"+((fecha_actual.getMonth()+1 < 10)?"0"+(fecha_actual.getMonth()+1):(fecha_actual.getMonth()+1))+"-"+getMaximosDiasPorMes((fecha_actual.getMonth()+1), fecha_actual.getFullYear());
            else return fecha_actual.getFullYear()+"-"+((fecha_actual.getMonth()+1 < 10)?"0"+(fecha_actual.getMonth()+1):(fecha_actual.getMonth()+1))+"-15";
        }
    }
    
    this.muestraComponente = function(){
        if(this.tipo_selector == 1){
            var contenido = "<table align='center' width='96%' cellpadding='6' cellspacing='0'>";
            contenido += "<tr>";
            contenido += "<td align='center' width='50%' valign='top'>";

            contenido += "<div class='texto_campo'>Fecha Inicial <span class='azul'>*</span></div>";

            contenido += "</td>";
            contenido += "<td align='center' width='50%' valign='top'>";

            contenido += "<div class='texto_campo'>Fecha Final <span class='azul'>*</span></div>";

            contenido += "</td>";
            contenido += "</tr>";

            contenido += "<tr>";
            contenido += "<td align='center' width='50%' valign='top'>";

            contenido += "<div id='"+this.nombre_instancia+"_zona_fecha_inicial'></div>";

            contenido += "</td>";
            contenido += "<td align='center' width='50%' valign='top'>";

            contenido += "<div id='"+this.nombre_instancia+"_zona_fecha_final'></div>";

            contenido += "</td>";
            contenido += "</tr>";
            contenido += "</table>";

            this.contenedor.innerHTML = contenido;

            var fecha_actual = new Date();
             this.getFechaInicial().setComponente(document.getElementById(this.nombre_instancia+"_zona_fecha_inicial"), this.nombre_instancia+".getFechaInicial()");
            this.getFechaInicial().funcionOnChange(this.nombre_instancia+".cambiaRangoDeFecha()");

            if(this.tipo_visualizacion == 1) this.getFechaInicial().setFechaDesdeDB(fecha_actual.getFullYear()+"-"+((fecha_actual.getMonth()+1 < 10)?"0"+(fecha_actual.getMonth()+1):(fecha_actual.getMonth()+1))+"-01");
            else if(this.tipo_visualizacion == 2) this.getFechaInicial().setFechaDesdeDB(fecha_actual.getFullYear()+"-"+((fecha_actual.getMonth()+1 < 10)?"0"+(fecha_actual.getMonth()+1):(fecha_actual.getMonth()+1))+"-"+fecha_actual.getDate());

            this.getFechaFinal().setComponente(document.getElementById(this.nombre_instancia+"_zona_fecha_final"), this.nombre_instancia+".getFechaFinal()");
            this.getFechaFinal().funcionOnChange(this.nombre_instancia+".cambiaRangoDeFecha()");
            this.getFechaFinal().setFechaDesdeDB(fecha_actual.getFullYear()+"-"+((fecha_actual.getMonth()+1 < 10)?"0"+(fecha_actual.getMonth()+1):(fecha_actual.getMonth()+1))+"-"+fecha_actual.getDate());
            
            if(this.tipo_visualizacion == 3){
                var fecha_actual = new Date();
                this.getFechaInicial().setComponente(document.getElementById(this.nombre_instancia+"_zona_fecha_inicial"), this.nombre_instancia+".getFechaInicial()");
                this.getFechaInicial().funcionOnChange(this.nombre_instancia+".cambiaRangoDeFecha()");
                this.getFechaInicial().setFechaDesdeDB(this.getFechaInicialPorTipoDeVisualizacion());

                this.getFechaFinal().setComponente(document.getElementById(this.nombre_instancia+"_zona_fecha_final"), this.nombre_instancia+".getFechaFinal()");
                this.getFechaFinal().funcionOnChange(this.nombre_instancia+".cambiaRangoDeFecha()");
                console.log("fecha_final: "+this.getFechaFinalPorTipoDeVisualizacion());
                this.getFechaFinal().setFechaDesdeDB(this.getFechaFinalPorTipoDeVisualizacion());
            }
        }else{
            var contenido = "<table align='center' width='96%' cellpadding='6' cellspacing='0'>";
            contenido += "<tr>";
            contenido += "<td align='center' width='24%' valign='top'>";

            contenido += "<div class='texto_campo'>Fecha Inicial <span class='azul'>*</span></div>";
            contenido += "<div id='"+this.nombre_instancia+"_zona_fecha_inicial'></div>";

            contenido += "</td>";
            contenido += "<td align='center' width='24%' valign='top'>";

            contenido += "<div class='texto_campo'>Hora Inicial <span class='azul'>*</span></div>";
            contenido += "<div id='"+this.nombre_instancia+"_zona_hora_inicial'></div>";

            contenido += "</td>";
            
            //detalle vertical
            contenido += "<td align='center' width='4%' valign='top'>";
            contenido += "<div class='detalle_vertical'></div>";
            contenido += "</td>";
            
            contenido += "<td align='center' width='24%' valign='top'>";

            contenido += "<div class='texto_campo'>Fecha Final <span class='azul'>*</span></div>";
            contenido += "<div id='"+this.nombre_instancia+"_zona_fecha_final'></div>";

            contenido += "</td>";
            contenido += "<td align='center' width='24%' valign='top'>";

            contenido += "<div class='texto_campo'>Hora Final <span class='azul'>*</span></div>";
            contenido += "<div id='"+this.nombre_instancia+"_zona_hora_final'></div>";

            contenido += "</td>";
            contenido += "</tr>";
            
            contenido += "</table>";

            this.contenedor.innerHTML = contenido;

            var fecha_actual = new Date();
            
            this.getFechaInicial().setComponente(document.getElementById(this.nombre_instancia+"_zona_fecha_inicial"), this.nombre_instancia+".getFechaInicial()");
            this.getFechaInicial().funcionOnChange(this.nombre_instancia+".cambiaRangoDeFecha()");
            this.getFechaInicial().setFechaDesdeDB(this.getFechaInicialPorTipoDeVisualizacion());

            this.getFechaFinal().setComponente(document.getElementById(this.nombre_instancia+"_zona_fecha_final"), this.nombre_instancia+".getFechaFinal()");
            this.getFechaFinal().funcionOnChange(this.nombre_instancia+".cambiaRangoDeFecha()");
            console.log("fecha_final: "+this.getFechaFinalPorTipoDeVisualizacion());
            this.getFechaFinal().setFechaDesdeDB(this.getFechaFinalPorTipoDeVisualizacion());
            
            this.getHoraInicial().setComponente(document.getElementById(this.nombre_instancia+"_zona_hora_inicial"), this.nombre_instancia+".getHoraInicial()");
            this.getHoraInicial().muestraComponente();
            this.getHoraInicial().setHora("12:01:00");
            
            this.getHoraFinal().setComponente(document.getElementById(this.nombre_instancia+"_zona_hora_final"), this.nombre_instancia+".getHoraFinal()");
            this.getHoraFinal().muestraComponente();
            this.getHoraFinal().setHora("12:00:00");
        }
    }
    
    this.setFechaInicial = function(fecha){
        this.getFechaInicial().setFechaDesdeDB(fecha);
    }
    
    this.setFechaFinal = function(fecha){
        this.getFechaFinal().setFechaDesdeDB(fecha);
    }
    
    this.cambiaRangoDeFecha = function(){
        //console.log("cambio rango de fecha");
        if(this.funcion_cambia_rango) setTimeout(this.funcion_cambia_rango, 0);
    }
    
    this.getFechaInicio = function(){
        return this.getFechaInicial().getFechaActualParaDB();
    }
    
    this.getHoraInicio = function(){
        return this.hora_inicial.getHoraActualLegible();
    }
    
    this.getFechaFin = function(){
        return this.getFechaFinal().getFechaActualParaDB();
    }
    
    this.getHoraFin = function(){
        return this.hora_final.getHoraActualLegible();
    }
}