<?php
header("Cache-Control: no-store, no-cache, must-revalidate");
header('Content-type: text/html; charset=utf-8');
class ConectorEdicom{
    
    private $usuario_prueba;
    private $password_prueba;
    private $usuario_produccion;
    private $password_produccion;
    private $url_web_service;
    private $ambiente;
    private $certificado;
    private $rfc_emisor;
    private $password_certificado;
    private $conector;
    
    //nombre del archivo pem
    private $archivoPem;
    
    private $idEmpresa;
    
    private $idFacturaEmitida = 0;
    
    public function getIdFacturaEmitida(){
        return $this->idFacturaEmitida;
    }
    
    public function setIdEmpresa($idEmpresa){
        $this->idEmpresa = $idEmpresa;
    }
    
    //variables específicas de cada xml
    private $folio;
    public function getFolio(){
        return $this->folio;    
    }
    
    private $montoFacturado;
    public function getMontoFacturado(){
        return $this->montoFacturado;
    }
    
    private $uuid;
    public function getUUID(){
        return $this->uuid;
    }
    
    private $pathCarpeta;
    public function getPathCarpeta(){
        return $this->pathCarpeta;
    }
    
    public function setPathCarpeta($path){
        $this->pathCarpeta = $path;
    }
    
    public function ConectorEdicom(){
        //inicialización con los valores proporcionados por FactureYa
        $this->usuario_prueba = "LOVH8108114K3";
        $this->password_prueba = 'wvcdftgel';
        
        $this->usuario_produccion = "LOVH8108114K3";
        $this->password_produccion = 'wvcdftgel';
        
        $this->url_web_service = "https://cfdiws.sedeb2b.com/EdiwinWS/services/CFDi?wsdl";
        
        //por defecto el ambiente es de prueba
        $this->ambiente = 1;
        $this->certificado = null;
        $this->password_certificado = "";
        
        $this->archivoPem = "key.pem";
        
        $this->conector = null;
        
        //contraseña hm 12345678a
    }
    
    public function getConector(){
        if($this->conector == null){
            $this->conector = new ConectorOrbe();
            $this->conector->estableceConexion();
        }
        return $this->conector;
    }
    
    public function getIdVisita(){
        return $this->idvisita;
    }
    
    public function getTipoPago(){
        return $this->tipoPago;
    }
    
    public function getNombreDelUltimoPaciente(){
        return $this->paciente;
    }
    
    public function getTotalFacturado(){
        return $this->totalFacturado;
    }
    
    private function getCertificadoEnBase64(){
        return "MIIEYjCCA0qgAwIBAgIUMDAwMDEwMDAwMDAzMDY5OTczMTMwDQYJKoZIhvcNAQEFBQAwggGKMTgwNgYDVQQDDC9BLkMuIGRlbCBTZXJ2aWNpbyBkZSBBZG1pbmlzdHJhY2nDs24gVHJpYnV0YXJpYTEvMC0GA1UECgwmU2VydmljaW8gZGUgQWRtaW5pc3RyYWNpw7NuIFRyaWJ1dGFyaWExODA2BgNVBAsML0FkbWluaXN0cmFjacOzbiBkZSBTZWd1cmlkYWQgZGUgbGEgSW5mb3JtYWNpw7NuMR8wHQYJKoZIhvcNAQkBFhBhY29kc0BzYXQuZ29iLm14MSYwJAYDVQQJDB1Bdi4gSGlkYWxnbyA3NywgQ29sLiBHdWVycmVybzEOMAwGA1UEEQwFMDYzMDAxCzAJBgNVBAYTAk1YMRkwFwYDVQQIDBBEaXN0cml0byBGZWRlcmFsMRQwEgYDVQQHDAtDdWF1aHTDqW1vYzEVMBMGA1UELRMMU0FUOTcwNzAxTk4zMTUwMwYJKoZIhvcNAQkCDCZSZXNwb25zYWJsZTogQ2xhdWRpYSBDb3ZhcnJ1YmlhcyBPY2hvYTAeFw0xNTA1MDYxNzA2NDhaFw0xOTA1MDYxNzA2NDhaMIGuMSAwHgYDVQQDExdPQ1RBVklPIEpPU0UgTUFZIE1FUklOTzEgMB4GA1UEKRMXT0NUQVZJTyBKT1NFIE1BWSBNRVJJTk8xIDAeBgNVBAoTF09DVEFWSU8gSk9TRSBNQVkgTUVSSU5PMRYwFAYDVQQtEw1NQU1YNjUxMTEzTVAyMRswGQYDVQQFExJNWE1PNjUxMTEzSFZaWVJDMDExETAPBgNVBAsTCEhvc3BpdGFsMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCFx0Sgj1Hovhy3wTzu+5hX1SI2rm6nAvmn0YSvPZL4LOrPyWlvWuZ55AvapFwD8lYo3oFsZnVFVfKRFUgggxuQ0iKGD+f5fk+5n/CBQPSbGq26736oUiO+BeEqhhGOt3i3h6HujCBH6Baj9SRVnLgfVkgL6FkAlSVBT5v3xlPQlQIDAQABox0wGzAMBgNVHRMBAf8EAjAAMAsGA1UdDwQEAwIGwDANBgkqhkiG9w0BAQUFAAOCAQEAFjlhtpC4fyiXvUqw7IAHreyJRyTQzXtKkTrTOaNdGoIkGQWCOCXjM7G00HLu6buWx1P17cW8ZDnoRHdNglJQWcowCjBIBeamv1b7k6Mkum1dplX0bjSHeW0YbW9khzDFwM0LJZ+SJRBJXRsxWVE1nezFmbMOQszORsXUKn1mlFOhc3ekpAo4SGbA2cGehNbXk5/iiMlBgR75wMxP1OAMTsj8B7c85xgu6j0QG2Uc5kDmuzKVN3IDHB1NjxH59H99g4wmCu/ClvGJHyzOi3eiA1mJ9PUF9NkGGZWOsMB9WWd4zRkp3+y6VQJLUOBTy2O6U5GibPTHrifwa6h25NKEIA==";
    }
    
    private function getNumeroDeCertificado(){
        return "00001000000306997313";
    }
    
    public function setAmbiente($valor){
        $this->ambiente = $valor;
    }
    
    public function timbraFactura($cadena_xml, $idelemento, $tipoElemento){
        $context = stream_context_create(array(
            'ssl' => array(
                // set some SSL/TLS specific options
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true  //--> solamente true en ambiente de pruebas
            ),
            'http' => array(
                'user_agent' => 'PHPSoapClient'
            )
        ));
        
        $servicio = $this->url_web_service;
        
        $parametros = array(); //parametros de la llamada
        $parametros['user'] = 'LOVH8108114K3';//$this->usuario_prueba;
        $parametros['password'] = 'wvcdftgel';//$this->password_prueba;
        $parametros['file'] = $cadena_xml;
        
        $options =array();
        $options['stream_context'] = $context;
        $options['cache_wsdl']= WSDL_CACHE_MEMORY;
        $options['trace']= true;

        libxml_disable_entity_loader(false);
        
        try{
            $client = new SoapClient($servicio, $options);
            
            $metodo = "getCfdiTest";
            if($this->ambiente == 2) $metodo = "getCfdi";
            
            $response = $client->__soapCall($metodo, array('parameters' => $parametros));
            
            $nombreArchivo = "vp_$idelemento.zip";
            $pathCarpeta = "cdfis/".$this->idEmpresa."/";
            
            if($this->ambiente == 1) file_put_contents($pathCarpeta.$nombreArchivo, $response->getCfdiTestReturn);
            else file_put_contents($pathCarpeta.$nombreArchivo, $response->getCfdiReturn);
            
            $zip = new ZipArchive;
            if ($zip->open($pathCarpeta.$nombreArchivo) === TRUE) {
                $zip->extractTo($pathCarpeta);
                $zip->close();
                
                $xml = $this->leeArchivo($pathCarpeta."SIGN_XML_COMPROBANTE_3_0.xml");
                
                if($this->ambiente == 2){
                    $this->pathCarpeta = $pathCarpeta;
                    $file = fopen($pathCarpeta.(($tipoElemento == 1)?'I':'P').$idelemento.".xml", "w+");
                    fwrite($file, $xml);
                    fclose($file);
                    
                    //se actualizan los datos de la factura
                    $infoXML = $this->getInfoDeXMLTimbrado($xml);
                    $timbre = $infoXML['TimbreFiscalDigital'];
                    $this->uuid = $timbre['UUID'];

                    $conector = $this->getConector();//new ConectorOrbe();

                    //en este punto, debemos recuperar el idfactura correspondiente a este elemento y a este tipo para esta empresa
                    $idFactura = $conector->getIdFactura($this->idEmpresa, $tipoElemento, $idelemento);
                    $conector->guardaFactura($idFactura, $tipoElemento, $idelemento, $this->getUUID(), $this->getMontoFacturado(), $this->idEmpresa);
                    
                    //enviamos la factura
                    //$conector->enviaFacturaPorCorreo($idFactura);
                    $this->idFacturaEmitida = $idFactura;
                }
                
                return $xml;
            } else {
                echo 'Unzip failed';
                return "";
            }
        }catch (SoapFault $fault){
            echo "SOAPFault: ".$fault->faultcode."-".$fault->faultstring."\n";
            return "";
        }
    }
    
    //esta función recupera un elemento timbre a partir del uuid
    public function getComplementoTimbre($uuid){
        $param = array('Usuario' => $this->usuario_produccion, 'Password' => $this->password_produccion, 'UUID' => $uuid, 'RFCEmisor' => $this->rfc_emisor);
        $metodo = "ObtenerAcuseEnvio";
        
        $error = 0; 
        try { 
            $client = new SoapClient($this->url_web_service);
            $result= $client->__call($metodo, array($param)); 
        } catch (SoapFault $fault) { 
            $error = 1; 
            return "Error: ".$fault->faultcode." - ".$fault->faultstring."<br/>";
        }

        //Validamos la respuesta
        if ($client->fault) {
            return 'Error: '.$result;
        } else {	// Chequea errores
            //comprobamos que las 3 primeras posiciones venga sin nada, lo cual indica que todo salió bien
            $posicion1 = $result->ObtenerAcuseEnvioResult->string[0];
            $posicion2 = $result->ObtenerAcuseEnvioResult->string[1];
            $posicion3 = $result->ObtenerAcuseEnvioResult->string[2];
            $posicion4 = $result->ObtenerAcuseEnvioResult->string[3];
            $posicion5 = $result->ObtenerAcuseEnvioResult->string[4];
            
            if($posicion1 == "True"){
                //si existe y recuperó el acuse de envío
                return $posicion4;
            }else if($posicion1 == "False"){
                return "";//no existe el acuse de envío
            }
            
            if($posicion1 != "" || $posicion2 != "" || $posicion3 != "") return "Error en el resultado de timbrado: <br/>posicion1: ".$posicion1."<br/>posicion2: ".$posicion2."<br/>posicion3: ".$posicion3."<br/>posicion4: ".$posicion4."<br/>posicion5: ".$posicion5."<br/>";
            
            return $posicion4;
        }
    }
    
    //esta función devuelve el UUID asignado a una referencia en caso de que ya haya sido facturada
    public function consultarTimbrePorReferencia($referencia){
        $param = array('Usuario' => $this->usuario_produccion, 'Password' => $this->password_produccion, 'RFCEmisor' => $this->rfc_emisor, 'Referencia' => $referencia);
        $metodo = "ConsultarTimbrePorReferencia";
        
        $error = 0; 
        try { 
            $client = new SoapClient($this->url_web_service);
            $result= $client->__call($metodo, array($param)); 
        } catch (SoapFault $fault) { 
            $error = 1; 
            return "Error: ".$fault->faultcode." - ".$fault->faultstring."<br/>";
        }

        //Validamos la respuesta
        if ($client->fault) {
            return 'Error: '.$result;
        } else {	// Chequea errores
            //comprobamos que las 3 primeras posiciones venga sin nada, lo cual indica que todo salió bien
            $posicion1 = $result->ConsultarTimbrePorReferenciaResult->string[0];
            $posicion2 = $result->ConsultarTimbrePorReferenciaResult->string[1];
            $posicion3 = $result->ConsultarTimbrePorReferenciaResult->string[2];
            $posicion4 = $result->ConsultarTimbrePorReferenciaResult->string[3];
            $posicion5 = $result->ConsultarTimbrePorReferenciaResult->string[4];
            
            //return "Error en el resultado de timbrado: <br/>posicion1: ".$posicion1."<br/>posicion2: ".$posicion2."<br/>posicion3: ".$posicion3."<br/>posicion4: ".$posicion4."<br/>posicion5: ".$posicion5."<br/>";
            
            if($posicion1 == "True"){
                return $posicion5;
            }else if($posicion1 == "False"){
                return "";//devuelve vacío por que no existe
            }
            
            if($posicion1 != "" || $posicion2 != "" || $posicion3 != "") return "Error en el resultado de timbrado: <br/>posicion1: ".$posicion1."<br/>posicion2: ".$posicion2."<br/>posicion3: ".$posicion3."<br/>posicion4: ".$posicion4."<br/>posicion5: ".$posicion5."<br/>";
            
            return $posicion5;
        }
    }
    
    public function cancelarCDFI($idFactura){
        $conector = $this->getConector();
        $datos = explode("|", $conector->getInfoFacturaPorId($idFactura));
        //idFacturaEmitida, tipoFactura, idElemento, UUID, cantidadFacturada, fecha, hora, usuario, estatus, nombreUsuario
        
        $datosElemento = null;
        if($datos[1] == 1) $datosElemento = explode("|", $conector->getGastoDeClientePorId($datos[2]));
        else if($datos[1] == 2) $datosElemento = explode("|", $conector->getGastoDeClientePorId($datos[2]));
        
        //gasto : idGastoCliente, idPromotor, idEmpresa, idCliente, idBanco, fecha, hora, cantidadTotal, porcentaje, idUsuario, estatus, promotor, empresa, cliente, banco, metodoDePago, idFormaDePago, moneda, tipoCambio, formaPago
        
        $empresa = explode("|", $conector->getEmpresaPorId($datosElemento[2]));
        //idEmpresa, nombre, estatus, idDatosFacturacion, noCertificado, fechaVencimiento, textoCertificado, passwordPFX
        
        $datosFacturacionEmpresa = explode("|", $conector->getDatosDeFacturacion($empresa[3]));
        //iddatosfacturacion, nombre, rfc, calle, exterior, interior, referencia, colonia, localidad, municipio, estado, pais, cp, telefono, email, fax, cuenta, estatus, claveRegimen, descripcionClave
        if($empresa[7] == "" || $empresa[7] == " "){
            return -1;
            die("La contraseña del archivo .pfx no puede ser vacía, no es posible continuar la cancelación!");
        }
        
        //leemos el archivo pfx para cancelación
        $pathArchivoCancelacion = "cdfis/".$empresa[0]."/archivoCancelacion.pfx";
        if(!file_exists($pathArchivoCancelacion)){
            return -2;
            die("No existe el archivo PFX de cancelación, no es posible continuar la cancelación!");
        }
        
        $pfx = "";
        $file = fopen($pathArchivoCancelacion, "r");
        while(!feof($file)) {
            $pfx .= fgets($file);
        }
        fclose($file);
        
        $context = stream_context_create(array(
            'ssl' => array(
                // set some SSL/TLS specific options
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true  //--> solamente true en ambiente de pruebas
            ),
            'http' => array(
                'user_agent' => 'PHPSoapClient'
            )
        ));
        
        $servicio = $this->url_web_service;
        
        $cdfis = array();
        $cdfis[] = $datos[3];
        
        $parametros = array(); //parametros de la llamada
        $parametros['user'] = 'LOVH8108114K3';//$this->usuario_prueba;
        $parametros['password'] = 'wvcdftgel';//$this->password_prueba;
        $parametros['rfc'] = $datosFacturacionEmpresa[2];//rfc de la empresa que emite al factura
        $parametros['uuid'] = $cdfis;//UUIDS que serán cancelados
        $parametros['pfx'] = $pfx;
        $parametros['pfxPassword'] = $empresa[7];
        
        $options =array();
        $options['stream_context'] = $context;
        $options['cache_wsdl']= WSDL_CACHE_MEMORY;
        $options['trace']= true;

        libxml_disable_entity_loader(false);
        
        try{
            $client = new SoapClient($servicio, $options);
            $metodo = "cancelaCFDi";
            $response = $client->__soapCall($metodo, array('parameters' => $parametros));
            
            $ack = $response->cancelaCFDiReturn->ack;
            $uuids = $response->cancelaCFDiReturn->uuids;
            
            $pathCarpeta = "cdfis/".$empresa[0]."/";
            
            //guardamos el ack
            file_put_contents($pathCarpeta.'ack_'.$idFactura.".xml", base64_decode($ack));
            
            
            if(count($uuids) == 1){
                //como si se pudo cancelar, entonces enviamos la factura cancelada y además, se 
                //$conector = $this->getConector();
                $conector->cambiaEstatusDeFacturaACancelada($idFactura);
                $conector->enviaFacturaCanceladaPorCorreo($idFactura);
                return 1;
            }
            return 0;
        }catch (SoapFault $fault){
            echo "SOAPFault: ".$fault->faultcode."-".$fault->faultstring."\n";
            return 0;
        }
    }
    
    public function consultarNumeroDeCreditos(){
        $param = array('Usuario' => $this->usuario_produccion, 'Password' => $this->password_produccion);
        $metodo = "ConsultarCreditos";
        
        $error = 0; 
        try { 
            $client = new SoapClient($this->url_web_service);
            $result= $client->__call($metodo, array($param)); 
        } catch (SoapFault $fault) { 
            $error = 1; 
            return "Error: ".$fault->faultcode." - ".$fault->faultstring."<br/>";
        }

        //Validamos la respuesta
        if ($client->fault) {
            return 'Error: '.$result;
        } else {	// Chequea errores
            //comprobamos que las 3 primeras posiciones venga sin nada, lo cual indica que todo salió bien
            $posicion1 = $result->ConsultarCreditosResult->string[0];
            $posicion2 = $result->ConsultarCreditosResult->string[1];
            $posicion3 = $result->ConsultarCreditosResult->string[2];
            $posicion4 = $result->ConsultarCreditosResult->string[3];
            $posicion5 = $result->ConsultarCreditosResult->string[4];
            
            //if($posicion1 != "" || $posicion2 != "" || $posicion3 != "") return "Error en el resultado de ConsultarCreditos: <br/>posicion1: ".$posicion1."<br/>posicion2: ".$posicion2."<br/>posicion3: ".$posicion3."<br/>posicion4: ".$posicion4."<br/>posicion5: ".$posicion5."<br/>";
            
            return $posicion4;
        }
    }
    
    public function obtenerXMLDeUUID($uuid){
        $param = array('Usuario' => $this->usuario_produccion, 'Password' => $this->password_produccion, 'UUID' => $uuid, 'RFCEmisor' => $this->rfc_emisor);
        $metodo = "ObtenerXML";
        
        $error = 0; 
        try { 
            $client = new SoapClient($this->url_web_service);
            $result= $client->__call($metodo, array($param)); 
        } catch (SoapFault $fault) { 
            $error = 1; 
            return "Error: ".$fault->faultcode." - ".$fault->faultstring."<br/>";
        }

        //Validamos la respuesta
        if ($client->fault) {
            return 'Error: '.$result;
        } else {	// Chequea errores
            $posicion1 = $result->ObtenerXMLResult->string[0];
            $posicion2 = $result->ObtenerXMLResult->string[1];
            $posicion3 = $result->ObtenerXMLResult->string[2];
            $posicion4 = $result->ObtenerXMLResult->string[3];
            $posicion5 = $result->ObtenerXMLResult->string[4];
            
            if($posicion1 == "True"){
                return $posicion4;
            }else if($posicion1 == "False"){
                return "";//devuelve vacío por que no existe
            }
            
            echo "Error en el resultado de timbrado: <br/>posicion1: ".$posicion1."<br/>posicion2: ".$posicion2."<br/>posicion3: ".$posicion3."<br/>posicion4: ".$posicion4."<br/>posicion5: ".$posicion5."<br/>"; 
        }
    }
    
    public function generaCadenaOriginal($pathCDFI, $archivoCadenaOriginal){
        // Crear un objeto DOMDocument para cargar el CFDI
        $xml = new DOMDocument("1.0","UTF-8"); 
        // Cargar el CFDI
        $xml->load($pathCDFI);

        // Crear un objeto DOMDocument para cargar el archivo de transformación XSLT
        $xsl = new DOMDocument();
        $xsl->load($archivoCadenaOriginal);

        // Crear el procesador XSLT que nos generará la cadena original con base en las reglas descritas en el XSLT
        $proc = new XSLTProcessor;
        // Cargar las reglas de transformación desde el archivo XSLT.
        $proc->importStyleSheet($xsl);
        // Generar la cadena original y asignarla a una variable
        $cadenaOriginal = $proc->transformToXML($xml);
        
        return $cadenaOriginal;
    }
    
    //esta función recibe como parámetro un cfdi y lo sella usando el sello en path
    public function getSelloParaCFDI($cfd, $idElemento, $idEmpresa){
        $file = fopen("cdfis/$idEmpresa/cdfi_$idElemento.xml", "w+");
        fwrite($file, $cfd);
        fclose($file);
        
        //path dinámico
        $path_carpeta = dirname(__FILE__)."/cdfis/$idEmpresa/";
        
        //iMac
        //$path_carpeta = "/Users/ralpuchev/Google\ Drive/Proyectos/Produccion/orbe/ms/cdfis/$idEmpresa/";
        
        //macbook cm
        //$path_carpeta = "/Applications/XAMPP/xamppfiles/htdocs/Sistemas/orbe/ms/cdfis/$idEmpresa/";
        
        //macbook Rafa
        //$path_carpeta = "/Users/ralpuchev/Google\ Drive/Proyectos/Produccion/orbeDos/orbe/ms/cdfis/$idEmpresa/";
        
        $cadena = @$this->generaCadenaOriginal("cdfis/$idEmpresa/cdfi_$idElemento.xml", "cdfis/$idEmpresa/cadenaoriginal_3_3.xslt");

        $file = fopen("cdfis/$idEmpresa/cadena_original_$idElemento.txt", "w+");
        fwrite($file, $cadena);
        fclose($file);
        
        shell_exec("openssl dgst -sha256 -sign ".$path_carpeta.$this->archivoPem." -out ".$path_carpeta."digest_".$idElemento.".txt  ".$path_carpeta."cadena_original_".$idElemento.".txt")."<br/>";
        
        shell_exec("openssl enc -in ".$path_carpeta."digest_".$idElemento.".txt -out ".$path_carpeta."sello_".$idElemento."_B64.txt -base64 -A -K ".$path_carpeta.$this->archivoPem)."<br/>";
        
        //se lee el contenido del sello
        $sello = "";
        $file = fopen("cdfis/$idEmpresa/sello_".$idElemento."_B64.txt", "r");
        while(!feof($file)) {
            $sello .= fgets($file);
        }
        fclose($file);
        return $sello;
    }
    
    public function leeArchivo($path){
        try{
            $file = fopen($path, "r");
            $contenido = "";
            while(!feof($file)) {
                $contenido .= fgets($file);
            }
            fclose($file);
            return $contenido;
        }catch(Exception $error){
            return null;
        }
    }
    
    public function getEstatusDeTimbre($uuid){
        $param = array('Usuario' => $this->usuario_produccion, 'Password' => $this->password_produccion, 'UUID' => $uuid);
        $metodo = "ConsultarEstadoComprobante";
        
        $error = 0; 
        try { 
            $client = new SoapClient($this->url_web_service);
            $result= $client->__call($metodo, array($param)); 
        } catch (SoapFault $fault) { 
            $error = 1; 
            return "Error: ".$fault->faultcode." - ".$fault->faultstring."<br/>";
        }

        //Validamos la respuesta
        if ($client->fault) {
            return 'Error: '.$result;
        } else {	// Chequea errores
            $posicion1 = $result->ConsultarEstadoComprobanteResult->string[0];
            $posicion2 = $result->ConsultarEstadoComprobanteResult->string[1];
            $posicion3 = $result->ConsultarEstadoComprobanteResult->string[2];
            $posicion4 = $result->ConsultarEstadoComprobanteResult->string[3];
            $posicion5 = $result->ConsultarEstadoComprobanteResult->string[4];
            
            if($posicion1 == "True") return $posicion4;
            else echo "Resultado: <br/>posicion1: ".$posicion1."<br/>posicion2: ".$posicion2."<br/>posicion3: ".$posicion3."<br/>posicion4: ".$posicion4."<br/>posicion5: ".$posicion5."<br/>"; 
        }
    }
    
    //esta función recupera los datos del xml timbrado y así poder generar el pdf correspondiente
    public function getInfoDeXMLTimbrado($raw_xml){
        $xml = new DOMDocument();
        $xml->loadXML($raw_xml);

        $elementos = array();
        $atributos = array();
        $conceptos = array();
        $pagos = array();

        foreach ($xml->getElementsByTagName('*') as $element) {
            //echo 'local name: ', $element->localName."<br/>";
            $atributos = array();
            foreach($element->attributes as $attribute_name => $attribute_node)
            {
                //echo "nombre: ".$attribute_name." valor: ".$attribute_node->nodeValue."<br/>";
                $atributos[$attribute_name] = $attribute_node->nodeValue;
            }
            if($element->localName == 'Concepto'){
                $conceptos[] = $atributos;
            }else if($element->localName == 'Pago'){
                $pagos[] = $atributos;
            }else $elementos[$element->localName] = $atributos;
        }
        
        $elementos['Conceptos'] = $conceptos;
        $elementos['Pagos'] = $pagos;
        
        return $elementos;
    }
    
    public function getInfoDeFolio($folio){
        $numeros = array();
        preg_match_all("/[0-9]+/", $folio, $numeros);
        $letras = array();
        preg_match("/[A-Z]+/", $folio, $letras);
        
        if(count($numeros[0]) == 2) return $numeros[0][0]."|".$letras[0]."|".$numeros[0][1];
        return "0|".$letras[0]."|".$numeros[0][0];
        //numeroConsecutivo, tipoFactura, idHoja
    }
    
    public function getImagenQRDeFolio($idElemento, $opcion, $ambiente){
        //Primero se comprueba que exista el archivo .png. Si no existe entonces se crea y se devuelve la ruta de la imagen
        $nombreArchivo = "cdfis/".$this->idEmpresa."/qrs/".$idElemento.".png";
        //echo "Nombre del archivo png: ".$nombreArchivo." idhoja: ".$idhoja." opcion: ".$opcion." ambiente: ".$ambiente."<br/>";
        if(!file_exists($nombreArchivo)){
            //ahora generamos el qr con la información de la cuenta para agregarlo al pdf
            //set it to writable location, a place for temp generated PNG files
            $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'cdfis'.DIRECTORY_SEPARATOR.$this->idEmpresa.DIRECTORY_SEPARATOR.'qrs'.DIRECTORY_SEPARATOR;
            
            //echo "directorio_temporal: ".$PNG_TEMP_DIR."<br/>";

            //html PNG location prefix
            $PNG_WEB_DIR = 'cdfis/'.$this->idEmpresa.'/qrs/';
            $matrixPointSize = 10;
            $errorCorrectionLevel = 'L';
            $filename = $PNG_TEMP_DIR.$idElemento.'.png';

            QRcode::png("http://facturas.orbe.solutions/Facturacion.php?idElemento=".$idElemento."&opcion=".$opcion."&ambiente=".$ambiente, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
        }
        return $nombreArchivo;
    }
    
    //esta función recupera el contenido del pdf para generar el archivo
    public function getContenidoDePDFPago($info_xml, $idElemento){
        
        $conector = new ConectorOrbe();
        if(!$conector->estableceConexion()) die("No se establecio conexion con la db");
        
        $conector->setConsulta("SET NAMES 'utf8';");
        $conector->ejecutaConsulta();
        
        $comprobante = $info_xml["Comprobante"];
        
        $timbre = $info_xml['TimbreFiscalDigital'];
        $emisor = $info_xml['Emisor'];
        $receptor = $info_xml['Receptor'];
        
        $folio = $comprobante['Folio'];
        
        $this->folio = $folio;
        $this->uuid = $timbre['UUID'];
        
        $datosEmisor = explode("|", $conector->getDatosDeFacturacion($conector->getIdDatosFacturacionDeRFC($emisor['Rfc'])));
        //iddatosfacturacion, nombre, rfc, calle, exterior, interior, referencia, colonia, localidad, municipio, estado, pais, cp, telefono, email, fax, cuenta, estatus, claveRegimen, descripcionClave
        
        $datosReceptor = explode("|", $conector->getDatosDeFacturacion($conector->getIdDatosFacturacionDeRFC($receptor['Rfc'])));
        //iddatosfacturacion, nombre, rfc, calle, exterior, interior, referencia, colonia, localidad, municipio, estado, pais, cp, telefono, email, fax, cuenta, estatus, claveRegimen, descripcionClave
        
        $contenido = '<html>';
        $contenido .= '<body>';

        $contenido .= '<table width="700" align="center" cellpadding="5" cellspacing="0" border="0">';
        $contenido .= '<tr align="center" style="font-size:9px; color:#223270"><td align="left" width="50%">';

        $contenido .= '<span style="font-weight:bold;">Factura</span> <span style="color:#ff0000">'.$comprobante['Serie'].$comprobante['Folio'].'</span><br/>';
        $contenido .= 'Cartificado Digital <span style="color:#ff0000">'.$comprobante['NoCertificado'].'</span><br/>';
        $contenido .= 'Folio Fiscal <span style="color:#ff0000">'.$timbre['UUID'].'</span><br/><br/>';

        $contenido .= '<span style="font-weight:bold;">'.$datosEmisor[1].'<br/>';
        $contenido .= "R.F.C. ".$emisor['Rfc']."<br/></span>";
        $contenido .= $datosEmisor[19]."<br/>";
        $contenido .= $datosEmisor[3]." ".$datosEmisor[4]." ";
        $contenido .= $datosEmisor[7]." C.P. ".$datosEmisor[12]." ".$datosEmisor[8]."<br/>";
        $contenido .= $datosEmisor[10]." ".$datosEmisor[11]."<br/>";
        if($datosEmisor[13] != "" && $datosEmisor[13] != " ") $contenido .= "Tel: ".$datosEmisor[13]."<br/>";
        if($datosEmisor[14] != "" && $datosEmisor[14] != " ") $contenido .= $datosEmisor[14]."<br/>";

        $contenido .= "</td><td align='right' width='50%'>";

        if($this->ambiente == 1) $contenido .= '<div style="font-size:20px; text-align:right;"><b><br/><br/><br/><br/>Vista Previa: Sin efecto fiscal</b></div>';

        $contenido .= "</td></tr>";
        $contenido .= "</table>";

        //datos del cliente
        $contenido .= '<table width="700" align="center" cellpadding="5" cellspacing="0" border="0">';
        $contenido .= '<tr align="center" style="width:700px; padding:12px; font-size:10px; color:#223270; font-weight:bold; background-color:#DFE5E7;"><td align="left" width="100%">';
        $contenido .= '<div>Cliente:</div>';
        $contenido .= "</td></tr>";
        $contenido .= "</table>";

        //fecha de emisión y rfc
        $contenido .= '<table width="700" align="center" cellpadding="3" cellspacing="0" border="0">';
        $contenido .= '<tr align="center" style="width:700px; padding:12px; font-size:9px; color:#223270;"><td align="left" width="15%">';

        $contenido .= '<div>Fecha de Emisión:</div>';

        $contenido .= '</td><td align="left" width="35%" style="background-color:#F2F2F2;">';

        $partes_fecha = explode("T", $comprobante['Fecha']);
        $contenido .= '<div>'.$conector->getFechaLegibleCorta($partes_fecha[0])." ".$partes_fecha[1].'</div>';

        $contenido .= '</td><td align="left" width="15%">';

        $contenido .= '<div>R.F.C.:</div>';

        $contenido .= '</td><td align="left" width="35%" style="background-color:#F2F2F2;">';

        $contenido .= '<div>'.$receptor['Rfc'].'</div>';

        $contenido .= '</td></tr>';
        $contenido .= '</table>';

        //razón social
        $contenido .= '<table width="700" align="center" cellpadding="3" cellspacing="0" border="0">';
        $contenido .= '<tr align="center" style="width:700px; padding:12px; font-size:9px; color:#223270;"><td align="left" width="15%">';

        $contenido .= '<div>Razón Social:</div>';

        $contenido .= '</td><td align="left" width="85%" style="background-color:#F2F2F2;">';

        $contenido .= '<div>'.$datosReceptor[1].'</div>';

        $contenido .= '</td></tr>';
        $contenido .= '</table>';

        //dirección
        $contenido .= '<table width="700" align="center" cellpadding="3" cellspacing="0" border="0">';
        $contenido .= '<tr align="center" style="width:700px; padding:12px; font-size:9px; color:#223270;"><td align="left" width="15%">';

        $contenido .= '<div>Calle:</div>';

        $contenido .= '</td><td align="left" width="35%" style="background-color:#F2F2F2;">';

        $contenido .= '<div>'.$datosReceptor[3].'</div>';

        $contenido .= '</td><td align="left" width="15%">';

        $contenido .= '<div>Número Exterior:</div>';

        $contenido .= '</td><td align="left" width="35%" style="background-color:#F2F2F2;">';

        $contenido .= '<div>'.$datosReceptor[4].'</div>';

        $contenido .= '</td></tr>';
        $contenido .= '<tr align="center" style="width:700px; padding:12px; font-size:9px; color:#223270;"><td align="left" width="15%">';

        $contenido .= '<div>Colonia:</div>';

        $contenido .= '</td><td align="left" width="35%" style="background-color:#F2F2F2;">';

        $contenido .= '<div>'.$datosReceptor[7].'</div>';

        $contenido .= '</td><td align="left" width="15%">';

        $contenido .= '<div>Referencia:</div>';

        $contenido .= '</td><td align="left" width="35%" style="background-color:#F2F2F2;">';

        $contenido .= '<div>'.$datosReceptor[6].'</div>';

        $contenido .= '</td></tr>';
        $contenido .= '<tr align="center" style="width:700px; padding:12px; font-size:9px; color:#223270;"><td align="left" width="15%">';

        $contenido .= '<div>Código Postal:</div>';

        $contenido .= '</td><td align="left" width="35%" style="background-color:#F2F2F2;">';

        $contenido .= '<div>'.$datosReceptor[12].'</div>';

        $contenido .= '</td><td align="left" width="15%">';

        $contenido .= '<div>Municipio:</div>';

        $contenido .= '</td><td align="left" width="35%" style="background-color:#F2F2F2;">';

        $contenido .= '<div>'.$datosReceptor[9].'</div>';

        $contenido .= '</td></tr>';
        $contenido .= '<tr align="center" style="width:700px; padding:12px; font-size:9px; color:#223270;"><td align="left" width="15%">';

        $contenido .= '<div>Estado:</div>';

        $contenido .= '</td><td align="left" width="35%" style="background-color:#F2F2F2;">';

        $contenido .= '<div>'.$datosReceptor[10].'</div>';

        $contenido .= '</td><td align="left" width="15%">';

        $contenido .= '<div>País:</div>';

        $contenido .= '</td><td align="left" width="35%" style="background-color:#F2F2F2;">';

        $contenido .= '<div>'.$datosReceptor[11].'</div>';

        $contenido .= '</td></tr>';
        
        $contenido .= '</table>';

        //datos de los conceptos
        $contenido .= '<table width="700" align="center" cellpadding="5" cellspacing="0" border="0">';
        $contenido .= '<tr align="center" style="width:700px; padding:12px; font-size:10px; color:#223270; font-weight:bold; background-color:#DFE5E7;"><td align="left" width="100%">';
        $contenido .= '<div>Conceptos:</div>';
        $contenido .= "</td></tr>";
        $contenido .= "</table>";
        $contenido .= '<table width="700" align="center" cellpadding="3" cellspacing="0" border="0" style="font-size:9px; color:#000000; background-color:#F2F2F2;">';
        $contenido .= '<tr><td width="15%" align="center"><span style="color:#223270">C. Producto</span></td><td width="15%" align="center"><span style="color:#223270">C. Unidad</span></td><td width="10%" align="center"><span style="color:#223270">Cantidad</span></td><td width="30%" align="left"><span style="color:#223270">Descripción</span></td><td width="15%" align="right"><span style="color:#223270">P. Unitario</span></td><td width="15%" align="right"><span style="color:#223270">Importe</span></td></tr>';
        
        //print_r($conceptos);
        foreach($info_xml['Conceptos'] as $concepto){
            
            $contenido .= '<tr><td width="15%" align="left">'.$conector->getDescripcionProductosAndServiciosSATPorId($concepto['ClaveProdServ']).'</td><td width="15%" align="center">'.$conector->getDescripcionUnidadSATPorId($concepto['ClaveUnidad']).'</td><td width="10%" align="center">'.$concepto['Cantidad'].'</td><td width="30%" align="left">'.$concepto['Descripcion'].'</td><td width="15%" align="right">'.$concepto['ValorUnitario'].'</td><td width="15%" align="right">'.$concepto['Importe'].'</td></tr>';
        }
        $contenido .= "</table>";
        
        //Pagos del xml
        $contenido .= '<table width="700" align="center" cellpadding="5" cellspacing="0" border="0">';
        $contenido .= '<tr align="center" style="width:700px; padding:12px; font-size:10px; color:#223270; font-weight:bold; background-color:#DFE5E7;"><td align="left" width="100%">';
        $contenido .= '<div>Pago Recibidos:</div>';
        $contenido .= "</td></tr>";
        $contenido .= "</table>";
        $contenido .= '<table width="700" align="center" cellpadding="3" cellspacing="0" border="0" style="font-size:9px; color:#000000; background-color:#F2F2F2;">';
        $contenido .= '<tr><td width="20%" align="center"><span style="color:#223270">Fecha y Hora</span></td><td width="20%" align="center"><span style="color:#223270">Forma de Pago</span></td><td width="20%" align="center"><span style="color:#223270">Moneda</span></td><td width="20%" align="center"><span style="color:#223270">Tipo Cambio</span></td><td width="20%" align="center"><span style="color:#223270">Cantidad</span></td></tr>';
        
        $total = 0;
        foreach($info_xml['Pagos'] as $pago){
            
            $partesFecha = explode("T", $pago['FechaPago']);
            
            $contenido .= '<tr><td width="20%" align="center">'.$partesFecha[0]." ".$partesFecha[1].'</td><td width="20%" align="center">'.$conector->getDescripcionFormaPagoSATPorId($pago['FormaDePagoP']).'</td><td width="20%" align="center">'.$pago['MonedaP'].'</td><td width="20%" align="center">'.(($pago['MonedaP'] != "MXN")? $pago['TipoCambioP'] : "1").'</td><td width="20%" align="center">$'.money_format('%!i', $pago['Monto']).'</td></tr>';
            
            $esBancarizado = $conector->esBancarizadaLaFormaPagoSATPorId($pago['FormaDePagoP']);
            if($esBancarizado){
                $contenido .= '<tr><td width="20%" align="center"><span style="color:#223270">Banco Origen</span></td><td width="20%" align="center"><span style="color:#223270">Cuenta Origen</span></td><td width="20%" align="center"><span style="color:#223270">Banco Destino</span></td><td width="20%" align="center"><span style="color:#223270">Cuenta Destino</span></td><td width="20%" align="center"><span style="color:#223270">No. Operación</span></td></tr>';
                
                $contenido .= '<tr><td width="20%" align="left">'.$conector->getNombreDelBancoPorRFC($pago['RfcEmisorCtaOrd']).'</td><td width="20%" align="center">'.$pago['CtaOrdenante'].'</td><td width="20%" align="left">'.$conector->getNombreDelBancoPorRFC($pago['RfcEmisorCtaBen']).'</td><td width="20%" align="center">'.$pago['CtaBeneficiario'].'</td><td width="20%" align="center">'.$pago['NumOperacion'].'</td></tr>';
            }
            
            $contenido .= '<tr><td width="20%" align="center"><span style="color:#223270">Cantidad con letra</span></td><td width="80%" align="left">'.$conector->num2letras(money_format('%!i', $pago['Monto']), false, true).'</td></tr>';
            
            $total += $pago['Monto'];
        }
        
        $this->montoFacturado = $total;
        
        $contenido .= "</table>";
        //$pagos .= '<pago10:Pago FechaPago="'.$fechaPago.'" FormaDePagoP="'.$formaPagoPago.'" MonedaP="'.$moneda.'" '.$tipoCambio.' Monto="'.$monto.'" NumOperacion="'.$numeroOperacion.'" '.$rfcBancoEmisor.' '.$ctaBancoEmisor.' '.$rfcBancoReceptor.' '.$ctaBancoReceptor.'/>';
        
        $contenido .= '<table width="700" align="center" cellpadding="5" cellspacing="0" border="0">';
        $contenido .= '<tr align="center" style="width:700px; padding:12px; font-size:10px; color:#223270; font-weight:bold; background-color:#DFE5E7;"><td align="left" width="100%">';
        $contenido .= '<div>Información del Timbre Fiscal Digital:</div>';
        
        $contenido .= "</td></tr>";
        $contenido .= "<tr style='width:700px;'><td align='left' width='100%' style='width:700px;'>";
        
        //tabla qr y demás datos
        $contenido .= '<table width="700" align="center" cellpadding="3" cellspacing="0" border="0">';
        $contenido .= '<tr><td align="center" width="80%">';

        //certificación digital sat y fecha de certificacion
        $contenido .= '<table width="100%" align="left" cellpadding="3" cellspacing="0" border="0" style="font-size:10px; color:#223270;">';
        $contenido .= '<tr><td align="center" width="50%">';
        //echo $info_timbre->TimbreFiscalDigital;
        $contenido .= '<div><b>Certificado Digital SAT</b><br/>'.$timbre['NoCertificadoSAT'].'</div>';
        $contenido .= '</td><td align="center" width="50%">';
        $fecha_t_separada = explode("T", $timbre['FechaTimbrado']);
        $contenido .= '<div><b>Fecha de Certificación</b><br/>'.$conector->getFechaLegibleCorta($fecha_t_separada[0]).' '.$fecha_t_separada[1].'</div>';
        $contenido .= '</td></tr></table>';

        //datos de certificación
        $contenido .= '<table width="100%" align="left" cellpadding="3" cellspacing="0" border="0" style="font-size:6px; color:#223270;">';
        $contenido .= '<tr><td align="left" width="100%">';
        //se lee el contenido del sello
        
        //echo strlen($timbre['selloCFD'])."<br/>";
        $selloCFD = $timbre['SelloCFD'];
        $cadena_cfd = "";
        for($i = 1; $i<=strlen($selloCFD); $i++){
            $cadena_cfd .= $selloCFD[$i-1];
            if($i%150 == 0) $cadena_cfd .= "<br/>".$selloCFD[$i-1];
        }
        
        $contenido .= "<div><b>Sello Digital del Emisor</b><br/>".$cadena_cfd."</div>";

        $contenido .= '</td></tr>';
        $contenido .= '<tr><td align="left" width="100%">';
        
        $selloSat = $timbre['SelloSAT'];
        $cadena_cfd = "";
        for($i = 1; $i<=strlen($selloSat); $i++){
            $cadena_cfd .= $selloSat[$i-1];
            if($i%150 == 0) $cadena_cfd .= "<br/>".$selloSat[$i-1];
        }

        $contenido .= '<div><b>Sello Digital del SAT</b><br/>'.$cadena_cfd.'</div>';

        $contenido .= '</td></tr>';
        $contenido .= '<tr><td align="left" width="100%">';
        
        $cadena = $this->leeArchivo("cdfis/".$this->idEmpresa."/cadena_original_$idElemento.txt");
        $cadenaOriginal = "";
        for($i = 1; $i<=strlen($cadena); $i++){
            $cadenaOriginal .= $cadena[$i-1];
            if($i%150 == 0) $cadenaOriginal .= "<br/>".$cadena[$i-1];
        }
        $cadenaOriginal = utf8_encode($cadenaOriginal);
        $contenido .= "<div><b>Cadena Original</b><br/>".$cadenaOriginal."</div>";
        
        $contenido .= '</td></tr>';
        $contenido .= '</table>';

        $contenido .= '</td><td align="center" width="20%">';
        //qr con la liga facturas.hospitalmay.com/FacturacionDeCuenta.php?idcuenta=numero_cuenta&opcion=1|2|3
        $contenido .= '<img src="'.$this->getImagenQRDeFolio($idElemento, "", "").'" width="130"/>';
        $contenido .= '</td></tr></table>';
            
        $contenido .= "</td></tr>";
        $contenido .= "</table>";
        $contenido .= "</body></html>";
        
        $conector->cierraConexion();
        
        return $contenido;
    }
    
    //esta función recupera el contenido del pdf para generar el archivo
    public function getContenidoDePDFFacturaGlobal($info_xml, $idElemento){
        $conector = $this->getConector();//new ConectorOrbe();
        //if(!$conector->estableceConexion()) die("No se establecio conexion con la db");
        
        $conector->setConsulta("SET NAMES 'utf8';");
        $conector->ejecutaConsulta();
        
        $comprobante = $info_xml["Comprobante"];
        
        $timbre = $info_xml['TimbreFiscalDigital'];
        $emisor = $info_xml['Emisor'];
        $receptor = $info_xml['Receptor'];
        
        $folio = $comprobante['Folio'];
        
        $datosEmisor = explode("|", $conector->getDatosDeFacturacion($conector->getIdDatosFacturacionDeRFC($emisor['Rfc'])));
        //iddatosfacturacion, nombre, rfc, calle, exterior, interior, referencia, colonia, localidad, municipio, estado, pais, cp, telefono, email, fax, cuenta, estatus, claveRegimen, descripcionClave
        
        $datosReceptor = explode("|", $conector->getDatosDeFacturacion($conector->getIdDatosFacturacionDeRFC($receptor['Rfc'])));
        //iddatosfacturacion, nombre, rfc, calle, exterior, interior, referencia, colonia, localidad, municipio, estado, pais, cp, telefono, email, fax, cuenta, estatus, claveRegimen, descripcionClave
        
        $contenido = '<html>';
        $contenido .= '<body>';

        $contenido .= '<table width="700" align="center" cellpadding="5" cellspacing="0" border="0">';
        $contenido .= '<tr align="center" style="font-size:9px; color:#223270"><td align="left" width="50%">';

        $contenido .= '<span style="font-weight:bold;">Factura</span> <span style="color:#ff0000">'.$comprobante['Serie'].$comprobante['Folio'].'</span><br/>';
        $contenido .= '<span style="font-weight:bold;">Cartificado Digital </span><span style="color:#ff0000">'.$comprobante['NoCertificado'].'</span><br/>';
        $contenido .= '<span style="font-weight:bold;">Folio Fiscal </span><span style="color:#ff0000">'.$timbre['UUID'].'</span><br/><br/>';

        $contenido .= '<span style="font-weight:bold;">'.$datosEmisor[1].'<br/>';
        $contenido .= "R.F.C. ".$emisor['Rfc']."<br/></span>";
        $contenido .= '<span style="font-weight:bold;">'.$datosEmisor[18]." - ".strtoupper($datosEmisor[19])."<br/></span>";
        $contenido .= '<span style="font-weight:bold;">INGRESO<br/></span>';
        $contenido .= '<span style="font-weight:bold;">EXPEDICIÓN: '.$datosEmisor[12].'<br/></span>';

        $contenido .= "</td><td align='right' width='50%'>";

        if($this->ambiente == 1) $contenido .= '<div style="font-size:20px; text-align:right;"><b><br/><br/><br/><br/>Vista Previa: Sin efecto fiscal</b></div>';

        $contenido .= "</td></tr>";
        $contenido .= "</table>";

        //datos del cliente
        $contenido .= '<table width="700" align="center" cellpadding="5" cellspacing="0" border="0">';
        $contenido .= '<tr align="center" style="width:700px; padding:12px; font-size:10px; color:#223270; font-weight:bold; background-color:#DFE5E7;"><td align="left" width="100%">';
        $contenido .= '<div>Cliente:</div>';
        $contenido .= "</td></tr>";
        $contenido .= "</table>";
        
        //razón social
        $contenido .= '<table width="700" align="center" cellpadding="3" cellspacing="0" border="0">';
        $contenido .= '<tr align="center" style="width:700px; padding:12px; font-size:9px; color:#223270;"><td align="left" width="15%">';

        $contenido .= '<div>Razón Social:</div>';

        $contenido .= '</td><td align="left" width="85%" style="background-color:#F2F2F2;">';

        $contenido .= '<div>'.$datosReceptor[1].'</div>';

        $contenido .= '</td></tr>';
        $contenido .= '</table>';

        //fecha de emisión y rfc
        $contenido .= '<table width="700" align="center" cellpadding="3" cellspacing="0" border="0">';
        $contenido .= '<tr align="center" style="width:700px; padding:12px; font-size:9px; color:#223270;"><td align="left" width="15%">';

        $contenido .= '<div>R.F.C.:</div>';

        $contenido .= '</td><td align="left" width="35%" style="background-color:#F2F2F2;">';
        
        $contenido .= '<div>'.$receptor['Rfc'].'</div>';

        $contenido .= '</td><td align="left" width="15%">';

        $contenido .= '<div>Fecha de Emisión:</div>';

        $contenido .= '</td><td align="left" width="35%" style="background-color:#F2F2F2;">';

        $partes_fecha = explode("T", $comprobante['Fecha']);
        $contenido .= '<div>'.$conector->getFechaLegibleCorta($partes_fecha[0])." ".$partes_fecha[1].'</div>';

        $contenido .= '</td></tr>';
        $contenido .= '</table>';

        //dirección
        $contenido .= '<table width="700" align="center" cellpadding="3" cellspacing="0" border="0">';
        $contenido .= '<tr align="center" style="width:700px; padding:12px; font-size:9px; color:#223270;"><td align="left" width="15%">';

        $contenido .= '<div>País:</div>';

        $contenido .= '</td><td align="left" width="35%" style="background-color:#F2F2F2;">';

        $contenido .= '<div>'.$datosReceptor[11].'</div>';

        $contenido .= '</td><td align="left" width="15%">';

        $contenido .= '<div>Uso CFDI:</div>';

        $contenido .= '</td><td align="left" width="35%" style="background-color:#F2F2F2;">';

        $contenido .= '<div>'.$receptor['UsoCFDI']." - ".strtoupper($conector->getDescripcionDeUsoCFDI($receptor['UsoCFDI'])).'</div>';

        $contenido .= '</td></tr>';
        $contenido .= '<tr align="center" style="width:700px; padding:12px; font-size:9px; color:#223270;"><td align="left" width="15%">';

        $contenido .= '<div>Método de Pago:</div>';

        $contenido .= '</td><td align="left" width="35%" style="background-color:#F2F2F2;">';

        if($comprobante['MetodoPago'] == "PUE") $contenido .= '<div>PUE - PAGO EN UNA SOLA EXHIBICIÓN</div>';
        else if($comprobante['MetodoPago'] == "PPD") $contenido .= '<div>PPD - PAGO EN PARCIALIDADES O DIFERIDO</div>';

        $contenido .= '</td><td align="left" width="15%">';

        $contenido .= '<div>Forma de Pago:</div>';

        $contenido .= '</td><td align="left" width="35%" style="background-color:#F2F2F2;">';

        $contenido .= '<div>'.$comprobante['FormaPago']." - ".strtoupper($conector->getDescripcionFormaPagoSATPorId($comprobante['FormaPago'])).'</div>';
        
        $contenido .= '</td></tr>';
        
        if($comprobante['MetodoPago'] == "PUE"){
            $contenido .= '<tr align="center" style="width:700px; padding:12px; font-size:9px; color:#223270;"><td align="left" width="15%">';

            $contenido .= '<div>Moneda:</div>';

            $contenido .= '</td><td align="left" width="35%" style="background-color:#F2F2F2;">';

            $contenido .= '<div>'.$comprobante['Moneda'].'</div>';

            $contenido .= '</td><td align="left" width="15%">';

            $contenido .= '<div>Tipo de Cambio:</div>';

            $contenido .= '</td><td align="left" width="35%" style="background-color:#F2F2F2;">';

            $contenido .= '<div>'.$comprobante['TipoCambio'].'</div>';

            $contenido .= '</td></tr>';    
        }
        
        $contenido .= '<tr align="center" style="width:700px; padding:12px; font-size:9px; color:#223270;"><td align="left" width="15%">';
        $contenido .= '</td><td align="left" width="35%">';
        $contenido .= '</td><td align="left" width="15%">';
        $contenido .= '</td><td align="left" width="35%">';
        $contenido .= '</td></tr>';

        $contenido .= '</table>';

        //datos de los conceptos
        $contenido .= '<table width="700" align="center" cellpadding="5" cellspacing="0" border="0">';
        $contenido .= '<tr align="center" style="width:700px; padding:12px; font-size:10px; color:#223270; font-weight:bold; background-color:#DFE5E7;"><td align="left" width="100%">';
        $contenido .= '<div>Conceptos:</div>';
        $contenido .= "</td></tr>";
        $contenido .= "</table>";
        $contenido .= '<table width="700" align="center" cellpadding="3" cellspacing="0" border="0" style="font-size:9px; color:#000000; background-color:#F2F2F2;">';
        $contenido .= '<tr><td width="15%" align="center">C. Producto</td><td width="15%" align="center">C. Unidad</td><td width="10%" align="center">Cantidad</td><td width="30%" align="left">Descripción</td><td width="15%" align="right">P. Unitario</td><td width="15%" align="right">Importe</td></tr>';
        
        //print_r($conceptos);
        foreach($info_xml['Conceptos'] as $concepto){
            
            $contenido .= '<tr><td width="15%" align="center">'.$concepto['ClaveProdServ'].'</td><td width="15%" align="center">'.$concepto['ClaveUnidad'].'</td><td width="10%" align="center">'.$concepto['Cantidad'].'</td><td width="30%" align="left">'.$concepto['Descripcion'].'</td><td width="15%" align="right">'.$concepto['ValorUnitario'].'</td><td width="15%" align="right">'.$concepto['Importe'].'</td></tr>';
        }
        
        //subtotal
        $contenido .= '<tr><td width="10%" align="center"></td><td width="10%" align="center"></td><td width="15%" align="center"></td><td width="35%" align="left"></td><td width="15%" align="right">Subtotal:</td><td width="15%" align="right">'.$comprobante['SubTotal'].'</td></tr>';
        
        //iva
        $traslado = $info_xml['Traslado'];
        $contenido .= '<tr><td width="10%" align="center"></td><td width="10%" align="center"></td><td width="15%" align="center"></td><td width="35%" align="left"></td><td width="15%" align="right">IVA 16%:</td><td width="15%" align="right">'.$traslado['Importe'].'</td></tr>';
        
        //total a pagar
        $contenido .= '<tr><td width="10%" align="center"></td><td width="10%" align="center"></td><td width="15%" align="center"></td><td width="35%" align="left"></td><td width="15%" align="right"><b>Total a pagar:</b></td><td width="15%" align="right">'.$comprobante['Total'].'</td></tr>';
        
        $this->totalFacturado = $comprobante['Total'];
        
        $contenido .= "</table>";

        $contenido .= '<table width="700" align="center" cellpadding="3" cellspacing="0" border="0">';
        $contenido .= '<tr align="center" style="width:700px; padding:12px; font-size:9px; color:#223270;"><td align="left" width="15%">';

        $contenido .= '<div>Importe con Letra:</div>';

        $contenido .= '</td><td align="left" width="85%" style="background-color:#F2F2F2;">';

        $contenido .= '<div>'.$conector->num2letras(money_format('%!i', $comprobante['Total']), false, true).'</div>';

        $contenido .= '</td></tr>';

        $contenido .= '<tr align="center" style="width:700px; padding:12px; font-size:9px; color:#223270;"><td align="left" width="15%">';
        $contenido .= '</td><td align="left" width="85%">';
        $contenido .= '</td></tr>';
        $contenido .= '</table>';

        $contenido .= '<table width="700" align="center" cellpadding="5" cellspacing="0" border="0">';
        $contenido .= '<tr align="center" style="width:700px; padding:12px; font-size:10px; color:#223270; font-weight:bold; background-color:#DFE5E7;"><td align="left" width="100%">';
        $contenido .= '<div>Información del Timbre Fiscal Digital:</div>';
        
        $contenido .= "</td></tr>";
        $contenido .= "<tr style='width:700px;'><td align='left' width='100%' style='width:700px;'>";
        
        //tabla qr y demás datos
        $contenido .= '<table width="700" align="center" cellpadding="3" cellspacing="0" border="0">';
        $contenido .= '<tr><td align="center" width="80%">';

        //certificación digital sat y fecha de certificacion
        $contenido .= '<table width="100%" align="left" cellpadding="3" cellspacing="0" border="0" style="font-size:10px; color:#223270;">';
        $contenido .= '<tr><td align="center" width="50%">';
        //echo $info_timbre->TimbreFiscalDigital;
        $contenido .= '<div><b>Certificado Digital SAT</b><br/>'.$timbre['NoCertificadoSAT'].'</div>';
        $contenido .= '</td><td align="center" width="50%">';
        $fecha_t_separada = explode("T", $timbre['FechaTimbrado']);
        $contenido .= '<div><b>Fecha de Certificación</b><br/>'.$conector->getFechaLegibleCorta($fecha_t_separada[0]).' '.$fecha_t_separada[1].'</div>';
        $contenido .= '</td></tr></table>';

        //datos de certificación
        $contenido .= '<table width="100%" align="left" cellpadding="3" cellspacing="0" border="0" style="font-size:6px; color:#223270;">';
        $contenido .= '<tr><td align="left" width="100%">';
        //se lee el contenido del sello
        
        //echo strlen($timbre['selloCFD'])."<br/>";
        $selloCFD = $timbre['SelloCFD'];
        $cadena_cfd = "";
        for($i = 1; $i<=strlen($selloCFD); $i++){
            $cadena_cfd .= $selloCFD[$i-1];
            if($i%150 == 0) $cadena_cfd .= "<br/>".$selloCFD[$i-1];
        }
        
        $contenido .= "<div><b>Sello Digital del Emisor</b><br/>".$cadena_cfd."</div>";

        $contenido .= '</td></tr>';
        $contenido .= '<tr><td align="left" width="100%">';
        
        $selloSat = $timbre['SelloSAT'];
        $cadena_cfd = "";
        for($i = 1; $i<=strlen($selloSat); $i++){
            $cadena_cfd .= $selloSat[$i-1];
            if($i%150 == 0) $cadena_cfd .= "<br/>".$selloSat[$i-1];
        }

        $contenido .= '<div><b>Sello Digital del SAT</b><br/>'.$cadena_cfd.'</div>';

        $contenido .= '</td></tr>';
        $contenido .= '<tr><td align="left" width="100%">';
        
        $cadena = $this->leeArchivo("cdfis/".$this->idEmpresa."/cadena_original_$idElemento.txt");
        $cadenaOriginal = "";
        for($i = 1; $i<=strlen($cadena); $i++){
            $cadenaOriginal .= $cadena[$i-1];
            if($i%150 == 0) $cadenaOriginal .= "<br/>".$cadena[$i-1];
        }
        $cadenaOriginal = utf8_encode($cadenaOriginal);
        $contenido .= "<div><b>Cadena Original</b><br/>".$cadenaOriginal."</div>";
        
        $contenido .= '</td></tr>';
        $contenido .= '</table>';

        $contenido .= '</td><td align="center" width="20%">';
        //qr con la liga facturas.hospitalmay.com/FacturacionDeCuenta.php?idcuenta=numero_cuenta&opcion=1|2|3
        $contenido .= '<img src="'.$this->getImagenQRDeFolio($idElemento, "", "").'" width="130"/>';
        $contenido .= '</td></tr></table>';
            
        $contenido .= "</td></tr>";
        $contenido .= "</table>";

        $contenido .= "</body></html>";
        
        return $contenido;
    }
    
    public function getXMLDeComprobanteDePago($idPago){
        //Pueden ser de uno a muchos. Es decir, un comprobante de pago engloba varias facturas o solo una. Esto deberá reflejarse en las UUID relacionados. Debemos considerar un apartado de pagos en donde podamos agregar un pago como tal y ahí seleccionar las facturas con las que está relacionado ese pago, de tal forma que permita cumplir con esta propiedad de 1 - 1 o 1 a muchos.
        
        //se podrá emitir un recibo electrónico de pago a más tardar 10 días naturales posteriores a la recepción del pago
        
        //si una factura ya tiene al menos un pago que la cubra en su totalidad, no podrá ser cancelada la factura. En caso de ser necesario, solo aplicará cuando el RFC del receptor esté mal y se deberá sustitur por otra factura o pago que tenga las correcciones necesarias.
        
        //Si el comprobante se emitió y no debía emitirse por que el total ya había sido cubierto, entonces se deberá cancelar y sustituir por uno nuevo que tenga en el monto 1.0 pesito.
        
        //Si todos los pagos son en efectivo, entonces podemos decir que el pago fue en PUE = Pago en una sola exhibición. En otro caso, si presenta al menos un pago con medios electrónicos, deberán generarse los comprobantes de recepción de pagos correspondientes en la fecha que se efectúan los pagos, para normalidad con SAT.
        
        //la parte de pagos considera los siguientes datos adicionales al cdfi normal
        //fecha y hora de recepción del pago. Si no se sabe la fecha y hora. Si no se conoce la hora entonces se pone 12:00:00
        //FormaDePagoP de acuerdo al catálogo de formas de pago del catálogo de sat
        //MonedaP de acuerdo al catálogo de monedas del sat
        //TipoCambioP siempre y cuando monedaP sea distinta de MXN
        //Monto deberá ser mayor a 0 y menor a la suma de los montos de los documentos relacionados.
        //NumOperacion permite capturar el número de operación, número de cheque, número de SPEI o cualquier otro número que permita identificar única y eficazmente la transacción. En el caso de que sea en efectivo, podrá dejarse en blanco y entonces el sistema agregarle el id del pago interno que lo identifica
        //RfcEmisorCtaOrd RFC de la entidad o banco de donde proviene el depósito o cualquier entidad que emita el pago
        //NomBancoOrdExt nombre del banco que emite el pago
        //CtaOrdenante número de cuenta origen del pago
        //RfcEmisorCtaBen rfc del banco que recibe el pago
        //CtaBeneficiario número de cuenta del beneficiario que recibe el pago
        
        //TipoCadPago dependiendo la forma en que fue recibido el pago
        //CertPago certificado que avala el pago registrado, siempre y cuando se llene el campo anterior
        //CadPago cadena de pago que habrá de convertirse a base64. La pleca | (pipe) deberá sustituirse por &#124;
        //SelloPago también deberá capturarse cuando se ponga la información de tipoCadPago
        
        //Deberá tener uno o más documentos relacionados, los cuales pueden ser UUID de pagos previos cancelados, o pueden ser facturas que son pagadas con este pago, para tal caso, se deberán capturar los siguientes datos
        //IdDocumento : UUID del documento relacionado
        //Serie : serie del documento original
        //Folio : folio del documento original
        //MonedaDR : moneda de los documentos relacionados, deberá coincidir en este documento y en los relacionados.
        //TipoCambioDR siempre y cuando MonedaDR sea distinta de MXN
        //MetodoDePagoDR = PPD por que por esa razón estas emitiendo un comprobante de pago electrónico
        //NumParcialidad dependiendo del número de parcialidades, o incluso podrá ser 1
        //ImpSaldoAnt lo que se adeuda del monto total de los documentos relacionados
        //ImpPagado saldo pagado a cuenta de este documento relacionado. Puede ser total o parcial pero siempre mayor a 0
        //ImpSaldoInsoluto deberá ser el resultado de ImpSaldoAnt - ImpPagado
        
        $conector = new ConectorOrbe();
        if(!$conector->estableceConexion()) die("No se establecio conexion con la db");
        $conector->setConsulta("SET NAMES 'utf8';");
        $conector->ejecutaConsulta();
        
        $numeroConsecutivo = 777;
        if($this->ambiente != 1){
            //debemos reservar el siguiente número consecutivo para este elemento
        }
        
        $pago = explode("|", $conector->getInfoPagoRecibido($idPago));
        $moneda = $pago[6];
        //idPagoRecibido, idEmpresa, idCliente, fecha, hora, formaPago, moneda, cantidadPagada, tipoCambio, bancoOrigen, cuentaOrigen, bancoReceptor, cuentaReceptor, numeroOperacion, estatus, nombreEmpresa, nombreCliente, nombreFormaPago, nombreMoneda, nombreBancoOrigen, nombreBancoReceptor
        
        //recuperamos los datos de la empresa que va a facturar
        $datosEmpresa = explode("|", $conector->getEmpresaPorId($pago[1]));
        $noCertificado = $datosEmpresa[4];
        $textoCertificado = "";
        $file = fopen("cdfis/".$datosEmpresa[0]."/certificado.txt", "r");
        $this->idEmpresa = $datosEmpresa[0];
        while(!feof($file)) {
            $textoCertificado .= fgets($file);
        }
        fclose($file);
        $textoCertificado = trim(preg_replace('/\s+/','',$textoCertificado));
        
        $fechahora = $conector->getFechaActual()."T".$conector->getHoraActual();
        
        //datos de facturación de la empresa que está facturando
        $datosFacturacionEmisor = explode("|", $conector->getDatosDeFacturacion($datosEmpresa[3]));
        //iddatosfacturacion, nombre, rfc, calle, exterior, interior, referencia, colonia, localidad, municipio, estado, pais, cp, telefono, email, fax, cuenta, estatus, claveRegimen, descripcion
        $lugarExpedicion = $datosFacturacionEmisor[12];
        $rfcEmisor = $datosFacturacionEmisor[2];
        $razonSocialEmisor = $datosFacturacionEmisor[1];
        $claveRegimen = $datosFacturacionEmisor[18];
        
        //recuperamos los datos del cliente para así poder llegar a los datos de facturación
        $datosCliente = explode("|", $conector->getClientePorId($pago[2]));
        //idCliente, nombre, idDatosFacturacion
        
        //recuperamos los datos de facturacion del receptor
        $datosFacturacionReceptor = explode("|", $conector->getDatosDeFacturacion($datosCliente[2]));
        $rfcReceptor = $datosFacturacionReceptor[2];
        $razonSocialReceptor = $datosFacturacionReceptor[1];
        
        //residencia fiscal solo aplica cuando es en el extranjero
        $residenciaFiscal = "";
        if($datosFacturacionReceptor[11] != "MEX" && $datosFacturacionReceptor[11] != "MEXICO"){
            $residenciaFiscal = $datosFacturacionReceptor[11];   
        }
        
        //EN EL CASO DE LOS PAGOS, SOLO DEBERÁ IR UN CONCEPTO CON 0
        $conceptosFactura = "<cfdi:Conceptos>";
            
        $conceptosFactura .= '<cfdi:Concepto ClaveProdServ="84111506" ClaveUnidad="ACT" Cantidad="1" Descripcion="Pago" ValorUnitario="0" Importe="0"></cfdi:Concepto>';
        
        $conceptosFactura .= "</cfdi:Conceptos>";
        
        //INFORMACION ESPECIFICA DEL PAGO Y DE LOS CDFIS A LOS QUE HACE REFERENCIA
        $fechaPago = $pago[3]."T".$pago[4];
        $formaPagoPago = ($pago[5] < 10)? "0".$pago[5] : $pago[5];
        $tipoCambio = "";
        if($moneda != "MXN"){
            $tipoCambio = 'TipoCambioP="'.$pago[8].'"';
        }
        $monto = $pago[7];
        $numeroOperacion = $pago[0];
        $esBancarizado = $conector->esBancarizadaLaFormaPagoSATPorId($pago[5]);
        
        $rfcBancoEmisor = "";
        $ctaBancoEmisor = "";
        $rfcBancoReceptor = "";
        $ctaBancoReceptor = "";   
        if($esBancarizado){
            if($pago[9] != "" && $pago[10] != "" && $pago[11] != "" && $pago[12] != "" && $pago[13] != ""){
                $rfcBancoEmisor = 'RfcEmisorCtaOrd="'.$conector->getRFCBancoSATPorId($pago[9]).'"';;
                $ctaBancoEmisor = 'CtaOrdenante="'.$pago[10].'"';
                $rfcBancoReceptor = 'RfcEmisorCtaBen="'.$conector->getRFCBancoSATPorId($pago[11]).'"';
                $ctaBancoReceptor = 'CtaBeneficiario="'.$pago[12].'"';
                $numeroOperacion = $pago[13];   
            }
        }
        
        $pagos = '<cfdi:Complemento><pago10:Pagos Version="1.0">';
        $pagos .= '<pago10:Pago FechaPago="'.$fechaPago.'" FormaDePagoP="'.$formaPagoPago.'" MonedaP="'.$moneda.'" '.$tipoCambio.' Monto="'.$monto.'" NumOperacion="'.$numeroOperacion.'" '.$rfcBancoEmisor.' '.$ctaBancoEmisor.' '.$rfcBancoReceptor.' '.$ctaBancoReceptor.'/>';
        $pagos .= '</pago10:Pagos></cfdi:Complemento>';
        
        $xml = '<?xml version="1.0" encoding="UTF-8"?><cfdi:Comprobante xmlns:cfdi="http://www.sat.gob.mx/cfd/3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:pago10="http://www.sat.gob.mx/Pagos" xsi:schemaLocation="http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd http://www.sat.gob.mx/Pagos http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos10.xsd" Total="0" Moneda="XXX" TipoDeComprobante="P" LugarExpedicion="'.$lugarExpedicion.'" SubTotal="0" Folio="777" Fecha="'.$fechahora.'" Version="3.3" Serie="P" Sello="" Certificado="'.$textoCertificado.'" NoCertificado="'.$noCertificado.'"><cfdi:Emisor Rfc="'.$rfcEmisor.'" Nombre="'.$razonSocialEmisor.'" RegimenFiscal="'.$claveRegimen.'"/><cfdi:Receptor Rfc="'.$rfcReceptor.'" Nombre="'.$razonSocialReceptor.'" UsoCFDI="P01"/>'.$conceptosFactura.$pagos.'</cfdi:Comprobante>';
        
        //generamos el sello para el cfdi considerando el xml de entrada, el idElemento a facturar y el idEmpresa de la empresa que va a generar esa factura
        $sello = trim($this->getSelloParaCFDI($xml, $idPago, $pago[1]));
        
        $xml = '<?xml version="1.0" encoding="UTF-8"?><cfdi:Comprobante xmlns:cfdi="http://www.sat.gob.mx/cfd/3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:pago10="http://www.sat.gob.mx/Pagos" xsi:schemaLocation="http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd http://www.sat.gob.mx/Pagos http://www.sat.gob.mx/sitio_internet/cfd/Pagos/Pagos10.xsd" Total="0" Moneda="XXX" TipoDeComprobante="P" LugarExpedicion="'.$lugarExpedicion.'" SubTotal="0" Folio="777" Fecha="'.$fechahora.'" Version="3.3" Serie="P" Sello="'.$sello.'" Certificado="'.$textoCertificado.'" NoCertificado="'.$noCertificado.'"><cfdi:Emisor Rfc="'.$rfcEmisor.'" Nombre="'.$razonSocialEmisor.'" RegimenFiscal="'.$claveRegimen.'"/><cfdi:Receptor Rfc="'.$rfcReceptor.'" Nombre="'.$razonSocialReceptor.'" UsoCFDI="P01"/>'.$conceptosFactura.$pagos.'</cfdi:Comprobante>';
        
        $conector->cierraConexion();
        
        return $xml;
    }
    
    //esta función genera el xml correspondiente a la factura
    public function getXMLParaTimbradoFacturaGlobal($idGasto){
        $conector = $this->getConector();//new ConectorOrbe();
        //if(!$conector->estableceConexion()) die("No se establecio conexion con la db");
        
        $conector->setConsulta("SET NAMES 'utf8';");
        $conector->ejecutaConsulta();
        
        $numeroConsecutivo = 777;
        if($this->ambiente != 1){
            //recuperamos un registro que tenga este tipo de factura y este idelemento; pero que en UUID tenga -1, lo cual indica que es una factura reservada y que por alguna razón no se pudo concluir el proceso de facturación.
            $idPrevio = $conector->getRegistroPrevioDeFacturaEmitida(1, $idGasto, $this->idEmpresa);
            if($idPrevio > 0) $numeroConsecutivo = $idPrevio;
            else{
                //como no existe, entonces hay que crear el registro para este elemento, o "apartar" el siguiente id para que sirva como consecutivo
                $numeroConsecutivo = $conector->guardaFactura(0, 1, $idGasto, -1, 0, $this->idEmpresa);
            }
        }
        
        //recuperamos la info del gasto asociado
        $gasto = explode("|", $conector->getGastoDeClientePorId($idGasto));
        //return idGastoCliente, idPromotor, idEmpresa, idCliente, idBanco, fecha, hora, cantidadTotal, porcentaje, idUsuario, estatus, promotor, empresa, cliente, banco, metodoDePago, idFormaDePago, moneda, tipoCambio, usoCFDI19, formaPago20, tieneXML21, descripcionUsoCFDI22
        $usoCFDI = $gasto[19];
        
        //recuperamos los datos de la empresa que va a facturar
        $datosEmpresa = explode("|", $conector->getEmpresaPorId($gasto[2]));
        $noCertificado = $datosEmpresa[4];
        $textoCertificado = "";
        $file = fopen("cdfis/".$datosEmpresa[0]."/certificado.txt", "r");
        $this->idEmpresa = $datosEmpresa[0];
        while(!feof($file)) {
            $textoCertificado .= fgets($file);
        }
        fclose($file);
        $textoCertificado = trim(preg_replace('/\s+/','',$textoCertificado));
        
        //idEmpresa, nombre, estatus, idDatosFacturacion, noCertificado, fechaVencimiento, textoCertificado
        
        //recuperamos los conceptos asociados al gasto que estamos considerando
        $conceptos = explode("^", $conector->getConceptosDeGasto($idGasto));
        array_pop($conceptos);
        $total = 0;
        $descuento = 0;
        $iva = 0;
        $totalConIva = 0;
        $conceptosFactura = "";
        foreach($conceptos as $concepto){
            $concepto = explode("|", $concepto);
            //idConcepto, nombreUnidad, nombreProducto, descripcion, precioUnitario, cantidad, descuento, total, idUnidad, idProducto
            
            /*
            <cfdi:Impuestos><cfdi:Traslados><cfdi:Traslado Base="2250000" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.160000" Importe="360000"/></cfdi:Traslados></cfdi:Impuestos></cfdi:Concepto>
            */
            $claveProducto = $concepto[9];
            $claveUnidad = $concepto[8];
            $noIdentificacion = $concepto[0];
            $cantidad = $concepto[5];
            $descripcion = $concepto[3];
            $valorUnitario = $concepto[4];
            $importe = money_format('%!i', ($concepto[5] * $concepto[4]));
            $iva = money_format('%!i', ($importe * 0.16));
            
            if($conceptosFactura == "") $conceptosFactura = "<cfdi:Conceptos>";
            
            $conceptosFactura .= '<cfdi:Concepto ClaveProdServ="'.$claveProducto.'" ClaveUnidad="'.$claveUnidad.'" NoIdentificacion="'.$noIdentificacion.'" Cantidad="'.$cantidad.'" Descripcion="'.$descripcion.'" ValorUnitario="'.$valorUnitario.'" Importe="'.$importe.'" Descuento="'.$concepto[6].'"><cfdi:Impuestos><cfdi:Traslados><cfdi:Traslado Base="'.$importe.'" Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.160000" Importe="'.$iva.'"/></cfdi:Traslados></cfdi:Impuestos></cfdi:Concepto>';
            
            $total += ($concepto[4]*$concepto[5]);
            $descuento += $concepto[6];
        }
        if($conceptosFactura != "") $conceptosFactura .= "</cfdi:Conceptos>";
        
        $iva = $total * 0.16;
        $totalConIva = $total * 1.16;
        
        $total = money_format('%!i', $total);
        $descuento = money_format('%!i', $descuento);
        $iva = money_format('%!i', $iva);
        $totalConIva = money_format('%!i', $totalConIva);
        
        $impuestos = '<cfdi:Impuestos TotalImpuestosTrasladados="'.$iva.'"><cfdi:Traslados><cfdi:Traslado Impuesto="002" TipoFactor="Tasa" TasaOCuota="0.160000" Importe="'.$iva.'"/></cfdi:Traslados></cfdi:Impuestos>';
        
        $fechahora = $conector->getFechaActual()."T".$conector->getHoraActual();
        
        //datos de facturación de la empresa que está facturando
        $datosFacturacionEmisor = explode("|", $conector->getDatosDeFacturacion($datosEmpresa[3]));
        //iddatosfacturacion, nombre, rfc, calle, exterior, interior, referencia, colonia, localidad, municipio, estado, pais, cp, telefono, email, fax, cuenta, estatus, claveRegimen, descripcion
        if(!isset($datosFacturacionEmisor[12])){
            die("La empresa no tiene lugar de expedición (código postal) en datos de facturación, no es posible continuar!");    
        }
        
        $lugarExpedicion = $datosFacturacionEmisor[12];
        
        if(!isset($datosFacturacionEmisor[2])){
            die("La empresa no tiene RFC en datos de facturación, no es posible continuar!");    
        }
        
        $rfcEmisor = $datosFacturacionEmisor[2];
        
        if(!isset($datosFacturacionEmisor[1])){
            die("La empresa no tiene Razón Social en datos de facturación, no es posible continuar!");    
        }
        
        $razonSocialEmisor = $datosFacturacionEmisor[1];
        
        if(!isset($datosFacturacionEmisor[18])){
            die("La empresa no tiene Clave Régimen en datos de facturación, no es posible continuar!");    
        }
        $claveRegimen = $datosFacturacionEmisor[18];
        
        //recuperamos los datos del cliente para así poder llegar a los datos de facturación
        $datosCliente = explode("|", $conector->getClientePorId($gasto[3]));
        //idCliente, nombre, idDatosFacturacion
        
        //recuperamos los datos de facturacion del receptor
        $datosFacturacionReceptor = explode("|", $conector->getDatosDeFacturacion($datosCliente[2]));
        
        if(!isset($datosFacturacionReceptor[2])){
            die("La empresa no tiene RFC en datos de facturación, no es posible continuar!");    
        }
        
        $rfcReceptor = $datosFacturacionReceptor[2];
        
        if(!isset($datosFacturacionReceptor[1])){
            die("La empresa no tiene RFC en datos de facturación, no es posible continuar!");    
        }
        
        $razonSocialReceptor = $datosFacturacionReceptor[1];
        
        //residencia fiscal solo aplica cuando es en el extranjero
        $residenciaFiscal = "";
        if($datosFacturacionReceptor[11] != "MEX" && $datosFacturacionReceptor[11] != "MEXICO"){
            $residenciaFiscal = $datosFacturacionReceptor[11];   
        }
        
        $metodoPago = $gasto[15];
        
        if($gasto[16] < 10) $formaPago = "0".$gasto[16];
        else $formaPago = $gasto[16];
        
        $moneda = "";
        $tipoCambio = "";
        
        $moneda = 'Moneda="'.$gasto[17].'"';
        $tipoCambio = 'TipoCambio="'.$gasto[18].'"';
        
        //datos necesarios
        //sello = generado cuando se firma este xml
        //noCertificado =  campo que debe ir en los datos de facturación de la empresa
        //Certificado = Certificado en base64, debemos permitir al usuario subir el csd en la parte de empresas
        //Este tipo de comprobante siempre será I = Ingreso
        //Forma de Pago //deberá de tomarse y considerarse el catálogo de pagos asociados a este gasto de cliente
        //MetodoPago //También deberá considerar los pagos agregados a este gasto de cliente
        //CondicionesDePago //quedará vacio de momento
        //Descuento = suma de todos los descuentos aplicados a cada producto, según los conceptos capturados
        //lugar de expedición = se tomará el código postal de los datos de facturación de la empresa que factura
        //regimen fiscal en la forma de datos de facturación
        //residencia fiscal deberá aparecer también en la forma de datos de facturación
        //si residencia fiscal es distinta de MEX, entonces deberán capturar NumRegIdTrib
        //USO CFDI en este caso deberá ser para uso general de adquisición de mercancía
        //cuando se vaya a emitir el cdfi se deberá permitir seleccionar el tipo de uso que le darán al comprobante
        //99 = Forma de pago por definir
        //Método Pago = PPD = Pago en Parcialidades o Diferido
        
        $this->folio = $numeroConsecutivo;
        $this->montoFacturado = $totalConIva;
        
        $xml = '<?xml version="1.0" encoding="UTF-8"?><cfdi:Comprobante xmlns:cfdi="http://www.sat.gob.mx/cfd/3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '.$tipoCambio.' Total="'.$totalConIva.'" Descuento="'.$descuento.'" '.$moneda.' TipoDeComprobante="I" xsi:schemaLocation="http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd" MetodoPago="'.$metodoPago.'" LugarExpedicion="'.$lugarExpedicion.'" SubTotal="'.$total.'" Folio="'.$numeroConsecutivo.'" Fecha="'.$fechahora.'" Version="3.3" Serie="I" Sello="" Certificado="'.$textoCertificado.'" FormaPago="'.$formaPago.'" NoCertificado="'.$noCertificado.'"><cfdi:Emisor Rfc="'.$rfcEmisor.'" Nombre="'.$razonSocialEmisor.'" RegimenFiscal="'.$claveRegimen.'"/><cfdi:Receptor Rfc="'.$rfcReceptor.'" Nombre="'.$razonSocialReceptor.'" UsoCFDI="'.$usoCFDI.'"/>'.$conceptosFactura.$impuestos.'</cfdi:Comprobante>';
        
        //generamos el sello para el cfdi considerando el xml de entrada, el idElemento a facturar y el idEmpresa de la empresa que va a generar esa factura
        $sello = trim($this->getSelloParaCFDI($xml, $idGasto, $gasto[2]));
        
        $xml = '<?xml version="1.0" encoding="UTF-8"?><cfdi:Comprobante xmlns:cfdi="http://www.sat.gob.mx/cfd/3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '.$tipoCambio.' Total="'.$totalConIva.'" Descuento="'.$descuento.'" '.$moneda.' TipoDeComprobante="I" xsi:schemaLocation="http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv33.xsd" MetodoPago="'.$metodoPago.'" LugarExpedicion="'.$lugarExpedicion.'" SubTotal="'.$total.'" Folio="'.$numeroConsecutivo.'" Fecha="'.$fechahora.'" Version="3.3" Serie="I" Sello="'.$sello.'" Certificado="'.$textoCertificado.'" FormaPago="'.$formaPago.'" NoCertificado="'.$noCertificado.'"><cfdi:Emisor Rfc="'.$rfcEmisor.'" Nombre="'.$razonSocialEmisor.'" RegimenFiscal="'.$claveRegimen.'"/><cfdi:Receptor Rfc="'.$rfcReceptor.'" Nombre="'.$razonSocialReceptor.'" UsoCFDI="'.$usoCFDI.'"/>'.$conceptosFactura.$impuestos.'</cfdi:Comprobante>';
        
        return $xml;
    }
}
?>