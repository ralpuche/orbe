<?php
header("Cache-Control: no-store, no-cache, must-revalidate");
header('Content-type: text/html; charset=utf-8');
include("ConectorBaseDatos.class.php");
class ConectorOrbe extends ConectorBaseDatos{

    public $query;
    public $id_conexion;
    public $debug;
    public $gateway;

    public function ConectorOrbe(){
            $this->inicializaComponentes();
        }

    private function inicializaComponentes(){
		$this->id_conexion = null;
		$this->query = "";
		$this->debug = false;
		$this->gateway = "172.16.1.1";
        date_default_timezone_set('America/Mexico_City');
    }

    public function setConsulta($consulta){
			$this->query = $consulta;
			if($this->debug) echo "<br>Consulta: ".$this->query;
        }

    public function ejecutaConsulta(){
		try{
			$this->id_conexion = mysql_query($this->query, $this->conexion);
		}catch(Exception $e){
			echo $e->getMessage();
		}
		if(!$this->id_conexion)return false;
		else return true;
        }

    public function getResultados(){
            return $this->id_conexion;
        }
    
    public function cleanInput($input) {
        $search = array(
            '@<script [^>]*?>.*?@si',            // Strip out javascript
			'@< [/!]*?[^<>]*?>@si',            // Strip out HTML tags
			'@<style [^>]*?>.*?</style>@siU',    // Strip style tags properly
			'@< ![sS]*?--[ tnr]*>@'         // Strip multi-line comments
        );
        $output = preg_replace($search, '', $input);
        return $output;
    }
	
    public function sanitize($input) {
        if (is_array($input)) {
            foreach($input as $var=>$val) {
                $output[$var] = $this->sanitize($val);
            }
        }
        else {
            if (get_magic_quotes_gpc()) {
				$input = stripslashes($input);
            }
            $input  = $this->cleanInput($input);
            $output = mysql_real_escape_string($input);
        }
        return $output;
    }

	public function cierraConexion(){
        mysql_close( $this->conexion);
    }	
		
	public function setDebugOn(){
			$this->debug = true;
		}
		
	public function quitaAcentos($cadena){
        	$tofind = "ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ";
            $replac = "AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn";
            return(strtr($cadena,$tofind,$replac));
        }
    
    public function getResultadosDeConsulta($consulta){
        $this->setConsulta($consulta);
        if($this->ejecutaConsulta()){
            $resultados = "";
            while($row = mysql_fetch_row($this->id_conexion)){
                $i = 0;
                foreach($row as $dato){
                    if($i == 0) $resultados .= $dato;
                    else $resultados .= "|".$dato;
                    $i++;
                }
                $resultados .= "^";
            }
            return $resultados;
        }
        return "";	
    }
    
    public function getResultadosDeConsultaConDelimitador($consulta, $delimitador){
        $this->setConsulta($consulta);
        if($this->ejecutaConsulta()){
            $resultados = "";
            while($row = mysql_fetch_row($this->id_conexion)){
                $i = 0;
                foreach($row as $dato){
                    if($i == 0) $resultados .= $dato;
                    else $resultados .= $delimitador.$dato;
                    $i++;
                }
                $resultados .= "^";
            }
            return $resultados;
        }
        return "";	
    }
    
    public function getResultadosDeConsultaConDelimitadorPorTupla($consulta, $delimitador){
        $this->setConsulta($consulta);
        if($this->ejecutaConsulta()){
            $resultados = "";
            while($row = mysql_fetch_row($this->id_conexion)){
                $i = 0;
                foreach($row as $dato){
                    if($i == 0) $resultados .= $dato;
                    else $resultados .= "|".$dato;
                    $i++;
                }
                $resultados .= $delimitador;
            }
            return $resultados;
        }
        return "";	
    }
    
    public function getResultadosDeConsultaConDelimitadorPorCampoAndTupla($consulta, $delimitador_campo, $delimitador_tupla){
        $this->setConsulta($consulta);
        if($this->ejecutaConsulta()){
            $resultados = "";
            while($row = mysql_fetch_row($this->id_conexion)){
                $i = 0;
                foreach($row as $dato){
                    if($i == 0) $resultados .= $dato;
                    else $resultados .= $delimitador_campo.$dato;
                    $i++;
                }
                $resultados .= $delimitador_tupla;
            }
            return $resultados;
        }
        return "";	
    }
    
    public function getResultadoDeConsulta($consulta){
        $this->setConsulta($consulta);
        if($this->ejecutaConsulta()){
            $resultado = "";
            while($row = mysql_fetch_row($this->id_conexion)){
                $i = 0;
                foreach($row as $dato){
                    if($i == 0) $resultado .= $dato;
                    else $resultado .= "|".$dato;
                    $i++;
                }
                break;
            }
            return $resultado;
        }
        return "";	
    }
    
    public function getResultadoDeConsultaConDelimitador($consulta, $delimitador){
        $this->setConsulta($consulta);
        if($this->ejecutaConsulta()){
            $resultado = "";
            while($row = mysql_fetch_row($this->id_conexion)){
                $i = 0;
                foreach($row as $dato){
                    if($i == 0) $resultado .= $dato;
                    else $resultado .= $delimitador.$dato;
                    $i++;
                }
                break;
            }
            return $resultado;
        }
        return "";	
    }
    
    public function insertaNuevoRegistro($consulta, $idsuceso, $mensaje, $datos){
        $this->setConsulta($consulta);
        if($this->ejecutaConsulta()){
            $idinsertado = mysql_insert_id();
            if($idinsertado > 0 && $idsuceso > 0) $this->addSucesoAlaBitacora($idsuceso, $mensaje, $datos);
            return $idinsertado;
        }
        return 0;
    }
    
    public function actualizaRegistro($consulta, $idsuceso, $mensaje, $datos){
        $this->setConsulta($consulta);
        if($this->ejecutaConsulta()){
            if($idsuceso > 0) $this->addSucesoAlaBitacora($idsuceso, $mensaje, $datos);
            return 1;
        }
        return 0;
    }
    
    public function existeRegistro($consulta){
        $this->setConsulta($consulta);
        if($this->ejecutaConsulta()){
            while($row = mysql_fetch_array($this->id_conexion)) return 1;
        }
        return 0;
    }
    
    public function getFechaActual(){
		return date("Y-m-d");
	}
	
    public function getHoraActual(){
		return date("H:i:s");
	}
	
    //esta función recupera la ip del cliente
    public function getIpDelCliente(){
		if( $_SERVER['HTTP_X_FORWARDED_FOR'] != '' ){
			$client_ip = (!empty($_SERVER['REMOTE_ADDR']))?$_SERVER['REMOTE_ADDR']:((!empty($_ENV['REMOTE_ADDR']))?$_ENV['REMOTE_ADDR']:"unknown");
			$entries = split('[, ]', $_SERVER['HTTP_X_FORWARDED_FOR']);
			reset($entries);
			while(list(,$entry)=each($entries)){
				$entry = trim($entry);
				if(preg_match("/^([0-9]+\\.[0-9]+\\.[0-9]+\\.[0-9]+)/",$entry,$ip_list)){
					$private_ip = array('/^0\\./','/^127\\.0\\.0\\.1/','/^192\\.168\\..*/','/^172\\.((1[6-9])|(2[0-9])|(3[0-1]))\\..*/','/^10\\..*/');	
					$found_ip = preg_replace($private_ip, $client_ip, $ip_list[1]);
					if ($client_ip != $found_ip){
						$client_ip = $found_ip;
						break;
					}
				}
			}
		}else{
			$client_ip=(!empty($_SERVER['REMOTE_ADDR']))?$_SERVER['REMOTE_ADDR']:((!empty($_ENV['REMOTE_ADDR']))?$_ENV['REMOTE_ADDR']:"unknown");
   		}
   		return $client_ip;
	}
    
    public function getTokenActual(){
		if(isset($_SESSION['token'])) return $_SESSION['token'];
		return "";
	}
	
    public function getToken(){
		//el token es un número que se genera de forma aleatoria y que sirve para identificar correctamente al cliente.
		return $this->getFechaActual.$this->getHoraActual().$this->getIpDelCliente();
	}
    
    //esta funcion recupera la diferencia en horas dada una fecha de inicio, fecha de fin, hora de inicio y hora de fin
    public function getDiferenciaDeTiempoEnHoras($fecha_inicio, $fecha_fin, $hora_inicio, $hora_fin){
        //[ int $hour = date("H") [, int $minute = date("i") [, int $second = date("s") [, int $month = date("n") [, int $day = date("j") [, int $year = date("Y") [, int $is_dst = -1 ]]]]]]] 
        $fecha_inicio_separada = explode("-", $fecha_inicio);
        $fecha_fin_separada = explode("-", $fecha_fin);
        
        $hora_inicio_separada = explode(":", $hora_inicio);
        $hora_fin_separada = explode(":", $hora_fin);
        
        $fecha1 = mktime($hora_inicio_separada[0],$hora_inicio_separada[1],00,$fecha_inicio_separada[1],$fecha_inicio_separada[2],$fecha_inicio_separada[0]);
        $fecha2 = mktime($hora_fin_separada[0],$hora_fin_separada[1],00,$fecha_fin_separada[1],$fecha_fin_separada[2],$fecha_fin_separada[0]);
        $diferencia = $fecha2-$fecha1;
        
        $horas_completas = ($diferencia/(60*60));
        
        $decimales = explode(".", $horas_completas);
        if(count($decimales) == 2 && $decimales[1] != "00") $horas_completas = $decimales[0]+1;
        else $horas_completas = $decimales[0];
        
        return $horas_completas;
    }
    
    public function getFechaConRestaDeNumeroDeHoras($fecha_inicial, $numero_horas){
        $fecha = date($fecha_inicial);
        $nuevafecha = strtotime ( '-'.$numero_horas.' hour' , strtotime ( $fecha ) ) ;
        return date ( 'Y-m-j' , $nuevafecha );
    }
    
    public function getHoraConRestaDeNumeroDeHoras($fecha_inicial, $numero_horas){
        $fecha = date($fecha_inicial);
        $nuevafecha = strtotime ( '-'.$numero_horas.' hour' , strtotime ( $fecha ) ) ;
        return date ( 'H:i:s' , $nuevafecha );
    }
    
    public function leeArchivoCSVGenerico($nombre, $ignoraHeader){
        $fp = fopen($nombre, "r");
        $lineas = Array();
        $i = 0;
        while(!feof($fp)) {
            $lineas[] = fgetcsv($fp, 1000);
        }
        fclose($fp);

        if($ignoraHeader){
            unset($lineas[0]);
            return $lineas;
        }
        return $lineas;
    }
    
    public function getCantidadConSoloDosDecimales($cantidad){
        $cantidad = explode(".", $cantidad);
        if(count($cantidad) == 1) return $cantidad[0];
        return $cantidad[0].".".substr($cantidad[1], 0, 2);
    }
    
    //función común para mejorar legilibilidaad de fechas. Toma como parámetro una fecha y la transforma en una fecha legible
	//el formato de la fecha debe ser año(2011)-mes(xx)-día(xx)
	public function getFechaLegible($fecha){
		//primero se separa la fecha en función de los guiones que presenta
		$fecha_separada = explode("-", $fecha);
		return $fecha_separada[2]." de ".$this->getNombreMes($fecha_separada[1])." del ".$fecha_separada[0];
	}
	
	//el formato de la fecha debe ser año(2011)-mes(xx)-día(xx)
	public function getFechaLegibleCorta($fecha){
		//primero se separa la fecha en función de los guiones que presenta
		$fecha_separada = explode("-", $fecha);
		return $fecha_separada[2]."-".$this->getNombreMesCorto($fecha_separada[1])."-".$fecha_separada[0];
	}
	
	//toma como parámetro el número del mes del 01 al 12
	public function getNombreMesCorto($mes){
		switch($mes){
			case "01": return "Ene";
			break;
						 
			case "02": return "Feb";
			break;
				
			case "03": return "Mar";
			break;
				
			case "04": return "Abr";
			break;
				
			case "05": return "May";
			break;
				
			case "06": return "Jun";
			break;
						 
			case "07": return "Jul";
			break;
						 
			case "08": return "Ago";
			break;
				
			case "09": return "Sep";
			break;
				
			case "10": return "Oct";
			break;
						 
			case "11": return "Nov";
			break;
						 
			case "12": return "Dic";
			break;
		}
	}	
		
	//función común para recuperar el nombre de un mes
	//toma como parámetro el número del mes del 01 al 12
	public function getNombreMes($mes){
		switch($mes){
			case "01": return "Enero";
			break;
						 
			case "02": return "Febrero";
			break;
				
			case "03": return "Marzo";
			break;
				
			case "04": return "Abril";
			break;
				
			case "05": return "Mayo";
			break;
				
			case "06": return "Junio";
			break;
						 
			case "07": return "Julio";
			break;
						 
			case "08": return "Agosto";
			break;
				
			case "09": return "Septiembre";
			break;
				
			case "10": return "Octubre";
			break;
						 
			case "11": return "Noviembre";
			break;
						 
			case "12": return "Diciembre";
			break;
		}
	}
    
    public function enviaCorreoAlAdministrador($texto, $titulo){
        //echo $contenido;
        require_once('phpgmailer/class.phpgmailer.php');
        
        $mail = new PHPGMailer();
        $cuenta_correo = "hospitalmaybk@gmail.com";
        $password_correo = "mayhospitalserver";
        $mail->Username = $cuenta_correo;
        $mail->Password = $password_correo;
        $mail->From = "sistema@hospitalmay.com";
        $mail->FromName = "Orbe Solutions";
        $mail->Subject = $titulo;
        $mail->AddReplyTo($cuenta_correo);

        $mensaje = $texto;

        $mail->Body = $mensaje;
        $mail->IsHTML(true);
        
        $mail->AddAddress('"ralpuchev@gmail.com"');
        
        while(!$mail->Send()){
            sleep(3000);//mientras no se envíe, nos dormimos 5 minutos
        }
    }
    
    public function esEmailValido($email){ 
        $email=trim($email); 
        $email=strtolower($email); 
        $email=addslashes($email); 
        if(!$email){ 
            return false;
        }else{ 
            if(@!eregi("^[a-z]+[_\.\-]{0,1}[a-z0-9]+@[a-z0-9]+[_\.\-]{0,1}[a-z0-9]+\.[a-z]{2,4}$", $email)){ 
                return false;
            }
        }
        return true;
    }
    
    /*
    FUNCIONES PARTICULARES DE LA APLICACION
    */
    
    //se elimina el comprobante de pago dado como idComprobante
    public function eliminaComprobanteDePago($idComprobante){
        return $this->actualizaRegistro("update PagosRecibidos set estatus = 0 where idPagoRecibido = $idComprobante limit 1;", 39, "Comprobante de Pago eliminado", $this->getInfoPagoRecibido($idConcepto));
    }
    
    //esta función elimina un concepto de facturación. En realidad solo cambia su estatus, para que pueda seguir siendo referenciado en las facturas que ya fueron emitidas y que lo contenían como gasto de facturación
    public function eliminaConceptoDeFacturacion($idConcepto){
        return $this->actualizaRegistro("update ConceptosFacturacion set estatus = 0 where idConceptoFacturacion = $idConcepto limit 1;", 38, "Concepto de Facturación eliminado", $this->getConceptoDeFacturacionPorId($idConcepto));
    }
    
    public function cambiaEstatusDeFacturaACancelada($idFactura){
        //cambiamos el estatus de la factura a -1
        return $this->actualizaRegistro("update FacturasEmitidas set fecha='".$this->getFechaActual()."', hora='".$this->getHoraActual()."', usuario=".$_SESSION['idusuario'].", estatus = -2 where idFacturaEmitida = $idFactura limit 1;", 37, "Factura Cancelada: $idFactura", $this->getInfoFacturaPorId($idFactura));
        //el estatus 1 es activo, -1 apartado y -2 cancelado
    }
    
    public function getIdFactura($idEmpresa, $tipoElemento, $idElemento){
        return $this->getResultadoDeConsulta("select idFacturaEmitida from FacturasEmitidas where idEmpresa = $idEmpresa and tipoFactura = $tipoElemento and idElemento = $idElemento limit 1;");
    }
    
    public function getDescripcionDeUsoCFDI($iduso){
        $datos = explode("^", $this->getUsosCFDI());
        array_pop($datos);
        
        foreach($datos as $dato){
            $dato = explode("|", $dato);
            if($dato[1] == $iduso) return $dato[2];
        }
        return "No encontrado";
    }
    
    //recupera el catálogo de usos cdfis
    public function getUsosCFDI(){
        //se recupera la información desde el catálogo de productos y servicios
        $pathArchivo = "catalogosFacturacion" . DIRECTORY_SEPARATOR . "usoCFDI.csv";
        $lineas = $this->leeArchivoCSVGenerico($pathArchivo, true);
        $usos = "";
        
        $i = 1;
        foreach($lineas as $linea){
            $usos .= $i."|".$linea[0]."|".$linea[1]."^";
            $i++;
        }
        return $usos;
    }
    
    //esta función cancela una factura
    public function enviaFacturaCanceladaPorCorreo($idFactura){
        //recuperamos primero los datos de la factura que se habrá de enviar en la notificicación
        $datos = explode("|", $this->getInfoFacturaPorId($idFactura));
        //idFacturaEmitida, tipoFactura, idElemento, UUID, cantidadFacturada, fecha, hora, usuario, estatus, nombreUsuario
        
        $datosElemento = null;
        if($datos[1] == 1) $datosElemento = explode("|", $this->getGastoDeClientePorId($datos[2]));
        else if($datos[1] == 2) $datosElemento = explode("|", $this->getGastoDeClientePorId($datos[2]));
        
        //gasto : idGastoCliente, idPromotor, idEmpresa, idCliente, idBanco, fecha, hora, cantidadTotal, porcentaje, idUsuario, estatus, promotor, empresa, cliente, banco, metodoDePago, idFormaDePago, moneda, tipoCambio, formaPago
        
        $empresa = explode("|", $this->getEmpresaPorId($datosElemento[2]));
        //idEmpresa, nombre, estatus, idDatosFacturacion, noCertificado, fechaVencimiento, textoCertificado
        $datosFacturacionEmpresa = explode("|", $this->getDatosDeFacturacion($empresa[3]));
        //iddatosfacturacion, nombre, rfc, calle, exterior, interior, referencia, colonia, localidad, municipio, estado, pais, cp, telefono, email, fax, cuenta, estatus, claveRegimen, descripcionClave
        
        $cliente = explode("|", $this->getClientePorId($datosElemento[3]));
        //idCliente, nombre, idDatosFacturacion
        $datosFacturacionCliente = explode("|", $this->getDatosDeFacturacion($cliente[2]));
        //iddatosfacturacion, nombre, rfc, calle, exterior, interior, referencia, colonia, localidad, municipio, estado, pais, cp, telefono, email, fax, cuenta, estatus, claveRegimen, descripcionClave
        
        $contenido = "<html>";
        $contenido .= "<body>";
        $contenido .= "<div style='background-color: #ffffff; border.radius: 5px; border:0.5px solid #868786;width: 96%;margin:0 auto 0 auto;text-align: center;'>";
        
        $contenido .= "<table align='center' width='96%' cellpadding='3' cellspacing='0' border='0'>";
        $contenido .= "<tr><td align='left' valign='top'>";
        
        $contenido .= "<br/>";
        
        //$contenido .= "<img src='http://sistema.hospitalmay.com/Imagenes/logo.png' width='200'/>";
        
        $contenido .= "</td></tr><tr><td align='center' valign='top'>";
        
        $contenido .= "<div style='color:#009EE3; font-size:30px;'><b>Notificación de Factura Cancelada</b></div><br/>";
        
        $contenido .= "<hr style='width:98%; border: 0; height: 0; border-top: 1px solid rgba(0, 0, 0, 0.1); border-bottom: 1px solid rgba(255, 255, 255, 0.3);'></hr><br/>";
        
        $contenido .= "<div style='font-size:21px; text-align:left;'>La siguiente factura ha sido cancelada:</div><br/>";
        
        $contenido .= "<table align='center' cellpadding='6' cellspacing='0' width='100%'><tr><td width='50%' align='center' valign='top'>";
        
        //datos del emisor
        $contenido .= "<div style='color:#009EE3; font-size:21px;'><b>Emisor</b></div><br/>";
        $contenido .= "<div style='font-size:21px; text-align:left;'>".$datosFacturacionEmpresa[1]."</div>";
        $contenido .= "<div style='font-size:21px; text-align:left;'>".$datosFacturacionEmpresa[2]."</div>";
        
        $contenido .= "</td><td width='50%' align='center' valign='top'>";
        
        //datos del receptor
        $contenido .= "<div style='color:#009EE3; font-size:21px;'><b>Receptor</b></div><br/>";
        $contenido .= "<div style='font-size:21px; text-align:left;'>".$datosFacturacionCliente[1]."</div>";
        $contenido .= "<div style='font-size:21px; text-align:left;'>".$datosFacturacionCliente[2]."</div>";
        
        $contenido .= "</td></tr></table>";
        $contenido .= "<br/>";
        $contenido .= "<div style='font-size:21px; text-align:left;'>Folio Fiscal: <b>".$datos[3]."</b></div>";
        $contenido .= "<div style='font-size:21px; text-align:left;'>Cancelada el <b>".$this->getFechaLegible($datos[5])."</b> a las <b>".$datos[6]."</b></div>";
        
        $contenido .= "<br/>";
        
        $contenido .= "<br/>";
        $contenido .= "<hr style='width:98%; border: 0; height: 0; border-top: 1px solid rgba(0, 0, 0, 0.1); border-bottom: 1px solid rgba(255, 255, 255, 0.3);'></hr><br/>";
        
        $contenido .= "<div style='font-size:16px; text-align:left;'>Este es un mensaje enviado de forma automática.</div><br/>";
        
        $contenido .= "</td></tr></table>";
        $contenido .= "</div>";
        $contenido .= "</body>";
        $contenido .= "</html>";
        
        require_once('phpgmailer/class.phpgmailer.php');
        
        $mail = new PHPGMailer();
        $cuenta_correo = "orbe.robot@gmail.com";
        $password_correo = "Lionheart777&2018";
        $mail->Username = $cuenta_correo;
        $mail->Password = $password_correo;
        $mail->From = "orbe.robot@gmail.com";
        $mail->FromName = "Orbe Solutions";
        $mail->Subject = "Factura Cancelada";
        $mail->AddReplyTo($cuenta_correo);

        $mensaje = $contenido;

        $mail->Body = $mensaje;
        $mail->IsHTML(true);
        
        //agremamos los archivos adjuntos, pero primero comprobamos que existan
        //path, nombre
        $path = "cdfis/".$empresa[0]."/".(($datos[1] == 1)? "I" : "P").$datos[2];
        $pathDos = "cdfis/".$empresa[0]."/ack_".$idFactura.".xml";
        
        @$mail->AddAttachment($pathDos, "Acuse.xml");
        @$mail->AddAttachment($path.".pdf", (($datos[1] == 1)? "I" : "P").$datos[2].".pdf");
        @$mail->AddAttachment($path.".xml", (($datos[1] == 1)? "I" : "P").$datos[2].".xml");
        

        if($this->esEmailValido($datosFacturacionCliente[14])){
            //Envío al cliente
            $mail->clearAddresses();
            $mail->AddAddress($datosFacturacionCliente[14]);
            while(!$mail->Send()){
                sleep(3000);//mientras no se envíe, nos dormimos 5 minutos
            }
        }
        
        //se envía también la factura a la empresa que está emitiendo
        if($this->esEmailValido($datosFacturacionEmpresa[14])){
            //Envío al cliente
            $mail->clearAddresses();
            $mail->AddAddress($datosFacturacionEmpresa[14]);
            while(!$mail->Send()){
                sleep(3000);//mientras no se envíe, nos dormimos 5 minutos
            }
        }
        
        //Envío al programador
        $mail->clearAddresses();
        $mail->AddAddress('"ralpuchev@gmail.com"');
        
        while(!$mail->Send()){
            sleep(3000);//mientras no se envíe, nos dormimos 5 minutos
        }

        return 1;
    }
    
    //esta función envía por correo la notificación de factura
    public function enviaFacturaPorCorreo($idFactura){
        //recuperamos primero los datos de la factura que se habrá de enviar en la notificicación
        $datos = explode("|", $this->getInfoFacturaPorId($idFactura));
        //idFacturaEmitida, tipoFactura, idElemento, UUID, cantidadFacturada, fecha, hora, usuario, estatus, nombreUsuario
        
        $datosElemento = null;
        if($datos[1] == 1) $datosElemento = explode("|", $this->getGastoDeClientePorId($datos[2]));
        else if($datos[1] == 2) $datosElemento = explode("|", $this->getGastoDeClientePorId($datos[2]));
        
        //gasto : idGastoCliente, idPromotor, idEmpresa, idCliente, idBanco, fecha, hora, cantidadTotal, porcentaje, idUsuario, estatus, promotor, empresa, cliente, banco, metodoDePago, idFormaDePago, moneda, tipoCambio, formaPago
        
        $empresa = explode("|", $this->getEmpresaPorId($datosElemento[2]));
        //idEmpresa, nombre, estatus, idDatosFacturacion, noCertificado, fechaVencimiento, textoCertificado
        $datosFacturacionEmpresa = explode("|", $this->getDatosDeFacturacion($empresa[3]));
        //iddatosfacturacion, nombre, rfc, calle, exterior, interior, referencia, colonia, localidad, municipio, estado, pais, cp, telefono, email, fax, cuenta, estatus, claveRegimen, descripcionClave
        
        $cliente = explode("|", $this->getClientePorId($datosElemento[3]));
        //idCliente, nombre, idDatosFacturacion
        $datosFacturacionCliente = explode("|", $this->getDatosDeFacturacion($cliente[2]));
        //iddatosfacturacion, nombre, rfc, calle, exterior, interior, referencia, colonia, localidad, municipio, estado, pais, cp, telefono, email, fax, cuenta, estatus, claveRegimen, descripcionClave
        
        $contenido = "<html>";
        $contenido .= "<body>";
        $contenido .= "<div style='background-color: #ffffff; border.radius: 5px; border:0.5px solid #868786;width: 96%;margin:0 auto 0 auto;text-align: center;'>";
        
        $contenido .= "<table align='center' width='96%' cellpadding='3' cellspacing='0' border='0'>";
        $contenido .= "<tr><td align='left' valign='top'>";
        
        $contenido .= "<br/>";
        
        //$contenido .= "<img src='http://sistema.hospitalmay.com/Imagenes/logo.png' width='200'/>";
        
        $contenido .= "</td></tr><tr><td align='center' valign='top'>";
        
        $contenido .= "<div style='color:#009EE3; font-size:30px;'><b>Notificación de Factura Emitida</b></div><br/>";
        
        $contenido .= "<hr style='width:98%; border: 0; height: 0; border-top: 1px solid rgba(0, 0, 0, 0.1); border-bottom: 1px solid rgba(255, 255, 255, 0.3);'></hr><br/>";
        
        $contenido .= "<div style='font-size:21px; text-align:left;'>Ha recibido una factura adjunta a este correo.</div><br/>";
        
        $contenido .= "<table align='center' cellpadding='6' cellspacing='0' width='100%'><tr><td width='50%' align='center' valign='top'>";
        
        //datos del emisor
        $contenido .= "<div style='color:#009EE3; font-size:21px;'><b>Emisor</b></div><br/>";
        $contenido .= "<div style='font-size:21px; text-align:left;'>".$datosFacturacionEmpresa[1]."</div>";
        $contenido .= "<div style='font-size:21px; text-align:left;'>".$datosFacturacionEmpresa[2]."</div>";
        
        $contenido .= "</td><td width='50%' align='center' valign='top'>";
        
        //datos del receptor
        $contenido .= "<div style='color:#009EE3; font-size:21px;'><b>Receptor</b></div><br/>";
        $contenido .= "<div style='font-size:21px; text-align:left;'>".$datosFacturacionCliente[1]."</div>";
        $contenido .= "<div style='font-size:21px; text-align:left;'>".$datosFacturacionCliente[2]."</div>";
        
        $contenido .= "</td></tr></table>";
        $contenido .= "<br/>";
        $contenido .= "<div style='font-size:21px; text-align:left;'>Folio Fiscal: <b>".$datos[3]."</b></div>";
        $contenido .= "<div style='font-size:21px; text-align:left;'>Emitida el <b>".$this->getFechaLegible($datos[5])."</b> a las <b>".$datos[6]."</b></div>";
        
        $contenido .= "<br/>";
        
        $contenido .= "<br/>";
        $contenido .= "<hr style='width:98%; border: 0; height: 0; border-top: 1px solid rgba(0, 0, 0, 0.1); border-bottom: 1px solid rgba(255, 255, 255, 0.3);'></hr><br/>";
        
        $contenido .= "<div style='font-size:16px; text-align:left;'>Este es un mensaje enviado de forma automática.</div><br/>";
        
        $contenido .= "</td></tr></table>";
        $contenido .= "</div>";
        $contenido .= "</body>";
        $contenido .= "</html>";
        
        require_once('phpgmailer/class.phpgmailer.php');
        
        $mail = new PHPGMailer();
        $cuenta_correo = "orbe.robot@gmail.com";
        $password_correo = "Lionheart777&2018";
        $mail->Username = $cuenta_correo;
        $mail->Password = $password_correo;
        $mail->From = "orbe.robot@gmail.com";
        $mail->FromName = "Orbe Solutions";
        $mail->Subject = "Factura Emitida";
        $mail->AddReplyTo($cuenta_correo);

        $mensaje = $contenido;

        $mail->Body = $mensaje;
        $mail->IsHTML(true);
        
        //agremamos los archivos adjuntos, pero primero comprobamos que existan
        //path, nombre
        $path = "cdfis/".$empresa[0]."/".(($datos[1] == 1)? "I" : "P").$datos[2];
        
        @$mail->AddAttachment($path.".pdf", (($datos[1] == 1)? "I" : "P").$datos[2].".pdf");
        @$mail->AddAttachment($path.".xml", (($datos[1] == 1)? "I" : "P").$datos[2].".xml");

        if($this->esEmailValido($datosFacturacionCliente[14])){
            //Envío al cliente
            $mail->clearAddresses();
            $mail->AddAddress($datosFacturacionCliente[14]);
            while(!$mail->Send()){
                sleep(3000);//mientras no se envíe, nos dormimos 5 minutos
            }
        }
        
        //se envía también la factura a la empresa que está emitiendo
        if($this->esEmailValido($datosFacturacionEmpresa[14])){
            //Envío al cliente
            $mail->clearAddresses();
            $mail->AddAddress($datosFacturacionEmpresa[14]);
            while(!$mail->Send()){
                sleep(3000);//mientras no se envíe, nos dormimos 5 minutos
            }
        }
        
        //Envío al programador
        $mail->clearAddresses();
        $mail->AddAddress('"ralpuchev@gmail.com"');
        
        while(!$mail->Send()){
            sleep(3000);//mientras no se envíe, nos dormimos 5 minutos
        }

        return 1;
    }

    //esta función devuelve únicamente el idEmpresa asociado al gasto dado como parámetro
    public function getIdEmpresaDePago($idPago){
        return $this->getResultadoDeConsulta("select idEmpresa from PagosRecibidos where idPagoRecibido = $idPago limit 1;");
    }
    
    //esta función devuelve únicamente el idEmpresa asociado al gasto dado como parámetro
    public function getIdEmpresaDeGasto($idGasto){
        return $this->getResultadoDeConsulta("select idEmpresa from GastosClientes where idGastoCliente = $idGasto limit 1;");
    }
    
    public function getDatosFacturacionPorTipoAndElemento($idElemento, $tipoElemento){
        return $this->getResultadoDeConsulta("select idFacturaEmitida, tipoFactura, idElemento, UUID, cantidadFacturada, fecha, hora, usuario, estatus, (select nombreCompleto from Usuarios where Usuarios.idUsuario = FacturasEmitidas.usuario limit 1) as nombreUsuario, idEmpresa, numeroConsecutivo from FacturasEmitidas where idElemento = $idElemento and tipoFactura = $tipoElemento  limit 1;");
        //idFacturaEmitida, tipoFactura, idElemento, UUID, cantidadFacturada, fecha, hora, usuario, estatus, nombreUsuario, idEmpresa, numeroConsecutivo
    }
    
    //esta función recupera los datos de una factura dada por id
    public function getInfoFacturaPorId($idFactura){
        return $this->getResultadoDeConsulta("select idFacturaEmitida, tipoFactura, idElemento, UUID, cantidadFacturada, fecha, hora, usuario, estatus, (select nombreCompleto from Usuarios where Usuarios.idUsuario = FacturasEmitidas.usuario limit 1) as nombreUsuario, idEmpresa, numeroConsecutivo from FacturasEmitidas where idFacturaEmitida = $idFactura limit 1;");
        //idFacturaEmitida, tipoFactura, idElemento, UUID, cantidadFacturada, fecha, hora, usuario, estatus, nombreUsuario
    }
    
    //esta función genera un número consecutivo para una empresa y un tipo de elemento.
    public function getNumeroConsecutivoSiguienteParaEmpresaPorTipoDeFactura($idEmpresa, $tipo){
        return $this->getResultadoDeConsulta("select count(*) from FacturasEmitidas where idEmpresa = $idEmpresa and tipoFactura = $tipo")+1;
    }
    
    //esta función guarda una nueva emisión de factura
    public function guardaFactura($idFactura, $tipoElemento, $idElemento, $uuid, $cantidad, $idEmpresa){
        if($idFactura == 0){
            //el número consecutivo, deberá generarse a partir de la empresa.
            $numeroConsecutivo = $this->getNumeroConsecutivoSiguienteParaEmpresaPorTipoDeFactura($idEmpresa, $tipoElemento);
            
            if($this->insertaNuevoRegistro("INSERT INTO FacturasEmitidas
(idFacturaEmitida, tipoFactura, idElemento, UUID, cantidadFacturada, fecha, hora, usuario, idEmpresa, numeroConsecutivo, estatus)VALUES(0, $tipoElemento, $idElemento, '$uuid', '$cantidad', '".$this->getFechaActual()."', '".$this->getHoraActual()."', ".$_SESSION['idusuario'].", $idEmpresa, $numeroConsecutivo, 1);", 35, "Factura Apartada", "") > 0) return $numeroConsecutivo;
            else return 0; //0 quiere decir que hubo algún error al guardar la factura
        }
        else return $this->actualizaRegistro("update FacturasEmitidas set UUID='$uuid', cantidadFacturada='$cantidad', fecha='".$this->getFechaActual()."', hora='".$this->getHoraActual()."', usuario=".$_SESSION['idusuario']." where idFacturaEmitida=$idFactura limit 1;", 36, "Factura Actualizada: $idFactura", $this->getInfoFacturaPorId($idFactura));
    }
    
    //esta función recupera el registro previo de un gasto si es que existe, en otro caso devolverá vacío
    public function getRegistroPrevioDeFacturaEmitida($tipoElemento, $idElemento, $idEmpresa){
        return $this->getResultadoDeConsulta("select numeroConsecutivo from FacturasEmitidas where tipoFactura = $tipoElemento and idElemento = $idElemento and UUID = -1 and idEmpresa = $idEmpresa limit 1;");
    }
    
    public function getFacturasEmitidasPorTipoElemento($idElemento, $tipoElemento){
        return $this->getResultadosDeConsulta("select idFacturaEmitida, (select nombreCompleto from Usuarios where Usuarios.idUsuario = FacturasEmitidas.usuario limit 1) as nombreUsuario, fecha, hora, cantidadFacturada, estatus, tipoFactura, idElemento from FacturasEmitidas where tipoFactura = $tipoElemento and idElemento = $idElemento and UUID != -1");
        return $this->query;
        //idFacturaEmitida, nombreUsuario, cantidadFacturada, fecha, hora, estatus, tipoFactura, idElemento
    }
    
    //esta función recupera los pagos recibidos de un rango de tiempo establecido
    public function getPagosRecibidosDeFecha($fechaInicio, $fechaFin, $idCliente, $idEmpresa){
        $query = "select idPagoRecibido, (select nombre from Empresas where idEmpresa = PagosRecibidos.idEmpresa limit 1) as nombreEmpresa, (select nombre from Clientes where idCliente = PagosRecibidos.idCliente) as nombreCliente, fecha, hora, cantidadPagada from PagosRecibidos ";
        
        $whereAgreagado = false;
        if($fechaInicio != "" && $fechaFin != ""){
            //esto quiere decir que no se aplicará filtro de fechas
            $query .= " where fecha between '".$fechaInicio."' and '".$fechaFin."' ";
            $whereAgreagado = true;
        }
        
        if($idEmpresa != 0){
            if(!$whereAgreagado){
                $query .= " where ";
                $whereAgreagado = true;
            }
            
            $query .= " and idEmpresa = ".$idEmpresa." ";
        }
        
        if($idCliente != 0){
            if(!$whereAgreagado){
                $query .= " where ";
                $whereAgreagado = true;
            }
            
            $query .= " and idCliente = ".$idCliente." ";
        }
        
        if(!$whereAgreagado){
            $query .= " where estatus > 0";
        }else {
            $query .= " and estatus > 0";
        }
        
        return $this->getResultadosDeConsulta($query);
        //idPagoRecibido, nombreEmpresa, nombreCliente, fecha, hora, cantidadPagada
    }
    
    //esta función recupera la descripcion del banco sat a partir del idBanco
    public function getRFCBancoSATPorId($idBanco){
        $datos = explode("^", $this->getBancosSat());
        array_pop($datos);
        
        foreach($datos as $dato){
            $dato = explode("|", $dato);
            if($dato[1] == $idBanco) return $dato[2];
        }
        return "No encontrado";
    }
    
    //esta función recupera el nombre del banco a partir de su RFC
    public function getNombreDelBancoPorRFC($rfc){
        $datos = explode("^", $this->getBancosSat());
        array_pop($datos);
        
        foreach($datos as $dato){
            $dato = explode("|", $dato);
            if($dato[2] == $rfc) return $dato[3];
        }
        return "No encontrado";
    }
    
    //esta función recupera el RFC del banco dado por ID
    public function getDescripcionBancoSATPorId($idBanco){
        $datos = explode("^", $this->getBancosSat());
        array_pop($datos);
        
        foreach($datos as $dato){
            $dato = explode("|", $dato);
            if($dato[1] == $idBanco) return $dato[3];
        }
        return "No encontrado";
    }
    
    //esta función recupera la descripcion de la forma de pago a partir del idFormaPago
    public function getDescripcionMonedaSATPorId($idMoneda){
        $datos = explode("^", $this->getMonedasSat());
        array_pop($datos);
        
        foreach($datos as $dato){
            $dato = explode("|", $dato);
            if($dato[1] == $idMoneda) return $dato[2];
        }
        return "No encontrado";
    }
    
    //esta función recupera si una forma de pago es bancarizada o no
    public function esBancarizadaLaFormaPagoSATPorId($idFormaPago){
        $datos = explode("^", $this->getFormaPagoSat());
        array_pop($datos);
        
        foreach($datos as $dato){
            $dato = explode("|", $dato);
            if($dato[1] == $idFormaPago){
                if($dato[3] == "No") return false;
                else return true;
            }
        }
        return "No encontrado";
    }
    
    //esta función recupera la descripcion de la forma de pago a partir del idFormaPago
    public function getDescripcionFormaPagoSATPorId($idFormaPago){
        $datos = explode("^", $this->getFormaPagoSat());
        array_pop($datos);
        
        foreach($datos as $dato){
            $dato = explode("|", $dato);
            if($dato[1] == $idFormaPago) return $dato[2];
        }
        return "No encontrado";
    }
    
    //esta función recupera la información de un pago recibido a partir de su idPagoRecibido
    public function getInfoPagoRecibido($idPago){
        $datos = $this->getResultadoDeConsulta("select idPagoRecibido, idEmpresa, idCliente, fecha, hora, formaPago, moneda, cantidadPagada, tipoCambio, bancoOrigen, cuentaOrigen, bancoReceptor, cuentaReceptor, numeroOperacion, estatus, (select nombre from Empresas where idEmpresa = PagosRecibidos.idEmpresa limit 1) as nombreEmpresa, (select nombre from Clientes where idCliente = PagosRecibidos.idCliente) as nombreCliente from PagosRecibidos where idPagoRecibido=$idPago limit 1;");
        $partes = explode("|", $datos);
        
        return $datos."|".$this->getDescripcionFormaPagoSATPorId($partes[5])."|".$this->getDescripcionMonedaSATPorId($partes[6])."|".$this->getDescripcionBancoSATPorId($partes[9])."|".$this->getDescripcionBancoSATPorId($partes[11])."|".$this->esBancarizadaLaFormaPagoSATPorId($partes[5]);
        //idPagoRecibido, idEmpresa, idCliente, fecha, hora, formaPago, moneda, cantidadPagada, tipoCambio, bancoOrigen, cuentaOrigen, bancoReceptor, cuentaReceptor, numeroOperacion, estatus, nombreEmpresa, nombreCliente, nombreFormaPago, nombreMoneda, nombreBancoOrigen, nombreBancoReceptor, esBancarizado
    }
    
    //esta función guarda la información de un pago recibido
    public function guardaPagoRecibido($info){
        $info = explode("|", $info);
        if($info[0] == 0){
            //guardamos un nuevo pago recibido
            return $this->insertaNuevoRegistro("INSERT INTO PagosRecibidos
(idPagoRecibido, idEmpresa, idCliente, fecha, hora, formaPago, moneda, cantidadPagada, tipoCambio, bancoOrigen, cuentaOrigen, bancoReceptor, cuentaReceptor, numeroOperacion, estatus) VALUES (0, '".$info[1]."', '".$info[2]."', '".$info[3]."', '".$info[4]."', '".$info[5]."', '".$info[6]."', '".$info[7]."', '".$info[8]."', '".$info[9]."', '".$info[10]."', '".$info[11]."', '".$info[12]."', '".$info[13]."', 1);
", 32, "Nuevo pago recibido guardado", "");
        }else{
            //actualizamos la informacion de un pago recibido
            return $this->actualizaRegistro("update PagosRecibidos set idEmpresa='".$info[1]."', idCliente='".$info[2]."', fecha='".$info[3]."', hora='".$info[4]."', formaPago='".$info[5]."', moneda='".$info[6]."', cantidadPagada='".$info[7]."', tipoCambio='".$info[8]."', bancoOrigen='".$info[9]."', cuentaOrigen='".$info[10]."', bancoReceptor='".$info[11]."', cuentaReceptor='".$info[12]."', numeroOperacion='".$info[13]."' where idPagoRecibido=".$info[0]." limit 1;", 33, "Pago recibido actualizado", $this->getInfoPagoRecibido($info[0]));
        }
    }
    
    //esta función recupera los bancos sat con el rfc y clave de institución
    public function getBancosSat(){
        //se recupera la información desde el catálogo de productos y servicios
        $pathArchivo = "catalogosFacturacion" . DIRECTORY_SEPARATOR . "bancos.csv";
        $lineas = $this->leeArchivoCSVGenerico($pathArchivo, true);
        $bancos = "";
        
        $i = 1;
        foreach($lineas as $linea){
            $bancos .= $i."|".$linea[0]."|".$linea[1]."|".$linea[2]."^";
            $i++;
        }
        return $bancos;
    }
    
    //esta función recupera las monedas del catálogo sat
    public function getMonedasSat(){
        //se recupera la información desde el catálogo de productos y servicios
        $pathArchivo = "catalogosFacturacion" . DIRECTORY_SEPARATOR . "moneda.csv";
        $lineas = $this->leeArchivoCSVGenerico($pathArchivo, true);
        $monedas = "";
        
        $i = 1;
        foreach($lineas as $linea){
            $monedas .= $i."|".$linea[0]."|".$linea[1]."^";
            $i++;
        }
        return $monedas;
    }
    
    //esta función recupera las formas de pago del catálogo SAT
    public function getFormaPagoSat(){
        //se recupera la información desde el catálogo de productos y servicios
        $pathArchivo = "catalogosFacturacion" . DIRECTORY_SEPARATOR . "FormaPago.csv";
        $lineas = $this->leeArchivoCSVGenerico($pathArchivo, true);
        $formasPago = "";
        
        $i = 1;
        foreach($lineas as $linea){
            $formasPago .= $i."|".$linea[0]."|".$linea[1]."|".$linea[2]."^";
            $i++;
        }
        return $formasPago;
    }
    
    //esta función recupera el idDatosFacturacion a partir del rfc
    public function getIdDatosFacturacionDeRFC($rfc){
        return $this->getResultadoDeConsulta("select idDatosFacturacion from DatosFacturacion where rfc = '$rfc' limit 1;");
    }
    
    //esta función recupera los regimenes fiscales del catálogo SAT
    public function getRegimenesFiscales(){
        //se recupera la información desde el catálogo de productos y servicios
        $pathArchivo = "catalogosFacturacion" . DIRECTORY_SEPARATOR . "regimenes.csv";
        $lineas = $this->leeArchivoCSVGenerico($pathArchivo, true);
        $regimenes = "";
        
        $i = 1;
        foreach($lineas as $linea){
            $regimenes .= $i."|".$linea[0]."|".$linea[1]."^";
            $i++;
        }
        return $regimenes;
    }
    
    //elimina un concepto de facturación de un gasto
    public function eliminaConceptoDeFacturacionDeGasto($idConcepto){
        $query = "delete from ConceptosFacturacion where idConceptoFacturacion = $idConcepto limit 1;";
        //return $query;
        return $this->actualizaRegistro($query, 31, "Concepto de Facturación de Gasto eliminado", $this->getConceptoDeFacturacionPorId($idConcepto));
    }
    
    //esta función recupera la lista de conceptos de facturación de un gasto específico
    public function getConceptosDeGasto($idGasto){
        //Unidad", "Producto", "Descripción", "P. Unitario", "Cantidad", "Total"
        $conceptos = explode("^", $this->getResultadosDeConsulta("select idConceptoFacturacion, claveProductoServicio, claveUnidad, descripcion, valorUnitario, cantidad, descuento from ConceptosFacturacion where idGasto = $idGasto"));
        //return $this->query;
        array_pop($conceptos);
        
        //para cada concepto, recuperamos la descripción de producto y unidad
        $conceptosDeGasto = "";
        foreach($conceptos as $concepto){
            $datos = explode("|", $concepto);
            
            $conceptosDeGasto .= $datos[0]."|".$this->getDescripcionUnidadSATPorId($datos[2])."|".$this->getDescripcionProductosAndServiciosSATPorId($datos[1])."|".$datos[3]."|".$datos[4]."|".$datos[5]."|".$datos[6]."|$".$this->getCantidadConSoloDosDecimales(($datos[4] * $datos[5]) - $datos[6])."|".$datos[2]."|".$datos[1]."^";
            
            //idConcepto, nombreUnidad, nombreProducto, descripcion, precioUnitario, cantidad, descuento, total, idUnidad, idProducto
        }
        
        return $conceptosDeGasto;
    }
    
    //esta función guarda un concepto de facturación y lo agrega a un concepto de gastos
    public function guardaConceptoDeFacturacionEnConceptosDeGasto($idConcepto, $idGasto){
        //recupera los datos del concepto global
        $info = $this->getResultadoDeConsulta("select 0, claveProductoServicio, claveUnidad, descripcion, valorUnitario, cantidad, descuento from ConceptosFacturacion where idConceptoFacturacion = $idConcepto limit 1;");
        //idConceptoFacturacion, idProductoServicio, idUnidad, descripcion, campoValorUnitario, campoCantidad, campoDescuento, idGasto
        
        //guardamos este concepto nuevamente pero agregando el idGasto, con lo cual estamos indicando que ahora este concepto es específico de un gasto identificado por el idGasto
        return $this->guardaConceptoDeFacturacion($info."|".$idGasto);
    }
    
    //esta función recupera los conceptos de facturación usados previamente
    public function getConceptosDeFacturacion(){
        return $this->getResultadosDeConsulta("select idConceptoFacturacion, claveProductoServicio, claveUnidad, descripcion, valorUnitario, cantidad, descuento, estatus from ConceptosFacturacion where estatus = 1 and idGasto = 0;");
        //idConceptoFacturacion, claveProductoServicio, claveUnidad, descripcion, valorUnitario, cantidad, descuento, estatus
    }
    
    //recuperamos los datos de un concepto de facturacion
    public function getConceptoDeFacturacionPorId($idConcepto){
        $concepto = $this->getResultadoDeConsulta("select idConceptoFacturacion, claveProductoServicio, claveUnidad, descripcion, valorUnitario, cantidad, descuento, idGasto from ConceptosFacturacion where idConceptoFacturacion = $idConcepto limit 1;");
        $datos = explode("|", $concepto);
        
        return $concepto."|".$this->getDescripcionUnidadSATPorId($datos[2])."|".$this->getDescripcionProductosAndServiciosSATPorId($datos[1]);
        
        //idConceptoFacturacion, claveProductoServicio, claveUnidad, descripcion, valorUnitario, cantidad, descuento, idGasto, descripcionUnidad, descripcionProductos
    }
    
    //esta función guarda un concepto de facturación o lo actualiza si ya existe
    public function guardaConceptoDeFacturacion($info){
        $info = explode("|", $info);
        //idConceptoFacturacion, idProductoServicio, idUnidad, descripcion, campoValorUnitario, campoCantidad, campoDescuento, idGasto
        
        if($info[0] == 0){
            //se agrega un nuevo registro de conceptos de facturación
            return $this->insertaNuevoRegistro("INSERT INTO ConceptosFacturacion
(idConceptoFacturacion, claveProductoServicio, claveUnidad, idGasto, descripcion, valorUnitario, cantidad, descuento, estatus)
VALUES(0, '".$info[1]."', '".$info[2]."', '".$info[7]."', '".$info[3]."', '".$info[4]."', '".$info[5]."', '".$info[6]."', 1);", 29, "Concepto de facturación agregado: ".$info[3], "");
        }else{
            //se actualiza un registro de conceptos de facturación
            $query = "UPDATE ConceptosFacturacion SET claveProductoServicio='".$info[1]."', claveUnidad='".$info[2]."', descripcion='".$info[3]."', valorUnitario='".$info[4]."', cantidad='".$info[5]."', descuento='".$info[6]."' WHERE idConceptoFacturacion=".$info[0]." limit 1;";
            return $this->actualizaRegistro($query, 30, "Concepto de facturación modificado: ".$info[3], $this->getConceptoDeFacturacionPorId($info[0]));
        }
    }
    
    //esta función recupera la descripción de la unidad sat a partir del id
    public function getDescripcionUnidadSATPorId($idUnidad){
        $unidades = explode("^", $this->getUnidadesSAT());
        array_pop($unidades);
        
        foreach($unidades as $unidad){
            $unidad = explode("|", $unidad);
            if($unidad[1] == $idUnidad) return $unidad[2];
        }
        return "No encontrado";
    }
    
    //esta función recupera el catálogo de unidades del sat
    public function getUnidadesSAT(){
        //se recupera la información desde el catálogo de productos y servicios
        $pathArchivo = "catalogosFacturacion" . DIRECTORY_SEPARATOR . "unidades.csv";
        $lineas = $this->leeArchivoCSVGenerico($pathArchivo, true);
        $unidades = "";
        
        $i = 1;
        foreach($lineas as $linea){
            $unidades .= $i."|".$linea[0]."|".$linea[1]."^";
            $i++;
        }
        return $unidades;
    }
    
    //recuperamos la descripción de la clave de productos y servicios a partir del id
    public function getDescripcionProductosAndServiciosSATPorId($idProducto){
        $productos = explode("^", $this->getProductosAndServiciosSAT());
        array_pop($productos);
        
        foreach($productos as $producto){
            $producto = explode("|", $producto);
            if($producto[1] == $idProducto) return $producto[2];
        }
        return "No encontrado";
    }
    
    //esta función recupera el catálogo de productos y servicios del sat
    public function getProductosAndServiciosSAT(){
        //se recupera la información desde el catálogo de productos y servicios
        $pathArchivo = "catalogosFacturacion" . DIRECTORY_SEPARATOR . "productos.csv";
        $lineas = $this->leeArchivoCSVGenerico($pathArchivo, true);
        $productos = "";
        
        $i = 1;
        foreach($lineas as $linea){
            @$productos .= $i."|".$linea[0]."|".$linea[1]."^";
            $i++;
        }
        return $productos;
    }
    
    public function getGastosDePromotor($fechaInicio, $fechaFin, $idpromotor){
        if($idpromotor == 0) return $this->getResultadosDeConsulta("select idGastoCliente, (select nombreCompleto from Usuarios where Usuarios.idUsuario = GastosClientes.idPromotor limit 1) as promotor, (select nombre from Clientes where Clientes.idCliente = GastosClientes.idCliente limit 1) as cliente, (select nombre from Bancos where Bancos.idBanco = GastosClientes.idBanco) as banco, fecha, concat('$',FORMAT(cantidadTotal, 2)), porcentaje, concat('$',FORMAT((cantidadTotal * porcentaje), 2)) from GastosClientes where estatus>0 and fecha >= '$fechaInicio' and fecha <= '$fechaFin'");
        else return $this->getResultadosDeConsulta("select idGastoCliente, (select nombreCompleto from Usuarios where Usuarios.idUsuario = GastosClientes.idPromotor limit 1) as promotor, (select nombre from Clientes where Clientes.idCliente = GastosClientes.idCliente limit 1) as cliente, (select nombre from Bancos where Bancos.idBanco = GastosClientes.idBanco) as banco, fecha, concat('$',FORMAT(cantidadTotal, 2)), porcentaje, concat('$',FORMAT((cantidadTotal * porcentaje), 2)) from GastosClientes where estatus>0 and fecha >= '$fechaInicio' and fecha <= '$fechaFin' and idPromotor = $idpromotor");
        //idGastoCliente, nombrePromotor, nombreCliente, nombreBanco, fecha, total, porcentaje, beneficio
    }
    
    public function cambiaEstatusDeGasto($idgasto, $estatus){
        return $this->actualizaRegistro("UPDATE GastosClientes set estatus = $estatus where idGastoCliente = $idgasto limit 1;", 27, "Gasto de Cliente Modificado", $this->getGastoDeClientePorId($idgasto));
    }
    
    //esta funcion recupera la lista de gastos de clientes pendientes de facturar
    public function getGastosPendientesPorFacturar(){
        return $this->getResultadosDeConsulta("select idGastoCliente, (select nombre from Empresas where Empresas.idEmpresa = GastosClientes.idEmpresa limit 1) as empresa, (select nombre from Clientes where Clientes.idCliente = GastosClientes.idCliente limit 1) as cliente, (select nombre from Bancos where Bancos.idBanco = GastosClientes.idBanco) as banco, fecha, concat('$',FORMAT(cantidadTotal, 2)), porcentaje, estatus from GastosClientes where estatus=1");
        //"Empresa", "Cliente", "Banco", "Fecha", "Cantidad", "Porcentaje", "Estatus"
    }
    
    //funcion para eliminar un gasto
    public function eliminaGastoPorId($idgasto){
        return $this->actualizaRegistro("UPDATE GastosClientes set estatus = 0 where idGastoCliente = $idgasto limit 1;", 28, "Gasto de Cliente Eliminado", $this->getGastoDeClientePorId($idgasto));
    }
    
    //recupera el gasto de un cliente por id
    public function getGastoDeClientePorId($idcliente){
        $datosRaw = $this->getResultadoDeConsulta("select idGastoCliente, idPromotor, idEmpresa, idCliente, idBanco, fecha, hora, cantidadTotal, porcentaje, idUsuario, estatus, (select nombreCompleto from Usuarios where Usuarios.idUsuario = GastosClientes.idPromotor limit 1) as promotor, (select nombre from Empresas where Empresas.idEmpresa = GastosClientes.idEmpresa limit 1) as empresa, (select nombre from Clientes where Clientes.idCliente = GastosClientes.idCliente limit 1) as cliente, (select nombre from Bancos where Bancos.idBanco = GastosClientes.idBanco) as banco, metodoDePago, formaDePago, moneda, tipoCambio, usoCFDI from GastosClientes where idGastoCliente=$idcliente limit 1;");
        $datos = explode("|", $datosRaw);
        //return idGastoCliente, idPromotor, idEmpresa, idCliente, idBanco, fecha, hora, cantidadTotal, porcentaje, idUsuario, estatus, promotor, empresa, cliente, banco, metodoDePago, idFormaDePago, moneda, tipoCambio
        
        //Verificamos la existencia del archivo xml de facturacion
        $pathCarpeta = "cdfis/".$datos[2]."/I".$datos[0].".xml";
        $existeFacturaXML = 0;
        if(file_exists($pathCarpeta)) $existeFacturaXML = 1;
        
        return $datosRaw."|".$this->getDescripcionFormaPagoSATPorId($datos[16])."|".$existeFacturaXML."|".$this->getDescripcionDeUsoCFDI($datos[19]);
        //return idGastoCliente, idPromotor, idEmpresa, idCliente, idBanco, fecha, hora, cantidadTotal, porcentaje, idUsuario, estatus, promotor, empresa, cliente, banco, metodoDePago, idFormaDePago, moneda, tipoCambio, usoCFDI, formaPago, tieneXML, descripcionUsoCFDI
    }
    
    //guardar un gasto de cliente
    public function guardaGastoDeCliente($info){
        //idGasto, idCliente, promotor, idEmpresa, idbanco, cantidad, porcentaje, fecha, estatus (1 pendiente de facturacion, 2 ya facturada), método de pago, forma de pago, moneda, tipo de cambio, uso cfdi
        $info = explode("|", $info);
        
        if($info[0] == 0){
            return $this->insertaNuevoRegistro("INSERT INTO `GastosClientes` (`idGastoCliente`, `idPromotor`, `idEmpresa`, `idCliente`, `idBanco`, `fecha`, `hora`, `cantidadTotal`, `porcentaje`, `idUsuario`, `metodoDePago`, `formaDePago`, `moneda`, `tipoCambio`, `usoCFDI`,  `estatus`) VALUES (0, ".$info[2].", ".$info[3].", ".$info[1].", ".$info[4].", '".$info[7]."', '".$this->getHoraActual()."', '".$info[5]."', '".$info[6]."', '".$_SESSION['idusuario']."', '".$info[9]."', '".$info[10]."', '".$info[11]."', '".$info[12]."', '".$info[13]."', ".$info[8].");", 26, "Gasto de Cliente agregado", "");
        }else{
            return $this->actualizaRegistro("UPDATE GastosClientes set idPromotor=".$info[2].", idEmpresa=".$info[3].", idCliente=".$info[1].", idBanco=".$info[4].", fecha='".$info[7]."', cantidadTotal='".$info[5]."', porcentaje='".$info[6]."', estatus=".$info[8].", metodoDePago='".$info[9]."', formaDePago='".$info[10]."', moneda='".$info[11]."', tipoCambio='".$info[12]."', usoCFDI = '".$info[13]."' where idGastoCliente=".$info[0]." limit 1;", 27, "Gasto de Cliente Actualizado", $this->getGastoDeClientePorId($info[0]));
        }
    }
    
    //recupera los gastos de cliente
    public function getGastosDeCliente($fechaInicio, $fechaFin, $idcliente){
        if($idcliente == 0) return $this->getResultadosDeConsulta("select idGastoCliente, (select nombreCompleto from Usuarios where Usuarios.idUsuario = GastosClientes.idPromotor limit 1) as promotor, (select nombre from Empresas where Empresas.idEmpresa = GastosClientes.idEmpresa limit 1) as empresa, (select nombre from Clientes where Clientes.idCliente = GastosClientes.idCliente limit 1) as cliente, (select nombre from Bancos where Bancos.idBanco = GastosClientes.idBanco) as banco, fecha, concat('$',FORMAT(cantidadTotal, 2)), porcentaje, estatus from GastosClientes where estatus>0 and fecha >= '$fechaInicio' and fecha <= '$fechaFin'");
        else return $this->getResultadosDeConsulta("select idGastoCliente, (select nombreCompleto from Usuarios where Usuarios.idUsuario = GastosClientes.idPromotor limit 1) as promotor, (select nombre from Empresas where Empresas.idEmpresa = GastosClientes.idEmpresa limit 1) as empresa, (select nombre from Clientes where Clientes.idCliente = GastosClientes.idCliente limit 1) as cliente, (select nombre from Bancos where Bancos.idBanco = GastosClientes.idBanco) as banco, fecha, concat('$',FORMAT(cantidadTotal, 2)), porcentaje, estatus from GastosClientes where estatus>0 and fecha >= '$fechaInicio' and fecha <= '$fechaFin' and idCliente = $idcliente");
        //idGastoCliente, nombrePromotor, nombreEmpresa, nombreCliente, nombreBanco, fecha, hora, total, porcentaje, estatus
    }
    
    //eliminamos un gasto interno
    public function eliminaGastoInterno($idGastoInterno){
        return $this->actualizaRegistro("UPDATE GastosInternos set estatus=0 where idGastosInterno=".$idGastoInterno." limit 1;", 25, "Gasto Interno Eliminado: ".$idGastoInterno, $this->getGastoInternoPorId($idGastoInterno));
    }
    
    //recuperamos un gasto interno por id
    public function getGastoInternoPorId($idgastointerno){
        return $this->getResultadoDeConsulta("select idGastosInterno, concepto, fecha, hora, cantidad, idUsuario, estatus from GastosInternos where idGastosInterno = $idgastointerno limit 1;");
    }
    
    //guarda un gasto interno
    public function guardaGastoInterno($info){
        $info = explode("|", $info);
        //idGasto, fecha, hora, concepto, cantidad
        
        if($info[0] == 0){
            return $this->insertaNuevoRegistro("INSERT INTO `GastosInternos` (`idGastosInterno`, `concepto`, `fecha`, `hora`, `cantidad`, `idUsuario`, `estatus`) VALUES (0, '".$info[3]."', '".$info[1]."', '".$info[2]."', '".$info[4]."', ".$_SESSION['idusuario'].", 1);", 23, "Gasto Interno agregado: ".$info[3], "");
        }else{
            return $this->actualizaRegistro("UPDATE GastosInternos set concepto='".$info[3]."', fecha='".$info[1]."', hora='".$info[2]."', cantidad='".$info[4]."' where idGastosInterno=".$info[0]." limit 1;", 24, "Gasto Interno modificado: ".$info[3], $this->getGastoInternoPorId($info[0]));
        }
    }
    
    //elimina un concepto a partir de su idconcepto
    public function eliminaConceptoPorId($idconcepto){
        return $this->actualizaRegistro("UPDATE ConceptosGastosInternos set estatus = 0 where idConcepto=".$idconcepto." limit 1;", 22, "Concepto eliminado: ".$idconcepto, $this->getConceptoPorId($idconcepto));
    }
    
    //recupera un concepto a partir del idconcepto
    public function getConceptoPorId($idconcepto){
        return $this->getResultadoDeConsulta("select idConcepto, descripcion, estatus from ConceptosGastosInternos where idConcepto = $idconcepto limit 1;");
    }
    
    //funcion para guardar un concepto para gasto interno
    public function guardaConcepto($info){
        $info = explode("|", $info);
        //idconcepto, descripcion
        
        if($info[0] == 0){
            return $this->insertaNuevoRegistro("INSERT INTO `ConceptosGastosInternos` (`idConcepto`, `descripcion`, `estatus`) VALUES (0, '".$info[1]."', 1);", 20, "Concepto Agregado: ".$info[1], "");
        }else{
            return $this->actualizaRegistro("UPDATE ConceptosGastosInternos set descripcion = '".$info[1]."' where idConcepto=".$info[0]." limit 1;", 21, "Concepto modificado: ".$info[1], $this->getConceptoPorId($info[0]));
        }
    }
    
    //recupera la lista de conceptos
    public function getListaDeConceptos(){
        return $this->getResultadosDeConsulta("select idConcepto, descripcion from ConceptosGastosInternos where estatus = 1");
        //idconcepto, descripcion
    }
    
    //recupera todos los gastos internos que sean parte del rango de fecha dado
    public function getGastosInternosDeRangoDeFechas($fechaInicio, $fechaFinal){
        return $this->getResultadosDeConsulta("select idGastosInterno, concepto, fecha, hora, (select nombreCompleto from Usuarios where Usuarios.idUsuario = GastosInternos.idUsuario limit 1) as usuario, concat('$',FORMAT((cantidad), 2)) from GastosInternos where fecha >= '$fechaInicio' and fecha <= '$fechaFinal' and estatus = 1");
        //idGastoInterno, concepto, fecha, hora, cantidad, usuario
    }
    
    //esta función recupera la descripción para una clave de regimen sat dada como parámetro
    public function getDescripcionClaveRegimenSat($claveRegimen){
        $regimenes = explode("^", $this->getRegimenesFiscales());
        array_pop($regimenes);
        
        foreach($regimenes as $regimen){
            $regimen = explode("|", $regimen);
            if($regimen[1] == $claveRegimen) return $regimen[2];
        }
        return "No encontrado";
    }
    
    //recupera los datos de facturacion a partir del iddatosfacturacion
    //función que recupera los datos de facturación
    public function getDatosDeFacturacion($iddatos_facturacion){
        $datos = $this->getResultadoDeConsulta("SELECT iddatosfacturacion, nombre, rfc, calle, exterior, interior, referencia, colonia, localidad, municipio, estado, pais, cp, telefono, email, fax, cuenta, estatus, claveRegimen FROM DatosFacturacion where iddatosfacturacion=$iddatos_facturacion limit 1;");
        $datosSeparados = explode("|", $datos);
        
        return $datos."|".$this->getDescripcionClaveRegimenSat($datosSeparados[18]);
        //iddatosfacturacion, nombre, rfc, calle, exterior, interior, referencia, colonia, localidad, municipio, estado, pais, cp, telefono, email, fax, cuenta, estatus, claveRegimen, descripcionClave
    }
    
    //funcion para guardar datos de facturacion
    public function guardaDatosFacturacion($info){
        //iddatos_facturacion, nombre, rfc, calle, exterior, interior, referencia, colonia, localidad, municipio, estado, pais, cp, telefono, email, fax, cuenta, claveRegimen
        $info = explode("|", $info);
        
        if($info[0] == 0){
            $iddatos_facturacion = $this->insertaNuevoRegistro("INSERT INTO `DatosFacturacion`
(`idDatosFacturacion`,`nombre`,`rfc`,`calle`,`exterior`,`interior`,`referencia`,`colonia`,`localidad`,`municipio`,`estado`,`pais`,`cp`,`telefono`,`email`,`fax`,`cuenta`, `claveRegimen`, `estatus`)VALUES(0, '".$info[1]."', '".$info[2]."', '".$info[3]."', '".$info[4]."', '".$info[5]."', '".$info[6]."', '".$info[7]."', '".$info[8]."', '".$info[9]."', '".$info[10]."', '".$info[11]."', '".$info[12]."', '".$info[13]."', '".$info[14]."', '".$info[15]."', '".$info[16]."', '".$info[17]."', 1);", 14, "Datos de facturacion agregados: ".$info[1]." RFC: ".$info[2], "");   
        }else{
            $iddatos_facturacion = $this->actualizaRegistro("update DatosFacturacion set nombre='".$info[1]."', rfc='".$info[2]."', calle='".$info[3]."', exterior='".$info[4]."', interior='".$info[5]."', referencia='".$info[6]."', colonia='".$info[7]."', localidad='".$info[8]."', municipio='".$info[9]."', estado='".$info[10]."', pais='".$info[11]."', cp='".$info[12]."', telefono='".$info[13]."', email='".$info[14]."', fax='".$info[15]."', cuenta='".$info[16]."', claveRegimen='".$info[17]."' where idDatosFacturacion=".$info[0]." limit 1;", 15, "Datos de facturacion actualizados: ".$info[1]." RFC: ".$info[2], $this->getDatosDeFacturacion($info[0]));
        }
        
        return $iddatos_facturacion;
    }
    
    //recuperamos la lista de datos de facturacion
    public function getListaDatosFacturacion(){
        return $this->getResultadosDeConsulta("select iddatosfacturacion, nombre, rfc from DatosFacturacion where estatus=1;");
    }
    
    //funcion para eliminar un cliente
    public function eliminaCliente($idcliente){
        return $this->actualizaRegistro("update `Clientes` set estatus=0 where idCliente=".$idcliente." limit 1;", 19, "Cliente Eliminado: ".$idcliente." por: ".$_SESSION['nombreCompleto'], $this->getClientePorId($idcliente));
    }
    
    //recuperamos un cliente a partir de su id
    public function getClientePorId($idcliente){
        return $this->getResultadoDeConsulta("select idCliente, nombre, idDatosFacturacion from Clientes where idCliente = $idcliente limit 1;");
    }
    
    //guardamos el cliente
    public function guardaCliente($info){
        $info = explode("|", $info);
        
        if($info[0] == 0){
            return $this->insertaNuevoRegistro("INSERT INTO `Clientes`(`idCliente`, `nombre`, `idDatosFacturacion`, `estatus`) VALUES (0, '".$info[1]."', ".$info[2].", 1);", 17, "Cliente agregado al sistema: ".$info[1]." agregado por: ".$_SESSION['nombreCompleto'], "");
        }else{
            //solo actualizamos la info
            return $this->actualizaRegistro("update `Clientes` set nombre='".$info[1]."', idDatosFacturacion=".$info[2]." where idCliente=".$info[0]." limit 1;", 18, "Cliente actualizado: ".$info[1]." por: ".$_SESSION['nombreCompleto'], $this->getClientePorId($info[0]));
        }
    }
    
    //recupera los clientes
    public function getClientes(){
        return $this->getResultadosDeConsulta("select idCliente, nombre, (select rfc from DatosFacturacion where Clientes.idDatosFacturacion = DatosFacturacion.idDatosFacturacion) as razonSocial, estatus from Clientes where estatus = 1;");
        //idCliente, nombre, razonSocial, estatus
    }
    
    //elimina la empresa dada por id
    public function eliminaEmpresa($idEmpresa){
        return $this->actualizaRegistro("update `Empresas` set estatus=0 where idEmpresa = $idEmpresa limit 1;", 13, "Empresa eliminada: ".$idEmpresa." por: ".$_SESSION['nombreCompleto'], $this->getEmpresaPorId($idEmpresa));
    }
    
    //recuperar la info de una empresa
    public function getEmpresaPorId($idEmpresa){
        return $this->getResultadoDeConsulta("select idEmpresa, nombre, estatus, idDatosFacturacion, noCertificado, fechaVencimiento, textoCertificado, passwordPFX from Empresas where idEmpresa = $idEmpresa limit 1;");
        //idEmpresa, nombre, estatus, idDatosFacturacion, noCertificado, fechaVencimiento, textoCertificado, passwordPFX
    }
    
    //guardar / actualizar info de una empresa
    public function guardaEmpresa($info){
        $info = explode("|", $info);
        //idempresa, nombrecompleto, datosFacturacion, noCertificado, fechaDB, certificadoText
        
        if($info[0] == 0){
            return $this->insertaNuevoRegistro("INSERT INTO `Empresas` (`idEmpresa`, `nombre`, `idDatosFacturacion`,`noCertificado`, `fechaVencimiento`, `textoCertificado`, `estatus`) VALUES (0, '".$info[1]."', '".$info[2]."', '".$info[3]."', '".$info[4]."', '".$info[5]."', 1);", 11, "Empresa agregada al sistema: ".$info[1]." agregado por: ".$_SESSION['nombreCompleto'], "");
        }else{
            //solo actualizamos la info
            return $this->actualizaRegistro("UPDATE `Empresas` set nombre='".$info[1]."', idDatosFacturacion = '".$info[2]."', noCertificado='".$info[3]."',  fechaVencimiento='".$info[4]."', textoCertificado='".$info[5]."' where idEmpresa = ".$info[0]." limit 1;", 12, "Empresa actualizada: ".$info[1]." por: ".$_SESSION['nombreCompleto'], $this->getEmpresaPorId($info[0]));
        }
    }
    
    //recuperamos las empresas del sistema
    public function getEmpresas(){
        return $this->getResultadosDeConsulta("select idEmpresa, nombre, estatus from Empresas where estatus = 1;");
    }
    
    public function eliminaBancoPorId($idbanco){
        return $this->actualizaRegistro("update `Bancos` set estatus=0 where idBanco = $idbanco limit 1;", 10, "Banco eliminado: ".$idbanco." por: ".$_SESSION['nombreCompleto'], $this->getBancoPorId($idbanco));
    }
    
    public function getBancoPorId($idbanco){
        return $this->getResultadoDeConsulta("select idBanco, nombre, estatus from Bancos where idBanco=$idbanco limit 1;");
    }
    
    public function guardaBanco($info){
        $info = explode("|", $info);
        
        if($info[0] == 0){
            return $this->insertaNuevoRegistro("INSERT INTO `Bancos` (`idBanco`, `nombre`, `estatus`) VALUES (0, '".$info[1]."', 1);", 8, "Banco agregado al sistema: ".$info[1]." agregado por: ".$_SESSION['nombreCompleto'], "");
        }else{
            //solo actualizamos la info
            return $this->actualizaRegistro("update `Bancos` set nombre='".$info[1]."' where idBanco=".$info[0]." limit 1;", 9, "Banco actualizado: ".$info[1]." por: ".$_SESSION['nombreCompleto'], $this->getBancoPorId($info[0]));
        }
    }
    
    public function getBancos(){
        return $this->getResultadosDeConsulta("select idBanco, nombre from Bancos where estatus = 1");
        //idBanco, nombre
    }
    
    public function eliminaUsuarioPorId($idUsuario){
        return $this->actualizaRegistro("update `Usuarios` set estatus=0 where idUsuario = $idUsuario limit 1;", 7, "Usuario eliminado: ".$idUsuario." por: ".$_SESSION['nombreCompleto'], $this->getUsuarioPorId($idUsuario));
    }
    
    public function getUsuarioPorId($idusuario){
        return $this->getResultadoDeConsulta("select idUsuario, nombreCompleto, idPerfil, nombreUsuario, passwordUsuario, correoElectronico, estatus, (select nombre from Clientes where Clientes.idCliente = Usuarios.nombreCompleto limit 1) as nombreCliente from `Usuarios` where idUsuario = $idusuario limit 1;");
        //idUsuario, nombreCompleto, idPerfil, nombreUsuario, passwordUsuario, correoElectronico, estatus, nombreCliente
    }
    
    //function para guardar a un usuario del sistema nuevo
    public function guardaUsuario($info){
        //idusuario, nombrecompleto | idUsuarioLigado, idperfil, campousuario, campopassword, campocorreo
        $info = explode("|", $info);
        
        //verificamos si el nombre de usuario ya existe. si ya existe se devuelve -1
        $idUsuario = $this->getResultadoDeConsulta("select idUsuario from Usuarios where binary nombreUsuario='".$info[3]."' limit 1;");
        if($idUsuario > 0 && $info[0] != $idUsuario) return -1;
        
        if($info[0] == 0){
            return $this->insertaNuevoRegistro("INSERT INTO `Usuarios` (`idUsuario`, `nombreCompleto`, `idPerfil`, `nombreUsuario`, `passwordUsuario`, `correoElectronico`, `estatus`) VALUES (0, '".$info[1]."', ".$info[2].", '".$info[3]."', '".$info[4]."', '".$info[5]."', 1);", 5, "Usuario agregado al sistema: ".$info[1]." agregado por: ".$_SESSION['nombreCompleto'], "");
        }else{
            //solo actualizamos la info
            return $this->actualizaRegistro("update `Usuarios` set nombreCompleto='".$info[1]."', idPerfil=".$info[2].", nombreUsuario='".$info[3]."', passwordUsuario='".$info[4]."', correoElectronico='".$info[5]."' where idUsuario = ".$info[0]." limit 1;", 6, "Usuario actualizado: ".$info[1]." por: ".$_SESSION['nombreCompleto'], $this->getUsuarioPorId($info[0]));
        }
    }
    
    //recupera la lista de usuarios del sistema que sean distintos de system
    public function getUsuariosDelSistema(){
        $usuarios = explode("^", $this->getResultadosDeConsulta("select idUsuario, nombreCompleto, idPerfil from Usuarios where estatus=1 and idUsuario > 1"));
        array_pop($usuarios);
        $nuevosUsuarios = ""; $perfil = "";
        foreach($usuarios as $usuario){
            $usuario = explode("|", $usuario);
            
            if($usuario[2] == 1) $perfil = "Administrador";
            if($usuario[2] == 2) $perfil = "Facturista";
            if($usuario[2] == 3) $perfil = "Promotor";
            if($usuario[2] == 4) $perfil = "Operaciones";
            if($usuario[2] == 5){
                $perfil = "Cliente";
                $cliente = explode("|", $this->getClientePorId($usuario[1]));
                $usuario[1] = $cliente[1];
            }
            
            /*
            contenido += '<option value="1">Administrador</option>';
            contenido += '<option value="2">Facturista</option>';
            contenido += '<option value="4">Operaciones</option>';
            contenido += '<option value="3">Promotor</option>';
            contenido += '<option value="5">Cliente</option>';
            */
            
            $nuevosUsuarios .= $usuario[0]."|".$usuario[1]."|".$perfil."|".$usuario[2]."^";
            //idusuario, nombreCompleto, perfil, idTipoPerfil
        }
        return $nuevosUsuarios;
    }
    
    /******************FUNCIONES PARA LA ADMINISTRACION DE USUARIOS Y BITACORA*******/
    
    //determina si existe o no un token en la db
    public function getTokenActualDeMovil($token){
        $idseguimiento = $this->getResultadoDeConsulta("select idSeguimiento from SeguimientoUsuariosMovil where tokenSistema='$token' limit 1;");
        if($idseguimiento > 0) return true;
    }
    
    public function getEntornoDelCliente(){
		$ip = $this->getIpDelCliente();
		//si la dirección ip desde donde se hace la petición es la puerta de enlace, entonces es desde fuera de la red
		if($ip == $this->gateway){
			$entorno = array("ip"=>$ip, "local"=>0);
		}else $entorno = array("ip"=>$ip, "local"=>1);
		return $entorno;
	}
	
    public function addSucesoAlaBitacora($tipo_suceso, $accion, $datos_accion){
		//recuperamos el usuario logueado, si no hay recuperamos entonces 0, correspondiente al sistema
		$idusuario = $_SESSION['idusuario'];
		if($idusuario == "") $idusuario = 0;
        
        $ip = "";
		if(isset($_SESSION['ip'])) $ip = $_SESSION['ip'];
			
		$this->setConsulta("INSERT INTO `Bitacora` (`idbitacora`, `idaccion`, `idusuario`, `textoaccion`, `datosaccion`, `fecha`, `hora`, `ip`, `estatus`) VALUES (0, $tipo_suceso, $idusuario, '$accion', '$datos_accion', '".$this->getFechaActual()."', '".$this->getHoraActual()."', '$ip', 1);");
		$this->ejecutaConsulta();
    }
    /*
    1.- inicio de sesion
    2.- error de inicio de sesion
    3.- datos de acceso incorrectos
    4.- no tiene permiso externo al sistema
    5.- usuario del sistema agregado
    6.- usuario actualizado
    7.- usuario eliminado
    8.- banco agregado
    9.- banco modificado
    10.- banco eliminado
    11.- empresa agregada
    12.- empresa modificada
    13.- empresa eliminada
    14.- datos facturacion agregados
    15.- datos facturacion modificados
    16.- datos facturacion eliminados
    17.- cliente agregado
    18.- cliente modificado
    19.- cliente eliminado
    20.- concepto agregado
    21.- concepto modificado
    22.- concepto eliminado
    23.- gasto interno agregado
    24.- gasto interno modificado
    25.- gasto interno eliminado
    26.- gasto cliente agregado
    27.- gasto cliente modificado
    28.- gasto cliente eliminado
    29.- concepto de facturacion agregado
    30.- concepto de facturacion modificado
    31.- concepto de facturacion eliminado de gasto
    32.- pago recibido guardado
    33.- pago recibido actualizado
    34.- pago recibido eliminado
    35.- Factura emitida
    36.- Factura actualizada
    37.- Factura Cancelada
    38.- Gasto de Facturación Eliminado
    39.- Comprobante de Pago Eliminado
    */
    
    //$tipo_inicio = 1 : web, 2 : android
    public function recuperaIdUsuario($nombre, $password, $tipo_inicio){
        $tipo = "Web";
        if($tipo_inicio == 1) $tipo = "Web";
        if($tipo_inicio == 2) $tipo = "Android";
        
        //recuperamos la informacion de la tabla de datos logueo
        $datos = explode("|", $this->getResultadoDeConsulta("select idUsuario, nombreCompleto, correoElectronico, idPerfil from Usuarios where BINARY nombreUsuario='$nombre' and BINARY passwordUsuario='$password' and estatus=1 limit 1;"));
        //return $this->query;
        
        //idusuario|nombre|appaterno|apmaterno
        if(count($datos) == 4){
            //como si se recuperó un usuario válido, entonces realizamos la autenticación en este servidor por que tiene comunicación con el navegador
            $_SESSION['idusuario'] = $datos[0];
            $_SESSION['token'] = $this->getToken();
            $_SESSION['usuario'] = $nombre;
            $_SESSION['password'] = $password;
            $_SESSION['nombreCompleto'] = $datos[1];
            $_SESSION['idPerfil'] = $datos[3];
            $this->addSucesoAlaBitacora(1, "El usuario inició sesión. Nombre de usuario: $nombre", "");
            return $this->getDatosSesionActiva();
        }
        
		//como no hubo ningún usuario logueado, se regresa 0 como resultado, no sin antes agregar el suceso a la bitácora
		$this->addSucesoAlaBitacora(3, "Datos de acceso incorrectos. Usuario: ".$nombre." y password: ".$password, "");
		return "0";
	}
    
    public function getDatosUsuarioPorId($idusuario){
		return $this->getResultadoDeConsulta("select idUsuario, nombreCompleto, correoElectronico from Usuarios where idUsuario=$idusuario limit 1;");
	}
    
    public function getNombreCompletoDeUsuarioPorId($idusuario){
        return $this->getResultadoDeConsulta("select nombrecompleto from Usuarios where idusuario=$idusuario limit 1;");
    }
	
    public function getDatosSesionActiva(){
		//primero recuperamos el idusuario del usuario logueado y que mantiene la sesión activa
		if(isset($_SESSION['idusuario']) && $_SESSION['idusuario'] != 0){
			$idusuario = $_SESSION['idusuario'];
			$token = $_SESSION['token'];
            $nombre = $_SESSION['nombreCompleto'];
            $usuario = $_SESSION['usuario'];
            $permisos = $_SESSION['idPerfil'];
			return "$token|".$idusuario."|".$nombre."|".$usuario."|".$permisos;
            //token|idusuario|nombrecompleto|nombreusuario|permisos
		}
		return 0;	
	}
    
    public function cerrarSesion(){
		$this->addSucesoAlaBitacora(2, "El usuario cerró sesión: ".$this->getNombreCompletoDeUsuarioPorId($_SESSION['idusuario']), $this->getDatosSesionActiva());
		session_destroy();
        return 1;
	}
    /******************FUNCIONES PARA LA ADMINISTRACION DE USUARIOS Y BITACORA*******/
    
    /*! 
      @function num2letras () 
      @abstract Dado un n?mero lo devuelve escrito. 
      @param $num number - N?mero a convertir. 
      @param $fem bool - Forma femenina (true) o no (false). 
      @param $dec bool - Con decimales (true) o no (false). 
      @result string - Devuelve el n?mero escrito en letra. 
    */ 
    public function num2letras($num, $fem = false, $dec = true) { 
       $matuni[2]  = "dos"; 
       $matuni[3]  = "tres"; 
       $matuni[4]  = "cuatro"; 
       $matuni[5]  = "cinco"; 
       $matuni[6]  = "seis"; 
       $matuni[7]  = "siete"; 
       $matuni[8]  = "ocho"; 
       $matuni[9]  = "nueve"; 
       $matuni[10] = "diez"; 
       $matuni[11] = "once"; 
       $matuni[12] = "doce"; 
       $matuni[13] = "trece"; 
       $matuni[14] = "catorce"; 
       $matuni[15] = "quince"; 
       $matuni[16] = "dieciseis"; 
       $matuni[17] = "diecisiete"; 
       $matuni[18] = "dieciocho"; 
       $matuni[19] = "diecinueve"; 
       $matuni[20] = "veinte"; 
       $matunisub[2] = "dos"; 
       $matunisub[3] = "tres"; 
       $matunisub[4] = "cuatro"; 
       $matunisub[5] = "quin"; 
       $matunisub[6] = "seis"; 
       $matunisub[7] = "sete"; 
       $matunisub[8] = "ocho"; 
       $matunisub[9] = "nove"; 

       $matdec[2] = "veint"; 
       $matdec[3] = "treinta"; 
       $matdec[4] = "cuarenta"; 
       $matdec[5] = "cincuenta"; 
       $matdec[6] = "sesenta"; 
       $matdec[7] = "setenta"; 
       $matdec[8] = "ochenta"; 
       $matdec[9] = "noventa"; 
       $matsub[3]  = 'mill'; 
       $matsub[5]  = 'bill'; 
       $matsub[7]  = 'mill'; 
       $matsub[9]  = 'trill'; 
       $matsub[11] = 'mill'; 
       $matsub[13] = 'bill'; 
       $matsub[15] = 'mill'; 
       $matmil[4]  = 'millones'; 
       $matmil[6]  = 'billones'; 
       $matmil[7]  = 'de billones'; 
       $matmil[8]  = 'millones de billones'; 
       $matmil[10] = 'trillones'; 
       $matmil[11] = 'de trillones'; 
       $matmil[12] = 'millones de trillones'; 
       $matmil[13] = 'de trillones'; 
       $matmil[14] = 'billones de trillones'; 
       $matmil[15] = 'de billones de trillones'; 
       $matmil[16] = 'millones de billones de trillones'; 

       //Zi hack
       $float=explode('.',$num);
       $num=$float[0];

       $num = trim((string)@$num); 
       if ($num[0] == '-') { 
          $neg = 'menos '; 
          $num = substr($num, 1); 
       }else 
          $neg = ''; 
       while ($num[0] == '0') $num = substr($num, 1); 
       if ($num[0] < '1' or $num[0] > 9) $num = '0' . $num; 
       $zeros = true; 
       $punt = false; 
       $ent = ''; 
       $fra = ''; 
       for ($c = 0; $c < strlen($num); $c++) { 
          $n = $num[$c]; 
          if (! (strpos(".,'''", $n) === false)) { 
             if ($punt) break; 
             else{ 
                $punt = true; 
                continue; 
             } 

          }elseif (! (strpos('0123456789', $n) === false)) { 
             if ($punt) { 
                if ($n != '0') $zeros = false; 
                $fra .= $n; 
             }else 

                $ent .= $n; 
          }else 

             break; 

       } 
       $ent = '     ' . $ent; 
       if ($dec and $fra and ! $zeros) { 
          $fin = ' coma'; 
          for ($n = 0; $n < strlen($fra); $n++) { 
             if (($s = $fra[$n]) == '0') 
                $fin .= ' cero'; 
             elseif ($s == '1') 
                $fin .= $fem ? ' una' : ' un'; 
             else 
                $fin .= ' ' . $matuni[$s]; 
          } 
       }else 
          $fin = ''; 
       if ((int)$ent === 0) return 'Cero ' . $fin; 
       $tex = ''; 
       $sub = 0; 
       $mils = 0; 
       $neutro = false; 
       while ( ($num = substr($ent, -3)) != '   ') { 
          $ent = substr($ent, 0, -3); 
          if (++$sub < 3 and $fem) { 
             $matuni[1] = 'una'; 
             $subcent = 'as'; 
          }else{ 
             $matuni[1] = $neutro ? 'un' : 'uno'; 
             $subcent = 'os'; 
          } 
          $t = ''; 
          $n2 = substr($num, 1); 
          if ($n2 == '00') { 
          }elseif ($n2 < 21) 
             $t = ' ' . $matuni[(int)$n2]; 
          elseif ($n2 < 30) { 
             $n3 = $num[2]; 
             if ($n3 != 0) $t = 'i' . $matuni[$n3]; 
             $n2 = $num[1]; 
             $t = ' ' . $matdec[$n2] . $t; 
          }else{ 
             $n3 = $num[2]; 
             if ($n3 != 0) $t = ' y ' . $matuni[$n3]; 
             $n2 = $num[1]; 
             $t = ' ' . $matdec[$n2] . $t; 
          } 
          $n = $num[0]; 
          if ($n == 1) { 
             $t = ' ciento' . $t; 
          }elseif ($n == 5){ 
             $t = ' ' . $matunisub[$n] . 'ient' . $subcent . $t; 
          }elseif ($n != 0){ 
             $t = ' ' . $matunisub[$n] . 'cient' . $subcent . $t; 
          } 
          if ($sub == 1) { 
          }elseif (! isset($matsub[$sub])) { 
             if ($num == 1) { 
                $t = ' mil'; 
             }elseif ($num > 1){ 
                $t .= ' mil'; 
             } 
          }elseif ($num == 1) { 
             $t .= ' ' . $matsub[$sub] . '?n'; 
          }elseif ($num > 1){ 
             $t .= ' ' . $matsub[$sub] . 'ones'; 
          }   
          if ($num == '000') $mils ++; 
          elseif ($mils != 0) { 
             if (isset($matmil[$sub])) $t .= ' ' . $matmil[$sub]; 
             $mils = 0; 
          } 
          $neutro = true; 
          $tex = $t . $tex; 
       } 
       $tex = $neg . substr($tex, 1) . $fin; 
       //Zi hack --> return ucfirst($tex);
       $end_num=ucfirst($tex).' pesos '.$float[1].'/100 M.N.';
       return $end_num; 
    } 
}

?>