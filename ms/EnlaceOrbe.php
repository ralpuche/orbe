<?php
	header('Content-type: text/html; charset=utf-8');
	require_once("ConectorOrbe.class.php");
    require_once("ConectorEdicom.class.php");
	error_reporting(E_ALL ^ E_NOTICE);
	$conector = new ConectorOrbe();
	if(!$conector->estableceConexion()) die("Conexion no establecida\n");
	
	//seteamos el conjunto de caracteres de la db a UTF8
	$conector->setConsulta("SET NAMES 'utf8';");
	$conector->ejecutaConsulta();

    $_POST = $conector->sanitize($_POST);

    $resultado = "";
	$opcion = $_REQUEST["opcion"];
	$token = $_REQUEST["token"];
	
	//si la opción es 1, entonces recupera únicamente el token para este usuario específico
	if((($opcion != 1)&&($opcion != 2)&&($opcion != 3))&&($token != $conector->getTokenActual())){
		die("");
	}
	
	switch($opcion){
		case 1: //esta función recupera el token que usará el servidor para identificar a este cliente
			     $resultado = $conector->getToken();
			     break;
		
		case 2:  $resultado = $conector->getDatosSesionActiva();
			     break;
		
		case 3:  $usuario = $_REQUEST['usuario'];
			     $password = $_REQUEST['password'];
			     $resultado = $conector->recuperaIdUsuario($usuario, $password, 1);
			     break;
		
		case 4:  $resultado = $conector->cerrarSesion();
			     break;
		
		//recupera la fecha actual del servidor
		case 5:  $resultado = $conector->getFechaActual();
			     break;
            
        //opción para recuperar la lista de usuarios del sistema
        case 6:  $resultado = $conector->getUsuariosDelSistema();
                 break;
            
        case 7:  $info = $_REQUEST['info'];
                 $resultado = $conector->guardaUsuario($info);
                 break;
            
        case 8:  $idusuario = $_REQUEST['idusuario'];
                 $resultado = $conector->getUsuarioPorId($idusuario);
                 break;
            
        case 9:  $idusuario = $_REQUEST['idusuario'];
                 $resultado = $conector->eliminaUsuarioPorId($idusuario);
                 break;
            
        case 10: $resultado = $conector->getBancos();
                 break;
            
        case 11: $info = $_REQUEST['info'];
                 $resultado = $conector->guardaBanco($info);
                 break;
            
        case 12: $idbanco = $_REQUEST['idbanco'];
                 $resultado = $conector->getBancoPorId($idbanco);
                 break;
            
        case 13: $idbanco = $_REQUEST['idbanco'];
                 $resultado = $conector->eliminaBancoPorId($idbanco);
                 break;
            
        case 14: $resultado = $conector->getEmpresas();
                 break;
            
        case 15: $info = $_REQUEST['info'];
                 $resultado = $conector->guardaEmpresa($info);
                 break;
            
        case 16: $idempresa = $_REQUEST['idempresa'];
                 $resultado = $conector->getEmpresaPorId($idempresa);
                 break;
            
        case 17: $idempresa = $_REQUEST['idempresa'];
                 $resultado = $conector->eliminaEmpresa($idempresa);
                 break;
            
        case 18: $resultado = $conector->getClientes();
                 break;
            
        case 19: $info = $_REQUEST['info'];
                 $resultado = $conector->guardaCliente($info);
                 break;
            
        case 20: $resultado = $conector->getListaDatosFacturacion();
                 break;
            
        case 21: $info = $_REQUEST['info'];
                 $resultado = $conector->guardaDatosFacturacion($info);
                 break;
            
        case 22: $idDatosFacturacion = $_REQUEST['idDatosFacturacion'];
                 $resultado = $conector->getDatosDeFacturacion($idDatosFacturacion);
                 break;
            
        case 23: $idcliente = $_REQUEST['idcliente'];
                 $resultado = $conector->getClientePorId($idcliente);
                 break;
                     
        case 24: $idcliente = $_REQUEST['idcliente'];
                 $resultado = $conector->eliminaCliente($idcliente);
                 break;
            
        case 25: $fechaInicio = $_REQUEST['fechaInicio'];
                 $fechaFinal = $_REQUEST['fechaFinal'];
                 $resultado = $conector->getGastosInternosDeRangoDeFechas($fechaInicio, $fechaFinal);
                 break;
            
        case 26: $resultado = $conector->getListaDeConceptos();
                 break;
            
        case 27: $info = $_REQUEST['info'];
                 $resultado = $conector->guardaConcepto($info);
                 break;
            
        case 28: $idconcepto = $_REQUEST['idconcepto'];
                 $resultado = $conector->getConceptoPorId($idconcepto);
                 break;
        
        case 29: $idconcepto = $_REQUEST['idconcepto'];
                 $resultado = $conector->eliminaConceptoPorId($idconcepto);
                 break;
            
        case 30: $info = $_REQUEST['info'];
                 $resultado = $conector->guardaGastoInterno($info);
                 break;
            
        case 31: $idGastoInterno = $_REQUEST['idgastointerno'];
                 $resultado = $conector->getGastoInternoPorId($idGastoInterno);
                 break;
            
        case 32: $idGastoInterno = $_REQUEST['idgastointerno'];
                 $resultado = $conector->eliminaGastoInterno($idGastoInterno);
                 break;
            
        case 33: $fechaInicio = $_REQUEST['fechaInicio'];
                 $fechaFin = $_REQUEST['fechaFinal'];
                 $idcliente = $_REQUEST['idcliente'];
                 if($_SESSION['idPerfil'] == 5){
                    //dado que este usuario es cliente, solo podrá ver sus propios movimientos
                    $idcliente = $_SESSION['nombreCompleto'];
                 }
                 $resultado = $conector->getGastosDeCliente($fechaInicio, $fechaFin, $idcliente);
                 break;
            
        case 34: $info = $_REQUEST['info'];
                 $resultado = $conector->guardaGastoDeCliente($info);
                 break;
            
        case 35: $idGasto = $_REQUEST['idgasto'];
                 $resultado = $conector->getGastoDeClientePorId($idGasto);
                 break;
            
        case 36: $idGasto = $_REQUEST['idgasto'];
                 $resultado = $conector->eliminaGastoPorId($idGasto);
                 break;
            
        case 37: $resultado = $conector->getGastosPendientesPorFacturar();
                 break;
            
        case 38: $idGasto = $_REQUEST['idGasto'];
                 $estatus = $_REQUEST['estatus'];
                 $resultado = $conector->cambiaEstatusDeGasto($idGasto, $estatus);
                 break;
            
        case 39: $fechaInicio = $_REQUEST['fechaInicio'];
                 $fechaFin = $_REQUEST['fechaFinal'];
                 $idpromotor = $_REQUEST['idpromotor'];
                 if($_SESSION['idPerfil'] == 3){
                    //dado que este usuario es promotor, solo podrá ver sus propios movimientos
                    $idpromotor = $_SESSION['idusuario'];
                 }
                 $resultado = $conector->getGastosDePromotor($fechaInicio, $fechaFin, $idpromotor);
                 break;
            
        case 40: $datos = $_POST['datos'];
                 $_SESSION['datos'] = $datos;
                 $resultado = 1;
                 break;
            
        case 41: $resultado = $conector->getProductosAndServiciosSAT();
                 break;
            
        case 42: $resultado = $conector->getUnidadesSAT();
                 break;
            
        case 43: $info = $_REQUEST['info'];
                 $resultado = $conector->guardaConceptoDeFacturacion($info);
                 break;
            
        case 44: $resultado = $conector->getConceptosDeFacturacion();
                 break;
            
        case 45: $idConcepto = $_REQUEST['idConceptoFacturacion'];
                 $resultado = $conector->getConceptoDeFacturacionPorId($idConcepto);
                 break;
            
        case 46: $idConcepto = $_REQUEST['idConcepto'];
                 $idGasto = $_REQUEST['idGasto'];
                 $resultado = $conector->guardaConceptoDeFacturacionEnConceptosDeGasto($idConcepto, $idGasto);
                 break;
            
        case 47: $idGasto = $_REQUEST['idGasto'];
                 $resultado = $conector->getConceptosDeGasto($idGasto);
                 break;
            
        case 48: $idConcepto = $_REQUEST['idConcepto'];
                 $resultado = $conector->eliminaConceptoDeFacturacionDeGasto($idConcepto);
                 break;
            
        case 49: $resultado = $conector->getRegimenesFiscales();
                 break;
            
        case 50: $resultado = $conector->getFormaPagoSat();
                 break;
            
        case 51: $resultado = $conector->getMonedasSat();
                 break;
            
        case 52: $resultado = $conector->getBancosSat();
                 break;
            
        case 53: $info = $_REQUEST['info'];
                 $resultado = $conector->guardaPagoRecibido($info);
                 break;
            
        case 54: $idPago = $_REQUEST['idPago'];
                 $resultado = $conector->getInfoPagoRecibido($idPago);
                 break;
            
        case 55: $fechaInicio = $_REQUEST['fechaInicio'];
                 $fechaFin = $_REQUEST['fechaFinal'];
                 $idCliente = $_REQUEST['idCliente'];
                 $idEmpresa = $_REQUEST['idEmpresa'];
                 $resultado = $conector->getPagosRecibidosDeFecha($fechaInicio, $fechaFin, $idCliente, $idEmpresa);
                 break;
        
        case 56: $idElemento = $_REQUEST['idElemento'];
                 $tipoElemento = $_REQUEST['tipoElemento'];
                 $resultado = $conector->getFacturasEmitidasPorTipoElemento($idElemento, $tipoElemento);
                 break;
            
        case 57: $idFactura = $_REQUEST['idFactura'];
                 $resultado = $conector->enviaFacturaPorCorreo($idFactura);
                 break;
            
        case 58: $idFactura = $_REQUEST['idFactura'];
                 $conectorFacturacion = new ConectorEdicom();
                 $resultado = $conectorFacturacion->cancelarCDFI($idFactura);
                 break;
            
        case 59: $resultado = $conector->getUsosCFDI();
                 break;
            
        case 60: $idFactura = $_REQUEST['idFactura'];
                 $resultado = $conector->enviaFacturaCanceladaPorCorreo($idFactura);
                 break;
            
        case 61: $idConcepto = $_REQUEST['idConcepto'];
                 $resultado = $conector->eliminaConceptoDeFacturacion($idConcepto);
                 break;
            
        case 62: $idComprobante = $_REQUEST['idComprobante'];
                 $resultado = $conector->eliminaComprobanteDePago($idComprobante);
                 break;
	}
	
	$conector->cierraConexion();
	echo $resultado;
    die();
?>