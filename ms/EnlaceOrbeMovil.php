<?php
	header('Content-type: text/html; charset=utf-8');
	include("ConectorMotos.class.php");
	error_reporting(E_ALL ^ E_NOTICE);
	$conector = new ConectorMotos();
	if(!$conector->estableceConexion()) die("Conexion no establecida\n");
	
	//seteamos el conjunto de caracteres de la db a UTF8
	$conector->setConsulta("SET NAMES 'utf8';");
	$conector->ejecutaConsulta();
	
	$_POST = $conector->sanitize($_POST);
	
	$resultado = "";
	$opcion = $_REQUEST["opcion"];
	$token = $_REQUEST["token"];
	
	//si la opción es 1, entonces recupera únicamente el token para este usuario específico
	if((($opcion != 1)&&($opcion != 2)&&($opcion != 3))&&(!$conector->getTokenActualDeMovil($token))){
		echo("token issue");
	}

	switch($opcion){
		case 1: //esta función recupera el token que usará el servidor para identificar a este cliente
			     $resultado = $conector->getToken();
			     break;
		
		case 2:  $resultado = $conector->getDatosSesionActiva();
			     break;
		
		case 3:  $usuario = $_REQUEST['usuario'];
			     $password = $_REQUEST['password'];
			     $resultado = $conector->recuperaIdUsuario($usuario, $password, 2);
			     break;
		
		case 4:  $resultado = $conector->cerrarSesion();
			     break;
            
        case 5:  $idusuario = $_REQUEST['idusuario'];
                 $identrega_final = $_REQUEST['identrega'];
                 $resultado = $conector->getNuevasEntregasPendientesAsignadas($idusuario, $identrega);
                 break;
            
        case 6:  $idusuario = $_REQUEST['idusuario'];
                 $tokenGCM = $_REQUEST['tokenGCM'];
                 $resultado = $conector->guardaTokenGCM($idusuario, $tokenGCM, $token);
                 //idusuario=numero&tokenGCM=asdasdasd
                 break;
            
        case 7:  //esta función se usa para avisar que ya se visualizó la entrega pendiente por parte del encargado de la entrega
                 $idusuario = $_REQUEST['idusuario'];
                 $identrega = $_REQUEST['identrega'];
                 $estatus = $_REQUEST['estatus'];
                 $resultado = $conector->guardaEstatusDeEntrega($idusuario, $identrega, $estatus);
                 break;
            
        case 8:  //esta función recupera el último estatus de la entrega, que deberá actualizar el conductor de la moto
                 $identrega = $_REQUEST['identrega'];
                 $resultado = $conector->getUltimoEstatusDeEntrega($identrega);
                 break;
            
        case 9: //esta función marca como entrega la entrega y le cambia el estatus. También guarda el nombre de la persona que recibió la entrega
                 $idusuario = $_REQUEST['idusuario'];
                 $identrega = $_REQUEST['identrega'];
                 $nombre_recibe = $_REQUEST['nombre_recibe'];
                 $resultado = $conector->guardaRegistroDeEntregaRealizada($idusuario, $identrega, $nombre_recibe);
                 break;
            
        case 18: $idusuario = $_REQUEST['idusuario'];
                 $latitud = $_REQUEST['lat'];
                 $longitud = $_REQUEST['long'];
                 $resultado = $conector->guardaLocalizacionDeUsuario($idusuario, $latitud, $longitud);
                 break;
	}
	
	$conector->cierraConexion();
    $resultado = str_replace("|", "<>", $resultado);
	echo $resultado;
    die();
?>