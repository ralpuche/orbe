<?php
    ini_set('memory_limit' , '2048M');
	ini_set('max_execution_time', 300); //300 seconds = 5 minutes
    error_reporting(E_ALL);
    ini_set('display_errors', '1');
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header('Content-type: text/html; charset=utf-8');
	require_once("ConectorOrbe.class.php");
    require_once("ConectorEdicom.class.php");
    include_once('tcpdf/config/lang/spa.php');
    include_once('tcpdf/tcpdf.php');
    include_once("phpqrcode/qrlib.php");

    // Extend the TCPDF class to create custom Header and Footer
    class MYPDF extends TCPDF {

        //Page header
        public function Header(){
            $this->setDefaultSettings();
            //$image_file = K_PATH_IMAGES.'logo2.jpg';
            //$this->Image($image_file, 485, 30, 100, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        }

        // Page footer
        public function Footer() {
            $this->SetY(-50);
            $this->SetFont('helvetica', 'I', 8);
            $this->Cell(0, 10, 'Este documento es una representación impresa de un CFDI 3.3 '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        }

        public function setDefaultSettings(){
            // set default monospaced font
            $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            //set margins
            $this->SetMargins(PDF_MARGIN_LEFT, 85, PDF_MARGIN_RIGHT);
            $this->SetHeaderMargin(85);
            $this->SetFooterMargin(PDF_MARGIN_FOOTER);

            //set auto page breaks
            $this->SetAutoPageBreak(TRUE, 60);

            //set image scale factor
            $this->setImageScale(PDF_IMAGE_SCALE_RATIO);

             // set document information
            $this->SetCreator(PDF_CREATOR);
            $this->SetAuthor('Orbe Solutions');
        }

        public function agregaPaginaConContenidoDelHospital($contenido){
            $this->AddPage();
            $this->SetXY(25, 80, 0);
            $this->writeHTML($contenido, true, false, true, false, '');
        }
    }
    
    /*if($_SESSION['nombreUsuario'] != 'ralpuchev'){
        die("servicio no disponible.");
        return;
    }*/

    //recuperamos la hoja de costos que se habrá de facturar
    $opcion = $_REQUEST['opcion'];//1 Factura Global
    $idElemento = $_REQUEST['idElemento'];
    $ambiente = $_REQUEST['ambiente'];// 1: vista previa 2: real
    $visualizacion = 0;
    if(isset($_REQUEST['visualizacion'])) $visualizacion = $_REQUEST['visualizacion'];

    //revisamos primero que para este elemento y este tipo de factura, no exista ya un UUID. Si existe, entonces trataremos de recuperar el archivo que debe encontrarse en la carpeta de la empresa correspondiente. Si el archivo existe, entonces nos moveremos a él y ahí terminará el flujo. Si el archivo no existe, pero hay un UUID, trataremos de recuperarlo del proveedor de facturación.
    $xml = null;
    $xmlYaFirmado = false;
    $conectorOrbe = new ConectorOrbe();
    $conectorOrbe->estableceConexion();

    $datos = explode("|", $conectorOrbe->getDatosFacturacionPorTipoAndElemento($idElemento, $opcion));
    //idFacturaEmitida, tipoFactura, idElemento, UUID, cantidadFacturada, fecha, hora, usuario, estatus, nombreUsuario, idEmpresa, numeroConsecutivo
    
    $idEmpresa = 0;

    if(count($datos) > 1){
        $idEmpresa = $datos[10];
        //ya hay una factura activa asociada a este tipo de elemento
        //idFacturaEmitida, tipoFactura, idElemento, UUID, cantidadFacturada, fecha, hora, usuario, estatus, nombreUsuario
        //rcuperamos la empresa asociada a este elemento
        
        //Verificamos que el archivo pdf de la factura exista. Si es así entonces direccionamos a él
        $path = "cdfis/".$idEmpresa."/".(($opcion == 1)? "I" : "P").$idElemento.".pdf";
        if (file_exists($path)){
            header ("Location: $path");
            die();
        }else{
            //como no existe el archivo, pero debería, se intenta recuperar el xml, para no consultar al proveedor.
            $path = "cdfis/".$idEmpresa."/".(($opcion == 1)? "I" : "P").$idElemento.".xml";
            if (file_exists($path)){
                $xml = "";
                $file = fopen($path, "r");
                while(!feof($file)) {
                    $xml .= fgets($file);
                }
                fclose($file);
                $xmlYaFirmado = true;
            }else{
                //no hay más remedio que buscar con el proveedor
                die("Esta elemento ya está facturado. Se deberá recuperar la factura con el proveedor de facturación.");
            }
        }
    }else{
        if($opcion == 1) $idEmpresa = $conectorOrbe->getIdEmpresaDeGasto($idElemento);
        else if($opcion == 2) $idEmpresa = $conectorOrbe->getIdEmpresaDePago($idElemento); 
    }

    if($visualizacion != 0){
        die("Debería existir el pdf o xml, por favor reporta este error!");
    }

    $conectorOrbe->cierraConexion();

    $conectorFacturacion = new ConectorEdicom();
    $conectorFacturacion->setAmbiente($ambiente);
    $conectorFacturacion->setIdEmpresa($idEmpresa);
    $conectorFacturacion->setPathCarpeta("cdfis/".$idEmpresa."/");

    //buscamos la referencia en las facturas emitidas, considerando el número consecutivo y también el estatus para recuperar solo aquellas facturas que estén vigentes (estatus 1).
    if($xml == null){
        if($opcion == 1) $xml = $conectorFacturacion->getXMLParaTimbradoFacturaGlobal($idElemento);
        else if($opcion == 2) $xml = $conectorFacturacion->getXMLDeComprobanteDePago($idElemento);    
    }

    if($xml != null && $xml != "" && $xml != " "){
        //si tenemos contenido en el xml, entonces podemos continuar
        if(!$xmlYaFirmado){
            $xml = $conectorFacturacion->timbraFactura($xml, $idElemento, $opcion);
        }
        
        if($xml != null){
            //recuperamos el contenido para el pdf del xml
            $infoXML = $conectorFacturacion->getInfoDeXMLTimbrado($xml);
            $conectorFacturacion->getImagenQRDeFolio($idElemento, $opcion, $ambiente);
            
            if($opcion == 1) $contenidoPDF = $conectorFacturacion->getContenidoDePDFFacturaGlobal($infoXML, $idElemento);
            else if($opcion == 2) $contenidoPDF = $conectorFacturacion->getContenidoDePDFPago($infoXML, $idElemento);
            
            $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

            // set document information
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('Orbe Solutions');
            $pdf->SetTitle('Factura Emitida');
            $pdf->SetSubject('Factura Electronica');

            $pdf->setDefaultSettings();

            $pdf->AddPage();
            
            $pdf->Image($conectorFacturacion->getPathCarpeta()."logo.jpg", 485, 30, 100, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
            
            $pdf->SetXY(25, 20, 0);
            $pdf->writeHTML($contenidoPDF, true, false, true, false, '');
            
            if($ambiente == 2){
                $pdf->Output($conectorFacturacion->getPathCarpeta().(($opcion == 1)?'I':'P').$idElemento.".pdf", 'F');
                $conectorFacturacion->getConector()->enviaFacturaPorCorreo($conectorFacturacion->getIdFacturaEmitida());
            }
            
            $conectorFacturacion->getConector()->cierraConexion();
            
            $pdf->Output("Factura ".$idElemento.".pdf", 'I');
        }
    }
?>