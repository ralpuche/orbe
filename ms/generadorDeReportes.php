<?php
    session_start();
    session_set_cookie_params('0');
    header('Content-type: text/html; charset=utf-8');
    ini_set('memory_limit' , '1024M');
    ini_set("max_execution_time","3600");
    require_once('tcpdf/config/lang/spa.php');
	require_once('tcpdf/tcpdf.php');

    //redeclaración del head y foot
	// Extend the TCPDF class to create custom Header and Footer
	class MYPDF extends TCPDF {
		
		//Page header
		public function Header(){
			$image_file = K_PATH_IMAGES.'logo2.jpg';
			$this->Image($image_file, 25, 15, 80, '', 'JPG', '', 'T', false, 100, '', false, false, 0, false, false, false);
		}
		
		// Page footer
		public function Footer() {
			// Position at 15 mm from bottom
			$this->SetY(-50);
			// Set font
			$this->SetFont('helvetica', 'I', 8);
			// Page number
			$this->Cell(0, 10, 'Página '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
		}
	}

    if(isset($_POST['datos'])){
        $_SESSION['datos'] = $_POST['datos'];
        return 1;   
    }

    $partes = explode("<*>", $_SESSION['datos']);
// 1<*>Nombre|R.F.C.<*>45%|45%<*>left|left<*>2|Rafa Alpuche Velázquez|AUVR8702108R5|1^3|Un nombre x||1^
    //opcion, encabezados, medidas, alineacion, datos
    $opcion = $partes[0];
    $encabezados = explode("|", $partes[1]);
    $medidas = explode("|", $partes[2]);
    $alineaciones = explode("|", $partes[3]);
    $datos = explode("^", str_replace('"', "'", $partes[4]));
    array_pop($datos);

    if($opcion == 1){
        $csv = '"No."';
        foreach($encabezados as $encabezado) $csv .= ',"'.$encabezado.'"';
        $csv .= PHP_EOL;
        $i = 1;
        foreach($datos as $dato){
            $csv .= '"'.$i.'"';
            $dato = explode("|", $dato);
            $j = 1;
            foreach($encabezados as $encabezado){
                $csv .= ',"'.$dato[$j].'"';
                $j++;
            }
            $i++;
            $csv .= PHP_EOL;
        }
        $file_name = "archivos/tmp_".$_SESSION['idusuario'].".csv";
        $myfile = fopen($file_name, "w") or die("Unable to open file!");
        fwrite($myfile, $csv);
        fclose($myfile);
        
        // make sure it's a file before doing anything!
        if(is_file($file_name))
        {

            /*
                Do any processing you'd like here:
                1.  Increment a counter
                2.  Do something with the DB
                3.  Check user permissions
                4.  Anything you want!
            */

            // required for IE
            if(ini_get('zlib.output_compression')) { ini_set('zlib.output_compression', 'Off'); }

            // get the file mime type using the file extension
            switch(strtolower(substr(strrchr($file_name,'.'),1)))
            {
                /*case 'pdf': $mime = 'application/pdf'; break;
                case 'zip': $mime = 'application/zip'; break;
                case 'jpeg':
                case 'jpg': $mime = 'image/jpg'; break;*/
                default: $mime = 'application/force-download';
            }
            header('Pragma: public');   // required
            header('Expires: 0');       // no cache
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Cache-Control: private',false);
            header('Content-Type: '.$mime);
            header('Content-Disposition: attachment; filename="'.basename($file_name).'"');
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: '.filesize($file_name));    // provide file size
            readfile($file_name);       // push it out
            exit();

        }else{
            echo "No se pudo tener acceso al archivo!";
        }
    }else{
        $totalMedidas = 0;
        foreach($medidas as $medida){
            $posicion = strpos($medida, "%");
            $totalMedidas += substr($medida, 0, $posicion);
        }
        $diferencia = 95-$totalMedidas;
        $i = 0;
        foreach($medidas as $medida){
            //echo "medida: ".$medida."<br/>";
            $posicion = strpos($medida, "%");
            $medidaActual = substr($medida, 0, $posicion);
            
            $medidaActual += $diferencia * ($medidaActual/100);
            //echo "medida nueva: ".$medidaActual."<br/>";
            
            $medidas[$i] = $medidaActual."%";
            $i++;
        }
        
        //se genera el contenido para una tabla en PDF
        $contenido = '<html>';
        $contenido .= '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>';
        $contenido .= '<body>';
        
        $contenido .= '<br/><table width="721" align="left" cellpadding="5" cellspacing="0" border="0" rules="rows"><tr style="background-color:#606060; color:#ffffff; height:30px; font-size:10px; font-weight:bold;"><td align="center" width="5%" valign="top">No</td>';
        
        $i = 0;
        foreach($encabezados as $encabezado){
            $contenido .= '<td align="center" width="'.$medidas[$i].'" valign="top">'.$encabezado.'</td>';
            $i++;
        }
        $contenido .= '</tr>';
        
        $i = 1;
        foreach($datos as $dato){
            $dato = explode("|", $dato);
            
            if($i%2 == 0) $contenido .= '<tr style="height:30px; font:Verdana, Geneva, sans-serif; font-size:10px; text-align:center; background-color:#ececec; color:#000000;"><td align="center" width="5%">'.$i.'</td>';
            else $contenido .= '<tr style="height:30px; font:Verdana, Geneva, sans-serif; font-size:10px; text-align:center; color:#000000;"><td align="center" width="5%">'.$i.'</td>';
            
            $j = 1;
            foreach($encabezados as $encabezado){
                @$contenido .= '<td align="'.$alineaciones[$j-1].'" width="'.$medidas[$j-1].'" valign="top">'.$dato[$j].'</td>';
                $j++;
            }
            $contenido .= '</tr>';
            $i++;
        }
        $contenido .= '</table>';
        $contenido .= '</body>';
        $contenido .= '</html>';
        
        //más información sobre el pdf que recién se ha agregado
        // create new PDF document
        $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('OrbeSolutions');
        $pdf->SetTitle('Reporte');
        $pdf->SetSubject('Reporte');
        $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        //set margins
        $pdf->SetMargins(24, 60, 24);
        $pdf->SetHeaderMargin(60);
        $pdf->SetFooterMargin(50);

        //set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, 60);

        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        //set some language-dependent strings
        $pdf->setLanguageArray($l);

        // add a page
        $pdf->AddPage();

        $pdf->SetXY(24, 60, 0);
        $pdf->SetX(24);
        $pdf->writeHTML($contenido, true, false, true, false, '');
        
        //Close and output PDF document
        $pdf->Output('reporte.pdf', 'I');
    }
?>