﻿<?php
/* 
KIT DE INTEGRACION PHP para CFID 3.3
versión 1.0
Integración de Timbrado
Licencia: MIT - https://opensource.org/licenses/MIT
Profact - La forma más simple de facturar
*/
//echo "si funciona";
//exit;
/* Ruta del servicio de integracion*/

$ws = "https://cfdi33-pruebas.buzoncfdi.mx:1443/Timbrado.asmx?wsdl";
$response = '';
$workspace="C:\\SVN\\TimbradoProfact2\\";

/* Ruta del comprobante a timbrar*/
$rutaArchivo = $workspace.'Error_CFDI33100-VALIDO2.xml';

/* El servicio recibe el comprobante (xml) codificado en Base64, el rfc del emisor deberá ser 'AAA010101AAA' para efecto de pruebas*/ 
$cdfi = '<?xml version="1.0" encoding="UTF-8"?><cfdi:Comprobante xmlns:cfdi="http://www.sat.gob.mx/cfd/3" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" NumCtaPago="8636" LugarExpedicion="COATZACOALCOS VERACRUZ" metodoDePago="03" tipoDeComprobante="ingreso" total="3465.89" Moneda="MXN" TipoCambio="1.0" descuento="0.00" subTotal="2987.84" certificado="MIIEYjCCA0qgAwIBAgIUMDAwMDEwMDAwMDAzMDY5OTczMTMwDQYJKoZIhvcNAQEFBQAwggGKMTgwNgYDVQQDDC9BLkMuIGRlbCBTZXJ2aWNpbyBkZSBBZG1pbmlzdHJhY2nDs24gVHJpYnV0YXJpYTEvMC0GA1UECgwmU2VydmljaW8gZGUgQWRtaW5pc3RyYWNpw7NuIFRyaWJ1dGFyaWExODA2BgNVBAsML0FkbWluaXN0cmFjacOzbiBkZSBTZWd1cmlkYWQgZGUgbGEgSW5mb3JtYWNpw7NuMR8wHQYJKoZIhvcNAQkBFhBhY29kc0BzYXQuZ29iLm14MSYwJAYDVQQJDB1Bdi4gSGlkYWxnbyA3NywgQ29sLiBHdWVycmVybzEOMAwGA1UEEQwFMDYzMDAxCzAJBgNVBAYTAk1YMRkwFwYDVQQIDBBEaXN0cml0byBGZWRlcmFsMRQwEgYDVQQHDAtDdWF1aHTDqW1vYzEVMBMGA1UELRMMU0FUOTcwNzAxTk4zMTUwMwYJKoZIhvcNAQkCDCZSZXNwb25zYWJsZTogQ2xhdWRpYSBDb3ZhcnJ1YmlhcyBPY2hvYTAeFw0xNTA1MDYxNzA2NDhaFw0xOTA1MDYxNzA2NDhaMIGuMSAwHgYDVQQDExdPQ1RBVklPIEpPU0UgTUFZIE1FUklOTzEgMB4GA1UEKRMXT0NUQVZJTyBKT1NFIE1BWSBNRVJJTk8xIDAeBgNVBAoTF09DVEFWSU8gSk9TRSBNQVkgTUVSSU5PMRYwFAYDVQQtEw1NQU1YNjUxMTEzTVAyMRswGQYDVQQFExJNWE1PNjUxMTEzSFZaWVJDMDExETAPBgNVBAsTCEhvc3BpdGFsMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCFx0Sgj1Hovhy3wTzu+5hX1SI2rm6nAvmn0YSvPZL4LOrPyWlvWuZ55AvapFwD8lYo3oFsZnVFVfKRFUgggxuQ0iKGD+f5fk+5n/CBQPSbGq26736oUiO+BeEqhhGOt3i3h6HujCBH6Baj9SRVnLgfVkgL6FkAlSVBT5v3xlPQlQIDAQABox0wGzAMBgNVHRMBAf8EAjAAMAsGA1UdDwQEAwIGwDANBgkqhkiG9w0BAQUFAAOCAQEAFjlhtpC4fyiXvUqw7IAHreyJRyTQzXtKkTrTOaNdGoIkGQWCOCXjM7G00HLu6buWx1P17cW8ZDnoRHdNglJQWcowCjBIBeamv1b7k6Mkum1dplX0bjSHeW0YbW9khzDFwM0LJZ+SJRBJXRsxWVE1nezFmbMOQszORsXUKn1mlFOhc3ekpAo4SGbA2cGehNbXk5/iiMlBgR75wMxP1OAMTsj8B7c85xgu6j0QG2Uc5kDmuzKVN3IDHB1NjxH59H99g4wmCu/ClvGJHyzOi3eiA1mJ9PUF9NkGGZWOsMB9WWd4zRkp3+y6VQJLUOBTy2O6U5GibPTHrifwa6h25NKEIA==" noCertificado="00001000000306997313" formaDePago="PAGO EN UNA SOLA EXHIBICIÓN" sello="A7SuOkLUwYR9vo/R69cY1Wmiy7lhvRX2N8sY1jJNFTIOvXVnRZDC5XSXH+im2KbaA8zb5HSECrrTK+RAw367PuuqdJ9yWTAslVCdvlVX7/59m+5+GHE+APPBmS3S7rIg7fAeB1xKmdgIS/xJ3AC/JTlrHyQRtVnR9g3/gf3IZu4=" fecha="2017-11-21T11:20:14" folio="S1635" serie="S" version="3.2" xsi:schemaLocation="http://www.sat.gob.mx/cfd/3 http://www.sat.gob.mx/sitio_internet/cfd/3/cfdv32.xsd"><cfdi:Emisor nombre="OCTAVIO JOSE MAY MERINO" rfc="AAA010101AAA"><cfdi:DomicilioFiscal codigoPostal="96400" pais="MEXICO" estado="VERACRUZ" municipio="COATZACOALCOS" localidad="COATZACOALCOS" colonia="CENTRO" noExterior="1009" calle="AV.I.ZARAGOZA" /><cfdi:RegimenFiscal Regimen="PERSONA FISICA CON ACTIVIDAD EMPRESARIAL Y PROFESIONAL" /></cfdi:Emisor><cfdi:Receptor nombre="ABA SEGUROS S.A. DE C.V." rfc="AAA010101AAA"><cfdi:Domicilio codigoPostal="06600" pais="MEXICO" estado="CIUDAD DE MEXICO" municipio="CUAUHTEMOC" localidad="CUAUHTEMOC" colonia="JUAREZ" noExterior="250 PISO 15" calle="AV PASEO DE LA REFORMA" /></cfdi:Receptor><cfdi:Conceptos><cfdi:Concepto importe="400.00" valorUnitario="400.00" descripcion="DR. JORGE GUILLERMO MAY MERINO HONORARIOS POR ATENCION DE URGENCIA" unidad="NA" cantidad="1" /><cfdi:Concepto importe="300.00" valorUnitario="300.00" descripcion="TRANSORAL DE COL CERVICAL" unidad="NA" cantidad="1" /><cfdi:Concepto importe="300.00" valorUnitario="300.00" descripcion="LAT DE COL CERVICAL" unidad="NA" cantidad="1" /><cfdi:Concepto importe="400.00" valorUnitario="400.00" descripcion="AP DE COL LUMBAR" unidad="NA" cantidad="1" /><cfdi:Concepto importe="400.00" valorUnitario="400.00" descripcion="LAT DE COL LUMBAR" unidad="NA" cantidad="1" /><cfdi:Concepto importe="200.00" valorUnitario="200.00" descripcion="COLLARIN BLANDO" unidad="PIEZA" cantidad="1" /><cfdi:Concepto importe="541.65" valorUnitario="541.65" descripcion="NAPROXENO/CARISOPRODOL 250/200MG CAP 30" unidad="CAJA" cantidad="1" /><cfdi:Concepto importe="135.69" valorUnitario="135.69" descripcion="KETOROLACO 10MG TAB 10" unidad="CAJA" cantidad="1" /><cfdi:Concepto importe="310.50" valorUnitario="103.50" descripcion="COMBESTERAL AMP 1" unidad="PIEZA" cantidad="3" /></cfdi:Conceptos><cfdi:Impuestos totalImpuestosTrasladados="478.05"><cfdi:Traslados><cfdi:Traslado importe="478.05" tasa="16.00" impuesto="IVA" /></cfdi:Traslados></cfdi:Impuestos></cfdi:Comprobante>';


//$base64Comprobante = file_get_contents($rutaArchivo);
$base64Comprobante = base64_encode($cdfi);
try
{
$params = array();
/*Nombre del usuario integrador asignado, para efecto de pruebas utilizaremos 'mvpNUXmQfK8='*/
$params['usuarioIntegrador'] = 'mvpNUXmQfK8=';
/* Comprobante en base 64*/
$params['xmlComprobanteBase64'] = $base64Comprobante;
/*Id del comprobante, deberá ser un identificador único, para efecto del ejemplo se utilizará un numero aleatorio*/
$params['idComprobante'] = rand(5, 999999);

$context = stream_context_create(array(
    'ssl' => array(
        // set some SSL/TLS specific options
        'verify_peer' => false,
        'verify_peer_name' => false,
        'allow_self_signed' => true  //--> solamente true en ambiente de pruebas
    ),
	'http' => array(
            'user_agent' => 'PHPSoapClient'
            )
 ) );
$options =array();
$options['stream_context'] = $context;
$options['cache_wsdl']= WSDL_CACHE_MEMORY;
$options['trace']= true;

libxml_disable_entity_loader(false);
//$client = new SoapClient($ws,$options);
$client = new SoapClient($ws,$options);
$response = $client->__soapCall('TimbraCFDI', array('parameters' => $params));
print_r($response);
return;
    
foreach($keys as $key) {
    echo($key);
	}
}
catch (SoapFault $fault)
{
	echo "SOAPFault: ".$fault->faultcode."-".$fault->faultstring."\n";
}
/*Obtenemos resultado del response*/
echo "resultado";
return;
//echo $response;
$tipoExcepcion = $response->TimbraCFDIResult->anyType[0];
$numeroExcepcion = $response->TimbraCFDIResult->anyType[1];
$descripcionResultado = $response->TimbraCFDIResult->anyType[2];
$xmlTimbrado = $response->TimbraCFDIResult->anyType[3];
$codigoQr = $response->TimbraCFDIResult->anyType[4];
$cadenaOriginal = $response->TimbraCFDIResult->anyType[5];
$errorInterno = $response->TimbraCFDIResult->anyType[6];
$mensajeInterno = $response->TimbraCFDIResult->anyType[7];
if($xmlTimbrado != '')
{
	echo "xmlTimbrado";
/*El comprobante fue timbrado correctamente*/

/*Guardamos comprobante timbrado*/
file_put_contents($workspace.'comprobanteTimbrado.xml', $xmlTimbrado);

/*Guardamos codigo qr*/
file_put_contents($workspace.'codigoQr.jpg', $codigoQr);

/*Guardamos cadena original del complemento de certificacion del SAT*/
file_put_contents($workspace.'cadenaOriginal.txt', $cadenaOriginal);

print_r("Timbrado exitoso");

}
else
{
	echo "else";
	echo "[".$tipoExcepcion."  ".$numeroExcepcion." ".$descripcionResultado."  ei=".$errorInterno." mi=".$mensajeInterno."]" ;
}
?>


