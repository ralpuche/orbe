DROP TABLE IF EXISTS `ConceptosGastosInternos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ConceptosGastosInternos` (
  `idConcepto` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text NOT NULL,
  `estatus` int(11) NOT NULL,
  PRIMARY KEY (`idConcepto`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DatosFacturacion`
--

DROP TABLE IF EXISTS `DatosFacturacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DatosFacturacion` (
  `idDatosFacturacion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text COLLATE utf8_spanish2_ci NOT NULL,
  `rfc` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `calle` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `exterior` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `interior` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `referencia` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `colonia` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `localidad` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `municipio` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `estado` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `pais` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `cp` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `telefono` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `fax` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `cuenta` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `estatus` int(11) NOT NULL,
  PRIMARY KEY (`idDatosFacturacion`)
) ENGINE=InnoDB AUTO_INCREMENT=815 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;


DROP TABLE IF EXISTS `GastosClientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GastosClientes` (
  `idGastoCliente` int(11) NOT NULL AUTO_INCREMENT,
  `idPromotor` int(11) NOT NULL,
  `idEmpresa` int(11) NOT NULL,
  `idCliente` int(11) NOT NULL,
  `idBanco` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `cantidadTotal` varchar(255) NOT NULL,
  `porcentaje` varchar(255) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  PRIMARY KEY (`idGastoCliente`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `GastosInternos`
--

DROP TABLE IF EXISTS `GastosInternos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GastosInternos` (
  `idGastosInterno` int(11) NOT NULL AUTO_INCREMENT,
  `idConcepto` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `cantidad` varchar(255) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  PRIMARY KEY (`idGastosInterno`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;