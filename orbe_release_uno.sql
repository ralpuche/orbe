-- MySQL dump 10.13  Distrib 5.7.12, for osx10.9 (x86_64)
--
-- Host: 127.0.0.1    Database: orbe
-- ------------------------------------------------------
-- Server version	5.7.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Bancos`
--

DROP TABLE IF EXISTS `Bancos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Bancos` (
  `idBanco` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `estatus` int(11) NOT NULL,
  PRIMARY KEY (`idBanco`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Bancos`
--

LOCK TABLES `Bancos` WRITE;
/*!40000 ALTER TABLE `Bancos` DISABLE KEYS */;
INSERT INTO `Bancos` VALUES (1,'BBVA - Bancomer',1),(2,'Santander Serfin',0);
/*!40000 ALTER TABLE `Bancos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Bitacora`
--

DROP TABLE IF EXISTS `Bitacora`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Bitacora` (
  `idBitacora` int(11) NOT NULL AUTO_INCREMENT,
  `idAccion` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `textoAccion` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `datosAccion` text CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `ip` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_spanish2_ci NOT NULL,
  `estatus` int(11) NOT NULL,
  PRIMARY KEY (`idBitacora`)
) ENGINE=InnoDB AUTO_INCREMENT=146 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Bitacora`
--

LOCK TABLES `Bitacora` WRITE;
/*!40000 ALTER TABLE `Bitacora` DISABLE KEYS */;
INSERT INTO `Bitacora` VALUES (13,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-08-28','07:38:40','',1),(14,2,2,'El usuario cerró sesión: Rafa Alpuche','07:38:40::1|2|Rafa Alpuche|ralpuchev','2017-08-28','07:44:16','',1),(15,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-08-28','07:44:44','',1),(16,2,2,'El usuario cerró sesión: Rafa Alpuche','07:44:44::1|2|Rafa Alpuche|ralpuchev','2017-08-28','08:02:49','',1),(17,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-08-28','08:02:57','',1),(18,2,2,'El usuario cerró sesión: Rafa Alpuche','1166<->3160|2||','2017-08-31','01:43:10','',1),(19,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-08-31','01:43:17','',1),(20,2,2,'El usuario cerró sesión: Rafa Alpuche','01:43:17127.0.0.1|2|Rafa Alpuche|ralpuchev','2017-08-31','01:43:34','',1),(21,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-08-31','01:43:49','',1),(22,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-08-31','22:16:26','',1),(23,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-08-31','22:33:17','',1),(24,5,2,'Usuario agregado al sistema: David Alpuche agregado por: Rafa Alpuche','','2017-09-01','00:11:04','',1),(25,6,2,'Usuario actualizado: David Alpuche por: Rafa Alpuche','3|David Alpuche|1|david|alpuche7|alpuche7|1','2017-09-01','00:31:43','',1),(26,6,2,'Usuario actualizado: David Alpuche por: Rafa Alpuche','3|David Alpuche|1|david|alpuche7|alpuche7|1','2017-09-01','00:31:54','',1),(27,6,2,'Usuario actualizado: David Alpuche por: Rafa Alpuche','3|David Alpuche|1|david|alpuche7|alpuche7|1','2017-09-01','00:32:52','',1),(28,5,2,'Usuario agregado al sistema: Armando Alpuche agregado por: Rafa Alpuche','','2017-09-01','00:34:26','',1),(29,6,2,'Usuario actualizado: Armando Alpuche por: Rafa Alpuche','4|Armando Alpuche|2|mando|alpuche2016||1','2017-09-01','00:34:40','',1),(30,6,2,'Usuario actualizado: Armando Alpuche por: Rafa Alpuche','4|Armando Alpuche|2|mando|alpuche2016|mando@gmail.com|1','2017-09-01','00:35:00','',1),(31,7,2,'Usuario eliminado:  por: Rafa Alpuche','','2017-09-01','00:42:07','',1),(32,7,2,'Usuario eliminado:  por: Rafa Alpuche','','2017-09-01','00:42:49','',1),(33,7,2,'Usuario eliminado:  por: Rafa Alpuche','','2017-09-01','00:43:41','',1),(34,7,2,'Usuario eliminado:  por: Rafa Alpuche','','2017-09-01','00:44:37','',1),(35,7,2,'Usuario eliminado:  por: Rafa Alpuche','','2017-09-01','00:44:43','',1),(36,7,2,'Usuario eliminado:  por: Rafa Alpuche','','2017-09-01','00:44:48','',1),(37,2,2,'El usuario cerró sesión: Rafa Alpuche','22:16:26127.0.0.1|2|Rafa Alpuche|ralpuchev','2017-09-01','00:44:54','',1),(38,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-09-01','00:44:59','',1),(39,2,2,'El usuario cerró sesión: Rafa Alpuche','00:44:59127.0.0.1|2|Rafa Alpuche|ralpuchev','2017-09-01','00:45:34','',1),(40,3,0,'Datos de acceso incorrectos. Usuario: ralpuchev y password: lionheart','','2017-09-01','00:45:42','',1),(41,3,0,'Datos de acceso incorrectos. Usuario: ralpuchev y password: lionheart','','2017-09-01','00:45:48','',1),(42,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-09-01','00:46:33','',1),(43,8,2,'Banco agregado al sistema: Bancomer agregado por: Rafa Alpuche','','2017-09-01','01:23:02','',1),(44,8,2,'Banco agregado al sistema: Santander agregado por: Rafa Alpuche','','2017-09-01','01:23:11','',1),(45,9,2,'Banco actualizado: BBVA - Bancomer por: Rafa Alpuche','1|Bancomer|1','2017-09-01','01:26:02','',1),(46,9,2,'Banco actualizado: Santander Serfin por: Rafa Alpuche','2|Santander|1','2017-09-01','01:26:19','',1),(47,7,2,'Usuario eliminado:  por: Rafa Alpuche','1|System|1|System|System1357908642|ralpuchev@gmail.com|1','2017-09-01','01:28:40','',1),(48,7,2,'Usuario eliminado:  por: Rafa Alpuche','2|Rafa Alpuche|1|ralpuchev|lionheart|ralpuchev@gmail.com|1','2017-09-01','01:29:26','',1),(49,7,2,'Usuario eliminado:  por: Rafa Alpuche','2|Rafa Alpuche|1|ralpuchev|lionheart|ralpuchev@gmail.com|0','2017-09-01','01:31:15','',1),(50,10,2,'Banco eliminado:  por: Rafa Alpuche','2|Santander Serfin|1','2017-09-01','01:32:07','',1),(51,11,2,'Empresa agregada al sistema: Mega agregado por: Rafa Alpuche','','2017-09-01','07:43:28','',1),(52,11,2,'Empresa agregada al sistema: Nexus agregado por: Rafa Alpuche','','2017-09-01','07:45:24','',1),(53,12,2,'Empresa actualizada: Mega Comercial por: Rafa Alpuche','1|Mega|1','2017-09-01','07:48:46','',1),(54,12,2,'Empresa actualizada: Nexus de México por: Rafa Alpuche','2|Nexus|1','2017-09-01','07:48:57','',1),(55,13,2,'Empresa eliminada:  por: Rafa Alpuche','2|Nexus de México|1','2017-09-01','07:51:52','',1),(56,2,2,'El usuario cerró sesión: Rafa Alpuche','00:46:33127.0.0.1|2|Rafa Alpuche|ralpuchev','2017-09-01','07:54:40','',1),(57,3,0,'Datos de acceso incorrectos. Usuario: ralpuchev y password: lionheart','','2017-09-01','07:54:49','',1),(58,3,0,'Datos de acceso incorrectos. Usuario: ralpuchev y password: lionheart','','2017-09-01','07:54:56','',1),(59,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-09-01','07:55:27','',1),(60,7,2,'Usuario eliminado:  por: Rafa Alpuche','4|Armando Alpuche|2|mando|alpuche2016|mando@gmail.com|1','2017-09-01','07:55:51','',1),(61,2,2,'El usuario cerró sesión: Rafa Alpuche','07:55:27127.0.0.1|2|Rafa Alpuche|ralpuchev','2017-09-01','08:10:58','',1),(62,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-09-01','08:11:03','',1),(63,2,2,'El usuario cerró sesión: Rafa Alpuche','08:11:03127.0.0.1|2|Rafa Alpuche|ralpuchev','2017-09-01','08:32:24','',1),(64,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-09-01','08:32:29','',1),(65,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-09-01','23:22:08','',1),(66,14,2,'Datos de facturacion agregados: Rafa Alpuche RFC: AUVR8702108R5','','2017-09-01','23:23:10','',1),(67,15,2,'Datos de facturacion actualizados: Rafa Alpuche RFC: AUVR8702108R5','809|Rafa Alpuche|AUVR8702108R5|LAGO TUS|5|506||ANAHUAC|MIGUEL HIDALGO|CDMX|MEXICO|MEXICO|911160|2282183578|ralpuchev@gmail.com|||1','2017-09-01','23:53:58','',1),(68,15,2,'Datos de facturacion actualizados: Rafa Alpuche RFC: AUVR8702108R5','809|Rafa Alpuche|AUVR8702108R5|LAGO TUS|5|506||ANAHUAC|MIGUEL HIDALGO|CDMX|MEXICO|MEXICO|911160|2282183578|ralpuchev@gmail.com||506|1','2017-09-01','23:55:44','',1),(69,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-09-02','00:13:47','',1),(70,14,2,'Datos de facturacion agregados: Mailene Alpuche RFC: AUMA887456','','2017-09-02','00:21:35','',1),(71,14,2,'Datos de facturacion agregados: Armando Alpuche Velázquez RFC: ARMA8745669','','2017-09-02','00:26:15','',1),(72,14,2,'Datos de facturacion agregados: Moises Lagunes Muro RFC: MULA8745231','','2017-09-02','00:32:17','',1),(73,14,2,'Datos de facturacion agregados: Alicia Lagunes Muro RFC: ALICE8975464','','2017-09-02','00:34:36','',1),(74,14,2,'Datos de facturacion agregados: Abertano Nuñez RFC: NUGA874512','','2017-09-02','00:37:15','',1),(75,17,2,'Cliente agregado al sistema: Rafa Alpuche agregado por: Rafa Alpuche','','2017-09-02','12:30:49','',1),(76,17,2,'Cliente agregado al sistema: Rafa Alpuche Velázquez agregado por: Rafa Alpuche','','2017-09-02','16:00:53','',1),(77,18,2,'Cliente actualizado: Rafa Alpuche Velázquez Papacito por: Rafa Alpuche','2|Rafa Alpuche Velázquez|809','2017-09-02','16:02:42','',1),(78,18,2,'Cliente actualizado: Rafa Alpuche Velázquez por: Rafa Alpuche','2|Rafa Alpuche Velázquez Papacito|809','2017-09-02','16:02:52','',1),(79,18,2,'Cliente actualizado: Rafa Alpuche Velázquez por: Rafa Alpuche','2|Rafa Alpuche Velázquez|809','2017-09-02','16:02:57','',1),(80,18,2,'Cliente actualizado: Rafa Alpuche Velázquez por: Rafa Alpuche','2|Rafa Alpuche Velázquez|814','2017-09-02','16:03:04','',1),(81,13,2,'Empresa eliminada: 1 por: Rafa Alpuche','1|Mega Comercial|1','2017-09-02','16:10:45','',1),(82,19,2,'Cliente Eliminado: 1 por: Rafa Alpuche','1|Rafa Alpuche|809','2017-09-02','16:12:06','',1),(83,20,2,'Concepto Agregado: Internet','','2017-09-02','23:49:36','',1),(84,20,2,'Concepto Agregado: Despensa','','2017-09-02','23:50:41','',1),(85,20,2,'Concepto Agregado: Eukanuba','','2017-09-02','23:51:16','',1),(86,21,2,'Concepto modificado: Eukanuba Para Yego','3|Eukanuba|1','2017-09-02','23:52:37','',1),(87,20,2,'Concepto Agregado: Eukanuba para Popo','','2017-09-02','23:53:00','',1),(88,20,2,'Concepto Agregado: Agua','','2017-09-03','00:00:27','',1),(89,20,2,'Concepto Agregado: Luz','','2017-09-03','00:01:15','',1),(90,17,2,'Cliente agregado al sistema: Un nombre x agregado por: Rafa Alpuche','','2017-09-03','00:02:46','',1),(91,21,2,'Concepto modificado: Internet Telmex','1|Internet|1','2017-09-03','00:10:02','',1),(92,22,2,'Concepto eliminado: 4','4|Eukanuba para Popo|1','2017-09-03','01:20:01','',1),(93,22,2,'Concepto eliminado: 4','4|Eukanuba para Popo|0','2017-09-03','01:20:27','',1),(94,22,2,'Concepto eliminado: 3','3|Eukanuba Para Yego|1','2017-09-03','01:21:51','',1),(95,20,2,'Concepto Agregado: Eukanuba para los bebes','','2017-09-03','01:22:08','',1),(96,21,2,'Concepto modificado: Despensa de la Comer','2|Despensa|1','2017-09-03','01:32:22','',1),(97,21,2,'Concepto modificado: Despensa','2|Despensa de la Comer|1','2017-09-03','01:32:31','',1),(98,21,2,'Concepto modificado: Eukanuba','7|Eukanuba para los bebes|1','2017-09-03','01:32:50','',1),(99,21,2,'Concepto modificado: Eukanuba de Pugs','7|Eukanuba|1','2017-09-03','01:33:32','',1),(100,20,2,'Concepto Agregado: Axtel Extremo','','2017-09-03','01:33:45','',1),(101,22,2,'Concepto eliminado: 5','5|Agua|1','2017-09-03','01:33:55','',1),(102,23,2,'Gasto Interno agregado: 1','','2017-09-03','02:02:49','',1),(103,24,2,'Gasto Interno modificado: 1','1|1|2017-09-03|02:02:00|1500|2|1','2017-09-03','02:18:06','',1),(104,24,2,'Gasto Interno modificado: 2','1|1|2017-09-03|02:02:00|1600|2|1','2017-09-03','02:18:14','',1),(105,24,2,'Gasto Interno modificado: 2','1|2|2017-09-03|02:02:00|1600|2|1','2017-09-03','02:18:25','',1),(106,23,2,'Gasto Interno agregado: 1','','2017-09-03','02:21:04','',1),(107,25,2,'Gasto Interno Eliminado: 2','2|1|2017-09-03|02:21:00|380|2|1','2017-09-03','02:21:13','',1),(108,5,2,'Usuario agregado al sistema: Alma Delia Méndez Sánchez agregado por: Rafa Alpuche','','2017-09-03','13:07:31','',1),(109,26,2,'Gasto de Cliente agregado','','2017-09-03','13:43:08','',1),(110,27,2,'1|5|2|2|1|2017-09-03|13:43:08|1850.00|0.1|2|1|Alma Delia Méndez Sánchez|Nexus de México|Rafa Alpuche Velázquez|BBVA - Bancomer','','2017-09-03','14:12:21','',1),(111,27,2,'Gasto de Cliente Actualizado','1|5|2|2|1|2017-09-03|13:43:08|1850.00|0.2|2|1|Alma Delia Méndez Sánchez|Nexus de México|Rafa Alpuche Velázquez|BBVA - Bancomer','2017-09-03','14:12:57','',1),(112,28,2,'Gasto de Cliente Eliminado','1|5|2|2|1|2017-09-03|13:43:08|1850.00|0.2|2|1|Alma Delia Méndez Sánchez|Nexus de México|Rafa Alpuche Velázquez|BBVA - Bancomer','2017-09-03','14:18:13','',1),(113,2,2,'El usuario cerró sesión: Rafa Alpuche','23:22:08127.0.0.1|2|Rafa Alpuche|ralpuchev','2017-09-03','14:19:14','',1),(114,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-09-03','14:19:23','',1),(115,26,2,'Gasto de Cliente agregado','','2017-09-03','14:54:33','',1),(116,27,2,'Gasto de Cliente Actualizado','2|5|2|2|1|2017-09-03|14:54:33|8500|0.1|2|1|Alma Delia Méndez Sánchez|Nexus de México|Rafa Alpuche Velázquez|BBVA - Bancomer','2017-09-03','14:54:37','',1),(117,27,2,'Gasto de Cliente Actualizado','2|5|2|2|1|2017-09-03|14:54:33|8500|0.1|2|1|Alma Delia Méndez Sánchez|Nexus de México|Rafa Alpuche Velázquez|BBVA - Bancomer','2017-09-03','17:29:49','',1),(118,27,2,'Gasto de Cliente Actualizado','2|5|2|2|1|2017-09-03|14:54:33|8500|0.1|2|2|Alma Delia Méndez Sánchez|Nexus de México|Rafa Alpuche Velázquez|BBVA - Bancomer','2017-09-03','17:31:05','',1),(119,27,2,'Gasto de Cliente Actualizado','2|5|2|2|1|2017-09-03|14:54:33|8500|0.1|2|1|Alma Delia Méndez Sánchez|Nexus de México|Rafa Alpuche Velázquez|BBVA - Bancomer','2017-09-03','17:31:17','',1),(120,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-09-03','17:49:10','',1),(121,27,2,'Gasto de Cliente Actualizado','2|5|2|2|1|2017-09-03|14:54:33|8500|0.1|2|2|Alma Delia Méndez Sánchez|Nexus de México|Rafa Alpuche Velázquez|BBVA - Bancomer','2017-09-03','19:16:33','',1),(122,27,2,'Gasto de Cliente Modificado','2|5|2|2|1|2017-09-03|14:54:33|8500|0.1|2|1|Alma Delia Méndez Sánchez|Nexus de México|Rafa Alpuche Velázquez|BBVA - Bancomer','2017-09-03','19:43:51','',1),(123,27,2,'Gasto de Cliente Actualizado','2|5|2|2|1|2017-09-03|14:54:33|8500|0.1|2|2|Alma Delia Méndez Sánchez|Nexus de México|Rafa Alpuche Velázquez|BBVA - Bancomer','2017-09-03','19:44:07','',1),(124,27,2,'Gasto de Cliente Modificado','2|5|2|2|1|2017-09-03|14:54:33|8500|0.1|2|1|Alma Delia Méndez Sánchez|Nexus de México|Rafa Alpuche Velázquez|BBVA - Bancomer','2017-09-03','19:44:11','',1),(125,27,2,'Gasto de Cliente Modificado','2|5|2|2|1|2017-09-03|14:54:33|8500|0.1|2|2|Alma Delia Méndez Sánchez|Nexus de México|Rafa Alpuche Velázquez|BBVA - Bancomer','2017-09-03','19:44:40','',1),(126,27,2,'Gasto de Cliente Modificado','2|5|2|2|1|2017-09-03|14:54:33|8500|0.1|2|1|Alma Delia Méndez Sánchez|Nexus de México|Rafa Alpuche Velázquez|BBVA - Bancomer','2017-09-03','19:44:42','',1),(127,27,2,'Gasto de Cliente Modificado','2|5|2|2|1|2017-09-03|14:54:33|8500|0.1|2|2|Alma Delia Méndez Sánchez|Nexus de México|Rafa Alpuche Velázquez|BBVA - Bancomer','2017-09-03','19:44:43','',1),(128,27,2,'Gasto de Cliente Actualizado','2|5|2|2|1|2017-09-03|14:54:33|8500|0.1|2|1|Alma Delia Méndez Sánchez|Nexus de México|Rafa Alpuche Velázquez|BBVA - Bancomer','2017-09-03','19:44:53','',1),(129,27,2,'Gasto de Cliente Actualizado','2|5|2|2|1|2017-09-03|14:54:33|8500|0.1|2|2|Alma Delia Méndez Sánchez|Nexus de México|Rafa Alpuche Velázquez|BBVA - Bancomer','2017-09-03','19:44:59','',1),(130,27,2,'Gasto de Cliente Actualizado','2|5|2|2|1|2017-09-03|14:54:33|8500|0.1|2|1|Alma Delia Méndez Sánchez|Nexus de México|Rafa Alpuche Velázquez|BBVA - Bancomer','2017-09-03','19:46:13','',1),(131,5,2,'Usuario agregado al sistema: Carlos Garcia agregado por: Rafa Alpuche','','2017-09-03','22:56:23','',1),(132,2,2,'El usuario cerró sesión: Rafa Alpuche','14:19:23127.0.0.1|2|Rafa Alpuche|ralpuchev','2017-09-03','23:10:18','',1),(133,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-09-03','23:10:24','',1),(134,2,2,'El usuario cerró sesión: Rafa Alpuche','23:10:24127.0.0.1|2|Rafa Alpuche|ralpuchev','2017-09-03','23:10:26','',1),(135,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-09-03','23:10:31','',1),(136,2,2,'El usuario cerró sesión: Rafa Alpuche','23:10:31127.0.0.1|2|Rafa Alpuche|ralpuchev','2017-09-03','23:10:32','',1),(137,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-09-03','23:10:37','',1),(138,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-09-04','00:42:58','',1),(139,27,2,'Gasto de Cliente Actualizado','2|5|2|2|1|2017-09-03|14:54:33|8500|0.1|2|2|Alma Delia Méndez Sánchez|Nexus de México|Rafa Alpuche Velázquez|BBVA - Bancomer','2017-09-04','01:56:37','',1),(140,2,2,'El usuario cerró sesión: Rafa Alpuche','00:42:58127.0.0.1|2|Rafa Alpuche|ralpuchev','2017-09-04','02:00:26','',1),(141,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-09-04','02:01:19','',1),(142,2,2,'El usuario cerró sesión: Rafa Alpuche','02:01:19127.0.0.1|2|Rafa Alpuche|ralpuchev','2017-09-04','02:02:55','',1),(143,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-09-04','02:07:07','',1),(144,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-09-04','02:09:24','',1),(145,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-09-04','02:27:51','',1);
/*!40000 ALTER TABLE `Bitacora` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Clientes`
--

DROP TABLE IF EXISTS `Clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Clientes` (
  `idCliente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `idDatosFacturacion` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  PRIMARY KEY (`idCliente`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Clientes`
--

LOCK TABLES `Clientes` WRITE;
/*!40000 ALTER TABLE `Clientes` DISABLE KEYS */;
INSERT INTO `Clientes` VALUES (1,'Rafa Alpuche',809,0),(2,'Rafa Alpuche Velázquez',809,1),(3,'Un nombre x',0,1);
/*!40000 ALTER TABLE `Clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ConceptosGastosInternos`
--

DROP TABLE IF EXISTS `ConceptosGastosInternos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ConceptosGastosInternos` (
  `idConcepto` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` text NOT NULL,
  `estatus` int(11) NOT NULL,
  PRIMARY KEY (`idConcepto`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ConceptosGastosInternos`
--

LOCK TABLES `ConceptosGastosInternos` WRITE;
/*!40000 ALTER TABLE `ConceptosGastosInternos` DISABLE KEYS */;
INSERT INTO `ConceptosGastosInternos` VALUES (1,'Internet Telmex',1),(2,'Despensa',1),(3,'Eukanuba Para Yego',0),(4,'Eukanuba para Popo',0),(5,'Agua',0),(6,'Luz',1),(7,'Eukanuba de Pugs',1),(8,'Axtel Extremo',1);
/*!40000 ALTER TABLE `ConceptosGastosInternos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DatosFacturacion`
--

DROP TABLE IF EXISTS `DatosFacturacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DatosFacturacion` (
  `idDatosFacturacion` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` text COLLATE utf8_spanish2_ci NOT NULL,
  `rfc` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `calle` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `exterior` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `interior` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `referencia` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `colonia` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `localidad` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `municipio` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `estado` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `pais` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `cp` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `telefono` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `fax` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `cuenta` varchar(250) COLLATE utf8_spanish2_ci NOT NULL,
  `estatus` int(11) NOT NULL,
  PRIMARY KEY (`idDatosFacturacion`)
) ENGINE=InnoDB AUTO_INCREMENT=815 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DatosFacturacion`
--

LOCK TABLES `DatosFacturacion` WRITE;
/*!40000 ALTER TABLE `DatosFacturacion` DISABLE KEYS */;
INSERT INTO `DatosFacturacion` VALUES (809,'Rafa Alpuche','AUVR8702108R5','LAGO TUS','5','506','Por Calle Carrillo Puerto','ANAHUAC','MIGUEL HIDALGO','CDMX','MEXICO','MEXICO','911160','2282183578','ralpuchev@gmail.com','','506',1),(810,'Mailene Alpuche','AUMA887456','Prol. Adolfo López Mateos','5','','','Otilpan','San Andrés Tal','San Andrés Tal','Veracruz','Mexico','91050','2281183578','','','',1),(811,'Armando Alpuche Velázquez','ARMA8745669','Alguna','8','','','Alguna','Tux','Tux','Oaxaca','Mexico','91020','','','','',1),(812,'Moises Lagunes Muro','MULA8745231','Xicotencatl','5','','','Centro','Coatepec','Coatepec','Veracruz','Mexico','91000','2282183578','','','',1),(813,'Alicia Lagunes Muro','ALICE8975464','Zaragoza','1009','','','Centro','Coatzacoalcos','Veracruz','Veracruz','Mexico','91050','','','','',1),(814,'Abertano Nuñez','NUGA874512','Xico','54','','','Centro','Xico','Xico','Veracruz','Mexico','91050','','','','',1);
/*!40000 ALTER TABLE `DatosFacturacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Empresas`
--

DROP TABLE IF EXISTS `Empresas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Empresas` (
  `idEmpresa` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `estatus` int(11) NOT NULL,
  PRIMARY KEY (`idEmpresa`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Empresas`
--

LOCK TABLES `Empresas` WRITE;
/*!40000 ALTER TABLE `Empresas` DISABLE KEYS */;
INSERT INTO `Empresas` VALUES (1,'Mega Comercial',0),(2,'Nexus de México',1);
/*!40000 ALTER TABLE `Empresas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GastosClientes`
--

DROP TABLE IF EXISTS `GastosClientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GastosClientes` (
  `idGastoCliente` int(11) NOT NULL AUTO_INCREMENT,
  `idPromotor` int(11) NOT NULL,
  `idEmpresa` int(11) NOT NULL,
  `idCliente` int(11) NOT NULL,
  `idBanco` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `cantidadTotal` varchar(255) NOT NULL,
  `porcentaje` varchar(255) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  PRIMARY KEY (`idGastoCliente`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GastosClientes`
--

LOCK TABLES `GastosClientes` WRITE;
/*!40000 ALTER TABLE `GastosClientes` DISABLE KEYS */;
INSERT INTO `GastosClientes` VALUES (1,5,2,2,1,'2017-09-03','13:43:08','1850.00','0.2',2,0),(2,5,2,2,1,'2017-09-03','14:54:33','8500','0.1',2,1);
/*!40000 ALTER TABLE `GastosClientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GastosInternos`
--

DROP TABLE IF EXISTS `GastosInternos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GastosInternos` (
  `idGastosInterno` int(11) NOT NULL AUTO_INCREMENT,
  `idConcepto` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `cantidad` varchar(255) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `estatus` int(11) NOT NULL,
  PRIMARY KEY (`idGastosInterno`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GastosInternos`
--

LOCK TABLES `GastosInternos` WRITE;
/*!40000 ALTER TABLE `GastosInternos` DISABLE KEYS */;
INSERT INTO `GastosInternos` VALUES (1,2,'2017-09-03','02:02:00','1600',2,1),(2,1,'2017-09-03','02:21:00','380',2,0);
/*!40000 ALTER TABLE `GastosInternos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Usuarios`
--

DROP TABLE IF EXISTS `Usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Usuarios` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombreCompleto` varchar(250) NOT NULL,
  `idPerfil` int(11) NOT NULL,
  `nombreUsuario` varchar(250) NOT NULL,
  `passwordUsuario` varchar(250) NOT NULL,
  `correoElectronico` varchar(250) NOT NULL,
  `estatus` int(11) NOT NULL,
  PRIMARY KEY (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Usuarios`
--

LOCK TABLES `Usuarios` WRITE;
/*!40000 ALTER TABLE `Usuarios` DISABLE KEYS */;
INSERT INTO `Usuarios` VALUES (1,'System',1,'System','System1357908642','ralpuchev@gmail.com',1),(2,'Rafa Alpuche',1,'ralpuchev','lionheart','ralpuchev@gmail.com',1),(3,'David Alpuche',1,'david','alpuche7','david@gmail.com',1),(4,'Armando Alpuche',2,'mando','alpuche2016','mando@gmail.com',1),(5,'Alma Delia Méndez Sánchez',3,'almiis','12345678','',1),(6,'Carlos Garcia',3,'charly','12345678','',1);
/*!40000 ALTER TABLE `Usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-09-04  2:39:03
