-- MySQL dump 10.13  Distrib 5.7.12, for osx10.9 (x86_64)
--
-- Host: 127.0.0.1    Database: orbe
-- ------------------------------------------------------
-- Server version	5.7.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `Bitacora`
--

LOCK TABLES `Bitacora` WRITE;
/*!40000 ALTER TABLE `Bitacora` DISABLE KEYS */;
INSERT INTO `Bitacora` VALUES (13,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-08-28','07:38:40','',1),(14,2,2,'El usuario cerró sesión: Rafa Alpuche','07:38:40::1|2|Rafa Alpuche|ralpuchev','2017-08-28','07:44:16','',1),(15,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-08-28','07:44:44','',1),(16,2,2,'El usuario cerró sesión: Rafa Alpuche','07:44:44::1|2|Rafa Alpuche|ralpuchev','2017-08-28','08:02:49','',1),(17,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-08-28','08:02:57','',1),(18,2,2,'El usuario cerró sesión: Rafa Alpuche','1166<->3160|2||','2017-08-31','01:43:10','',1),(19,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-08-31','01:43:17','',1),(20,2,2,'El usuario cerró sesión: Rafa Alpuche','01:43:17127.0.0.1|2|Rafa Alpuche|ralpuchev','2017-08-31','01:43:34','',1),(21,1,2,'El usuario inició sesión. Nombre de usuario: ralpuchev','','2017-08-31','01:43:49','',1);
/*!40000 ALTER TABLE `Bitacora` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Usuarios`
--

LOCK TABLES `Usuarios` WRITE;
/*!40000 ALTER TABLE `Usuarios` DISABLE KEYS */;
INSERT INTO `Usuarios` VALUES (1,'System',1,'System','System1357908642','ralpuchev@gmail.com',1),(2,'Rafa Alpuche',1,'ralpuchev','lionheart','ralpuchev@gmail.com',1);
/*!40000 ALTER TABLE `Usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-31  9:01:45
